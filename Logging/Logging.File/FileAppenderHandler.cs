﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using log4net.Appender;
using yats.Logging.Interface;
using yats.TestCase.Interface;
using yats.Utilities;
using Newtonsoft.Json;

namespace yats.Logging.File
{
    public class FileAppenderHandler : ILogEventSink
    {
        public static string FileName = "yats.log";

        static string directory = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments ), "Yats" );
        public static string Directory
        {
            get 
            { 
                return directory; 
            }

            set 
            {
                directory = yats.Utilities.PlatformHelper.ExpandEnvironmentVariables(value);
            }
        }
		
		protected bool enabled = false;
		public bool Enabled 
		{
			get
			{
				return enabled;
			}
			set
			{
				enabled = value;
			}
		}

        protected log4net.Core.Level level = log4net.Core.Level.Debug;
        public log4net.Core.Level Level
        {
            get
            {
                return level;
            }
            set
            {
                level = value;
                lock(TestRunAppenders)
                {
                    foreach(var appender in TestRunAppenders.Values)
                    {
                        appender.Threshold = value;
                    }
                }
            }
        }
						
        public FileAppenderHandler()
        {			
        }

        protected static Dictionary<int, FileAppender> TestRunAppenders = new Dictionary<int, FileAppender>( );

        public void StartTestRun(string name, string description, byte[] settings, int testRunId)
        {
            if(!enabled)
            {
                return;
            }

            // remove characters that can not be used in file path
            string cleanedUpName = yats.Utilities.FileUtil.StripInvalidFileCharacters( name );
            if(string.IsNullOrEmpty( cleanedUpName ))
            {
                cleanedUpName = "Test run";
            }

            DateTime dt = DateTime.Now;//TODO: make configurable format?
            string logFolderName = String.Format( "[{0:yyyy.MM.dd HH_mm_ss}] - {1}", dt, cleanedUpName );

            string path = Path.Combine( directory, logFolderName );
            
            System.IO.Directory.CreateDirectory( path );
            string fileName = System.IO.Path.Combine( path, FileName );

            FileAppender appender = new FileAppender();
            appender.Layout = new log4net.Layout.PatternLayout( "%d{HH:mm:ss,fff} %-5level [%-25logger] - %message%newline" ); //TODO: make configurable
            appender.File = fileName;
            appender.Threshold = level;
            appender.ActivateOptions( );

            if(settings != null)
            {
                try
                {
                    System.IO.File.WriteAllBytes( System.IO.Path.Combine( path, "settings.testrun" ), settings );
                }
                catch
                {
                    //TODO
                }
            }

            lock(TestRunAppenders)
            {
                TestRunAppenders[testRunId] = appender;
            }
        }

        public void EndTestRun(int testRunId, ITestResult result)
        {
            lock(TestRunAppenders)
            {
                FileAppender appender = null;
                if(TestRunAppenders.TryGetValue( testRunId, out appender ) == false)
                {
                    return;
                }
                appender.Close( );
                TestRunAppenders.Remove(testRunId);
            }
        }

        public void UpdateStep(int testRunId, StepInfo stepInfo)
        {
            /*
            if(!enabled || base.File == null)
            {
                return;
            }

            ILog Logger = LogManager.GetLogger( "TestMetaData" );
            switch(stepInfo.RowType)
            {
                case StepInfoTypeEnum.STEP_START:
                    if(stepInfo.IsGroup)
                    {
                        Logger.WarnFormat( "Group start: {0}", stepInfo.Name );
                    }
                    else if(stepInfo.IsTestCase)
                    {
                        Logger.WarnFormat( "Test case start: {0}", stepInfo.Name );
                    }
                    break;
                //TODO
            }
            */
        }

        public void DoAppend(int? testRunId, string stepPath, log4net.Core.LoggingEvent loggingEvent, ref LogMessage logMessage)
        {
            try
            {
                if(enabled == false || testRunId.HasValue == false)
                {
                    return;
                }
                FileAppender appender = null;
                lock(TestRunAppenders)
                {
                    if(TestRunAppenders.TryGetValue( testRunId.Value, out appender ) == false)
                    {
                        return;
                    }
                }

                if(logMessage == null)
                {
                    logMessage = new LogMessage( stepPath, loggingEvent );
                }

                var data = new log4net.Core.LoggingEventData( );
                data.Level = loggingEvent.Level;
                data.LoggerName = loggingEvent.LoggerName;
                data.TimeStamp = logMessage.Timestamp;
                    
                if (string.IsNullOrEmpty(logMessage.Message) == false) {
                    data.Message = logMessage.Message;
                    appender.DoAppend( new log4net.Core.LoggingEvent( data ) );
                }

                if(string.IsNullOrEmpty( logMessage.BinaryDataType ) == false)
                {
                    data.Message = logMessage.BinaryDataType;
                    appender.DoAppend( new log4net.Core.LoggingEvent( data ) );
                }
                
                if(logMessage.BinaryData != null)
                {
                    foreach(var line in Encoding.ASCII.GetString( logMessage.BinaryData ).SplitByNewLines( ))
                    {
                        data.Message = line;
                        appender.DoAppend( new log4net.Core.LoggingEvent( data ) );
                    }
                }
            }
            catch { }
        }

        public void WriteBinaryData(int testRunId, string stepPath, string logger, string level, string message, object value)
        {
            try
            {
                var data = new log4net.Core.LoggingEventData( );
                data.Level = LogLevelHelper.Parse(level);
                data.LoggerName = logger;
                data.TimeStamp = DateTime.Now;
                                
                if(enabled == false)
                {
                    return;
                }
                FileAppender appender = null;
                lock(TestRunAppenders)
                {
                    if(TestRunAppenders.TryGetValue( testRunId, out appender ) == false)
                    {
                        return;
                    }
                }

                List<string> lines = new List<string>( );
                lines.Add( message );

                if(value != null)
                {
                    if(value is Exception)// attach stack trace for exceptions
                    {
                        lines.AddRange( value.ToString( ).SplitByNewLines( ) );
                    }
                    else if(value is byte[])// attach binary data for byte[]
                    {
                        lines.Add( (value as byte[]).ToHexString( ) );
                    }
                    else // try to serialize other objects
                    {
                        lines.AddRange( Serialize( value, value.GetType( ).AssemblyQualifiedName ) );
                    }
                }

                foreach(string s in lines)
                {
                    data.Message = s;
                    appender.DoAppend( new log4net.Core.LoggingEvent( data ) );
                }
            }
            catch { }
        }

        private static string[] s_emptyArray = new string[0];

        private string[] Serialize(object o, string typeName)
        {
            try
            {
                if(o is Exception)
                {
                    return o.ToString( ).SplitByNewLines( );
                }
                else
                {
                    var jsonString = JsonConvert.SerializeObject(o, Formatting.Indented);

                    return jsonString.SplitByNewLines( );
                }
            }
            catch
            {
                return s_emptyArray;
            }
        }

    }    
}
