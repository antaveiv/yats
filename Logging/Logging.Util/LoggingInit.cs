﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.IO;
using log4net;
using yats.Logging.File;
using yats.Logging.Interface;
using yats.Logging.SQLite;
using yats.Utilities;
using System.Security.Permissions;
using System.ComponentModel;

namespace yats.Logging.Util
{
    public delegate void LogInitErrorDelegate(string errorMessage);

    public class LoggingInit
    {
        public static event EventHandler<ConnectionErrorEventArgs> OnError;
        
        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        public static void Init(ILogSettings settings)
        {
            try {
                //TODO this smells
                var configFile = FileUtil.GetApplicationDataPath( "log4net.config" );
                var defaultLog4NetConfigFile = Path.Combine( FileUtil.GetExePath( ), "log4net.config" );
                if(System.IO.File.Exists( configFile ) == false && System.IO.File.Exists(defaultLog4NetConfigFile))
                {
                    System.IO.File.Copy(defaultLog4NetConfigFile, configFile);
                }

                //TODO: should be handled by software update
                if (System.IO.File.GetLastWriteTimeUtc(defaultLog4NetConfigFile) > System.IO.File.GetLastWriteTime(configFile))
                {
                    System.IO.File.Delete(configFile);
                    System.IO.File.Copy(defaultLog4NetConfigFile, configFile);
                }

                // Set Log4Net configuration file. It will be watched for changes
                log4net.Config.XmlConfigurator.ConfigureAndWatch( new FileInfo( configFile ) );

                // Force Log4Net initialization
                LogManager.GetRepository( );

            } 
            catch (Exception ex)
            {
                CrashLog.Write(ex, "Logging init");
            }

            #region SQLite initialization
            // used to disable on free versions
            //if (true)
            {
                string sqliteFileName = settings.ConnectionString;
                string sqliteEmptyDbPath = settings.ConnectionStringDefault;
                string sqliteFilePath = settings.SQLiteDbPath;

                bool reset = yats.Logging.SQLite.Properties.Settings.Default.ResetSQLiteDb;

                AppenderFactory.Asynchronous = yats.Logging.SQLite.Properties.Settings.Default.AsynchronousLogging;
                // Add error handler - show SQLite initialization error in message box
                AppenderFactory.OnError += OnError;
                AppenderFactory.Init( reset, sqliteFilePath, sqliteFileName, sqliteEmptyDbPath );

                if(AppenderFactory.IsInitializedSuccessfully == false)
                {
                    // Initialization failed
                    return;
                }
                if (yats.Logging.SQLite.Properties.Settings.Default.SQLiteLoggingEnabled)
                {
                    var sink = AppenderFactory.SinkInstance;
                    YatsLogAppenderCollection.Instance.AddEventSink(sink);
                }
            }
            //else
            //{
            // Just mark as initialized
            //AppenderFactory.InitDisabled( );
            //}
            #endregion

            #region File logging 

            FileLoggingSettingChanged(null, null);

            yats.Logging.File.Properties.Settings.Default.PropertyChanged += FileLoggingSettingChanged;

            #endregion
        }

        private static void FileLoggingSettingChanged(object sender, PropertyChangedEventArgs e)
        {
            var currentAppenders = YatsLogAppenderCollection.Instance.GetEventSinks< FileAppenderHandler>();
            foreach(var app in currentAppenders)
            {
                YatsLogAppenderCollection.Instance.RemoveEventSink(app);
            }

            if (yats.Logging.File.Properties.Settings.Default.FileLogEnabled)
            {
                FileAppenderHandler fileAppender = new FileAppenderHandler();
                fileAppender.Enabled = true;
                fileAppender.Level = LogLevelHelper.Parse(yats.Logging.File.Properties.Settings.Default.FileLogLevel);
                FileAppenderHandler.Directory = yats.Logging.File.Properties.Settings.Default.FileLogPath;
                YatsLogAppenderCollection.Instance.AddEventSink(fileAppender);
            }
        }

        public static void ApplicationExitHandler(object sender, EventArgs e)
        {
            AppenderFactory.CloseInstance();
        }
        
        public static bool IsDatabaseInitializedSuccessfully
        {
            get
            {
                return AppenderFactory.IsInitializedSuccessfully;
            }
        }
        
        public static IDatabaseTestRunIdMap GetDatabaseTestRunIdMap()
        {
            return DatabaseTestRunIdMap.Instance;
        }
    }
}
