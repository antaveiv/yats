﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace yats.Logging.Interface
{
    public class TestRunLog : TestRunLogInfo
    {
        public TestRunLog(List<StepInfo> steps,
                List<LogMessage> messages,
                DateTime startTime,
                string name,
                string description,
                byte[] settings): base(startTime, name, description)
        {
            this.steps = steps;
            this.messages = messages;
            this.settings = settings;
        }

        public TestRunLog(TestRunLogInfo info) : base(info.StartTime, info.Name, info.Description)
        {
        }

        public TestRunLog(TestRunLog log)
            : this(log.Steps, log.Messages, log.StartTime, log.Name, log.Description, log.Settings)
        {
        }
        
        protected List<StepInfo> steps;
        public List<StepInfo> Steps
        {
            get { return steps; }
            set { steps = value; }
        }

        protected List<LogMessage> messages;
        public List<LogMessage> Messages
        {
            get { return messages; }
            set { messages = value; }
        }
        
        protected byte[] settings;
        public byte[] Settings
        {
            get { return settings; }
            set { settings = value; }
        }

        [XmlIgnore]
        public ILogStorage Storage
        {
            get;
            set;
        }
    }
}
