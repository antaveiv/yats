/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Xml.Serialization;
using System.Linq;
using yats.Utilities;
using yats.TestCase.Interface;

namespace yats.Logging.Interface
{
    public class YatsLogAppenderCollection : ILogEventSource
    {
        #region Singleton
        //TODO refactor, remove
        private static YatsLogAppenderCollection s_instance = new YatsLogAppenderCollection();

        [XmlIgnore]
        public static YatsLogAppenderCollection Instance
        {
            [DebuggerStepThrough]
            get { return s_instance; }
        }

        [DebuggerStepThrough]
        private YatsLogAppenderCollection()
        {
        }

        #endregion

        #region Appender list managemet
        protected Dictionary<ILogEventSink, int?> eventSinkDictionary = new Dictionary<ILogEventSink, int?>();
        protected static ReaderWriterLock lck = new ReaderWriterLock();
        protected const int LOCK_TIMEOUT = 10000;


        public void AddEventSink(ILogEventSink eventSink)
        {
            lck.AcquireWriterLock(LOCK_TIMEOUT);
            foreach (var l in eventSinkDictionary)
            {
                if (l.Key.GetType() == eventSink.GetType() && l.Value == null)
                {
                    eventSinkDictionary.Remove(l.Key);
                    break;
                }
            }
            eventSinkDictionary[eventSink] = null;
            lck.ReleaseWriterLock();
        }

        public void AddEventSink(ILogEventSink eventSink, int testRunId)
        {
            lck.AcquireWriterLock(LOCK_TIMEOUT);
            foreach (var l in eventSinkDictionary)
            {
                if (l.Key.GetType() == eventSink.GetType() && l.Value == testRunId)
                {
                    eventSinkDictionary.Remove(l.Key);
                    break;
                }
            }
            eventSinkDictionary[eventSink] = testRunId;
            lck.ReleaseWriterLock();
        }

        public void RemoveEventSink(ILogEventSink eventSink)
        {
            lck.AcquireWriterLock(LOCK_TIMEOUT);
            eventSinkDictionary.Remove(eventSink);
            lck.ReleaseWriterLock();
        }

        public IEnumerable<T> GetEventSinks<T>() where T : ILogEventSink 
        {
            lck.AcquireReaderLock(LOCK_TIMEOUT);
            var result = eventSinkDictionary.Keys.Where(s => s is T).Select(s => (T)s).ToList();
            lck.ReleaseReaderLock();
            return result;
        }

        //not used
        public List<T> GetByType<T>() where T : class, ILogEventSink
        {
            var result = new List<T>();
            lck.AcquireReaderLock(LOCK_TIMEOUT);
            foreach (var log in eventSinkDictionary)
            {
                result.AddIfNotNull(log as T);
            }

            lck.ReleaseReaderLock();
            return result;
        }

        #endregion

        #region IYatsLogAppender implementation

        public void StartTestRun(string name, string description, byte[] settings, int testRunId)
        {
            lck.AcquireReaderLock(LOCK_TIMEOUT);
            foreach (var pair in eventSinkDictionary)
            {
                if (pair.Value.HasValue && pair.Value.Value != testRunId)
                {
                    continue;
                }
                pair.Key.StartTestRun(name, description, settings, testRunId);
            }
            lck.ReleaseReaderLock();
        }

        public void EndTestRun(int testRunId, ITestResult result)
        {
            lck.AcquireReaderLock(LOCK_TIMEOUT);
            foreach (var pair in eventSinkDictionary)
            {
                if (pair.Value.HasValue && pair.Value.Value != testRunId)
                {
                    continue;
                }
                pair.Key.EndTestRun(testRunId, result);
            }
            lck.ReleaseReaderLock();
        }

        /// <summary>
        /// Associates thread ID of a test case being executed with its hierarchy 'path'
        /// </summary>
        protected class ThreadMapping
        {
            public int testRunId;
            public string stepPath;
            public ThreadMapping(int testRunId, string stepPath)
            {
                this.testRunId = testRunId;
                this.stepPath = stepPath;
            }
        }

        protected Dictionary<int, ThreadMapping> ThreadTestMapping = new Dictionary<int, ThreadMapping>();

        public void UpdateStep(int testRunId, StepInfo stepInfo)
        {
            lck.AcquireReaderLock(LOCK_TIMEOUT);
            var app = eventSinkDictionary.SelectKeys(x => (x.HasValue == false || x.Value == testRunId));
            lck.ReleaseReaderLock();
            foreach (var appender in app)
            {
                appender.UpdateStep(testRunId, stepInfo);
            }

            switch (stepInfo.RowType)
            {
                case StepInfoTypeEnum.STEP_START:
                    ThreadTestMapping[Thread.CurrentThread.ManagedThreadId] = new ThreadMapping(testRunId, stepInfo.TreePath);
                    break;
                case StepInfoTypeEnum.STEP_RESULT:
                case StepInfoTypeEnum.REPETITION_RESULT:
                    ThreadTestMapping.Remove(Thread.CurrentThread.ManagedThreadId);
                    break;
            }
        }
                
        #endregion

        #region log4net
        /// <summary>
        /// called by log4net DoAppend in Log4NetAppender
        /// </summary>
        /// <param name="loggingEvent"></param>
        public void DoAppend(log4net.Core.LoggingEvent loggingEvent)
        {
            int? testRunId = null;
            string stepPath = null;

            ThreadMapping threadInfo = null;
            ThreadTestMapping.TryGetValue(Thread.CurrentThread.ManagedThreadId, out threadInfo);


            if (threadInfo != null)
            {
                testRunId = threadInfo.testRunId;
                stepPath = threadInfo.stepPath;
            }

            lck.AcquireReaderLock(LOCK_TIMEOUT);
            // get all appenders that are listening for events on this test run
            var app = eventSinkDictionary.SelectKeys(x => (x.HasValue == false || (threadInfo != null && x.Value == threadInfo.testRunId)));
            lck.ReleaseReaderLock();

            // dispatch the message to all appenders. 
            LogMessage logMessage = null;

            foreach (var appender in app)
            {
                appender.DoAppend(testRunId, stepPath, loggingEvent, ref logMessage);
            }
        }

        #endregion

        public void WriteBinaryData(int testRunId, string logger, string level, string message, object value)
        {
            ThreadMapping threadInfo = null;
            ThreadTestMapping.TryGetValue(Thread.CurrentThread.ManagedThreadId, out threadInfo);

            string stepPath = null;

            if (threadInfo != null)
            {
                if (threadInfo.testRunId != testRunId)
                {
                    //WTF
                    //TODO:log
                    return;
                }
                stepPath = threadInfo.stepPath;
            }

            lck.AcquireReaderLock(LOCK_TIMEOUT);
            var app = eventSinkDictionary.SelectKeys(x => (x.HasValue == false || x.Value == testRunId));
            lck.ReleaseReaderLock();
            foreach (var appender in app)
            {
                DateTime start = DateTime.Now;
                appender.WriteBinaryData(testRunId, stepPath, logger, level, message, value);
                DateTime end = DateTime.Now;
                var ms = end.Subtract(start).TotalMilliseconds;
                if (ms > 100)
                {
                    Console.WriteLine("long logging: " + appender.GetType().Name + ", " + ms);
                }
            }
        }

    }
}

