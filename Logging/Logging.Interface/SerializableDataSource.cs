﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;

namespace yats.Logging.Interface
{
    public class SerializableDataSource : IDataSource
    {
        public const string VERSION = "v1";

        public SerializableDataSource()
        {
        }

        public SerializableDataSource(List<StepInfo> steps,
                List<LogMessage> messages,
                DateTime startTime,
                string name,
                string description,
                byte[] settings)
        {
            this.steps = steps;
            this.messages = messages;
            this.startTime = startTime;
            this.name = name;
            this.description = description;
            this.settings = settings;
        }

        #region serializable properties
        protected List<StepInfo> steps;
        public List<StepInfo> Steps
        {
            get { return steps; }
            set { steps = value; }
        }

        protected List<SerializableLogMessage> serializableLogMessages;
        public List<SerializableLogMessage> SerializableLogMessages
        {
            get {
                if (serializableLogMessages == null && messages != null)
                {
                    serializableLogMessages = new List<SerializableLogMessage>(messages.Count);
                    foreach (var msg in messages)
                    {
                        serializableLogMessages.Add(new SerializableLogMessage(msg));
                    }
                    messages = null;
                }
                return serializableLogMessages;
            }
            set { serializableLogMessages = value; messages = null; }
        }

        protected DateTime startTime;
        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }

        protected string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        protected string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        protected byte[] settings;
        public byte[] Settings
        {
            get { return settings; }
            set { settings = value; }
        }

        protected string formatVersion = VERSION;
        public string FormatVersion
        {
            get { return formatVersion; }
            set { formatVersion = value; }
        }

        #endregion

        #region IDataSource Members

        public List<StepInfo> GetSteps()
        {
            return steps;
        }

#pragma warning disable 0067
        public event StepAddedDelegate TestStepAdded;
        public event MessageAddedDelegate MessageAdded;
#pragma warning restore 0067

        List<LogMessage> messages = null;
        public List<LogMessage> GetMessages()
        {
            // store either serialized or deserialized messages, not both. One is used for saving, another for loading, therefore conversion is done only once
            if (messages == null && serializableLogMessages != null)
            {
                messages = new List<LogMessage>(serializableLogMessages.Count);
                foreach (var msg in serializableLogMessages)
                {
                    messages.Add(new LogMessage(msg));
                }
                serializableLogMessages = null;
            }
            return messages;
        }



        #endregion

        public void Dispose()
        {
        }
    }
}
