﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Text;

namespace yats.Logging.Interface
{
    public class SerializableLogMessage
    {
        public long Id;
        public string Path;
        public DateTime Timestamp;
        public string Logger;
        public string Level;
        public string Message;
        public string BinaryData;
        public string BinaryDataType;

        public SerializableLogMessage()
        {
        }

        public SerializableLogMessage(LogMessage msg)
        {
            this.Id = msg.Id;
            this.Path= msg.Path;
            this.Timestamp = msg.Timestamp;
            this.Logger= msg.Logger;
            this.Level  = msg.Level;
            if (msg.Message != null)
            {
                this.Message = Convert.ToBase64String(Encoding.UTF8.GetBytes(msg.Message));
            }
            if (msg.BinaryData != null)
            {
                this.BinaryData = Convert.ToBase64String(msg.BinaryData);
            }
            this.BinaryDataType = msg.BinaryDataType;
        }
    }
}
