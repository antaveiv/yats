﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using log4net.Core;
using yats.Logging.Interface;
using yats.TestCase.Interface;
using yats.Utilities;

namespace yats.Logging.Interface
{
    // wraps around DatabaseAppender and calls DB updates asynchronously
    public class AsynchronousAppender : ILogEventSink, IDisposable
    {
        private readonly ILogEventSink m_appender;
        public AsynchronousAppender(ILogEventSink appender)
        {
            this.m_appender = appender;
            m_queue = new ProducerConsumer<DbTask>(1, System.Threading.ThreadPriority.BelowNormal, this.ConsumptionFunction);
        }

        // these methods do not directly call the appender but queue the actions instead
        public void StartTestRun(string name, string description, byte[] settings, int testRunId)
        {
            m_queue.EnqueueTask(new StartTestRunTask(m_appender, name, description, settings, testRunId));
            GenerateQueueSizeEvent();
        }
        
        public void EndTestRun(int testRunId, ITestResult result)
        {
            m_queue.EnqueueTask(new EndTestRunTask(m_appender, testRunId, result));
            GenerateQueueSizeEvent();
        }
        
        public void UpdateStep(int testRunId, StepInfo stepInfo)
        {
            m_queue.EnqueueTask(new UpdateStepTask(m_appender, testRunId, stepInfo));
            GenerateQueueSizeEvent();
        }
        
        public void DoAppend(int? testRunId, string stepPath, LoggingEvent loggingEvent, ref LogMessage logMessage)
        {
            if (testRunId.HasValue == false || stepPath == null)
            {
                return;
            }
            if (logMessage == null)
            {
                logMessage = new LogMessage(stepPath, loggingEvent);
            }
            m_queue.EnqueueTask(new DoAppendTask(m_appender, testRunId, stepPath, loggingEvent, logMessage));
            GenerateQueueSizeEvent();
        }

        public void WriteBinaryData(int testRunId, string stepPath, string logger, string level, string message, object value)
        {
            LogMessage msg = new LogMessage(stepPath, logger, level, message, value);
            m_queue.EnqueueTask(new DoAppendTask(m_appender, testRunId, stepPath, null, msg)); //is null OK here? Yes, as long as msg is not.
            GenerateQueueSizeEvent();
        }
        
        private ProducerConsumer<DbTask> m_queue;

        static int [] queueSizeTriggers = new int[]{10, 100, 1000, 10000, 100000};

        private void ConsumptionFunction(DbTask action)
        {
            action.DoWork();
            GenerateQueueSizeEvent();
        }

        private void GenerateQueueSizeEvent()
        {
            if (OnQueueSizeChange != null)
            {
                int ct = m_queue.Count;
                foreach (int t in queueSizeTriggers)
                {
                    if (ct == t)
                    {
                        OnQueueSizeChange(ct);
                    }
                }
            }
        }        

        private abstract class DbTask
        {
            public abstract void DoWork();
            protected ILogEventSink appender;
            public DbTask(ILogEventSink appender)
            {
                this.appender = appender;
            }
        }

        private class StartTestRunTask : DbTask
        {
            private string m_name;
            private string m_description;
            private byte[] m_settings;
            private int m_testRunId;

            public StartTestRunTask(ILogEventSink appender, string name, string description, byte[] settings, int testRunId)
                : base(appender)
            {
                this.m_name = name;
                this.m_description = description;
                this.m_settings = settings;
                this.m_testRunId = testRunId;
            }

            public override void DoWork()
            {
                //Console.WriteLine("StartTestRunTask");
                appender.StartTestRun(m_name, m_description, m_settings, m_testRunId);
            }
        }

        private class EndTestRunTask : DbTask
        {
            private int m_testRunId;
            private ITestResult m_result;

            public EndTestRunTask(ILogEventSink appender, int testRunId, ITestResult result)
                : base(appender)
            {
                this.m_testRunId = testRunId;
                this.m_result = result;
            }

            public override void DoWork()
            {
                //Console.WriteLine("EndTestRunTask");
                appender.EndTestRun(m_testRunId, m_result);
            }
        }

        private class UpdateStepTask : DbTask
        {
            private int m_testRunId;
            private StepInfo m_stepInfo;

            public UpdateStepTask(ILogEventSink appender, int testRunId, StepInfo stepInfo)
                : base(appender)
            {
                this.m_testRunId = testRunId;
                this.m_stepInfo = stepInfo;
            }

            public override void DoWork()
            {
                //Console.WriteLine("UpdateStepTask " + stepInfo);
                appender.UpdateStep(m_testRunId, m_stepInfo);
            }
        }

        private class DoAppendTask : DbTask
        {
            int? m_testRunId;
            LogMessage m_logMessage;
            string m_stepPath;
            LoggingEvent m_loggingEvent;
            public DoAppendTask(ILogEventSink appender, int? testRunId, string stepPath, LoggingEvent loggingEvent, LogMessage logMessage)
                : base(appender)
            {
                this.m_testRunId = testRunId;
                this.m_stepPath = stepPath;
                this.m_loggingEvent = loggingEvent;
                this.m_logMessage = logMessage;
            }

            public override void DoWork()
            {
                appender.DoAppend(m_testRunId, m_stepPath, m_loggingEvent, ref m_logMessage);
            }
        }
        
        #region IDataBaseAppender Members

        public event QueueSizeChangeDelegate OnQueueSizeChange;

        #endregion

        public void Dispose()
        {
            m_queue.Dispose();
        }
    }
}
