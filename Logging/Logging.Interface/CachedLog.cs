﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace yats.Logging.Interface
{
    // collects a test run log as the test ir executed. The steps and messages are collected into cache lists
    public class CachedLog : TestRunLog, ILogEventSink
    {
        public CachedLog(List<StepInfo> steps,
                List<LogMessage> messages,
                DateTime startTime,
                string name,
                string description,
                byte[] settings): base(steps, messages, startTime, name, description, settings)
        {
            this.steps = steps;
            this.messages = messages;
        }

        public CachedLog(TestRunLog info)
            : base(info)
        {
        }

        public CachedLog(
                DateTime startTime,
                string name,
                string description,
                byte[] settings)
            : base(new List<StepInfo>(), new List<LogMessage>(), startTime, name, description, settings)
        {

        }
        

        public CachedLog(TestRunLogInfo info)
            : base(info)
        {
        }

        public void StartTestRun(string name, string description, byte[] settings, int testRunId)
        {
        }

        public void EndTestRun(int testRunId, TestCase.Interface.ITestResult result)
        {
        }

        public void UpdateStep(int testRunId, StepInfo stepInfo)
        {
            lock (messages)
            {
                steps.Add(stepInfo);
            }
            if (TestStepAdded != null)
            {
                TestStepAdded(this, new LogStepAddedEventArgs(testRunId, stepInfo));
            }
        }
        
        public void DoAppend(int? testRunId, string stepPath, log4net.Core.LoggingEvent loggingEvent, ref LogMessage logMessage)
        {
            if (logMessage == null)
            {
                logMessage = new LogMessage(stepPath, loggingEvent);
            }

            logMessage.Timestamp = DateTime.Now;
            lock (messages)
            {
                messages.Add(logMessage);
                logMessage.SequenceNumber = messages.Count;
            }
            if (MessageAdded != null)
            {
                MessageAdded(this, new LogMessageAddedEventArgs(testRunId.Value, logMessage));
            }
        }

        public void WriteBinaryData(int testRunId, string stepPath, string logger, string level, string message, object value)
        {
            var logMessage = new LogMessage(stepPath, logger, level, message, value);
            logMessage.Timestamp = DateTime.Now;
            lock (messages)
            {
                messages.Add(logMessage);
                logMessage.SequenceNumber = messages.Count;
            }
            if (MessageAdded != null)
            {
                MessageAdded(this, new LogMessageAddedEventArgs(testRunId, logMessage));
            }
        }

        public event EventHandler<LogStepAddedEventArgs> TestStepAdded;
        public event EventHandler<LogMessageAddedEventArgs> MessageAdded;
        public event EventHandler<LogMessageAddedEventArgs> MessageChanged;
        public void UpdateMessage(LogMessage message)
        {
            if (MessageChanged != null)
            {
                MessageChanged(this, new LogMessageAddedEventArgs(-1, message));
            }
        }
    }
}
