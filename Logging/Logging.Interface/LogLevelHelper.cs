﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using log4net.Core;

namespace yats.Logging.Interface
{
    public static class LogLevelHelper
    {
        static List<Level> levels = new List<Level>( );
            
        static LogLevelHelper()
        {
            levels.Add( log4net.Core.Level.Debug );
            levels.Add( log4net.Core.Level.Info );
            levels.Add( log4net.Core.Level.Warn );
            levels.Add( log4net.Core.Level.Error );
            levels.Add( log4net.Core.Level.Fatal );
        }

        public static List<Level> GetAvailableLevels()
        {            
            return levels;
        }

        public static Level Parse(string name)
        {
            foreach(var lvl in levels)
            {
                if(lvl.DisplayName.ToLower( ) == name.ToLower( ))
                {
                    return lvl;
                }
            }
            return Level.Debug;
        }

        public static string ToString(Level level)
        {
            return level.DisplayName;
        }
    }
}
