﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using yats.TestCase.Interface;

namespace yats.Logging.Interface
{
    //TODO: return an TestRunLogInfo object from StartTestRun, pass it to all other methods instead of testRunId. Hide the testRunId from outside
    public interface ILogEventSink
    {
        /// <summary>
        /// To be called when test run starts. 
        /// </summary>
        /// <param name="description">test description</param>
        /// <param name="settings">serialized test settings to be saved together with the test run</param>
        /// <param name="testRunId">index, should be incremented to be able to distinguish between test runs</param>
        void StartTestRun(string name, string description, byte[] settings, int testRunId);
        void EndTestRun(int testRunId, ITestResult result);
        /// <summary>
        /// To be called when test case or composite test starts or has result
        /// </summary>
        /// <param name="testRunId"></param>
        /// <param name="step"></param>
        void UpdateStep(int testRunId, StepInfo stepInfo);
        /// <summary>
        /// Append log4net LoggingEvent entry. This method combines the log4net message format with the message's path in the Yats test run.
        /// </summary>
        /// <param name="testRunId">Test run ID</param>
        /// <param name="stepPath">step path (hierarchy + repetitions)</param>
        /// <param name="loggingEvent">log4net log entry</param>
        /// <param name="logMessage">Performance optimization - the method should use the object if not null. Otherwise it shoud return the created LogMessage so that it does not have to be serialized again</param>
        void DoAppend(int? testRunId, string stepPath, log4net.Core.LoggingEvent loggingEvent, ref LogMessage logMessage);
        void WriteBinaryData(int testRunId, string stepPath, string logger, string level, string message, object value);
    }
}
