﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Text;
using yats.Utilities;

namespace yats.Logging.Interface
{
    public struct PathSegment
    {
        /// <summary>
        /// Test step index in a composite step
        /// </summary>
        public int Index;

        /// <summary>
        /// Zero-based step repetition index
        /// </summary>
        public int Repetition;

        public static bool operator ==(PathSegment s1, PathSegment s2)
        {
            return s1.Index == s2.Index && s1.Repetition == s2.Repetition;
        }
        public static bool operator !=(PathSegment s1, PathSegment s2)
        {
            return !(s1 == s2);
        }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }
            
            if ((obj is PathSegment) == false)
            {
                return false;
            }
            return this.Index == ((PathSegment)obj).Index && this.Repetition == ((PathSegment)obj).Repetition;
        }

        public override int GetHashCode()
        {
            return 8517721 ^ Index ^ Repetition;
        }

        public override string ToString()
        {
            return Index.ToString() + StepPath.RepetitionSeparators[0] + Repetition;
        }
    }

    public class StepPath
    {
        public int Length { get { return Segments.Length; } }
        string path; // stored as string, only parsed on Segments property access
        PathSegment [] segments = null;

        public PathSegment [] Segments
        { 
            get {
                if(segments != null)
                {
                    return segments;
                }
                
                var strings = path.Split( LevelSeparators, StringSplitOptions.RemoveEmptyEntries);
                //path = null; // release memory

                segments = new PathSegment[strings.Length];

                int i = 0;
                foreach(var s in strings)
                {
                    var split = s.Split(RepetitionSeparators);
                    if (split.Length != 2)
                    {
                        CrashLog.Write("Path parse error: " + path);
                    }
                    else
                    {
                        segments[i].Index = int.Parse(split[0]);
                        segments[i].Repetition = string.IsNullOrEmpty(split[1])? 0:int.Parse(split[1]);
                        i++;
                    }
                }               
                
                return segments; 
            } 
        }       
        
        public static readonly char [] LevelSeparators = new char[]{'|'};
        public static readonly char [] RepetitionSeparators = new char [] {'.'};
        static readonly string LevelSeparator = "|";

        public StepPath(string path)
        {
            this.path = path;
        }
                
        public void Reset()
        {
            index = 0;
        }

        int index = 0;
        
        public bool Next(ref PathSegment pathSegment)
        {
            if (index >= Segments.Length)
            {
                return false;
            }
            pathSegment = Segments[index];
            index++;
            return index == Segments.Length;
        }

        public bool Finished { get { return index == Segments.Length; } }

        public override string ToString()
        {
            if (path != null)
            {
                return path;
            }

            StringBuilder sb = new StringBuilder(segments.Length * 6);
            char repSeparator = StepPath.RepetitionSeparators[0];
            char levelSeparator = LevelSeparator[0];

            for (int i = 0; i < segments.Length; i++)
            {
                sb.Append(segments[i].Index);
                sb.Append(repSeparator);
                sb.Append(segments[i].Repetition);
                if (i != segments.Length - 1){
                    sb.Append(levelSeparator);
                }
            }
            path = sb.ToString();
            return path;
            //return String.Join(LevelSeparator, Segments.Select(x => x.ToString()).ToArray());
        }
    }
}
