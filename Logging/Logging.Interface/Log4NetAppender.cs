﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using log4net.Appender;

namespace yats.Logging.Interface
{
    class Log4NetAppender : IAppender
    {
        #region IAppender Members

        public void Close()
        {
        }

        public void DoAppend(log4net.Core.LoggingEvent loggingEvent)
        {
            try
            {
                YatsLogAppenderCollection.Instance.DoAppend( loggingEvent );
            }
            catch(Exception e)
            {
                System.Console.Write( e.Message );
                System.Console.Write( e.StackTrace );
            }
        }

        protected string name;
        public string Name { get { return name; } set { name = value; } }
		

        #endregion
    }
}
