﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace yats.Logging.Interface
{
    public class TestRunLogInfo
    {
        public TestRunLogInfo()
        {

        }

        public TestRunLogInfo(
                DateTime startTime,
                string name,
                string description
                ):this()
        {
            this.startTime = startTime;
            this.name = name;
            this.description = description;
        }

        public TestRunLogInfo(
                DateTime startTime,
                string name,
                string description,
                int size
                ) : this(startTime, name, description)
        {
            this.size = size;
        }

        protected DateTime startTime;
        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }

        protected string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        protected string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        
        protected int size;
        public int Size
        {
            get { return size; }
            set { size = value; }
        }
    }
}
