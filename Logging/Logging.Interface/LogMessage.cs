﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text;
using yats.Utilities;

namespace yats.Logging.Interface
{
    public class LogMessage
    {
        public int SequenceNumber;
        public string Path;
        public DateTime Timestamp;
        public string Logger;
        public string Level;
        public string Message;
        public byte[] BinaryData;
        public string BinaryDataType;
        public string Note;

        public const string MESSAGE_BUG = "#bug";

        public LogMessage()
        {
        }

        public LogMessage(string stepPath, log4net.Core.LoggingEvent loggingEvent) : this()
        {
            if(loggingEvent.ExceptionObject != null)// attach stack trace for exceptions
            {
                _Init(stepPath, loggingEvent.LoggerName, loggingEvent.Level.DisplayName, loggingEvent.RenderedMessage, loggingEvent.ExceptionObject, null);
                return;
            }
            
            if(loggingEvent.MessageObject is log4net.Util.SystemStringFormat || loggingEvent.MessageObject is string) //simple message
            {
                _Init(stepPath, loggingEvent.LoggerName, loggingEvent.Level.DisplayName, loggingEvent.RenderedMessage, null, null);
                return;
            }

            if(loggingEvent.MessageObject != null)// try to serialize other objects
            {
                _Init(stepPath, loggingEvent.LoggerName, loggingEvent.Level.DisplayName, null, loggingEvent.MessageObject, null);
                return;
            }

            _Init(stepPath, loggingEvent.LoggerName, loggingEvent.Level.DisplayName, loggingEvent.RenderedMessage, null, null);
        }

        public LogMessage(string stepPath, string logger, string level, string message, object value) : this()
        {
            _Init(stepPath, logger, level, message, value, null);
        }
        
        private void _Init(string stepPath, string logger, string level, string message, object value, string note)
        {
            this.Path = stepPath;
            this.Logger = logger;
            this.Level = level;
            this.Message = message;
            this.Timestamp = DateTime.Now;
            this.Note = note;

            Exception ExceptionObject = value as Exception;
            if (ExceptionObject != null)// attach stack trace for exceptions
            {
                this.BinaryData = Encoding.UTF8.GetBytes(ExceptionObject.ToString());
                this.BinaryDataType = "Exception";
                if (Message == null)
                {
                    Message = ExceptionObject.Message;
                }
                return;
            }
            
            if (value is byte[])// attach binary data for byte[]
            {
                byte[] bytes = value as byte[];
                this.BinaryData = bytes;
                this.BinaryDataType = "byte[]";
                this.Message = string.Format("Byte [{0}] {1}", bytes.Length,  bytes.ContainsAsciiControlCharacters() ? bytes.ToHexString() : Encoding.ASCII.GetString(bytes) );
                
                return;
            }
            
            if (value != null)// try to serialize other objects
            {
                Serialize(value, value.GetType().AssemblyQualifiedName);
                return;
            }
        }

        private void Serialize(object o, string typeName)
        {
            try
            {
                /*
                XmlSerializer serializer = yats.Utilities.AssemblyHelper.GetSerializer(o.GetType());
                var tw = new System.IO.MemoryStream();
                serializer.Serialize(tw, o);
                tw.Close();
                */
                byte[] valueAsBytes;
                if (NumericTypeHelper.TryConvertToByteArray(o, out valueAsBytes))
                {
                    this.Message = o.ToString();
                    this.BinaryData = valueAsBytes;
                    this.BinaryDataType = o.GetType().AssemblyQualifiedName;
                } 
                else if (o is string)
                {
                    this.BinaryData = Encoding.UTF8.GetBytes(o as string);
                    //this.BinaryData = tw.ToArray();
                    this.BinaryDataType = typeof(string).AssemblyQualifiedName;
                }
                else if (o is Exception)
                {
                    this.BinaryData = Encoding.ASCII.GetBytes(o.ToString());
                    //this.BinaryData = tw.ToArray();
                    this.BinaryDataType = "Exception";
                    this.Message = (o as Exception).Message;
                }
                else if (o.GetType().GetMethod("ToString", Type.EmptyTypes).DeclaringType == o.GetType())
                {
                    this.BinaryData = Encoding.UTF8.GetBytes(o.ToString());
                    this.BinaryDataType = typeName;
                }
                else
                {
                    var jsonString = JsonConvert.SerializeObject(o, Formatting.Indented);
                    this.Message = jsonString;

                    //this.BinaryData = Encoding.UTF8.GetBytes(jsonString);
                    //this.BinaryData = tw.ToArray();
                    //this.BinaryDataType = typeof(string).AssemblyQualifiedName;

                    //this.BinaryData = Encoding.ASCII.GetBytes(typeName + Environment.NewLine + jsonString );
                    //this.BinaryDataType = typeName;
                }
            }
            catch
            {
            }
        }

        public override string ToString()
        {
            return string.Format( "{0} {1}", this.Path, this.Message );
        }

        /// <summary>
        /// Check if path A is part of path B
        /// </summary>
        /// <param name="pathA"></param>
        /// <param name="pathB"></param>
        /// <returns></returns>
        public static bool PathMatches(string pathA, string pathB)
        {
            if(pathB.StartsWith( pathA ) == false)
            {
                return false;
            }
            if(pathB.Length == pathA.Length)
            {
                return true;
            }
            if(StepPath.RepetitionSeparators.Contains( pathA[pathA.Length - 1] )) // for 'all repetition' paths like "0.0|1."
            {
                return true;
            }
            char nextChar = pathB[pathA.Length];
            return StepPath.LevelSeparators.Contains( nextChar );
        }
    }
}
