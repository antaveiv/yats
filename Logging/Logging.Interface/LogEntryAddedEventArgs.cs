﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using yats.TestCase.Interface;

namespace yats.Logging.Interface
    {
    public class LogEntryAddedEventArgs : EventArgs
        {
        public int? TestRunId;
        public string StepPath;
        public log4net.Core.LoggingEvent LoggingEvent;

        public LogEntryAddedEventArgs (int? testRunId, string stepPath, log4net.Core.LoggingEvent loggingEvent)
            {
            this.TestRunId = testRunId;
            this.StepPath = stepPath;
            this.LoggingEvent = loggingEvent;
            }
        }
    }
