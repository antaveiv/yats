﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Xml.Serialization;

namespace yats.Logging.Interface
{    
    public class StepInfo:EventArgs
    {
        public string testCaseUniqueId;
    
        public string TreePath
        {
            get
            {
                return Path.ToString();
            }
            set
            {
                Path = new StepPath(value);
            }
        }
        [XmlIgnore]
        public StepPath Path;

        public string RawResult;
        public string EvaluatedResult;
        public string RepetitionResult;

        public StepInfoTypeEnum RowType;
        public DateTime Timestamp;
        public string Name;
        

        public bool IsGroup
        {
            get { return testCaseUniqueId  == null; }
        }

        public bool IsTestCase
        {
            get { return !IsGroup; }
        }

        public StepInfo()
        {
        }

        // it's a mess to have constructors with very similar parameter sets - converted to static methods 

        public static StepInfo GetStepStart(string name, string path)
        {
            var result = new StepInfo();
            result.Name = name;
            result.Path = new StepPath(path);
            result.TreePath = path;
            result.Timestamp = DateTime.Now;
            result.RowType = StepInfoTypeEnum.STEP_START;
            return result;
        }

        public static StepInfo GetStepStart(string name, string path, string testCaseUniqueId)
        {
            var result = new StepInfo();
            result.Name = name;
            result.Path = new StepPath(path);
            result.TreePath = path;
            result.Timestamp = DateTime.Now;
            result.RowType = StepInfoTypeEnum.STEP_START;
            result.testCaseUniqueId = testCaseUniqueId;
            return result;
        }

        public static StepInfo GetRepetitionResult(string name, string path, string repetitionResult)
        {
            var result = new StepInfo();
            result.Name = name;
            result.Path = new StepPath(path);
            result.TreePath = path;
            result.Timestamp = DateTime.Now;
            result.RowType = StepInfoTypeEnum.REPETITION_RESULT;
            result.RepetitionResult = repetitionResult;
            return result;
        }

        public static StepInfo GetStepResult(string name, string path, string rawResult, string evalResult)
        {
            var result = new StepInfo();
            result.Name = name;
            result.Path = new StepPath(path);
            result.TreePath = path;
            result.Timestamp = DateTime.Now;
            result.RowType = StepInfoTypeEnum.STEP_RESULT;
            result.RawResult = rawResult;
            result.EvaluatedResult = evalResult;
            return result;
        }
        
        public override string ToString()
        {
            return string.Format("{0} {1} {2}", TreePath, Name, RowType);
        }
    }
}
