﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Linq;
using System.Windows.Forms;
using yats.Logging.Interface;
using yats.Logging.Util;
using yats.Logging.SQLite;
using Gui.Winforms.PublicAPI;

namespace yats.LoggingSettingsEditor
{
    public partial class LogSettingsPanel : SettingsTabPanel
    {
        public LogSettingsPanel()
        {
            InitializeComponent();
        }

        private void logStorageChangeBtn_Click(object sender, EventArgs e)
        {
            folderBrowserDialog.SelectedPath = logStorageTb.Text;
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                logStorageTb.Text = folderBrowserDialog.SelectedPath;
            }
        }

        public override string GetText()
        {
            return "Logging";
        }

        public override void LoadSettings()
        {
            bool txtEnabled = yats.Logging.File.Properties.Settings.Default.FileLogEnabled;
            string logDirectory = yats.Logging.File.Properties.Settings.Default.FileLogPath;
            bool dbEnabled = yats.Logging.SQLite.Properties.Settings.Default.SQLiteLoggingEnabled;
            bool openLogOnRun = yats.Logging.Interface.Properties.Settings.Default.OpenLogOnRun;
         
#if FREE_VERSION
            this.sqliteGroupBox.Enabled = false;
            dbEnabled = false;
#endif
            this.txtEnabledCB.Checked = txtEnabled;
            this.logStorageTb.Text = logDirectory;
            this.dbEnabledCB.Checked = dbEnabled;
            this.cbOpenLogOnTestRun.Checked = openLogOnRun;
            this.cbLogTestParameters.Checked = yats.Logging.Interface.Properties.Settings.Default.LogTestParameters;
            this.cbLogTestResults.Checked = yats.Logging.Interface.Properties.Settings.Default.LogTestResults;
            this.comboFileLogLevel.DataSource = LogLevelHelper.GetAvailableLevels();
            
            this.comboFileLogLevel.SelectedItem = LogLevelHelper.Parse(yats.Logging.File.Properties.Settings.Default.FileLogLevel);
#if DEBUG
            //TODO: wrong display when async is used
            this.sqliteLibraryInfoLabel.Visible = this.sqliteLibraryInfoValueLabel.Visible = true;
            this.sqliteLibraryInfoValueLabel.Text = (AppenderFactory.StorageInstance != null) ? AppenderFactory.StorageInstance.GetType().Namespace.Split('.').Last() : "N/A";
#endif
            this.cbAsyncDbLogging.Checked = yats.Logging.SQLite.Properties.Settings.Default.AsynchronousLogging;
        }

        public override void SaveSettings()
        {
            yats.Logging.File.Properties.Settings.Default.FileLogEnabled = txtEnabledCB.Checked;
            yats.Logging.File.Properties.Settings.Default.FileLogPath = logStorageTb.Text;
            yats.Logging.File.Properties.Settings.Default.FileLogLevel = comboFileLogLevel.SelectedItem.ToString();
            yats.Logging.File.Properties.Settings.Default.Save();

            yats.Logging.SQLite.Properties.Settings.Default.SQLiteLoggingEnabled = dbEnabledCB.Checked;
            yats.Logging.SQLite.Properties.Settings.Default.AsynchronousLogging = this.cbAsyncDbLogging.Checked;
            yats.Logging.SQLite.Properties.Settings.Default.Save();

            yats.Logging.Interface.Properties.Settings.Default.OpenLogOnRun = cbOpenLogOnTestRun.Checked;
            yats.Logging.Interface.Properties.Settings.Default.LogTestParameters = cbLogTestParameters.Checked;
            yats.Logging.Interface.Properties.Settings.Default.LogTestResults = cbLogTestResults.Checked;
            yats.Logging.Interface.Properties.Settings.Default.Save();
        }

        public override int GetSortIndex()
        {
            return 200;
        }
    }
}
