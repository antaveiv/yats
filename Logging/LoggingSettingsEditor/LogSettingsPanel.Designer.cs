﻿namespace yats.LoggingSettingsEditor
{
    partial class LogSettingsPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogSettingsPanel));
            this.fileGroupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboFileLogLevel = new System.Windows.Forms.ComboBox();
            this.logStorageChangeBtn = new System.Windows.Forms.Button();
            this.logStorageTb = new yats.Gui.Winforms.Components.Util.TextBoxAdv();
            this.TxtLocationLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtEnabledCB = new System.Windows.Forms.CheckBox();
            this.sqliteGroupBox = new System.Windows.Forms.GroupBox();
            this.sqliteLibraryInfoValueLabel = new System.Windows.Forms.Label();
            this.sqliteLibraryInfoLabel = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.dbEnabledCB = new System.Windows.Forms.CheckBox();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbLogTestResults = new System.Windows.Forms.CheckBox();
            this.cbLogTestParameters = new System.Windows.Forms.CheckBox();
            this.cbOpenLogOnTestRun = new System.Windows.Forms.CheckBox();
            this.cbAsyncDbLogging = new System.Windows.Forms.CheckBox();
            this.fileGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.sqliteGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileGroupBox
            // 
            this.fileGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.fileGroupBox.Controls.Add(this.label1);
            this.fileGroupBox.Controls.Add(this.comboFileLogLevel);
            this.fileGroupBox.Controls.Add(this.logStorageChangeBtn);
            this.fileGroupBox.Controls.Add(this.logStorageTb);
            this.fileGroupBox.Controls.Add(this.TxtLocationLabel);
            this.fileGroupBox.Controls.Add(this.pictureBox1);
            this.fileGroupBox.Controls.Add(this.txtEnabledCB);
            this.fileGroupBox.Location = new System.Drawing.Point(3, 3);
            this.fileGroupBox.Name = "fileGroupBox";
            this.fileGroupBox.Size = new System.Drawing.Size(567, 112);
            this.fileGroupBox.TabIndex = 0;
            this.fileGroupBox.TabStop = false;
            this.fileGroupBox.Text = "Text file logging";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Level";
            // 
            // comboFileLogLevel
            // 
            this.comboFileLogLevel.FormattingEnabled = true;
            this.comboFileLogLevel.Location = new System.Drawing.Point(169, 61);
            this.comboFileLogLevel.Name = "comboFileLogLevel";
            this.comboFileLogLevel.Size = new System.Drawing.Size(121, 21);
            this.comboFileLogLevel.TabIndex = 5;
            // 
            // logStorageChangeBtn
            // 
            this.logStorageChangeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.logStorageChangeBtn.Location = new System.Drawing.Point(484, 34);
            this.logStorageChangeBtn.Name = "logStorageChangeBtn";
            this.logStorageChangeBtn.Size = new System.Drawing.Size(75, 23);
            this.logStorageChangeBtn.TabIndex = 4;
            this.logStorageChangeBtn.Text = "Browse...";
            this.logStorageChangeBtn.UseVisualStyleBackColor = true;
            this.logStorageChangeBtn.Click += new System.EventHandler(this.logStorageChangeBtn_Click);
            // 
            // logStorageTb
            // 
            this.logStorageTb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.logStorageTb.Location = new System.Drawing.Point(169, 36);
            this.logStorageTb.Name = "logStorageTb";
            this.logStorageTb.Size = new System.Drawing.Size(309, 20);
            this.logStorageTb.TabIndex = 3;
            // 
            // TxtLocationLabel
            // 
            this.TxtLocationLabel.AutoSize = true;
            this.TxtLocationLabel.Location = new System.Drawing.Point(60, 39);
            this.TxtLocationLabel.Name = "TxtLocationLabel";
            this.TxtLocationLabel.Size = new System.Drawing.Size(103, 13);
            this.TxtLocationLabel.TabIndex = 2;
            this.TxtLocationLabel.Text = "Log storage location";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(6, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 53);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // txtEnabledCB
            // 
            this.txtEnabledCB.AutoSize = true;
            this.txtEnabledCB.Location = new System.Drawing.Point(63, 19);
            this.txtEnabledCB.Name = "txtEnabledCB";
            this.txtEnabledCB.Size = new System.Drawing.Size(65, 17);
            this.txtEnabledCB.TabIndex = 0;
            this.txtEnabledCB.Text = "Enabled";
            this.txtEnabledCB.UseVisualStyleBackColor = true;
            // 
            // sqliteGroupBox
            // 
            this.sqliteGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.sqliteGroupBox.Controls.Add(this.cbAsyncDbLogging);
            this.sqliteGroupBox.Controls.Add(this.sqliteLibraryInfoValueLabel);
            this.sqliteGroupBox.Controls.Add(this.sqliteLibraryInfoLabel);
            this.sqliteGroupBox.Controls.Add(this.pictureBox2);
            this.sqliteGroupBox.Controls.Add(this.dbEnabledCB);
            this.sqliteGroupBox.Location = new System.Drawing.Point(3, 121);
            this.sqliteGroupBox.Name = "sqliteGroupBox";
            this.sqliteGroupBox.Size = new System.Drawing.Size(567, 83);
            this.sqliteGroupBox.TabIndex = 1;
            this.sqliteGroupBox.TabStop = false;
            this.sqliteGroupBox.Text = "Database logging";
            // 
            // sqliteLibraryInfoValueLabel
            // 
            this.sqliteLibraryInfoValueLabel.AutoSize = true;
            this.sqliteLibraryInfoValueLabel.Location = new System.Drawing.Point(224, 61);
            this.sqliteLibraryInfoValueLabel.Name = "sqliteLibraryInfoValueLabel";
            this.sqliteLibraryInfoValueLabel.Size = new System.Drawing.Size(27, 13);
            this.sqliteLibraryInfoValueLabel.TabIndex = 3;
            this.sqliteLibraryInfoValueLabel.Text = "N/A";
            this.sqliteLibraryInfoValueLabel.Visible = false;
            // 
            // sqliteLibraryInfoLabel
            // 
            this.sqliteLibraryInfoLabel.AutoSize = true;
            this.sqliteLibraryInfoLabel.Location = new System.Drawing.Point(63, 61);
            this.sqliteLibraryInfoLabel.Name = "sqliteLibraryInfoLabel";
            this.sqliteLibraryInfoLabel.Size = new System.Drawing.Size(146, 13);
            this.sqliteLibraryInfoLabel.TabIndex = 2;
            this.sqliteLibraryInfoLabel.Text = "SQLite library currently in use:";
            this.sqliteLibraryInfoLabel.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(6, 19);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(51, 55);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // dbEnabledCB
            // 
            this.dbEnabledCB.AutoSize = true;
            this.dbEnabledCB.Location = new System.Drawing.Point(63, 19);
            this.dbEnabledCB.Name = "dbEnabledCB";
            this.dbEnabledCB.Size = new System.Drawing.Size(65, 17);
            this.dbEnabledCB.TabIndex = 0;
            this.dbEnabledCB.Text = "Enabled";
            this.dbEnabledCB.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cbLogTestResults);
            this.groupBox1.Controls.Add(this.cbLogTestParameters);
            this.groupBox1.Controls.Add(this.cbOpenLogOnTestRun);
            this.groupBox1.Location = new System.Drawing.Point(3, 210);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(567, 90);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Misc.";
            // 
            // cbLogTestResults
            // 
            this.cbLogTestResults.AutoSize = true;
            this.cbLogTestResults.Location = new System.Drawing.Point(6, 65);
            this.cbLogTestResults.Name = "cbLogTestResults";
            this.cbLogTestResults.Size = new System.Drawing.Size(152, 17);
            this.cbLogTestResults.TabIndex = 2;
            this.cbLogTestResults.Text = "Log test case result values";
            this.cbLogTestResults.UseVisualStyleBackColor = true;
            // 
            // cbLogTestParameters
            // 
            this.cbLogTestParameters.AutoSize = true;
            this.cbLogTestParameters.Location = new System.Drawing.Point(6, 42);
            this.cbLogTestParameters.Name = "cbLogTestParameters";
            this.cbLogTestParameters.Size = new System.Drawing.Size(174, 17);
            this.cbLogTestParameters.TabIndex = 1;
            this.cbLogTestParameters.Text = "Log test case parameter values";
            this.cbLogTestParameters.UseVisualStyleBackColor = true;
            // 
            // cbOpenLogOnTestRun
            // 
            this.cbOpenLogOnTestRun.AutoSize = true;
            this.cbOpenLogOnTestRun.Location = new System.Drawing.Point(6, 19);
            this.cbOpenLogOnTestRun.Name = "cbOpenLogOnTestRun";
            this.cbOpenLogOnTestRun.Size = new System.Drawing.Size(172, 17);
            this.cbOpenLogOnTestRun.TabIndex = 0;
            this.cbOpenLogOnTestRun.Text = "Open log when a test is started";
            this.cbOpenLogOnTestRun.UseVisualStyleBackColor = true;
            // 
            // cbAsyncDbLogging
            // 
            this.cbAsyncDbLogging.AutoSize = true;
            this.cbAsyncDbLogging.Location = new System.Drawing.Point(63, 42);
            this.cbAsyncDbLogging.Name = "cbAsyncDbLogging";
            this.cbAsyncDbLogging.Size = new System.Drawing.Size(413, 17);
            this.cbAsyncDbLogging.TabIndex = 4;
            this.cbAsyncDbLogging.Text = "Write to database asynchronously. NOTE: application may take more time to close";
            this.cbAsyncDbLogging.UseVisualStyleBackColor = true;
            // 
            // LogSettingsPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.sqliteGroupBox);
            this.Controls.Add(this.fileGroupBox);
            this.Name = "LogSettingsPanel";
            this.Size = new System.Drawing.Size(573, 306);
            this.fileGroupBox.ResumeLayout(false);
            this.fileGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.sqliteGroupBox.ResumeLayout(false);
            this.sqliteGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox fileGroupBox;
        private System.Windows.Forms.CheckBox txtEnabledCB;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button logStorageChangeBtn;
        private yats.Gui.Winforms.Components.Util.TextBoxAdv logStorageTb;
        private System.Windows.Forms.Label TxtLocationLabel;
        private System.Windows.Forms.GroupBox sqliteGroupBox;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.CheckBox dbEnabledCB;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbOpenLogOnTestRun;
        private System.Windows.Forms.CheckBox cbLogTestResults;
        private System.Windows.Forms.CheckBox cbLogTestParameters;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboFileLogLevel;
        private System.Windows.Forms.Label sqliteLibraryInfoLabel;
        private System.Windows.Forms.Label sqliteLibraryInfoValueLabel;
        private System.Windows.Forms.CheckBox cbAsyncDbLogging;
    }
}
