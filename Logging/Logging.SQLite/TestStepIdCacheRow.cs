﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


namespace yats.Logging.SQLite
{
    class TestStepIdCacheRow
    {
        public long testRunId;
        public string stepPath;

        public TestStepIdCacheRow(long testRunId, string stepPath)
        {
            this.testRunId = testRunId;
            this.stepPath = stepPath;
        }

        public override string ToString()
        {
            return string.Format("TR {0} Path {1}", testRunId, stepPath);
        }

        public override int GetHashCode()
        {
            return stepPath.GetHashCode() ^ testRunId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }
            TestStepIdCacheRow other = obj as TestStepIdCacheRow;
            if (other == null)
            {
                return false;
            }
            return testRunId == other.testRunId && stepPath == other.stepPath;
        }
    }
}
