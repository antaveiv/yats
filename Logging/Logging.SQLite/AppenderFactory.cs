﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using yats.Logging.Interface;
using System.Security.Permissions;
using yats.Utilities;

namespace yats.Logging.SQLite
{
    
    public class AppenderFactory
    {
        private static bool sIsInitialized = false;

        public static bool IsInitializedSuccessfully
        {
            get
            {
                return sIsInitialized && sSinkInstance != null;
            }
        }

        public static ILogEventSink SinkInstance
        {
            [DebuggerStepThrough]
            get
            {
                Debug.Assert(sIsInitialized);
                return sSinkInstance;
            }
        }

        public static ILogStorage StorageInstance
        {
            [DebuggerStepThrough]
            get
            {
                Debug.Assert(sIsInitialized);
                return sInstance;
            }
        }

        private static IDataBaseAppender sInstance = null;
        private static ILogEventSink sSinkInstance = null; 

        private static bool sIsAsynchronous = false;
        public static bool Asynchronous
        {
            get { return sIsAsynchronous; }
            set { sIsAsynchronous = value; }
        }

        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        public static void Init(bool reset, string sqliteFilePath, string sqliteFileName, string sqliteEmptyDbPath)
        {
            try
            {
                if(sIsInitialized)
                {
                    throw new Exception( "Already initialized" );
                }
                                
#if FREE_VERSION
                initialized = true;
                return;
#endif

#if !__MonoCS__
                // If the native library loading fails, try the Community.CSharpSqlite library (slow, buggy but platform-independent)
                if (sInstance == null)
                {
                    // First try initializing the Native SQLite connector. May fail if e.g. 32-bit application is run on a 64-bit system
                    try
                    {   
                        sSinkInstance = sInstance = yats.Logging.SQLite.Native.DatabaseConnection.Init(reset, sqliteFilePath, sqliteFileName, sqliteEmptyDbPath);
                    }
                    catch (Exception ex)
                    {
                        CrashLog.Write(ex, "Failed to init native SQLite");
                    }
                }
#endif
#if __MonoCS__
                if (instance == null)
                {
                    try
                    {
                        sinkInstance = instance = yats.Logging.SQLite.Mono.DatabaseConnection.Init(reset, sqliteFilePath, sqliteFileName, sqliteEmptyDbPath);
                    }
                    catch
                    {
                    }
                }
#endif
            }
            catch(Exception ex)
            {
                RaiseOnError( ex.Message );
            }
            sIsInitialized = true;
            if (OnInitialized != null)
            {
                OnInitialized(sInstance, null);
            }
        }

        public static void InitDisabled()
        {
            if(sIsInitialized)
            {
                RaiseOnError( "Already initialized" );
                throw new Exception( "Already initialized" );
            }
            sIsInitialized = true;
            sSinkInstance  = sInstance = null;
            if (OnInitialized != null)
            {
                OnInitialized(sInstance, null);
            }
        }

        protected static void RaiseOnError(string message)
        {
            if(OnError != null)
            {
                OnError (typeof (AppenderFactory), new ConnectionErrorEventArgs (message));
            }
        }

        public static event EventHandler<ConnectionErrorEventArgs> OnError;
        public static event EventHandler<EventArgs> OnInitialized;

        public static void CloseInstance()
        {
            if (sSinkInstance != null && sSinkInstance != sInstance && sSinkInstance is IDisposable)
            {
                (sSinkInstance as IDisposable).Dispose();
            }
            if (sInstance != null)
            {
                sInstance.Dispose();
                sInstance = null;
            }            
        }
    }
}
