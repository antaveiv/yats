﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using log4net.Core;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using yats.Logging.Interface;
using yats.TestCase.Interface;

namespace yats.Logging.SQLite
{
    //This class implements all the logic behind SQLite logging. The actual queries are implemented in library-specific SQLiteAppender classes
    abstract class Common
    {
        #region Fields
        protected static Dictionary<TestStepIdCacheRow, long> StepIdMap = new Dictionary<TestStepIdCacheRow, long>();
        protected ReaderWriterLock lck = new ReaderWriterLock();
        protected const int LOCK_TIMEOUT = 10000;

        /// <summary>
        /// Cache of test case unique IDs
        /// </summary>
        protected static Dictionary<string, long> TestCaseIdMap = new Dictionary<string, long>();
        #endregion

        #region Helpers
        protected long ToLong(object value)
        {
            long v;
            if (value is Int32)
            {
                v = (long)(Int32)value; // After switching to Community.CsharpSqlite.SQLiteClient, returns an Int32 object. It can not be casted to long
            }
            else // should be long already
            {
                v = (long)value;
            }
            return v;
        }

        protected string ReadString(DbDataReader reader, int index)
        {
            if (reader.IsDBNull(index) == false)
            {
                return reader.GetString(index);
            }
            return null;
        }

        protected long? ReadLong(DbDataReader reader, int index)
        {
            if (reader.IsDBNull(index) == false)
            {
                return reader.GetInt64(index);
            }
            return null;
        }

        //http://stackoverflow.com/questions/625029/how-do-i-store-and-retrieve-a-blob-from-sqlite
        protected byte[] ReadBytes(DbDataReader reader, int index)
        {
            return reader.GetValue(index) as byte[];
        }
        #endregion

        #region Abstract methods to be implemented by an actual SQLite implementation (Native or non-native etc).
        protected abstract void ExecuteNonQuery(string query, params KeyValuePair<string, object>[] parameters);
        protected abstract object ExecuteScalar(string query, params KeyValuePair<string, object>[] parameters);
        //not at all sure what it does, but such semantics was used in the code. TODO: find out
        protected abstract object ExecuteNonQueryThenScalar(string query, params KeyValuePair<string, object>[] parameters);
        protected abstract DbDataReader ExecuteReader(string query, params KeyValuePair<string, object>[] parameters);
        protected abstract bool HasConnection();
        #endregion

        public event EventHandler<TestRunLogInfoEventArgs> LogAdded;
        public event EventHandler<TestRunLogInfoEventArgs> LogRemoved;
        public event EventHandler<TestRunLogInfoEventArgs> LogChanged;

        public void StartTestRun(string name, string description, byte[] settings, int testRunId)
        {
            if (!HasConnection())
            {
                return;
            }

            object value = ExecuteScalar("INSERT INTO TestRuns (Name, Description, StartTime, Settings )" +
                " VALUES (@Name, @Description, @StartTime, @Settings); select last_insert_rowid();",
                new KeyValuePair<string, object>("@Name", name),
                new KeyValuePair<string, object>("@Description", description),
                new KeyValuePair<string, object>("@StartTime", DateTime.Now),
                new KeyValuePair<string, object>("@Settings", settings));
            
            if (value == null)
            {
                return;
            }
            DatabaseTestRunIdMap.Instance.AddID(testRunId, ToLong(value));
        }

        public void EndTestRun(int testRunId, ITestResult result)
        {
            if (!HasConnection())
            {
                return;
            }

            lck.AcquireWriterLock(LOCK_TIMEOUT);
            try
            {
                if (LogAdded != null)
                {
                    long testRunDbID;
                    if (DatabaseTestRunIdMap.Instance.TryGetDbID(testRunId, out testRunDbID))
                    {
                        LogAdded(this, new TestRunLogInfoEventArgs() { TestRunLogInfo = GetStoredLog(testRunDbID) });
                    }
                }
                foreach (var item in StepIdMap.Where(x => x.Key.testRunId == testRunId).ToList())
                {
                    StepIdMap.Remove(item.Key);
                }
            }
            finally
            {
                lck.ReleaseWriterLock();
            }
        }
        
        public void UpdateStep(int testRunId, StepInfo stepInfo)
        {
            if (!HasConnection())
            {
                return;
            }

            try
            {
                long testRunDbID;
                if (DatabaseTestRunIdMap.Instance.TryGetDbID(testRunId, out testRunDbID) == false)
                {
                    return;
                }

                long? testCaseId = null;
                if (stepInfo.testCaseUniqueId != null)
                {
                    testCaseId = GetTestCaseID(stepInfo.testCaseUniqueId);
                }
                
#if DEBUG
                if (stepInfo.Timestamp == default(DateTime))
                {
                    // expecting the Timestamp to be initialized
                    yats.Utilities.CrashLog.Write("stepInfo.Timestamp is not initialized");
                }
#endif

                object value = ExecuteScalar("INSERT INTO TestSteps (TestRunId, TestCaseID, Name, TreePath, RowType, timestamp, RawResult, EvaluatedResult, RepetitionResult) " +
                    " VALUES (@TestRunId, @TestCaseID, @Name, @TreePath, @RowType, @timestamp, @RawResult, @EvaluatedResult, @RepetitionResult); select last_insert_rowid();",
                    new KeyValuePair<string, object>("@TestRunId", testRunDbID),
                    new KeyValuePair<string, object>("@TestCaseID", testCaseId),
                    new KeyValuePair<string, object>("@Name", stepInfo.Name),
                    new KeyValuePair<string, object>("@TreePath", stepInfo.TreePath),
                    new KeyValuePair<string, object>("@RowType", stepInfo.RowType.ToString()),
                    new KeyValuePair<string, object>("@timestamp", stepInfo.Timestamp),
                    new KeyValuePair<string, object>("@RawResult", stepInfo.RawResult),
                    new KeyValuePair<string, object>("@EvaluatedResult", stepInfo.EvaluatedResult),
                    new KeyValuePair<string, object>("@RepetitionResult", stepInfo.RepetitionResult));

                if (value == null || stepInfo.IsGroup)
                {
                    return;
                }
                long stepId = ToLong(value);
                switch (stepInfo.RowType)
                {
                    case StepInfoTypeEnum.STEP_START:
                        lck.AcquireWriterLock(LOCK_TIMEOUT);
                        try
                        {
                            StepIdMap[new TestStepIdCacheRow(testRunId, stepInfo.TreePath)] = stepId;
                        }
                        finally
                        {
                            lck.ReleaseWriterLock();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }

        public void WriteBinaryData(int testRunId, string stepPath, string logger, string level, string message, object value)
        {
            if (!HasConnection())
            {
                return;
            }

            try
            {
                if (string.IsNullOrEmpty(stepPath))
                {
                    return;
                }
                long testCaseRunId;
                lck.AcquireReaderLock(LOCK_TIMEOUT);
                try
                {
                    if (StepIdMap.TryGetValue(new TestStepIdCacheRow(testRunId, stepPath), out testCaseRunId) == false)
                    {
                        return;
                    }
                }
                finally
                {
                    lck.ReleaseReaderLock();
                }
                LogMessage msg = new LogMessage(stepPath, logger, level, message, value);

                AddLogEntry(testCaseRunId, msg);
            }
            catch
            {
            }
        }

        public void DoAppend(int? testRunId, string stepPath, LoggingEvent loggingEvent, ref LogMessage logMessage)
        {
            if (!HasConnection())
            {
                return;
            }

            try
            {
                if (testRunId == null || string.IsNullOrEmpty(stepPath))
                {
                    return;
                }
                long testCaseRunId;
                lck.AcquireReaderLock(LOCK_TIMEOUT);
                try
                {
                    if (StepIdMap.TryGetValue(new TestStepIdCacheRow(testRunId.Value, stepPath), out testCaseRunId) == false)
                    {
                        return;
                    }
                }
                finally
                {
                    lck.ReleaseReaderLock();
                }

                if (logMessage == null)
                {
                    logMessage = new LogMessage(stepPath, loggingEvent);
                }
                AddLogEntry(testCaseRunId, logMessage);
            }
            catch
            {
            }
        }
        
        protected long GetTestCaseID(string testCaseUniqueId)
        {
            if (!HasConnection())
            {
                return -1;
            }

            long result;
            lck.AcquireReaderLock(LOCK_TIMEOUT);
            try
            {
                if (TestCaseIdMap.TryGetValue(testCaseUniqueId, out result))//try cache first
                {
                    return result;
                }
            }
            finally
            {
                lck.ReleaseReaderLock();
            }

            //if not, try querying the database

            object value = ExecuteScalar("SELECT ID FROM TestCases WHERE UniqueID = @UniqueID", new KeyValuePair<string, object>("@UniqueID", testCaseUniqueId));
            if (value != null)
            {
                lck.AcquireWriterLock(LOCK_TIMEOUT);
                try
                {
                    TestCaseIdMap[testCaseUniqueId] = ToLong(value);//add to cache
                }
                finally
                {
                    lck.ReleaseWriterLock();
                }
                return ToLong(value);
            }

            // not in database - insert and cache
            value = ExecuteNonQueryThenScalar("INSERT INTO TestCases (UniqueID)" +
            " VALUES (@UniqueID); select last_insert_rowid();", new KeyValuePair<string, object>("@UniqueID", testCaseUniqueId));

            if (value != null)
            {
                long v = ToLong(value);

                lck.AcquireWriterLock(LOCK_TIMEOUT);
                try
                {
                    TestCaseIdMap[testCaseUniqueId] = v;//add to cache
                }
                finally
                {
                    lck.ReleaseWriterLock();
                }

                return v;
            }

            return -1;
        }

        /// <summary>
        /// Get test case unique ID from database ID
        /// </summary>
        /// <param name="testCaseId"></param>
        /// <param name="uniqueId"></param>
        /// <returns></returns>
        protected bool GetTestCase(long testCaseId, out string uniqueId)
        {
            uniqueId = null;
            if (!HasConnection())
            {
                return false;
            }
            //try cache first
            lck.AcquireReaderLock(LOCK_TIMEOUT);
            if (TestCaseIdMap.Values.Contains(testCaseId))
            {
                uniqueId = TestCaseIdMap.First(x => x.Value == testCaseId).Key;
                lck.ReleaseReaderLock();
                return true;
            }
            lck.ReleaseReaderLock();

            object value = ExecuteScalar("SELECT UniqueID FROM TestCases WHERE ID = @ID", new KeyValuePair<string, object>("@ID", testCaseId));

            if (value != null)
            {
                uniqueId = (string)value;
                lck.AcquireWriterLock(LOCK_TIMEOUT);
                TestCaseIdMap[uniqueId] = testCaseId;//add to cache
                lck.ReleaseWriterLock();
                return true;
            }
            return false;
        }

        public void AddLogEntry(long testCaseRunId, LogMessage msg)
        {
            if (!HasConnection())
            {
                return;
            }

            ExecuteNonQuery("INSERT INTO LogEntries (TestStepId, Logger, Level, Timestamp, TextData, BinaryData, BinaryDataType, Note)" +
                " VALUES (@TestStepId, @Logger, @Level, @Timestamp, @TextData, @BinaryData, @BinaryDataType, @Note)",
                new KeyValuePair<string, object>("@TestStepId", testCaseRunId),
                new KeyValuePair<string, object>("@Logger", msg.Logger),
                new KeyValuePair<string, object>("@Level", msg.Level),
                new KeyValuePair<string, object>("@Timestamp", msg.Timestamp),
                new KeyValuePair<string, object>("@TextData", msg.Message),
                new KeyValuePair<string, object>("@BinaryData", msg.BinaryData),
                new KeyValuePair<string, object>("@BinaryDataType", msg.BinaryDataType),
                new KeyValuePair<string, object>("@Note", msg.Note));
        }

        public bool Delete(TestRunLogInfo info)
        {
            if (!HasConnection())
            {
                return false;
            }

            TestRunLogInfoSqlite logInfo = info as TestRunLogInfoSqlite;
            if (logInfo == null)
            {
                return false;
            }

            ExecuteNonQuery("DELETE FROM LogEntries WHERE TestStepID IN (SELECT ID FROM TestSteps WHERE TestRunId=@id)", new KeyValuePair<string, object>("@id", logInfo.DbId));
            ExecuteNonQuery("DELETE FROM TestSteps WHERE TestRunId=@id", new KeyValuePair<string, object>("@id", logInfo.DbId));
            ExecuteNonQuery("DELETE FROM TestRuns WHERE ID=@id", new KeyValuePair<string, object>("@id", logInfo.DbId));

            if (LogRemoved != null)
            {
                LogRemoved(this, new TestRunLogInfoEventArgs() { TestRunLogInfo = info });
            }

            return true;
        }

        public void Rename(TestRunLogInfo info, string newName)
        {
            if (!HasConnection())
            {
                return;
            }

            TestRunLogInfoSqlite logInfo = info as TestRunLogInfoSqlite;
            if (logInfo == null)
            {
                return;
            }

            ExecuteNonQuery("UPDATE TestRuns SET Name=@Name WHERE ID=@id", new KeyValuePair<string, object>("@id", logInfo.DbId), new KeyValuePair<string, object>("@Name", newName));
            logInfo.Name = newName;

            if (LogChanged != null)
            {
                LogChanged(this, new TestRunLogInfoEventArgs() { TestRunLogInfo = info });
            }
        }

        public long GetStoredLogCount()
        {
            if (!HasConnection())
            {
                return 0;
            }

            using (var reader = ExecuteReader("SELECT COUNT(*) FROM TestRuns"))
            {
                while (reader.Read())
                {
                    return ReadLong(reader, 0).Value;
                }
            }
            return 0;
        }

        public IEnumerable<TestRunLogInfo> GetStoredLogs()
        {
            if (!HasConnection())
            {
                yield break;
            }

            using (var reader = ExecuteReader("SELECT ID FROM TestRuns"))
            {
                while (reader.Read())
                {
                    yield return GetStoredLog(ReadLong(reader, 0).Value);
                    //var row = new TestRunLogInfoSqlite(ReadLong(reader, 0).Value, reader.GetDateTime(1), ReadString(reader, 4), ReadString(reader, 2), ReadBytes(reader, 3), (int)ReadLong(reader, 5));
                    //yield return row;
                    //result.Add(row);
                }
            }

            yield break;
        }

        TestRunLogInfo GetStoredLog(long dbId)
        {
            if (!HasConnection())
            {
                return null;
            }

            using (var reader = ExecuteReader("SELECT TestRuns.ID, TestRuns.StartTime, TestRuns.Description, TestRuns.Name, (SELECT COUNT(*) FROM TestSteps WHERE TestSteps.TestRunId = TestRuns.ID) FROM TestRuns WHERE  TestRuns.ID=@id", new KeyValuePair<string, object>("@id", dbId)))
            {
                while (reader.Read())
                {
                    return new TestRunLogInfoSqlite(dbId, reader.GetDateTime(1), ReadString(reader, 3), ReadString(reader, 2), (int)ReadLong(reader, 4));
                }
            }
            
            return null;
        }
        
        public TestRunLog GetFullLogData(TestRunLogInfo info)
        {
            if (!HasConnection())
            {
                return null;
            }

            TestRunLogInfoSqlite logInfo = info as TestRunLogInfoSqlite;
            if (logInfo == null)
            {
                return null;
            }
            TestRunLog result = new TestRunLog(info); // basic constructor, fill in steps and messages later
            result.Storage = this as ILogStorage; //this class does not implement ILogStorage but the ones inheriting Common are

            result.Steps = new List<StepInfo>();

            using (var reader = ExecuteReader("SELECT Settings FROM TestRuns WHERE ID=@id", new KeyValuePair<string, object>("@id", logInfo.DbId)))
            {
                while (reader.Read())
                {
                    result.Settings = ReadBytes(reader, 0);
                }
            }

            using (var reader = ExecuteReader("SELECT timestamp, TestCaseID, Name, TreePath, RowType, RawResult, EvaluatedResult, RepetitionResult FROM TestSteps WHERE TestRunID=@id ORDER BY ID", new KeyValuePair<string, object>("@id", logInfo.DbId)))
            {
                while (reader.Read())
                {
                    var row = new StepInfo();
                    row.Timestamp = reader.GetDateTime(0);
                    long? testCaseId = ReadLong(reader, 1);

                    if (testCaseId.HasValue)
                    {
                        string uniqueId;
                        if (GetTestCase(testCaseId.Value, out uniqueId))
                        {
                            row.testCaseUniqueId = uniqueId;
                        }
                    }


                    row.Name = ReadString(reader, 2);
                    row.TreePath = ReadString(reader, 3);
                    row.Path = new StepPath(row.TreePath);
                    row.RowType = (StepInfoTypeEnum)Enum.Parse(typeof(StepInfoTypeEnum), ReadString(reader, 4));
                    row.RawResult = ReadString(reader, 5);
                    row.EvaluatedResult = ReadString(reader, 6);
                    row.RepetitionResult = ReadString(reader, 7);
                    
                    result.Steps.Add(row);
                }
            }
            
            result.Messages = new List<LogMessage>();
            int sequenceNumber = 1;
            using (var reader = ExecuteReader("SELECT LogEntries.Timestamp, Logger, Level, TextData, BinaryData, BinaryDataType, Note, TestSteps.TreePath, LogEntries.ID FROM LogEntries INNER JOIN TestSteps ON LogEntries.TestStepId=TestSteps.ID WHERE TestSteps.TestRunID=@id ORDER BY LogEntries.ID", new KeyValuePair<string, object>("@id", logInfo.DbId)))
            {
                while (reader.Read())
                {
                    var row = new LogMessageSqlite();
                    row.Timestamp = reader.GetDateTime(0);
                    row.Logger = ReadString(reader, 1);
                    row.Level = ReadString(reader, 2);
                    row.Message = ReadString(reader, 3);
                    row.BinaryData = ReadBytes(reader, 4);
                    row.BinaryDataType = ReadString(reader, 5);
                    row.Note = ReadString(reader, 6);
                    row.Path = ReadString(reader, 7);
                    row.Id = ReadLong(reader, 8).Value;
                    row.SequenceNumber = sequenceNumber++;

                    result.Messages.Add(row);
                }
            }
            
            return result;
        }

        public void Update(LogMessage message)
        {
            LogMessageSqlite msg = message as LogMessageSqlite;
            if (msg == null)
            {
                return;
            }

            if (!HasConnection())
            {
                return;
            }

            ExecuteNonQuery("UPDATE LogEntries SET TextData = @TextData, BinaryData = @BinaryData, BinaryDataType = @BinaryDataType, Note = @Note WHERE ID=@id",
                new KeyValuePair<string, object>("@TextData", msg.Message),
                new KeyValuePair<string, object>("@BinaryData", msg.BinaryData),
                new KeyValuePair<string, object>("@BinaryDataType", msg.BinaryDataType),
                new KeyValuePair<string, object>("@Note", msg.Note),
                new KeyValuePair<string, object>("@Id", msg.Id));
        }
    }
}
