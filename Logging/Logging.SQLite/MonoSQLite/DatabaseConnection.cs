﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#if __MonoCS__
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Diagnostics;
using Mono.Data.Sqlite;
using yats.Logging.Interface;

namespace yats.Logging.SQLite.Mono
{    
    internal class DatabaseConnection
    {
        private static string m_sqliteFilePath;
        private static string m_sqliteEmptyDbPath;
        

        private DatabaseConnection()
        {
        }

        /// <param name="reset">If true, the existing database will be replaced by empty database</param>
        /// <param name="sqliteFilePath">.sqlite file path</param>
        /// <param name="sqliteEmptyDbPath">SQLite file path for the default empty database. It is used when the normal database is not found or must be reset</param>
        public static IDataBaseAppender Init(bool reset, string sqliteFilePath, string sqliteFileName, string sqliteEmptyDbPath)
        {
            SqliteConnection connection = null;
            m_sqliteFilePath = sqliteFileName;
            m_sqliteEmptyDbPath = sqliteEmptyDbPath;

            try
            {
                Directory.CreateDirectory(sqliteFilePath);

                if(!File.Exists( sqliteFileName ) || reset)
                {
                    ResetDatabase( connection );
                }
                var connString = @"Data Source=" + Path.GetFullPath(sqliteFileName);
                connection = new SqliteConnection( connString );
                connection.Open( );
                SqliteCommand command = new SqliteCommand( "PRAGMA integrity_check;", connection );
                var result2 = command.ExecuteScalar( );
                if(result2 is string)
                {
                    if((result2 as string) != "ok")
                    {
                        throw new Exception( "Database check failed: " + result2 );
                    }
                }

                TryUpdateVersion(connection);                

                command = new SqliteCommand( "PRAGMA foreign_keys = ON;", connection );
                command.ExecuteNonQuery( );
                return new SQLiteAppender(connection);
            }
            catch (Exception ex)
            {
                RaiseOnError(ex.Message);
                connection = null;
            }
            
            try
            {
                ResetDatabase( connection );
                var connString = @"Data Source=" + Path.GetFullPath(sqliteFileName);
                connection = new SqliteConnection( connString );
                connection.Open( );
                SqliteCommand command = new SqliteCommand( "PRAGMA foreign_keys = ON;", connection );
                command.ExecuteNonQuery( );
            }
            catch
            {
                return null;
            }
            
            return new SQLiteAppender(connection);
        }

        internal static long ToLong(object value)
        {
            long v;
            if (value is Int32)
            {
                v = (long)(Int32)value; // After switching to Community.CsharpSqlite.SQLiteClient, returns an Int32 object. It can not be casted to long
            }
            else // should be long already
            {
                v = (long)value;
            }
            return v;
        }

        protected static void ResetDatabase(SqliteConnection connection)
        {
            if(connection != null && connection.State == System.Data.ConnectionState.Open)
            {
                connection.Close( );
                connection = null;
            }

            yats.Utilities.FileUtil.BackupPush( m_sqliteFilePath, 5 );

            try
            {
                File.Copy( m_sqliteEmptyDbPath, m_sqliteFilePath, true );
            }
            catch (Exception ex)
            {
                RaiseOnError(ex.Message);
            }
        }

        protected static void RaiseOnError(string message)
        {
            if (OnError != null)
            {
                OnError(message);
            }
        }

        private static void TryUpdateVersion(SqliteConnection connection)
        {
            if (new SchemaUpdate().TryUpdateVersion(connection) == false)
            {
                RaiseOnError("Can not update database - creating new database. The previous data was backed up");
                ResetDatabase(connection);
            }
        }

        public static event ConnectionErrorDelegate OnError;
    }
}
#endif
