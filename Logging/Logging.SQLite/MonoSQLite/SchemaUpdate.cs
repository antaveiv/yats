﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#if __MonoCS__
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Data.Sqlite;

namespace yats.Logging.SQLite.Mono
{
    internal class SchemaUpdate
    {
        public bool TryUpdateVersion(SqliteConnection instance)
        {
            long currentVersion = ToLong(new SqliteCommand("PRAGMA user_version;", instance).ExecuteScalar());

            if (currentVersion == 3)
            {
                SqliteCommand command = new SqliteCommand( "ALTER TABLE TestRuns ADD COLUMN Name TEXT", instance );
                command.ExecuteNonQuery( );

                command = new SqliteCommand( "PRAGMA user_version = 4;", instance );
                command.ExecuteNonQuery( );

                currentVersion = 4;
            }

            // if upgrading more through than one version, several sequences may be executed, e.g 3->4, 4->5
            if (currentVersion == 4)
            {
                // Remove Timestamp default value from LogEntries
                SqliteCommand command = new SqliteCommand( "ALTER TABLE LogEntries RENAME TO oXHFcGcd04oXHFcGcd04_LogEntries", instance );
                command.ExecuteNonQuery( );
                command = new SqliteCommand( "CREATE TABLE LogEntries (\"ID\" INTEGER PRIMARY KEY  NOT NULL ,\"Timestamp\" DATETIME,\"TestStepId\" INTEGER NOT NULL ,\"Logger\" VARCHAR NOT NULL ,\"Level\" VARCHAR NOT NULL ,\"TextData\" TEXT,\"BinaryData\" BLOB,\"BinaryDataType\" text)", instance );
                command.ExecuteNonQuery( );
                command = new SqliteCommand( "INSERT INTO LogEntries SELECT \"ID\",\"Timestamp\",\"TestStepId\",\"Logger\",\"Level\",\"TextData\",\"BinaryData\",\"BinaryDataType\" FROM oXHFcGcd04oXHFcGcd04_LogEntries", instance );
                command.ExecuteNonQuery( );
                command = new SqliteCommand( "DROP TABLE oXHFcGcd04oXHFcGcd04_LogEntries", instance );
                command.ExecuteNonQuery( );

                // Remove Timestamp default value from TestRuns
                command = new SqliteCommand( "ALTER TABLE TestRuns RENAME TO oXHFcGcd04oXHFcGcd04_TestRuns", instance );
                command.ExecuteNonQuery( );
                command = new SqliteCommand( "CREATE TABLE TestRuns (\"ID\" INTEGER PRIMARY KEY  NOT NULL ,\"StartTime\" DATETIME,\"Description\" TEXT,\"Settings\" BLOB,\"Name\" TEXT)", instance );
                command.ExecuteNonQuery( );
                command = new SqliteCommand( "INSERT INTO TestRuns SELECT \"ID\",\"StartTime\",\"Description\",\"Settings\",\"Name\" FROM oXHFcGcd04oXHFcGcd04_TestRuns", instance );
                command.ExecuteNonQuery( );
                command = new SqliteCommand( "DROP TABLE oXHFcGcd04oXHFcGcd04_TestRuns", instance );
                command.ExecuteNonQuery( );

                // Remove Timestamp default value from TestSteps
                command = new SqliteCommand( "ALTER TABLE TestSteps RENAME TO oXHFcGcd04oXHFcGcd04_TestSteps", instance );
                command.ExecuteNonQuery( );
                command = new SqliteCommand( "CREATE TABLE TestSteps (\"ID\" INTEGER PRIMARY KEY  NOT NULL ,\"TestRunId\" INTEGER NOT NULL , \"timestamp\" DATETIME, \"TestCaseID\" INTEGER,\"Name\" VARCHAR, \"TreePath\" varchar,\"RowType\" varchar,\"RawResult\" varchar, \"EvaluatedResult\" varchar, \"RepetitionResult\" varchar)", instance );
                command.ExecuteNonQuery( );
                command = new SqliteCommand( "INSERT INTO TestSteps SELECT \"ID\",\"TestRunId\",\"timestamp\",\"TestCaseID\",\"Name\",\"TreePath\",\"RowType\",\"RawResult\",\"EvaluatedResult\",\"RepetitionResult\" FROM oXHFcGcd04oXHFcGcd04_TestSteps", instance );
                command.ExecuteNonQuery( );
                command = new SqliteCommand( "DROP TABLE oXHFcGcd04oXHFcGcd04_TestSteps", instance );
                command.ExecuteNonQuery( );

                command = new SqliteCommand( "PRAGMA user_version = 5;", instance );
                command.ExecuteNonQuery( );

                currentVersion = 5;
            }
            
            //Add indices
            if(currentVersion == 5)
            {
                new SQLiteCommand("CREATE INDEX TestRunIdIdx ON TestSteps (TestRunId)", instance ).ExecuteNonQuery( );
                new SQLiteCommand("CREATE INDEX TestStepIdIdx ON LogEntries (TestStepId)", instance).ExecuteNonQuery();
                
                new SQLiteCommand( "PRAGMA user_version = 6;", instance ).ExecuteNonQuery( );

                currentVersion = 6;
            }

            if (currentVersion == 6)
            {
                SQLiteCommand command = new SQLiteCommand("ALTER TABLE LogEntries ADD COLUMN Note TEXT", instance);
                command.ExecuteNonQuery();

                command = new SQLiteCommand("PRAGMA user_version = 7;", instance);
                command.ExecuteNonQuery();

                currentVersion = 7;
            }

            return currentVersion == 7;
        }

        internal static long ToLong(object value)
        {
            long v;
            if(value is Int32)
            {
                v = (long)(Int32)value; // After switching to Community.CsharpSqlite.SQLiteClient, returns an Int32 object. It can not be casted to long
            }
            else // should be long already
            {
                v = (long)value;
            }
            return v;
        }
    }
}
#endif