﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using yats.Logging.Interface;

namespace yats.Logging.SQLite
{
    public class DatabaseTestRunIdMap : IDatabaseTestRunIdMap
    {
        private DatabaseTestRunIdMap()
        {
        }

        private static DatabaseTestRunIdMap s_instance = new DatabaseTestRunIdMap();

        public static DatabaseTestRunIdMap Instance
        {
            [DebuggerStepThrough]
            get
            {
                return s_instance;
            }
        }

        /// <summary>
        /// maps auto-incremented testRunId with actual DB row ID
        /// </summary>
        protected Dictionary<int, long> TestRunIdMap = new Dictionary<int, long>();
        protected ReaderWriterLock lck = new ReaderWriterLock();
        protected const int LOCK_TIMEOUT = 10000;
        
        public void AddID(int testRunId, long dbId)
        {
            lck.AcquireWriterLock(LOCK_TIMEOUT);
            TestRunIdMap[testRunId] = dbId;
            lck.ReleaseWriterLock();
        }

        /// <summary>
        /// Convert runtime Test Run ID (incremented from 0 on each test run start) to DB row ID
        /// </summary>
        /// <param name="testRunId"></param>
        /// <returns></returns>
        public long ToTestRunDbID(int testRunId)
        {
            lck.AcquireReaderLock(LOCK_TIMEOUT);
            long result;
            if (false == TestRunIdMap.TryGetValue(testRunId, out result))
            {
                lck.ReleaseReaderLock();
                throw new Exception("Test run DB ID is not in map");
            }
            lck.ReleaseReaderLock();
            return result;
        }

        public bool TryGetDbID(int testRunId, out long testRunDbID)
        {
            bool result = false;
            lck.AcquireReaderLock(LOCK_TIMEOUT);
            result = TestRunIdMap.TryGetValue(testRunId, out testRunDbID);
            lck.ReleaseReaderLock();
            return result;
        }
    }
}
