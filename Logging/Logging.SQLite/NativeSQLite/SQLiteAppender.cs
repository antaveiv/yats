﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#if !__MonoCS__
using System;
using System.Collections.Generic;
using Microsoft.Data.Sqlite;
using System.Linq;
using System.Threading;
using log4net.Core;
using yats.Logging.Interface;
using yats.TestCase.Interface;
using System.Diagnostics;
using yats.Utilities;
using System.Data.Common;
using yats.Logging.SQLite.Properties;

namespace yats.Logging.SQLite.Native
{
    internal class SQLiteAppender : Common, IDataBaseAppender
    {
        protected SqliteConnection connection = null;
                
        public SQLiteAppender(Microsoft.Data.Sqlite.SqliteConnection connection)
        {
            this.connection = connection;
        }

        #region IDisposable Members

        private bool _disposed = false;

        ~SQLiteAppender()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);

            // Use SupressFinalize in case a subclass 
            // of this type implements a finalizer.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (connection != null)
                    {
                        try
                        {
                            if (Settings.Default.VacuumCounter > Settings.Default.VacuumCounterMax)
                            {
                                new SqliteCommand("VACUUM", connection).ExecuteNonQuery();
                                Settings.Default.VacuumCounter = 0;
                                Settings.Default.Save();
                            }
                            else
                            {
                                Settings.Default.VacuumCounter++;
                                Settings.Default.Save();
                            }                            
                        }
                        catch { }
                        connection.Close();
                        connection = null;
                    }
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
        }

        #endregion

        [System.Diagnostics.CodeAnalysis.SuppressMessage ("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        protected override void ExecuteNonQuery(string query, params KeyValuePair<string, object>[] parameters)
        {
            using (SqliteCommand command = new SqliteCommand(query, connection))
            {
                if (parameters != null)
                {
                    foreach (var pair in parameters)
                    {
                        command.Parameters.AddWithValue(pair.Key, pair.Value ?? DBNull.Value);
                    }
                }
                command.ExecuteNonQuery();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage ("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        protected override object ExecuteScalar(string query, params KeyValuePair<string, object>[] parameters)
        {
            using (SqliteCommand command = new SqliteCommand(query, connection))
            {
                if (parameters != null)
                {
                    foreach (var pair in parameters)
                    {
                        command.Parameters.AddWithValue(pair.Key, pair.Value ?? DBNull.Value);
                    }
                }
                return command.ExecuteScalar();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage ("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        protected override object ExecuteNonQueryThenScalar(string query, params KeyValuePair<string, object>[] parameters)
        {
            using (SqliteCommand command = new SqliteCommand(query, connection))
            {
                if (parameters != null)
                {
                    foreach (var pair in parameters)
                    {
                        command.Parameters.AddWithValue(pair.Key, pair.Value ?? DBNull.Value);
                    }
                }
                command.ExecuteNonQuery();
                return command.ExecuteScalar(); 
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage ("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        protected override DbDataReader ExecuteReader(string query, params KeyValuePair<string, object>[] parameters)
        {
            using (SqliteCommand command = new SqliteCommand(query, connection))
            {
                if (parameters != null)
                {
                    foreach (var pair in parameters)
                    {
                        command.Parameters.AddWithValue(pair.Key, pair.Value ?? DBNull.Value);
                    }
                }
                return command.ExecuteReader();
            }
        }

        protected override bool HasConnection()
        {
            return connection != null;
        }
    }
}
#endif
