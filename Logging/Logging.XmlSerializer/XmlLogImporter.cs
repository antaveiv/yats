﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.IO;
using System.IO.Compression;
using System.Xml.Serialization;
using yats.Logging.Interface;

namespace yats.Logging.XmlLogSerializer
{
    public class XmlLogImporter : ILogImporter
    {
        private string m_fileName;
        public XmlLogImporter(string fileName)
        {
            m_fileName = fileName;
        }

        public TestRunLog Import()
        {
            XmlSerializer serializer = yats.Utilities.SerializationHelper.GetSerializer(typeof(SerializableTestRunLog));
            SerializableTestRunLog result = null;

            if (m_fileName.EndsWith(".ylogz"))
            {
                using (FileStream fileStream = new FileInfo(m_fileName).OpenRead())
                {
                    using (GZipStream decompressionStream = new GZipStream(fileStream, CompressionMode.Decompress))
                    {
                        //MemoryStream ms = new MemoryStream();
                        //byte[] buffer = new byte[16 * 1024]; // Fairly arbitrary size
                        //int bytesRead;

                        //while ((bytesRead = decompressionStream.Read(buffer, 0, buffer.Length)) > 0)
                        //{
                        //    ms.Write(buffer, 0, bytesRead);
                        //}

                        //byte [] test = ms.ToArray();
                        result = (SerializableTestRunLog)serializer.Deserialize(decompressionStream);
                    }
                }
            }
            else
            {
                using (TextReader textReader = new StreamReader(m_fileName))
                {
                    result = (SerializableTestRunLog)serializer.Deserialize(textReader);
                }
            }
            if (result.FormatVersion != SerializableTestRunLog.VERSION)
            {
                throw new Exception("Unsupported file version");
            }
            return result.ToTestRunLog();
        }
    }
}