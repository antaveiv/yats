﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.IO;
using System.IO.Compression;
using yats.Logging.Interface;

namespace yats.Logging.XmlLogSerializer
{
    public class XmlLogExporter : ILogExporter
    {
        private string m_fileName;
        public XmlLogExporter(string fileName)
        {
            m_fileName = fileName;
        }

        public void Export(TestRunLog log)
        {
            SerializableTestRunLog serializableLog = new SerializableTestRunLog(log.Steps, log.Messages, log.StartTime, log.Name, log.Description, log.Settings);

            if (m_fileName.EndsWith(".ylogz"))
            {
                using (FileStream compressedFileStream = File.Create(m_fileName))
                {
                    using (GZipStream compressionStream = new GZipStream(compressedFileStream, CompressionMode.Compress))
                    {
                        yats.Utilities.SerializationHelper.SerializeToStream(serializableLog, compressionStream);
                    }
                }
            }
            else
            {
                yats.Utilities.SerializationHelper.SerializeToFile(serializableLog, m_fileName);
            }
        }
    }
}
