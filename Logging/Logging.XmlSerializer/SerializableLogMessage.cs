﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Text;
using yats.Logging.Interface;

namespace yats.Logging.XmlLogSerializer
{
    public class SerializableLogMessage
    {
        public int SequenceNumber;
        public string Path;
        public DateTime Timestamp;
        public string Logger;
        public string Level;
        public string Message;
        public string BinaryData;
        public string BinaryDataType;
        public string Note;

        public SerializableLogMessage()
        {
        }

        public SerializableLogMessage(LogMessage msg)
        {
            this.SequenceNumber = msg.SequenceNumber;
            this.Path= msg.Path;
            this.Timestamp = msg.Timestamp;
            this.Logger= msg.Logger;
            this.Level  = msg.Level;
            this.Note = msg.Note;
            if (msg.Message != null)
            {
                this.Message = Convert.ToBase64String(Encoding.UTF8.GetBytes(msg.Message));
            }
            if (msg.BinaryData != null)
            {
                this.BinaryData = Convert.ToBase64String(msg.BinaryData);
            }
            this.BinaryDataType = msg.BinaryDataType;
        }
    }
}
