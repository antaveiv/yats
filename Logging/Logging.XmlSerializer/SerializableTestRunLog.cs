﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Text;
using yats.Logging.Interface;

namespace yats.Logging.XmlLogSerializer
{
    public class SerializableTestRunLog 
    {
        public const string VERSION = "v1";

        public SerializableTestRunLog()
        {
            // need to have parameterless constructor for XML serializer
        }

        public SerializableTestRunLog(List<StepInfo> steps,
                List<LogMessage> messages,
                DateTime startTime,
                string name,
                string description,
                byte[] settings)
        {
            this.steps = steps;
            this.messages = messages;
            this.startTime = startTime;
            this.name = name;
            this.description = description;
            this.settings = settings;
        }

        protected List<StepInfo> steps;
        public List<StepInfo> Steps
        {
            get { return steps; }
            set { steps = value; }
        }

        protected List<SerializableLogMessage> serializableLogMessages;
        public List<SerializableLogMessage> SerializableLogMessages
        {
            get {
                if (serializableLogMessages == null && messages != null)
                {
                    serializableLogMessages = new List<SerializableLogMessage>(messages.Count);
                    foreach (var msg in messages)
                    {
                        serializableLogMessages.Add(new SerializableLogMessage(msg));
                    }
                    messages = null;
                }
                return serializableLogMessages;
            }
            set { serializableLogMessages = value; messages = null; }
        }

        protected DateTime startTime;
        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }

        protected string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        protected string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        protected byte[] settings;
        public byte[] Settings
        {
            get { return settings; }
            set { settings = value; }
        }

        protected string formatVersion = VERSION;
        public string FormatVersion
        {
            get { return formatVersion; }
            set { formatVersion = value; }
        }
                        
        List<StepInfo> GetSteps()
        {
            return steps;
        }

        List<LogMessage> messages = null;
        List<LogMessage> GetMessages()
        {
            // store either serialized or deserialized messages, not both. One is used for saving, another for loading, therefore conversion is done only once
            if (messages == null && serializableLogMessages != null)
            {
                messages = new List<LogMessage>(serializableLogMessages.Count);
                foreach (var msg in serializableLogMessages)
                {
                    var message = new LogMessage();

                    message.SequenceNumber = msg.SequenceNumber;
                    message.Path = msg.Path;
                    message.Timestamp = msg.Timestamp;
                    message.Logger = msg.Logger;
                    message.Level = msg.Level;
                    message.Note = msg.Note;
                    if (msg.Message != null)
                    {
                        message.Message = Encoding.UTF8.GetString(Convert.FromBase64String(msg.Message));
                    }
                    if (msg.BinaryData != null)
                    {
                        message.BinaryData = Convert.FromBase64String(msg.BinaryData);
                    }
                    message.BinaryDataType = msg.BinaryDataType;

                    messages.Add(message);
                }
                serializableLogMessages = null;
            }
            return messages;
        }

        internal TestRunLog ToTestRunLog()
        {
            return new TestRunLog(GetSteps(), GetMessages(), startTime, name, description, settings);
        }
    }
}
