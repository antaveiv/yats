﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using yats.Ports;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Threading;

namespace TestCase.AtCommand
{
#if FALSE
    public class WaitForSms : IYatsTestCase, ICancellable
    {
        protected string number;
        [CanBeNull]
        public string Number
        {
            get { return number; }
            set { number = value; }
        }

        protected string text;
        [CanBeNull]
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        protected int timeout;
        public int Timeout
        {
            get { return timeout; }
            set { timeout = value; }
        }

        protected static String received_text;
        public String ReceivedText
        {
            get { return received_text; }            
        }

        protected static String received_number;
        public String ReceivedNumber
        {
            get { return received_number; }
        }

         protected SerialPortSettingsParameter portSettings;
        public SerialPortSettingsParameter Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        public WaitForSms()
        {
        }

        public ITestResult Execute()
        {
            AbstractPort port = PortOpenHelper.Open(portSettings, cancelWaitEvent);
            
            Dictionary<string, ReadOperationResult> responses = new Dictionary<string, ReadOperationResult>();
            
            responses.Add("+CMTI", new ReadOperationResult(ReadOperationResult.Result.SUCCESS));

            Parser parser = new ModemResponseParser(responses);

            ReadOperationResult result = port.Execute( timeout, parser, cancelWaitEvent );

            if (result.OperationResult == ReadOperationResult.Result.SUCCESS)
            {
                String res;
                int sms_index;

                responses.Remove("+CMTI");
                res = System.Text.ASCIIEncoding.ASCII.GetString(parser.GetReceivedData());

                int index = res.IndexOf("+CMTI");
                if (index > 0)
                {
                    res = res.Remove(0, index);
                    index = res.IndexOf(",");
                    if (index > 0)
                    {
                        res = res.Remove(0, index + 1);

                        try
                        {
                            /*  istraukiam sms'o indexa */
                            sms_index = System.Convert.ToInt32(res); //offset+7

                            responses.Add("OK", new ReadOperationResult(ReadOperationResult.Result.SUCCESS));
                            responses.Add("ERROR", new ReadOperationResult(ReadOperationResult.Result.ERROR));

                            parser = new ModemResponseParser(responses);

                            while (sms_index > 0)
                            { /* skaitom visus sms*/

                                result = port.Command("AT+CMGR=" + sms_index + "\r", parser, timeout, cancelWaitEvent); /* laikti ok arba error arba +cmgr*/

                                if (result.OperationResult == ReadOperationResult.Result.SUCCESS)
                                {                                    
                                    /* sms'o apdorojimas*/
                                    res = System.Text.ASCIIEncoding.ASCII.GetString(parser.GetReceivedData());

                                    String[] received_sms = res.Split(new string[] {"\r\n"}, StringSplitOptions.None);
                                    received_text = received_sms[2];

                                    received_sms = received_sms[1].Split('"');
                                    received_number = received_sms[3];

                                    /*jeigu number !=NULL cia reikia tikrinti ar gavom sms is laukiamo numerio */

                                    /*jeigu text !=NULL cia reikia tikrinti ar gautame sms yra laukiamas stringas*/

                                    /*  trinam apdorota sms*/

                                    result = port.Command("AT+CMGD=" + sms_index + "\r", parser, timeout, cancelWaitEvent);
                                    if (result.OperationResult == ReadOperationResult.Result.SUCCESS)
                                    {
                                        // log4net sms istrintas sekmingai
                                    }
                                }

                                sms_index--;
                            }
                        }
                        catch (OverflowException)
                        {
                            //Logger.Error("Convert Exception " + "OverflowException");
                        }
                        catch (FormatException)
                        {
                            //Logger.Error("Convert Exception " + "FormatException");
                        }
                    }
                }
            }

            PortOpenHelper.Close(port);

            return ResultHelper.Convert(result);
        }

    #region ICancellable Members

        ManualResetEvent cancelWaitEvent = new ManualResetEvent(false);

        public void Cancel()
        {
            cancelWaitEvent.Set();
        }

        #endregion
    }
#endif
}
