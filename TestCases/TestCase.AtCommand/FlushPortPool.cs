﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Attributes;
using yats.Ports.Utilities;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.YatsNativeTestCase;

namespace TestCase.AtCommand
{
    [Description("Force all ports closed by test cases to be actually closed")]
    public class FlushPortPool : IYatsTestCase
    {

        #region IYatsTestCase Members

        public ITestResult Execute()
        {
            PortOpenHelper.Flush();
            return new TestResult(true);
        }

        #endregion
    }
}
