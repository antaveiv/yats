﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using log4net;
using yats.Attributes;
using yats.Ports;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.IO;
using System.Threading;
using yats.Ports.Utilities;

namespace TestCase.AtCommand
{
    [Description("Write a configured byte sequence to a port")]
    public class WritePortData : IYatsTestCase, ICancellable
    {
        internal static ILog Logger = LogManager.GetLogger("WritePortData ");

        protected byte[] data;
        public byte[] Data
        {
            get { return data; }
            set { data = value; }
        }

        protected PortSettings portSettings;
        public PortSettings Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        public WritePortData()
        {
        }

        public ITestResult Execute()
        {
            AbstractPort port = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;
                try
                {
                    //PortReusePool.CancelLog.WriteLine(string.Format("{0:G} TC.Before Open()", DateTime.Now));
                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);
                    //PortReusePool.CancelLog.WriteLine(string.Format("{0:G} TC.After Open()", DateTime.Now));
                    port.Write(data, 0, data.Length);
                    return new TestResult(ResultEnum.PASS);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cancelHelper.OnCancel -= myDelegate;
                    PortOpenHelper.Close(port, portSettings);
                }
            }
        }

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
            PortOpenHelper.CancelOpen(portSettings);
        }
        
        #endregion
    }
}