﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using log4net;
using yats.Ports;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;

using System.Threading;
using yats.Ports.Utilities.Parsers;
using yats.Ports.Utilities;

namespace TestCase.AtCommand
{
#if DEBUG
    public class PermanentParserTest : IYatsTestCase, ICancellable
    {
        internal static ILog Logger = LogManager.GetLogger("WaitForPortData");

        protected PortSettings portSettings;
        public PortSettings Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        protected object rxLock = new object();
        protected Parser rx;
        protected AbstractPort port;

        private void OnParserDone(TextLineParser parser)
        {
            string line = parser.GetReceivedLineUTF();
            Logger.Info(line);
        }

        public ITestResult Execute()
        {
            AbstractPort port = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;
                try
                {
                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);

                    lock (rxLock)
                    {
                        this.rx = new PermanentParser<TextLineParser>(new TextLineParser(), OnParserDone);
                    }

                    ReadOperationResult result = port.Execute(rx, cancelWaitEvent);

                    Logger.Debug(rx.GetReceivedData());

                    lock (rxLock)
                    {
                        this.rx = null;
                    }

                    return ResultHelper.Convert(result);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cancelHelper.OnCancel -= myDelegate;
                    PortOpenHelper.Close(port, portSettings);
                }
            }
        }

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
            lock (rxLock)
            {
                if (rx != null)
                {
                    port.Cancel(rx);
                }
                else
                {
                    PortOpenHelper.CancelOpen(portSettings);
                }
            }
        }

        #endregion
    }
#endif
}
