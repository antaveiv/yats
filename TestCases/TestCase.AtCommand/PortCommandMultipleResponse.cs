﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using log4net;
using yats.Ports;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Threading;
using yats.Ports.Utilities;
using yats.Ports.Utilities.Parsers;
using System.Collections.Generic;

namespace TestCase.AtCommand
{
    [Description("Write configured data to a port. Wait for a configured response or timeout. Can be cancelled")]
    public class PortCommandMultipleResponse : IYatsTestCase, ICancellable
    {
        [Description("If any of PassResponses sequences is received the test will pass")]
        [Parameter, CanBeNull]
        public List<byte[]> PassResponses;

        [Description("If any of FailResponses sequences is received the test will fail")]
        [Parameter, CanBeNull]
        public List<byte[]> FailResponses;

        [Description("Command to send before waiting for response")]
        [Parameter, CanBeNull]
        public byte[] Command;

        [Parameter, UseDefault]
        public TimeSpan Timeout = TimeSpan.FromMilliseconds(100);
        
        [Description("Port settings")]
        protected PortSettings portSettings;
        public PortSettings Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        [Description("If set, will lock the port object from other threads. Normally, should not be used")]
        [Parameter, CanBeNull]
        public bool? LockPort;

        public PortCommandMultipleResponse()
        {
        }

        public ITestResult Execute()
        {
            AbstractPort port = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;
                try
                {
                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);
                    var rx = new WaitForMultipleSequences(PassResponses, FailResponses);

                    ReadOperationResult result;
                    if (LockPort.HasValue && LockPort.Value)
                    {
                        lock (port)
                        {
                            result = port.Command(Command, rx, Timeout, cancelWaitEvent);
                        }
                    }
                    else
                    {
                        result = port.Command(Command, rx, Timeout, cancelWaitEvent);
                    }

                    LogManager.GetLogger("Response").Debug(rx.GetReceivedData());

                    return ResultHelper.Convert(result);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cancelHelper.OnCancel -= myDelegate;
                    PortOpenHelper.Close(port, portSettings);
                }
            }
        }

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }

}
