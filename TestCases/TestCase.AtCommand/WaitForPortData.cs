﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using log4net;
using yats.Ports;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Threading;
using yats.Ports.Utilities;
using yats.Ports.Utilities.Parsers;

namespace TestCase.AtCommand
{
    [Description("Wait until a configured byte sequence is received to a port")]
    public class WaitForPortData : IYatsTestCase, ICancellable
    {
        internal static ILog Logger = LogManager.GetLogger("WaitForPortData");

        protected byte[] waitFor;
        //If null, will just wait and log incoming data until the specified timeout
        [CanBeNull]
        public byte[] WaitFor
        {
            get { return waitFor; }
            set { waitFor = value; }
        }

        protected TimeSpan timeout;
        public TimeSpan Timeout
        {
            get { return timeout; }
            set { timeout = value; }
        }

        protected PortSettings portSettings;
        public PortSettings Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        public WaitForPortData()
        {
        }
            
        public ITestResult Execute()
        {
            AbstractPort port = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;
                try
                {
                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);

                    var rx = new WaitForBytes(waitFor);

                    ReadOperationResult result = port.Execute((int)timeout.TotalMilliseconds, rx, cancelWaitEvent);

                    Logger.Debug(rx.GetReceivedData());

                    return ResultHelper.Convert(result);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cancelHelper.OnCancel -= myDelegate;
                    PortOpenHelper.Close(port, portSettings);
                }
            }
        }
        
        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }

}
