﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Threading;
using yats.Ports;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;

namespace TestCase.AtCommand
{
    [Description("Wait until a TCP/IP client disconnects or timeout. Can be cancelled")]
    public class WaitForClientDisconnect : IYatsTestCase, ICancellable
    {
        protected TimeSpan? timeout;
        [CanBeNull]
        public TimeSpan? Timeout
        {
            get { return timeout; }
            set { timeout = value; }
        }

        protected PortSettings portSettings;
        public PortSettings PortSettings
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        public ITestResult Execute()
        {
            bool isCanceled = false;

             AbstractPort port = null;
             using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
             {
                 EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); isCanceled = true; };
                 cancelHelper.OnCancel += myDelegate;
                 try
                 {
                     switch (portSettings.Selected)
                     {
                         case PortSettings.SelectedSetup.TCP_IP_SERVER:
                             port = PortReusePool.OpenTcpIpServer(portSettings.TcpIpServer.PortNumber, 0, portSettings.TcpIpServer.ReplaceSocket, cancelWaitEvent, true, portSettings.TcpIpServer.KeepAlive);
                             break;
                         default:
                             throw new Exception("Only TCP/IP client disconnect wait is supported");
                     }
                     if (port == null)
                     {
                         return new TestResult(true);
                     }

                     DateTime timeoutTime = DateTime.MaxValue;
                     if (timeout.HasValue)
                     {
                         timeoutTime = DateTime.Now.Add(timeout.Value);
                     }

                     while (DateTime.Now < timeoutTime && port.IsOpen() && !isCanceled)
                     {
                         Thread.Sleep(100);
                     }

                     return new TestResult(port.IsOpen() == false);
                 }
                 catch
                 {
                     throw;
                 }
                 finally
                 {
                     cancelHelper.OnCancel -= myDelegate;
                 }
             }
        }

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }
    }
}
