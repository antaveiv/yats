﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using yats.Ports;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Threading;

namespace TestCase.AtCommand
{
#if FALSE
    [Description("Send a SMS via a serial modem, uses AT+CMGS command")]
    public class SendSms : IYatsTestCase, ICancellable
    {
        protected string number;
        public string Number
        {
            get { return number; }
            set { number = value; }
        }

        protected string text;
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        protected int timeout;
        public int Timeout
        {
            get { return timeout; }
            set { timeout = value; }
        }

        protected SerialPortSettingsParameter portSettings;
        public SerialPortSettingsParameter Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        public SendSms()
        {
        }

        public ITestResult Execute()
        {
            AbstractPort port = PortOpenHelper.Open(portSettings, cancelWaitEvent);
            
            Dictionary<string, ReadOperationResult> responses = new Dictionary<string, ReadOperationResult>();
            responses.Add("> ", new ReadOperationResult(ReadOperationResult.Result.SUCCESS));
            responses.Add("ERROR", new ReadOperationResult(ReadOperationResult.Result.ERROR));

            ReadOperationResult result = port.Command("AT+CMGS=\"" + number + "\"\r", new ModemResponseParser(responses), timeout, cancelWaitEvent);

            if (result.OperationResult == ReadOperationResult.Result.SUCCESS)
            {
                responses.Remove("> ");
                responses.Add("OK", new ReadOperationResult(ReadOperationResult.Result.SUCCESS));
                result = port.Command(text + "\x1A", new ModemResponseParser(responses), timeout, cancelWaitEvent);
            }

            PortOpenHelper.Close(port);

            return ResultHelper.Convert(result);
        }

    #region ICancellable Members

        ManualResetEvent cancelWaitEvent = new ManualResetEvent(false);

        public void Cancel()
        {
            cancelWaitEvent.Set();
        }

    #endregion
    }
#endif
}
