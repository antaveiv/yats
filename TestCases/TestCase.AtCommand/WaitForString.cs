﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using log4net;
using yats.Attributes;
using yats.Ports;
using yats.Ports.Utilities;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Threading;
using yats.Ports.Utilities.Parsers;

namespace TestCase.AtCommand
{
    [Description("Wait until a configured string is received to a port")]
    public class WaitForString : IYatsTestCase, ICancellable
    {
        internal static ILog Logger = LogManager.GetLogger("WaitForString");

        protected string waitFor;
        public string WaitFor
        {
            get { return waitFor; }
            set { waitFor = value; }
        }

        protected TimeSpan timeout;
        public TimeSpan Timeout
        {
            get { return timeout; }
            set { timeout = value; }
        }

        protected PortSettings portSettings;
        public PortSettings Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        [Result]
        public string ReceivedData;

        public ITestResult Execute()
        {
            AbstractPort port = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;

                try
                {
                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);

                    Dictionary<string, ReadOperationResult> responses = new Dictionary<string, ReadOperationResult>();
                    responses.Add(waitFor, new ReadOperationResult(ReadOperationResult.Result.SUCCESS));

                    var rx = new ModemResponseParser(responses);

                    ReadOperationResult result = port.Execute(timeout, rx, cancelWaitEvent);

                    if (result.PortException != null)
                    {
                        Logger.Warn("Exception", result.PortException);
                        return new TestResult(ResultEnum.INCONCLUSIVE);
                    }

                    if (result.SerialPortError.HasValue)
                    {
                        Logger.Warn("Serial port error: " + result.SerialPortError.Value);
                        return new TestResult(ResultEnum.INCONCLUSIVE);
                    }

                    ReceivedData = rx.GetModemResponse();

                    return ResultHelper.Convert(result);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cancelHelper.OnCancel -= myDelegate;
                    PortOpenHelper.Close(port, portSettings);
                }
            }
        }

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }
}
