﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using yats.Ports;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Threading;
using yats.Ports.Utilities;
using yats.Ports.Utilities.Parsers;
using log4net;

namespace TestCase.AtCommand
{
#if DEBUG
    [Description("Writes the configured AT command to port. Success if OK is received within given timeout, fail if ERROR is received or command timeouts. Can be cancelled.")]
    public class AtCommand : IYatsTestCase, ICancellable
    {
        protected string command;
        [Description("AT command. <CR><LF> is appended automatically if missing")]
        public string Command
        {
            get { return command; }
            set { command = value; }
        }

        protected TimeSpan timeout;
        public TimeSpan Timeout
        {
            get { return timeout; }
            set { timeout = value; }
        }

        protected PortSettings portSettings;
        public PortSettings Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        [Result]
        public string ModemResponse;

        [Parameter, CanBeNull]
        [Description("Additional OK response. If not set, will wait for 'OK'")]
        public string ExpectedResponse;

        public AtCommand()
        {
        }

        public ITestResult Execute()
        {
            AbstractPort port = null;
            this.ModemResponse = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;
                try
                {
                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);

                    Dictionary<string, ReadOperationResult> responses = new Dictionary<string, ReadOperationResult>();

                    if (string.IsNullOrEmpty(ExpectedResponse))
                    {
                        responses.Add("OK", new ReadOperationResult(ReadOperationResult.Result.SUCCESS));
                    }
                    else
                    {
                        responses.Add(ExpectedResponse, new ReadOperationResult(ReadOperationResult.Result.SUCCESS));
                    }

                    responses.Add("ERROR", new ReadOperationResult(ReadOperationResult.Result.ERROR));
                    LogManager.GetLogger("Command").Debug(command);
                    if (command.EndsWith("\r\n") == false)
                    {
                        command += "\r\n";
                    }
                    var parser = new ModemResponseParser(responses);
                    ReadOperationResult result = port.Command(command, parser, timeout, cancelWaitEvent);
                    string modemResponse = parser.GetModemResponse();
                    if (string.IsNullOrEmpty(modemResponse) == false)
                    {
                        foreach (var c in command)
                        {
                            if (modemResponse.Length > 0 && c == modemResponse[0])
                            {
                                modemResponse = modemResponse.Substring(1);
                            }
                        }
                        modemResponse = modemResponse.TrimStart('\r', '\n');
                        modemResponse = modemResponse.TrimEnd('\r', '\n');
                    }
                    LogManager.GetLogger("Response").Debug(modemResponse);
                    PortOpenHelper.Close(port, portSettings);
                    this.ModemResponse = modemResponse;
                    return ResultHelper.Convert(result);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cancelHelper.OnCancel -= myDelegate;
                    PortOpenHelper.Close(port, portSettings);
                }
            }
        }

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }
#endif
}
