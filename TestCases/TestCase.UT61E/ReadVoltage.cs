﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Attributes;
using yats.Ports;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Threading;
using System;
using yats.Ports.Utilities;
using yats.Ports.Utilities.Parsers;
using yats.Utilities;
using System.Linq;

namespace TestCase.UT61E
{
    [Description("Read a single voltage measurement from a UNI-T UT61E multimeter")]
    public class ReadVoltage : IYatsTestCase, ICancellable
    {
        protected PortSettings portSettings;
        public PortSettings Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }
                
        [Result]
        [Description("Parsed value. Can be null if the meter is showing overload or other errors")]
        public float? MeasuredValue;

        [Parameter]
        [CanBeNull]
        [Description("If set, the test will fail if measured value is outside min..max")]
        public float? ExpectedMin { get; set; }

        [Parameter]
        [CanBeNull]
        [Description("If set, the test will fail if measured value is outside min..max")]
        public float? ExpectedMax { get; set; }

        public ReadVoltage()
        {
            //initializing default port settings so that only port number is left to configure
            this.portSettings = new PortSettings(new PortSettings.SerialPortInfo("COM1", 19200, 7, System.IO.Ports.Parity.Odd));
            this.portSettings.SerialPort.DtrEnable = true; // power on the optocoupler
            this.portSettings.SerialPort.RtsEnable = yats.TestCase.Parameter.RtsEnableEnum.OFF;
        }

        // call this from other tests or applications
        public static ReadOperationResult Measure(AbstractPort port, WaitHandle cancelWaitEvent, out float measuredValue)
        {
            measuredValue = 0;
            TextLineParser rx = new TextLineParser();
            ReadOperationResult result = port.Execute(1000, rx, cancelWaitEvent);
            if (result.OperationResult == ReadOperationResult.Result.SUCCESS)
            {
                byte[] packet = rx.GetReceivedData();
                byte range = packet[0];
                byte[] digits = packet.Slice(1, 5);
                byte function = packet[6];
                byte status = packet[7];
                byte option1 = packet[8];
                byte option2 = packet[9];
                byte option3 = packet[10];
                byte option4 = packet[11];
                if (function != 0b0111011)
                {
                    throw new Exception("Meter not in voltage measurement");
                }
                float rangeMultiplier = 1;
                if (range == 0b0110000)
                {
                    rangeMultiplier = 1;
                }
                if (range == 0b0110001)
                {
                    rangeMultiplier = 10;
                }
                if (range == 0b0110010)
                {
                    rangeMultiplier = 100;
                }
                if (range == 0b0110011)
                {
                    rangeMultiplier = 1000;
                }
                if (range == 0b0110100)
                {
                    rangeMultiplier = 0.1f;
                }
                measuredValue = float.Parse(ByteArray.ToString(digits)) / 10000.0f * rangeMultiplier;
            }
            return result;
        }

        #region IYatsTestCase Members

        public ITestResult Execute()
        {
            AbstractPort port = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;
                try
                {
                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);
                    float measuredValue;
                    var result = Measure(port, cancelWaitEvent, out measuredValue);
                    this.MeasuredValue = measuredValue;
                    if (ExpectedMin.HasValue)
                    {
                        if (MeasuredValue < ExpectedMin.Value)
                        {
                            return new TestResult(ResultEnum.FAIL);
                        }
                    }

                    if (ExpectedMax.HasValue)
                    {
                        if (MeasuredValue > ExpectedMax.Value)
                        {
                            return new TestResult(ResultEnum.FAIL);
                        }
                    }
                    return ResultHelper.Convert(result);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cancelHelper.OnCancel -= myDelegate;
                    PortOpenHelper.Close(port, portSettings);
                }
            }
        }

        #endregion

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }
}
