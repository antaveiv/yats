﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Threading;
using log4net;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;

namespace yats.TestCase.Yats
{
#if DEBUG
    public enum TestEnum
    {
        ONE,
        TWO,
        THREE
    }

    public class ParameterTypeTest : IYatsTestCase
    {
        public byte DONT_FIND_ME_1;
        
        [Parameter]
        [Result]
        
        public byte DONT_FIND_ME_2;
        [Parameter]
        
        public string[] StringArrayFieldParameter = new string [] {"a", "b"};
        
        [Result]
        public string[] StringArrayFieldResult;

        [Parameter]
        [CanBeNull]
        public string StringFieldParameterCanBeNull;
        
        [Parameter]
        public string StringFieldParameterCanNotBeNull = "test";

        protected bool? boolParameter;
        public bool? BoolParameter
        {
            get {return boolParameter;}
            set {boolParameter = value;}
        }

        //DO NOT REMOVE - used by unit tests
        protected byte[] byteArrayParameter = new byte[] { 1, 2, 3 };
        [CanBeNull]
        public byte[] ByteArrayParameter
        {
            get {return byteArrayParameter;}
            set {byteArrayParameter = value;}
        }

        //DO NOT REMOVE - used by unit tests
        protected List<byte> byteListParameter;
        [CanBeNull]
        public List<byte> ByteListParameter
        {
            get { return byteListParameter; }
            set { byteListParameter = value; }
        }

        protected List<byte[]> listOfByteArrayParameter;
        [CanBeNull]
        public List<byte[]> ListOfByteArrayParameter
        {
            get { return listOfByteArrayParameter; }
            set { listOfByteArrayParameter = value; }
        }

        protected byte[] byteArrayResult;
        public byte[] ByteArrayResult
        {
            get { return byteArrayResult; }
        }

        protected byte? nullableByteParameter = 42;
        public byte? NullableByteParameter 
        {
            get { return nullableByteParameter; }
            set { nullableByteParameter = value; }
        }

        protected byte byteParameter = 42;
        public byte ByteParameter
        {
            get { return byteParameter; }
            set { byteParameter = value; }
        }

        [Parameter, CanBeNull]
        public sbyte[] SignedByteArray;

        protected int? intParameter = 3;
        public int? IntParameter
        {
            get {return intParameter;}
            set {intParameter = value;}
        }

        protected sbyte? sByteParameter = -112;
        public sbyte? SByteParameter
        {
            get { return sByteParameter; }
            set { sByteParameter = value; }
        }

        protected uint? uIntParameter = 19131;
        public uint? UIntParameter
        {
            get { return uIntParameter; }
            set { uIntParameter = value; }
        }

        protected UInt16 uInt16Parameter = 430;
        [CanBeNull]
        public UInt16 UInt16Parameter
        {
            get { return uInt16Parameter; }
            set { uInt16Parameter = value; }
        }

        protected IPAddress ip;
        [CanBeNull]
        public IPAddress IP
        {
            get { return ip; }
            set { ip = value; }
        }

        protected string stringParameter = "default";
        [CanBeNull]
        public string StringParameter
        {
            get {return stringParameter;}
            set {stringParameter = value;}
        }

        protected string[] stringArray = new string[] { "one", "two", "file not found" };
        [CanBeNull]
        public string[] StringArrayParameter
        {
            get { return stringArray; }
            set { stringArray = value; }
        }

        protected List<string> stringList;
        [CanBeNull]
        public List<string> StringListParameter
        {
            get { return stringList; }
            set { stringList = value; }
        }

        protected List<string> stringListResult;
        public List<string> StringListResult
        {
            get { return stringListResult; }
        }

        protected bool[] boolArray = new bool[] { false, true, false };
        [CanBeNull]
        public bool[] BoolArrayParameter
        {
            get { return boolArray; }
            set { boolArray= value; }
        }

        protected int[] intArray = new int[] { -1, 0, 1 };
        [CanBeNull]
        public int[] IntArrayParameter
        {
            get { return intArray; }
            set { intArray= value; }
        }

        protected PortSettings serialPortParameter;
        [CanBeNull]
        public PortSettings SerialPortParameter
        {
            get { return serialPortParameter; }
            set { serialPortParameter = value; }
        }
        
        protected TestEnum enumParameter = TestEnum.THREE;
        [CanBeNull]
        public TestEnum EnumParameter
        {
            get { return enumParameter; }
            set { enumParameter = value; }
        }

        protected TestEnum [] enumParameter2 = new TestEnum[] {TestEnum.TWO, TestEnum.THREE};
        [CanBeNull]
        public TestEnum [] EnumParameterArray
        {
            get { return enumParameter2; }
            set { enumParameter2 = value; }
        }

        protected DateTime dateTimeParameter;
        [CanBeNull]
        public DateTime DateTimeParameter
        {
            get { return dateTimeParameter; }
            set { dateTimeParameter = value; }
        }

        protected DateTime? dateTimeParameter2 = DateTime.Now;
        public DateTime? DateTimeParameter2
        {
            get { return dateTimeParameter2; }
            set { dateTimeParameter2 = value; }
        }

        protected TimeSpan timespanParameter = TimeSpan.FromHours(3);
        [CanBeNull]
        public TimeSpan TimespanParameter
        {
            get { return timespanParameter; }
            set { timespanParameter = value; }
        }

        protected TimeSpan? timespanParameter2 = TimeSpan.FromHours(3);
        public TimeSpan? TimespanParameterNullable
        {
            get { return timespanParameter2; }
            set { timespanParameter2 = value; }
        }

        protected Regex regexParameter;
        [CanBeNull]
        public Regex RegexParameter
        {
            get { return regexParameter; }
            set { regexParameter = value; }
        }

        protected float floatParameter;
        [CanBeNull]
        public float FloatParameter
        {
            get { return floatParameter; }
            set { floatParameter = value; }
        }

        protected float[] floatArrayParameter = new float[] { 1, -1e12f };
        [CanBeNull]
        public float [] FloatArrayParameter
        {
            get { return floatArrayParameter; }
            set { floatArrayParameter = value; }
        }

        protected double doubleParameter = 3.1456;
        [CanBeNull]
        public double DoubleParameter
        {
            get { return doubleParameter; }
            set { doubleParameter = value; }
        }

        protected EndpointAddress endpointAddressParameter;
        [CanBeNull]
        public EndpointAddress EndpointAddress
        {
            get { return endpointAddressParameter; }
            set { endpointAddressParameter = value; }
        }

        protected FilenameParameter fileNameParameter;
        [CanBeNull]
        public FilenameParameter Filename
        {
            get { return fileNameParameter; }
            set { fileNameParameter = value; }
        }

        protected FilenameParameter existingFileName = new FilenameParameter(true);
        [CanBeNull]
        public FilenameParameter ExistingFileName
        {
            get { return existingFileName; }
            set { existingFileName = value; }
        }
        
        public ParameterTypeTest()
        {
            stringList = new List<string>();
            stringList.Add("This");
            stringList.Add("is");
            stringList.Add("the default value");
            stringList.Add("from constructor");
        }

        #region IYatsTestCase Members

        public ITestResult Execute()
        {
            byteArrayResult = byteArrayParameter;
            stringListResult = stringList;
            ILog Logger = LogManager.GetLogger(this.GetType());
            Logger.Info(byteArrayParameter);
            Logger.Warn(stringList);
            
            return new TestResult(ResultEnum.PASS);
        }

        #endregion
    }

    public class EnumParameterTest : IYatsTestCase
    {
        protected TestEnum enumParameter;
        public TestEnum EnumParameter
        {
            get { return enumParameter; }
            set { enumParameter = value; }
        }

        public EnumParameterTest()
        {
        }

        #region IYatsTestCase Members

        public ITestResult Execute()
        {
            return new TestResult( ResultEnum.PASS );
        }

        #endregion
    }
    
    public class WaitUntilParamSet : IYatsTestCase, ICancellable
    {
        protected int timeoutMs;
        public int TimeoutMs
        {
            get { return timeoutMs; }
            set
            {
                timeoutMs = value;
            }
        }

        public object CancelObject
        {
            set
            {
                cancelHelper.Cancel();
            }
            get
            {
                return null;
            }
        }

        #region IYatsTestCase Members
        
        private CancelHelper cancelHelper = new CancelHelper();

        public ITestResult Execute()
        {
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;
                try
                {
                    cancelWaitEvent.WaitOne(timeoutMs);
                    return new TestResult(ResultEnum.PASS);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cancelHelper.OnCancel -= myDelegate;
                }
            }
        }

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }

    public class ResultTest : IYatsTestCase
    {
        private PortSettings m_result;
        private PortSettings m_parameter;

        [CanBeNull]
        public PortSettings Result {
            get {return m_result;}
        }

        [CanBeNull]
        public PortSettings SerialPortParameter
        {
            get { return m_parameter; }
            set { m_parameter = value; }
        }

        public ITestResult Execute()
        {
            m_result = m_parameter;
            ILog Logger = LogManager.GetLogger( this.GetType( ) );
            Logger.Info( "Info print" );
            return new TestResultWithData(ResultEnum.PASS, "attached string");
        }
    }

    public class ResultTestWithCrash : IYatsTestCase
    {
        private int m_result;
        private int m_parameter;

        [CanBeNull]
        public int Result
        {
            get { throw new NullReferenceException(); }
        }

        [CanBeNull]
        public int Parameter
        {
            get { return m_parameter; }
            set { m_parameter = value; }
        }

        public ITestResult Execute()
        {
            m_result = m_parameter;
            return new TestResult( ResultEnum.PASS );
        }
    }


    public class CheckEquals : IYatsTestCase
    {
        private object m_param1;
        private object m_param2;

        [CanBeNull]
        public object A
        {
            get { return m_param1; }
            set { m_param1 = value; }
        }

        [CanBeNull]
        public object B
        {
            get { return m_param2; }
            set { m_param2 = value; }
        }

        public ITestResult Execute()
        {
            if (object.Equals(m_param1, m_param2))
            {
                return new TestResult(ResultEnum.PASS);
            }
            return new TestResult(ResultEnum.FAIL);
        }

    }
#endif
}
