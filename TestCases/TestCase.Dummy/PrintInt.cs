﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using log4net;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.YatsNativeTestCase;

namespace yats.TestCase.Yats
{
#if DEBUG
    public class PrintInt : IYatsTestCase
    {

        private int m_number;
        public int Number
        {
            get { return m_number; }
            set { m_number = value; }
        }

        public ITestResult Execute()
        {
            LogManager.GetLogger("PrintInt").Error(m_number.ToString());
            return new TestResult(ResultEnum.PASS);
        }
    }
#endif
}
