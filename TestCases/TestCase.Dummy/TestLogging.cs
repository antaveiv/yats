﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Text;
using log4net;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.YatsNativeTestCase;
using yats.Attributes;

namespace yats.TestCase.Yats
{
#if DEBUG
	public class TestLogging : IYatsTestCase
	{
        [Result]
        public int IntegerResult = -1921;

        [Result]
        public byte ByteResult = 16;

        [Result]
        public List<string> StringListResult = new List<string>();
        
        [Result]
        public Dictionary<string, int> DictionaryResult = new Dictionary<string,int>();

        [Result]
        public int[] IntArrayResult = new int[] { 1, 2, 300 };

        [Result]
        public byte[] ByteArrayResult = new byte[] { 1, 2, 255, 0, 1 };

        [Result]
        public List<int> ListResult = new List<int>(new int[] { 1, 2, 300 });

        [Result]
        public string NullStringResult = null;

        [Parameter, UseDefault]
        public int IntegerParameter = -1921;
        
        [Parameter, UseDefault]
        public decimal DecimalParameter = -1921;

        [Parameter, UseDefault]
        public byte ByteParameter = 16;

        [Parameter, UseDefault]
        public List<string> StringListParameter = new List<string>();

        [Parameter, UseDefault]
        public int[] IntArrayParameter = new int[] { 1, 2, 300 };

        [Parameter, UseDefault]
        public float[] FloatArrayParameter;

        [Parameter, UseDefault]
        public decimal?[] DecimalArrayParameter;

        [Parameter, UseDefault]
        public byte[] ByteArrayParameter = new byte[] { 1, 2, 255, 0, 1 };

        [Parameter, UseDefault]
        public List<int> ListParameter = new List<int>(new int[] { 1, 2, 300 });

        [Parameter, CanBeNull]
        public string NullStringParameter = null;

        public TestLogging()
		{
            StringListResult.Add("AaA");
            StringListResult.Add("CCC2");

            DictionaryResult.Add("true", 1);
            DictionaryResult.Add("false", 2);
            DictionaryResult.Add("file not found", -1);
		}

        public class TestClass
        {
            string longString = @"- 520: Parameter value is shown as ing Export test run log to a single XML file - new.";
            public string LongString
            {
                get
                {
                    return longString;
                }
                set 
                {
                    longString = value;
                }
            }

            public int integer { get { return -200194885; } set { } }
            public int[] arrayOfInts { get { return new int [] {1, 2, 3, 6, -200194885}; } set { } }

            public class InternalClass
            {
                public string LongString
                {
                    get
                    {
                        return @"- 520ameter value is shown asuse test resulew.";
                    }
                    set { }
                }
            }

            public InternalClass Test
            {
                get { return new InternalClass(); }
                set { }
            }

            public byte[] binary { get { return new byte[] { 0xFF, 0x01, 0x00, 0x0a, 0x80}; } set { } }
        }

        #region IYatsTestCase Members

        public ITestResult Execute()
        {

            ILog Logger = LogManager.GetLogger(this.GetType());
            byte bt = 16;
            Logger.Info(bt);
            var list = new List<TestClass>();
            list.Add(new TestClass());
            list.Add(new TestClass());
            Logger.Info(list);
            Logger.Debug("test debug");
            Logger.Info("test info");
            Logger.Warn("test warn");
            System.Threading.Thread.Sleep( 10 );
            Logger.Error("test error");
            Logger.Fatal("test fatal");
            Logger.Fatal(Encoding.ASCII.GetString(new byte[]{0x35, 0x02, 0x00, 60, 0x10, 0x13, 65}));
            System.Threading.Thread.Sleep( 10 );
            Exception inner = new NullReferenceException();
            try
            {
                throw new Exception( "outer", inner );
            }
            catch (Exception ex)
            {
                Logger.Fatal( ex);
                Logger.Fatal( "log exception: ", ex );
            }

            Logger.Info( System.Text.Encoding.UTF8.GetBytes( "Laba diena\x04" ) );
            var tc = new TestClass();
            Logger.Debug(tc);
            tc.LongString = "pakeiciau";
            Logger.Debug(tc);
            return new TestResult( ResultEnum.PASS );
        }

        #endregion
    }
#endif
}

