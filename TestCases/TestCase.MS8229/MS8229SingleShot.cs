﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using log4net;
using yats.Attributes;
using yats.Ports;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Threading;
using System;
using yats.Ports.Utilities;

namespace yats.TestCase.MS8229
{
    [Description("Read a single measurement from a Mastech MS8229 multimeter")]
    public class MS8229SingleShot : IYatsTestCase, ICancellable
    {
        protected PortSettings portSettings;
        public PortSettings Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        [Result]
        [Description("Parsed display data object, contains units, error messages etc")]
        public FS9721_LP3_Parser Result;

        [Result]
        [Description("Parsed value. Can be null if the meter is showing overload or other errors")]
        public float? MeasuredValue;

        public MS8229SingleShot()
        {
            this.portSettings = new PortSettings(new PortSettings.SerialPortInfo("COM1", 2400, 8, System.IO.Ports.Parity.None));
            this.portSettings.SerialPort.DtrEnable = true; // power on the optocoupler
            this.portSettings.SerialPort.RtsEnable = yats.TestCase.Parameter.RtsEnableEnum.OFF;
        }

        #region IYatsTestCase Members
        
        public ITestResult Execute()
        {
             AbstractPort port = null;
             using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
             {
                 EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                 cancelHelper.OnCancel += myDelegate;
                 try
                 {
                     port = PortOpenHelper.Open(portSettings, cancelWaitEvent);

                     FS9721_LP3_Parser rx = new FS9721_LP3_Parser();

                     ReadOperationResult result = port.Execute(400, rx, cancelWaitEvent);
                     LogManager.GetLogger("Measurement").Info(rx.ToString());

                     this.Result = rx;
                    this.MeasuredValue = rx.MeasuredValue;

                     return ResultHelper.Convert(result);
                 }
                 catch
                 {
                     throw;
                 }
                 finally
                 {
                     cancelHelper.OnCancel -= myDelegate;
                     PortOpenHelper.Close(port, portSettings);
                 }
             }
        }

        #endregion

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }
}
