﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Linq;
using yats.Attributes;
using yats.Ports;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using yats.Utilities;
using System.Threading;
using System;
using yats.Ports.Utilities;

namespace yats.TestCase.MS8229
{
    [Description("Read multiple measurements from a Mastech MS8229 multimeter. Calculate statistics as test results")]
    public class MS8229MultiShot : IYatsTestCase, ICancellable
    {
        protected PortSettings portSettings;
        public PortSettings Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }
        
        //until cancelled if not set
        protected int? numMeasurements;
        public int? NumMeasurements
        {
            get { return numMeasurements; }
            set { numMeasurements = value; }
        }
        
        public float? Min
        {
            get
            {
                if (readings != null && readings.Count > 0)
                {
                    return readings.Min();
                }
                return null;
            }
        }

        public float? Max
        {
            get
            {
                if (readings != null && readings.Count > 0)
                {
                    return readings.Max();
                }
                return null;
            }
        }

        public float? Mean
        {
            get
            {
                if (readings != null && readings.Count > 0)
                {
                    return readings.Mean();
                }
                return null;
            }
        }

        protected List<float> readings = new List<float>();
        public List<float> Readings
        {
            get { return readings; }
        }

        public float? StandardDeviation
        {
            get
            {
                if (readings != null && readings.Count > 0)
                {
                    return readings.StandardDeviation();
                }
                return null;
            }
        }

        public int NumSuccessfulReads
        {
            get { return readings.Count; }
        }
        
        [Result]
        [Description("Parsed display data object, contains units, error messages etc")]
        public FS9721_LP3_Parser LastResult;

        [Result]
        [Description("Parsed value. Can be null if the meter is showing overload or other errors")]
        public float? LastMeasuredValue;

        public MS8229MultiShot()
        {
            this.portSettings = new PortSettings(new PortSettings.SerialPortInfo("COM1", 2400, 8, System.IO.Ports.Parity.None));
            this.portSettings.SerialPort.DtrEnable = true; // power on the optocoupler
            this.portSettings.SerialPort.RtsEnable = yats.TestCase.Parameter.RtsEnableEnum.OFF;
        }

        #region IYatsTestCase Members

        public ITestResult Execute()
        {
            AbstractPort port = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;

                try
                {
                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);

                    int reads = 0;
                    while (true)
                    {
                        FS9721_LP3_Parser rx = new FS9721_LP3_Parser();
                        ReadOperationResult result = port.Execute(400, rx, cancelWaitEvent);
                        this.LastResult = rx;
                        this.LastMeasuredValue = rx.MeasuredValue;

                        if (result.OperationResult == ReadOperationResult.Result.INTERNAL_ERROR || result.OperationResult == ReadOperationResult.Result.PORT_ERROR || result.OperationResult == ReadOperationResult.Result.CANCELLED)
                        {
                            break;
                        }

                        if (result.OperationResult == ReadOperationResult.Result.SUCCESS && string.IsNullOrEmpty(rx.Error) && rx.MeasuredValue.HasValue)
                        {
                            readings.Add(rx.MeasuredValue.Value);
                        }

                        reads++;
                        if (numMeasurements.HasValue && reads >= numMeasurements.Value)
                        {
                            break;
                        }
                    }
                    //LogManager.GetLogger("Measurement").Info(rx.ToString());

                    return new TestResult(readings.Count > 0);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    PortOpenHelper.Close(port, portSettings);
                    cancelHelper.OnCancel -= myDelegate;
                }
            }
        }

        #endregion

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }
}
