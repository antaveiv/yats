﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Linq;
using NUnit.Framework;
using yats.Ports;
using yats.Utilities;

namespace yats.TestCase.MS8229
{
    // TODO:
    // detect measurement mode, test all modes, also "Select" button
    // read error codes, RTFM
    // for each measurement mode generate a readable string property

    public class FS9721_LP3_Parser : Parser
    {
        private byte[] packet = new byte[14];
        private int state = 0;
        private const int LAST_STATE = 14;
        #region Parser interface
        public override bool IncomingData(byte dataByte)
        {
            if (dataByte >> 4 == (state + 1)) // packets start with 0x1 in 4 MSB, 4 LSB contain LCD segment data
            {
                packet[state] = dataByte;
                state++;
            }
            else
            {
                state = 0;
            }

            return state == LAST_STATE;
        }

        public override ReadOperationResult GetResult()
        {
            if (state == LAST_STATE)
            {
                return new ReadOperationResult(ReadOperationResult.Result.SUCCESS);
            }
            else
            {
                return new ReadOperationResult(ReadOperationResult.Result.TIMEOUT);
            }
        }

        public override byte[] GetReceivedData()
        {
            return yats.Utilities.ByteArray.Copy(packet, 0, state);
        }

        #endregion

        public bool AutoRange
        {
            get { return (packet[0] & 0x02) != 0; }
        }

        public bool DC
        {
            get { return (packet[0] & 0x04) != 0; }
        }

        public bool AC
        {
            get { return (packet[0] & 0x08) != 0; }
        }

        public bool LowBatteryWarning
        {
            get { return (packet[12] & 0x01) != 0; }
        }

        public bool LCDC1
        {
            get { return ((packet[13] & 0x02) != 0) || ((packet[13] & 0x08) != 0); }
        }

        public bool LCDC2
        {
            get { return ((packet[13] & 0x04) != 0) || ((packet[13] & 0x08) != 0); }
        }

        public bool IsDiodeMeasurement
        {
            get { return (packet[9] & 0x01) != 0; }
        }

        public bool IsContinuityMeasurement
        {
            get { return (packet[10] & 0x01) != 0; }
        }

        public bool IsRelativeMeasurement
        {
            get { return (packet[11] & 0x02) != 0; }
        }

        public bool IsHoldingMeasurement
        {
            get { return (packet[11] & 0x01) != 0; }
        }

        public string UnitPrefix
        {
            get
            {
                if ((packet[9] & 0x04) != 0)
                {
                    return "n"; // nano
                }
                else if ((packet[9] & 0x08) != 0)
                {
                    return "µ"; // micro
                }
                else if ((packet[10] & 0x08) != 0)
                {
                    return "m"; // milli
                }
                else if ((packet[9] & 0x02) != 0)
                {
                    return "k"; // kilo
                }
                else if ((packet[10] & 0x02) != 0)
                {
                    return "M"; //mega
                }
                return "";
            }
        }

        public enum MeasurementModeEnum
        {
            V,
            A,
            Hz,
            Ohm,
            F,
            Percent,

            UNKNOWN_MODE = 43
        }

        public MeasurementModeEnum MeasurementMode
        {
            get
            {
                MeasurementModeEnum res = MeasurementModeEnum.UNKNOWN_MODE;
                if ((packet[12] & 0x04) != 0)
                {
                    res = MeasurementModeEnum.V;
                }
                else if ((packet[12] & 0x08) != 0)
                {
                    res = MeasurementModeEnum.A;
                }
                else if ((packet[12] & 0x02) != 0)
                {
                    res = MeasurementModeEnum.Hz;
                }
                else if ((packet[11] & 0x04) != 0)
                {
                    res = MeasurementModeEnum.Ohm;
                }
                else if ((packet[11] & 0x08) != 0)
                {
                    res = MeasurementModeEnum.F;
                }

                return res;
            }
        }

        static char[] digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        const int NUM_DIGITS = 4;

        // takes into account unit prefix
        public float? MeasuredValue
        {
            get
            {
                float res = 0;

                for (int i = 0; i < NUM_DIGITS; i++)
                {
                    char digit = GetDigit(i);
                    if (digit == ' ')
                    {
                        continue;
                    }
                    if (digits.Contains(digit) == false)
                    {
                        return null;
                    }
                    res = res * 10 + digit - '0';
                }

                if ((packet[3] & 0x08) != 0)
                {
                    res /= 1000;
                }
                else if ((packet[5] & 0x08) != 0)
                {
                    res /= 100;
                }
                else if ((packet[7] & 0x08) != 0)
                {
                    res /= 10;
                }

                if ((packet[1] & 0x08) != 0)
                {
                    res = -res; // sign
                }

                //scaler
                if (UnitPrefix == "n")
                {
                    res /= 1000000000;
                }
                else if (UnitPrefix == "µ")
                {
                    res /= 1000000;
                }
                else if (UnitPrefix == "m")
                {
                    res /= 1000;
                }
                else if (UnitPrefix == "k")
                {
                    res *= 1000;
                }
                else if (UnitPrefix == "M")
                {
                    res *= 1000000;
                }

                return res;
            }
        }

        public string Error
        {
            get
            {
                string display = string.Empty;
                for (int i = 0; i < NUM_DIGITS; i++)
                {
                    char digit = GetDigit(i);
                    display += digit;
                }
                if (display.Contains("0L"))
                {
                    return "Overload";
                }
                return null;
            }
        }

        private char GetDigit(int digitIndex)
        {
            int segmentIndex = 1 + digitIndex * 2;
            bool s1 = (packet[segmentIndex] & 0x01) != 0;
            bool s6 = (packet[segmentIndex] & 0x02) != 0;
            bool s5 = (packet[segmentIndex] & 0x04) != 0;
            bool s2 = (packet[segmentIndex + 1] & 0x01) != 0;
            bool s7 = (packet[segmentIndex + 1] & 0x02) != 0;
            bool s3 = (packet[segmentIndex + 1] & 0x04) != 0;
            bool s4 = (packet[segmentIndex + 1] & 0x08) != 0;

            if (!s1 && s2 && s3 && !s4 && !s5 && !s6 && !s7)
            {
                return '1';
            }

            if (s1 && s2 && !s3 && s4 && s5 && !s6 && s7)
            {
                return '2';
            }

            if (s1 && s2 && s3 && s4 && !s5 && !s6 && s7)
            {
                return '3';
            }

            if (!s1 && s2 && s3 && !s4 && !s5 && s6 && s7)
            {
                return '4';
            }

            if (s1 && !s2 && s3 && s4 && !s5 && s6 && s7)
            {
                return '5';
            }

            if (s1 && !s2 && s3 && s4 && s5 && s6 && s7)
            {
                return '6';
            }

            if (s1 && s2 && s3 && !s4 && !s5 && !s6 && !s7)
            {
                return '7';
            }

            if (s1 && s2 && s3 && s4 && s5 && s6 && s7)
            {
                return '8';
            }

            if (s1 && s2 && s3 && s4 && !s5 && s6 && s7)
            {
                return '9';
            }

            if (s1 && s2 && s3 && s4 && s5 && s6 && !s7)
            {
                return '0';
            }

            if (!s1 && !s2 && !s3 && s4 && s5 && s6 && !s7)
            {
                return 'L';
            }

            if (!s1 && !s2 && !s3 && !s4 && s5 && s6 && !s7)
            {
                return 'I';
            }

            if (!s1 && !s2 && s3 && s4 && s5 && !s6 && s7)
            {
                return 'o';
            }

            if (s1 && !s2 && !s3 && !s4 && s5 && s6 && s7)
            {
                return 'F';
            }

            if (s1 && !s2 && !s3 && s4 && s5 && s6 && s7)
            {
                return 'E';
            }

            if (!s1 && !s2 && !s3 && !s4 && !s5 && !s6 && !s7)
            {
                return ' ';
            }

            throw new Exception("Not a digit");
        }

        public override string ToString()
        {
            string result = "";
            if (MeasuredValue.HasValue)
            {
                result += MeasuredValue.Value.ToString("0.000 ");
                if (MeasurementMode != MeasurementModeEnum.UNKNOWN_MODE)
                {
                    result += MeasurementMode.ToString() + " ";
                }
                if (AC)
                {
                    result += "AC ";
                }
                if (DC)
                {
                    result += "DC ";
                }

                if (IsHoldingMeasurement)
                {
                    result += "HOLD ";
                }
                if (IsRelativeMeasurement)
                {
                    result += "REL ";
                }
                if (IsDiodeMeasurement)
                {
                    result += "Diode ";
                }

                if (IsContinuityMeasurement)
                {
                    result += "Continuity ";
                }
            }
            if (string.IsNullOrEmpty(Error) == false)
            {
                result += Error + " ";
            }
            else
            {
                if (!AutoRange)
                {
                    result += "Manual range ";
                }
                if (LowBatteryWarning)
                {
                    result += ", Battery low ";
                }
            }
            return result;
        }

        public string RawData
        {
            get
            {
                if (state == LAST_STATE)
                {
                    return packet.ToHexString(" ");
                }
                return ByteArray.Copy(packet, 0, state).ToHexString(" ");
            }
        }
    }

    [TestFixture]
    public class MS8229Tests
    {
        [Test]
        public void ParseVoltage()
        {
            FS9721_LP3_Parser parser = new FS9721_LP3_Parser();
            byte[] bytes = new byte[] {0xD0, 
				0xE0, // garbage
				0x17, // RS232, Auto, DC
				0x29,
				0x35,  // -7
				0x45,
				0x5B, // 2
				0x6A,
				0x77, // .4
				0x87,
				0x9E, // 6
				0xA2, //k
				0xB0,
				0xC1, // Hold
				0xD4, // Volt
				0xE0
			};

            foreach (byte bt in bytes)
            {
                if (parser.IncomingData(bt))
                {
                    break;
                }
            }
            Assert.IsNotNull(parser.MeasuredValue);
            Assert.IsNull(parser.Error);
            Assert.IsTrue(Math.Abs(parser.MeasuredValue.Value - -72460.0f) < 0.00001f);
            Assert.AreEqual(FS9721_LP3_Parser.MeasurementModeEnum.V, parser.MeasurementMode);
            Assert.AreEqual("k", parser.UnitPrefix);

            Assert.IsFalse(parser.AC);
            Assert.IsTrue(parser.AutoRange);
            Assert.IsTrue(parser.DC);
            Assert.IsFalse(parser.IsContinuityMeasurement);
            Assert.IsFalse(parser.IsDiodeMeasurement);
            Assert.IsTrue(parser.IsHoldingMeasurement);
            Assert.IsFalse(parser.IsRelativeMeasurement);
            Assert.IsFalse(parser.LCDC1);
            Assert.IsFalse(parser.LCDC2);
            Assert.IsFalse(parser.LowBatteryWarning);
        }

        [Test]
        public void ParseAmps()
        {
            FS9721_LP3_Parser parser = new FS9721_LP3_Parser();
            byte[] bytes = new byte[] {0xD0, 
				0xE0, // garbage
				0x19, // RS232, AC
				0x20,
				0x35,  // 1
				0x49,
				0x5F, // .3
				0x63,
				0x7E, // 5
				0x81,
				0x95, // 7
				0xA8, //micro
				0xB0,
				0xC2, // delta
				0xD9, // Amp + low battery
				0xE0
			};

            foreach (byte bt in bytes)
            {
                if (parser.IncomingData(bt))
                {
                    break;
                }
            }

            Assert.IsNotNull(parser.MeasuredValue);
            Assert.IsNull(parser.Error);
            Assert.IsTrue(Math.Abs(parser.MeasuredValue.Value - 0.000001357f) < 0.00000000001f);
            Assert.AreEqual(FS9721_LP3_Parser.MeasurementModeEnum.A, parser.MeasurementMode);
            Assert.AreEqual("µ", parser.UnitPrefix);

            Assert.IsTrue(parser.AC);
            Assert.IsFalse(parser.AutoRange);
            Assert.IsFalse(parser.DC);
            Assert.IsFalse(parser.IsContinuityMeasurement);
            Assert.IsFalse(parser.IsDiodeMeasurement);
            Assert.IsFalse(parser.IsHoldingMeasurement);
            Assert.IsTrue(parser.IsRelativeMeasurement);
            Assert.IsFalse(parser.LCDC1);
            Assert.IsFalse(parser.LCDC2);
            Assert.IsTrue(parser.LowBatteryWarning);
        }
    }

}
