﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using Renci.SshNet;
using System.IO;
using System.Globalization;

namespace TestCase.SSH
{
    [Description("File operations (SCP or SFTP protocols)")]
    public class FileTransfer
    {
        [TestMethod]
        [Description("Download a single file via SFTP")]
        public static ITestResult SftpDownload(string host, string username, string password, [Description("full path")] string remoteFileName, [Description("full path")] string localFileName)
        {
            using (var sftp = new SftpClient(host, username, password))
            {
                sftp.Connect();
                using (Stream file1 = File.OpenWrite(localFileName))
                {
                    sftp.DownloadFile(remoteFileName, file1);
                }
            }
            return new TestResult(true);
        }

        [TestMethod]
        [Description("Upload a single file via SFTP")]
        public static ITestResult SftpUpload(string host, string username, string password, [Description("full path")] string remoteFileName, [Description("full path")] string localFileName)
        {
            using (var sftp = new SftpClient(host, username, password))
            {
                sftp.Connect();
                using (FileStream filestream = File.OpenRead(localFileName))
                {
                    sftp.UploadFile(filestream, remoteFileName);
                    sftp.Disconnect();
                }
            }
            return new TestResult(true);
        }

        [TestMethod]
        [Description("Download a single file via SCP")]
        public static ITestResult ScpDownload(string host, string username, string password, [Description("full path")] string remoteFileName, [Description("full path")] string localFileName)
        {
            using (var scp = new ScpClient(host, username, password))
            {
                scp.Connect();
                using (Stream file1 = File.OpenWrite(localFileName))
                {
                    scp.Download(remoteFileName, file1);
                }
            }
            return new TestResult(true);
        }

        [TestMethod]
        [Description("Upload a single file via SCP")]
        public static ITestResult ScpUpload(string host, string username, string password, [Description("full path")] string remoteFileName, [Description("full path")] string localFileName)
        {
            using (var scp = new ScpClient(host, username, password))
            {
                scp.Connect();
                using (FileStream filestream = File.OpenRead(localFileName))
                {
                    scp.Upload(filestream, remoteFileName);
                    scp.Disconnect();
                }
            }
            return new TestResult(true);
        }

        //TODO SFTP upload download create delete directory
    }
}