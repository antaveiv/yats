﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using Renci.SshNet;
using System;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.YatsNativeTestCase;

namespace TestCase.SSH
{
    [Description("Execute a command via SSH")]
    public class SshCommand : IYatsTestCase
    {
        private string host;
        private string user;
        private string password;
        private string command;
        private TimeSpan timeout;

        public string Host
        {
            get { return host; }
            set { host = value; }
        }

        public string User
        {
            get { return user; }
            set { user = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public string Command
        {
            get { return command; }
            set { command = value; }
        }

        private string stdOut;
        public string StdOut
        {
            get { return stdOut; }
        }

        private int retValue;
        public int RetValue
        {
            get { return retValue; }
        }

        [CanBeNull]
        public TimeSpan Timeout
        {
            get { return timeout; }
            set { timeout = value; }
        }

        #region IYatsTestCase Members

        public ITestResult Execute()
        {
            SshClient sshclient = new SshClient(host, user, password);
            sshclient.Connect();
            Renci.SshNet.SshCommand sc = sshclient.CreateCommand(command);
            sc.CommandTimeout = timeout;
            sc.Execute();
            stdOut = sc.Result;
            retValue = sc.ExitStatus;
            return new TestResult(retValue == 0);
        }

        #endregion
    }
}
