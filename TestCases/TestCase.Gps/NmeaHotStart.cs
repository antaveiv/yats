﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Attributes;
using yats.Ports;
using yats.Ports.Utilities;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using log4net;
using System;
using System.Globalization;
using System.Threading;
using System.Linq;
using System.Diagnostics;
using System.Text;

namespace TestCase.Gps
{
    public class NmeaHotStart : IYatsTestCase
    {
        const string command = "$PSRF101,0,0,0,0,0,0,12,1*15\r\n";

        [Parameter]
        public PortSettings PortSettings;


        public ITestResult Execute()
        {
            AbstractPort port = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                try
                {
                    port = PortOpenHelper.Open(PortSettings, cancelWaitEvent);
                    //port.OnDataReceived += (sender, e) => { Debug.WriteLine(string.Format("RX: {0}", Encoding.ASCII.GetString(e.Data))); };
                    port.Write(command);
                    return new TestResult(ResultEnum.PASS);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    PortOpenHelper.Close(port, PortSettings);
                }
            }
        }
    }
}
