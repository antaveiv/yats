﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Ports;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TestCase.Gps
{
    public class NmeaParser : Parser, IResetable, ICloneable
    {
        public string ExpectedSentenceType { get; private set; }
        public string Message = "";
        public IList<string> Parts { get; private set; }
        public string SentenceType { get; private set; }
        ReadOperationResult readOperationResult;

        public NmeaParser()
        {
        }

        public NmeaParser(string expectedSentenceType)
            : this()
        {
            ExpectedSentenceType = expectedSentenceType;
        }

        public bool ParseString(string message)
        {
            try
            {
                if (string.IsNullOrEmpty(message))
                {
                    return false;
                }

                int idx = message.IndexOf('$');
                if (idx > 0)
                {
                    message = message.Substring(idx);//truncate garbage before $
                }

                if (message[0] != '$')
                    return false;

                string contents;
                int checksum = -1;
                idx = message.IndexOf('*');
                if (idx >= 0)
                {
                    checksum = Convert.ToInt32(message.Substring(idx + 1), 16);
                    contents = message.Substring(1, idx - 1);
                }
                else
                {
                    return false;
                }

                int checksumTest = 0;
                for (int i = 0; i < contents.Length; i++)
                {
                    checksumTest ^= Convert.ToByte(contents[i]);
                }
                if (checksum != checksumTest)
                {
                    return false;
                }

                string[] parts = contents.Split(new char[] { ',' });
                this.SentenceType = parts[0];
                this.Parts = parts.Skip(1).ToList();
                this.Message = message;
                if (string.IsNullOrEmpty(ExpectedSentenceType))
                {
                    return true;
                }
                return ExpectedSentenceType == SentenceType;
            }
            catch 
            {
                return false;
            }
        }
        
        public override bool IncomingData(byte dataByte)
        {
            char c = (char)dataByte;
            //Debug.WriteLine("Parse " + c);
            //bool result = false;
            if (c == '\r' || c == '\n') {
                bool parse = ParseString(Message);
                readOperationResult = new ReadOperationResult(parse ? ReadOperationResult.Result.SUCCESS : ReadOperationResult.Result.ERROR);
                //Debug.WriteLine("Parsed [" + Message + "] " + parse.ToString());
                return true;
            }
            else
            {
               Message += c;
            }
            return false;
        }

        
        public override ReadOperationResult GetResult()
        {
            return readOperationResult;
        }

        public void Reset()
        {
            Message = "";
            Parts = null;
            SentenceType = null;
            readOperationResult = null;
        }

        public object Clone()
        {
            var clone = new NmeaParser();
            clone.ExpectedSentenceType = this.ExpectedSentenceType;
            clone.Message = this.Message;
            clone.Parts = this.Parts;
            clone.SentenceType = this.SentenceType;
            clone.readOperationResult = this.readOperationResult;
            return clone;
        }
    }
}