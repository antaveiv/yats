﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Attributes;
using yats.Ports;
using yats.Ports.Utilities;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using log4net;
using System;
using System.Globalization;
using System.Threading;
using System.Linq;
using System.Collections.Generic;
using yats.Ports.Utilities.Parsers;
using yats.Utilities;

namespace TestCase.Gps
{
    //Listen to GPGGA until fix (with optional expected max HDOP) or timeout. Logs all NMEA sentences
    public class WaitForGpggaFix : IYatsTestCase, ICancellable
    {
        ILog NmeaLogger = LogManager.GetLogger("NMEA");
        ILog Logger = LogManager.GetLogger("Fix");
        ILog SnrLogger = LogManager.GetLogger("SNR");
        ILog OtherLogger= LogManager.GetLogger("Other");

        [Parameter]
        public TimeSpan Timeout;

        [Parameter]
        [Description("Do not count as fix if HDOP is higher than this value")]
        public float? MaxHdop;

        [Parameter]
        public PortSettings PortSettings;

        [Parameter]
        [Description("Run until timeout, even if good fix is received")]
        public bool RunUntilTimeout = false;

        [Parameter]
        [CanBeNull]
        [Description("If configured will give inconclusive results in case a sentence with given name is encountered. Can be used to detect GPS module sleep, reset etc.")]
        public string [] TroubleSentences;

        private class SatteliteData
        {
            public int SatNumber;
            private List<float> samples = new List<float>(10);
            const int MAX_SAMPLES = 10;
            public void AddSample(float snr)
            {
                if (samples.Count >= MAX_SAMPLES)
                {
                    samples.RemoveAt(0);
                }
                samples.Add(snr);
            }

            public float SnrAverage {
                get {
                    if (samples.Count > 0)
                    {
                        return samples.Average();
                    }
                    return 0;
                }
            }
        }

        public ITestResult Execute()
        {
            Dictionary<int, SatteliteData> satelliteData = new Dictionary<int, SatteliteData>();

            AbstractPort port = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler cancelHandler = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += cancelHandler;

                try
                {
                    port = PortOpenHelper.Open(PortSettings, cancelWaitEvent);
                    port.DiscardDataWithoutParsers = false;
                    TextLineParser lineParser = new TextLineParser();
                   
                    port.DiscardInBuffer();
                    var nmea = new NmeaParser();
                    DateTime startTime = DateTime.Now;
                    DateTime endTime = startTime.Add(Timeout);
                    bool firstFix = false;
                    var testResult = new TestResult(ResultEnum.FAIL);
                    int numSentencesReceived = 0;
                    do
                    {
                        nmea.Reset();
                        lineParser.Reset();
                        ReadOperationResult result = port.Execute(2000, lineParser, cancelWaitEvent);
                        if (result.OperationResult == ReadOperationResult.Result.SUCCESS)
                        {
                            string s = lineParser.GetReceivedLineUTF();
                            if (nmea.ParseString(s) == false)
                            {
                                OtherLogger.Info(s);
                                continue;
                            } 

                            NmeaLogger.Warn(nmea.Message);
                            numSentencesReceived++;

                            if (TroubleSentences != null && TroubleSentences.Contains(nmea.SentenceType) && numSentencesReceived > 1)
                            {
                                testResult = new TestResult(ResultEnum.INCONCLUSIVE);
                                NmeaLogger.Error("Trouble detected");
                            }

                            if (nmea.SentenceType == "GPGSV")
                            {
                                for (int i = 0; i < 4; i++)
                                {
                                    try
                                    {
                                        int satNumber = int.Parse(nmea.Parts[i * 4 + 3]);
                                        int snr = 0;
                                        if (string.IsNullOrEmpty(nmea.Parts[i * 4 + 6]) == false)
                                        {
                                            snr = int.Parse(nmea.Parts[i * 4 + 6]);
                                        }
                                        if (satelliteData.ContainsKey(satNumber) == false)
                                        {
                                            satelliteData.Add(satNumber, new SatteliteData() { SatNumber = satNumber });
                                        }
                                        satelliteData[satNumber].AddSample(snr);
                                    }
                                    catch (ArgumentOutOfRangeException) { }

                                }
                            }

                            if (nmea.SentenceType != "GPGGA" && nmea.SentenceType != "GNGGA")
                            {
                                nmea.Reset();
                                continue;
                            }

                            if (satelliteData.Count >= 5)
                            {
                                var bestSattelites = satelliteData.Values.OrderByDescending(x => x.SnrAverage).Take(5).ToList();
                                SnrLogger.Debug(string.Join(" ", bestSattelites.Select(x => string.Format("{0}:{1:0.0}", x.SatNumber, x.SnrAverage))));
                                SnrLogger.InfoFormat("Average: {0:0.0}", bestSattelites.Select(x => x.SnrAverage).Average());
                            }

                            if (nmea.Parts[5] != "1")// no fix indication
                            {
                                nmea.Reset();
                                continue;
                            }

                            float hdop = float.Parse(nmea.Parts[7], CultureInfo.InvariantCulture);
                            if (!MaxHdop.HasValue || hdop <= MaxHdop.Value)
                            {
                                Logger.Warn(string.Format("Good fix: {0}s SV: {1} HDOP: {2}", (int)DateTime.Now.Subtract(startTime).TotalSeconds, nmea.Parts[6], nmea.Parts[7]));
                                testResult = new TestResult(true);
                                if (!RunUntilTimeout)
                                {
                                    return testResult;
                                }
                            }
                            if (!firstFix)
                            {
                                firstFix = true;
                                Logger.Warn(string.Format("TTFF: {0}s SV: {1} HDOP: {2}", (int)DateTime.Now.Subtract(startTime).TotalSeconds, nmea.Parts[6], nmea.Parts[7]));
                                //testResult = new TestResult(true);
                                if (MaxHdop.HasValue == false)
                                {
                                    testResult = new TestResult(true);
                                    if (RunUntilTimeout == false)
                                    {
                                        return testResult;
                                    }
                                }
                            }
                        }
                        else if (result.OperationResult == ReadOperationResult.Result.CANCELLED)
                        {
                            Logger.Warn(string.Format("Canceled"));
                            return new TestResult(ResultEnum.CANCELED);
                        }
                        else if (result.OperationResult == ReadOperationResult.Result.TIMEOUT)
                        {
                            NmeaLogger.Error("NMEA timeout");
                            testResult = new TestResult(ResultEnum.INCONCLUSIVE);
                        }
                        
                    } while (DateTime.Now < endTime);

                    if (testResult.Result != ResultEnum.PASS)
                    {
                        Logger.Warn(string.Format("Fix timeout"));
                    }
                    port.Detach(lineParser);
                    return testResult;
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cancelHelper.OnCancel -= cancelHandler;
                    PortOpenHelper.Close(port, PortSettings);
                }
            }
        }

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }
}