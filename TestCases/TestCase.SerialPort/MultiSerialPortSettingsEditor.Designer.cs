﻿namespace yats.TestCase.SerialPort
{
    partial class MultiSerialPortSettingsEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.portName1 = new System.Windows.Forms.ListBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.stopBits = new System.Windows.Forms.ListBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.parity = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataBits = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.baudRate = new System.Windows.Forms.ListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.portName2 = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.portName1);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 166);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Serial port 1";
            // 
            // portName1
            // 
            this.portName1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.portName1.FormattingEnabled = true;
            this.portName1.Location = new System.Drawing.Point(3, 16);
            this.portName1.Name = "portName1";
            this.portName1.Size = new System.Drawing.Size(194, 147);
            this.portName1.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.stopBits);
            this.groupBox6.Location = new System.Drawing.Point(419, 107);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(115, 98);
            this.groupBox6.TabIndex = 18;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Stop bits";
            // 
            // stopBits
            // 
            this.stopBits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stopBits.FormattingEnabled = true;
            this.stopBits.Location = new System.Drawing.Point(3, 16);
            this.stopBits.Name = "stopBits";
            this.stopBits.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.stopBits.Size = new System.Drawing.Size(109, 69);
            this.stopBits.TabIndex = 1;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.parity);
            this.groupBox5.Location = new System.Drawing.Point(419, 211);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(115, 130);
            this.groupBox5.TabIndex = 19;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Parity";
            // 
            // parity
            // 
            this.parity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parity.FormattingEnabled = true;
            this.parity.Location = new System.Drawing.Point(3, 16);
            this.parity.Name = "parity";
            this.parity.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.parity.Size = new System.Drawing.Size(109, 108);
            this.parity.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataBits);
            this.groupBox3.Location = new System.Drawing.Point(419, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(115, 98);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Data bits";
            // 
            // dataBits
            // 
            this.dataBits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataBits.FormattingEnabled = true;
            this.dataBits.Location = new System.Drawing.Point(3, 16);
            this.dataBits.Name = "dataBits";
            this.dataBits.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.dataBits.Size = new System.Drawing.Size(109, 69);
            this.dataBits.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.baudRate);
            this.groupBox2.Location = new System.Drawing.Point(213, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 338);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Baud rates";
            // 
            // baudRate
            // 
            this.baudRate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.baudRate.FormattingEnabled = true;
            this.baudRate.Items.AddRange(new object[] {
            "110",
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "19200",
            "28800",
            "38400",
            "56000",
            "57600",
            "115200",
            "128000",
            "153600",
            "230400",
            "256000",
            "460800",
            "921600"});
            this.baudRate.Location = new System.Drawing.Point(3, 16);
            this.baudRate.Name = "baudRate";
            this.baudRate.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.baudRate.Size = new System.Drawing.Size(194, 316);
            this.baudRate.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.portName2);
            this.groupBox4.Location = new System.Drawing.Point(6, 175);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(200, 166);
            this.groupBox4.TabIndex = 16;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Serial port 2";
            // 
            // portName2
            // 
            this.portName2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.portName2.FormattingEnabled = true;
            this.portName2.Location = new System.Drawing.Point(3, 16);
            this.portName2.Name = "portName2";
            this.portName2.Size = new System.Drawing.Size(194, 147);
            this.portName2.TabIndex = 0;
            // 
            // MultiSerialPortSettingsEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Name = "MultiSerialPortSettingsEditor";
            this.Size = new System.Drawing.Size(865, 395);
            this.groupBox1.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox portName1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ListBox stopBits;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ListBox parity;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox dataBits;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox baudRate;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListBox portName2;
    }
}
