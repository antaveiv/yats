﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using yats.TestCase.Parameter;

namespace yats.TestCase.SerialPort
{
    public class MultiSerialPortSettings : AbstractParameterValue
    {
        [DebuggerStepThrough]
        public MultiSerialPortSettings()
        {
        }
        
        protected List<int> baudRates = new List<int>();
        public List<int> BaudRates
        {
            [DebuggerStepThrough]
            get
            {
                return baudRates;
            }
            [DebuggerStepThrough]
            set
            {
                baudRates = value;
            }
        }

        protected string portNameA;
        public string PortNameA
        {
            [DebuggerStepThrough]
            get
            {
                return portNameA;
            }
            [DebuggerStepThrough]
            set
            {
                portNameA = value;
            }
        }

        protected string portNameB;
        public string PortNameB
        {
            [DebuggerStepThrough]
            get
            {
                return portNameB;
            }
            [DebuggerStepThrough]
            set
            {
                portNameB = value;
            }
        }

        protected List<int> dataBits = new List<int>();
        public List<int> DataBits
        {
            [DebuggerStepThrough]
            get
            {
                return dataBits;
            }
            [DebuggerStepThrough]
            set
            {
                dataBits = value;
            }
        }

        protected List<Parity> parity = new List<Parity>();
        public List<Parity> Parity
        {
            [DebuggerStepThrough]
            get
            {
                return parity;
            }
            [DebuggerStepThrough]
            set
            {
                parity = value;
            }
        }

        protected List<StopBits> stopBits = new List<StopBits>();
        public List<StopBits> StopBits
        {
            [DebuggerStepThrough]
            get
            {
                return stopBits;
            }
            [DebuggerStepThrough]
            set
            {
                stopBits = value;
            }
        }

        public override AbstractParameterValue Clone()
        {
            MultiSerialPortSettings result = new MultiSerialPortSettings();
            result.baudRates = new List<int>(this.baudRates);
            result.portNameA = this.portNameA;
            result.portNameB = this.portNameB;
            result.dataBits = new List<int>(this.dataBits);
            result.parity = new List<Parity>(this.parity);
            result.stopBits = new List<StopBits>(this.stopBits);
            return result;
        }

        public override string ToString()
        {
            return string.Format("Multiple serial ports");
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "serial port settings", typeof(MultiSerialPortSettings).AssemblyQualifiedName);
        }

        public override string GetDisplayValue()
        {
            return this.ToString();
        }

        public override object GetValue(IParameter format)
        {
            return this;
        }

        public override int GetParamValueTypeHash()
        {
            return GetType().FullName.GetHashCode();
        }
                
    }
}
