﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using log4net;
using yats.Attributes;
using yats.Ports;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Threading;

namespace yats.TestCase.SerialPort
{
    [Description("Perform loopback test on two ports")]
    public class SerialPortLoopTest : IYatsTestCase, ICancellable
    {
        protected MultiSerialPortSettings portSettings;
        public MultiSerialPortSettings PortSettings
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        protected int? minPacketSize;
        public int? MinPacketSize
        {
            get { return minPacketSize; }
            set { minPacketSize = value; }
        }

        protected int? maxPacketSize;
        public int? MaxPacketSize
        {
            get { return maxPacketSize; }
            set { maxPacketSize = value; }
        }

        protected int repetitions;
        public int Repetitions
        {
            get { return repetitions; }
            set { repetitions = value; }
        }

        protected TimeSpan? sleepBetweenPackets;
        public TimeSpan? SleepBetweenPackets
        {
            get { return sleepBetweenPackets; }
            set { sleepBetweenPackets = value; }
        }

        #region IYatsTestCase Members

        public ITestResult Execute()
        {
            yats.Ports.SerialPort portA = null;
            yats.Ports.SerialPort portB = null;
            bool testResult = true;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;
                try
                {                    
                    if (portSettings.BaudRates.Count < 1)
                    {
                        throw new Exception("Must set >= 1 baud rate");
                    }
                    if (portSettings.DataBits.Count < 1)
                    {
                        throw new Exception("Must set >= 1 data bits setting");
                    }
                    if (portSettings.Parity.Count < 1)
                    {
                        throw new Exception("Must set >= 1 parity setting");
                    }
                    if (portSettings.StopBits.Count < 1)
                    {
                        throw new Exception("Must set >= 1 stop bits setting");
                    }

                    if (minPacketSize.HasValue == false)
                    {
                        minPacketSize = 1;
                    }

                    if (maxPacketSize.HasValue == false)
                    {
                        maxPacketSize = 1024;
                    }

                    Random rnd = new Random();

                    for (int i = 0; i < repetitions; i++)
                    {

                        yats.Ports.SerialPortInfo settingsA = new SerialPortInfo();
                        yats.Ports.SerialPortInfo settingsB = new SerialPortInfo();

                        if (rnd.Next(2) == 0)
                        { // send 1->2
                            settingsA.PortName = portSettings.PortNameA;
                            settingsB.PortName = portSettings.PortNameB;
                        }
                        else
                        {
                            settingsA.PortName = portSettings.PortNameB;
                            settingsB.PortName = portSettings.PortNameA;
                        }
                        settingsA.BaudRate = settingsB.BaudRate = portSettings.BaudRates[rnd.Next(portSettings.BaudRates.Count)];
                        int dataBits = settingsA.DataBits = settingsB.DataBits = portSettings.DataBits[rnd.Next(portSettings.DataBits.Count)];
                        settingsA.DtrEnable = settingsB.DtrEnable = false;
                        settingsA.Handshake = settingsB.Handshake = System.IO.Ports.Handshake.None;
                        settingsA.Parity = settingsB.Parity = portSettings.Parity[rnd.Next(portSettings.Parity.Count)];
                        settingsA.RtsEnable = settingsB.RtsEnable = RtsEnableEnum.DONT_CHANGE;
                        settingsA.StopBits = settingsB.StopBits = portSettings.StopBits[rnd.Next(portSettings.StopBits.Count)];

                        portA = PortReusePool.OpenSerial(settingsA);
                        portB = PortReusePool.OpenSerial(settingsB);
                        portB.DiscardInBuffer();

                        var packet = GetRandomDataPacket(dataBits);
                        portA.Write(packet, 0, packet.Length);
                        Receiver parser = new Receiver(packet);
                        var result = portB.Execute(portB.GetTransmissionTime(packet.Length) + 80, parser, cancelWaitEvent);

                        if (result == null)
                        {
                            result = null;//breakpoint
                        }

                        if (result.OperationResult != ReadOperationResult.Result.SUCCESS)
                        {
                            testResult = false;
                            LogManager.GetLogger("SerialPortLoopTest").InfoFormat("Error {0} -> {1} {2} {3}.{4} parity {5}", portA.Name, portB.Name, settingsA.BaudRate, settingsA.DataBits, settingsA.StopBits, settingsA.Parity);
                            LogManager.GetLogger("TX").Info(packet);
                            LogManager.GetLogger("RX").Info(parser.GetReceivedData());
                        }

                        if (sleepBetweenPackets.HasValue)
                        {
                            System.Threading.Thread.Sleep(sleepBetweenPackets.Value);
                        }
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cancelHelper.OnCancel -= myDelegate;
                    PortReusePool.Close(portA);
                    PortReusePool.Close(portB);
                }
            }
            return new TestResult(testResult);
        }
        
    

        protected class Receiver : Parser
        {
            byte[] packet;
            byte[] received;
            int compareIndex = 0;
            ReadOperationResult result = null;

            public Receiver(byte[] packet)
            {
                this.packet = packet;
                received = new byte[packet.Length];
            }

            /// <summary>
            /// When a data byte is received by a port, this function is called
            /// </summary>
            /// <returns>True if the analyzer has finished receiving all the necessary data to make a pass/fail decision. <br/>False, if more data should be read from the port</returns>
            public override bool IncomingData(byte dataByte)
            {
                received[compareIndex] = dataByte;
                bool match = packet[compareIndex++] == dataByte;
                if (result == null && !match){
                    result = new ReadOperationResult(ReadOperationResult.Result.ERROR);
                }

                if (compareIndex == packet.Length)
                {
                    if (result == null) {
                        result = new ReadOperationResult(ReadOperationResult.Result.SUCCESS);
                    }
                    
                    return true;
                }

                return false;
            }

            /// <summary>
            /// Is called automatically after IncomingData returns true. It gives a pass/fail result of the command
            /// </summary>
            /// <returns>value that the command(...) method of ExtSerialPort class should return as the result of the current function</returns>
            public override ReadOperationResult GetResult()
            {
                return result;
            }

            public override byte[] GetReceivedData()
            {
                return received;
            }
        }

        protected byte[] GetRandomDataPacket(int dataBits)
        {
            int shiftBy = 8 - dataBits;
            Random rnd = new Random();
            int length = rnd.Next(minPacketSize.Value, maxPacketSize.Value+1);
            byte[] result = new byte[length];
            rnd.NextBytes(result);
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = (byte)(result[i] >> shiftBy);
            }
            return result;
        }

        #endregion

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }
}
