﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using log4net;
using yats.Ports;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Threading;
using yats.Utilities;
using yats.Ports.Utilities;
using yats.Ports.Utilities.Protocols.ModbusRTU;

namespace yats.TestCase.SerialPort
{
    [Description("Write configured data to a port using ModbusRTU protocol")]
    public class ModbusRTUWrite : IYatsTestCase, ICancellable
    {
        protected PortSettings portSettings;
        public PortSettings Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }
        protected TimeSpan timeout;
        public TimeSpan Timeout
        {
            get { return timeout; }
            set { timeout = value; }
        }

        protected int address;
        public int Address
        {
            get { return address; }
            set { address = value; }
        }

        protected int byteOffset;
        public int ByteOffset
        {
            get { return byteOffset; }
            set { byteOffset = value; }
        }

        protected byte[] data;
        public byte[] Data
        {
            get { return data; }
            set { data = value; }
        }

        public ITestResult Execute()
        {
            AbstractPort port = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;
                try
                {
                    if (data.IsNullOrEmpty() || data.Length % 2 != 0)
                    {
                        throw new Exception("Data should not be empty. Data length should be divisible by 2");
                    }
                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);
                    int timeoutMs = (int)timeout.TotalMilliseconds;

                    return ResultHelper.Convert(new ModbusRtu(port).Write(address, byteOffset, data, timeoutMs, cancelWaitEvent));
                }
                catch
                {
                    throw;
                }
                finally
                {
                    PortOpenHelper.Close(port, portSettings);
                }
            }
        }

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }
}
