﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Windows.Forms;
using yats.TestCase.Parameter;
using yats.Utilities;

namespace yats.TestCase.SerialPort
{
    public partial class MultiSerialPortSettingsEditor : UserControl, IEditor
    {
        public MultiSerialPortSettingsEditor()
        {
            InitializeComponent();
            var names = SerialPortExtension.OrderedPortNames(false);
            portName1.Items.AddRange(names);
            portName2.Items.AddRange(names);
            stopBits.DataSource = Enum.GetValues(typeof(StopBits));
            parity.DataSource = Enum.GetValues(typeof(Parity));
            baudRate.DataSource = new int[]{110,
                    300,
                    600,
                    1200,
                    2400,
                    4800,
                    9600,
                    14400,
                    19200,
                    28800,
                    38400,
                    56000,
                    57600,
                    115200,
                    128000,
                    153600,
                    230400,
                    256000,
                    460800,
                    921600};
            dataBits.DataSource = new int[] { 8, 7, 6, 5 };
        }


        #region IEditor Members

        public void ParameterToEditor(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ParameterToEditor(parameter); }));
            }
            else
            {
                MultiSerialPortSettings param = parameter as MultiSerialPortSettings;
                if (param != null)
                {
                    //serial port settings
                    baudRate.SelectedItems.Clear();
                    foreach (var boundObject in param.BaudRates)
                    {
                        baudRate.SelectedItems.Add(boundObject);
                    }

                    portName1.SelectedItem = param.PortNameA;
                    portName2.SelectedItem = param.PortNameB;

                    dataBits.SelectedItems.Clear();
                    foreach (var boundObject in param.DataBits)
                    {
                        dataBits.SelectedItems.Add(boundObject);
                    }

                    stopBits.SelectedItems.Clear();
                    foreach (var boundObject in param.StopBits)
                    {
                        stopBits.SelectedItems.Add(boundObject);
                    }

                    parity.SelectedItems.Clear();
                    foreach (var boundObject in param.Parity)
                    {
                        parity.SelectedItems.Add(boundObject);
                    }
                }
            }
        }

        public void EditorToParameter(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameter); }));
            }
            else
            {
                MultiSerialPortSettings param = parameter as MultiSerialPortSettings;
                if (param != null)
                {
                    param.BaudRates.Clear();
                    foreach (var boundObject in baudRate.SelectedItems)
                    {
                        param.BaudRates.Add((int)boundObject);
                    }

                    param.PortNameA = (string)portName1.SelectedItem;
                    param.PortNameB = (string)portName2.SelectedItem;

                    param.DataBits.Clear();
                    foreach (var boundObject in dataBits.SelectedItems)
                    {
                        param.DataBits.Add((int)boundObject);
                    }

                    param.StopBits.Clear();
                    foreach (var boundObject in stopBits.SelectedItems)
                    {
                        param.StopBits.Add((StopBits)boundObject);
                    }

                    param.Parity.Clear();
                    foreach (var boundObject in parity.SelectedItems)
                    {
                        param.Parity.Add((Parity)boundObject);
                    }
                }
            }
        }

        [DebuggerStepThrough]
        public string TabName()
        {
            return "Serial port properties";
        }

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(typeof(MultiSerialPortSettings), this.GetType(), 10);
        }

        public event EventHandler OnConfirm;

        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }

        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
        {
            //TODO implement
        }

        #endregion
    }
}
