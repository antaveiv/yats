﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using log4net;
using yats.Ports;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Threading;
using yats.Utilities;
using yats.Ports.Utilities;
using InTheHand.Net.Bluetooth;
namespace yats.TestCase.SerialPort
{
    [Description("Wait to discover a Bluetooth device")]
    public class BluetoothConnect : IYatsTestCase, ICancellable
    {
        [Parameter]
        [Description("Configure to use Bluetooth port")]
        public PortSettings Port;

        [Parameter]
        public byte[] ExpectedBluetoothAddress;

        [Parameter]
        public TimeSpan Timeout = TimeSpan.FromSeconds(30);

        public ITestResult Execute()
        {
            using (ManualResetEvent passEvent = new ManualResetEvent(false))
            {
                //BluetoothPort port = (BluetoothPort) PortOpenHelper.Open(Port);
                BluetoothComponent localComponent = new BluetoothComponent();//port.Client);

                localComponent.DiscoverDevicesAsync(255, true, true, true, true, null);
                localComponent.DiscoverDevicesProgress += new EventHandler<DiscoverDevicesEventArgs>((object sender, DiscoverDevicesEventArgs e) =>
        {
            // log and save all found devices
            for (int i = 0; i < e.Devices.Length; i++)
            {
                if (ByteArray.Equal(Port.BluetoothPort.Address.ToByteArray(), e.Devices[i].DeviceAddress.ToByteArray()))
                {
                    passEvent.Set();
                }
                if (e.Devices[i].Remembered)
                {
                    log4net.LogManager.GetLogger(GetType().Name).Info(e.Devices[i].DeviceName + " (" + e.Devices[i].DeviceAddress + "): Device is known");
                }
                else
                {
                    log4net.LogManager.GetLogger(GetType().Name).Info(e.Devices[i].DeviceName + " (" + e.Devices[i].DeviceAddress + "): Device is unknown");
                }
                //this.deviceList.Add(e.Devices[i]);
            }
        });
                localComponent.DiscoverDevicesComplete += new EventHandler<DiscoverDevicesEventArgs>((object sender, DiscoverDevicesEventArgs e) =>
        {
            log4net.LogManager.GetLogger(GetType().Name).Info("Discover complete");
        });
                //PortOpenHelper.Close(port);

                using (ManualResetEvent cancelWaitEvent = new ManualResetEvent(false))
                {
                    EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                    cancelHelper.OnCancel += myDelegate;
                    WaitHandle[] waitHandles = new WaitHandle[] { passEvent, cancelWaitEvent };
                    int res = WaitHandle.WaitAny(waitHandles, Timeout, false);
                    return new TestResult(res == 0);
                }
            }
        }

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }
}