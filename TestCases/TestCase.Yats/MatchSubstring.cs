﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.YatsNativeTestCase;

namespace yats.TestCase.Yats
{
    [Description("Check that string contains a substring")]
    public class MatchSubstring : IYatsTestCase
    {
        protected string val;
        public string Value
        {
            get { return val; }
            set { val = value; }
        }

        protected string substring;
        public string Substring
        {
            get { return substring; }
            set { substring = value; }
        }

        protected bool? caseSensitive;
        public bool? CaseSensitive
        {
            get { return caseSensitive; }
            set { caseSensitive = value; }
        }

        #region IYatsTestCase Members
        
        public ITestResult Execute()
        {
            if (caseSensitive.HasValue && caseSensitive.Value == false){
                return new TestResult(val.ToLower().Contains(substring.ToLower()));
            }
            return new TestResult(val.Contains(substring));
        }

        #endregion
    }
}
