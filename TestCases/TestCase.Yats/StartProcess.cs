﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Security.Permissions;

namespace yats.TestCase.Yats
{
    [Description("Start a process (run an application with configured arguments)")]
    public class ProcessStart : IYatsTestCase
    {
        protected string fileName;
        protected string arguments;

        public string FileName
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }

        [CanBeNull]
        public string Arguments
        {
            get
            {
                return arguments;
            }
            set
            {
                arguments = value;
            }
        }

        protected Process result;
        public Process Result
        {
            get
            {
                return result;
            }
        }

        #region IYatsTestCase Members

        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public ITestResult Execute()
        {
            var psi = new ProcessStartInfo(fileName, arguments);
            psi.RedirectStandardError = true;
            psi.RedirectStandardOutput = true;
            psi.UseShellExecute = false;

            result = Process.Start(psi);
            
            return new TestResult(true);
        }

        #endregion
    }

    //Fails on timeout, passes if timeout is not set or process exits on time
    [Description("Wait for a process to end or timeout. Process should be started with ProcessStart test case")]
    public class ProcessWait : IYatsTestCase
    {
        protected Process process;
        public Process WaitFor
        {
            get
            {
                return process;
            }
            set
            {
                process = value;
            }
        }

        protected TimeSpan? timeout;
        public TimeSpan? Timeout
        {
            get
            {
                return timeout;
            }
            set
            {
                timeout = value;
            }
        }

        protected string stdOut;
        public string StdOut
        {
            get
            {
                return stdOut;
            }
        }

        protected string stdErr;
        public string StdErr
        {
            get
            {
                return stdErr;
            }
        }

        protected int exitCode;
        public int ExitCode
        {
            get
            {
                return exitCode;
            }
        }

        #region IYatsTestCase Members

        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public ITestResult Execute()
        {
            if (timeout.HasValue)
            {
                if (process.WaitForExit((int)timeout.Value.TotalMilliseconds))
                {
                    exitCode = process.ExitCode;
                }
                else
                {
                    return new TestResult(false);
                }
            }
            else
            {
                process.WaitForExit();
                exitCode = process.ExitCode;
            }

            stdOut = process.StandardOutput.ReadToEnd();
            stdErr = process.StandardError.ReadToEnd();
            return new TestResult(true);
        }

        #endregion
    }

    [Description("Start a process and wait for finish")]
    public class ProcessRun : IYatsTestCase
    {
        protected string fileName;
        protected string arguments;

        public string FileName
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }

        [CanBeNull]
        public string Arguments
        {
            get
            {
                return arguments;
            }
            set
            {
                arguments = value;
            }
        }


        protected TimeSpan? timeout;
        public TimeSpan? Timeout
        {
            get
            {
                return timeout;
            }
            set
            {
                timeout = value;
            }
        }

        protected string stdOut;
        public string StdOut
        {
            get
            {
                return stdOut;
            }
        }

        protected string stdErr;
        public string StdErr
        {
            get
            {
                return stdErr;
            }
        }

        protected int exitCode;
        public int ExitCode
        {
            get
            {
                return exitCode;
            }
        }

        #region IYatsTestCase Members

        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public ITestResult Execute()
        {
            var psi = new ProcessStartInfo(fileName, arguments);
            psi.RedirectStandardError = true;
            psi.RedirectStandardOutput = true;
            psi.UseShellExecute = false;

            Process process = Process.Start(psi);
           
            if (timeout.HasValue)
            {
                if (process.WaitForExit((int)timeout.Value.TotalMilliseconds))
                {
                    exitCode = process.ExitCode;
                }
                else
                {
                    return new TestResult(false);
                }
            }
            else
            {
                process.WaitForExit();
                exitCode = process.ExitCode;
            }

            stdOut = process.StandardOutput.ReadToEnd();
            stdErr = process.StandardError.ReadToEnd();
            return new TestResult(true);
        }

        #endregion
    }

}
