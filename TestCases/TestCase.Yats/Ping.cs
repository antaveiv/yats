﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Net;
using System.Net.NetworkInformation;
using log4net;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.YatsNativeTestCase;

namespace yats.TestCase.Yats
{
    [Description("Ping a server")]
    public class Ping : IYatsTestCase
    {
        protected string serverAddress;
        public string ServerAddress
        {
            get { return serverAddress; }
            set { serverAddress = value; }
        }

        protected TimeSpan? timeout;
        public TimeSpan? Timeout
        {
            get { return timeout; }
            set { timeout = value; }
        }

        protected IPAddress replyAddress;
        public IPAddress ReplyAddress
        {
            get { return replyAddress; }
        }

        #region IYatsTestCase Members

        
        public ITestResult Execute()
        {
            try
            {
                ILog Logger = LogManager.GetLogger(this.GetType());
                this.replyAddress = null;

                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping( );
                PingReply reply = ping.Send( serverAddress, (timeout.HasValue)?(int)timeout.Value.TotalMilliseconds : 1000 );
                if(reply.Status == IPStatus.Success)
                {
                    this.replyAddress = reply.Address;
                    Logger.WarnFormat( "Ping {0} {1} OK, roundtrip {2}ms", serverAddress, reply.Address.ToString( ), reply.RoundtripTime);
                    return new TestResult( ResultEnum.PASS );
                }
                else
                {
                    Logger.WarnFormat( "Ping failed: {0}", reply.Status);
                    return new TestResult( ResultEnum.FAIL );
                }
            }
            catch(Exception e)
            {
                return new ExceptionResult( e );
            }
        }

        #endregion
    }
}
