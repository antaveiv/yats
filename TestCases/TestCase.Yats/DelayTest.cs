/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Attributes;
using System;
using System.Threading;
using log4net;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.YatsNativeTestCase;
using yats.Utilities;

namespace yats.TestCase.Yats
{
    [Description("Delay test by a specified time")]
	public class DelayTest : IYatsTestCase, ICancellable
	{
		TimeSpan delay;
        public TimeSpan Delay
		{
			get {return delay;}
			set {delay = value;}
		}
		
		public DelayTest ()
		{
		}

        public DelayTest(int delayMs)
        {
            this.delay = TimeSpan.FromMilliseconds(delayMs);
        }

        public DelayTest(TimeSpan delay)
        {
            this.delay = delay;
        }

        

        #region IYatsTestCase Members
        
        public ITestResult Execute()
        {
            LogManager.GetLogger( "Delay" ).DebugFormat( "Delay {0}", delay.ToFriendlyDisplay(3));
            using (AutoResetEvent waitFor = new AutoResetEvent (false))
            {
            EventHandler myDelegate = (sender, e) => { waitFor.Set (); };
            cancelHelper.OnCancel += myDelegate;
            try
                {
                if (waitFor.WaitOne (delay) == false)
                    {
                    return new TestResult (ResultEnum.PASS);
                    }
                else
                    {
                    return new TestResult (ResultEnum.CANCELED);
                    }
                }
            catch { throw; }
            finally { cancelHelper.OnCancel -= myDelegate; }
            }
        }

        #endregion

        #region ICancellable Members
        
        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel ();
        }
        
        #endregion
    }
}

