﻿using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.YatsNativeTestCase;
using NativeWifi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCase.WiFi
{
    class CheckConnectedToAccessPoint : IYatsTestCase
    {
        [Parameter]
        [Description("Expected access point name")]
        public string AccessPointName;

        public ITestResult Execute()
        {
            WlanClient client = new WlanClient();
            foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
            {
                if (wlanIface.CurrentConnection.profileName == AccessPointName)
                {
                    return new TestResult(true);
                }
            }
            return new TestResult(false);
        }

        public static bool IsConnected(string accessPointName)
        {
            return new CheckConnectedToAccessPoint() { AccessPointName = accessPointName}.Execute().Result == ResultEnum.PASS;
        }
    }
}
