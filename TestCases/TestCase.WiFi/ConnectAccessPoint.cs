﻿using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.YatsNativeTestCase;
using NativeWifi;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using System.Net.NetworkInformation;

namespace TestCase.WiFi
{
    [Description("Connect to an access point with given name")]
    public class ConnectAccessPoint : IYatsTestCase, ICancellable
    {
        [Parameter]
        [Description("Access point to connect to")]
        public string AccessPointName;

        [Parameter]
        [Description("Optional address to ping after connect")]
        [CanBeNull]
        public string PingAddress;

        [Parameter]
        [Description("Connect timeout, ms")]
        public TimeSpan Timeout;

        bool canceled = false;

        public ITestResult Execute()
        {
            canceled = false;
            Stopwatch sw = new Stopwatch();
            var Logger = LogManager.GetLogger("WiFi");
            Logger.Debug("Connecting to " + AccessPointName);

            WlanClient client = new WlanClient();
            while (true) {
                foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
                {
                    if (canceled)
                    {
                        return new TestResult(ResultEnum.CANCELED);
                    }

                    Wlan.WlanAvailableNetwork[] networks = wlanIface.GetAvailableNetworkList(0);
                    bool foundNetwork = false;
                    foreach (var network in networks)
                    {
                        Wlan.Dot11Ssid ssid = network.dot11Ssid;
                        string networkName = Encoding.ASCII.GetString(ssid.SSID, 0, (int)ssid.SSIDLength);
                        if (networkName == AccessPointName)
                        {
                            foundNetwork = true;
                            break;
                        }
                    }
                    if (foundNetwork)
                    {
                        foreach (Wlan.WlanProfileInfo profileInfo in wlanIface.GetProfiles().Where(x => x.profileName == AccessPointName))
                        {                               
                            if (wlanIface.InterfaceState == Wlan.WlanInterfaceState.Connected && wlanIface.CurrentConnection.profileName == AccessPointName && wlanIface.CurrentConnection.isState == Wlan.WlanInterfaceState.Connected)
                            {
                                if (string.IsNullOrEmpty(PingAddress) == false)
                                {
                                    using (Ping ping = new Ping())
                                    {
                                        PingReply reply = ping.Send(PingAddress, 500);
                                        if (reply.Status == IPStatus.Success)
                                        {
                                            Logger.Debug("Connected to " + AccessPointName);
                                            Logger.DebugFormat("Ping {0} {1} OK, roundtrip {2}ms", PingAddress, reply.Address.ToString(), reply.RoundtripTime);
                                            return new TestResult(ResultEnum.PASS);
                                        }
                                    }
                                }
                                else
                                {
                                    Logger.Debug("Connected to " + AccessPointName);
                                    return new TestResult(ResultEnum.PASS);
                                }
                            }

                            switch (wlanIface.InterfaceState)
                            {
                                case Wlan.WlanInterfaceState.Connected:
                                    if (wlanIface.CurrentConnection.profileName != AccessPointName)
                                    {
                                        Logger.Debug("Connected to " + wlanIface.CurrentConnection.profileName + ", Connecting to " + AccessPointName);
                                        string xml = wlanIface.GetProfileXml(profileInfo.profileName);
                                        wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, xml, true);
                                        wlanIface.Connect(Wlan.WlanConnectionMode.Profile, Wlan.Dot11BssType.Any, AccessPointName);                                        
                                    }
                                    break;
                                case Wlan.WlanInterfaceState.Disconnected:
                                    {
                                        string xml = wlanIface.GetProfileXml(profileInfo.profileName);
                                        wlanIface.SetProfile(Wlan.WlanProfileFlags.AllUser, xml, true);
                                        wlanIface.Connect(Wlan.WlanConnectionMode.Profile, Wlan.Dot11BssType.Any, AccessPointName);
                                        Logger.Debug("Disconnected, connecting to " + AccessPointName);
                                        break;
                                    }

                            }
                            
                            //return new TestResult( wlanIface.ConnectSynchronously(Wlan.WlanConnectionMode.Profile, Wlan.Dot11BssType.Any, name, (int) Timeout.TotalMilliseconds));
                                                        
                            break;
                            //    while (wlanIface.InterfaceState != Wlan.WlanInterfaceState.Connected || wlanIface.CurrentConnection.profileName != name || wlanIface.CurrentConnection.isState != Wlan.WlanInterfaceState.Connected)
                            //{
                            //    Thread.Sleep(100);
                            //    if (Timeout != null && Timeout.TotalMilliseconds > 0 && sw.Elapsed > Timeout)
                            //    {
                            //        return new TestResult(false);
                            //    }
                            //}                                

                        }
                    }
                }
                Thread.Sleep(1000);
                if (Timeout != null && Timeout.TotalMilliseconds > 0 && sw.Elapsed > Timeout)
                {
                    return new TestResult(false);
                }
            }
        }

        public static ITestResult ConnectToWifiAP(string accessPointName, TimeSpan timeout)
        {
            return new ConnectAccessPoint() { AccessPointName = accessPointName, Timeout = timeout }.Execute();
        }

        public void Cancel()
        {
            canceled = true;
        }
    }
}
