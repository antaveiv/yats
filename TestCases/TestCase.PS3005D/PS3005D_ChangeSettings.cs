﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Attributes;
using System;
using yats.Ports;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Threading;
using yats.Ports.Utilities;
using System.Globalization;

namespace yats.TestCase.PS3005D
{
    [Description("Set voltage and current on PS3005 power supply")]
    class PS3005D_ChangeSettings : IYatsTestCase
    {
        protected PortSettings portSettings;
        public PortSettings Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        protected float? iset = 0;
        public float? ISet
        {
            get { return iset; }
            set { iset = value; }
        }

        protected float? vset = 0;
        public float? VSet
        {
            get { return vset; }
            set { vset = value; }
        }

        public PS3005D_ChangeSettings()
        {
            this.portSettings = new PortSettings(new PortSettings.SerialPortInfo("COM1", 9600, 8, System.IO.Ports.Parity.None));
        }

        public ITestResult Execute()
        {
            if (vset.HasValue && (vset < 0 || vset > 30))
            {
                throw new Exception("Invalid voltage value, must be 0..30");
            }
            if (iset.HasValue && (iset < 0 || iset > 5))
            {
                throw new Exception("Invalid current value, must be 0..5");
            }

            AbstractPort port = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;
                try
                {
                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);
                    
                    if (vset.HasValue)
                    {
                        port.Command(string.Format(CultureInfo.InvariantCulture, "VSET1:{0:0.00}", vset.Value), null, 200, cancelWaitEvent);

                        VellemanCrapResponseParser parser = new VellemanCrapResponseParser();
                        ReadOperationResult cmdRes = port.Command("VSET1?", parser, 400, cancelWaitEvent);
                        if (parser.ReceivedSomething() == false || this.vset != parser.GetFloat())
                        {
                            return new TestResult(false);
                        }
                    }
                    if (iset.HasValue)
                    {
                        port.Command(string.Format(CultureInfo.InvariantCulture, "ISET1:{0:0.00}", iset.Value), null, 200, cancelWaitEvent);

                        VellemanCrapResponseParser parser = new VellemanCrapResponseParser();
                        ReadOperationResult cmdRes = port.Command("ISET1?", parser, 400, cancelWaitEvent);
                        if (parser.ReceivedSomething() == false || this.iset != parser.GetFloat())
                        {
                            return new TestResult(false);
                        }
                    }
                    
                    return new TestResult(true);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cancelHelper.OnCancel -= myDelegate;
                    PortOpenHelper.Close(port, portSettings);
                }
            }
        }

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }
}
