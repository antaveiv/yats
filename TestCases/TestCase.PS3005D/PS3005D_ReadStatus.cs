﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
using yats.Attributes;
using yats.Ports;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Threading;
using System;
using yats.Ports.Utilities;
using yats.Ports.Utilities.Parsers;

namespace yats.TestCase.PS3005D
{
    [Description("Get PS3005 status")]
    class PS3005D_ReadStatus: IYatsTestCase
    {
        protected PortSettings portSettings;
        public PortSettings Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        protected string ch1Mode;
        public string Channel_1_Mode
        {
            get { return ch1Mode; }
        }
        
        protected string ch2Mode;
        public string Channel_2_Mode
        {
            get { return ch2Mode; }
        }

        protected string tracking;
        public string Tracking_Mode
        {
            get { return tracking; }
        }

        protected bool beep;
        public bool Beep
        {
            get { return beep; }
        }

        protected bool lockMode;
        public bool Lock
        {
            get { return lockMode; }
        }

        protected bool output;
        public bool Output
        {
            get { return output; }
        }

        public PS3005D_ReadStatus()
        {
            this.portSettings = new PortSettings(new PortSettings.SerialPortInfo("COM1", 9600, 8, System.IO.Ports.Parity.None));
        }

        #region IYatsTestCase Members

        public ITestResult Execute()
        {
            AbstractPort port = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;
                try
                {
                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);
                    WaitForNBytes parser = new WaitForNBytes(1);
                    ReadOperationResult cmdRes = port.Command("STATUS?", parser, 300, cancelWaitEvent);

                    if (cmdRes.OperationResult != ReadOperationResult.Result.SUCCESS || parser.GetReceivedData().Length != 1)
                    {
                        return new TestResult(false);
                    }
                    byte status = parser.GetReceivedData()[0];
                    this.ch1Mode = ((status & 0x01) != 0) ? "CV" : "CC";
                    this.ch2Mode = ((status & 0x02) != 0) ? "CV" : "CC";
                    switch ((status >> 2) & 0x03)
                    {
                        case 0: tracking = "Independent"; break;
                        case 1: tracking = "Tracking series"; break;
                        case 3: tracking = "Tracking parallel"; break;
                    }
                    this.beep = (status & 0x10) != 0;
                    this.lockMode = (status & 0x20) != 0;
                    this.output = (status & 0x40) != 0;

                    return new TestResult(true);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cancelHelper.OnCancel -= myDelegate;
                    PortOpenHelper.Close(port, portSettings);
                }
            }
        }

        #endregion

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }
}
