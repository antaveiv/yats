﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Attributes;
using yats.Ports;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Threading;
using System;
using yats.Ports.Utilities;

namespace yats.TestCase.PS3005D
{
    [Description("Get PS3005 voltage and current readings")]
    class PS3005D_ReadOutput : IYatsTestCase
    {
        protected PortSettings portSettings;
        public PortSettings Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        protected float iset;
        public float ISet
        {
            get { return iset; }
        }

        protected float vset;
        public float VSet
        {
            get { return vset; }
        }

        protected float iout;
        public float IOut
        {
            get { return iout; }
        }

        protected float vout;
        public float VOut
        {
            get { return vout; }
        }

        public PS3005D_ReadOutput()
        {
            this.portSettings = new PortSettings(new PortSettings.SerialPortInfo("COM1", 9600, 8, System.IO.Ports.Parity.None));
        }

        #region IYatsTestCase Members

        public ITestResult Execute()
        {
            AbstractPort port = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;
                try
                {
                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);
                    VellemanCrapResponseParser parser = new VellemanCrapResponseParser();
                    ReadOperationResult cmdRes = port.Command("ISET1?", parser, 200, cancelWaitEvent);
                    if (parser.ReceivedSomething() == false)
                    {
                        return new TestResult(false);
                    }
                    this.iset = parser.GetFloat();

                    parser = new VellemanCrapResponseParser();
                    cmdRes = port.Command("VSET1?", parser, 200, cancelWaitEvent);
                    if (parser.ReceivedSomething() == false)
                    {
                        return new TestResult(false);
                    }
                    this.vset = parser.GetFloat();

                    parser = new VellemanCrapResponseParser();
                    cmdRes = port.Command("IOUT1?", parser, 200, cancelWaitEvent);
                    if (parser.ReceivedSomething() == false)
                    {
                        return new TestResult(false);
                    }
                    this.iout = parser.GetFloat();

                    parser = new VellemanCrapResponseParser();
                    cmdRes = port.Command("VOUT1?", parser, 200, cancelWaitEvent);
                    if (parser.ReceivedSomething() == false)
                    {
                        return new TestResult(false);
                    }
                    this.vout = parser.GetFloat();

                    return new TestResult(true);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cancelHelper.OnCancel -= myDelegate;
                    PortOpenHelper.Close(port, portSettings);
                }
            }
        }

        #endregion

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }
}
