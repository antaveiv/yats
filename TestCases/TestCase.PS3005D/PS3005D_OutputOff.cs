﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Threading;
using yats.Attributes;
using yats.Ports;
using yats.Ports.Utilities;
using yats.Ports.Utilities.Parsers;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;

namespace yats.TestCase.PS3005D
{
    [Description("Turn off PS3005 output")]
    class PS3005D_OutputOff : IYatsTestCase
    {
        protected PortSettings portSettings;
        public PortSettings Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        public PS3005D_OutputOff()
        {
            this.portSettings = new PortSettings(new PortSettings.SerialPortInfo("COM1", 9600, 8, System.IO.Ports.Parity.None));
        }

        #region IYatsTestCase Members

        public ITestResult Execute()
        {
            AbstractPort port = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;
                try
                {
                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);
                    ReadOperationResult cmdRes = port.Command("OUT0", null, 200, cancelWaitEvent);
                    if (cmdRes.OperationResult != ReadOperationResult.Result.SUCCESS)
                    {
                        return new TestResult(false);
                    }

                    WaitForNBytes parser = new WaitForNBytes(1);
                    cmdRes = port.Command("STATUS?", parser, 100, cancelWaitEvent);
                    if (cmdRes.OperationResult != ReadOperationResult.Result.SUCCESS || parser.GetReceivedData().Length != 1)
                    {
                        return new TestResult(false);
                    }
                    byte status = parser.GetReceivedData()[0];

                    if ((status & 0x40) == 0)
                    {
                        return new TestResult(true);
                    }
                    return new TestResult(false);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cancelHelper.OnCancel -= myDelegate;
                    PortOpenHelper.Close(port, portSettings);
                }
            }
        }

        #endregion

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }
}
