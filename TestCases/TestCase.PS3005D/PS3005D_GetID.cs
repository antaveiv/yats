﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
using yats.Attributes;
using System.Text;
using yats.Ports;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.YatsNativeTestCase;
using System.Threading;
using System;
using yats.Ports.Utilities;
using yats.Ports.Utilities.Parsers;

namespace yats.TestCase.PS3005D
{
    [Description("Get PS3005 device id")]
    class PS3005D_GetID : IYatsTestCase
    {
        protected PortSettings portSettings;
        public PortSettings Port
        {
            get { return portSettings; }
            set { portSettings = value; }
        }

        protected string result;
        public string Result
        {
            get { return result; }
        }
        public PS3005D_GetID()
        {
            this.portSettings = new PortSettings(new PortSettings.SerialPortInfo("COM1", 9600, 8, System.IO.Ports.Parity.None));
        }

        #region IYatsTestCase Members

        public ITestResult Execute()
        {
            AbstractPort port = null;
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                EventHandler myDelegate = (sender, e) => { cancelWaitEvent.Set(); };
                cancelHelper.OnCancel += myDelegate;
                try
                {
                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);
                    TextLineParser parser = new TextLineParser(new char[] { '\r', '\n' });
                    ReadOperationResult cmdRes = port.Command("*IDN?", parser, 500, cancelWaitEvent);
                    this.result = parser.GetReceivedLine(Encoding.ASCII);
                    if (string.IsNullOrEmpty(result) == false)
                    {
                        return new TestResult(false);
                    }
                    return new TestResult(true);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cancelHelper.OnCancel -= myDelegate;
                    PortOpenHelper.Close(port, portSettings);
                }
            }
        }

        #endregion

        #region ICancellable Members

        private CancelHelper cancelHelper = new CancelHelper();

        public void Cancel()
        {
            cancelHelper.Cancel();
        }

        #endregion
    }
}
