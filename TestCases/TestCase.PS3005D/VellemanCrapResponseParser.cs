﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Globalization;
using System.Text;
using yats.Ports;

namespace yats.TestCase.PS3005D
{
    class VellemanCrapResponseParser : Parser
    {
        private Queue<byte> receivedData = new Queue<byte>(128);

        public VellemanCrapResponseParser()
        {
        }
        
        //the PSU may return "10.32M", just take the digits and .
        public override bool IncomingData(byte dataByte)
        {
            char c = (char)dataByte;
            if ((c >= '0' && c <= '9')||(c == '.'))
            {
                receivedData.Enqueue(dataByte);
            }

            return false;
        }

        public override ReadOperationResult GetResult()
        {
            if (receivedData.Count > 0)
            {
                return new ReadOperationResult(ReadOperationResult.Result.SUCCESS);
            }
            else
            {
                return new ReadOperationResult(ReadOperationResult.Result.TIMEOUT);
            }
        }
        
        public override byte[] GetReceivedData()
        {
            return this.receivedData.ToArray();
        }

        // returns received line as ASCII string
        public string GetReceivedLine()
        {
            return Encoding.ASCII.GetString(GetReceivedData());
        }

        public bool ReceivedSomething()
        {
            return receivedData.Count > 0;
        }

        public float GetFloat()
        {
            return float.Parse(GetReceivedLine(), CultureInfo.InvariantCulture);
        }
    }
}
