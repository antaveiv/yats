﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using Gui.Winforms.PublicAPI;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Forms;
using yats.TestRepositoryManager.YatsComposite.Properties;
using yats.Utilities;

namespace yats.Gui.Winforms.YatsApplication.Configuration
{
    public partial class CompositeTestRepositoryPanel : SettingsTabPanel
    {
        public CompositeTestRepositoryPanel()
        {
            InitializeComponent();
        }

        public override string GetText()
        {
            return "Composite tests";
        }

        StringCollection previous = Settings.Default.TestSearchPaths;

        public override void LoadSettings()
        {
            if (Settings.Default.TestSearchPaths.IsNullOrEmpty() == false)
            {
                textBox1.Lines = Settings.Default.TestSearchPaths.Cast<string>().ToArray();
            }
        }

        public override void SaveSettings()
        {
            if (previous.IsNullOrEmpty() && string.IsNullOrEmpty(textBox1.Text)) {
                return;
            }
            if (string.Equals(textBox1.Text, previous.IsNullOrEmpty()? "" : String.Join(Environment.NewLine, previous.Cast<string>().ToArray()))){
                return;
            }

            Settings.Default.TestSearchPaths = new StringCollection();
            Settings.Default.TestSearchPaths.AddRange(textBox1.Lines.Where(l => string.IsNullOrEmpty(l) == false).ToArray());
            Settings.Default.Save();
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                if (textBox1.Lines.Contains(dlg.SelectedPath))
                {
                    return;
                }
                var tmp = new List<string>(textBox1.Lines);
                tmp.Add(dlg.SelectedPath);
                textBox1.Lines = tmp.ToArray();
            }
        }

        public override int GetSortIndex()
        {
            return 1000;
        }
    }
}
