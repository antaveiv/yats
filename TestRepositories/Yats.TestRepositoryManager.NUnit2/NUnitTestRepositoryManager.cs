﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.Interface;
using yats.Utilities;
using System.IO;
using yats.TestRepositoryManager.NUnit2.Properties;
using NUnit.Core;
using log4net;
using System.Security.Cryptography;

namespace yats.TestRepositoryManager.NUnit2
{
    /// <summary>
    /// looks for NUnit test cases 
    /// </summary>
    public class NUnitTestRepositoryManager : ITestRepositoryManager
    {
        //stores a list of UniqueIDs associated with discovered test cases
        protected Dictionary<string, ITestCaseInfo> testCases = new Dictionary<string, ITestCaseInfo>();
        protected string[] SkipDlls =
        {
            "*vshost*",
            "Aga.Controls.dll",
            "Be.Windows.Forms.HexBox.dll",
            "*sqlite*",
            "DiffieHellman.dll",
            "*.XmlSerializers.dll",
            "gTrackBar.dll",
            "Gui.Winforms.Components.*",
            "IPAddressControlLib.dll",
            "log4net.dll",
            "nunit.framework.dll",
            "Org.Mentalis.Security.dll",
            "ServiceStack.Text.dll",
            "Tamir.SharpSSH.dll",
            "WeifenLuo.WinFormsUI.Docking.dll",
            "Gui.Winforms.PublicAPI.dll",
            "Yats.Attributes.dll",
            "Yats.ExecutionEngine.dll",
            "Yats.ExecutionQueue.dll",
            "Yats.TestRun.Cancel.dll",
            "Yats.TestRun.dll",
            "Yats.Utilities.dll",
            "Yats.Logging.File.dll",
            "Yats.Logging.Interface.dll",
            "Yats.Logging.Util.dll",
            "Yats.Logging.XmlSerializer.dll",
            "Yats.LoggingSettingsEditor.dll",
            "Yats.Ports.dll",
            "Yats.Ports.Settings.dll",
            "Yats.Ports.Utilities.dll",
            "Yats.TestCase.Interface.dll",
            "Yats.TestCase.Parameter.dll",
            "Yats.TestRun.Commands.dll",
            "Gui.Winforms.YatsApplication.exe",
            "Yats.TestRepositoryManager.YatsComposite.dll",
            "Yats.TestRepositoryManager.YatsNative.dll",
            "Yats.TestRepositoryManager.YatsNativeTestCase.dll",
            "Yats.TestRepositoryManager.Discovery.dll",
            "Yats.TestRepositoryManager.Interface.dll",
            "Yats.TestRepositoryManager.YatsCompositeSettings.dll",
            "InTheHand.Net.Personal.dll",
            "Newtonsoft.Json.dll",

            "ManagedWifi.dll",
            "nunit.*",
        };//TODO: remove, store list of hashes of previously discovered dll that don't contain test cases

        public void Initialize()
        {
            try
            {
                CoreExtensions.Host.InitializeService();

                testCases = new Dictionary<string, ITestCaseInfo>();
                var assemblyFileNames = yats.Utilities.AssemblyHelper.Instance.GetAssemblyFileNames(SkipDlls, Settings.Default.ExtraAssemblyLookupPaths?.Cast<string>(), true);
                foreach (string assemblyFileName in assemblyFileNames)
                {
                    try
                    {
                        if (IsKnownIgnoredAssembly(assemblyFileName))
                        {
                            continue;
                        }
                        if (BeforeLoadingTestAssembly != null)
                        {
                            CancelEventArgs e = new CancelEventArgs(false);
                            BeforeLoadingTestAssembly(this, new AssemblyLoadingEventArgs() { FullAssemblyPath = assemblyFileName, Manager = this, CancelEvent = e });
                            // some event handler has cancelled. For example, TestCaseOrganizer allows disabling selected test assemblies
                            if (e.Cancel)
                            {
                                OnTestAssemblyLoad?.Invoke(this, new AssemblyLoadedEventArgs() { FullAssemblyPath = assemblyFileName, IsLoaded = false, Manager = this });
                                continue;
                            }
                        }

                        bool foundTestCases = false;
                        
                        TestSuiteBuilder builder = new TestSuiteBuilder();
                        TestPackage testPackage = new TestPackage(assemblyFileName);
                        RemoteTestRunner remoteTestRunner = new RemoteTestRunner();
                        remoteTestRunner.Load(testPackage);
                        TestSuite suite = builder.Build(testPackage);
                        
                        foreach (var t in suite.Tests)
                        {
                            try {
                                LoadTest(testPackage, t, "", ref foundTestCases);
                            }
                            catch (Exception ex)
                            {
                                OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
                            }
                        }
                        if (foundTestCases)
                        {
                            OnTestAssemblyLoad?.Invoke(this, new AssemblyLoadedEventArgs() { FullAssemblyPath = assemblyFileName, IsLoaded = true, Manager = this });

                            if (Utilities.AssemblyHelper.IsFileInUnknownDirectory(assemblyFileName))
                            {
                                if (Settings.Default.ExtraAssemblyLookupPaths == null)
                                {
                                    Settings.Default.ExtraAssemblyLookupPaths = new System.Collections.Specialized.StringCollection();
                                }
                                string path = Path.GetDirectoryName(assemblyFileName);
                                bool found = false;
                                foreach (var extraPath in Settings.Default.ExtraAssemblyLookupPaths)
                                {
                                    if (extraPath.IsSubPathOf(path))
                                    {
                                        found = true;
                                        break;
                                    }
                                }

                                if (!found)
                                {
                                    // perhaps Yats was run from a different working directory. Remember this directory as a valid path for test cases
                                    Settings.Default.ExtraAssemblyLookupPaths.Add(path);
                                    Settings.Default.Save();
                                }
                            }
                        }
                        else
                        {
                            AddIgnoredAssembly(assemblyFileName);
                        }
                    }
                    catch (BadImageFormatException ex)
                    {
                        AddIgnoredAssembly(assemblyFileName);
                        OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
                    }
                    catch (Exception ex)
                    {
                        OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
                    }
                }
            }            
            catch (Exception ex)
            {
                OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
            }
        }

        private bool IsKnownIgnoredAssembly(string assemblyFileName)
        {
            if (Settings.Default.IgnoredAssemblies != null)
            {
                using (FileStream fs = new FileStream(assemblyFileName, FileMode.Open, FileAccess.Read))
                using (BufferedStream bs = new BufferedStream(fs))
                using (var cryptoProvider = new SHA1CryptoServiceProvider())
                {
                    string hash = ByteArray.ToHexString(cryptoProvider.ComputeHash(bs));
                    return Settings.Default.IgnoredAssemblies.Contains(assemblyFileName + "|" + hash);
                }
            }
            return false;
        }

        private void AddIgnoredAssembly(string assemblyFileName)
        {
            if (Settings.Default.IgnoredAssemblies == null)
            {
                Settings.Default.IgnoredAssemblies = new System.Collections.Specialized.StringCollection();
            }

            using (FileStream fs = new FileStream(assemblyFileName, FileMode.Open, FileAccess.Read))
            using (BufferedStream bs = new BufferedStream(fs))
            using (var cryptoProvider = new SHA1CryptoServiceProvider())
            {
                string hash = ByteArray.ToHexString(cryptoProvider.ComputeHash(bs));
                foreach (var s in Settings.Default.IgnoredAssemblies)
                {
                    if (s.StartsWith(assemblyFileName))// existing with old hash
                    {
                        Settings.Default.IgnoredAssemblies.Remove(s);
                        break;
                    }
                }
                Settings.Default.IgnoredAssemblies.Add(assemblyFileName + "|" + hash);
                Settings.Default.Save();
            }
        }

        private void LoadTest(TestPackage testPackage, object t, string uniqueId, ref bool found)
        {            
            if (t is TestSuite)
            {
                TestSuite ts = t as TestSuite;
                foreach (var t2 in ts.Tests)
                {
                    LoadTest(testPackage, t2, uniqueId + "." + ts.TestName.Name, ref found);
                }
            }
            if (t is TestFixture tf)
            {
                string id = uniqueId + "." + tf.TestName.Name;
                //Console.WriteLine(id);
                NUnitTestCaseInfo testCaseInfo = new NUnitTestCaseInfo(testPackage, tf.TestName.Name, tf, this) { };
                this.testCases.Add(GetUniqueTestId(testCaseInfo.TestCase), testCaseInfo);
                found = true;
            }
            if (t is NUnitTestMethod)
            {
                //Not listing every single test method as a separate test case because test fixtures have setup/teardown code that need to be called. Not handling that for now.

                //NUnitTestMethod tm = (NUnitTestMethod)t;
                //uniqueId += ":" + tm.TestName.Name;
                //Console.WriteLine(uniqueId);
                //manager.testCases.Add(uniqueId, new NUnitTestCaseInfo() { });
                //found = true;
            }
        }

        public IList<ITestCaseInfo> TestCases
        {
            get
            { return testCases.Values.ToList(); }
        }

        public string Name { get { return "NUnit tests"; } }

        // Raised when the repository manager is about to analyze an assembly 
        public event EventHandler<AssemblyLoadingEventArgs> BeforeLoadingTestAssembly;
        // Raised after the repository manager loads a test case assembly 
        public event EventHandler<AssemblyLoadedEventArgs> OnTestAssemblyLoad;

        public event EventHandler<ExceptionEventArgs> OnError;

        public string GetUniqueTestId(ITestCase testCase)
        {
            if (!(testCase is NUnitTestCase tc))
            {
                throw new Exception("Not NUnit test case");
            }
            return this.GetType().FullName + "|" + tc.TestFixture.TestName.Name;
        }

        public ITestCase GetByUniqueId(string testCaseUniqueId)
        {
            if (testCases == null)
            {
                return null;
            }
            if (testCases.TryGetValue(testCaseUniqueId, out ITestCaseInfo result))
            {
                NUnitTestCaseInfo tcInfo = result as NUnitTestCaseInfo;
                return new NUnitTestCase() { TestFixture = tcInfo.TestFixture, TestPackage = tcInfo.TestPackage };
            }
            return null;
        }

        public bool IsMyTestCase(ITestCase testCase)
        {
            if (testCases == null || testCase == null)
            {
                return false;
            }
            if ((testCase is NUnitTestCase) == false)
            {
                return false;
            }
            string testCaseUniqueId = GetUniqueTestId(testCase);
            return testCases.ContainsKey(testCaseUniqueId);
        }

        public bool IsMyTestCase(string testCaseUniqueId)
        {
            if (testCases == null || testCaseUniqueId == null)
            {
                return false;
            }
            return testCases.ContainsKey(testCaseUniqueId);
        }

        private static ILog Logger = LogManager.GetLogger("NUnit");

        private class RunEventListener : EventListener
        {
            public void RunFinished(Exception exception)
            {
                Logger.Error("Run finished", exception);
            }

            public void RunFinished(global::NUnit.Core.TestResult result)
            {
                Logger.Info("Run finished: " + result.ResultState);
            }

            public void RunStarted(string name, int testCount)
            {
                Logger.Info("Run started: " + name);
            }

            public void SuiteFinished(global::NUnit.Core.TestResult result)
            {
                Logger.Info("Suite finished: " + result.ResultState);
            }

            public void SuiteStarted(TestName testName)
            {
                Logger.Info("Started: " + testName.Name);
            }

            public void TestFinished(global::NUnit.Core.TestResult result)
            {
                Logger.Info("Test finished: " + result.ResultState);
                if (result.IsError)
                {
                    Logger.Debug(result.StackTrace);
                }                
            }

            public void TestOutput(TestOutput testOutput)
            {
                Logger.Info(testOutput.Text);
            }

            public void TestStarted(TestName testName)
            {
                Logger.Info("Started: " + testName.Name);
            }

            public void UnhandledException(Exception exception)
            {
                Logger.Error("Exception", exception);
            }
        }

        private class NUnitTestFilter : ITestFilter
        {
            public bool IsEmpty
            {
                get
                {
                    return false;
                }
            }

            public bool Match(ITest test)
            {
                return true;
            }

            public bool Pass(ITest test)
            {
                return true;
            }
        }

        private static readonly object lockObject = new object();

        public ITestResult Execute(ITestCase test, IList<IParameter> testParameters, IGlobalParameterCollection globalParameters, object testRunner, string path)
        {
            if (!(test is NUnitTestCase tc))
            {
                return new TestResultWithData(ResultEnum.INCONCLUSIVE, "Not NUnit test case");
            }

            lock (lockObject) // running in parallel causes race conditions in NUnit engine
            {
                TestSuiteBuilder builder = new TestSuiteBuilder();
                RemoteTestRunner remoteTestRunner = new RemoteTestRunner();
                remoteTestRunner.Load(tc.TestPackage);
                TestSuite suite = builder.Build(tc.TestPackage);

                var result = tc.TestFixture.Run(new RunEventListener(), new NUnitTestFilter());
                switch (result.ResultState)
                {
                    case ResultState.Success:
                        return new yats.TestCase.Interface.TestResult(ResultEnum.PASS);
                    case ResultState.Cancelled:
                        return new yats.TestCase.Interface.TestResult(ResultEnum.CANCELED);
                    case ResultState.Error:
                    case ResultState.Failure:
                        return new yats.TestCase.Interface.TestResult(ResultEnum.FAIL);
                    case ResultState.Ignored:
                    case ResultState.NotRunnable:
                    case ResultState.Skipped:
                        return new yats.TestCase.Interface.TestResult(ResultEnum.NOT_RUN);
                    case ResultState.Inconclusive:
                    default:
                        return new yats.TestCase.Interface.TestResult(ResultEnum.INCONCLUSIVE);
                }
            }
        }
        
        public bool GetParameterValue(ITestCase testCase, IParameter paramInfo, out object result)
        {
            result = null;
            return false;
        }

        public bool GetParameterDefault(ITestCase testCase, IParameter paramInfo, out object result)
        {
            result = null;
            return false;            
        }

        public void SetParameterValue(ITestCase testCase, IParameter paramInfo, object value)
        {
            throw new NotImplementedException();
        }

        public bool GetTestResult(ITestCase testCase, IParameter paramInfo, out object value)
        {
            value = null;
            return false;
        }

        public IList<IParameter> GetParameters(ITestCase testCase)
        {
            return new List<IParameter>();
        }

        private bool AssignDefaultParameterValue(ITestCase testCase, TestParameterInfo parameterInfo, bool tryFindingSuitableParamType)
        {
            return false;
        }

        [DebuggerStepThrough]
        public string GetTestName(ITestCase testCase)
        {
            if (!(testCase is NUnitTestCase tc))
            {
                throw new Exception("Not NUnit test case");
            }
            return tc.TestFixture.TestName.Name;
        }

        public string GetTestDescription(ITestCase testCase)
        {
            //TODO find description attribute on test suite class
            return "";
        }

        public bool CanParameterBeNull(ITestCase testCase, IParameter param)
        {
            return true;
        }
                
        public bool Cancel(ITestCase testCase)
        {
            return false;
        }

        public string GetGroupName(ITestCase testCase)
        {
            if (!(testCase is NUnitTestCase tc))
            {
                throw new Exception("Not NUnit test case");
            }
            return tc.TestFixture.ClassName;
        }
    }
}
