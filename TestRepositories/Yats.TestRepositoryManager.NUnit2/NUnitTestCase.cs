﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Core;
using yats.TestCase.Interface;

namespace yats.TestRepositoryManager.NUnit2
{
    class NUnitTestCase : ITestCase
    {
        public TestFixture TestFixture { get; internal set; }
        public TestPackage TestPackage { get; internal set; }
    }
}
