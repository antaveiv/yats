﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Attributes;
using yats.TestCase.Interface;

namespace TestRepositoryManager.YatsNativeTests
{
    class DefaultValueTestCaseStub : ITestCase
    {
        protected byte? shouldBe42 = 42;
        public byte? ShouldBe42
        {
            get { return shouldBe42; }
            set { shouldBe42 = value; }
        }

        protected byte? shouldBeNull1;
        public byte? ShouldBeNull1
        {
            get { return shouldBeNull1; }
            set { shouldBeNull1 = value; }
        }

        protected byte shouldBe4 = 4;
        public byte ShouldBe4
        {
            get { return shouldBe4; }
            set { shouldBe4 = value; }
        }

        protected string shouldBeDefault = "default";
        [CanBeNull]
        public string ShouldBeDefault
        {
            get { return shouldBeDefault; }
            set { shouldBeDefault = value; }
        }

        protected string shouldBeNull2;
        [CanBeNull]
        public string ShouldBeNull2
        {
            get { return shouldBeNull2; }
            set { shouldBeNull2 = value; }
        }
    }
}
