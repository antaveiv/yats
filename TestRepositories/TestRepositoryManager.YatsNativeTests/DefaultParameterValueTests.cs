﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Linq;
using NUnit.Framework;
using yats.TestCase.Interface;

namespace TestRepositoryManager.YatsNativeTests
{
    [TestFixture]
    public class DefaultParameterValueTests
    {
        static yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();

        [Test]
        public void DefaultParameterValueTest()
        {
            var tc = new DefaultValueTestCaseStub();
            Assert.IsTrue(discover(tc, "ShouldBe42"));
            var discoveredParams = manager.GetParameters(tc);
            object value; 
            var param = discoveredParams.First(x => x.Name == "ShouldBe42");
            Assert.IsTrue(param.IsParameter);            
            Assert.IsTrue(manager.GetParameterDefault(tc, param, out value));
            Assert.AreEqual((byte)42, value);

            param = discoveredParams.First(x => x.Name == "ShouldBeNull1");
            Assert.IsTrue(param.IsParameter);
            Assert.IsTrue(manager.GetParameterDefault(tc, param, out value));
            Assert.IsNull(value);

            param = discoveredParams.First(x => x.Name == "ShouldBe4");
            Assert.IsTrue(param.IsParameter);
            Assert.IsTrue(manager.GetParameterDefault(tc, param, out value));
            Assert.AreEqual((byte)4, value);

            param = discoveredParams.First(x => x.Name == "ShouldBeDefault");
            Assert.IsTrue(param.IsParameter);
            Assert.IsTrue(manager.GetParameterDefault(tc, param, out value));
            Assert.AreEqual("default", value);

            param = discoveredParams.First(x => x.Name == "ShouldBeNull2");
            Assert.IsTrue(param.IsParameter);
            Assert.IsTrue(manager.GetParameterDefault(tc, param, out value));
            Assert.IsNull(value);
        }

        private bool discover(ITestCase tc, string parameterName)
        {
            var discoveredParams = manager.GetParameters(tc);
            return discoveredParams.Count(x => x.Name == parameterName) == 1;
        }
    }
}
