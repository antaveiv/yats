﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Linq;
using NUnit.Framework;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;

namespace TestRepositoryManager.YatsNativeTests
{
    [TestFixture]
    public class ParameterDiscovery
    {
        static yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();

        [Test]
        public void FindProperty()
        {            
            Assert.IsTrue(discover(new TestCaseStub(), "NullableByteParameter"));
        }

        //find inherited property
        [Test]
        public void FindInheritedProperty()
        {            
            Assert.IsTrue(discover(new InheritedTestCaseStub(), "NullableByteParameter"));
        }

        //ignore static property
        [Test]
        public void IgnoreStaticProperty()
        {
            Assert.IsFalse(discover(new InheritedTestCaseStub(), "StaticParameter"));
        }

        private bool discover(ITestCase tc, string parameterName)
        {
            var discoveredParams = manager.GetParameters(tc);
            return discoveredParams.Count(x => x.Name == parameterName) == 1;
        }

        //when property has a private setter, should be found as test result
        [Test]
        public void PrivateSetterIsResult()
        {
            var tc = new InheritedTestCaseStub();
            Assert.IsTrue(discover(tc, "PrivateSetter"));
            var discoveredParams = manager.GetParameters(tc);
            var param = discoveredParams.First(x=>x.Name == "PrivateSetter");
            Assert.IsFalse(param.IsParameter);
            Assert.IsTrue(param.IsResult);
        }
        
        //ignore private getter
        [Test]
        public void IgnorePrivateGetter()
        {
            Assert.IsFalse(discover(new InheritedTestCaseStub(), "PrivateGetter"));
        }

        //non existing parameter
        [Test]
        public void DontFindNonExisting()
        {
            Assert.IsFalse(discover(new InheritedTestCaseStub(), "IDoNotExist"));
        }

        //ignore private property
        [Test]
        public void IgnorePrivate()
        {
            Assert.IsFalse(discover(new InheritedTestCaseStub(), "ProtectedParameter"));
            Assert.IsFalse(discover(new InheritedTestCaseStub(), "PrivateParameter"));
        }
                
        //canbenull attribute
        [Test]
        public void CanBeNull()
        {
            var tc = new CanBeNullParameterStub();
            var discoveredParams = manager.GetParameters(tc);
            Assert.AreEqual(12, discoveredParams.Count);

            Assert.IsFalse(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CantBeNull1")));
            Assert.IsFalse(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CantBeNull2")));
            Assert.IsFalse(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CantBeNull3")));
            Assert.IsFalse(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CantBeNull4")));
            Assert.IsFalse(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CantBeNull5")));
            Assert.IsFalse(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CantBeNull6")));

            Assert.IsTrue(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CanBeNull1")));
            Assert.IsTrue(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CanBeNull2")));
            Assert.IsTrue(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CanBeNull3")));
            Assert.IsTrue(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CanBeNull4")));
            Assert.IsTrue(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CanBeNull5")));
            Assert.IsTrue(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CanBeNull6")));
        }

        [Test]
        public void NullableEnumParameter()
        {
            var tc = new EnumParameterTestStub();
            var discoveredParams = manager.GetParameters(tc);
            Assert.AreEqual(7, discoveredParams.Count);

            Assert.IsFalse(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CantBeNull1")));
            Assert.IsFalse(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CantBeNull2")));
            Assert.IsFalse(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CantBeNull3")));

            Assert.IsTrue(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CanBeNull1")));
            Assert.IsTrue(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CanBeNull2")));
            Assert.IsTrue(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CanBeNull3")));
            Assert.IsTrue(manager.CanParameterBeNull(tc, discoveredParams.First(x => x.Name == "CanBeNull4")));
        }
         
        //find [parameter] field
        [Test]
        public void FieldParameterTest()
        {
            var tc = new FieldParameterTestCaseStub();
            Assert.IsTrue(discover(tc, "FindMe1"));
            Assert.IsTrue(discover(tc, "FindMe2"));
            Assert.IsFalse(discover(tc, "DontFindMe1"));
            Assert.IsFalse(discover(tc, "DontFindMe2"));
            Assert.IsFalse(discover(tc, "DontFindMe3"));
            //ignore field without [parameter]
            Assert.IsFalse(discover(tc, "DontFindMe4"));            
            //ignore field with both [parameter] and [result]
            Assert.IsFalse(discover(tc, "DontFindMe5"));
        }
        
        //find inherited field
        [Test]
        public void InheritedFieldParameterTest()
        {
            var tc = new InheritedFieldParameterTestCaseStub();
            Assert.IsTrue(discover(tc, "FindMe1"));
            Assert.IsTrue(discover(tc, "FindMe2"));
            Assert.IsFalse(discover(tc, "DontFindMe1"));
            Assert.IsFalse(discover(tc, "DontFindMe2"));
            Assert.IsFalse(discover(tc, "DontFindMe3"));
            Assert.IsFalse(discover(tc, "DontFindMe4"));
        }

        //find [result] field
        [Test]
        public void FindResultFields()
        {
            var tc = new FieldResultTestCaseStub();
            checkResults(tc);
        }

        //find inherited [result] field
        [Test]
        public void FindInheritedResultFields()
        {
            var tc = new InheritedFieldResultTestCaseStub();
            checkResults(tc);
        }

        // reuse between FindResultFields and
        private void checkResults(ITestCase tc)
        {
            Assert.IsTrue(discover(tc, "FindMe1"));
            Assert.IsTrue(discover(tc, "FindMe2"));
            Assert.IsFalse(discover(tc, "DontFindMe1"));
            Assert.IsFalse(discover(tc, "DontFindMe2"));
            Assert.IsFalse(discover(tc, "DontFindMe3"));
            Assert.IsFalse(discover(tc, "DontFindMe4"));

            var discoveredParams = manager.GetParameters(tc);
            IParameter param = discoveredParams.First(x => x.Name == "FindMe1");
            Assert.IsFalse(param.IsParameter);
            Assert.IsTrue(param.IsResult);

            param = discoveredParams.First(x => x.Name == "FindMe2");
            Assert.IsFalse(param.IsParameter);
            Assert.IsTrue(param.IsResult);
        }

        //find result properties
        [Test]
        public void FindResultProperties()
        {
            var tc = new ResultDiscoveryTestCaseStub();
            
            Assert.IsTrue(discover(tc, "FindResult1"));
            Assert.IsTrue(discover(tc, "FindResult2"));

            var discoveredParams = manager.GetParameters(tc);
            IParameter param = discoveredParams.First(x => x.Name == "FindResult1");
            Assert.IsFalse(param.IsParameter);
            Assert.IsTrue(param.IsResult);
            param = discoveredParams.First(x => x.Name == "FindResult2");
            Assert.IsFalse(param.IsParameter);
            Assert.IsTrue(param.IsResult);

            Assert.IsFalse(discover(tc, "DontFindMe1"));
            Assert.IsFalse(discover(tc, "DontFindMe2"));
            Assert.IsFalse(discover(tc, "DontFindMe3"));
            Assert.IsFalse(discover(tc, "DontFindMe4"));
        }
        
        //TODO automatic conversion value->array->list
    }
}
