﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.YatsNative;
using yats.TestRepositoryManager.YatsNativeTestCase;

namespace TestRepositoryManager.YatsNativeTests
{
    class TestCaseStub : IYatsTestCase
    {
        protected byte? nullableByteParameter = 42;
        public byte? NullableByteParameter
        {
            get { return nullableByteParameter; }
            set { nullableByteParameter = value; }
        }

        public static bool StaticParameter
        {
            get;
            set;
        }

        public bool PrivateSetter
        {
            get;
            private set;
        }

        public bool PrivateGetter
        {
            private get;
            set;
        }

        protected bool ProtectedParameter
        {
            get;
            set;
        }

        private bool PrivateParameter
        {
            get;
            set;
        }

        public yats.TestCase.Interface.ITestResult Execute()
        {
            throw new NotImplementedException();
        }
    }

    class ResultDiscoveryTestCaseStub : ITestCase
    {

        public bool FindResult1
        {
            get;
            private set;
        }

        public bool FindResult2
        {
            get { return false; }
        }

        public static bool DontFindResult1
        {
            get { return false; }
        }

        protected bool DontFindResult2
        {
            get { return false; }
        }

        private bool DontFindResult3
        {
            get { return false; }
        }
    }

    class InheritedTestCaseStub : TestCaseStub
    {
    }

    public class FieldParameterTestCaseStub : ITestCase
    {
        [Parameter]
        public bool FindMe1;
        [Parameter, CanBeNull]
        public string FindMe2;
        [Parameter]
        public static bool DontFindMe1;
        [Parameter]
        protected bool DontFindMe2;
        [Parameter]
        private bool DontFindMe3;
        public bool DontFindMe4;
        [Parameter, Result]
        public bool DontFindMe5;

        public void test()
        {
            DontFindMe3 = true;
        }
    }

    public class InheritedFieldParameterTestCaseStub : FieldParameterTestCaseStub
    {
    }

    public class FieldResultTestCaseStub : ITestCase
    {
        [Result]
        public bool FindMe1;
        [Result, CanBeNull]
        public string FindMe2;
        [Result]
        public static bool DontFindMe1;
        [Result]
        protected bool DontFindMe2;
        [Result]
        private bool DontFindMe3;
        public bool DontFindMe4;

        public void test()
        {
            DontFindMe3 = true;
        }
    }

    public class InheritedFieldResultTestCaseStub : FieldResultTestCaseStub
    {
    }

    public class CanBeNullParameterStub : ITestCase
    {
        public bool CantBeNull1
        {
            get;
            set;
        }

        [CanBeNull]
        public bool CantBeNull2
        {
            get;
            set;
        }

        public string CantBeNull3
        {
            get;
            set;
        }

        [Parameter]
        public bool CantBeNull4;

        [Parameter]
        public bool CantBeNull5;

        [Parameter]
        public string CantBeNull6;

        public bool? CanBeNull1
        {
            get;
            set;
        }

        public Nullable<bool> CanBeNull2
        {
            get;
            set;
        }

        [CanBeNull]
        public string CanBeNull3
        {
            get;
            set;
        }

        [Parameter]
        public bool? CanBeNull4;

        [Parameter]
        public Nullable<bool> CanBeNull5;

        [Parameter, CanBeNull]
        public string CanBeNull6;
    }

    public class EnumParameterTestStub : ITestCase
    {
        public enum ParamEnum : byte
        {
            ONE,
            TWO,
            WTF
        }

        public ParamEnum CantBeNull1
        {
            get;
            set;
        }

        [CanBeNull]
        public ParamEnum CantBeNull2
        {
            get;
            set;
        }

        [Parameter]
        public ParamEnum CantBeNull3;
        
        public ParamEnum? CanBeNull1
        {
            get;
            set;
        }

        [Parameter]
        public ParamEnum? CanBeNull2;

        [Parameter, CanBeNull]
        public ParamEnum? CanBeNull3;

        [Parameter]
        public Nullable<ParamEnum> CanBeNull4;
    }

    public class ResultValueTestCaseStub : ITestCase
    {
        [Result]
        public int Res1 = 53148;

        [Result, CanBeNull]
        public string Res2 = "FindMe2";

        public int Res3
        {
            get {return 87745;}
            private set{}
        }

        public string Res4
        {
            get { return "jajajai"; }
        }

    }
}
