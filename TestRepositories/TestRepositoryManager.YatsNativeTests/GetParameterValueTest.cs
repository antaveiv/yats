﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Linq;
using NUnit.Framework;

namespace TestRepositoryManager.YatsNativeTests
{
    [TestFixture]
    public class GetParameterValueTest
    {
        static yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();

        //sets value via manager and reads it back
        [Test]
        public void ReadBackParameters()
        {
            var tc = new DefaultValueTestCaseStub();

            var discoveredParams = manager.GetParameters(tc);
            object value; 
            var param = discoveredParams.First(x => x.Name == "ShouldBe42");
            Assert.IsTrue(param.IsParameter);
            manager.SetParameterValue(tc, param, (byte)99);
            Assert.IsTrue(manager.GetParameterValue(tc, param, out value));
            Assert.AreEqual((byte)99, value);

            param = discoveredParams.First(x => x.Name == "ShouldBeNull1");
            Assert.IsTrue(param.IsParameter);
            manager.SetParameterValue(tc, param, (byte)13); 
            Assert.IsTrue(manager.GetParameterValue(tc, param, out value));
            Assert.AreEqual((byte)13, value);

            param = discoveredParams.First(x => x.Name == "ShouldBe4");
            Assert.IsTrue(param.IsParameter);
            Assert.IsTrue(manager.GetParameterValue(tc, param, out value));
            Assert.AreEqual((byte)4, value);
            manager.SetParameterValue(tc, param, (byte)77);
            Assert.IsTrue(manager.GetParameterValue(tc, param, out value));
            Assert.AreEqual((byte)77, value);

            param = discoveredParams.First(x => x.Name == "ShouldBeDefault");
            Assert.IsTrue(param.IsParameter);
            manager.SetParameterValue(tc, param, "balls");
            Assert.IsTrue(manager.GetParameterValue(tc, param, out value));
            Assert.AreEqual("balls", value);

            param = discoveredParams.First(x => x.Name == "ShouldBeNull2");
            Assert.IsTrue(param.IsParameter);
            manager.SetParameterValue(tc, param, "24B");
            Assert.IsTrue(manager.GetParameterValue(tc, param, out value));
            Assert.AreEqual("24B", value);
        }
    }
}
