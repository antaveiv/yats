﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Linq;
using NUnit.Framework;

namespace TestRepositoryManager.YatsNativeTests
{
    [TestFixture]
    public class GetResultValueTest
    {
        static yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();

        //sets value via manager and reads it back
        [Test]
        public void GetResultValue()
        {
            var tc = new ResultValueTestCaseStub();

            var discoveredParams = manager.GetParameters(tc);
            object value;
            var param = discoveredParams.First(x => x.Name == "Res1");
            Assert.IsTrue(param.IsResult);
            Assert.IsTrue(manager.GetTestResult(tc, param, out value));
            Assert.AreEqual((int)53148, value);

            param = discoveredParams.First(x => x.Name == "Res2");
            Assert.IsTrue(param.IsResult);
            Assert.IsTrue(manager.GetTestResult(tc, param, out value));
            Assert.AreEqual("FindMe2", value);

            param = discoveredParams.First(x => x.Name == "Res3");
            Assert.IsTrue(param.IsResult);
            Assert.IsTrue(manager.GetTestResult(tc, param, out value));
            Assert.AreEqual(87745, value);

            param = discoveredParams.First(x => x.Name == "Res4");
            Assert.IsTrue(param.IsResult);
            Assert.IsTrue(manager.GetTestResult(tc, param, out value));
            Assert.AreEqual("jajajai", value);
        }
    }
}
