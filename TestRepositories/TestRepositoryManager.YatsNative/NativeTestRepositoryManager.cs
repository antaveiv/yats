﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.Interface;
using yats.TestRepositoryManager.YatsNativeTestCase;
using yats.Utilities;
using yats.TestRepositoryManager.YatsNative.Properties;
using System.IO;
using System.Security.Cryptography;

namespace yats.TestRepositoryManager.YatsNative
{
    /// <summary>
    /// looks for test cases implementing TestCase.Yats.IYatsTestCase
    /// </summary>
    // to disable for certain platforms, use e.g.: [UnavailablePlatform(System.PlatformID.Unix)]
	public class NativeTestRepositoryManager : ITestRepositoryManager
    {
        //stores a list of UniqueIDs associated with discovered test cases
        protected Dictionary<string, ITestCaseInfo> testCases = new Dictionary<string, ITestCaseInfo>();
       
        public void Initialize()
        {
            try
            {
                testCases = new Dictionary<string, ITestCaseInfo>();

                foreach (string assemblyFileName in yats.Utilities.AssemblyHelper.Instance.GetAssemblyFileNames(null, Settings.Default.ExtraAssemblyLookupPaths?.Cast<string>(), false))
                {
                    try
                    {
                        if (IsKnownIgnoredAssembly(assemblyFileName))
                        {
                            continue;
                        }

                        //skip loading assemblies with the same name if they are already loaded to application
                        /*foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
                        {
                            try
                            {
                                if (Path.GetFileName(a.Location) == Path.GetFileName(assemblyFileName))
                                {
                                    continue;
                                }
                            }
                            catch (System.NotImplementedException)
                            {
                            }
                            //continue;
                        }*/
                        
                        if (BeforeLoadingTestAssembly != null)
                        {
                            CancelEventArgs e = new CancelEventArgs(false);
                            BeforeLoadingTestAssembly (this, new AssemblyLoadingEventArgs () { FullAssemblyPath = assemblyFileName, Manager = this, CancelEvent = e });
                            // some event handler has cancelled. For example, TestCaseOrganizer allows disabling selected test assemblies
                            if (e.Cancel)
                            {
                                OnTestAssemblyLoad?.Invoke(this, new AssemblyLoadedEventArgs() { FullAssemblyPath = assemblyFileName, IsLoaded = false, Manager = this });
                                continue;
                            }
                        }
                        //Debug.WriteLine("Loading " + assemblyFileName);
                        Assembly asm = Assembly.LoadFrom( assemblyFileName );
                        Type[] typesInAssembly = asm.GetTypes( );
                        bool foundTestCases = false;
                        
                        foreach(Type type in typesInAssembly)
                        {
                            try
                            {
                                if (null != type.GetInterface(typeof(IYatsTestCase).FullName))
                                {
                                    // can not instantiate abstract types, skip
                                    if (type.IsAbstract || type.IsInterface)
                                    {
                                        continue;
                                    }

                                    if (UnavailablePlatformAttribute.IsPlatformSupported(type) == false)
                                    {
                                        continue;
                                    }

                                    // We found a type, load it
                                    /*Console.WriteLine(string.Format( "Assembly: {0} has a type {1} that " +
                                                                     "implements {2}",
                                                                     asm.FullName, type.FullName, typeof(ITestCase).FullName));
                                    */
                                    IYatsTestCase instance = (IYatsTestCase)Activator.CreateInstance(type);

                                    testCases.Add(GetUniqueTestId(instance), new TestCaseInfo(instance, this));
                                    foundTestCases = true;
                                }

                            }
                            catch (Exception ex)
                            {
                                OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
                            }
                        }
                        if (foundTestCases)
                        {
                            OnTestAssemblyLoad?.Invoke(this, new AssemblyLoadedEventArgs() { FullAssemblyPath = assemblyFileName, IsLoaded = true, Manager = this });
                            if (AssemblyHelper.IsFileInUnknownDirectory(assemblyFileName))
                            {
                                if (Settings.Default.ExtraAssemblyLookupPaths == null)
                                {
                                    Settings.Default.ExtraAssemblyLookupPaths = new System.Collections.Specialized.StringCollection();
                                }
                                string path = Path.GetDirectoryName(assemblyFileName);
                                if (Settings.Default.ExtraAssemblyLookupPaths.Contains(path) == false)
                                {
                                    Settings.Default.ExtraAssemblyLookupPaths.Add(path);
                                    Settings.Default.Save();
                                }
                            }
                        }
                        else
                        {
                            AddIgnoredAssembly(assemblyFileName);
                        }
                    }
                    catch (BadImageFormatException ex)
                    {
                        AddIgnoredAssembly(assemblyFileName);
                        OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
                    }
                    catch (Exception ex)
                    {
                        OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
                    }
                }
            }
            catch(Exception ex)
            {
                OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
            }
        }


        private bool IsKnownIgnoredAssembly(string assemblyFileName)
        {
            if (Settings.Default.IgnoredAssemblies != null)
            {
                using (FileStream fs = new FileStream(assemblyFileName, FileMode.Open, FileAccess.Read))
                using (BufferedStream bs = new BufferedStream(fs))
                using (var cryptoProvider = new SHA1CryptoServiceProvider())
                {
                    string hash = ByteArray.ToHexString(cryptoProvider.ComputeHash(bs));
                    return Settings.Default.IgnoredAssemblies.Contains(assemblyFileName + "|" + hash);
                }
            }
            return false;
        }

        private void AddIgnoredAssembly(string assemblyFileName)
        {
            if (Settings.Default.IgnoredAssemblies == null)
            {
                Settings.Default.IgnoredAssemblies = new System.Collections.Specialized.StringCollection();
            }

            using (FileStream fs = new FileStream(assemblyFileName, FileMode.Open, FileAccess.Read))
            using (BufferedStream bs = new BufferedStream(fs))
            using (var cryptoProvider = new SHA1CryptoServiceProvider())
            {
                string hash = ByteArray.ToHexString(cryptoProvider.ComputeHash(bs));
                foreach (var s in Settings.Default.IgnoredAssemblies)
                {
                    if (s.StartsWith(assemblyFileName))// existing with old hash
                    {
                        Settings.Default.IgnoredAssemblies.Remove(s);
                        break;
                    }
                }
                Settings.Default.IgnoredAssemblies.Add(assemblyFileName + "|" + hash);
                Settings.Default.Save();
            }
        }

        public IList<ITestCaseInfo> TestCases
        {
            [DebuggerStepThrough]
            get { return testCases.Values.ToList(); }
        }

        public string Name { get { return "Native Yats tests"; } }

        // Raised when the repository manager is about to analyze an assembly 
        public event EventHandler<AssemblyLoadingEventArgs> BeforeLoadingTestAssembly;
        // Raised after the repository manager loads a test case assembly 
        public event EventHandler<AssemblyLoadedEventArgs> OnTestAssemblyLoad;

        public event EventHandler<ExceptionEventArgs> OnError;
        
        [DebuggerStepThrough]
        public string GetUniqueTestId(ITestCase testCase)
        {
            return this.GetType().FullName + "|" + testCase.GetType( ).FullName;
        }

        public ITestCase GetByUniqueId(string testCaseUniqueId)
        {
            if (testCases == null){
                return null;
            }
            if (testCases.TryGetValue(testCaseUniqueId, out ITestCaseInfo result))
            {
                return (ITestCase)Activator.CreateInstance(result.TestCase.GetType());
            }
            return null;
        }
        
        public bool IsMyTestCase(ITestCase testCase)
        {
            if (testCases == null || testCase == null)
            {
                return false;
            }
            string testCaseUniqueId = GetUniqueTestId(testCase);
            return testCases.ContainsKey(testCaseUniqueId);
        }

        public bool IsMyTestCase(string testCaseUniqueId)
        {
            if (testCases == null || testCaseUniqueId == null)
            {
                return false;
            }
            return testCases.ContainsKey(testCaseUniqueId);
        }

        public ITestResult Execute(ITestCase test, IList<IParameter> testParameters, IGlobalParameterCollection globalParameters, object testRunner, string path)
        {
            if (!(test is IYatsTestCase testCase))
            {
                return new TestResultWithData(ResultEnum.INCONCLUSIVE, "Not a native Yats test case");
            }

            AssignParameters( testCase, testParameters, globalParameters );
            return testCase.Execute( );
        }

        private void AssignParameters(IYatsTestCase testCase, IList<IParameter> testCaseParameters, IGlobalParameterCollection globalParameters)
        {
            List<MemberInfo> members = new List<MemberInfo>();
            PropertyInfo[] properties = GetProperties(testCase);
            foreach(PropertyInfo property in properties)
            {
                // skip read-only properties
                if(!property.CanWrite) 
                { 
                    continue; 
                }
                
                MethodInfo mset = property.GetSetMethod( false );

                // Get and set methods have to be public
                if(mset == null) { continue; }

                members.Add(property);
            }
            var fields = GetFields(testCase);
            foreach (var field in fields)
            {
                if (field.HasAttribute(typeof(Attributes.ParameterAttribute)))
                {
                    members.Add(field);
                }
            }

            foreach (var member in members)
            {
                object paramValue = null;
                IParameter currentParameterInfo = null;
                bool found = false;
                foreach (var param in testCaseParameters)
                {
                    if (param.Name == member.Name)
                    {
                        currentParameterInfo = param;
                        paramValue = GetValueVisitor.Get(param.Value, currentParameterInfo, globalParameters, out found);

                        break;
                    }
                }

                try
                {
                    if (found)
                    {
                        SetParameterValue(testCase, currentParameterInfo, paramValue);
                    }
                }
                catch //(Exception ex)
                {
                    throw;
                }
            }
        }

        public bool GetParameterValue(ITestCase testCase, IParameter paramInfo, out object result)
        {
            result = null;

            var properties = GetProperties(testCase, paramInfo.Name);
            foreach (PropertyInfo property in properties)
            {
                result = property.GetValue(testCase, null);
                return true;                
            }

            var fields = GetFields(testCase, paramInfo.Name);
            foreach (var field in fields)
            {
                result = field.GetValue(testCase);
                return true;
            }

            return false;
        }

        public bool GetParameterDefault(ITestCase testCase, IParameter paramInfo, out object result)
        {
            result = null;
            try
            {
                // create new instance to get the default value. Simply using the testCase object is not OK, the value may be changed
                var tc = (ITestCase)Activator.CreateInstance(testCase.GetType());
                //TODO: try to optimize by reusing the instance in TestCases list
                return this.GetParameterValue(tc, paramInfo, out result);
            } catch
            {
                return false;
            }
        }

        public void SetParameterValue(ITestCase testCase, IParameter paramInfo, object value)
        {
            var properties = GetProperties(testCase, paramInfo.Name);
            foreach (PropertyInfo property in properties)
            {
                value = IParameter.Convert(paramInfo, value);
                property.SetValue(testCase, value, null);
                return;
            }

            var fields = GetFields(testCase, paramInfo.Name);
            foreach (var field in fields)
            {
                value = IParameter.Convert(paramInfo, value);
                field.SetValue(testCase, value);
                return;
            }
        }
        
        private PropertyInfo[] GetProperties(ITestCase testCase)
        {
            return testCase.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance); 
        }

        private IEnumerable<PropertyInfo> GetProperties(ITestCase testCase, string name)
        {
            return GetProperties(testCase).Where(x => x.Name == name);
        }

        private FieldInfo[] GetFields(ITestCase testCase)
        {
            return testCase.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);
        }

        private IEnumerable<FieldInfo> GetFields(ITestCase testCase, string name)
        {
            return GetFields(testCase).Where(x => x.Name == name);
        }

        public bool GetTestResult(ITestCase testCase, IParameter paramInfo, out object value)
        {
            var properties = GetProperties(testCase, paramInfo.Name);
            foreach (PropertyInfo property in properties)
            {
                value = property.GetValue(testCase, null);
                return true;
            }

            var fields = GetFields(testCase, paramInfo.Name);
            foreach (var field in fields)
            {
                value = field.GetValue(testCase);
                return true;
            }

            value = null;
            return false;
        }

        public IList<IParameter> GetParameters(ITestCase testCase)
		{
            PropertyInfo[] properties = GetProperties(testCase);
			List<IParameter> result = new List<IParameter>(properties.Length);
			
			foreach (PropertyInfo property in properties)
		    {
		        bool hasSet = (property.CanWrite && property.GetSetMethod(false) != null);
                bool hasGet = (property.CanRead && property.GetGetMethod(false) != null);
                bool isList = false;
                bool isArray = false;
                ParameterDirection type = ParameterDirection.Input;
                if (hasSet && hasGet)
                {
                    type = ParameterDirection.InputOutput;
                }
                else if (!hasSet && hasGet)
                {
                    type = ParameterDirection.Output;
                }
                else if (hasSet)
                {
                    type = ParameterDirection.Input;
                }

                if (!hasGet)
                {
                    continue;
                }

                Type propertyType = property.PropertyType;
                if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition()
                        == typeof(List<>))
                {
                    propertyType = propertyType.GetGenericArguments()[0]; // use this...
                    isList = true;
                }
                else if (propertyType.IsArray)
                {
                    propertyType = propertyType.GetElementType();
                    isArray = true;
                }

                string description = null;
                //TODO use assemblyHelper method
                var attributes = property.GetCustomAttributes(typeof(yats.Attributes.DescriptionAttribute), false);
                if (attributes.Length > 0)
                {
                    description = (attributes[0] as yats.Attributes.DescriptionAttribute).Description;
                }
                attributes= property.GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);
                if (attributes.Length > 0)
                {
                    description = (attributes[0] as System.ComponentModel.DescriptionAttribute).Description;
                }

                TestParameterInfo parameterInfo = new TestParameterInfo(property.Name, description, propertyType.AssemblyQualifiedName, isList, isArray, type);
                AssignDefaultParameterValue(testCase, parameterInfo, (property.GetCustomAttributes(typeof(yats.Attributes.UseDefaultAttribute), false).Count() > 0));
                
                result.Add(parameterInfo);
		    }

            var fields = GetFields(testCase);
            foreach (var field in fields)
            {
                bool markedAsParameter = field.HasAttribute(typeof(Attributes.ParameterAttribute));
                bool markedAsResult = field.HasAttribute(typeof(Attributes.ResultAttribute));
                if (markedAsParameter ^ markedAsResult == false){
                    continue;
                }
                bool isList = false;
                bool isArray = false;
                ParameterDirection type = markedAsParameter? ParameterDirection.InputOutput : ParameterDirection.Output;

                Type propertyType = field.FieldType;
                if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition()
                        == typeof(List<>))
                {
                    propertyType = propertyType.GetGenericArguments()[0]; // use this...
                    isList = true;
                }
                else if (propertyType.IsArray)
                {
                    propertyType = propertyType.GetElementType();
                    isArray = true;
                }
                
                string description = null;
                //TODO use assemblyHelper method
                var attributes = field.GetCustomAttributes(typeof(yats.Attributes.DescriptionAttribute), false);
                if (attributes.Length > 0)
                {
                    description = (attributes[0] as yats.Attributes.DescriptionAttribute).Description;
                }
                attributes = field.GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);
                if (attributes.Length > 0)
                {
                    description = (attributes[0] as System.ComponentModel.DescriptionAttribute).Description;
                }

                TestParameterInfo parameterInfo = new TestParameterInfo(field.Name, description, propertyType.AssemblyQualifiedName, isList, isArray, type);
                AssignDefaultParameterValue(testCase, parameterInfo, (field.GetCustomAttributes(typeof(yats.Attributes.UseDefaultAttribute), false).Count() > 0));
                
                result.Add(parameterInfo);
            }

			result.Sort(delegate(IParameter p1, IParameter p2) { return ((TestParameterInfo)p1).Name.CompareTo(((TestParameterInfo)p2).Name); });
			
			return result;
		}

        private bool AssignDefaultParameterValue(ITestCase testCase, TestParameterInfo parameterInfo, bool tryFindingSuitableParamType)
        {
            if (GetParameterDefault(testCase, parameterInfo, out object defaultValue) == false)
            {
                return false;
            }
            if (defaultValue == null)
            {
                return false;
            }
            if (defaultValue is AbstractParameterValue)
            {
                parameterInfo.Value = defaultValue as AbstractParameterValue;
                return true;
            }
            if (!tryFindingSuitableParamType)
            {
                return false;
            }

            List<AbstractParameterValue> suitableTypes = ParameterTypeCollection.Instance.GetSuitableValueTypes(parameterInfo);
            
            foreach (var suitableType in suitableTypes)
            {
                if (suitableType.SetValue(parameterInfo, defaultValue))
                {
                    parameterInfo.Value = suitableType;
                    return true;
                }
            }
            return false;
        }

        [DebuggerStepThrough]
        public string GetTestName(ITestCase testCase)
        {
            return testCase.GetType( ).Name;
        }

        public string GetTestDescription(ITestCase testCase)
        {
            if (testCase == null)
            {
                return null;
            }
            //TODO use assemblyHelper method
            var attributes = testCase.GetType().GetCustomAttributes(typeof(yats.Attributes.DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                return (attributes[0] as yats.Attributes.DescriptionAttribute).Description;
            }
            attributes = testCase.GetType().GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                return (attributes[0] as System.ComponentModel.DescriptionAttribute).Description;
            }
            return testCase.GetType().FullName;
        }

        public bool CanParameterBeNull(ITestCase testCase, IParameter param)
        {
            var properties = GetProperties(testCase, param.Name);
            foreach (var property in properties) {
                return CanBeNull(property, property.PropertyType);
            }

            var fields = GetFields(testCase, param.Name);
            foreach (var field in fields)
            {
                return CanBeNull(field, field.FieldType);
            }

            return false;            
        }

        private bool CanBeNull(MemberInfo member, Type memberType)
        {
            if (Utilities.ReflectionHelper.IsNotNullable(memberType))
            {
                return false;
            }

            if (Utilities.ReflectionHelper.IsNullableType(memberType))
            {
                return true;
            }
            //TODO use assemblyHelper method
            foreach (var attr in member.GetCustomAttributes(typeof(Attributes.CanBeNullAttribute), true))
            {
                return true;
            }

            return false;
        }

        public bool Cancel(ITestCase testCase)
        {
            if(testCase == null)
            {
                return false;
            }
            if (!(testCase is ICancellable cancellableTestCase))
            {
                return false;
            }

            try
            {
                cancellableTestCase.Cancel();
                return true;
            }
            catch { }
            return false;            
        }

        public string GetGroupName(ITestCase testCase)
        {
            return testCase.GetType().Assembly.GetName().Name;
        }
    }
}
