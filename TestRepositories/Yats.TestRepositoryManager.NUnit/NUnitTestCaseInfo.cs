﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.Interface;

namespace yats.TestRepositoryManager.NUnit
{
    class NUnitTestCaseInfo : ITestCaseInfo
    {
        public NUnitTestCaseInfo()
        {
        }

        public NUnitTestCaseInfo(TestPackage testPackage, string name, TestFixture testFixture, ITestRepositoryManager manager)
        {
            this.Name = name;
            this.TestFixture = testFixture;
            this.RepositoryManager = manager;
            this.TestPackage = testPackage;
        }

        public TestPackage TestPackage { get; internal set; }

        public string Name { get; internal set; }

        public TestFixture TestFixture { get; internal set; }

        public override ITestRepositoryManager RepositoryManager { get; }

        public override ITestCase TestCase
        {
            get
            {
                return new NUnitTestCase() { TestFixture = this.TestFixture, TestPackage = this.TestPackage};
            }
        }
                
        public override ITestCaseInfo Clone()
        {
            return new NUnitTestCaseInfo(TestPackage, Name, TestFixture, RepositoryManager);
        }
    }
}
