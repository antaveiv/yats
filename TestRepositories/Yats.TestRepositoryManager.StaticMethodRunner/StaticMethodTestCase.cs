﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using yats.TestCase.Interface;

namespace yats.TestRepositoryManager.StaticMethodRunner
{
    class StaticMethodTestCase : ITestCase
    {
        public StaticMethodTestCase(MethodInfo methodInfo)
        {
            this.MethodInfo = methodInfo;
            this.Parameters = new Dictionary<string, object>();
        }

        public MethodInfo MethodInfo { get; internal set; }

        // stores parameters and results during test execution
        [XmlIgnore]
        public Dictionary<string, object> Parameters { get; set; }
    }
}
