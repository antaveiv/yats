﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.Interface;
using System.Reflection;

namespace yats.TestRepositoryManager.StaticMethodRunner
{
    class StaticMethodTestInfo : ITestCaseInfo
    {
        public StaticMethodTestInfo()
        {
        }

        public StaticMethodTestInfo(MethodInfo methodInfo, string name, ITestRepositoryManager manager)
        {
            this.Name = name;
            this.MethodInfo = methodInfo;
            this.RepositoryManager = manager;
        }

        public MethodInfo MethodInfo { get; internal set; }

        public string Name { get; internal set; }

        public override ITestRepositoryManager RepositoryManager { get; }

        public override ITestCase TestCase
        {
            get
            {
                return new StaticMethodTestCase(this.MethodInfo);
            }
        }

        public override ITestCaseInfo Clone()
        {
            return new StaticMethodTestInfo(MethodInfo, Name, RepositoryManager);
        }
    }
}
