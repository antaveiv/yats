﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.Interface;
using yats.Utilities;
using System.IO;
using yats.TestRepositoryManager.StaticMethodRunner.Properties;
using System.Threading;
using System.Security.Cryptography;

namespace yats.TestRepositoryManager.StaticMethodRunner
{
    /// <summary>
    /// looks for test cases implementing TestCase.Yats.IYatsTestCase
    /// </summary>
    // to disable for certain platforms, use e.g.: [UnavailablePlatform(System.PlatformID.Unix)]
    public class StaticMethodTestRepositoryManager : ITestRepositoryManager
    {
        //stores a list of UniqueIDs associated with discovered test cases
        protected Dictionary<string, ITestCaseInfo> testCases = new Dictionary<string, ITestCaseInfo>();
        
        public void Initialize()
        {
            try
            {
                testCases = new Dictionary<string, ITestCaseInfo>();

                foreach (string assemblyFileName in yats.Utilities.AssemblyHelper.Instance.GetAssemblyFileNames(null, Settings.Default.ExtraAssemblyLookupPaths?.Cast<string>(), false))
                {
                    if (IsKnownIgnoredAssembly(assemblyFileName))
                    {
                        continue;
                    }
                    try
                    {
                        foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
                        {
                            try
                            {
                                if (Path.GetFileName(a.Location) == Path.GetFileName(assemblyFileName))
                                {
                                    continue;
                                }
                            }
                            catch
                            {
                            }
                        }

                        if (BeforeLoadingTestAssembly != null)
                        {
                            CancelEventArgs e = new CancelEventArgs(false);
                            BeforeLoadingTestAssembly(this, new AssemblyLoadingEventArgs() { FullAssemblyPath = assemblyFileName, Manager = this, CancelEvent = e });
                            // some event handler has cancelled. For example, TestCaseOrganizer allows disabling selected test assemblies
                            if (e.Cancel)
                            {
                                OnTestAssemblyLoad?.Invoke(this, new AssemblyLoadedEventArgs() { FullAssemblyPath = assemblyFileName, IsLoaded = false, Manager = this });
                                continue;
                            }
                        }

                        Assembly asm = Assembly.LoadFrom(assemblyFileName);
                        Type[] typesInAssembly = asm.GetTypes();
                        bool foundTestCases = false;

                        foreach (Type type in typesInAssembly)
                        {
                            foreach (var methodInfo in type.GetMethods())
                            {
                                try
                                {
                                    if (!methodInfo.IsPublic || !methodInfo.IsStatic)
                                    {
                                        continue;
                                    }

                                    var attributes = methodInfo.GetCustomAttributes(typeof(TestMethodAttribute), false);
                                    if (attributes.Length <= 0)
                                    {
                                        continue;
                                    }

                                    if (UnavailablePlatformAttribute.IsPlatformSupported(methodInfo) == false)
                                    {
                                        continue;
                                    }
                                                                        
                                    if (methodInfo.ReturnType.IsAssignableFrom(typeof(ITestResult)) == false)
                                    {
                                        continue;
                                    }

                                    //StaticMethodTestCase instance = new StaticMethodTestCase(methodInfo);
                                    testCases.Add(GetUniqueTestId(new StaticMethodTestCase(methodInfo)), new StaticMethodTestInfo(methodInfo, methodInfo.Name, this));
                                    foundTestCases = true;
                                }

                                catch (Exception ex)
                                {
                                    OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
                                }
                            }
                        }
                        if (foundTestCases)
                        {
                            OnTestAssemblyLoad?.Invoke(this, new AssemblyLoadedEventArgs() { FullAssemblyPath = assemblyFileName, IsLoaded = true, Manager = this });
                            if (AssemblyHelper.IsFileInUnknownDirectory(assemblyFileName))
                            {
                                if (Settings.Default.ExtraAssemblyLookupPaths == null)
                                {
                                    Settings.Default.ExtraAssemblyLookupPaths = new System.Collections.Specialized.StringCollection();
                                }
                                string path = Path.GetDirectoryName(assemblyFileName);
                                if (Settings.Default.ExtraAssemblyLookupPaths.Contains(path) == false)
                                {
                                    Settings.Default.ExtraAssemblyLookupPaths.Add(path);
                                    Settings.Default.Save();
                                }
                            }
                        }
                        else
                        {
                            AddIgnoredAssembly(assemblyFileName);
                        }
                    }
                    catch (BadImageFormatException ex)
                    {
                        AddIgnoredAssembly(assemblyFileName);
                        OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
                    }
                    catch (Exception ex)
                    {
                        OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
                    }
                }
            }
            catch (Exception ex)
            {
                OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
            }
        }


        private bool IsKnownIgnoredAssembly(string assemblyFileName)
        {
            if (Settings.Default.IgnoredAssemblies != null)
            {
                using (FileStream fs = new FileStream(assemblyFileName, FileMode.Open, FileAccess.Read))
                using (BufferedStream bs = new BufferedStream(fs))
                using (var cryptoProvider = new SHA1CryptoServiceProvider())
                {
                    string hash = ByteArray.ToHexString(cryptoProvider.ComputeHash(bs));
                    return Settings.Default.IgnoredAssemblies.Contains(assemblyFileName + "|" + hash);
                }
            }
            return false;
        }

        private void AddIgnoredAssembly(string assemblyFileName)
        {
            if (Settings.Default.IgnoredAssemblies == null)
            {
                Settings.Default.IgnoredAssemblies = new System.Collections.Specialized.StringCollection();
            }

            using (FileStream fs = new FileStream(assemblyFileName, FileMode.Open, FileAccess.Read))
            using (BufferedStream bs = new BufferedStream(fs))
            using (var cryptoProvider = new SHA1CryptoServiceProvider())
            {
                string hash = ByteArray.ToHexString(cryptoProvider.ComputeHash(bs));
                foreach (var s in Settings.Default.IgnoredAssemblies)
                {
                    if (s.StartsWith(assemblyFileName))// existing with old hash
                    {
                        Settings.Default.IgnoredAssemblies.Remove(s);
                        break;
                    }
                }
                Settings.Default.IgnoredAssemblies.Add(assemblyFileName + "|" + hash);
                Settings.Default.Save();
            }
        }

        public IList<ITestCaseInfo> TestCases
        {
            [DebuggerStepThrough]
            get
            { return testCases.Values.ToList(); }
        }

        public string Name { get { return "Static method runner"; } }

        // Raised when the repository manager is about to analyze an assembly 
        public event EventHandler<AssemblyLoadingEventArgs> BeforeLoadingTestAssembly;
        // Raised after the repository manager loads a test case assembly 
        public event EventHandler<AssemblyLoadedEventArgs> OnTestAssemblyLoad;

        public event EventHandler<ExceptionEventArgs> OnError;

        public string GetUniqueTestId(ITestCase testCase)
        {
            if (!(testCase is StaticMethodTestCase tc))
            {
                throw new Exception("Not SM test case");
            }
            return this.GetType().FullName + "|" +tc.MethodInfo.Name; // MethodInfo.ToString also contains method parameter types. You can have methods with same name but overloaded parameter types, this solves the uniqueness
        }

        public ITestCase GetByUniqueId(string testCaseUniqueId)
        {
            if (testCases == null)
            {
                return null;
            }
            if (testCases.TryGetValue(testCaseUniqueId, out ITestCaseInfo result))
            {
                return result.TestCase;
            }
            return null;
        }

        public bool IsMyTestCase(ITestCase testCase)
        {
            if (testCases == null || testCase == null || !(testCase is StaticMethodTestCase))
            {
                return false;
            }
            string testCaseUniqueId = GetUniqueTestId(testCase);
            return testCases.ContainsKey(testCaseUniqueId);
        }

        public bool IsMyTestCase(string testCaseUniqueId)
        {
            if (testCases == null || testCaseUniqueId == null)
            {
                return false;
            }
            return testCases.ContainsKey(testCaseUniqueId);
        }

        public ITestResult Execute(ITestCase testCase, IList<IParameter> testParameters, IGlobalParameterCollection globalParameters, object testRunner, string path)
        {
            if (!(testCase is StaticMethodTestCase tc))
            {
                throw new Exception("Not SM test case");
            }

            object[] parameters = new object[tc.MethodInfo.GetParameters().Count()];
            int i = 0;
            foreach (var methodParam in tc.MethodInfo.GetParameters())
            {
                // this is used to pass results to parameters. Should initialize tc.Parameters on first run, later use it without going to testParameters (saved test case parameters, not updated with results during runtime)
                if (tc.Parameters.TryGetValue(methodParam.Name, out object paramValue))
                {
                    parameters[i] = paramValue;
                }
                else if (IsCancelParameter(methodParam))
                {
                    if (parameters[i] == null)
                    {
                        parameters[i] = new AutoResetEvent(false);
                        tc.Parameters.Add(methodParam.Name, parameters[i]);
                    }
                }
                else
                {
                    IParameter currentParameterInfo = null;
                    foreach (var param in testParameters)
                    {
                        if (param.Name == methodParam.Name)
                        {
                            currentParameterInfo = param;
                            paramValue = GetValueVisitor.Get(param.Value, currentParameterInfo, globalParameters, out bool f);
                            object convertedValue = IParameter.Convert(param, paramValue);
                            parameters[i] = convertedValue;
                            if (param.IsResult)
                            {
                                tc.Parameters.Add(param.Name, convertedValue);
                            }
                            break;
                        }
                    }
                }

                i++;
            }

            ITestResult returnValue = (ITestResult)tc.MethodInfo.Invoke(null, parameters.Count() > 0 ? parameters : null);//parameters must be null if method has no parameters

            // save results to a local Parameters copy attached to tc
            i = 0;
            tc.Parameters.Clear();
            foreach (var methodParam in tc.MethodInfo.GetParameters())
            {
                var paramValue = testParameters.FirstOrDefault(x => x.Name == methodParam.Name);
                if (paramValue != null && paramValue.IsResult)
                {
                    tc.Parameters[methodParam.Name] = parameters[i];
                }
                if (IsCancelParameter(methodParam))
                {
                    if (parameters[i] is WaitHandle)
                    {
                        (parameters[i] as WaitHandle ).Close();
                    }
                }
                i++;
            }
            return returnValue;
        }
        
        public bool GetParameterValue(ITestCase testCase, IParameter paramInfo, out object result)
        {
            if (!(testCase is StaticMethodTestCase tc))
            {
                throw new Exception("Not SM test case");
            }
            return tc.Parameters.TryGetValue(paramInfo.Name, out result);
        }

        public bool GetParameterDefault(ITestCase testCase, IParameter paramInfo, out object result)
        {
            // static method does not have a constructor to initialize parameters to defaults. TODO could return e.g. 0 for int parameter, not much value in that
            result = null;
            return false;
        }

        public void SetParameterValue(ITestCase testCase, IParameter paramInfo, object value)
        {
            if (!(testCase is StaticMethodTestCase tc))
            {
                throw new Exception("Not SM test case");
            }
            tc.Parameters[paramInfo.Name] = value;
        }
                
        public bool GetTestResult(ITestCase testCase, IParameter paramInfo, out object value)
        {
            if (!(testCase is StaticMethodTestCase tc))
            {
                throw new Exception("Not SM test case");
            }
            return tc.Parameters.TryGetValue(paramInfo.Name, out value);
        }

        private bool IsCancelParameter(ParameterInfo paramInfo)
        {
            //don't show method argument marked with [CancelHandler]

            if (paramInfo.GetCustomAttributes(typeof(yats.Attributes.CancelHandlerAttribute), false).Length == 0) {
                return false;
            }
            if (paramInfo.ParameterType.IsByRef || paramInfo.IsOut || paramInfo.IsRetval) {
                return false;
            }
            if (!typeof(WaitHandle).IsAssignableFrom(paramInfo.ParameterType)) {
                return false;
            }
            return true;
        }

        public IList<IParameter> GetParameters(ITestCase testCase)
        {
            if (!(testCase is StaticMethodTestCase tc))
            {
                throw new Exception("Not SM test case");
            }

            List<IParameter> result = new List<IParameter>();
            
            foreach (ParameterInfo paramInfo in tc.MethodInfo.GetParameters())
            {
                bool isList = false;
                bool isArray = false;
                ParameterDirection type = ParameterDirection.Input;

                if (paramInfo.ParameterType.IsByRef)
                {
                    type = ParameterDirection.InputOutput;
                }
                if (paramInfo.IsOut)
                {
                    type = ParameterDirection.Output;
                }
                if (paramInfo.IsRetval)
                {
                    type = ParameterDirection.ReturnValue;
                }
                                
                if (IsCancelParameter(paramInfo))
                {
                    continue;
                }
                
                Type propertyType = paramInfo.ParameterType;
                if (propertyType.FullName.EndsWith("&")) //e.g. System.Boolean& for 'ref bool' argument
                {
                    //propertyType = Type.GetType(propertyType.FullName.Substring(0, propertyType.FullName.Length - 1), true, false);
                    propertyType = AssemblyHelper.Instance.GetType(propertyType.FullName.Substring(0, propertyType.FullName.Length - 1));
                }
                if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition()
                        == typeof(List<>))
                {
                    propertyType = propertyType.GetGenericArguments()[0]; // use this...
                    isList = true;
                }
                else if (propertyType.IsArray)
                {
                    propertyType = propertyType.GetElementType();
                    isArray = true;
                }

                string description = null;
                //TODO use assemblyHelper method
                var attributes = paramInfo.GetCustomAttributes(typeof(yats.Attributes.DescriptionAttribute), false);
                if (attributes.Length > 0)
                {
                    description = (attributes[0] as yats.Attributes.DescriptionAttribute).Description;
                }
                attributes = paramInfo.GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);
                if (attributes.Length > 0)
                {
                    description = (attributes[0] as System.ComponentModel.DescriptionAttribute).Description;
                }

                TestParameterInfo parameterInfo = new TestParameterInfo(paramInfo.Name, description, propertyType.AssemblyQualifiedName, isList, isArray, type);

                result.Add(parameterInfo);
            }

            result.Sort(delegate (IParameter p1, IParameter p2) { return ((TestParameterInfo)p1).Name.CompareTo(((TestParameterInfo)p2).Name); });
            return result;            
        }
        
        [DebuggerStepThrough]
        public string GetTestName(ITestCase testCase)
        {
            if (!(testCase is StaticMethodTestCase tc))
            {
                throw new Exception("Not SM test case");
            }
            var nameAttributes = tc.MethodInfo.GetCustomAttributes(typeof(NameAttribute), false);
            if (nameAttributes.Length > 0)
            {
                return (nameAttributes[0] as NameAttribute).Name;
            }
            return tc.MethodInfo.DeclaringType.Name + "." + tc.MethodInfo.Name;
        }

        public string GetTestDescription(ITestCase testCase)
        {
            if (testCase == null)
            {
                return null;
            }
            if (!(testCase is StaticMethodTestCase tc))
            {
                throw new Exception("Not SM test case");
            }
            //TODO use assemblyHelper method
            var attributes = tc.MethodInfo.GetCustomAttributes(typeof(yats.Attributes.DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                return (attributes[0] as yats.Attributes.DescriptionAttribute).Description;
            }
            attributes = tc.MethodInfo.GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                return (attributes[0] as System.ComponentModel.DescriptionAttribute).Description;
            }
            return tc.MethodInfo.ToString();
        }

        public bool CanParameterBeNull(ITestCase testCase, IParameter param)
        {
            if (!(testCase is StaticMethodTestCase tc))
            {
                throw new Exception("Not SM test case");
            }

            var paramInfo = tc.MethodInfo.GetParameters().FirstOrDefault(x => x.Name == param.Name);
            if (paramInfo == null)
            {
                throw new Exception("Parameter " + param.Name + " not found");
            }
            return CanBeNull(paramInfo);
        }

        private bool CanBeNull(ParameterInfo paramInfo)
        {
            if (Utilities.ReflectionHelper.IsNotNullable(paramInfo.ParameterType))
            {
                return false;
            }

            if (Utilities.ReflectionHelper.IsNullableType(paramInfo.ParameterType))
            {
                return true;
            }
            
            foreach (var attr in paramInfo.GetCustomAttributes(typeof(Attributes.CanBeNullAttribute), true))
            {
                return true;
            }

            return false;
        }

        public bool Cancel(ITestCase testCase)
        {
            if (!(testCase is StaticMethodTestCase tc))
            {
                throw new Exception("Not SM test case");
            }

            foreach (ParameterInfo paramInfo in tc.MethodInfo.GetParameters())
            {
                //don't show method argument marked with [CancelHandler]
                if (IsCancelParameter(paramInfo))
                {
                    if (tc.Parameters.TryGetValue(paramInfo.Name, out object cancelHandler))
                    {
                        (cancelHandler as EventWaitHandle).Set();
                        return true;
                    }
                }
            }
            return false;
        }

        public string GetGroupName(ITestCase testCase)
        {
            StaticMethodTestCase tc = testCase as StaticMethodTestCase;
            return tc.MethodInfo.DeclaringType.Name;
        }
    }
#if DEBUG
    public class SMTest
    {
        [TestMethod]
        public static ITestResult TestCase(
            [CancelHandler]
            //this argument should not be visible in the test parameter list
            AutoResetEvent cancelBLAH)
        {
            cancelBLAH.WaitOne(20000);
            return new TestResult(true);
        }

        [TestMethod]
        public static ITestResult TestCase(
            string str, 
            [CanBeNull]
            string nullStr,
            int? nullInt,
            out int[] outIntArray, 
            [Attributes.Description("test description") ]
            ref bool refBool,
            ref DateTime sometime)
        {
            outIntArray = null;
            refBool = true;
            sometime = DateTime.Now;
            outIntArray = new int[] { 1, 2, 3 };
            return new TestResult(false);
        }

        [TestMethod]
        public static ITestResult Increment(
            [CancelHandler]
            //this argument should not be visible in the test parameter list
            AutoResetEvent cancelBLAH,
            int dummy, 
            int input, 
            out int result)
        {
            result = input + 1;
            return new TestResult(true);
        }
    }
#endif
}
