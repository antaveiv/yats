﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.Logging.Interface;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.Discovery;

namespace yats.TestRepositoryManager.YatsComposite
{
    internal class CompositeTestRunner
    {
        ITestRunner testRunner;

        public CompositeTestRunner(ITestRunner testRunner)
        {
            this.testRunner = testRunner;
        }

        public void Execute()
        {
            testRunner.TestRunFinished += ExecuteEngine_TestRunFinished;
            testRunner.TestRunStarting += ExecuteEngine_TestRunStarting;
            testRunner.TestStepResult += ExecuteEngine_TestStepResult;
            testRunner.TestStepRepetitionResult += ExecuteEngine_TestStepRepetitionResult;
            testRunner.TestStepStarting += ExecuteEngine_TestStepStarting;

            testRunner.Execute(testRunner.TestRun.TestSequence, "");

            testRunner.TestRunFinished -= ExecuteEngine_TestRunFinished;
            testRunner.TestRunStarting -= ExecuteEngine_TestRunStarting;
            testRunner.TestStepResult -= ExecuteEngine_TestStepResult;
            testRunner.TestStepRepetitionResult -= ExecuteEngine_TestStepRepetitionResult;
            testRunner.TestStepStarting -= ExecuteEngine_TestStepStarting;         
        }

        void ExecuteEngine_TestStepStarting (object sender, TestStepStartingEventArgs e)
        {
            TestStepComposite composite = e.Step as TestStepComposite;

            StepInfo stepInfo = null;
            if (e.Step is TestStepSingle single)
            {
                stepInfo = StepInfo.GetStepStart(e.Step.Name, e.Path, TestCaseRepository.Instance.Managers.GetUniqueTestId(single.TestCase));
            }
            else if (composite != null)
            {
                stepInfo = StepInfo.GetStepStart(e.Step.Name, e.Path);
            }

            //log4net.LogManager.GetLogger("Execute").Info("Starting test " + step.Name);

            YatsLogAppenderCollection.Instance.UpdateStep(e.TestRunId, stepInfo);            
        }

        void ExecuteEngine_TestStepRepetitionResult(object sender, TestStepRepetitionResultEventArgs e)
        {
            StepInfo stepInfo = StepInfo.GetRepetitionResult(e.Step.Name, e.Path, e.Result.Result.ToString());
            YatsLogAppenderCollection.Instance.UpdateStep(e.TestRunId, stepInfo);
            //log4net.LogManager.GetLogger( "Execute" ).InfoFormat( "Evaluated result: {0} {1}", e.step.Name, e.result.Result);
        }

        void ExecuteEngine_TestStepResult(object sender, TestStepResultEventArgs e)
        {
            // when test step is finished, transfer its results to other test parameters
            yats.ExecutionEngine.AssignTestResultsToParametersVisitor.Process(testRunner.TestRun, e.Step);

            StepInfo stepInfo = StepInfo.GetStepResult(e.Step.Name, e.Path, e.RawResult.Result.ToString(), e.EvaluatedResult.Result.ToString());

            if (e.Step is TestStepSingle single)
            {
                stepInfo.testCaseUniqueId = TestCaseRepository.Instance.Managers.GetUniqueTestId(single.TestCase);
            }

            YatsLogAppenderCollection.Instance.UpdateStep(e.TestRunId, stepInfo);

            //log4net.LogManager.GetLogger( "Execute" ).InfoFormat( "Result: {0} raw {1} evaluated {2}", step.Name, rawResult.Result, evaluatedResult.Result);
        }

        void ExecuteEngine_TestRunStarting (object sender, TestRunStartEventArgs e)
        {
        }

        void ExecuteEngine_TestRunFinished (object sender, TestRunFinishEventArgs e)
        {
        }
    }
}
