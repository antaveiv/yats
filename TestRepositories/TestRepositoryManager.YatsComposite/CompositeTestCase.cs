﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.IO;
using yats.ExecutionQueue;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.Discovery;
using yats.TestRepositoryManager.Interface;
using yats.TestRun;
using yats.Utilities;
using yats.TestRun.Commands.Visitors;
using yats.TestRun.Commands;
using yats.Utilities.Commands;

namespace yats.TestRepositoryManager.YatsComposite
{
    public class CompositeTestCase : ITestCaseInfo, ITestCase
    {
        protected ITestRepositoryManager repositoryManager;
        
        public CompositeTestCase(ITestRepositoryManager repositoryManager, string fileName)
        {
            this.repositoryManager = repositoryManager;
            this.FileName = fileName;
        }

        protected SavedCompositeTestStep composite = null;
        protected DateTime fileLastUpdated;
        public SavedCompositeTestStep Composite
        {
            get
            {
                if (composite == null || fileLastUpdated != File.GetLastWriteTimeUtc(FileName))
                {
                    LoadTest();
                }
                return composite; 
            }
        }

        private void LoadTest()
        {
            composite = (SavedCompositeTestStep)SerializationHelper.DeserializeFile<ITestRun>(FileName);
            fileLastUpdated = File.GetLastWriteTimeUtc(FileName);
            //the rest is taken from console project TestRun_AfterLoadFromFile 

            var loadedGlobals = yats.TestCase.Parameter.ExtractGlobalParameters.GetGlobals(composite.GlobalParameters, ParameterEnumerator.GetParameters(composite.TestSequence));
            ParameterImporter.ImportGlobalParameters(composite.GlobalParameters, loadedGlobals, GlobalParameterSingleton.Instance.GlobalParameters);

            // allow access to application global parameters
            composite.GlobalParameters = GlobalParameterSingleton.Instance.GlobalParameters;

            // creates test case instances from their stored unique IDs. 
            if (composite.TestSequence != null)
            {
                CheckTestCasesAvailableVisitor.Process(composite.TestSequence, TestCaseRepository.Instance.Managers.Managers);
                if (SynchronizeLoadedParametersVisitor.Process(composite.TestSequence, TestCaseRepository.Instance.Managers, GlobalParameterSingleton.Instance.GlobalParameters))
                {
                    throw new Exception("Can not run test: some test case parameters were updated since the test run was saved. Please review the test parameters");
                }
            }

            ICommand cleanupCommand = RemoveNonExistingGlobals.Validate(composite.TestSequence, composite.GlobalParameters);
            if (cleanupCommand != null)
            {
                cleanupCommand.Execute();
            }
        }

        public override ITestCase TestCase
        {
            get { return this; }
        }

        public override ITestRepositoryManager RepositoryManager
        {
            get  { return repositoryManager; }
        }

        public string FileName
        {
            get;
            protected set;
        }

        public override ITestCaseInfo Clone()
        {
            return new CompositeTestCase(this.repositoryManager, this.FileName);
        }
    }
}
