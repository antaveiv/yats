﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using yats.ExecutionEngine;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.Interface;
using yats.TestRepositoryManager.YatsComposite.Properties;

namespace yats.TestRepositoryManager.YatsComposite
{
    /// <summary>
    /// looks for test cases implementing in *.testcase
    /// </summary>
	public class CompositeTestRepositoryManager : ITestRepositoryManager
    {
        //stores a list of UniqueIDs associated with discovered test cases
        protected Dictionary<string, ITestCaseInfo> testCases = new Dictionary<string, ITestCaseInfo>();

        public void Initialize()
        {            
            try
            {
                testCases = new Dictionary<string, ITestCaseInfo>();
                if (Settings.Default.TestSearchPaths != null)
                {
                    foreach (var directory in Settings.Default.TestSearchPaths)
                    {
                        foreach (var fileName in Directory.GetFiles(directory, "*.testcase", SearchOption.AllDirectories))
                        {
                            try
                            {
                                if (BeforeLoadingTestAssembly != null)
                                {
                                    CancelEventArgs e = new CancelEventArgs(false);
                                    BeforeLoadingTestAssembly (this, new AssemblyLoadingEventArgs () { FullAssemblyPath = fileName, Manager = this, CancelEvent = e });
                                    if (e.Cancel)
                                    {
                                        if (OnTestAssemblyLoad != null)
                                        {
                                        OnTestAssemblyLoad (this, new AssemblyLoadedEventArgs () { FullAssemblyPath = fileName, IsLoaded = false, Manager = this });
                                        }
                                        continue;
                                    }
                                }

                                var tc = new CompositeTestCase(this, fileName);
                                testCases.Add(GetUniqueTestId(tc), tc);


                                if (OnTestAssemblyLoad != null)
                                {
                                OnTestAssemblyLoad (this, new AssemblyLoadedEventArgs () { FullAssemblyPath = fileName, IsLoaded = true, Manager = this });
                                }

                            }
                            catch (Exception ex)
                            {
                                if (OnError != null)
                                {
                                OnError (this, new ExceptionEventArgs () { Exception = ex });
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (OnError != null)
                {
                OnError (this, new ExceptionEventArgs () { Exception = ex });
                }
            }
        }

        public IList<ITestCaseInfo> TestCases
        {
            [DebuggerStepThrough]
            get { return testCases.Values.ToList(); }
        }

        public string Name { get { return "Composite"; } }


        // Raised when the repository manager is about to analyze an assembly 
        public event EventHandler<AssemblyLoadingEventArgs> BeforeLoadingTestAssembly;
        // Raised after the repository manager loads a test case assembly 
        public event EventHandler<AssemblyLoadedEventArgs> OnTestAssemblyLoad;
        // Error indication
        public event EventHandler<ExceptionEventArgs> OnError;
        
        [DebuggerStepThrough]
        public string GetUniqueTestId(ITestCase testCase)
        {
            return this.GetType().FullName + "|" + (testCase as CompositeTestCase).FileName;
        }

        public ITestCase GetByUniqueId(string testCaseUniqueId)
        {
            if (testCases == null){
                return null;
            }
            ITestCaseInfo result = null;
            if (testCases.TryGetValue(testCaseUniqueId, out result))
            {
                return ((CompositeTestCase)result).Clone() as ITestCase;
            }
            return null;
        }
        
        public bool IsMyTestCase(ITestCase testCase)
        {
            if (testCases == null)
            {
                return false;
            }
            if (testCase is CompositeTestCase == false)
            {
                return false;
            }
            string testCaseUniqueId = GetUniqueTestId(testCase);
            return testCases.ContainsKey(testCaseUniqueId);
        }

        public bool IsMyTestCase(string testCaseUniqueId)
        {
            if (testCases == null)
            {
                return false;
            }
            return testCases.ContainsKey(testCaseUniqueId);
        }

        public ITestResult Execute(ITestCase test, IList<IParameter> testParameters, IGlobalParameterCollection globalParameters, object testRunner, string path)
        {
            CompositeTestCase testCase = test as CompositeTestCase;
            if (testCase == null)
            {
                return new TestResultWithData(ResultEnum.INCONCLUSIVE, "Internal error: not a composite Yats test case");
            }

            #region Reset cancel handler
            
            #endregion

            ITestRunner runner = (testRunner as yats.ExecutionEngine.ITestRunner);
            ResultListener resultListener = new ResultListener(testCase);
            runner.TestStepResult += resultListener.runner_TestStepResult;
            // removing .0 from the path - repetition number will be added later
            runner.Execute(testCase.Composite.TestSequence, path.Substring(0, path.Length - 2));

            runner.TestStepResult -= resultListener.runner_TestStepResult;
            return resultListener.evaluatedResult;
        }

        private class ResultListener
        {
            CompositeTestCase testCase;
            public ITestResult evaluatedResult;
            public ResultListener(CompositeTestCase testCase)
            {
                this.testCase = testCase;
            }

            public void runner_TestStepResult(object sender, TestStepResultEventArgs e)
            {
                if (e.Step == testCase.Composite.TestSequence)
                {
                    this.evaluatedResult = e.EvaluatedResult;
                }
            }
        }

        public bool GetParameterValue(ITestCase testCase, IParameter paramInfo, out object result)
        {
            result = null;
                        
            return false;
        }

        public bool GetParameterDefault(ITestCase testCase, IParameter paramInfo, out object result)
        {
            result = null;
            return false;
        }

        public void SetParameterValue(ITestCase testCase, IParameter paramInfo, object value)
        {
        }

        public bool GetTestResult(ITestCase testCase, IParameter paramInfo, out object value)
        {            
            value = null;
            return false;
        }

        public IList<IParameter> GetParameters(ITestCase testCase)
		{
			List<IParameter> result = new List<IParameter>(0);
            return result;
		}

        public string GetTestName(ITestCase testCase)
        {
            return Path.GetFileNameWithoutExtension((testCase as CompositeTestCase).FileName);
        }
        
        public string GetTestDescription(ITestCase testCase)
        {
            return (testCase as CompositeTestCase).FileName;
        }

        public bool CanParameterBeNull(ITestCase testCase, IParameter param)
        {
            return false;            
        }

        public bool Cancel(ITestCase testCase)
        {
            //TODO
            return false;            
        }

        public string GetGroupName(ITestCase testCase)
        {
            return "Composite";
        }
    }
}
