/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
using System;
using System.Reflection;

namespace yats.Attributes
{
    /// <summary>
    /// Adding this attribute to a class indicates that the feature should not be used on the specified platform.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class UnavailablePlatformAttribute : Attribute
    {
        private System.PlatformID platform;

        /// <summary>
        /// Which platform is the feature not available for
        /// </summary>
        public System.PlatformID Platform
        {
            get
            {
                return platform;
            }
            set
            {
                platform = value;
            }
        }

        public UnavailablePlatformAttribute(System.PlatformID platform)
        {
            this.platform = platform;
        }

        public static bool IsPlatformSupported(Type type)
        {
            foreach (var attr in type.GetCustomAttributes(typeof(UnavailablePlatformAttribute), true))
            {
                if ((attr as UnavailablePlatformAttribute).Platform == System.Environment.OSVersion.Platform)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsPlatformSupported(MethodInfo methodInfo)
        {
            foreach (var attr in methodInfo.GetCustomAttributes(typeof(UnavailablePlatformAttribute), true))
            {
                if ((attr as UnavailablePlatformAttribute).Platform == System.Environment.OSVersion.Platform)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
