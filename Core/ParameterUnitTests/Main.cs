/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.Discovery;
using yats.TestRepositoryManager.Interface;
using yats.TestRun;

namespace yats.ParameterTest
{
	class MainClass
	{
        protected static void LoadTest()
        {
            ITestRun testRun = null;
            try 
			{
				//test serialization
                XmlSerializer serializer = yats.Utilities.SerializationHelper.GetSerializer(typeof(ITestRun));
	            TextReader tw = new StreamReader( "testRun.xml" );
	            testRun = (ITestRun) serializer.Deserialize( tw );
	            tw.Close( );
			} catch  (Exception ex)
			{
				Console.WriteLine(ex);
			}

            IGlobalParameters globalParams = null;
            try
            {
                //test serialization
                XmlSerializer serializer = yats.Utilities.SerializationHelper.GetSerializer(typeof(IGlobalParameters));
                TextReader tw = new StreamReader( "globals.xml" );
                globalParams = (IGlobalParameters) serializer.Deserialize( tw );
                tw.Close( );
            }
            catch(Exception ex)
            {
                Console.WriteLine( ex );
            }

            ITestRepositoryManagerCollection testManagerCollection = new TestRepositoryManagerCollection( );
            testManagerCollection.Initialize( true );
            // Initialize ITestCase instances using the unique test ids stored in XML
            TestHierarchyEnumerator.OnSingle(testRun.TestSequence, single =>
            {
                single.TestCase = testManagerCollection.GetByUniqueId(single.UniqueTestId);
            });
            
            ITestRunner testRunner = new TestRunner( testRun);
            IGlobalParameterCollection globals = new GlobalParameterCollection();
            testRun.GlobalParameters = globals;
			globals.Add(globalParams);

            try {
			    testRunner.StartTestRun( );
                //globalParams.Set( guid2, new GlobalParameter( "Delay from globals", new IntegerParameterValue( 333 ) ) );
                //Console.WriteLine( "global value changed to 333" );
                //testRunner.StartTestRun( testRun );
            }
            catch(Exception ex)
            {
                Console.WriteLine( ex );
            }
            Console.ReadLine( );
        }

		public static void Main (string[] args)
		{
            LoadTest();
			Console.WriteLine ("Parameter test");
            ITestRun testRun = new TestRunImplementation();
			ITestRepositoryManagerCollection testManagerCollection = new TestRepositoryManagerCollection();
            testManagerCollection.Initialize(false);

            ITestCase delay = null;
			TestStepComposite sequence = new TestStepComposite();
			List<TestStepSingle> steps = new List<TestStepSingle>();
			
			//setup global parameters
			IGlobalParameters testRunParams = new GlobalParameters(ParameterScope.TEST_RUN);
            IGlobalParameters globalParams = new GlobalParameters(ParameterScope.GLOBAL);
            IGlobalParameterCollection globals = new GlobalParameterCollection();
            globals.Add(testRunParams);
            globals.Add(globalParams);
            
            testRun.GlobalParameters = globals;
			
			Guid guid1 = testRunParams.Set( new GlobalParameter( "Delay from test run", new IntegerParameterValue( 2, 0, 1000 ) ) );
            Guid guid2 = globalParams.Set( new GlobalParameter( "Delay from globals", new IntegerParameterValue( 3,0, 1000 ) ) );
				
			Console.WriteLine ("TC unique IDs:");
			foreach (var mgr in testManagerCollection.Managers){
				mgr.Initialize();
				foreach (var tc in mgr.TestCases){
					Console.WriteLine ("TC {0} {1}", tc, mgr.GetUniqueTestId(tc.TestCase));
				}
				delay = mgr.GetByUniqueId("yats.TestRepositoryManager.YatsNative.TestRepositoryManager|yats.TestCase.Yats.DelayTest");
				if (delay != null){
					for (int i = 0; i < 5; i++){
                        var step = new TestStepSingle( delay, mgr );
						steps.Add( step );
						sequence.Steps.Add(step);
					}
					testRun.TestSequence = sequence;
				}
			}
			sequence.Steps[0].Parameters[0].Value = new IntegerParameterValue(125, 0, 1000);
			sequence.Steps[1].Parameters[0].Value = new IntegerParameterValue(126,0,1000);
			sequence.Steps[2].Parameters[0].Value = new GlobalValue(testRunParams.ID, guid1);
			sequence.Steps[3].Parameters[0].Value = new GlobalValue(globalParams.ID, guid2);
			sequence.Steps[4].Parameters[0].Value = new GlobalValue(globalParams.ID, guid2);
			
			ITestRunner testRunner = new TestRunner( testRun );
			
			try 
			{
				//test serialization
                XmlSerializer serializer = yats.Utilities.SerializationHelper.GetSerializer(typeof(ITestRun));
	            TextWriter tw = new StreamWriter( "testRun.xml" );
	            serializer.Serialize( tw, testRun );
	            tw.Close( );
			} catch  (Exception ex)
			{
				Console.WriteLine(ex);
			}

            try
            {
                //test serialization
                XmlSerializer serializer = yats.Utilities.SerializationHelper.GetSerializer(typeof(IGlobalParameters));
                TextWriter tw = new StreamWriter( "globals.xml" );
                serializer.Serialize( tw, globalParams );
                tw.Close( );
            }
            catch(Exception ex)
            {
                Console.WriteLine( ex );
            }
            
            try {
			    testRunner.StartTestRun( );
                globalParams.Set( guid2, new GlobalParameter( "Delay from globals", new IntegerParameterValue( 333,0,1000 ) ) );
                Console.WriteLine( "global value changed to 333" );
                testRunner.StartTestRun( );
            }
            catch(Exception ex)
            {
                Console.WriteLine( ex );
            }
            Console.ReadLine( );
		}
	}
}


			