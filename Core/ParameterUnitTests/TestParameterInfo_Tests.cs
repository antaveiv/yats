﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using yats.TestCase.Parameter;
using System.Data;

namespace yats.ParameterTest
{
    [TestFixture]
    public class TestParameterInfo_Tests
    {
        [Test]
        public void TestHashCode()
        {
            TestParameterInfo p1 = new TestParameterInfo("aa", typeof(string).AssemblyQualifiedName, false, false, ParameterDirection.Input);
            TestParameterInfo p2 = new TestParameterInfo("aa", typeof(string).AssemblyQualifiedName, false, false, ParameterDirection.Input);
            Assert.AreEqual(p1.GetHashCode(), p2.GetHashCode());
        }
    }
}
