﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;

namespace yats.TestRepositoryManager.Interface
{
    public class AssemblyLoadingEventArgs : EventArgs
        {
        public ITestRepositoryManager Manager;
        public string FullAssemblyPath;
        public CancelEventArgs CancelEvent;
        }

    public class AssemblyLoadedEventArgs : EventArgs
        {
        public ITestRepositoryManager Manager;
        public string FullAssemblyPath;
        public bool IsLoaded;
        }

    public class ExceptionEventArgs : EventArgs
        {
        public Exception Exception;
        }

    public class RepositoryManagerEventArgs : EventArgs
        {
        public ITestRepositoryManager Repository;
        }

    public class RepositoryManagerCollectionEventArgs : EventArgs
        {
        public ITestRepositoryManagerCollection Repository;
        }

    public interface ITestRepositoryManager
    {
		#region Events
		// Raised when the repository manager is about to analyze an assembly 
        event EventHandler<AssemblyLoadingEventArgs> BeforeLoadingTestAssembly;
        // Raised after the repository manager loads a test case assembly 
        event EventHandler<AssemblyLoadedEventArgs> OnTestAssemblyLoad;
		// Error indication
        event EventHandler<ExceptionEventArgs> OnError;
        
		#endregion

		#region Test case discovery
		// Test case discovery should be done here
        void Initialize();
		// List of available test cases
        IList<ITestCaseInfo> TestCases { get; }
		#endregion

		#region Unique test case ID handling
        // Returns an unique name for a given test case (class type name, script file name etc.)
        string GetUniqueTestId(ITestCase testCase);
        // Returns the corresponding test case by ID
        ITestCase GetByUniqueId(string testCaseUniqueId);
		#endregion 

		#region Test execution methods
        ITestResult Execute(ITestCase testCase, IList<IParameter> testParameters, IGlobalParameterCollection globalParameters, object testRunner, string path);
        string GetGroupName(ITestCase testCase);

        // try to cancel a running test case, return false if the test does not support it
        bool Cancel(ITestCase testCase);
		#endregion

        [Obsolete]
		string GetTestName(ITestCase testCase);
        [Obsolete]
        string GetTestDescription(ITestCase testCase);

		#region Test parameter handling
        IList<IParameter> GetParameters(ITestCase testCase);
        bool GetTestResult(ITestCase testCase, IParameter param, out object value);
        void SetParameterValue(ITestCase testCase, IParameter param, object value);
        bool GetParameterValue(ITestCase testCase, IParameter param, out object value);
        bool GetParameterDefault(ITestCase testCase, IParameter param, out object value);
        bool CanParameterBeNull(ITestCase testCase, IParameter param);
		#endregion
        
        bool IsMyTestCase(ITestCase testCase);
        bool IsMyTestCase(string testCaseUniqueId);
        string Name { get; }
    }
}
