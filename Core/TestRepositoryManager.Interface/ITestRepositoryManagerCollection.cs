﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using yats.TestCase.Interface;

namespace yats.TestRepositoryManager.Interface
{
    /// <summary>
    /// Responsible for finding TestRepositoryManager instances in the application. These instances
    /// can look further for different types of test descriptions in plug-in DLLs, scripts, files etc.
    /// The Manager Collection contains all managers. 
    /// When implementing support for a new type of test cases, a new class:ITestRepositoryManager
    /// should be written. It then should create ITestCase instances on program startup.
    /// </summary>
    public interface ITestRepositoryManagerCollection 
    {
        event EventHandler<RepositoryManagerCollectionEventArgs> InitializationStarting;
        event EventHandler<RepositoryManagerCollectionEventArgs> InitializationFinished;
        event EventHandler<AssemblyLoadingEventArgs> BeforeLoadingManagerAssembly;
        event EventHandler<AssemblyLoadingEventArgs> BeforeLoadingTestAssembly;
        event EventHandler<AssemblyLoadedEventArgs> OnTestAssemblyLoad;
        event EventHandler<RepositoryManagerEventArgs> ManagerAdded;
        event EventHandler<ExceptionEventArgs> OnError;

        /// <summary>
        /// Call Initialize to find all TestRepositoryManagers. During the method execution
        /// events may be called to indicate managers and errors. After return, 
        /// ITestRepositoryManagerCollection.Managers shall be available.
        /// </summary>
        /// <param name="recursive">If true, should initialize all found TestRepositoryManagers</param>
        void Initialize(bool recursive);

        List<ITestRepositoryManager> Managers { get; }
        ITestRepositoryManager this[int i]
        {
            get;
        }

        /// <summary>
        /// Get unique test case ID. May throw an exception if not found
        /// </summary>
        /// <param name="testCase"></param>
        /// <returns></returns>
        string GetUniqueTestId(ITestCase testCase);
        ITestCase GetByUniqueId(string testCaseUniqueId);
        ITestRepositoryManager GetManagerByTestUniqueId(string testCaseUniqueId);

        /// <summary>
        /// Get user-friendly test case name. May throw an exception if not found
        /// </summary>
        /// <param name="testCase"></param>
        /// <returns></returns>
        string GetTestName(ITestCase testCase);
    }
}
