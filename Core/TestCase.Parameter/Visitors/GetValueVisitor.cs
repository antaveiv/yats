/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;

namespace yats.TestCase.Parameter
{
    /// <summary>
    /// Gets the internal parameter value object. For example, an object of type int is returned for a IntegerParameterValue or a Global parameter pointing to a IntegerParameterValue 
    /// </summary>
	public class GetValueVisitor 
	{        
        public static object Get(AbstractParameterValue parameterValue, IParameter paramInfo, IGlobalParameterCollection globalParameters, out bool found)
        {
            found = false;
            if (parameterValue == null)
            {
                found = true;
                return null;
            }
            if (parameterValue is GlobalParameter)
            {
                found = true;
                return Get((parameterValue as GlobalParameter).Value, paramInfo, globalParameters, out found);
            }
            GlobalValue globalValue = (parameterValue as GlobalValue);
            if (globalValue != null)
            {
                try
                {
                    return Get(globalParameters.Managers[globalValue.StorageId].Get(globalValue.ParameterID), paramInfo, globalParameters, out found);
                }
                catch (Exception ex)
                {
                    //TODO: logging
                    Console.WriteLine(ex);
                    return null;
                }
            }
            TestResultParameter valueParameter = parameterValue as TestResultParameter;
            if (valueParameter != null)
            {
                found = false; // result value should be assigned during run-time. Here we don't have handles to test run, steps etc. that would be needed to look up the value
                return null;                
            }

            found = true;//really?
            return parameterValue.GetValue(paramInfo);
        }
    }
}

