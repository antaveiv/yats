﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;

namespace yats.TestCase.Parameter
{
    public class ExtractGlobalParameters
    {
        public static ICollection<GlobalValue> GetGlobals(IGlobalParameterCollection globalParameters, IList<IParameter> allTestParameters)
        {
            List<GlobalValue> result = new List<GlobalValue>();
            if (globalParameters == null)
            {
                return result;
            }

            foreach (var p in allTestParameters)
            {
                GlobalValue asGlobal = (p.Value as GlobalValue);
                if (asGlobal == null)
                {
                    continue;
                }
                if (globalParameters.Managers.ContainsKey(asGlobal.StorageId) == false)
                {
                    continue; // unknown, should not happen
                }
                result.Add(asGlobal);
            }
            return result;
        }

        public static ICollection<GlobalValue> GetGlobals(IGlobalParameterCollection globalParameters, params IParameter[] allTestParameters )
        {
            List<GlobalValue> result = new List<GlobalValue>();
            if (globalParameters == null){
                return result;
            }

            foreach (var p in allTestParameters)
            {
                GlobalValue asGlobal = (p.Value as GlobalValue);
                if (asGlobal == null)
                {
                    continue;
                }
                if (globalParameters.Managers.ContainsKey(asGlobal.StorageId) == false)
                {
                    continue; // unknown, should not happen
                }
                result.Add(asGlobal);
            }
            return result;
        }
    }
}
