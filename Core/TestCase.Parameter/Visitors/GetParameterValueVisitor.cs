﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;

namespace yats.TestCase.Parameter
{
    /// <summary>
    /// Gets the parameter value object. For example, IntegerParameterValue object is returned for a IntegerParameterValue or a Global parameter pointing to a IntegerParameterValue 
    /// </summary>
    public class GetParameterValueVisitor 
    {
        

        public static AbstractParameterValue Get(AbstractParameterValue parameterValue, IGlobalParameterCollection globalParameters, out bool found)
        {
            found = false;
            if(globalParameters == null || parameterValue == null)
            {
                return null;
            }
            if (parameterValue is GlobalParameter)
            {
                return Get((parameterValue as GlobalParameter).Value, globalParameters, out found);
            }
            
            GlobalValue globalValue = (parameterValue as GlobalValue);
            if (globalValue != null)
            {
                try
                {
                    return Get(globalParameters.Managers[globalValue.StorageId].Get(globalValue.ParameterID), globalParameters, out found);
                }
                catch (Exception ex)
                {
                    //TODO: logging
                    Console.WriteLine(ex);
                    return null;
                }
            }

            found = true;
            return parameterValue;
        }
    }
}
