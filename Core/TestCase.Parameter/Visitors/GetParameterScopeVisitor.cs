﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


namespace yats.TestCase.Parameter
{
	// Get parameter scope - local, test run or global
    public class GetParameterScopeVisitor 
    {
        public static ParameterScope Get(AbstractParameterValue param, IGlobalParameterCollection globals)
        {
            if(param == null)
            {
                return ParameterScope.LOCAL;
            }

            if (param is GlobalParameter){
                return ParameterScope.GLOBAL;
            }

            if (param is GlobalValue){
                try
                {
                    switch(globals.Managers[(param as GlobalValue).StorageId].Scope)
                    {
                        case ParameterScope.TEST_RUN:
                            return ParameterScope.TEST_RUN;
                        case ParameterScope.GLOBAL:
                            return ParameterScope.GLOBAL;
                    }
                }
                catch
                {
                    return ParameterScope.LOCAL;
                }
            }

            return ParameterScope.LOCAL;
        }
    }
}
