﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;

namespace yats.TestCase.Parameter
{
    /// <summary>
    /// Updates the IParameter.ValueTypeHashCode values, e.g. before serialization. If a value is not assigned, the hash is set to null
    /// </summary>
	public class UpdateParamTypeHashCode 
	{
        public static void Update(IList<IParameter> parameters, IGlobalParameterCollection globalParameters)
        {
            foreach (var p in parameters)
            {
                Update(p, globalParameters);
            }
        }

        public static void Update(IParameter paramInfo, IGlobalParameterCollection globalParameters)
        {
            paramInfo.ValueTypeHashCode = GetHashCode(paramInfo, globalParameters);
        }

        public static int? GetHashCode (IParameter paramInfo, IGlobalParameterCollection globalParameters)
        {
            AbstractParameterValue val = paramInfo.Value;

            if (paramInfo.Value is GlobalParameter)
            {
                val = (paramInfo.Value as GlobalParameter).Value;
            }
            GlobalValue globalValue = (paramInfo.Value as GlobalValue);
            if (globalValue != null)
            {
                try
                {
                    val = globalParameters.Managers[globalValue.StorageId].Get(globalValue.ParameterID).Value;
                }
                catch 
                {
                    val = null;
                }
            }

            if (val == null)
            {
                return null;
            }
            else
            {
                return val.GetParamValueTypeHash();
            }
        }
    }
}

