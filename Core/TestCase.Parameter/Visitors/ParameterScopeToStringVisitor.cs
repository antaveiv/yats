﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


namespace yats.TestCase.Parameter
{
	// Get a string description about parameter scope
    public class ParamSimpleTypeVisitor 
    {
        public static string Get(AbstractParameterValue param, IGlobalParameterCollection globals)
        {
            if (param == null)
            {
                return string.Empty;
            }
            if (param is GlobalParameter)
            {
                return string.Format("global: {0}", (param as GlobalParameter).Name);
            }
            if (param is GlobalValue)
            {
                return GetGlobalValue(param as GlobalValue, globals);
            }

            return "local";
        }


        static string GetGlobalValue(GlobalValue param, IGlobalParameterCollection globals)
        {
            try
            {
                switch (globals.Managers[param.StorageId].Scope)
                {
                    case ParameterScope.TEST_RUN:
                        return "test run";
                    case ParameterScope.GLOBAL:
                        return string.Format("global: {0}", globals.Managers[param.StorageId].Get(param.ParameterID).Name); 
                }
            }
            catch
            {
                return "global, not found";
            }
            return "global";
        }
    }
}
