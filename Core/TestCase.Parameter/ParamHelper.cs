﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;

namespace yats.TestCase.Parameter
{
    public class ParamHelper
    {
        public static bool IsAssignable(IParameter from, IParameter to)
        {
            Type t1 = Type.GetType(from.ParamType);
            Type t2 = Type.GetType(to.ParamType);
            if (t2.IsAssignableFrom(t1) == false)
            {
                return false;
            }
            bool fromMultiple = from.IsArray || from.IsList;
            bool toMultiple = to.IsArray || to.IsList;

            return (fromMultiple == toMultiple) || (!fromMultiple && toMultiple);
        }
    }
}
