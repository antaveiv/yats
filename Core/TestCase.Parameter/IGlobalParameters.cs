/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.Xml.Serialization;
using yats.Utilities;

namespace yats.TestCase.Parameter
{
    // TODO: parameters not used, simplify
    public class GlobalParameterEventArgs : EventArgs
    {
        public Guid Id;
        public GlobalParameter Value;
        public IGlobalParameters Manager;
        public GlobalParameterEventArgs(Guid id, GlobalParameter value, IGlobalParameters manager)
        {
            Id = id;
            Value = value;
            Manager = manager;
        }
    }

    [XmlInclude( typeof( GlobalParameters ) )]
	public abstract class IGlobalParameters
	{
		[XmlIgnore]
		protected Guid id = Guid.NewGuid();
        public Guid ID
        {
            [DebuggerStepThrough]
            get { return id; }
            [DebuggerStepThrough]
            set { id = value; }
        }

        protected ParameterScope scope = ParameterScope.TEST_RUN;
        public ParameterScope Scope
        {
            [DebuggerStepThrough]
            get { return scope; }
            set {
                if (scope == ParameterScope.GLOBAL && value == ParameterScope.TEST_RUN)
                {
                    try
                    {
                        throw new Exception("Global parameter storage changed to TestRun");
                    }
                    catch (Exception ex)
                    {
                        CrashLog.Write(ex);
                    }
                }
                scope = value; 
            }
        }

        public abstract SerializableDictionary<Guid, GlobalParameter> Parameters
        {
            get;
            set;
        }

        public abstract GlobalParameter Get(Guid id);
        
        /// <summary>
        /// Store value and return the new associated parameter GUID
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public abstract Guid Set(GlobalParameter value);

        /// <summary>
        /// Store value with specified GUID (e.g. modify existing value)
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public abstract void Set(Guid id, GlobalParameter value);
        public abstract void Add(Guid id, GlobalParameter value);


        internal event EventHandler<GlobalParameterEventArgs> ValueAdded;
        internal event EventHandler<GlobalParameterEventArgs> ValueRemoved;
        internal event EventHandler<GlobalParameterEventArgs> ValueUpdated;

        internal void RaiseValueAdded(Guid id, GlobalParameter value, IGlobalParameters manager)
        {
            if (ValueAdded != null)
            {
            ValueAdded (this, new GlobalParameterEventArgs (id, value, manager));
            }
        }

        internal void RaiseValueRemoved(Guid id, GlobalParameter value, IGlobalParameters manager)
        {
            if (ValueRemoved != null)
            {
            ValueRemoved (this, new GlobalParameterEventArgs (id, value, manager));
            }
        }

        internal void RaiseValueUpdated(Guid id, GlobalParameter value, IGlobalParameters manager)
        {
            if (ValueUpdated != null)
            {
            ValueUpdated (this, new GlobalParameterEventArgs (id, value, manager));
            }
        }

        public abstract void Remove(Guid id);
    }
}

