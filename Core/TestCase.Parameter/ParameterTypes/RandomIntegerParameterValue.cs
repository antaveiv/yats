﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;

namespace yats.TestCase.Parameter
{
    public class RandomIntegerParameterValue : AbstractParameterValue
	{
		protected int userMin;
        public int UserMinimum
        {
            [DebuggerStepThrough]
			get {
				return userMin;
			}
            [DebuggerStepThrough]
			set {
				userMin = value;
			}
		}

        protected int userMax;
        public int UserMaximum
        {
            [DebuggerStepThrough]
			get {
				return userMax;
			}
            [DebuggerStepThrough]
			set {
				userMax = value;
			}
		}

        protected decimal min;
        public decimal Min
        {
            [DebuggerStepThrough]
            get
            {
                return min;
            }
            [DebuggerStepThrough]
            set
            {
                min = value;
            }
        }

        protected decimal max;
        public decimal Max
        {
            [DebuggerStepThrough]
            get
            {
                return max;
            }
            [DebuggerStepThrough]
            set
            {
                max = value;
            }
        }

        [DebuggerStepThrough]
        public RandomIntegerParameterValue()
		{
		}
        
        [DebuggerStepThrough]
        public RandomIntegerParameterValue(decimal min, decimal max)
        {
            this.min = min;
            this.max = max;
            this.userMin = (int)min;
            this.userMax = (int)max;
        }

        [DebuggerStepThrough]
        public RandomIntegerParameterValue(decimal min, decimal max, int userMin, int userMax)
        {
            this.min = min;
            this.max = max;
            this.userMin = userMin;
            this.userMax = userMax;
        }
        
        public override AbstractParameterValue Clone()
        {
            return new RandomIntegerParameterValue(min, max, userMin, userMax);
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "random decimal", delegate(IParameter parameterInfo, ref int valueTypePriority)
                {
                    if (parameterInfo.IsArray || parameterInfo.IsList)
                    {
                        return null;
                    }

                    decimal min;
                    decimal max;

                    if (Utilities.NumericTypeHelper.IsNumericType(Type.GetType(parameterInfo.ParamType), out min, out max) == false){
                        return null;
                    }

                    if (min < int.MinValue || max > int.MaxValue)
                    {
                        return null;
                    }
                    valueTypePriority = 60;
                    return new RandomIntegerParameterValue(min, max);
                }
            );
        }

        public override string GetDisplayValue()
        {
            return string.Format("Random [{0}..{1}]", userMin, userMax);
        }

        static Random rnd = new Random();

        public override object GetValue(IParameter format)
        {
            int val = rnd.Next(userMin, userMax + 1);

            if (format == null)
            {
                return val;
            }

            Type t = Type.GetType(format.ParamType);
            if(t.IsGenericType && t.GetGenericTypeDefinition( ) == typeof( Nullable<> ))
            {
                // If it is NULLABLE, then get the underlying type. eg if "Nullable<int>" then this will return just "int"
                Type dataType = t.GetGenericArguments( )[0];
                return Convert.ChangeType( val, dataType );
            }
            return Convert.ChangeType(val, t);
        }

        public override bool SetValue(IParameter format, object value)
        {
            if (value == null)
            {
                return false; // should not come here
            }

            if (value is RandomIntegerParameterValue)
            {
                var val = value as RandomIntegerParameterValue;
                this.min = val.min;
                this.max = val.max;
                this.userMin = val.userMin;
                this.userMax = val.userMax;
                return true;
            }

            return false;
        }

        public override int GetParamValueTypeHash()
        {
            int res = 58420;
            res ^= min.GetHashCode();
            res ^= max.GetHashCode();
            return res;
        }
	}
}
