/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;
using yats.Utilities;

namespace yats.TestCase.Parameter
{
	public class SimpleStringValue : AbstractParameterValue
	{
        public SimpleStringValue()
        {
        }

        public SimpleStringValue(string value)
            : this()
        {
            this.paramValue = value;
        }

        protected string paramValue;
		public string Value {
            [DebuggerStepThrough]
			get {
				return paramValue;
			}
            [DebuggerStepThrough]
			set {
				paramValue = value;
			}
		}

        public override AbstractParameterValue Clone()
        {
            return new SimpleStringValue(this.paramValue);
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "string", typeof(string).AssemblyQualifiedName);
        }

        public override string GetDisplayValue()
        {
            return paramValue.ControlCharactersToHex();
        }

        public override object GetValue(IParameter format)
        {
            return paramValue;
        }

        public override bool SetValue(IParameter format, object value)
        {
            if (value is string)
            {
                this.paramValue = value as string;
                return true;
            }
            return false;
        }

        public override int GetParamValueTypeHash()
        {
            return typeof(string).GUID.GetHashCode();
        }
	}
}

