﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.Xml.Serialization;

namespace yats.TestCase.Parameter
{
    public class GlobalValue : AbstractParameterValue
    {
		protected Guid parameterId;
        public Guid ParameterID
        {
            [DebuggerStepThrough]
            get { return parameterId; }
            [DebuggerStepThrough]
            set { parameterId = value; }
        }
		
		[XmlIgnore]
		protected Guid storageId;
        public Guid StorageId
        {
            [DebuggerStepThrough]
            get { return storageId; }
            [DebuggerStepThrough]
            set { storageId = value; }
        }
				
		public GlobalValue()
		{
		}

        public GlobalValue(Guid storageGuid, Guid parameterId)
		{
			this.storageId = storageGuid;
            this.parameterId = parameterId;
		}

        public override AbstractParameterValue Clone()
        {
            return new GlobalValue(storageId, parameterId);
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
        }

        public override string GetDisplayValue()
        {
            return "";
        }

        public override object GetValue(IParameter format)
        {
            return null;
        }
        
        public override int GetParamValueTypeHash()
        {
            throw new NotImplementedException("Should not call GlobalValue.GetParamValueTypeHash");
        }
    }
}
