﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Utilities;
using System.Diagnostics;
using System.IO;

namespace yats.TestCase.Parameter
{
    public class StringFromFile : AbstractParameterValue
    {
        protected string fileName;
        public string FileName
        {
            [DebuggerStepThrough]
            get
            {
                return fileName;
            }
            [DebuggerStepThrough]
            set
            {
                fileName = value;
            }
        }

        [DebuggerStepThrough]
        public StringFromFile()
		{
		}

        [DebuggerStepThrough]
        public StringFromFile(string fileName)
            : this()
        {
            this.fileName = fileName;
        }

        public override AbstractParameterValue Clone()
        {
            return new StringFromFile(this.fileName);
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "load from file", delegate(IParameter parameterInfo, ref int valueTypePriority)
            {
                if (parameterInfo.ParamType == typeof(string).AssemblyQualifiedName) // ignore the parameterInfo.IsArray and IsList - this class can handle both
                {
                    valueTypePriority = 50;
                    return new StringFromFile();
                }
                return null;
            });
        }

        public override string GetDisplayValue()
        {
            return string.Format("Load from [{0}]", fileName);
        }

        public override object GetValue(IParameter format)
        {
            if (format == null)
            {
                return File.ReadAllText(this.FileName);
            }
            if (format.ParamType == typeof(string).AssemblyQualifiedName && format.IsArray)
            {
                return File.ReadAllLines(this.FileName);
            }
            if (format.ParamType == typeof(string).AssemblyQualifiedName && format.IsList)
            {
                return ListExtensions.GetList<string>(File.ReadAllText(this.FileName));
            }
            if (format.ParamType == typeof(string).AssemblyQualifiedName)
            {
                return File.ReadAllText(this.FileName);
            }
            return this;
        }

        public override int GetParamValueTypeHash()
        {
            return GetType().FullName.GetHashCode();
        }
    }
}
