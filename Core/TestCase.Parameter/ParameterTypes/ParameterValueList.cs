﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace yats.TestCase.Parameter
{
    public class ParameterValueList : AbstractParameterValue
    {
        protected List<AbstractParameterValue> values = new List<AbstractParameterValue>();

        public List<AbstractParameterValue> Values
        {
            [DebuggerStepThrough]
            get { return values; }
            [DebuggerStepThrough]
            set { values = value; }
        }

        protected IParameter parameterInfo;
        public IParameter ParameterInfo
        {
            [DebuggerStepThrough]
            get { return parameterInfo; }
            [DebuggerStepThrough]
            set { parameterInfo = value; } 
        }

        [DebuggerStepThrough]
        public ParameterValueList(IParameter parameterInfo)
        {
            this.parameterInfo = parameterInfo;
        }

        [DebuggerStepThrough]
        public ParameterValueList()
        {
        }
        
        [DebuggerStepThrough]
        public ParameterValueList(IParameter parameterInfo, params AbstractParameterValue[] values)
        {
            this.parameterInfo = parameterInfo; // values[0].GetType().FullName;
            this.values.AddRange(values);
        }

        public override AbstractParameterValue Clone()
        {
            ParameterValueList result = new ParameterValueList();
            if (this.parameterInfo != null)
            {
                result.parameterInfo = this.parameterInfo.Clone();
            }
            if (values != null)
            {
                foreach (var val in this.values)
                {
                    if (val != null)
                    {
                        result.values.Add(val.Clone());
                    }
                    else
                    {
                        result.values.Add(null);
                    }
                }
            }
            return result;
        }


        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "value list", delegate(IParameter parameterInfo, ref int valueTypePriority) 
                        {
                            valueTypePriority = 70;
                            if (parameterInfo.IsList == false && parameterInfo.IsArray == false){
                                return null;
                            }
                            var cloneWithoutArrays = parameterInfo.Clone();
                            cloneWithoutArrays.IsList = false;
                            cloneWithoutArrays.IsArray = false;

                            return new ParameterValueList(cloneWithoutArrays);
                        }
                );
        }

        public override string GetDisplayValue()
        {
            if (values == null || values.Count == 0)
            {
                return "[empty]";
            }

            string tmpRes = string.Format("{0} items [", values.Count);
            foreach (var val in values)
            {
                if (val != null)
                {
                    tmpRes = string.Format("{0}{1},", tmpRes, val.GetDisplayValue());
                }
                else
                {
                    tmpRes = string.Format("{0}[null],", tmpRes);
                }
            }
            return tmpRes.TrimEnd(new char[] { ',' }) + "]";
        }

        public override object GetValue(IParameter paramInfo)
        {
            if (paramInfo == null)
            {
                return values;
            }

            //Reflection - dynamically create and fill an Array or List of a type
            if (paramInfo.IsArray)
            {
                //create Array (param type[])
                Array res = Array.CreateInstance(Type.GetType(paramInfo.ParamType), Values.Count);
                int index = 0;
                foreach (var value in Values)
                {
                    res.SetValue(value.GetValue(paramInfo), index);
                    index++;
                }
                return res;
            }
            else if (paramInfo.IsList)
            {
                // create List<param type>
                Type elementType = Type.GetType(paramInfo.ParamType);
                Type[] types = new Type[] { elementType };
                Type listType = typeof(List<>);
                Type genericType = listType.MakeGenericType(types);
                IList list = Activator.CreateInstance(genericType) as IList;

                foreach (var value in Values)
                {
                    list.Add(value.GetValue(paramInfo));
                }

                return list;
            }
            return null;
        }

        public override bool SetValue(IParameter format, object value)
        {
            var valueAsList = (value as IList);
            bool result = false;
            if (valueAsList != null)
            {
                this.values = new List<AbstractParameterValue>(valueAsList.Count);
                foreach (var item in valueAsList)
                {
                    if (item == null)
                    {
                        result = true;
                        continue;
                    }
                    if (item is AbstractParameterValue)
                    {
                        AbstractParameterValue itemAsParam = item as AbstractParameterValue;
                        AbstractParameterValue newItem = itemAsParam.Clone();
                        newItem.SetValue(format, item);
                        this.values.Add(newItem);
                        result = true;
                        continue;
                    }
                }
            }
            return result;
            //TODO: process value (may be an array or list)
        }

        public override int GetParamValueTypeHash()
        {
            var result = 2192139;
            if (parameterInfo != null){
                result ^= parameterInfo.GetParamValueTypeHash();
            }
            return result;
        }
    }
}
