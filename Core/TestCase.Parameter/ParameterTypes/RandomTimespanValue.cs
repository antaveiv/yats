﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;

namespace yats.TestCase.Parameter
{
    public class RandomTimespanValue : AbstractParameterValue
    {
        // Minimum size of the generated buffer
        int minMs;
        public int MinMs
        {
            [DebuggerStepThrough]
            get { return minMs; }
            [DebuggerStepThrough]
            set { minMs = value; }
        }

        // Maximum size of the generated buffer
        int maxMs;
        public int MaxMs
        {
            [DebuggerStepThrough]
            get { return maxMs; }
            [DebuggerStepThrough]
            set { maxMs = value; }
        }

        // parameterless constructor is needed for serialization
        public RandomTimespanValue()
        {
        }

        public RandomTimespanValue(int minMs, int maxMs)
            : this()
        {
            this.minMs = minMs;
            this.maxMs = maxMs;
        }

        public override AbstractParameterValue Clone()
        {
            return new RandomTimespanValue(minMs, maxMs);
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "random", new string[] { typeof(TimeSpan).AssemblyQualifiedName, GetType().AssemblyQualifiedName }, 30);
        }

        // User-friendly summary for GUI
        public override string GetDisplayValue()
        {
            return string.Format("Random [{0}..{1}] ms", minMs, maxMs);
        }

        // Returns the actual parameter value, to be assigned before running a test case
        public override object GetValue(IParameter format)
        {
            var rnd = new Random();
            TimeSpan result = TimeSpan.FromMilliseconds(rnd.Next(minMs, maxMs + 1));
            return result;
        }

        // Used to detect when parameter implementation has been changed between program runs
        public override int GetParamValueTypeHash()
        {
            return this.GetType().Name.GetHashCode();
        }
    }
}
