﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;
using System.IO;

namespace yats.TestCase.Parameter
{
    public class ByteArrayFromFile : AbstractParameterValue
    {
        protected string fileName;
        public string FileName
        {
            [DebuggerStepThrough]
            get
            {
                return fileName;
            }
            [DebuggerStepThrough]
            set
            {
                fileName = value;
            }
        }

        [DebuggerStepThrough]
        public ByteArrayFromFile()
		{
		}

        [DebuggerStepThrough]
        public ByteArrayFromFile(string fileName)
            : this()
        {
            this.fileName = fileName;
        }

        public override AbstractParameterValue Clone()
        {
            return new ByteArrayFromFile(this.fileName);
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "load from file", delegate(IParameter parameterInfo, ref int valueTypePriority)
            {
                if (((parameterInfo.IsArray || parameterInfo.IsList) && parameterInfo.ParamType == typeof(byte).AssemblyQualifiedName) ||
                    ((parameterInfo.IsArray == false && parameterInfo.IsList == false) && parameterInfo.ParamType == typeof(byte[]).AssemblyQualifiedName)
                    )
                {
                    valueTypePriority = 50;
                    return new ByteArrayFromFile();
                }
                return null;
            });
        }

        public override string GetDisplayValue()
        {
            return string.Format("Load from [{0}]", fileName);
        }

        public override object GetValue(IParameter format)
        {
            if (format == null)
            {
                return File.ReadAllBytes(this.FileName);
            }
            if ((format.IsArray || format.IsList) && format.ParamType == typeof(byte).AssemblyQualifiedName)
            {
                return File.ReadAllBytes(this.FileName);
            }
            if (format.ParamType == typeof(byte[]).AssemblyQualifiedName)
            {
                return File.ReadAllBytes(this.FileName);
            }
            return this;
        }

        public override int GetParamValueTypeHash()
        {
            return GetType().FullName.GetHashCode();
        }
    }
}
