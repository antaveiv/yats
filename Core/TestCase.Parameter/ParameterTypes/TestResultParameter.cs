﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;

namespace yats.TestCase.Parameter
{
    public class TestResultParameter : AbstractParameterValue
    {
        protected Guid id;
        public Guid ResultID
        {
            [DebuggerStepThrough]
            get { return id; }
            [DebuggerStepThrough]
            set { id = value; }
        }

        public TestResultParameter()
		{
		}

        public TestResultParameter(Guid resultId)
		{
            this.id = resultId;
		}

        public override AbstractParameterValue Clone()
        {
            return new TestResultParameter(id);
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            //ParameterTypeCollection.AddEntry(this, "test result", null);
        }

        public override string GetDisplayValue()
        {
            return "[use test result]";
        }

        public override object GetValue(IParameter format)
        {
            return null;
        }

        public override int GetParamValueTypeHash()
        {
            return GetType().FullName.GetHashCode();
        }
    }
}
