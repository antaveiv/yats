﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;

namespace yats.TestCase.Parameter
{
    public class EnumParameterValue : AbstractParameterValue
    {
        public EnumParameterValue()
        {
        }

        public EnumParameterValue(string value, string enumTypeName)
            : this()
        {
            this.paramValue = value;
            this.enumTypeName = enumTypeName;
        }

        protected string paramValue;
        public string Value
        {
            [DebuggerStepThrough]
            get
            {
                return paramValue;
            }
            [DebuggerStepThrough]
            set
            {
                paramValue = value;
            }
        }
        
        protected string enumTypeName;
        public string EnumTypeName
        {
            [DebuggerStepThrough]
            get
            {
                return enumTypeName;
            }
            [DebuggerStepThrough]
            set
            {
                enumTypeName = value;
            }
        }
        
        public override AbstractParameterValue Clone()
        {
            return new EnumParameterValue(this.paramValue, this.enumTypeName);
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "enumeration", delegate(IParameter parameterInfo, ref int valueTypePriority)
            {
                Type t = Type.GetType(parameterInfo.ParamType);
                if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    t = Nullable.GetUnderlyingType(t);
                }
                if (t.IsEnum == false)
                {
                    return null;
                }

                if (parameterInfo.IsList || parameterInfo.IsArray )
                {
                    return null; // Let ParameterValueList delegate handle multiple enums
                }

                return new EnumParameterValue("", t.AssemblyQualifiedName);                
            }
            );
        }

        public override string GetDisplayValue()
        {
            return paramValue;
        }

        public override object GetValue(IParameter format)
        {
            if (format == null)
            {
                return paramValue;
            }
            string enumType;
            if (string.IsNullOrEmpty(format.ParamType))
            {
                enumType = this.enumTypeName;
            }
            else
            {
                enumType = format.ParamType;
            }

            Type t = Type.GetType(enumType);
            return Enum.Parse(t, paramValue);
        }

        public override bool SetValue(IParameter format, object value)
        {
            if (value != null)
            {
                this.paramValue = value.ToString();
            }
            return true;
        }

        public override int GetParamValueTypeHash()
        {
            int value = enumTypeName.GetHashCode();
            foreach ( var e in Enum.GetValues(Type.GetType(enumTypeName)))
            {
                var val = Convert.ChangeType(e, Enum.GetUnderlyingType(Type.GetType(enumTypeName)));
                value = value ^ val.GetHashCode() ^ e.ToString().GetHashCode();
            }
            return value;
        }
    }
}
