﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;

namespace yats.TestCase.Parameter
{
    public class RandomByteArrayValue : AbstractParameterValue
    {
        // Minimum size of the generated buffer
        int minSize;
        public int MinSize
        {
            [DebuggerStepThrough]
            get { return minSize; }
            [DebuggerStepThrough]
            set { minSize = value; }
        }

        // Maximum size of the generated buffer
        int maxSize;
        public int MaxSize
        {
            [DebuggerStepThrough]
            get { return maxSize; }
            [DebuggerStepThrough]
            set { maxSize = value; }
        }

        // parameterless constructor is needed for serialization
        public RandomByteArrayValue()
        {
        }

        public RandomByteArrayValue(int minSize, int maxSize):this()
        {
            this.minSize = minSize;
            this.maxSize = maxSize;
        }

        public override AbstractParameterValue Clone()
        {
            return new RandomByteArrayValue(minSize, maxSize);
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            // a delegate is used to determine that RandomByteArrayValue is suitable for byte[] or List<byte>
            parameterTypes.RegisterParameterType(this.GetType(), "random buffer", delegate(IParameter parameterInfo, ref int valueTypePriority)
            {
                if (((parameterInfo.IsArray || parameterInfo.IsList) && parameterInfo.ParamType == typeof(byte).AssemblyQualifiedName) ||
                    ((parameterInfo.IsArray == false && parameterInfo.IsList == false) && parameterInfo.ParamType == typeof(byte[]).AssemblyQualifiedName)
                    )
                {
                    valueTypePriority = 30; // low priority - should put the editor tab towards the end
                    return new RandomByteArrayValue();
                }
                return null;
            });
        }

        // User-friendly summary for GUI
        public override string GetDisplayValue()
        {
            return string.Format("Random buffer of [{0}..{1}] bytes", minSize, maxSize);
        }

        // Returns the actual parameter value, to be assigned before running a test case
        public override object GetValue(IParameter format)
        {
            var rnd = new Random();
            byte [] result = new byte[rnd.Next(minSize, maxSize+1)];
            rnd.NextBytes(result);
            return result;
        }

        // Used to detect when parameter implementation has been changed between program runs
        public override int GetParamValueTypeHash()
        {
            return this.GetType().Name.GetHashCode();
        }
    }
}
