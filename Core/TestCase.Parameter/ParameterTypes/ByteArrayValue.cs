﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;
using System.Text;
using yats.Utilities;

namespace yats.TestCase.Parameter
{
    public class ByteArrayValue: AbstractParameterValue
    {
		protected byte[] val;
		public byte[] Value {
            [DebuggerStepThrough]
			get {
				return val;
			}
            [DebuggerStepThrough]
			set {
				val = value;
			}
		}

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "binary", delegate(IParameter parameterInfo, ref int valueTypePriority)
            {
                if (((parameterInfo.IsArray || parameterInfo.IsList) && parameterInfo.ParamType == typeof(byte).AssemblyQualifiedName) ||
                    ((parameterInfo.IsArray == false && parameterInfo.IsList == false) && parameterInfo.ParamType == typeof(byte[]).AssemblyQualifiedName)
                    )
                {
                    return new ByteArrayValue();
                }
                return null;
            });
        }

        [DebuggerStepThrough]
        public ByteArrayValue()
		{
		}

        [DebuggerStepThrough]
        public ByteArrayValue(byte[] value)
		{
			val = value;
		}

        public override AbstractParameterValue Clone()
        {
            return new ByteArrayValue(this.val);
        }

        public override string GetDisplayValue()
        {
            if (val.ContainsAsciiControlCharacters())
            {
                return string.Format("Hex: {0}", Utilities.ByteArray.ToStringHex(val));
            }
            string asString = Encoding.ASCII.GetString(this.val);
            
            return string.Format("ASCII: {0}", asString);
        }

        public override object GetValue(IParameter format)
        {
            return val;
        }

        public override bool SetValue(IParameter format, object value)
        {
            if (value is byte [])
            {
                this.val = value as byte[];
                return true;
            }
            return false;
        }

        public override int GetParamValueTypeHash()
        {
            return typeof(byte[]).GUID.GetHashCode();
        }
    }
}
