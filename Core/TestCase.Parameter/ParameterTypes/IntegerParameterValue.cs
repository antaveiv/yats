/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;

namespace yats.TestCase.Parameter
{
	public class IntegerParameterValue : AbstractParameterValue
	{
		protected decimal val;
		public decimal DecimalValue {
            [DebuggerStepThrough]
			get {
				return val;
			}
            [DebuggerStepThrough]
			set {
				val = value;
			}
		}

        protected decimal min;
        public decimal Min
        {
            [DebuggerStepThrough]
            get
            {
                return min;
            }
            [DebuggerStepThrough]
            set
            {
                min = value;
            }
        }

        protected decimal max;
        public decimal Max
        {
            [DebuggerStepThrough]
            get
            {
                return max;
            }
            [DebuggerStepThrough]
            set
            {
                max = value;
            }
        }

        [DebuggerStepThrough]
        public IntegerParameterValue()
		{
		}
        
        [DebuggerStepThrough]
        public IntegerParameterValue(decimal value, decimal min, decimal max)
        {
            val = value;
            this.min = min;
            this.max = max;
        }

        public IntegerParameterValue(decimal value, Type underlyingType)
        {
            val = value;
            if (Utilities.NumericTypeHelper.IsNumericType(underlyingType, out min, out max) == false)
            {
                throw new NotImplementedException("IntegerParameterValue does not support " + underlyingType.AssemblyQualifiedName);
            }
            if (value > max)
            {
                throw new ArgumentOutOfRangeException("Value > max");
            }
            if (value < min)
            {
                throw new ArgumentOutOfRangeException("Value < min");
            }
        }

        public override AbstractParameterValue Clone()
        {
            return new IntegerParameterValue(val, min, max);
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "decimal", delegate(IParameter parameterInfo, ref int valueTypePriority)
                {
                    if (parameterInfo.IsArray || parameterInfo.IsList)
                    {
                        return null;
                    }

                    decimal min;
                    decimal max;

                    if (Utilities.NumericTypeHelper.IsNumericType(Type.GetType(parameterInfo.ParamType), out min, out max) == false){
                        return null;
                    }

                    return new IntegerParameterValue(0, min, max);
                }
            );
        }

        public override string GetDisplayValue()
        {
            return val.ToString();
        }

        public override object GetValue(IParameter format)
        {
            if (format == null)
            {
                return val;
            }

            Type t = Type.GetType(format.ParamType);
            if(t.IsGenericType && t.GetGenericTypeDefinition( ) == typeof( Nullable<> ))
            {
                // If it is NULLABLE, then get the underlying type. eg if "Nullable<int>" then this will return just "int"
                Type dataType = t.GetGenericArguments( )[0];
                return Convert.ChangeType( val, dataType );
            }
            return Convert.ChangeType(val, t);
        }

        public override bool SetValue(IParameter format, object value)
        {
            if (value == null)
            {
                return false; // should not come here
            }

            if (Utilities.NumericTypeHelper.IsNumericType(value.GetType(), out min, out max) == false)
            {
                return false;
            }

            this.val = (decimal)Convert.ChangeType(value, typeof(decimal));
            return true;
        }

        public override int GetParamValueTypeHash()
        {
            int res = 884492;
            res ^= min.GetHashCode();
            res ^= max.GetHashCode();
            return res;
        }
	}
}

