﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using InTheHand.Net;
using System;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Net;
using System.Xml.Serialization;

namespace yats.TestCase.Parameter
{
    public enum RtsEnableEnum
    {
        OFF,
        ON,
        DONT_CHANGE
    }
    
    public class PortSettings : AbstractParameterValue
    {
        [DebuggerStepThrough]
        public PortSettings()
        {
        }

        [DebuggerStepThrough]
		public PortSettings (SerialPortInfo serial): this()
        {
            this.serialPort = serial;
            this.Selected = SelectedSetup.SERIAL;
        }

        [DebuggerStepThrough]
        public PortSettings(TcpIpClientInfo tcpIpClient)
            : this()
        {
            this.tcpIpClient = tcpIpClient;
            this.selected = SelectedSetup.TCP_IP_CLIENT;
        }

        [DebuggerStepThrough]
        public PortSettings(TcpIpServerInfo tcpIpServer)
            : this()
        {
            this.tcpIpServer = tcpIpServer;
            this.selected = SelectedSetup.TCP_IP_SERVER;
        }

        [DebuggerStepThrough]
        public PortSettings(SshClientInfo sshClient)
            : this( )
        {
            this.sshClient = sshClient;
            this.selected = SelectedSetup.SSH_CLIENT;
        }

        [DebuggerStepThrough]
        public PortSettings(UdpSocketInfo udpSocketInfo)
            : this()
        {
            this.udpSocketInfo = udpSocketInfo;
            this.selected = SelectedSetup.UDP_SOCKET;
        }

        public override AbstractParameterValue Clone()
        {
            var result = new PortSettings();
            if(this.serialPort != null)
            {
                result.serialPort = this.serialPort.Clone( );
            }
            if(this.tcpIpServer != null)
            {
                result.tcpIpServer = this.tcpIpServer.Clone( );
            }
            if(this.tcpIpClient != null)
            {
                result.tcpIpClient = this.tcpIpClient.Clone( );
            }
            if(this.sshClient != null)
            {
                result.sshClient = this.sshClient.Clone( );
            }
            if (this.udpSocketInfo != null)
            {
                result.udpSocketInfo = this.udpSocketInfo.Clone();
            }
            if (this.bluetoothPortInfo != null)
            {
                result.bluetoothPortInfo = this.bluetoothPortInfo.Clone();
            }
            if (this.portEmulatorInfo != null)
            {
                result.portEmulatorInfo = this.portEmulatorInfo.Clone();
            }
            result.selected = this.selected;
            result.discardDataWithoutParsers = this.discardDataWithoutParsers;
            return result;
        }

        public override string ToString()
        {
            switch (this.selected)
            {
                case SelectedSetup.SERIAL:
                    return serialPort.ToString();
                case SelectedSetup.TCP_IP_CLIENT:
                    return tcpIpClient.ToString();
                case SelectedSetup.TCP_IP_SERVER:
                    return tcpIpServer.ToString();
                case SelectedSetup.SSH_CLIENT:
                    return sshClient.ToString( );
                case SelectedSetup.UDP_SOCKET:
                    return udpSocketInfo.ToString();
                case SelectedSetup.BLUETOOTH:
                    return bluetoothPortInfo.ToString();
                case SelectedSetup.EMULATOR:
                    return portEmulatorInfo.ToString();
            }
            return "Not set";            
        }
        
        protected SerialPortInfo serialPort = new SerialPortInfo();
        public SerialPortInfo SerialPort
        {
            [DebuggerStepThrough]
            get { return serialPort; }
            [DebuggerStepThrough]
            set { serialPort = value; }
        }
                
        protected TcpIpServerInfo tcpIpServer = new TcpIpServerInfo();
        public TcpIpServerInfo TcpIpServer
        {
            [DebuggerStepThrough]
            get { return tcpIpServer; }
            [DebuggerStepThrough]
            set { tcpIpServer = value; }
        }
        
        protected TcpIpClientInfo tcpIpClient = new TcpIpClientInfo();
        public TcpIpClientInfo TcpIpClient
        {
            [DebuggerStepThrough]
            get { return tcpIpClient; }
            [DebuggerStepThrough]
            set { tcpIpClient = value; }
        }

        protected SshClientInfo sshClient = new SshClientInfo( );
        public SshClientInfo SshClient
        {
            [DebuggerStepThrough]
            get { return sshClient; }
            [DebuggerStepThrough]
            set { sshClient = value; }
        }

        protected UdpSocketInfo udpSocketInfo = new UdpSocketInfo();
        public UdpSocketInfo UdpSocket
        {
            [DebuggerStepThrough]
            get { return udpSocketInfo; }
            [DebuggerStepThrough]
            set { udpSocketInfo = value; }
        }

        protected BluetoothPortInfo bluetoothPortInfo = new BluetoothPortInfo();
        public BluetoothPortInfo BluetoothPort
        {
            [DebuggerStepThrough]
            get { return bluetoothPortInfo; }
            [DebuggerStepThrough]
            set { bluetoothPortInfo = value; }
        }

        protected PortEmulatorInfo portEmulatorInfo = new PortEmulatorInfo();
        public PortEmulatorInfo Emulator
        {
            [DebuggerStepThrough]
            get { return portEmulatorInfo; }
            [DebuggerStepThrough]
            set { portEmulatorInfo = value; }
        }

        
        public enum SelectedSetup
        {
            SERIAL,
            TCP_IP_CLIENT,
            TCP_IP_SERVER,
            UDP_SOCKET,
            SSH_CLIENT,
            BLUETOOTH,
            EMULATOR
        }

        protected SelectedSetup selected = SelectedSetup.SERIAL;
        public SelectedSetup Selected
        {
            [DebuggerStepThrough]
            get { return selected; }
            [DebuggerStepThrough]
            set { selected = value; }
        }

        protected bool discardDataWithoutParsers = true;
        /// <summary>
        /// If true, incoming data will be lost when there are no parsers listening for it. If false data will be queued until parser is added by Command, Execute or Attach methods
        /// </summary>
        public bool DiscardDataWithoutParsers { get { return discardDataWithoutParsers; } set { discardDataWithoutParsers = value; } }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "port settings", typeof(PortSettings).AssemblyQualifiedName);
        }

        public override string GetDisplayValue()
        {
            return this.ToString();
        }

        public override object GetValue(IParameter format)
        {
            return this;
        }

        public override int GetParamValueTypeHash()
        {
            return GetType().FullName.GetHashCode();
        }

        public class SerialPortInfo
        {
            [DebuggerStepThrough]
            public SerialPortInfo()
            {
            }

            [DebuggerStepThrough]
            public SerialPortInfo(string deviceName, int baudRate, int dataBits, Parity parity)
                : this( )
            {
                this.portName = deviceName;
                this.baudRate = baudRate;
                this.dataBits = dataBits;
                this.parity = parity;
            }

            
            protected int baudRate = 9600;
            public int BaudRate
            {
                [DebuggerStepThrough]
                get
                {
                    return baudRate;
                }
                [DebuggerStepThrough]
                set
                {
                    baudRate = value;
                }
            }

            protected string portName = "COM1";
            public string PortName
            {
                [DebuggerStepThrough]
                get
                {
                    return portName;
                }
                [DebuggerStepThrough]
                set
                {
                    portName = value;
                }
            }

            /// <summary>
            /// Gets or sets the standard length of data bits per byte.
            /// 
            /// </summary>
            /// 
            /// <returns>
            /// The data bits length.
            /// 
            /// </returns>
            /// <exception cref="T:System.IO.IOException">The port is in an invalid state.
            /// 
            /// - or -
            /// 
            /// An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort"/> object were invalid.
            /// </exception>
            /// <exception cref="T:System.ArgumentOutOfRangeException">The data bits value is less than 5 or more than 8. </exception>

            protected int dataBits = 8;
            public int DataBits
            {
                [DebuggerStepThrough]
                get
                {
                    return dataBits;
                }
                [DebuggerStepThrough]
                set
                {
                    dataBits = value;
                }
            }

            /// <summary>
            /// Gets or sets the parity-checking protocol.
            /// 
            /// </summary>
            /// 
            /// <returns>
            /// One of the <see cref="T:System.IO.Ports.Parity"/> values that represents the parity-checking protocol. The default is None.
            /// 
            /// </returns>
            /// <exception cref="T:System.IO.IOException">The port is in an invalid state.
            /// 
            /// - or -
            /// 
            /// An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort"/> object were invalid.
            /// </exception>
            /// <exception cref="T:System.ArgumentOutOfRangeException">The <see cref="P:System.IO.Ports.SerialPort.Parity"/> value passed is not a valid value in the <see cref="T:System.IO.Ports.Parity"/> enumeration. </exception>
            /// <PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/></PermissionSet>
            protected Parity parity = Parity.None;
            public Parity Parity
            {
                [DebuggerStepThrough]
                get
                {
                    return parity;
                }
                [DebuggerStepThrough]
                set
                {
                    parity = value;
                }
            }

            /// <summary>
            /// Gets or sets the standard number of stopbits per byte.
            /// 
            /// </summary>
            /// 
            /// <returns>
            /// One of the <see cref="T:System.IO.Ports.StopBits"/> values.
            /// 
            /// </returns>
            /// <exception cref="T:System.ArgumentOutOfRangeException">The <see cref="P:System.IO.Ports.SerialPort.StopBits"/> value is not one of the values from the <see cref="T:System.IO.Ports.StopBits"/> enumeration. </exception>
            /// <exception cref="T:System.IO.IOException">The port is in an invalid state.
            /// 
            /// - or -
            /// 
            /// An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort"/> object were invalid.
            /// </exception>
            /// <PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/></PermissionSet>
            protected StopBits stopBits = StopBits.One;
            public StopBits StopBits
            {
                [DebuggerStepThrough]
                get
                {
                    return stopBits;
                }
                [DebuggerStepThrough]
                set
                {
                    stopBits = value;
                }
            }

            const RtsEnableEnum DEFAULT_RTS_ENABLE = RtsEnableEnum.DONT_CHANGE;
            protected RtsEnableEnum rtsEnable = DEFAULT_RTS_ENABLE;
            public RtsEnableEnum RtsEnable
            {
                get
                {
                    return rtsEnable;
                }
                set
                {
                    rtsEnable = value;
                }
            }

            const bool DEFAULT_DTR = false;
            protected bool dtrEnable = DEFAULT_DTR;
            public bool DtrEnable
            {
                get
                {
                    return dtrEnable;
                }
                set
                {
                    dtrEnable = value;
                }
            }

            const Handshake DEFAULT_HANDSHAKE = Handshake.None;
            protected Handshake handshake = DEFAULT_HANDSHAKE;
            public Handshake Handshake
            {
                get
                {
                    return handshake;
                }
                set
                {
                    handshake = value;
                }
            }

            public SerialPortInfo Clone()
            {
                SerialPortInfo result = new SerialPortInfo( );
                result.baudRate = this.baudRate;
                result.dataBits = this.dataBits;
                //result.newLine = this.newLine;
                result.parity = this.parity;
                result.portName = this.portName;
                //result.readBufferSize = this.readBufferSize;
                //result.readTimeout = this.readTimeout;
                result.rtsEnable = this.rtsEnable;
                result.dtrEnable = this.dtrEnable;
                result.stopBits = this.stopBits;
                //result.writeBufferSize = this.writeBufferSize;
                result.handshake = this.handshake;
                return result;
            }

            public override string ToString()
            {
                string stop = "";
                switch(stopBits)
                {
                    case StopBits.None:
                        stop = "0";
                        break;
                    case StopBits.One:
                        stop = "1";
                        break;
                    case StopBits.OnePointFive:
                        stop = "1.5";
                        break;
                    case StopBits.Two:
                        stop = "2";
                        break;
                }

                return string.Format( "Serial port {0} {1}bps {2}.{3} {4}", portName, baudRate, dataBits, stop, parity.ToString( ) );
            }
        }

        public class BluetoothPortInfo
        {
            [DebuggerStepThrough]
            public BluetoothPortInfo()
            {
            }

            [DebuggerStepThrough]
            public BluetoothPortInfo(BluetoothAddress address, string pin)
                : this( )
            {
                this.address = address;
                this.pin = pin;
            }

            
            protected BluetoothAddress address;
            public BluetoothAddress Address
            {
                [DebuggerStepThrough]
                get
                {
                    return address;
                }
                [DebuggerStepThrough]
                set
                {
                    address = value;
                }
            }

            protected string pin = "";
            public string PIN
            {
                [DebuggerStepThrough]
                get
                {
                    return pin;
                }
                [DebuggerStepThrough]
                set
                {
                    pin = value;
                }
            }

            public BluetoothPortInfo Clone()
            {
                BluetoothPortInfo result = new BluetoothPortInfo();
                if (this.address != null)
                {
                    result.address = (BluetoothAddress)this.address.Clone();
                }
                result.pin = this.pin;
                return result;
            }

            public override string ToString()
            {
                return string.Format( "Bluetooth port {0} [{1}]", address, pin);
            }
        }

        public class SshClientInfo
        {
            protected string host;
            public string Host
            {
                [DebuggerStepThrough]
                get { return host; }
                [DebuggerStepThrough]
                set { host = value; }
            }

            protected string user;
            public string User
            {
                [DebuggerStepThrough]
                get { return user; }
                [DebuggerStepThrough]
                set { user = value; }
            }

            protected string password;
            public string Password
            {
                [DebuggerStepThrough]
                get { return password; }
                [DebuggerStepThrough]
                set { password = value; }
            }

            [DebuggerStepThrough]
            public SshClientInfo()
            {
            }

            [DebuggerStepThrough]
            public SshClientInfo(string host, string user, string password)
                : this( )
            {
                this.host = host;
                this.user = user;
                this.password = password;
            }

            public SshClientInfo Clone()
            {
                SshClientInfo result = new SshClientInfo( this.host, this.user, this.password );
                return result;
            }

            public override string ToString()
            {
                return string.Format( "SSH client {0}@{1}", user, host );
            }

            public override bool Equals(object obj)
            {
                if(object.ReferenceEquals( this, obj ))
                {
                    return true;
                }

                SshClientInfo other = obj as SshClientInfo;
                if(other == null || this == null)
                {
                    return false;
                }

                return this.host == other.host && this.user == other.user && this.password == other.password;
            }

            public override int GetHashCode()
            {
                int res = 88311;
                if(host != null)
                {
                    res ^= host.GetHashCode( );
                }
                if(user != null)
                {
                    res ^= user.GetHashCode( );
                }
                if(password != null)
                {
                    res ^= password.GetHashCode( );
                }
                return res;
            }
        }

        public class TcpIpClientInfo
        {
            protected int portNumber;
            public int PortNumber
            {
                [DebuggerStepThrough]
                get { return portNumber; }
                [DebuggerStepThrough]
                set { portNumber = value; }
            }

            protected string address;
            public string Address
            {
                [DebuggerStepThrough]
                get { return address; }
                [DebuggerStepThrough]
                set { address = value; }
            }

            protected bool keepAlive;
            public bool KeepAlive
            {
                [DebuggerStepThrough]
                get
                { return keepAlive; }
                [DebuggerStepThrough]
                set
                { keepAlive = value; }
            }

            [DebuggerStepThrough]
            public TcpIpClientInfo()
            {
            }

            [DebuggerStepThrough]
            public TcpIpClientInfo(string address, int portNumber, bool keepAlive)
                : this( )
            {
                this.address = address;
                this.portNumber = portNumber;
                this.keepAlive = keepAlive;
            }

            public TcpIpClientInfo Clone()
            {
                TcpIpClientInfo result = new TcpIpClientInfo( );
                result.portNumber = this.portNumber;
                result.address = this.address;
                result.keepAlive = this.keepAlive;
                return result;
            }

            public override string ToString()
            {
                return string.Format( "TCP/IP client {0}:{1}", address, portNumber );
            }

            public bool IsConfigured
            {
                get
                {
                    return string.IsNullOrEmpty(address) == false && portNumber > 0 && portNumber < ushort.MaxValue;
                }
            }
        }

        public class TcpIpServerInfo
        {
            protected int portNumber;
            public int PortNumber
            {
                [DebuggerStepThrough]
                get { return portNumber; }
                [DebuggerStepThrough]
                set { portNumber = value; }
            }

            protected int? clientConnectTimeoutMs = null;

            /// <summary>
            /// Timeout for client connect wait. If null - no timeout
            /// </summary>
            public int? ClientConnectTimeoutMs
            {
                [DebuggerStepThrough]
                get { return clientConnectTimeoutMs; }
                [DebuggerStepThrough]
                set { clientConnectTimeoutMs = value; }
            }

            protected bool replaceSocket = false;
            public bool ReplaceSocket
            {
                get { return replaceSocket; }
                set { replaceSocket = value; }
            }

            protected bool keepAlive;
            public bool KeepAlive
            {
                [DebuggerStepThrough]
                get
                { return keepAlive; }
                [DebuggerStepThrough]
                set
                { keepAlive = value; }
            }

            [DebuggerStepThrough]
            public TcpIpServerInfo()
            {
            }

            [DebuggerStepThrough]
            public TcpIpServerInfo(int portNumber)
                : this( )
            {
                this.portNumber = portNumber;
            }

            [DebuggerStepThrough]
            public TcpIpServerInfo(int portNumber, int? clientConnectTimeoutMs)
                : this(portNumber)
            {
                this.clientConnectTimeoutMs = clientConnectTimeoutMs;
            }

            public TcpIpServerInfo Clone()
            {
                TcpIpServerInfo result = new TcpIpServerInfo( );
                result.portNumber = this.portNumber;
                result.clientConnectTimeoutMs = this.clientConnectTimeoutMs;
                result.replaceSocket = this.replaceSocket;
                result.keepAlive = this.keepAlive;
                return result;
            }

            public override string ToString()
            {
                string timeout = "no connect timeout";
                if (clientConnectTimeoutMs.HasValue)
                {
                    timeout = string.Format("connect timeout: {0}s", clientConnectTimeoutMs.Value / 1000);
                }
                return string.Format("TCP/IP server port {0}, {1}", portNumber, timeout);
            }

            public bool IsConfigured
            {
                get
                {
                    return portNumber > 0 && portNumber < ushort.MaxValue;
                }
            }
        }

        public class UdpSocketInfo
        {
            protected int localPortNumber = 1;
            public int LocalPortNumber
            {
                [DebuggerStepThrough]
                get { return localPortNumber; }
                [DebuggerStepThrough]
                set { localPortNumber = value; }
            }

            protected int remotePortNumber = 1;
            public int RemotePortNumber
            {
                [DebuggerStepThrough]
                get { return remotePortNumber; }
                [DebuggerStepThrough]
                set { remotePortNumber = value; }
            }

            protected IPAddress remoteIp;
            [XmlIgnore]
            public IPAddress RemoteIp
            {
                [DebuggerStepThrough]
                get { return remoteIp; }
                [DebuggerStepThrough]
                set { remoteIp = value; }
            }

            protected bool overwriteIP = false;
            public bool OverwriteIP
            {
                [DebuggerStepThrough]
                get { return overwriteIP; }
                [DebuggerStepThrough]
                set { overwriteIP = value; }
            }

            protected bool broadcast = false;
            public bool Broadcast
            {
                [DebuggerStepThrough]
                get { return broadcast; }
                [DebuggerStepThrough]
                set { broadcast = value; }
            }

            /// <summary>
            /// for XML serialization
            /// </summary>
            public String RemoteIpAddress
            {
                [DebuggerStepThrough]
                get
                {
                    if (remoteIp == null)
                    {
                        return "";
                    }
                    return remoteIp.ToString();
                }
                [DebuggerStepThrough]
                set {
                    IPAddress.TryParse(value, out this.remoteIp); 
                }
            }

            [DebuggerStepThrough]
            public UdpSocketInfo()
            {
            }

            [DebuggerStepThrough]
            public UdpSocketInfo(IPAddress remoteIp, int localPort, int remotePort, bool overwriteIP, bool broadcast)
                : this()
            {
                this.remoteIp = remoteIp;
                this.localPortNumber = localPort;
                this.remotePortNumber = remotePort;
                this.overwriteIP = overwriteIP;
                this.broadcast = broadcast;
            }

            public UdpSocketInfo Clone()
            {
                return new UdpSocketInfo(this.remoteIp, this.localPortNumber, this.remotePortNumber, this.overwriteIP, this.broadcast);
            }

            public override string ToString()
            {
                if (!IsConfigured)
                {
                    return "UDP: not configured";
                }
                if (broadcast)
                {
                    return string.Format("UDP broadcast: local port {0}, remote port {1}", localPortNumber, remotePortNumber);
                }
                if (overwriteIP)
                {
                    return string.Format("UDP: local port {0}, reply to sender", localPortNumber);
                }
                return string.Format("UDP: local port {1}, {0}-{2}", ((remoteIp != null) ? remoteIp.ToString() : "IP not set"), localPortNumber, remotePortNumber);
            }

            public bool IsConfigured
            {
                get
                {
                    if (overwriteIP && localPortNumber > 0 && localPortNumber < ushort.MaxValue)
                    {
                        return true;
                    }
                    return remoteIp != null && localPortNumber > 0 && localPortNumber < ushort.MaxValue && remotePortNumber > 0 && remotePortNumber < ushort.MaxValue;
                }
            }
        }

        public class PortEmulatorInfo
        {
            public enum DataSouceType
            {
                FILE,
                BYTE_ARRAY
            }
            public DataSouceType DataSource { get; set; }
            // used when DataSource is set to FILE
            public string FileName { get; set; }
            // used when DataSource is set to BYTE_ARRAY
            public byte[] Data { get; set; }

            public PortEmulatorInfo() { }
            public PortEmulatorInfo(DataSouceType dataSource, string fileName, byte[] data)
                : this()
            {
                this.DataSource = dataSource;
                this.FileName = fileName;
                this.Data = data;
            }

            public override string ToString()
            {
                switch (DataSource)
                {
                    case DataSouceType.BYTE_ARRAY: return string.Format("Port emulator: preconfigured buffer");
                    case DataSouceType.FILE: return string.Format("Port emulator: file {0}", Path.GetFileName(FileName));
                    default: throw new NotImplementedException(string.Format("DataSource {0} not implemented", DataSource));
                }
            }

            internal PortEmulatorInfo Clone()
            {
                PortEmulatorInfo clone = new PortEmulatorInfo();
                clone.DataSource = this.DataSource;
                clone.FileName = this.FileName;
                if (this.Data != null){
                    clone.Data = (byte[])this.Data.Clone();
                }
                return clone;
            }
        }
    }
}
