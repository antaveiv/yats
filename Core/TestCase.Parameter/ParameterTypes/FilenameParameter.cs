﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;

namespace yats.TestCase.Parameter
{
    public class FilenameParameter : AbstractParameterValue
    {
        protected string val;
		public string FileName {
            [DebuggerStepThrough]
			get {
				return val;
			}
            [DebuggerStepThrough]
			set {
				val = value;
			}
		}

        protected bool mustExist = false;
		public bool MustExist {
            [DebuggerStepThrough]
			get {
				return mustExist;
			}
            [DebuggerStepThrough]
            set
            {
                mustExist = value;
            }
		}

        [DebuggerStepThrough]
        public FilenameParameter()
		{
		}

        [DebuggerStepThrough]
        public FilenameParameter(bool mustExist):this()
		{
            this.mustExist = mustExist;
		}

        [DebuggerStepThrough]
        public FilenameParameter(bool mustExist, string fileName) : this(mustExist)
        {
            val = fileName;
        }

        public override AbstractParameterValue Clone()
        {
            return new FilenameParameter(this.mustExist, this.val);
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "file name", new string[] { this.GetType().AssemblyQualifiedName, });
        }

        public override string GetDisplayValue()
        {
            return string.Format("{0}", val);
        }

        public override object GetValue(IParameter format)
        {
            if (format == null)
            {
                return val;
            }
            return this;
        }

        public override int GetParamValueTypeHash()
        {
            return GetType().FullName.GetHashCode() ^ mustExist.GetHashCode();
        }
    }
}
