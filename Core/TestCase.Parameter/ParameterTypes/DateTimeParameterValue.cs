﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;

namespace yats.TestCase.Parameter
{
    public class DateTimeParameterValue : AbstractParameterValue
    {
        public DateTimeParameterValue()
        {
        }

        public DateTimeParameterValue(DateTime? value)
            : this()
        {
            this.paramValue = value;
        }

        protected DateTime? paramValue;
        public DateTime? Value
        {
            [DebuggerStepThrough]
            get
            {
                return paramValue;
            }
            [DebuggerStepThrough]
            set
            {
                paramValue = value;
            }
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "date/time", typeof(DateTime).AssemblyQualifiedName, 80);
        }

        public override AbstractParameterValue Clone()
        {
            return new DateTimeParameterValue(this.paramValue);
        }

        public override string GetDisplayValue()
        {
            if (paramValue.HasValue == false)
            {
                return string.Empty;
            }
            if (paramValue.Value.Date.Equals(DateTime.Now.Date))
            {
                return paramValue.Value.ToShortTimeString();
            }
            else
            {
                return paramValue.Value.ToString("u");
            }            
        }

        public override object GetValue(IParameter format)
        {
            return paramValue;
        }

        public override bool SetValue(IParameter format, object value)
        {
            if (value is DateTime)
            {
                this.paramValue = (DateTime)value;
                return true;
            }
            return false;
        }

        public override int GetParamValueTypeHash()
        {
            return typeof(DateTime).GUID.GetHashCode();
        }
    }
}
