/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;

namespace yats.TestCase.Parameter
{    
    public abstract class AbstractParameterValue
	{
        [DebuggerStepThrough]
		public AbstractParameterValue ()
		{
		}

        public abstract AbstractParameterValue Clone();

        public abstract void RegisterParameterType(IParameterTypeCollection parameterTypes);

        public abstract string GetDisplayValue();

        public abstract object GetValue(IParameter format);
        // Currently there is no need to override this method in parameter classes that handle their own values. For example, SerialPortSettingsParameter registers its own type in ParameterTypeCollection.
        // In such case, instead of SetValue, a simple assignment can be used
        public virtual bool SetValue(IParameter format, object value)
        {
            return false;
        }

        public abstract int GetParamValueTypeHash();
    }
}

