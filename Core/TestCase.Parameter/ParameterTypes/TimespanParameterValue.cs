﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Xml;
using System.Xml.Serialization;
using yats.Utilities;

namespace yats.TestCase.Parameter
{
    public class TimespanParameterValue : AbstractParameterValue
    {
        public TimespanParameterValue()
        {
        }

        public TimespanParameterValue(TimeSpan? value)
            : this()
        {
            this.paramValue = value;
        }

        protected TimeSpan? paramValue;
        [XmlIgnore]
        public TimeSpan? Value
        {
            [DebuggerStepThrough]
            get
            {
                return paramValue;
            }
            [DebuggerStepThrough]
            set
            {
                paramValue = value;
            }
        }

        // XmlSerializer does not support TimeSpan, so use this property for serialization 
        // instead.
        [Browsable(false)]
        [XmlElement( DataType = "duration", ElementName = "Timespan" )]
        public string ValueAsString
        {
            get 
            {
                if(paramValue.HasValue)
                {
                    return XmlConvert.ToString( paramValue.Value );
                }
                return null;
            }
            set 
            { 
                if (string.IsNullOrEmpty(value))
                {
                    paramValue = null;
                } else {
                    paramValue = XmlConvert.ToTimeSpan(value); 
                }                
            }
        }


        public override AbstractParameterValue Clone()
        {
            return new TimespanParameterValue(this.paramValue);
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "timespan", typeof(TimeSpan).AssemblyQualifiedName);
        }

        public override string GetDisplayValue()
        {
            if (paramValue.HasValue == false)
            {
                return string.Empty;
            }
            return paramValue.Value.ToFriendlyDisplay(3);
        }

        public override object GetValue(IParameter format)
        {
            if(paramValue.HasValue)
            {
                return paramValue.Value;
            }
            return null;
        }

        public override bool SetValue(IParameter format, object value)
        {
            if (value is TimeSpan)
            {
                this.paramValue = (TimeSpan)value;
                return true;
            }
            return false;
        }

        public override int GetParamValueTypeHash()
        {
            return typeof(TimeSpan).GUID.GetHashCode();
        }
    }
}
