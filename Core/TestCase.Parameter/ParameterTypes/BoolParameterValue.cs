﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;

namespace yats.TestCase.Parameter
{
    public class BoolParameterValue : AbstractParameterValue
    {
		protected bool val;
		public bool Value {
            [DebuggerStepThrough]
			get {
				return val;
			}
            [DebuggerStepThrough]
			set {
				val = value;
			}
		}

        [DebuggerStepThrough]
        public BoolParameterValue()
		{
		}

        [DebuggerStepThrough]
        public BoolParameterValue(bool value)
		{
			val = value;
		}

        public override AbstractParameterValue Clone()
        {
            return new BoolParameterValue(this.val);
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "boolean", new string[] { typeof(bool).AssemblyQualifiedName });
        }

        public override string GetDisplayValue()
        {
            return string.Format("{0}", val);
        }

        public override object GetValue(IParameter format)
        {
            return val;
        }
        
        public override bool SetValue(IParameter format, object value)
        {
            if (value is bool)
            {
                this.val = (bool)value;
                return true;
            }
            return false;
        }

        public override int GetParamValueTypeHash()
        {
            return typeof(bool).GUID.GetHashCode();
        }
    }
}
