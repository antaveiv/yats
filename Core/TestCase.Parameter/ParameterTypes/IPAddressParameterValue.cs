﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;
using System.Net;
using System.Xml.Serialization;
using yats.Utilities;

namespace yats.TestCase.Parameter
{
    public class IPAddressParameterValue : AbstractParameterValue
    {
        protected SerializableIPAddress ip;
        public SerializableIPAddress IP
        {
            [DebuggerStepThrough]
            get
            {
                return ip;
            }
            [DebuggerStepThrough]
            set
            {
                ip = value;
            }
        }

        [DebuggerStepThrough]
        public IPAddressParameterValue()
		{
		}
        
        [DebuggerStepThrough]
        public IPAddressParameterValue(IPAddress ip): this(new SerializableIPAddress(ip))
        {
        }

        public IPAddressParameterValue(SerializableIPAddress ip)
        {
            this.ip = ip;
        }

        public IPAddressParameterValue(string ip)
        {
            this.ip = new SerializableIPAddress(IPAddress.Parse(ip));
        }

        public override AbstractParameterValue Clone()
        {
            return new IPAddressParameterValue(this.ip);
        }

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterParameterType(this.GetType(), "IP address", new string[] { typeof(IPAddress).AssemblyQualifiedName });
        }

        public override string GetDisplayValue()
        {
            return ip.ToString();
        }

        public override object GetValue(IParameter format)
        {
            return ip;
        }

        public override bool SetValue(IParameter format, object value)
        {
            if (value is IPAddress)
            {
                this.ip = new SerializableIPAddress(value as IPAddress);
                return true;
            }
            if (value is SerializableIPAddress)
            {
                this.ip = (value as SerializableIPAddress);
                return true;
            }
            return false;
        }

        public override int GetParamValueTypeHash()
        {
            int res = 71884491;
            if (ip != null)
            {
                res ^= ip.GetHashCode();
            }
            return res;
        }
    }
}
