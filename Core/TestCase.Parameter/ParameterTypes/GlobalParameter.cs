﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;

namespace yats.TestCase.Parameter
{
    public class GlobalParameter : AbstractParameterValue
    {
        protected AbstractParameterValue paramValue;
        public AbstractParameterValue Value
        {
            [DebuggerStepThrough]
            get { return paramValue; }
            [DebuggerStepThrough]
            set { paramValue = value; }
        }

        protected string paramName;
        public string Name
        {
            [DebuggerStepThrough]
            get { return paramName; }
            [DebuggerStepThrough]
            set { paramName = value; }
        }
		
		
		public GlobalParameter()
		{
		}
		
		public GlobalParameter(AbstractParameterValue paramValue)
		{
			this.paramValue = paramValue;
		}
		
		public GlobalParameter(string name, AbstractParameterValue paramValue)
		{
			this.paramName = name;
			this.paramValue = paramValue;
		}

        public override void RegisterParameterType(IParameterTypeCollection parameterTypes)
        {
        }

        public override AbstractParameterValue Clone()
        {
            return new GlobalParameter(paramName, (paramValue != null ? paramValue.Clone() : null));
        }

        public override string GetDisplayValue()
        {
            if (paramValue != null)
            {
                return paramValue.GetDisplayValue();
            }
            return string.Empty;
        }

        public override object GetValue(IParameter format)
        {
            if (paramValue == null)
            {
                return null;
            }
            return paramValue.GetValue(format);
        }

        public override bool SetValue(IParameter format, object value)
        {
            if (paramValue == null)
            {
                return false;
            }
            return paramValue.SetValue(format, value);
        }

        public override int GetParamValueTypeHash()
        {
            return Value != null? Value.GetParamValueTypeHash() : 0;
        }
    }
}
