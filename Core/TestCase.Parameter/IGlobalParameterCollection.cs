/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using yats.Utilities;
using System.Collections.Generic;

namespace yats.TestCase.Parameter
{
    public class GlobalParameterManagerEventArgs : EventArgs
        {
        public Guid Id;
        public IGlobalParameters Manager;
        public GlobalParameterManagerEventArgs (Guid id, IGlobalParameters manager)
            {
            Id = id;
            Manager = manager;
            }
        }

    //[XmlInclude( typeof( GlobalParameterCollection ) )]
	public abstract class IGlobalParameterCollection
	{
        public abstract SerializableDictionary<Guid, IGlobalParameters> Managers
		{get;set;}
        public abstract List<IGlobalParameters> GetManagers(ParameterScope scope, bool addIfNotFound);

        public abstract void Add(IGlobalParameters manager);
        public abstract void Remove(IGlobalParameters manager);
        public abstract void Remove(Guid managerId);

        public GlobalParameter Get(GlobalValue param)
        {
            try
            {
                IGlobalParameters manager = this.Managers[param.StorageId];
                return manager.Get(param.ParameterID);
            }
            catch { }
            return null;
        }

        public event EventHandler<GlobalParameterEventArgs> ValueAdded;
        public event EventHandler<GlobalParameterEventArgs> ValueRemoved;
        public event EventHandler<GlobalParameterEventArgs> ValueUpdated;
        public event EventHandler<GlobalParameterManagerEventArgs> ManagerAdded;
        public event EventHandler<GlobalParameterManagerEventArgs> ManagerRemoved;

        public void RaiseValueAdded (object sender, GlobalParameterEventArgs e)
        {
            if (ValueAdded != null)
            {
            ValueAdded (sender, e);
            }
        }

        public void RaiseValueRemoved (object sender, GlobalParameterEventArgs e)
        {
            if (ValueRemoved != null)
            {
            ValueRemoved (sender, e);
            }
        }

        public void RaiseValueUpdated (object sender, GlobalParameterEventArgs e)
        {
            if (ValueUpdated != null)
                {
                ValueUpdated (sender, e);
            }
        }

        public void RaiseManagerAdded (object sender, GlobalParameterManagerEventArgs e)
        {
            if (ManagerAdded != null)
            {
            ManagerAdded (sender, e);
            }
        }

        public void RaiseManagerRemoved (object sender, GlobalParameterManagerEventArgs e)
        {
            if (ManagerRemoved != null)
            {
            ManagerRemoved (sender, e);
            }
        }
    }
}

