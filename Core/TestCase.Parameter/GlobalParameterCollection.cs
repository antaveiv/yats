/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.Linq;
using yats.Utilities;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace yats.TestCase.Parameter
{
	public class GlobalParameterCollection : IGlobalParameterCollection
	{
        protected SerializableDictionary<Guid, IGlobalParameters> managers = new SerializableDictionary<Guid, IGlobalParameters>( );
        public override SerializableDictionary<Guid, IGlobalParameters> Managers
		{
            [DebuggerStepThrough]
			get {return managers;}
            [DebuggerStepThrough]
			set {
                if(managers != null)
                {
                    foreach(var mgr in managers.Values)
                    {
                        mgr.ValueAdded -= RaiseValueAdded;
                        mgr.ValueRemoved -= RaiseValueRemoved;
                        mgr.ValueUpdated -= RaiseValueUpdated;
                    }
                }
                managers = value;
                if(managers != null)
                {
                    foreach(var mgr in managers.Values)
                    {
                        mgr.ValueAdded += RaiseValueAdded;
                        mgr.ValueRemoved += RaiseValueRemoved;
                        mgr.ValueUpdated += RaiseValueUpdated;
                    }
                }
            }
		}

        [DebuggerStepThrough]
		public GlobalParameterCollection ()
		{
            GlobalParameters mgr = new GlobalParameters(ParameterScope.GLOBAL);
            Add_( mgr );
		}
        
        public GlobalParameterCollection(IGlobalParameterCollection a)
        {
            foreach (var pair in a.Managers)
            {
                Add_(pair.Value);
            }
        }
        
        public override List<IGlobalParameters> GetManagers(ParameterScope scope, bool addIfNotFound)
        {
            List<IGlobalParameters> result = new List<IGlobalParameters>();
            if (managers == null)
            {
                result = new List<IGlobalParameters>();
            }
            else
            {
                result = managers.Values.Where(x => x.Scope == scope).ToList();
            }
            if (addIfNotFound && result.Count == 0)
            {
                IGlobalParameters manager = new GlobalParameters(scope);
                Add(manager);
                result.Add(manager);
            }
            return result;
        }

        private void Add_(IGlobalParameters mgr)
        {
            if (managers.ContainsKey(mgr.ID))
            {
                return;
            }
            managers[mgr.ID] = mgr;
            mgr.ValueAdded += RaiseValueAdded;
            mgr.ValueRemoved += RaiseValueRemoved;
            mgr.ValueUpdated += RaiseValueUpdated;
            RaiseManagerAdded(this, new GlobalParameterManagerEventArgs(mgr.ID, mgr));
            foreach (var p in mgr.Parameters)
            {
                RaiseValueAdded(this, new GlobalParameterEventArgs(p.Key, p.Value, mgr));
            }
        }

        public override void Add(IGlobalParameters mgr)
        {
            Add_(mgr);
        }

        public override void Remove(IGlobalParameters mgr)
        {
            if (managers.Remove(mgr.ID))
            {
                mgr.ValueAdded -= RaiseValueAdded;
                mgr.ValueRemoved -= RaiseValueRemoved;
                mgr.ValueUpdated -= RaiseValueUpdated;
                foreach (var p in mgr.Parameters)
                {
                RaiseValueRemoved (this, new GlobalParameterEventArgs (p.Key, p.Value, mgr));
                }
                RaiseManagerRemoved (this, new GlobalParameterManagerEventArgs (mgr.ID, mgr));               
            }
        }

        public override void Remove(Guid managerId)
        {
            try
            {
                if (managers.ContainsKey(managerId) == false){
                    return;
                }
                managers[managerId].ValueAdded -= RaiseValueAdded;
                managers[managerId].ValueRemoved -= RaiseValueRemoved;
                managers[managerId].ValueUpdated -= RaiseValueUpdated;
                var tmp = managers[managerId];
                managers.Remove(managerId);
                foreach (var p in managers[managerId].Parameters)
                {
                    RaiseValueRemoved(this, new GlobalParameterEventArgs(p.Key, p.Value, managers[managerId]));
                }
                RaiseManagerRemoved(this, new GlobalParameterManagerEventArgs(managerId, tmp));
            }
            catch
            { }            
        }
    }
}

