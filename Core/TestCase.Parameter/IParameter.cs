/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Xml.Serialization;

namespace yats.TestCase.Parameter
{
    [XmlInclude( typeof( TestParameterInfo ) )]
	public abstract class IParameter
	{
        public abstract string ParamType
		{
			get; set;
		}

        public abstract bool IsList
        {
            get;
            set;
        }

        public abstract bool IsArray
        {
            get;
            set;
        }

        public abstract string Name
		{
			get; set;
		}

        [XmlIgnore]
        public string DefaultDescription
        {
            get;
            set;
        }

        protected string userDescription;
        public string UserDescription
        {
            get { return userDescription; }
            set
            {
                if (DefaultDescription == value)
                {
                    userDescription = null;
                }
                else
                {
                    userDescription = value;
                }
            }
        }

        /// <summary>
        /// returns UserDescription if set, DefaultDescription otherwise
        /// </summary>
        public string ActiveDescription
        {
            get { return string.IsNullOrEmpty(UserDescription) ? DefaultDescription : UserDescription; }
        }

        public abstract ParameterDirection Direction
        {
            get;
            set;
        }

        public abstract AbstractParameterValue Value
		{
			get; set;
		}

        public abstract IParameter Clone();

        public bool IsParameter
        {
            get
            {
                return Direction == ParameterDirection.InputOutput || Direction == ParameterDirection.Input;
            }
        }

        public bool IsResult
        {
            get
            {
                return Direction == ParameterDirection.Output;
            }
        }

        public int? ValueTypeHashCode
        {
            get;
            set;
        }

        public abstract int GetParamValueTypeHash();

        public static object Convert(IParameter paramInfo, object value)
        {
            if (value == null)
            {
                return value;
            }

            if (paramInfo.IsArray)
            {
                if (value.GetType().IsArray) // already an array, just assign
                {
                    return value;
                }

                if (value is IList)
                //if (value.GetType().IsGenericType && value.GetType().GetGenericTypeDefinition() == typeof(List<>)) // convert list to array
                {
                    var valueAsList = (value as IList);
                    Array res = Array.CreateInstance(Type.GetType(paramInfo.ParamType), valueAsList.Count);
                    int index = 0;
                    foreach (var x in valueAsList)
                    {
                        res.SetValue(x, index++);
                    }
                    return res;
                }
                else
                {
                    Array res = Array.CreateInstance(Type.GetType(paramInfo.ParamType), 1);
                    res.SetValue(value, 0);
                    return res;
                }
            }

            if (paramInfo.IsList)
            {
                if (value.GetType().IsGenericType && value.GetType().GetGenericTypeDefinition() == typeof(List<>)) // already a List<>
                {
                    return value;
                }

                // create List<param type>
                Type elementType = Type.GetType(paramInfo.ParamType);
                Type[] types = new Type[] { elementType };
                Type listType = typeof(List<>);
                Type genericType = listType.MakeGenericType(types);
                IList list = Activator.CreateInstance(genericType) as IList;

                if (value.GetType().IsArray) // Array, convert to List
                {
                    foreach (object x in (Array)value)
                    {
                        list.Add(x);
                    }
                }
                else
                {
                    list.Add(value);
                }

                return list;
            }

            return value; //should not come here
        }
    }
}

