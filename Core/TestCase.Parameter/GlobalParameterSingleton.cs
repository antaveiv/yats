﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using System.Linq;
using yats.Utilities;

namespace yats.TestCase.Parameter
{
    public class GlobalParameterLoadErrorEventArgs : EventArgs
        {
        public string FileName;
        public Exception Exception;

        public GlobalParameterLoadErrorEventArgs (string fileName, Exception ex)
            {
            FileName = fileName;
            Exception = ex;
            }
        }

    public class GlobalParameterSingleton
    {
        public static event EventHandler<GlobalParameterLoadErrorEventArgs> OnLoadError;

        private static GlobalParameterSingleton s_instance = null;
        private static object mLockObject = new object();
		protected IGlobalParameterCollection m_globalParameters;
        const string shortFileName = "globals.xml";
        private string m_fileName = FileUtil.GetApplicationDataPath(shortFileName);

        [XmlIgnore]
		public static GlobalParameterSingleton Instance {
			get {
				lock (mLockObject) {
					if (s_instance == null) {
                        s_instance = new GlobalParameterSingleton();
                        s_instance.Load();
                        Debug.Assert(s_instance.GlobalParameters.Managers.Values.Count(x => x.Scope != ParameterScope.GLOBAL) == 0);
                    }
                    return s_instance;
                }
            }
        }

        public IGlobalParameterCollection GlobalParameters
        {
            [DebuggerStepThrough]
            get { return m_globalParameters; }
            [DebuggerStepThrough]
            set {
                UnregisterAutoSave(m_globalParameters);
                m_globalParameters = value;
                RegisterAutoSave(m_globalParameters);
            }
        }

        public void Save()
        {
            lock (mLockObject)
            {
                try
                {
                    //save to tmp, rename on success
                    string tempFileName = Path.ChangeExtension(m_fileName, ".tmp");
                    yats.Utilities.SerializationHelper.SerializeToFile<IGlobalParameterCollection>(m_globalParameters, tempFileName);

                    try
                    {
                        File.Delete(m_fileName);
                    }
                    catch
                    {
                    }
                    try
                    {
                        File.Move(tempFileName, m_fileName);
                    }
                    catch
                    {
                    }
                }
                catch (Exception ex)
                {
                    CrashLog.Write(ex, "GlobalParameterSingleton.Save()");
                }
            }
        }
                
        public void Load()
        {
            lock (mLockObject)
            {
                try
                {
                    if (File.Exists(m_fileName))
                    {
                        XmlSerializer serializer = yats.Utilities.SerializationHelper.GetSerializer(typeof(IGlobalParameterCollection));
                        using (TextReader reader = new StreamReader(m_fileName))
                        {
                            var loaded = (IGlobalParameterCollection)serializer.Deserialize(reader);
                            this.GlobalParameters = loaded;
                        }
                    }
                    else
                    {
                        if (this.GlobalParameters == null)
                        {
                            this.GlobalParameters = new GlobalParameterCollection();
                        }
                    }
                }
                catch (Exception ex)
                {                    
                    if (this.GlobalParameters == null)
                    {
                        this.GlobalParameters = new GlobalParameterCollection();
                    }

                    if (OnLoadError != null)
                    {
                        OnLoadError(this, new GlobalParameterLoadErrorEventArgs(m_fileName, ex));
                    }
                }
                // create global storage if missing
                GlobalParameters.GetManagers(ParameterScope.GLOBAL, true);
                
                // remove test run storages - they are added when tests are loaded and may get into the global parameter file. This should reduce the size bloat
                foreach (var mgr in GlobalParameters.GetManagers(ParameterScope.TEST_RUN, false))
                {
                    GlobalParameters.Remove(mgr);
                }
            }
        }

        public void RegisterAutoSave(IGlobalParameterCollection globals)
        {
			if (globals == null) {
                return;
            }
            globals.ValueAdded += value_Changed;
            globals.ValueRemoved += value_Changed;
            globals.ValueUpdated += value_Changed;
            globals.ManagerAdded += mgr_Changed;
            globals.ManagerRemoved += mgr_Changed;
        }

        void value_Changed (object sender, GlobalParameterEventArgs e)
        {
            Save();
        }

        void mgr_Changed (object sender, GlobalParameterManagerEventArgs e)
        {
            Save();
        }

        public void UnregisterAutoSave(IGlobalParameterCollection globals)
        {
			if (globals == null) {
                return;
            }
            globals.ValueAdded -= value_Changed;
            globals.ValueRemoved -= value_Changed;
            globals.ValueUpdated -= value_Changed;
            globals.ManagerAdded -= mgr_Changed;
            globals.ManagerRemoved -= mgr_Changed;
        }
    }
}
