﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;

namespace yats.TestCase.Parameter
{
    public interface IEditor
    {
		/// <summary>
		/// Set editor control values from a given set of parameters. Parameters may contain multiple values (multiple selection in parameter editor). It is up to the Editor if common values should be found and displayed
		/// </summary>
		/// <param name='parameters'>
		/// Collection of parameter values to be edited
		/// </param>
		/// <param name='globals'>
		/// Global parameter handlers. If a parameter value being edited is a global value, the handlers should be used to find the actual value
		/// </param>
        void ParameterToEditor(AbstractParameterValue parameter);
		// Update the parameter values from editor control fields
        void EditorToParameter(AbstractParameterValue parameter);
		// Visible editor tab name
        string TabName();
		// Called by parameter type initialization. The editor should register itself here
        void RegisterEditorType(IParameterTypeCollection parameterTypes);
        /// <summary>
        /// An Editor should raise OnConfirm in order to validate and close the parent form (on e.g. double-click or keyboard shortcut)
        /// </summary>
        event EventHandler OnConfirm;
		// Allow raising an OnConfirm event externally
        void RaiseConfirm();
        // Should throw an exception if editor control values are invalid. validatingForSave - if true, treat errors as critical. 
        // validatingForSave is false when swithing between editor tabs. True when OK is pressed. For example, a configured file name should be ignored when switching tabs - perhaps I don't want to use a file. When OK is pressed, the file existence should be checked
        void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave);
    }
}
