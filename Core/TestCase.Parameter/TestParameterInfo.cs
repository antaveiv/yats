/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Data;
using System.Diagnostics;
using System.Xml.Serialization;

namespace yats.TestCase.Parameter
{
	public class TestParameterInfo : IParameter
	{
        protected string paramType;
        public override string ParamType
        {
            [DebuggerStepThrough]
            get { return paramType; }
            [DebuggerStepThrough]
            set { paramType = value; }
        }

        protected bool isList;
        public override bool IsList
        {
            [DebuggerStepThrough]
            get { return isList; }
            [DebuggerStepThrough]
            set { isList = value; }
        }
        
        protected bool isArray;
        public override bool IsArray
        {
            [DebuggerStepThrough]
            get { return isArray; }
            [DebuggerStepThrough]
            set { isArray = value; }
        }

		protected string name;
        public override string Name
		{
            [DebuggerStepThrough]
			get {return name;}
            [DebuggerStepThrough]
			set {name = value;}
		}
        
		protected AbstractParameterValue paramValue;
        public override AbstractParameterValue Value
		{
            [DebuggerStepThrough]
			get {return paramValue;}
            [DebuggerStepThrough]
			set {paramValue = value;}
		}

        protected ParameterDirection directionType;
        public override ParameterDirection Direction
        {
            [DebuggerStepThrough]
            get { return directionType; }
            [DebuggerStepThrough]
            set { directionType = value; }
        }

        [DebuggerStepThrough]
		public TestParameterInfo ()
		{
		}

        [DebuggerStepThrough]
		public TestParameterInfo (string name, string paramType, bool isList, bool isArray, ParameterDirection direction)
		{
			this.name = name;
			this.paramType = paramType;
            this.isArray = isArray;
            this.isList = isList;
            this.directionType = direction;
		}

        [DebuggerStepThrough]
        public TestParameterInfo(string name, string description, string paramType, bool isList, bool isArray, ParameterDirection direction)
            :this(name, paramType, isList, isArray, direction)
        {
            this.DefaultDescription = description;
        }
    
        public override IParameter Clone()
        {
            var result = new TestParameterInfo(name, DefaultDescription, paramType, isList, isArray, directionType);
            if (this.paramValue != null)
            {
                result.paramValue = this.paramValue.Clone();
            }
            return result;
        }

        public override bool Equals(object obj)
        {
            IParameter other = obj as IParameter;
            if(other == null)
            {
                return false;
            }

            bool result = this.Direction == other.Direction
                && this.IsArray == other.IsArray
                && this.IsList == other.IsList
                && this.IsParameter == other.IsParameter
                && this.IsResult == other.IsResult
                && this.ParamType == other.ParamType;

            return result;
        }

        public override int GetHashCode()
        {
            int result = 1;
            if (paramType != null)
            {
                result = result ^ paramType.GetHashCode();
            }
            result = result ^ isArray.GetHashCode();
            result = result ^ isList.GetHashCode();
            result = result ^ directionType.GetHashCode();
            return result;
        }

        public override string ToString()
        {
            return string.Format( "{0} '{1}' {2}[{3}]", (IsParameter)?"Parameter" : "Result", Name,
                ((isList)? "list of " : ((isArray)? "array of " : "")), paramType);
        }

        public override int GetParamValueTypeHash()
        {
            return GetHashCode();
        }
    }
}

