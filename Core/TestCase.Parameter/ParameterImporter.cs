﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using yats.Utilities;

namespace yats.TestCase.Parameter
{
    public class ParameterImporter
    {
        public static void ImportGlobalParameters(IGlobalParameterCollection fromStorage, ICollection<GlobalValue> parameters, IGlobalParameterCollection toStorage)
        {
            ImportGlobalParameters(fromStorage, parameters, toStorage, false);
        }

        public static void ImportGlobalParameters(IGlobalParameterCollection fromStorage, ICollection<GlobalValue> parameters, IGlobalParameterCollection toStorage, bool overwrite, params ParameterScope[] scope)
        {
            // import global parameters from a loaded test run file            
            if (fromStorage == null || toStorage == null || parameters == null)
            {
                return;
            }
            
            HashSet<ParameterScope> scopes = new HashSet<ParameterScope>(scope);

            foreach (var p in parameters)
            {
                if (scopes.IsNullOrEmpty() == false && scopes.Contains(fromStorage.Managers[p.StorageId].Scope) == false)
                {
                    continue;
                }

                IGlobalParameters manager;

                if (toStorage.Managers.TryGetValue(p.StorageId, out manager) == false)
                {
                    manager = new GlobalParameters(p.StorageId, fromStorage.Managers[p.StorageId].Scope);
                    toStorage.Add(manager);
                }

                if (manager.Parameters.ContainsKey(p.ParameterID) && overwrite == false) {
                    continue;
                }

                manager.Add(p.ParameterID, fromStorage.Managers[p.StorageId].Get(p.ParameterID));
            }
        }
    }
}
