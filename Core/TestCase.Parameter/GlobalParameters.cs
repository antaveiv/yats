/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using yats.Utilities;

namespace yats.TestCase.Parameter
{
    /// <summary>
    /// Contains a list of global parameter values. For example, one such list could be added to a test run to define 
    /// parameter values that are used between test steps in the test run. Another list could contain parameters that are common
    /// between different test runs. In this case the test case parameter values are defined as GUID identificators (GlobalValue class objects). 
    /// The actual values are then looked up in a collection of GlobalParameter objects (test run-specific, global, etc.)
    /// </summary>
    public class GlobalParameters : IGlobalParameters
	{
        protected SerializableDictionary<Guid, GlobalParameter> dictionary = new SerializableDictionary<Guid, GlobalParameter>();

        public override SerializableDictionary<Guid, GlobalParameter> Parameters
		{
            [DebuggerStepThrough]
			get {return dictionary;}
            [DebuggerStepThrough]
			set {this.dictionary = value;}
		}
        
        [DebuggerStepThrough]
        public GlobalParameters(Guid id, ParameterScope scope)
            : this(scope)
        {
            this.id = id;
        }

        [DebuggerStepThrough]
		public GlobalParameters (ParameterScope scope):this()
		{
            this.scope = scope;
		}

        [DebuggerStepThrough]
        public GlobalParameters()
        {
        }

        [DebuggerStepThrough]
        public override GlobalParameter Get(Guid id)
        {
            try
            {
                if (dictionary.ContainsKey(id))
                {
                    return dictionary[id];
                }
            }
            catch { }
            return null;
		}

        public override Guid Set(GlobalParameter value)
		{
            Guid id = Guid.NewGuid( );
			dictionary[id] = value;
            RaiseValueAdded( id, value, this );
            return id;
		}

        public override void Add(Guid id, GlobalParameter value)
        {
            dictionary[id] = value;
            RaiseValueAdded(id, value, this);
        }

        public override void Set(Guid id, GlobalParameter value)
        {
            if (dictionary.ContainsKey(id))
            {
                dictionary[id] = value;
                RaiseValueUpdated(id, value, this);
            }
            else
            {
                dictionary[id] = value;
                RaiseValueAdded(id, value, this);
            }
        }
                
        public override void Remove(Guid id)
        {
            GlobalParameter value;
            dictionary.TryGetValue(id, out value);
            if(dictionary.Remove( id ))
            {
                RaiseValueRemoved( id, value, this );
            }
        }
	}
}

