﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace yats.TestCase.Parameter
{
    public class ParameterTypeCollection : IParameterTypeCollection
    {
        /* 
         * Why is it hard to make this class not static? I tried but changed my mind and created a singleton instead (see r1036). It's possible but adds some bad dependencies, most notably:
         * 1. CompositeTestRepositoryManager is a repository manager discovered and constructed during runtime. It uses the singleton in CompositeTestCase to 
         * create the test and global parameters from an XML file. Perhaps the whole XML deserialization should be made external to the CompositeTest project 
         * 2. ParameterValueListEditor is similar - it is not initialized directly from the main program, therefore passing a reference to a ParameterTypeCollection would not look right
         * In other places it is possible to pass around a ParameterTypeCollection object
         */

        protected static ParameterTypeCollection instance = null;
        public static ParameterTypeCollection Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ParameterTypeCollection();
                }
                return instance;
            }
        }
        public const int DEFAULT_VALUE_TYPE_PRIORITY = 100;

        private static List<Entry> s_entryList = new List<Entry>();
        private static readonly ManualResetEvent s_initializedLock = new ManualResetEvent(false);//wait for multi-threaded initialization before loading or running tests
        
        // a list entry connects one or more object type names (e.g. system.integer) with its supported AbstractParameterValue class and editors
        private class Entry
        {
            // associate parameter value types with their priorities
            public Dictionary<string, int> DataTypes = new Dictionary<string,int>();
            //Type: AbstractParameterValue
            public Type ValueType;
            public string DisplayName;
            //Type: IEditor,  int - priority, higher is better
            public Dictionary<Type, int> Editors = new Dictionary<Type,int>();
            //instead or together with the dataTypes list, a more advanced matching can be provided with a delegate method
            public GetSuitableTypeDelegate GetSuitableTypeDelegate;
            public GetSuitableEditorTypeDelegate GetSuitableEditorDelegate;

            public Entry(GetSuitableEditorTypeDelegate isSuitableDelegate)
            {
                this.GetSuitableEditorDelegate = isSuitableDelegate;
            }

            public Entry(Type valueType, string paramDisplayName, List<string> dataTypes, int valueTypePriority)
            {
                foreach (var t in dataTypes)
                {
                    if (this.DataTypes.ContainsKey(t) == false)
                    {
                        this.DataTypes.Add(t, valueTypePriority);
                    }
                }
                this.ValueType = valueType;
                this.DisplayName = paramDisplayName;
            }

            public override string ToString()
            {
                return string.Format("[{1}]   {0}", ValueType.Name, DisplayName);
            }
        }
                       
        private ParameterTypeCollection()
        {
        }

        /// <summary>
        /// Register supported parameter type
        /// </summary>
        /// <param name="paramType">An instance of class inheriting from AbstractParameterValue</param>
        /// <param name="paramDisplayName">User-friendly name to use for paramType</param>
        /// <param name="dataType">List of assembly-qualified data types that paramType is compatible with</param>
        /// <param name="editors">List of editors capable of editing the parameter value</param>
        /// <param name="canMultiEdit">Allow editing multiple parameter values at once?</param>
        private void RegisterParameterType(Type paramValueType, string paramDisplayName, List<string> dataTypes, Dictionary<Type, int> editors, int valueTypePriority)
        {            
            foreach (var entry in s_entryList)
            {
                if (entry.ValueType == paramValueType)
                {
                    entry.DisplayName = paramDisplayName;
                    foreach (var dt in dataTypes)
                    {
                        bool found = false;
                        foreach (var curr in entry.DataTypes)
                        {
                            if (dt == curr.Key)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            entry.DataTypes.Add(dt, valueTypePriority);
                        }
                    }

                    foreach (var ed in editors)
                    {
                        entry.Editors[ed.Key] = ed.Value;
                    }
                    return;
                }
            }
            var newEntry = new Entry(paramValueType, paramDisplayName, dataTypes, valueTypePriority);
            newEntry.Editors = editors;
            s_entryList.Add(newEntry);
        }

        // priority: higher number is higher priority
        public void RegisterParameterType(Type paramValueType, string paramDisplayName, string dataType, int valueTypePriority)
        {
            var dataTypes = new List<string>();
            if (string.IsNullOrEmpty(dataType) == false)
            {
                dataTypes.Add(dataType);
            }
            Dictionary<Type, int> editors = new Dictionary<Type, int>();
            RegisterParameterType(paramValueType, paramDisplayName, dataTypes, editors, valueTypePriority);
        }

        public void RegisterParameterType(Type paramValueType, string paramDisplayName, string dataType)
        {
            RegisterParameterType(paramValueType, paramDisplayName, dataType, DEFAULT_VALUE_TYPE_PRIORITY);
        }

        public void RegisterParameterType(Type paramValueType, string paramDisplayName, string[] dataTypes)
        {
            RegisterParameterType(paramValueType, paramDisplayName, dataTypes, DEFAULT_VALUE_TYPE_PRIORITY);
        }

        public void RegisterParameterType(Type paramValueType, string paramDisplayName, string[] dataTypes, int valueTypePriority)
        {
            Dictionary<Type, int> editors = new Dictionary<Type, int>();
            RegisterParameterType(paramValueType, paramDisplayName, new List<string>(dataTypes), editors, valueTypePriority);
        }

        public void RegisterParameterType(Type paramValueType, string paramDisplayName, GetSuitableTypeDelegate isSuitableDelegate)
        {
            foreach (var entry in s_entryList)
            {
                if (entry.ValueType == paramValueType)
                {
                    entry.DisplayName = paramDisplayName;
                    entry.GetSuitableTypeDelegate = isSuitableDelegate;
                    return;
                }
            }
            Entry newEntry = new Entry(paramValueType, paramDisplayName, new List<string>(), DEFAULT_VALUE_TYPE_PRIORITY);
            newEntry.GetSuitableTypeDelegate = isSuitableDelegate;
            s_entryList.Add(newEntry);
        }

        public void RegisterEditorType(GetSuitableEditorTypeDelegate isSuitableDelegate)
        {
            s_entryList.Add(new Entry(isSuitableDelegate));
        }

        /// <summary>
        /// Add association between AbstractParameterValue type and its editor
        /// </summary>
        /// <param name="paramValueType">type inheriting from AbstractParameterValue</param>
        /// <param name="editorType">Editor type, a class inheriting UserControl and IEditor</param>
        /// <param name="editorPriority">priority value, higher value = higher priority</param>
        /// <param name="canMultiEdit">can multiple values be edited at once?</param>
        public void RegisterEditorType(Type paramValueType, Type editorType, int editorPriority)
        {
            foreach (var entry in s_entryList.Where(x=> x.ValueType == paramValueType))
            {
                if (entry.Editors.ContainsKey(editorType))
                {
                    yats.Utilities.CrashLog.Write(string.Format("Warning: duplicate editor registration for {0}: {1}", paramValueType.AssemblyQualifiedName, editorType.AssemblyQualifiedName));
                }
                entry.Editors[editorType] = editorPriority;
                return;
            }
            
            Entry newEntry = new Entry(paramValueType, "", new List<string>(), 0);
            newEntry.Editors[editorType] = editorPriority;
            s_entryList.Add(newEntry);
        }
        
        public string GetDisplayName(AbstractParameterValue paramType)
        {
            s_initializedLock.WaitOne();
            foreach (var entry in s_entryList)
            {
                if (entry.ValueType == paramType.GetType())
                {
                    return entry.DisplayName;
                }
            }
            return string.Empty;
        }
        
        public void Initialize()
        {
            foreach (var t in Utilities.AssemblyHelper.Instance.GetDerivedTypes(typeof(AbstractParameterValue)))
            {
                ((AbstractParameterValue)(Activator.CreateInstance(t))).RegisterParameterType(this);
            }
            foreach (var t in Utilities.AssemblyHelper.Instance.GetDerivedTypes(typeof(IEditor)))
            {
                ((IEditor)(Activator.CreateInstance(t))).RegisterEditorType(this);
            }
            s_initializedLock.Set();
        }

        public Dictionary<IEditor, int> GetEditors(AbstractParameterValue valueType)
        {
            s_initializedLock.WaitOne();
            Dictionary<IEditor, int> result = new Dictionary<IEditor,int>();
            foreach (var entry in s_entryList.Where(x => x.GetSuitableEditorDelegate != null)){
                int editorPriority = DEFAULT_VALUE_TYPE_PRIORITY;
                Type editorType = entry.GetSuitableEditorDelegate(valueType, ref editorPriority);
                if (editorType != null && result.Where(x=>x.Key.GetType() == editorType).Count() == 0) // meaning the delegate says it's OK to edit this type. Do not add multiple editors of the same type
                {
                    result.Add((IEditor)Activator.CreateInstance(editorType), editorPriority);
                }
            }
            foreach (var entry in s_entryList.Where(x=>x.ValueType == valueType.GetType()))
            {                
                foreach(var editorType in entry.Editors){
                    if (result.Where(x => x.Key.GetType() == editorType.GetType()).Count() == 0)
                    {
                        result.Add((IEditor)Activator.CreateInstance(editorType.Key), editorType.Value);
                    }
                }
            }

            //var r = result.OrderByDescending(x => x.Value).Select(x => x.Key).ToList();
            return result;// r.Select(x => x.Key).Reverse().ToList();
        }

        public List<AbstractParameterValue> GetSuitableValueTypes(IParameter paramInfo)
        {
            s_initializedLock.WaitOne();
            Dictionary<AbstractParameterValue, int> result = new Dictionary<AbstractParameterValue, int>();

            foreach (var entry in s_entryList.Where(x => x.GetSuitableTypeDelegate != null))
            {
                int valueTypePriority = DEFAULT_VALUE_TYPE_PRIORITY;
                var tmp = entry.GetSuitableTypeDelegate(paramInfo, ref valueTypePriority);
                if (tmp != null)
                {
                    result[tmp] = valueTypePriority;
                }
            }

            // skip searching the dataTypes, they dont take into account lists
            if (paramInfo.IsList || paramInfo.IsArray)
            {
                var r = result.OrderBy(x => x.Value).ToList();
                return r.Select(x => x.Key).Reverse().ToList();
            }

            foreach (var entry in s_entryList)
            {
                Type type = Type.GetType(paramInfo.ParamType);
                if (type == null)
                {
                    continue;
                }
                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    type = type.GetGenericArguments()[0];
                    if (entry.DataTypes.Keys.Contains(type.AssemblyQualifiedName))
                    {
                        result[(AbstractParameterValue)Activator.CreateInstance(entry.ValueType)] = entry.DataTypes[type.AssemblyQualifiedName];
                    }
                }

                if (entry.DataTypes.Keys.Contains(paramInfo.ParamType))
                {
                    result[(AbstractParameterValue)Activator.CreateInstance(entry.ValueType)] = entry.DataTypes[type.AssemblyQualifiedName];
                }
            }

            {
                var r = result.OrderBy(x => x.Value).ToList();
                return r.Select(x => x.Key).Reverse().ToList();
            }
        }

        public int GetNumberOfEntries()
        {
            s_initializedLock.WaitOne();
            return s_entryList.Count;
        }
    }
}
