﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace yats.TestCase.Parameter
{
    public delegate AbstractParameterValue GetSuitableTypeDelegate(IParameter parameterInfo, ref int valueTypePriority);
    public delegate Type GetSuitableEditorTypeDelegate(AbstractParameterValue parameterValue, ref int editorPriority);
    public interface IParameterTypeCollection
    {
        /// <summary>
        /// Add association between AbstractParameterValue type and its editor
        /// </summary>
        /// <param name="paramValueType">type inheriting from AbstractParameterValue</param>
        /// <param name="editorType">Editor type, a class inheriting UserControl and IEditor</param>
        /// <param name="editorPriority">priority value, higher value = higher priority</param>
        /// <param name="canMultiEdit">can multiple values be edited at once?</param>
        void RegisterEditorType(Type paramValueType, Type editorType, int editorPriority);
        void RegisterEditorType(GetSuitableEditorTypeDelegate isSuitableDelegate);
        void RegisterParameterType(Type paramValueType, string paramDisplayName, string dataType, int valueTypePriority);
        void RegisterParameterType(Type paramValueType, string paramDisplayName, string dataType);
        void RegisterParameterType(Type paramValueType, string paramDisplayName, string[] dataTypes);
        void RegisterParameterType(Type paramValueType, string paramDisplayName, string[] dataTypes, int valueTypePriority);
        void RegisterParameterType(Type paramValueType, string paramDisplayName, GetSuitableTypeDelegate isSuitableDelegate);

        //TODO split interface to one for registering types an another for consuming
        List<AbstractParameterValue> GetSuitableValueTypes(IParameter paramInfo);
        Dictionary<IEditor, int> GetEditors(AbstractParameterValue valueType);
    }
}
