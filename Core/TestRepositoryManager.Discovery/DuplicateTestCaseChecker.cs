﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using yats.TestRepositoryManager.Interface;

namespace yats.TestRepositoryManager.Discovery
{
    public class DuplicateTestCaseChecker
    {
        public delegate void DuplicateErrorDelegate(string testCaseId);
        public DuplicateErrorDelegate OnDuplicateFound;

        public DuplicateTestCaseChecker() { }
        public DuplicateTestCaseChecker(DuplicateErrorDelegate onDuplicateFound)
        {
            OnDuplicateFound += onDuplicateFound;
        }

        public void Check (object sender, RepositoryManagerCollectionEventArgs e)
        {
            if (OnDuplicateFound != null)
            {
                HashSet<string> uniqueIds = new HashSet<string>();

                var allTestCases = e.Repository.Managers.SelectMany(manager => manager.TestCases.Select(tc => manager.GetUniqueTestId(tc.TestCase))).ToList();

                var duplicateTests = allTestCases.GroupBy(x => x)
                            .Where(group => group.Count() > 1)
                            .Select(group => group.Key).ToList();
                
                duplicateTests.ForEach(x => OnDuplicateFound(x));
            }
        }
    }
}
