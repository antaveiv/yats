﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Threading;
using yats.Attributes;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.Discovery.Properties;
using yats.TestRepositoryManager.Interface;
using yats.Utilities;

namespace yats.TestRepositoryManager.Discovery
{
    public class TestRepositoryManagerCollection : ITestRepositoryManagerCollection, IDisposable
    {
        public event EventHandler<RepositoryManagerCollectionEventArgs> InitializationStarting;
        public event EventHandler<RepositoryManagerCollectionEventArgs> InitializationFinished;
        public event EventHandler<AssemblyLoadingEventArgs> BeforeLoadingManagerAssembly;
        public event EventHandler<AssemblyLoadingEventArgs> BeforeLoadingTestAssembly;
        public event EventHandler<AssemblyLoadedEventArgs> OnTestAssemblyLoad;
        public event EventHandler<RepositoryManagerEventArgs> ManagerAdded;
        public event EventHandler<ExceptionEventArgs> OnError;

        protected ManualResetEvent initializationLock = new ManualResetEvent(false);

        protected List<ITestRepositoryManager> managers;
        
        public void Initialize(bool recursive)
        {
            try
            {
                //raise event
                if (InitializationStarting != null)
                {
                    foreach (EventHandler<RepositoryManagerCollectionEventArgs> handler in InitializationStarting.GetInvocationList())
                    {
                        try
                        {
                            handler(this, new RepositoryManagerCollectionEventArgs() { Repository = this });
                        }
                        catch (Exception e)
                        {
                            CrashLog.Write(e, "Error in InitializationStarting " + handler.Method.Name);
                        }
                    }
                }

                managers = new List<ITestRepositoryManager>();

                foreach (string assemblyFileName in yats.Utilities.AssemblyHelper.Instance.GetAssemblyFileNames(null))
                {
                    if (IsKnownIgnoredAssembly(assemblyFileName))
                    {
                        continue;
                    }
                    try
                    {
                        if (BeforeLoadingManagerAssembly != null)
                        {
                            CancelEventArgs e = new CancelEventArgs(false);
                            BeforeLoadingManagerAssembly(this, new AssemblyLoadingEventArgs() { FullAssemblyPath = assemblyFileName });
                            if (e.Cancel)
                            {
                                continue;
                            }
                        }

                        Assembly asm = Assembly.LoadFrom(assemblyFileName);
                        Type[] typesInAssembly = asm.GetTypes();
                        bool managerFound = false;
                        foreach (Type type in typesInAssembly)
                        {
                            try
                            {
                                if (null != type.GetInterface(typeof(ITestRepositoryManager).FullName))
                                {
                                    // can not instantiate abstract types, skip
                                    if (type.IsAbstract || type.IsInterface)
                                    {
                                        continue;
                                    }

                                    if (UnavailablePlatformAttribute.IsPlatformSupported(type) == false)
                                    {
                                        continue;
                                    }

                                    // We found a type, load it
                                    /*Console.WriteLine(string.Format( "Assembly: {0} has a type {1} that " +
                                                                     "implements {2}",
                                                                     asm.FullName, type.FullName, typeof(ITestCase).FullName));
                                    */
                                    ITestRepositoryManager instance = (ITestRepositoryManager)Activator.CreateInstance(type);

                                    managers.Add(instance);
                                    managerFound = true;
                                    if (recursive)
                                    {
                                        instance.BeforeLoadingTestAssembly += Manager_BeforeLoadingTestAssembly;
                                        instance.OnTestAssemblyLoad += Manager_OnTestAssemblyLoad;
                                        instance.Initialize();
                                    }

                                    ManagerAdded?.Invoke(this, new RepositoryManagerEventArgs() { Repository = instance });
                                }

                            }
                            catch (Exception ex)
                            {
                                OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
                            }
                        }
                        if (managerFound == false)
                        {
                            AddIgnoredAssembly(assemblyFileName);
                        }
                    }
                    catch (BadImageFormatException ex)
                    {
                        AddIgnoredAssembly(assemblyFileName);
                        OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
                    }
                    catch (Exception ex)
                    {
                        OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
                    }
                }
            }
            catch (Exception ex)
            {
                OnError?.Invoke(this, new ExceptionEventArgs() { Exception = ex });
            }
            finally
            {
                initializationLock.Set();

                //raise event
                if (InitializationFinished != null)
                {
                    foreach (EventHandler<RepositoryManagerCollectionEventArgs> handler in InitializationFinished.GetInvocationList())
                    {
                        try
                        {
                            handler(this, new RepositoryManagerCollectionEventArgs() { Repository = this });
                        }
                        catch (Exception e)
                        {
                            CrashLog.Write(e, "Error in InitializationFinished " + handler.Method.Name);
                        }
                    }
                }
            }
        }

        private bool IsKnownIgnoredAssembly(string assemblyFileName)
        {
            if (Settings.Default.IgnoredAssemblies != null)
            {
                using (FileStream fs = new FileStream(assemblyFileName, FileMode.Open, FileAccess.Read))
                using (BufferedStream bs = new BufferedStream(fs))
                using (var cryptoProvider = new SHA1CryptoServiceProvider())
                {
                    string hash = ByteArray.ToHexString(cryptoProvider.ComputeHash(bs));
                    return Settings.Default.IgnoredAssemblies.Contains(assemblyFileName + "|" + hash);
                }
            }
            return false;
        }

        private void AddIgnoredAssembly(string assemblyFileName)
        {
            if (Settings.Default.IgnoredAssemblies == null)
            {
                Settings.Default.IgnoredAssemblies = new System.Collections.Specialized.StringCollection();
            }

            using (FileStream fs = new FileStream(assemblyFileName, FileMode.Open, FileAccess.Read))
            using (BufferedStream bs = new BufferedStream(fs))
            using (var cryptoProvider = new SHA1CryptoServiceProvider())
            {
                string hash = ByteArray.ToHexString(cryptoProvider.ComputeHash(bs));
                foreach(var s in Settings.Default.IgnoredAssemblies)
                {
                    if (s.StartsWith(assemblyFileName))// existing with old hash
                    {
                        Settings.Default.IgnoredAssemblies.Remove(s);
                        break;
                    }
                }
                Settings.Default.IgnoredAssemblies.Add(assemblyFileName + "|" + hash);
                Settings.Default.Save();
            }
        }

        void Manager_OnTestAssemblyLoad(object sender, AssemblyLoadedEventArgs e)
        {
            OnTestAssemblyLoad?.Invoke(sender, e);
        }

        void Manager_BeforeLoadingTestAssembly(object sender, AssemblyLoadingEventArgs e)
        {
            BeforeLoadingTestAssembly?.Invoke(sender, e);
        }

        public List<ITestRepositoryManager> Managers
        {
            [DebuggerStepThrough]
            get
            {
                initializationLock.WaitOne();
                return managers;
            }
        }

        public ITestRepositoryManager this[int i]
        {
            [DebuggerStepThrough]
            get
            {
                initializationLock.WaitOne();
                return managers[i];
            }
        }

        [DebuggerStepThrough]
        public string GetUniqueTestId(ITestCase testCase)
        {
            initializationLock.WaitOne();

            foreach (var mgr in managers)
            {
                if (mgr.IsMyTestCase(testCase))
                {
                    return mgr.GetUniqueTestId(testCase);
                }
            }
            throw new Exception(string.Format("Manager not found for test case {0}", testCase));
        }

        [Obsolete]
        [DebuggerStepThrough]
        public string GetTestName(ITestCase testCase)
        {
            initializationLock.WaitOne();

            foreach (var mgr in managers)
            {
                try
                {
                    if (mgr.IsMyTestCase(testCase))
                    {
                        return mgr.GetTestName(testCase);
                    }
                }
                catch { }
            }
            throw new Exception(string.Format("Manager not found for test case {0}", testCase));
        }

        public ITestCase GetByUniqueId(string testCaseUniqueId)
        {
            initializationLock.WaitOne();

            ITestCase result = null;
            foreach (var mgr in managers)
            {
                result = mgr.GetByUniqueId(testCaseUniqueId);
                if (result != null)
                {
                    return result;
                }
            }
            return null;
        }

        public ITestRepositoryManager GetManagerByTestUniqueId(string testCaseUniqueId)
        {
            initializationLock.WaitOne();
            foreach (var mgr in managers)
            {
                if (mgr.IsMyTestCase(testCaseUniqueId))
                {
                    return mgr;
                }
            }
            return null;
        }

        #region IDisposable
        private bool _disposed = false;

        ~TestRepositoryManagerCollection()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);

            // Use SupressFinalize in case a subclass 
            // of this type implements a finalizer.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    initializationLock.Close();
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
        }
        #endregion
    }
}
