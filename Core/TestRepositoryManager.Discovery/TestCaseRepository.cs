﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using yats.TestRepositoryManager.Interface;

namespace yats.TestRepositoryManager.Discovery
{
    public class TestCaseRepository : IDisposable
    {
        private static object s_sync = new object();
        ITestRepositoryManagerCollection managers;
        public ITestRepositoryManagerCollection Managers
        {
            [DebuggerStepThrough]
            get { return managers; }
        }

        [DebuggerStepThrough]
        private TestCaseRepository()
        {
            managers = new TestRepositoryManagerCollection();
        }

        private static TestCaseRepository s_instance;
        public static TestCaseRepository Instance
        {
            [DebuggerStepThrough]
            get
            {
                lock (s_sync)
                {
                    if (s_instance == null)
                    {
                        s_instance = new TestCaseRepository();
                    }
                    return s_instance;
                }
            }
        }

        #region IDisposable
        private bool _disposed = false;

        ~TestCaseRepository()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);

            // Use SupressFinalize in case a subclass 
            // of this type implements a finalizer.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (managers is IDisposable)
                    {
                        (managers as IDisposable).Dispose();
                    }
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
        }
        #endregion
    }
}
