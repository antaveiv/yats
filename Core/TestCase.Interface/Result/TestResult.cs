/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;

namespace yats.TestCase.Interface
{
	public class TestResult: ITestResult
	{
		protected ResultEnum result;

        [DebuggerStepThrough]
		public TestResult (ResultEnum res)
		{
			result = res;
		}

        /// <summary>
        /// Initializes TestResult to PASS if ok, FAIL otherwise
        /// </summary>
        /// <param name="ok"></param>
        [DebuggerStepThrough]
        public TestResult(bool ok)
        {
            if(ok)
            {
                result = ResultEnum.PASS;
            }
            else
            {
                result = ResultEnum.FAIL;
            }
        }

        [DebuggerStepThrough]
		public TestResult ()
		{
			result = ResultEnum.INCONCLUSIVE;
		}

        public override ITestResult Clone()
        {
            return new TestResult(this.result);
        }

		#region ITestResult implementation
		public override ResultEnum Result {
            [DebuggerStepThrough]
			get 
            {
				return result;
			}
            [DebuggerStepThrough]
            set
            {
                result = value;
            }
		}
		#endregion
		
		public override string ToString ()
		{
			return string.Format ("{0}", Result);
		}
    }
}

