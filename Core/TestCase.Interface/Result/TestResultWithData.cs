﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;

namespace yats.TestCase.Interface
{
    public class TestResultWithData : TestResult
    {
        protected object data;
        public object Data
        {
            get { return data; }
            set { data = value; }
        }

        [DebuggerStepThrough]
        public TestResultWithData(ResultEnum res):base(res)
        {
            result = res;
        }

        [DebuggerStepThrough]
        public TestResultWithData(ResultEnum res, object data):base(res)
        {
            result = res;
            this.data = data;
        }

        /// <summary>
        /// Initializes TestResult to PASS if ok, FAIL otherwise
        /// </summary>
        /// <param name="ok"></param>
        [DebuggerStepThrough]
        public TestResultWithData(bool ok) : base(ok)
        {
        }

        [DebuggerStepThrough]
        public TestResultWithData(bool ok, object data) : base(ok)
        {
            this.data = data;
        }

        [DebuggerStepThrough]
        public TestResultWithData()
        {
            result = ResultEnum.INCONCLUSIVE;
        }

        public override ITestResult Clone()
        {
            return new TestResultWithData( this.result, this.data );
        }
                
        public override string ToString()
        {
            return string.Format( "{0}", Result );
        }
    }
}
