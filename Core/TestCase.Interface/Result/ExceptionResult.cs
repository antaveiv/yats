/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Xml.Serialization;

namespace yats.TestCase.Interface
{
	public class ExceptionResult : ITestResult
	{
        protected Exception ex;
		protected ResultEnum result;
		protected string exceptionInfo;

        public ExceptionResult(ResultEnum result, Exception exception)
        {
            this.result = result;
            this.exceptionInfo = exception.ToString();
            this.ex = exception;
        }

		public ExceptionResult (Exception exception) : this(ResultEnum.INCONCLUSIVE, exception)
		{			
		}	
		
		public ExceptionResult ()
		{
			result = ResultEnum.INCONCLUSIVE;
		}

        
        public string ExceptionInfo
        {
            get { return exceptionInfo; }
            set { exceptionInfo = value; }
        }

        [XmlIgnore]
        public Exception Exception
        {
            get { return ex; }
        }

        public override ITestResult Clone()
        {
            var result = new ExceptionResult();
            result.exceptionInfo = this.exceptionInfo;
            result.ex = this.ex;
            result.result = this.result;
            return result;
        }
		
		#region ITestResult implementation
		public override ResultEnum Result {
			get 
            {
				return result;
			}
            set
            {
                result = value;
            }
		}
		#endregion
	}
}

