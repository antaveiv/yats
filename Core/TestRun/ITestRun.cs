/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.Xml.Serialization;
using yats.ExecutionQueue;
using yats.TestCase.Parameter;

namespace yats.TestRun
{
    public delegate void TestRunEventDelegate(ITestRun testRun);

    //[XmlInclude(typeof(TestRunImplementation))]
	public abstract class ITestRun
	{	
		protected TestStep testSequence;
		public TestStep TestSequence
		{
            [DebuggerStepThrough]
			get {
                if (testSequence == null)
                {
                    testSequence = new TestStepComposite();
                    TestSequence.Name = "Root";
                }
                return testSequence;
            }
            [DebuggerStepThrough]
			set {testSequence = value;}
		}

        protected IGlobalParameterCollection globalParameters;
        /// <summary>
        /// contains all global parameter managers (including TestRunParameters). Not serialized, should be stored as application settings
        /// </summary>
        
        public IGlobalParameterCollection GlobalParameters
        {
            [DebuggerStepThrough]
            get { return globalParameters; }
            [DebuggerStepThrough]
            set { globalParameters = value; }
        }

        protected string description;
        public string Description
        {
            [DebuggerStepThrough]
            get { return description; }
            [DebuggerStepThrough]
            set { description = value; }
        }

        protected string name;
        public string Name
        {
            [DebuggerStepThrough]
            get { return name; }
            [DebuggerStepThrough]
            set { name = value; }
        }
        
        [DebuggerStepThrough]
		public ITestRun ()
		{
		}

        protected bool isModified = false;
        public event EventHandler OnModifiedChanged;
        [XmlIgnore]
        public bool IsModified
        {
            set
            {
                if(this.isModified == value)
                {
                    return;
                }

                this.isModified = value;

                if(OnModifiedChanged != null)
                {
                    OnModifiedChanged( this, null );
                }
            }
            [DebuggerStepThrough]
            get
            {
                return isModified;
            }
        }
	}
}
