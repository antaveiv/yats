﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
using System;
using System.IO;
using System.Xml.Serialization;

namespace yats.TestRun
{
    public class TestRunLoadEventArgs : EventArgs
    {
        public ITestRun TestRun;
    }

    public class TestRunLoadHelper
    {
        public event EventHandler<TestRunLoadEventArgs> AfterLoad;

        public ITestRun LoadTestRun(byte[] xmlContents)
        {
            return LoadTestRun( new MemoryStream( xmlContents, false ) );
        }

        public ITestRun LoadTestRun(Stream stream)
        {
            XmlSerializer serializer = Utilities.SerializationHelper.GetSerializer(typeof(ITestRun));
            TextReader tw = new StreamReader( stream );
            ITestRun loadedTestRun = (ITestRun)serializer.Deserialize( tw );
            tw.Close( );

            if(loadedTestRun == null)
            {
                return null;
            }

            if(AfterLoad != null)
            {
                AfterLoad(this, new TestRunLoadEventArgs() { TestRun = loadedTestRun });
            }

            return loadedTestRun;
        }

        public ITestRun LoadTestRun(string fileName)
        {
            return LoadTestRun( new FileStream( fileName, FileMode.Open, FileAccess.Read ) );
        }
    }
}
