﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using log4net;
using yats.ExecutionQueue;
using yats.TestCase.Parameter;
using yats.TestRun;

namespace yats.ExecutionEngine
{
    // given a test step that has just been executed. Its results are assigned to parameters of other steps, if configured so
    public class AssignTestResultsToParametersVisitor : ITestStepVisitor
    {
        public static void Process(ITestRun testRun, TestStep finishedStep)
        {
            TestStepSingle singleStep = finishedStep as TestStepSingle;
            if (singleStep == null)
            {
                return;
            }

            foreach (var param in singleStep.Parameters)
            {
                if (param.IsResult == false)
                {
                    continue;
                }

                TestResultParameter resultParameter = param.Value as TestResultParameter;
                if (resultParameter == null)
                {
                    continue;
                }

                object value = null;
                try
                {
                    bool ok = singleStep.RepositoryManager.GetTestResult(singleStep.TestCase, param, out value);
                    if (!ok)
                    {
                        continue;
                    }
                }
                catch (Exception ex)
                {
                    LogManager.GetLogger("Results").Warn("Exception while getting result value:", ex);
                    continue;
                }

                //try to find any parameters 'connected' to this result in the whole test run
                AssignTestResultsToParametersVisitor visitor = new AssignTestResultsToParametersVisitor(resultParameter, value);
                testRun.TestSequence.Accept(visitor);
            }
        }

        TestResultParameter resultParameter;
        object result;
        protected AssignTestResultsToParametersVisitor(TestResultParameter resultParameter, object result)
        {
            this.resultParameter = resultParameter;
            this.result = result;
        }


        #region ITestStepVisitor Members

        public void AcceptSingle(TestStepSingle test)
        {
            foreach (var p in test.Parameters)
            {
                if (p.IsParameter == false)
                {
                    continue;
                }
                TestResultParameter paramAsTestResultListener = p.Value as TestResultParameter;
                if (paramAsTestResultListener == null || paramAsTestResultListener.ResultID != this.resultParameter.ResultID)
                {
                    continue;
                }
                test.RepositoryManager.SetParameterValue(test.TestCase, p, result);
            }
        }

        public void AcceptComposite(TestStepComposite test)
        {
            foreach (var step in test.Steps)
            {
                step.Accept(this);
            }
        }

        #endregion
    }
}
