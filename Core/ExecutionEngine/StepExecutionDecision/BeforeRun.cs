﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.ExecutionQueue;
using yats.ExecutionQueue.DecisionHandler;

namespace yats.ExecutionEngine.DecisionHandler
{
    // Initialize an execution decision handler before a step run
    public class BeforeRun : IDecisionHandlerVisitor
    {
        public BeforeRun()
        {
        }

        private static BeforeRun s_visitor = new BeforeRun();

        public static void Process(TestStep test)
        {
            test.DecisionHandler.Accept(s_visitor);
        }

        #region IDecisionHandlerVisitor implementation

        public void VisitSingleRun(SingleRun decision)
        {
            decision.Reset();
        }

        public void VisitRepeatWhilePass(RepeatWhilePass decision)
        {
            decision.Reset();
        }

        public void VisitRepeatUntilCancel(RepeatUntilCancel decision)
        {
            decision.Reset();
        }

        public void VisitRepeatIgnoreFails(RepeatIgnoreFails decision)
        {
            decision.Reset();
        }

        public void VisitRepeatUntilConfiguredResult(RepeatUntilConfiguredResult decision)
        {
            decision.Reset();
        }

        #endregion
    }
}
