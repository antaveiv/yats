/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.ExecutionQueue;
using yats.ExecutionQueue.DecisionHandler;
using yats.TestCase.Interface;

namespace yats.ExecutionEngine.DecisionHandler
{
    /// <summary>
    /// Generates step execution decision
    /// </summary>	
    public class AfterRun : IDecisionHandlerVisitor
    {
        public AfterRun()
        {
        }

        //TestStep Test;
        ITestResult result;

        public AfterRun(ITestResult result)
            : this()
        {
            if (result == null)
            {
                yats.Utilities.CrashLog.Write("public AfterRun(ITestResult result) : result is null");
            }
            //this.Test = test;
            this.result = result;
        }

        public static void Process(TestStep test, ITestResult result)
        {
            AfterRun visitor = new AfterRun(result);
            test.DecisionHandler.Accept(visitor);
        }

        #region IDecisionHandlerVisitor implementation

        public void VisitSingleRun(SingleRun decision)
        {
            decision.Executed = true;
        }

        public void VisitRepeatWhilePass(RepeatWhilePass decision)
        {
            switch (this.result.Result)
            {
                case ResultEnum.NOT_RUN:
                case ResultEnum.PASS:
                    decision.NumPassResults++;
                    break;
            }
            decision.RepetitionsDone++;
        }

        public void VisitRepeatUntilCancel(RepeatUntilCancel decision)
        {
            if (this.result.Result == ResultEnum.CANCELED)
            {
                decision.Canceled = true;
            }
        }

        public void VisitRepeatIgnoreFails(RepeatIgnoreFails decision)
        {
            if (this.result.Result == ResultEnum.CANCELED)
            {
                decision.Canceled = true;
            }
            decision.RepetitionsDone++;
        }

        public void VisitRepeatUntilConfiguredResult(RepeatUntilConfiguredResult decision)
        {
            decision.RepetitionsDone++;

            if (decision.WaitFor.Contains(this.result.Result))
            {
                decision.Finished = true;
            }
        }

        #endregion
    }
}
