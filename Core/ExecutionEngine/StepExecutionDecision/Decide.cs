/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.ExecutionQueue;
using yats.ExecutionQueue.DecisionHandler;

namespace yats.ExecutionEngine.DecisionHandler
{
    // Process the step execution decision
    public class Decide : IDecisionHandlerVisitor
    {
        protected ExecutionDecision Result;

        public Decide()
        {
        }

        public static ExecutionDecision GetDecision(TestStep test)
        {

            if (IsStepDisabledVisitor.Get(test))
            {
                return ExecutionDecision.STOP;
            }
            var d = new Decide();
            test.DecisionHandler.Accept(d);
            return d.Result;
        }

        #region IDecisionHandlerVisitor implementation
        public void VisitSingleRun(SingleRun decision)
        {
            Result = (decision.Executed) ? ExecutionDecision.STOP : ExecutionDecision.RUN;
        }

        public void VisitRepeatWhilePass(RepeatWhilePass decision)
        {
            if (decision.RepetitionsDone >= decision.Repetitions)
            {
                Result = ExecutionDecision.STOP;
                return;
            }

            if (decision.RepetitionsDone != decision.NumPassResults)
            {
                Result = ExecutionDecision.STOP;
                return;
            }

            Result = ExecutionDecision.RUN;
        }

        public void VisitRepeatUntilCancel(RepeatUntilCancel decision)
        {
            Result = (decision.Canceled) ? ExecutionDecision.STOP : ExecutionDecision.RUN;
        }

        public void VisitRepeatIgnoreFails(RepeatIgnoreFails decision)
        {
            if (decision.Canceled)
            {
                Result = ExecutionDecision.STOP;
                return;
            }

            if (decision.RepetitionsDone >= decision.Repetitions)
            {
                Result = ExecutionDecision.STOP;
                return;
            }

            Result = ExecutionDecision.RUN;
        }

        public void VisitRepeatUntilConfiguredResult(RepeatUntilConfiguredResult decision)
        {
            if (decision.RepetitionsDone >= decision.Repetitions)
            {
                Result = ExecutionDecision.STOP;
                return;
            }

            Result = (decision.Finished) ? ExecutionDecision.STOP : ExecutionDecision.RUN;
        }

        #endregion
    }
}
