﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.ExecutionQueue;
using yats.TestCase.Interface;
using yats.TestRun;
using yats.TestRun.Cancel;
using System.Collections.Generic;
using System;

namespace yats.ExecutionEngine
{
    public class TestRunStartEventArgs : EventArgs
    {
        public int TestRunId;
    }

    public class TestRunFinishEventArgs : EventArgs
    {
        public int TestRunId;
        public ITestResult Result;
    }

    public class TestStepStartingEventArgs : EventArgs
    {
        public int TestRunId;
        public TestStep Step;
        public string Path;
    }

    public class TestStepResultEventArgs : EventArgs
    {
        public int TestRunId;
        public TestStep Step;
        public string Path;
        public ITestResult RawResult;
        public ITestResult EvaluatedResult;
    }

    public class TestStepRepetitionResultEventArgs : EventArgs
    {
        public int TestRunId;
        public TestStep Step;
        public string Path;
        public ITestResult Result;
    }

    public interface ITestRunner
    {
        ITestRun TestRun
        {
            get;
            set;
        }

        void Execute(TestStep step, string path);
        void StartTestRun();

        CancelHandler CancelHandler
        {
            get;
            set;
        }

        event EventHandler<TestRunStartEventArgs> TestRunStarting;
        //TODO these functions should not go to public interface. Perhaps create a second internal interface?
        void RaiseTestRunStarting(int testRunId);

        event EventHandler<TestRunFinishEventArgs> TestRunFinished;
        void RaiseTestRunFinished(int testRunId, ITestResult result);

        event EventHandler<TestStepStartingEventArgs> TestStepStarting;
        void RaiseTestStepStarting(int testRunId, TestStep step, string path);

        event EventHandler<TestStepResultEventArgs> TestStepResult;
        void RaiseTestStepResult(int testRunId, TestStep step, string path, ITestResult rawResult, ITestResult evaluatedResult);

        event EventHandler<TestStepRepetitionResultEventArgs> TestStepRepetitionResult;
        void RaiseTestStepRepetitionResult(int testRunId, TestStep step, string path, ITestResult result);

        ITestRunner Clone();

        IEnumerable<TestStep> GetStepsToRun();
    }
}
