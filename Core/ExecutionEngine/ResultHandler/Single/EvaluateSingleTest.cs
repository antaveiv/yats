﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using yats.ExecutionQueue;
using yats.ExecutionQueue.ResultHandler;
using yats.TestCase.Interface;

namespace yats.ExecutionEngine.ResultHandler
{
    // Evaluates a step result after a single run
    internal class EvaluateSingleTest : ISingleResultVisitor
    {
        ResultEnum result;
        [DebuggerStepThrough]
        public static ResultEnum Evaluate(TestStepSingle test, ResultEnum result)
        {
            var visitor = new EvaluateSingleTest(result);
            test.SingleRunResultHandler.Accept(visitor);
            return visitor.result;
        }

        [DebuggerStepThrough]
        protected EvaluateSingleTest(ResultEnum result)
        {
            this.result = result;
        }

        #region ISingleResultVisitor implementation
        public void VisitDefaultSingleResultHandler(DefaultSingleResultHandler handler)
        {
            // does not change the actual result from test case
        }

        public void VisitCustomResultMapping(CustomResultMapping map)
        {
            this.result = map.Convert(this.result);
        }

        public void VisitIgnoreFailureSingleResultHandler(IgnoreFailureSingleResultHandler handler)
        {
            switch (this.result)
            {
                case ResultEnum.CANCELED:
                    this.result = ResultEnum.CANCELED;
                    return;
                case ResultEnum.PASS:
                    this.result = ResultEnum.PASS;
                    return;
                case ResultEnum.FAIL:
                    this.result = ResultEnum.PASS;
                    return;
                case ResultEnum.INCONCLUSIVE:
                    this.result = ResultEnum.PASS;
                    return;
                case ResultEnum.NOT_RUN:
                    this.result = ResultEnum.NOT_RUN;
                    return;
                default:
                    throw new Exception("Invalid input");
            }

        }
        #endregion
    }
}
