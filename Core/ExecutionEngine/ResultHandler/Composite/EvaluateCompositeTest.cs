﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
using System;
using System.Collections.Generic;
using System.Text;
using yats.TestCase.Interface;
using yats.ExecutionQueue.ResultHandler;
using yats.ExecutionQueue;

namespace yats.ExecutionEngine.ResultHandler
{
    internal class EvaluateCompositeTest : ICompositeResultVisitor
    {
        TestStepComposite test;
        ResultEnum result;
        public static ResultEnum Evaluate(TestStepComposite test)
        {
            if (test.Result != null && test.Result.Result == ResultEnum.NOT_RUN)
            {
                //the whole composite test step was not run - don't even look at child steps
                return test.Result.Result;
            }

            var visitor = new EvaluateCompositeTest(test);
            test.CompositeResultHandler.Accept(visitor);
            return visitor.result;
        }

        protected EvaluateCompositeTest(TestStepComposite test)
        {
            this.test = test;
        }

        #region ICompositeResultVisitor implementation
        public void VisitDefaultCompositeResultHandler(DefaultCompositeResultHandler handler)
        {
            this.result = ResultEnum.PASS;
            foreach (var t in test.Steps)
            {
                if (t.Result != null && t.Result.Result == ResultEnum.CANCELED)
                {
                    this.result = t.Result.Result;
                    return;
                }
            }
        }

        public void VisitWorstStep(WorstCaseCompositeResultHandler handler)
        {
            //just reusing the Update logic
            WorstCaseRepetitionResultHandler res = new WorstCaseRepetitionResultHandler();
            res.Update(new TestResult(ResultEnum.PASS));
            res.Update(test.Result);//TODO: why?

            foreach (var t in test.Steps)
            {
                res.Update(t.Result);
            }

            if (res.Current != null)
            {
                this.result = res.Current.Result;
            }
        }

        public void VisitSelectedSteps(SelectedStepResultHandler handler)
        {
            // similar to VisitWorstStep 
            WorstCaseRepetitionResultHandler res = new WorstCaseRepetitionResultHandler();
            res.Update(new TestResult(ResultEnum.PASS));
            res.Update(test.Result);//TODO: why?

            foreach (var t in test.Steps)
            {
                if (handler.StepIdsToEvaluate.Contains(t.Guid)) // only looking at selected steps
                {
                    res.Update(t.Result);
                }
            }

            if (res.Current != null)
            {
                this.result = res.Current.Result;
            }
        }

        public void VisitStateMachine(StateMachineResultHandler handler)
        {
            if (test.Result == null)
            {
                yats.Utilities.CrashLog.Write("Step result is null after state machine execution");
                test.Result = new TestResult(ResultEnum.INCONCLUSIVE);
            }
            this.result = test.Result.Result;
        }

        #endregion
    }
}
