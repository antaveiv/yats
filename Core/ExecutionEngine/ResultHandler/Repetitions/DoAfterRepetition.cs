/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
using System;
using yats.TestCase.Interface;
using yats.ExecutionQueue.ResultHandler;
using yats.ExecutionQueue;
using System.Diagnostics;

namespace yats.ExecutionEngine.ResultHandler
{
    /// <summary>
    /// Calls the configured repetition handler after a step is executed
    /// </summary>
    public class DoAfterRepetition : IRepetitionResultVisitor
    {
        ITestResult result;

        [DebuggerStepThrough]
        protected DoAfterRepetition(ITestResult result)
        {
            this.result = result;
        }

        [DebuggerStepThrough]
        public static void Process(TestStep test, ITestResult result)
        {
            test.RepetitionResultHandler.Accept(new DoAfterRepetition(result));
        }

        #region IRepetitionResultVisitor implementation
        [DebuggerStepThrough]
        public void VisitDefaultRepetitionResult(DefaultRepetitionResultHandler handler)
        {
            handler.Update(result.Result);
        }

        [DebuggerStepThrough]
        public void VisitWorstCaseRepetition(WorstCaseRepetitionResultHandler handler)
        {
            handler.Update(result);
        }

        [DebuggerStepThrough]
        public void VisitBestCaseRepetition(BestCaseRepetitionResultHandler handler)
        {
            handler.Update(result);
        }
        #endregion
    }
}
