using yats.ExecutionQueue;
/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
using yats.ExecutionQueue.ResultHandler;

namespace yats.ExecutionEngine.ResultHandler
{
    // Initializes a repetition result handler before any test repetitions are done
    public class DoBeforeRepetitions : IRepetitionResultVisitor
    {
        protected DoBeforeRepetitions()
        {
        }

        protected static DoBeforeRepetitions instance = new DoBeforeRepetitions();
        public static void Process(TestStep test)
        {
            test.RepetitionResultHandler.Accept(instance);
        }

        #region IRepetitionResultVisitor implementation
        public void VisitDefaultRepetitionResult(DefaultRepetitionResultHandler handler)
        {
            handler.result = yats.TestCase.Interface.ResultEnum.NOT_RUN;
        }

        public void VisitWorstCaseRepetition(WorstCaseRepetitionResultHandler handler)
        {
            handler.Current = null;
        }

        public void VisitBestCaseRepetition(BestCaseRepetitionResultHandler handler)
        {
            handler.Current = null;
        }

        #endregion
    }
}
