/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
using System;
using yats.TestCase.Interface;
using yats.ExecutionQueue.ResultHandler;
using yats.ExecutionQueue;
using System.Diagnostics;

namespace yats.ExecutionEngine.ResultHandler
{
    /// <summary>
    /// Assigns a 'repetition' result to a step
    /// </summary>	
    public class DoAfterRepetitions : IRepetitionResultVisitor
    {
        [DebuggerStepThrough]
        protected DoAfterRepetitions()
        {
        }

        ITestResult result;

        [DebuggerStepThrough]
        public static ITestResult Process(TestStep test)
        {
            DoAfterRepetitions visitor = new DoAfterRepetitions();
            test.RepetitionResultHandler.Accept(visitor);
            return visitor.result;
        }

        #region IRepetitionResultVisitor implementation
        public void VisitDefaultRepetitionResult(DefaultRepetitionResultHandler handler)
        {
            this.result = new TestResult(handler.result);
        }

        public void VisitWorstCaseRepetition(WorstCaseRepetitionResultHandler handler)
        {
            this.result = handler.Current;
        }

        public void VisitBestCaseRepetition(BestCaseRepetitionResultHandler handler)
        {
            this.result = handler.Current;
        }
        #endregion
    }
}
