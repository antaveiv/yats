﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Text;
using yats.TestCase.Interface;
using yats.ExecutionQueue;

namespace yats.ExecutionEngine.ResultHandler
{
    /// <summary>
    /// Generates execution decision for a step according to the assigned strategy
    /// </summary>
    public class EvaluateTestStepRun : ITestStepVisitor
    {
        ResultEnum result;
        public static ResultEnum Evaluate(TestStep test, ResultEnum result)
        {
            var visitor = new EvaluateTestStepRun(result);
            test.Accept(visitor);
            return visitor.result;
        }

        protected EvaluateTestStepRun(ResultEnum result)
        {
            this.result = result;
        }

        #region ITestStepVisitor Members

        public void AcceptSingle(TestStepSingle test)
        {
            this.result = EvaluateSingleTest.Evaluate(test, result);
        }

        public void AcceptComposite(TestStepComposite test)
        {
            this.result = EvaluateCompositeTest.Evaluate(test);
        }

        #endregion
    }
}
