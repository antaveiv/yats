/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
using System;
using yats.TestCase.Interface;
using yats.ExecutionQueue.CompositeStepExecuteDecision;
using yats.ExecutionQueue;

namespace yats.ExecutionEngine.CompositeStepExecuteDecision
{
    /// <summary>
    /// Generates execution decision for composite steps according to the assigned strategy
    /// </summary>
    public class Decide : ICompositeDecisionVisitor
    {
        protected ExecutionDecision Result;

        public Decide()
        {
        }

        public Decide(ITestResult stepResult)
        {
            this.stepResult = stepResult;
        }

        ITestResult stepResult;

        public static ExecutionDecision GetDecision(TestStepComposite test, TestStep stepExecuted, ITestResult result)
        {
            var d = new Decide(result);
            test.StepDecisionHandler.Accept(d);
            return d.Result;
        }

        #region ICompositeDecisionVisitor implementation
        public void VisitExecuteUntilCanceled(ExecuteUntilCanceled decision)
        {
            Result = (this.stepResult.Result == ResultEnum.CANCELED) ? ExecutionDecision.STOP : ExecutionDecision.RUN;
        }

        public void VisitExecuteWhilePassing(ExecuteWhilePassing decision)
        {
            switch (stepResult.Result)
            {
                case ResultEnum.CANCELED:
                case ResultEnum.FAIL:
                case ResultEnum.INCONCLUSIVE:
                    Result = ExecutionDecision.STOP;
                    return;
                default:
                    Result = ExecutionDecision.RUN;
                    return;
            }
        }
        #endregion
    }
}
