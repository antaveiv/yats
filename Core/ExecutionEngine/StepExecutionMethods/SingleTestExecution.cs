/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using yats.ExecutionEngine.ResultHandler;
using yats.ExecutionQueue;
using yats.TestCase.Interface;

namespace yats.ExecutionEngine
{
    // Executes a test case
    public class SingleTestExecution : IStepExecutionVisitor
    {
        TestStepSingle test;
        ITestRunner testRunner;
        string path;
        int testRunId;

        ITestResult rawResult;
        public ITestResult Result
        {
            [DebuggerStepThrough]
            get { return rawResult; }
            [DebuggerStepThrough]
            set { rawResult = value; }
        }

        [DebuggerStepThrough]
        public SingleTestExecution(TestStepSingle test, ITestRunner testRunner, int testRunId, string path)
        {
            this.test = test;
            this.testRunner = testRunner;
            this.path = path;
            this.testRunId = testRunId;
        }

        void Execute()
        {
            testRunner.RaiseTestStepStarting(testRunId, test, path);//EVENT - starting test case run
            try
            {
                var result = test.RepositoryManager.Execute(test.TestCase, test.Parameters, testRunner.TestRun.GlobalParameters, testRunner, path);
                if (result == null)
                {
                    result = new ExceptionResult(new Exception("Test execution returned null: " + test.UniqueTestId));
                }
                test.Result = result;
            }
            catch (Exception ex)
            {
                //log4net.LogManager.GetLogger("Exception").Error("Exception during step execution", ex);
                test.Result = new ExceptionResult(ex);
            }

            var rawResult = test.Result.Clone();

            // run ResultHandler
            test.Result.Result = EvaluateSingleTest.Evaluate(test, test.Result.Result);
            testRunner.RaiseTestStepResult(testRunId, test, path, rawResult, test.Result);
        }

        #region IStepExecutionVisitor implementation

        [DebuggerStepThrough]
        public void VisitParallelExecutionModifier(ParallelExecutionModifier modifier)
        {
            Execute();
        }

        [DebuggerStepThrough]
        public void VisitSequentialExecutionModifier(SequentialExecutionModifier modifier)
        {
            Execute();
        }

        [DebuggerStepThrough]
        public void VisitRandomOrderModifier(RandomOrderModifier modifier)
        {
            Execute();
        }

        public void VisitStepDisabledModifier(StepDisabledModifier modifier)
        {
            //almost like Execute, except for the test result is created without calling the test case

            //testRunner.RaiseTestStepStarting(testRunId, test, path);//EVENT - starting test case run

            rawResult = new TestResult(ResultEnum.NOT_RUN);

            test.Result = rawResult.Clone();

            // run ResultHandler
            test.Result.Result = EvaluateSingleTest.Evaluate(test, test.Result.Result);

            //testRunner.RaiseTestStepResult(testRunId, test, path, rawResult, test.Result );//EVENT - result after conversion
        }

        public void VisitStateMachine(StateMachineExecutionModifier decision)
        {
            //State machine method should not be assigned to Single steps
            throw new NotImplementedException();
        }
        #endregion


        public void VisitOneRandomStepExecutionModifier(OneRandomStepExecutionModifier modifier)
        {
            Execute();
        }
    }
}
