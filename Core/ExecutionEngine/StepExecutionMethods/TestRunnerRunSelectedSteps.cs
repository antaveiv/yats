﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using yats.ExecutionQueue;
using yats.TestCase.Interface;
using yats.TestRun;

namespace yats.ExecutionEngine
{
    /// <summary>
    /// Executes only the given "selected" steps of a test run
    /// </summary>
    public class TestRunnerRunSelectedSteps : TestRunner
    {
        private List<TestStep> m_stepList;

        public TestRunnerRunSelectedSteps(ITestRun testRun, IList<TestStep> steps)
            : base(testRun)
        {
            this.m_stepList = TestStepHierarchyFilter.Filter(steps);
        }

        public override IEnumerable<TestStep> GetStepsToRun()
        {
            return m_stepList;
        }

        public override void StartTestRun()
        {
            if (Utilities.Util.IsNullOrEmpty(m_stepList))
            {
                return; // no selected steps
            }
            CancelHandler.Reset();
            System.Diagnostics.Debug.Assert(testRun != null);
            testRunId = TestRunCounter;
            TestRunCounter++;
            RaiseTestRunStarting(testRunId);

            //generate dummy Root element
            RaiseTestStepStarting(testRunId, new TestStepComposite(), "0.0");
            int stepIndex = 0;

            if (testRun.TestSequence != null)
            {
                testRun.TestSequence.Accept(new ClearCurrentResultsVisitor());
                foreach (var step in m_stepList)
                {
                    //step.Accept( new ClearCurrentResultsVisitor( ) );
                    Accept(step, "0.0|" + stepIndex.ToString());
                    stepIndex++;
                }

                //close dummy Root element
                RaiseTestStepResult(testRunId, new TestStepComposite(), "0.0", new TestResult(true), new TestResult(true));


                RaiseTestRunFinished(testRunId, testRun.TestSequence.Result);
            }
            else
            {
                RaiseTestRunFinished(testRunId, new TestResultWithData(ResultEnum.INCONCLUSIVE, "testRun.TestSequence == null"));
            }
        }

        public override ITestRunner Clone()
        {
            TestRunnerRunSelectedSteps result = new TestRunnerRunSelectedSteps(this.testRun, this.m_stepList);
            return result;
        }
    }
}
