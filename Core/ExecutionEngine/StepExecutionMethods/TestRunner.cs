/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using yats.ExecutionEngine.DecisionHandler;
using yats.ExecutionEngine.ResultHandler;
using yats.ExecutionQueue;
using yats.TestCase.Interface;
using yats.TestRun;
using yats.TestRun.Cancel;

namespace yats.ExecutionEngine
{
    // Executes a test run. Generates various ITestRunner events during the test run
    public class TestRunner : ITestRunner
    {
        [DebuggerStepThrough]
        public TestRunner()
        {
            this.CancelHandler = new CancelHandler();
        }

        [DebuggerStepThrough]
        public TestRunner(ITestRun testRun)
            : this()
        {
            this.TestRun = testRun;
        }

        #region ITestStepVisitor implementation
        // currently there is no difference how single or composite tests are executed:
        // BeforeRun is called to (re)initialize remaining repetitions 
        // Next, ExecutionMethod and AfterRun are called to execute and update decision
        // Finally, Decision controls if the test should be repeated or not

        public virtual void AcceptSingle(TestStepSingle test, string path)
        {
            DoBeforeRepetitions.Process(test);
            int numRepetitions = 0;
            try
            {
                BeforeRun.Process(test);
                while (CancelHandler.IsCanceled == false)
                {
                    var decision = Decide.GetDecision(test);
                    switch (decision)
                    {
                        case ExecutionDecision.STOP:
                            if (test.Result == null)
                            {
                                test.Result = new TestResult(ResultEnum.NOT_RUN);
                            }
                            return;
                        case ExecutionDecision.RUN:
                            var visitor = new SingleTestExecution(test, this, testRunId, path + "." + numRepetitions);
                            test.ExecutionMethod.Accept(visitor);
                            AfterRun.Process(test, test.Result);
                            DoAfterRepetition.Process(test, test.Result);
                            numRepetitions++;
                            break;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (numRepetitions > 1)
                {
                    test.Result = DoAfterRepetitions.Process(test);
                    RaiseTestStepRepetitionResult(testRunId, test, path + "." + (numRepetitions - 1), test.Result);
                }
            }
        }

        public virtual void AcceptComposite(TestStepComposite test, string path)
        {
            BeforeRun.Process(test);
            DoBeforeRepetitions.Process(test);
            int numRepetitions = 0;
            while (CancelHandler.IsCanceled == false)
            {
                var decision = Decide.GetDecision(test);
                switch (decision)
                {
                    case ExecutionDecision.STOP:
                        if (numRepetitions > 1)
                        {
                            test.Result = DoAfterRepetitions.Process(test);
                            RaiseTestStepRepetitionResult(testRunId, test, path + "." + (numRepetitions - 1), test.Result);
                        }
                        if (test.Result == null) // Stop is returned when the step is disabled. If Result is null, would crash somewhere else
                        {
                            test.Result = new TestResult(ResultEnum.NOT_RUN);
                        }
                        return;
                    case ExecutionDecision.RUN:
                        test.Accept(new ClearCurrentResultsVisitor());

                        RaiseTestStepStarting(testRunId, test, path + "." + numRepetitions);//EVENT

                        CompositeTestExecution.Execute(test, this, testRunId, path + "." + numRepetitions);
                        //test.ExecutionMethod.Accept( new CompositeTestExecution( test, this ) );

                        if (test.Result == null)
                        {
                            test.Result = new TestResult(EvaluateCompositeTest.Evaluate(test));
                        }
                        else
                        {
                            test.Result.Result = EvaluateCompositeTest.Evaluate(test);
                        }
                        RaiseTestStepResult(testRunId, test, path + "." + numRepetitions, test.Result, test.Result);//since a composite test step is not really a test case, the 'unevaluated' result is same as evaluated 

                        AfterRun.Process(test, test.Result);
                        DoAfterRepetition.Process(test, test.Result);
                        numRepetitions++;
                        break;
                }
            }
        }

        #endregion

        #region ITestRunner Members

        protected ITestRun testRun;
        public ITestRun TestRun
        {
            [DebuggerStepThrough]
            get { return testRun; }
            [DebuggerStepThrough]
            set { testRun = value; }
        }

        [DebuggerStepThrough]
        public void Execute(TestStep step, string path)
        {
            System.Diagnostics.Debug.Assert(testRun != null);
            Accept(step, path);
        }

        /// <summary>
        /// Incremented on each test run start. Is used to distinguish between different test runs in e.g. logging events
        /// </summary>
        protected static int TestRunCounter = 1;
        protected int testRunId = 0;

        [DebuggerStepThrough]
        protected virtual void Accept(TestStep step, string path)
        {
            TestStepSingle single = step as TestStepSingle;
            if (single != null)
            {
                //changed from visitor to method because of the need to pass the path
                AcceptSingle(single, path);
                return;
            }
            TestStepComposite composite = step as TestStepComposite;
            if (composite != null)
            {
                AcceptComposite(composite, path);
                return;
            }
        }

        public virtual IEnumerable<TestStep> GetStepsToRun()
        {
            yield return testRun.TestSequence;
        }

        public virtual void StartTestRun()
        {
            CancelHandler.Reset();
            System.Diagnostics.Debug.Assert(testRun != null);
            testRunId = TestRunCounter;
            TestRunCounter++;
            RaiseTestRunStarting(testRunId);
            if (testRun.TestSequence != null)
            {
                testRun.TestSequence.Accept(new ClearCurrentResultsVisitor());
                Accept(testRun.TestSequence, "0");
                RaiseTestRunFinished(testRunId, testRun.TestSequence.Result);
            }
            else
            {
                RaiseTestRunFinished(testRunId, new TestResult(ResultEnum.INCONCLUSIVE));
            }
        }

        public event EventHandler<TestRunStartEventArgs> TestRunStarting;
        [DebuggerStepThrough]
        public virtual void RaiseTestRunStarting(int testRunId)
        {
            var copy = TestRunStarting;
            if (copy != null)
            {
                copy(this, new TestRunStartEventArgs() { TestRunId = testRunId });
            }
        }

        public event EventHandler<TestRunFinishEventArgs> TestRunFinished;
        [DebuggerStepThrough]
        public void RaiseTestRunFinished(int testRunId, ITestResult result)
        {
            var copy = TestRunFinished;
            if (copy != null)
            {
                copy(this, new TestRunFinishEventArgs() { TestRunId = testRunId, Result = result });
            }
        }

        public event EventHandler<TestStepStartingEventArgs> TestStepStarting;
        [DebuggerStepThrough]
        public void RaiseTestStepStarting(int testRunId, TestStep step, string path)
        {
            var copy = TestStepStarting;
            if (copy != null)
            {
                copy(this, new TestStepStartingEventArgs() { TestRunId = testRunId, Step = step, Path = path });
            }

            lock (CurrentlyRunningSteps)
            {
                CurrentlyRunningSteps.Add(step);
            }
        }

        public event EventHandler<TestStepResultEventArgs> TestStepResult;
        [DebuggerStepThrough]
        public void RaiseTestStepResult(int testRunId, TestStep step, string path, ITestResult rawResult, ITestResult evaluatedResult)
        {
            lock (CurrentlyRunningSteps)
            {
                CurrentlyRunningSteps.Remove(step);
            }
            var copy = TestStepResult;
            if (copy != null)
            {
                copy(this, new TestStepResultEventArgs() { TestRunId = testRunId, Step = step, Path = path, RawResult = rawResult, EvaluatedResult = evaluatedResult });
            }
        }

        public event EventHandler<TestStepRepetitionResultEventArgs> TestStepRepetitionResult;
        [DebuggerStepThrough]
        public virtual void RaiseTestStepRepetitionResult(int testRunId, TestStep step, string path, ITestResult repetitionResult)
        {
            var copy = TestStepRepetitionResult;
            if (copy != null)
            {
                copy(this, new TestStepRepetitionResultEventArgs() { TestRunId = testRunId, Step = step, Path = path, Result = repetitionResult });
            }
        }


        internal HashSet<TestStep> CurrentlyRunningSteps = new HashSet<TestStep>();

        protected CancelHandler cancelHandler;
        public CancelHandler CancelHandler
        {
            [DebuggerStepThrough]
            get { return cancelHandler; }
            [DebuggerStepThrough]
            set
            {
                if (cancelHandler != null && cancelHandler != value)
                {
                    CancelHandlerCollection.Remove(cancelHandler);
                }
                cancelHandler = value;
                cancelHandler.OnCancel += Cancel;
            }
        }

        public void Cancel(object sender, CancelEventArgs e)
        {
            var visitor = new CancelStepVisitor();

            lock (CurrentlyRunningSteps)
            {
                foreach (var step in CurrentlyRunningSteps)
                {
                    step.Accept(visitor);
                }
            }
        }

        // Cancels a running test case, if the test case supports it 
        private class CancelStepVisitor : ITestStepVisitor
        {
            #region ITestStepVisitor Members

            public void AcceptSingle(TestStepSingle test)
            {
                test.RepositoryManager.Cancel(test.TestCase);
            }

            public void AcceptComposite(TestStepComposite test)
            {
            }

            #endregion
        }

        public virtual ITestRunner Clone()
        {
            TestRunner result = new TestRunner(this.testRun);
            return result;
        }
        #endregion

        //USE FROM UNIT TESTS ONLY
        public void SetTestRun(ITestRun testRun)
        {
            System.Diagnostics.Debug.Assert(this.testRun == null);
            System.Diagnostics.Debug.Assert(testRun != null);
            this.testRun = testRun;
        }
    }
}
