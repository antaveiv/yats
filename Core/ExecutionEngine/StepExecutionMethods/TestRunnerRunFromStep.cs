﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.ExecutionQueue;
using yats.TestCase.Interface;
using yats.TestRun;
using System.Collections.Generic;

namespace yats.ExecutionEngine
{
    /// <summary>
    /// Executes test run from a selected start step
    /// </summary>
    public class TestRunnerRunFromStep : TestRunner
    {
        private TestStep m_startStep;
        private bool m_hasStartedRunning = false;
        private int m_stepIndex = 0;
        public TestRunnerRunFromStep(ITestRun testRun, TestStep startStep)
            : base(testRun)
        {
            this.m_startStep = startStep;
        }

        public override IEnumerable<TestStep> GetStepsToRun()
        {
            List<TestStep> result = new List<TestStep>();
            bool startFound = false;
            TestHierarchyEnumerator.OnAny(testRun.TestSequence, (TestStep step) =>
            {
                if (step == m_startStep)
                {
                    startFound = true;
                }
                if (startFound)
                {
                    result.Add(step);
                }
            });
            return result;
        }

        protected void SkipUntilSelectedSingle(TestStepSingle test, string path)
        {
            if (test == m_startStep)
            {
                m_hasStartedRunning = true;
            }
            if (m_hasStartedRunning)
            {
                base.Accept(test, path + "|" + m_stepIndex.ToString());
                m_stepIndex++;
                return;
            }
        }

        protected void SkipUntilSelectedComposite(TestStepComposite test, string path)
        {
            if (test == m_startStep)
            {
                m_hasStartedRunning = true;
            }
            if (m_hasStartedRunning)
            {
                base.Accept(test, path + "|" + m_stepIndex.ToString());
                m_stepIndex++;
                return;
            }
            foreach (var step in test.Steps)
            {
                SkipUntilSelected(step, path);
            }
        }

        protected void SkipUntilSelected(TestStep step, string path)
        {
            TestStepSingle single = step as TestStepSingle;
            if (single != null)
            {
                //changed from visitor to method because of the need to pass the path
                SkipUntilSelectedSingle(single, path);
                return;
            }
            TestStepComposite composite = step as TestStepComposite;
            if (composite != null)
            {
                SkipUntilSelectedComposite(composite, path);
                return;
            }
        }

        public override void StartTestRun()
        {
            if (this.m_startStep == null || testRun.TestSequence == null)
            {
                return;
            }

            m_hasStartedRunning = false; // reinitialize - needed for "Repeat test" button
            m_stepIndex = 0;

            CancelHandler.Reset();
            System.Diagnostics.Debug.Assert(testRun != null);
            testRunId = TestRunCounter;
            TestRunCounter++;
            RaiseTestRunStarting(testRunId);

            if (object.ReferenceEquals(m_startStep, testRun.TestSequence) == false)
            {
                // not starting from root - generate dummy Root element
                RaiseTestStepStarting(testRunId, new TestStepComposite(), "0.0");
            }

            testRun.TestSequence.Accept(new ClearCurrentResultsVisitor());
            SkipUntilSelected(testRun.TestSequence, "0.0");

            if (object.ReferenceEquals(m_startStep, testRun.TestSequence) == false)
            {
                // not starting from root - close dummy Root element
                RaiseTestStepResult(testRunId, new TestStepComposite(), "0.0", new TestResult(true), new TestResult(true));
            }

            RaiseTestRunFinished(testRunId, m_startStep.Result);

        }

        public override ITestRunner Clone()
        {
            TestRunnerRunFromStep result = new TestRunnerRunFromStep(this.testRun, this.m_startStep);
            return result;
        }
    }
}
