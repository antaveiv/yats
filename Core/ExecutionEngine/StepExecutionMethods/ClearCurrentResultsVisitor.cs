﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.ExecutionQueue;

namespace yats.ExecutionEngine
{
    // Sets all step results to null
    public class ClearCurrentResultsVisitor : ITestStepVisitor
    {
        #region ITestStepVisitor Members

        public void AcceptSingle(TestStepSingle test)
        {
            test.Result = null;
        }

        public void AcceptComposite(TestStepComposite test)
        {
            test.Result = null;
            foreach (var step in test.Steps)
            {
                step.Accept(this);
            }
        }

        #endregion
    }
}
