/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using yats.ExecutionEngine.CompositeStepExecuteDecision;
using yats.ExecutionEngine.ResultHandler;
using yats.ExecutionQueue;
using yats.TestCase.Interface;
using yats.Utilities;

namespace yats.ExecutionEngine
{
    // Executes a composite test step according to its execution strategy (sequence, parallel etc.)
    public class CompositeTestExecution : IStepExecutionVisitor
    {
        static Random random = new Random();
        TestStepComposite test;
        ITestRunner testRunner;
        int testRunId;
        string path;

        [DebuggerStepThrough]
        public static void Execute(TestStepComposite test, ITestRunner testRunner, int testRunId, string path)
        {
            test.ExecutionMethod.Accept(new CompositeTestExecution(test, testRunner, testRunId, path));
        }

        [DebuggerStepThrough]
        protected CompositeTestExecution(TestStepComposite test, ITestRunner testRunner, int testRunId, string path)
        {
            this.test = test;
            this.testRunner = testRunner;
            this.testRunId = testRunId;
            this.path = path;
        }

        #region IStepExecutionVisitor implementation

        private class ThreadData
        {
            CompositeTestExecution parent;
            TestStep step;
            public string path;

            public ThreadData(CompositeTestExecution parent, TestStep step, string path)
            {
                this.parent = parent;
                this.step = step;
                this.path = path;
            }

            public void DoWork()
            {
                parent.testRunner.Execute(step, path);
            }
        }

        public void VisitParallelExecutionModifier(ParallelExecutionModifier modifier)
        {
            List<Thread> threads = new List<Thread>();
            int stepIndex = 0;
            foreach (var t in test.Steps)
            {
                ThreadData work = new ThreadData(this, t, path + "|" + stepIndex);
                stepIndex++;
                ThreadStart threadDelegate = new ThreadStart(work.DoWork);
                Thread newThread = new Thread(threadDelegate);
                newThread.Name = "Exec " + work.path;
                threads.Add(newThread);
                newThread.Start();
            }
            foreach (var t in threads)
            {
                t.Join();
            }
        }

        public void VisitSequentialExecutionModifier(SequentialExecutionModifier modifier)
        {
            bool running = true;
            int stepIndex = 0;
            foreach (var t in test.Steps)
            {
                if (testRunner.CancelHandler.IsCanceled)
                {
                    running = false;
                }

                if (running)
                {
                    testRunner.Execute(t, path + "|" + stepIndex);
                    if (Decide.GetDecision(test, t, t.Result) == ExecutionDecision.STOP)
                    {
                        running = false;
                    }
                }
                else
                {
                    testRunner.RaiseTestStepStarting(testRunId, t, path + "|" + stepIndex + ".0");
                    t.Result = new TestResult(ResultEnum.NOT_RUN);
                    var rawResult = t.Result.Clone();
                    // run ResultHandler
                    t.Result.Result = EvaluateTestStepRun.Evaluate(t, t.Result.Result);
                    testRunner.RaiseTestStepResult(testRunId, t, path + "|" + stepIndex + ".0", rawResult, t.Result);
                }
                stepIndex++;
            }
        }

        public void VisitOneRandomStepExecutionModifier(OneRandomStepExecutionModifier modifier)
        {
            int stepIndex = 0;
            bool running = true;
            if (testRunner.CancelHandler.IsCanceled)
            {
                running = false;
            }
            if (test.Steps.IsNullOrEmpty())
            {
                return;
            }
            var enabledTests = test.Steps.Where(x => IsStepDisabledVisitor.Get(test, x) == false);
            if (enabledTests.Count() <= 0)
            {
                return;
            }
            int index = random.Next(enabledTests.Count());
            var t = test.Steps[index];

            if (running)
            {
                testRunner.Execute(t, path + "|" + stepIndex);
                if (Decide.GetDecision(test, t, t.Result) == ExecutionDecision.STOP)
                {
                    running = false;
                }
            }
            else
            {
                testRunner.RaiseTestStepStarting(testRunId, t, path + "|" + stepIndex + ".0");
                t.Result = new TestResult(ResultEnum.NOT_RUN);
                var rawResult = t.Result.Clone();
                // run ResultHandler
                t.Result.Result = EvaluateTestStepRun.Evaluate(t, t.Result.Result);
                testRunner.RaiseTestStepResult(testRunId, t, path + "|" + stepIndex + ".0", rawResult, t.Result);
            }
        }

        public void VisitRandomOrderModifier(RandomOrderModifier modifier)
        {
            if (test.Steps == null || test.Steps.Count == 0)
            {
                return;
            }

            List<TestStep> randomOrder = new List<TestStep>(test.Steps);
            bool running = true;
            int stepIndex = 0;

            while (randomOrder.Count > 0)
            {
                if (testRunner.CancelHandler.IsCanceled)
                {
                    running = false;
                }

                int index = random.Next(randomOrder.Count);
                var t = randomOrder[index];

                if (running)
                {
                    testRunner.Execute(t, path + "|" + stepIndex);
                    if (Decide.GetDecision(test, t, t.Result) == ExecutionDecision.STOP)
                    {
                        running = false;
                    }
                }
                else
                {
                    testRunner.RaiseTestStepStarting(testRunId, t, path + "|" + stepIndex + ".0");
                    t.Result = new TestResult(ResultEnum.NOT_RUN);
                    var rawResult = t.Result.Clone();
                    // run ResultHandler
                    t.Result.Result = EvaluateTestStepRun.Evaluate(t, t.Result.Result);
                    testRunner.RaiseTestStepResult(testRunId, t, path + "|" + stepIndex + ".0", rawResult, t.Result);
                }
                stepIndex++;
                randomOrder.RemoveAt(index);
            }
        }

        public void VisitStepDisabledModifier(StepDisabledModifier modifier)
        {
            foreach (var t in test.Steps)
            {
                if (IsStepDisabledVisitor.Get(t) == false)
                {
                    //TODO: find a better way to do this
                    var tmp = t.ExecutionMethod;
                    t.ExecutionMethod = new StepDisabledModifier();
                    testRunner.Execute(t, path);
                    t.ExecutionMethod = tmp; //restore
                    //throw new Exception( "child step not disabled in disabled composite" );
                }
                else
                {
                    testRunner.Execute(t, path);
                }
            }

            test.Result = new TestResult(ResultEnum.NOT_RUN);
        }

        public void VisitStateMachine(StateMachineExecutionModifier decision)
        {
            StateMachineExecutionModifier.State currentState = decision.States.First(x => x.IsStartState);
            bool running = true;
            int stepIndex = 0;
            var random = new Random();

            while (currentState.IsFinishState == false)
            {
                // reject Disabled steps
                var executableSteps = currentState.Steps.Where(x => IsStepDisabledVisitor.Get(test, test.Steps.First(step => step.Guid == x.StepGuid)) == false).ToList();
                int totalProbability = executableSteps.Sum(x => x.Probability);
                int p = random.Next(totalProbability);
                StateMachineExecutionModifier.TestCaseResultMap stepToRun = null;
                int sum = 0;
                for (int i = 0; i < executableSteps.Count; i++)
                {
                    stepToRun = executableSteps[i];
                    if (p >= sum && p < sum + executableSteps[i].Probability && stepToRun.Probability > 0)
                    {
                        break;
                    }
                    sum += executableSteps[i].Probability;
                }

                if (testRunner.CancelHandler.IsCanceled)
                {
                    running = false;
                }

                var toExecute = test.Steps.First(x => x.Guid == stepToRun.StepGuid);

                if (running)
                {
                    Console.WriteLine("Current state: " + currentState.Name + " running " + toExecute.Name);//TODO: log4net
                    testRunner.Execute(toExecute, path + "|" + stepIndex);
                    if (stepToRun.ResultStateMap.ContainsKey(toExecute.Result.Result))
                    {
                        currentState = decision.States.First(x => x.StateGUID == stepToRun.ResultStateMap[toExecute.Result.Result]);
                        Console.WriteLine("Next state: " + currentState.Name);
                    }
                }
                else
                {
                    Console.WriteLine("Cancelled");
                    testRunner.RaiseTestStepStarting(testRunId, toExecute, path + "|" + stepIndex + ".0");
                    toExecute.Result = new TestResult(ResultEnum.NOT_RUN);
                    var rawResult = toExecute.Result.Clone();
                    // run ResultHandler
                    toExecute.Result.Result = EvaluateTestStepRun.Evaluate(toExecute, toExecute.Result.Result);
                    testRunner.RaiseTestStepResult(testRunId, toExecute, path + "|" + stepIndex + ".0", rawResult, toExecute.Result);
                    break;
                }
                stepIndex++;
            }

            if (running)
            {
                test.Result = new TestResult(currentState.ResultOnFinish);
            }
            else
            {
                test.Result = new TestResult(ResultEnum.CANCELED);
            }

            // evaluation may fall apart - state machine may not run all the steps to reach finish
            foreach (var step in test.Steps)
            {
                if (step.Result == null)
                {
                    step.Result = new TestResult(ResultEnum.NOT_RUN);
                }
            }
        }

        #endregion
    }
}
