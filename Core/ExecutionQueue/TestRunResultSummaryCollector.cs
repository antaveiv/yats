﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Threading;
using yats.TestCase.Interface;

namespace yats.ExecutionQueue
{
    /// <summary>
    /// Collects test step results during a test run.
    /// </summary>
    public class TestRunResultSummaryCollector
    {
        private Dictionary<TestStep, StepResultCounters> m_stepCounterDictionary = new Dictionary<TestStep, StepResultCounters>();
        private ReaderWriterLock m_lock = new ReaderWriterLock();
        protected const int LOCK_TIMEOUT = 10000;

        public void Reset()
        {
            m_lock.AcquireWriterLock(LOCK_TIMEOUT);
            m_stepCounterDictionary = new Dictionary<TestStep, StepResultCounters>();
            m_lock.ReleaseWriterLock();
        }

        private class StepResultCounters
        {
            public int NumNotRun;
            public int NumPass;
            public int NumInconclusive;
            public int NumFail;
            public int NumCanceled;

            internal void Update(ResultEnum result)
            {
                switch (result)
                {
                    case ResultEnum.CANCELED:
                        NumCanceled++;
                        break;
                    case ResultEnum.FAIL:
                        NumFail++;
                        break;
                    case ResultEnum.INCONCLUSIVE:
                        NumInconclusive++;
                        break;
                    case ResultEnum.NOT_RUN:
                        NumNotRun++;
                        break;
                    case ResultEnum.PASS:
                        NumPass++;
                        break;
                }
            }

            public override string ToString()
            {
                List<string> tmp = new List<string>();
                if (NumPass > 0)
                {
                    tmp.Add(NumPass.ToString() + " pass");
                }
                if (NumFail > 0)
                {
                    tmp.Add(NumFail.ToString() + " fail");
                }
                if (NumInconclusive > 0)
                {
                    tmp.Add(NumInconclusive.ToString() + " inconclusive");
                }
                if (NumNotRun > 0)
                {
                    tmp.Add(NumNotRun.ToString() + " not run");
                }
                if (NumCanceled > 0)
                {
                    tmp.Add(NumCanceled.ToString() + " canceled");
                }

                string res = string.Join(", ", tmp.ToArray());
                return res;
            }
        }

        public void AddResult(TestStep step, ResultEnum result)
        {
            if (step == null)
            {
                return;
            }
            StepResultCounters ct = null;
            m_lock.AcquireWriterLock(LOCK_TIMEOUT);
            if (m_stepCounterDictionary.TryGetValue(step, out ct) == false)
            {
                ct = new StepResultCounters();
                m_stepCounterDictionary[step] = ct;
            }
            ct.Update(result);
            m_lock.ReleaseWriterLock();
        }

        public void AddResult(TestStep step, ITestResult result)
        {
            StepResultCounters ct = null;
            m_lock.AcquireWriterLock(LOCK_TIMEOUT);
            if (m_stepCounterDictionary.TryGetValue(step, out ct) == false)
            {
                ct = new StepResultCounters();
                m_stepCounterDictionary[step] = ct;
            }
            ct.Update(result.Result);
            m_lock.ReleaseWriterLock();
        }

        public string GetStatus(TestStep step)
        {
            StepResultCounters ct = null;
            m_lock.AcquireReaderLock(LOCK_TIMEOUT);
            if (m_stepCounterDictionary.TryGetValue(step, out ct))
            {
                m_lock.ReleaseReaderLock();
                return ct.ToString();
            }
            m_lock.ReleaseReaderLock();
            return string.Empty;
        }
    }
}
