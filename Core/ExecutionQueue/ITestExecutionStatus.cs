﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace yats.ExecutionQueue
{
    public interface ITestExecutionStatus
    {
        bool IsRunning { get; set; }
        event EventHandler<TestExecutionStatusEventArgs> OnStatusChange;
    }

    public class TestExecutionStatusEventArgs: EventArgs {
        public ITestExecutionStatus Status { get; protected set; }
        public TestExecutionStatusEventArgs(ITestExecutionStatus status)
        {
            this.Status = status;
        }
    }

    public class TestExecutionStatus : ITestExecutionStatus
    {
        protected bool isRunning = false;
        public bool IsRunning
        {
            get
            {
                return isRunning;
            }
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    if (OnStatusChange != null)
                    {
                        OnStatusChange(this, new TestExecutionStatusEventArgs(this));
                    }
                }
            }
        }
        public event EventHandler<TestExecutionStatusEventArgs> OnStatusChange;
        //System.Threading.Timer timeoutTimer;
        public TestExecutionStatus()
        {
            //timeoutTimer = new System.Threading.Timer(_ =>
            //{
            //    IsRunning = !IsRunning;
            //}, null, 5000, 5000);
        }
    }
}
