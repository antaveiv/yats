/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

namespace yats.ExecutionQueue
{
    /// IStepExecution classes and visitors contain settings and methods to execute a single or composite
    /// test step. The following execution methods are implemented:
    /// * Step disabled
    /// * Execute steps in sequence
    /// * Execute steps in parallel (multi-thread execution)
    /// * Execute steps in random order

	public abstract class IStepExecution
	{
		public abstract void Accept(IStepExecutionVisitor visitor);

        public abstract IStepExecution Clone();
    }
}

