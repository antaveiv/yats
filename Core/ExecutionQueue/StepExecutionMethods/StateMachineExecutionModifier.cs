﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using yats.TestCase.Interface;
using yats.Utilities;

namespace yats.ExecutionQueue
{
    public class StateMachineExecutionModifier : IStepExecution
    {
        public class State
        {
            public string Name { get; set; }
            public bool IsStartState { get; set; }
            public bool IsFinishState { get; set; }
            
            //Using GUIDs for serialization - can not have cyclic references :/
            Guid stateGUID = Guid.NewGuid();
            public Guid StateGUID
            {
                get
                {
                    return stateGUID;
                }
                set
                {
                    stateGUID = value;
                }
            }
            protected ResultEnum resultOnFinish = ResultEnum.PASS;
            public ResultEnum ResultOnFinish
            {
                get { return resultOnFinish; }
                set { resultOnFinish = value; }
            }

            public State()
            {
            }

            // constructor for Finish state
            public State(string name, ResultEnum resultOnFinish):this(name, false, true)
            {
                this.ResultOnFinish = resultOnFinish;
            }

            // constructor for intermediate state (not start, not finish)
            public State(string name) : this(name, false, false)
            {
            }

            public State(string name, bool isStart, bool isFinish):this()
            {
                this.Name = name;
                this.IsStartState = isStart;
                this.IsFinishState = isFinish;
                this.StateGUID = Guid.NewGuid();
            }

            List<TestCaseResultMap> steps = new List<TestCaseResultMap>();
            public List<TestCaseResultMap> Steps
            {
                get { return steps; }
                set { steps = value; }
            }

            internal State Clone()
            {
                State result = new State(this.Name, this.IsStartState, this.IsFinishState);
                result.stateGUID = new Guid(this.stateGUID.ToByteArray());
                result.steps = new List<TestCaseResultMap>(this.steps.Count);
                foreach (var step in this.steps)
                {
                    result.steps.Add(step.Clone());
                }
                return result;
            }

            public override string ToString()
            {
                return Name;
            }
        }

        public class TestCaseResultMap
        {
            private Guid m_stepGuid = Guid.NewGuid();
            public Guid StepGuid
            {
                get { return m_stepGuid; }
                set { m_stepGuid = value; }
            }

            private int m_probability = 1;
            public int Probability
            {
                get { return m_probability; }
                set { m_probability = value; }
            }

            private SerializableDictionary<ResultEnum, Guid> resultStateMap = new SerializableDictionary<ResultEnum, Guid>();
            public SerializableDictionary<ResultEnum, Guid> ResultStateMap
            {
                get { return resultStateMap; }
                set { resultStateMap = value; }
            }

            public TestCaseResultMap()
            {
            }

            public TestCaseResultMap(TestStep step):this(step.Guid)
            {
            }

            public TestCaseResultMap(Guid stepGuid):this()
            {
                this.m_stepGuid = stepGuid.Clone();;
            }

            internal TestCaseResultMap Clone()
            {
                TestCaseResultMap result = new TestCaseResultMap(this.StepGuid);
                result.m_probability = this.m_probability;
                foreach (var pair in this.resultStateMap)
                {
                    result.resultStateMap.Add(pair.Key, pair.Value.Clone());
                }
                return result;
            }
        }

        private List<State> m_stateList = new List<State>();
        public List<State> States
        {
            get { return m_stateList; }
            set { m_stateList = value; }
        }

        public StateMachineExecutionModifier()
        {
        }

        public static StateMachineExecutionModifier NewWithStartAndFinish()
        {
            StateMachineExecutionModifier result = new StateMachineExecutionModifier();
            result.m_stateList.Add(new State("Start", true, false));
            result.m_stateList.Add(new State("Finish", false, true));
            return result;
        }

        public override void Accept(IStepExecutionVisitor visitor)
        {
            visitor.VisitStateMachine(this);
        }

        public override IStepExecution Clone()
        {
            StateMachineExecutionModifier result = new StateMachineExecutionModifier();
            foreach (var state in this.m_stateList)
            {
                result.m_stateList.Add(state.Clone());
            }
            return result;
        }
    }
}
