/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Serialization;
using yats.ExecutionQueue.DecisionHandler;
using yats.ExecutionQueue.ResultHandler;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.Utilities;

namespace yats.ExecutionQueue
{
    public class TestStepsEventArgs : EventArgs
    {
        public IList<TestStep> Steps;
        public TestStepsEventArgs(IList<TestStep> steps)
        {
            this.Steps = steps;
        }
        public TestStepsEventArgs(TestStep step)
        {
            this.Steps = step.ToSingleItemList();
        }
        public TestStepsEventArgs(params TestStep [] steps)
        {
            this.Steps = new List<TestStep>(steps);
        }
    }

    public abstract class TestStep : IConfigurable
	{
		protected IStepExecution executionMethod;
        public IStepExecution ExecutionMethod
		{
            [DebuggerStepThrough]
			get {
				if (executionMethod == null){
					executionMethod = new SequentialExecutionModifier();
				}
				return executionMethod;
			}
            [DebuggerStepThrough]
			set {executionMethod = value;}
		}

        protected string name = string.Empty;
        public string Name
        {
            [DebuggerStepThrough]
            get
            {
				if (string.IsNullOrEmpty(name)){
					return "Group";
				}
                return name;
            }
            [DebuggerStepThrough]
            set { name = value; }
        }
	
		/// <summary>
		/// Determines the overall result from multiple step repetitions
		/// </summary>
		public abstract IRepetitionResultHandler RepetitionResultHandler
		{
			get;
			set;
		}
		
		protected IDecisionHandler decisionHandler;
        /// <summary>
        /// Supported types: SingleRun, RepeatWhilePass, RepeatIgnoreFails, RepeatUntilCancel
        /// </summary>
		public IDecisionHandler DecisionHandler
		{
            [DebuggerStepThrough]
			get {
				if (decisionHandler == null){
					decisionHandler = new SingleRun();
				}
				return decisionHandler;
			}
            [DebuggerStepThrough]
			set {decisionHandler = value;}
		}

        protected ITestResult result;
        [XmlIgnore]
        public ITestResult Result
        {
            [DebuggerStepThrough]
            get { return result; }
            [DebuggerStepThrough]
            set { result = value; }
        }

        public abstract List<IParameter> Parameters
        {
            get;
            set;
        }

		public TestStep ()
		{			
		}

        protected Guid guid = Guid.NewGuid();
        public Guid Guid
        {
            //[DebuggerStepThrough]
            get
            {
                return guid;
            }
            //[DebuggerStepThrough]
            set
            {
                guid = value;
            }
        }
		
		public abstract void Accept(ITestStepVisitor visitor);


        public abstract TestStep Clone();
    }
}

