﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using yats.ExecutionQueue.CompositeStepExecuteDecision;
using yats.ExecutionQueue.DecisionHandler;
using yats.ExecutionQueue.ResultHandler;

namespace yats.ExecutionQueue
{
    /// <summary>
    /// Given a test step object, generates a human-readable string with a summary of the step's settings
    /// </summary>
    public class StepInfoStringGenerator : ITestStepVisitor, IStepExecutionVisitor, IDecisionHandlerVisitor, IRepetitionResultVisitor, ISingleResultVisitor, ICompositeResultVisitor, ICompositeDecisionVisitor
    {
        string exec = null;
        string dec = null;
        string rep = null;
        string single = null;
        string composite = null;
        string compDecision = null;
        static char[] endChars = new char[] { ',', ' ' };
        
        public static string Get(TestStep step)
        {
        if (IsStepDisabledVisitor.Get (step))
            {
                return "";
            }

            StepInfoStringGenerator visitor = new StepInfoStringGenerator();
            step.Accept(visitor);

            visitor.dec = (string.IsNullOrEmpty(visitor.dec) ? "" : visitor.dec + ", ");
            visitor.exec = (string.IsNullOrEmpty(visitor.exec) ? "" : visitor.exec + ", ");
            visitor.rep = (string.IsNullOrEmpty(visitor.rep) ? "" : visitor.rep + ", ");
            visitor.single = (string.IsNullOrEmpty(visitor.single) ? "" : visitor.single + ", ");
            visitor.composite = (string.IsNullOrEmpty(visitor.composite) ? "" : visitor.composite + ", ");
            visitor.compDecision = (string.IsNullOrEmpty(visitor.compDecision) ? "" : visitor.compDecision + ", ");

            return (visitor.dec + visitor.exec + visitor.rep + visitor.single + visitor.composite + visitor.compDecision).TrimEnd(endChars);
        }

        #region ITestStepVisitor Members

        public void AcceptSingle(TestStepSingle test)
        {
            test.DecisionHandler.Accept(this);
            test.ExecutionMethod.Accept(this);
            test.RepetitionResultHandler.Accept(this);
            test.SingleRunResultHandler.Accept(this);
        }

        public void AcceptComposite(TestStepComposite test)
        {
            test.CompositeResultHandler.Accept(this);
            test.DecisionHandler.Accept(this);
            test.ExecutionMethod.Accept(this);
            test.RepetitionResultHandler.Accept(this);
            test.StepDecisionHandler.Accept(this);
        }
        #endregion

        #region IStepExecutionVisitor Members

        public void VisitParallelExecutionModifier(ParallelExecutionModifier modifier)
        {
            exec = "in parallel";
        }

        public void VisitSequentialExecutionModifier(SequentialExecutionModifier modifier)
        {
        }

        public void VisitRandomOrderModifier(RandomOrderModifier modifier)
        {
            exec = "in random order";
        }

        public void VisitStepDisabledModifier(StepDisabledModifier modifier)
        {
        }

        public void VisitOneRandomStepExecutionModifier(OneRandomStepExecutionModifier modifier)
        {
            exec = "one random step";
        }

        #endregion

        #region IDecisionHandlerVisitor Members

        public void VisitSingleRun(SingleRun decision)
        {
        }

        public void VisitRepeatWhilePass(RepeatWhilePass decision)
        {
            if (decision.Repetitions > 1)
            {
                dec = string.Format("{0} repetitions while passing", decision.Repetitions);
            }
        }

        public void VisitRepeatUntilCancel(RepeatUntilCancel decision)
        {
            dec = "run until canceled";
        }

        public void VisitRepeatIgnoreFails(RepeatIgnoreFails decision)
        {
            if (decision.Repetitions > 1)
            {
                dec = string.Format("{0} repetitions ignoring failures", decision.Repetitions);
            }
        }

        public void VisitRepeatUntilConfiguredResult(RepeatUntilConfiguredResult decision)
        {
            string waitfor = string.Empty;
            foreach (var res in decision.WaitFor)
            {
                if (string.IsNullOrEmpty(waitfor))
                {
                    waitfor = res.ToString().ToLower();
                }
                else
                {
                    waitfor += " or " + res.ToString().ToLower();
                }
            }
            dec = string.Format("max. {0} repetitions until {1}", decision.Repetitions, waitfor);
        }

        #endregion

        #region IRepetitionResultVisitor Members

        public void VisitDefaultRepetitionResult(DefaultRepetitionResultHandler handler)
        {
        }

        public void VisitWorstCaseRepetition(WorstCaseRepetitionResultHandler handler)
        {
            rep = "evaluate to worst repetition";            
        }

        public void VisitBestCaseRepetition(BestCaseRepetitionResultHandler handler)
        {
            rep = "evaluate to best repetition";
        }

        #endregion

        #region ISingleResultVisitor Members

        public void VisitDefaultSingleResultHandler(DefaultSingleResultHandler handler)
        {
        }

        public void VisitCustomResultMapping(CustomResultMapping handler)
        {
            List<string> mapped = new List<string>();
            if (handler.ValuePass!= yats.TestCase.Interface.ResultEnum.PASS)
            {
                mapped.Add("pass\u2192" + handler.ValuePass.ToString().ToLower());
            }
            if (handler.ValueFail!= yats.TestCase.Interface.ResultEnum.FAIL)
            {
                mapped.Add("fail\u2192" + handler.ValueFail.ToString().ToLower());
            }
            if (handler.ValueInconclusive!= yats.TestCase.Interface.ResultEnum.INCONCLUSIVE)
            {
                mapped.Add("inconclusive\u2192" + handler.ValueInconclusive.ToString().ToLower());
            }
            if (handler.ValueCanceled != yats.TestCase.Interface.ResultEnum.CANCELED)
            {
                mapped.Add("cancel\u2192" + handler.ValueCanceled.ToString().ToLower());
            }
            if (handler.ValueNotRun!= yats.TestCase.Interface.ResultEnum.NOT_RUN)
            {
                mapped.Add("not run\u2192" + handler.ValueNotRun.ToString().ToLower());
            }
            if (mapped.Count > 0)
            {
                single = string.Format("Result map [{0}]", string.Join(", ", mapped.ToArray()));
                return;
            }
            single = "Result map [not set]";
        }

        public void VisitIgnoreFailureSingleResultHandler(IgnoreFailureSingleResultHandler handler)
        {
            single = "ignore failure";
        }

        #endregion
        
        #region ICompositeResultVisitor Members

        public void VisitDefaultCompositeResultHandler(DefaultCompositeResultHandler handler)
        {
        }

        public void VisitWorstStep(WorstCaseCompositeResultHandler handler)
        {
            composite = "take the worst step result";
        }

        public void VisitSelectedSteps(SelectedStepResultHandler handler)
        {
            composite = "evaluate selected steps";
        }
        
        public void VisitStateMachine(StateMachineResultHandler handler)
        {
        }

        #endregion

        #region ICompositeDecisionVisitor Members

        public void VisitExecuteUntilCanceled(ExecuteUntilCanceled decision)
        {
        }

        public void VisitExecuteWhilePassing(ExecuteWhilePassing decision)
        {
            compDecision = "stop if step fails";
        }

        public void VisitStateMachine(StateMachineExecutionModifier decision)
        {
            compDecision = "execute as state machine";
        }
        #endregion
    }
}
