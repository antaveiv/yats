﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using yats.TestCase.Parameter;

namespace yats.ExecutionQueue
{
	// Gets a user-friendly name of test step and result that is used as a parameter to another test
    public class GetTestResultNameVisitor : ITestStepVisitor
    {
        #region ITestStepVisitor Members

        public void AcceptSingle(TestStepSingle test)
        {
            foreach(var param in test.Parameters)
            {
                var useResult = param.Value as TestResultParameter;
                if(useResult != null && useResult.ResultID == searchForGuid && param.IsResult)
                {
                    result = string.Format("[use result {0}.{1}]", test.Name, param.Name);
                    return;
                }
            }
        }

        public void AcceptComposite(TestStepComposite test)
        {
            foreach(var step in test.Steps)
            {
                if(result != null)
                {
                    return;
                }
                step.Accept( this );
            }
        }

        #endregion

        Guid searchForGuid;
        string result = null;

        public static string GetName(TestResultParameter useResult, TestStep root)
        {
            try
            {
                var visitor = new GetTestResultNameVisitor( );
                visitor.searchForGuid = useResult.ResultID;
                root.Accept( visitor );
                if(visitor.result != null)
                {
                    return visitor.result;
                }

                return useResult.GetDisplayValue( );
            }
            catch
            {
                return "[use test result]";
            }
        }
    }
}
