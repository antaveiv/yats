﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.TestCase.Parameter;
using yats.TestRepositoryManager.Interface;
using System.Collections;
using System.Collections.Generic;

namespace yats.ExecutionQueue
{
	// This visitor checks that all global test parameters are available. If not available, the value is automatically set to null.
	// If a test result value is used as a parameter and the test is no longer available, the parameter is also set to null
    public class FindNullParametersVisitor : ITestStepVisitor
    {
        TestStep root;
        bool result = true;
        IGlobalParameterCollection globals;
        ITestRepositoryManagerCollection testRepositoryManagers;

        /// <summary>
        /// check if all global parameter values can be found. If not found, the corresponding parameter is set to null
        /// </summary>
        /// <param name="globalParameters"></param>
        /// <param name="value"></param>
        /// <returns>true if all global parameters were found</returns>
        public static bool Validate(TestStep root, IGlobalParameterCollection globals, ITestRepositoryManagerCollection testRepositoryManagers)
        {
            var visitor = new FindNullParametersVisitor();
            visitor.globals = globals;
            visitor.root = root;
            visitor.testRepositoryManagers = testRepositoryManagers;
            root.Accept(visitor);
            return visitor.result;
        }

        public static bool Validate(TestStep root, IEnumerable<TestStep> steps, IGlobalParameterCollection globals, ITestRepositoryManagerCollection testRepositoryManagers)
        {
            var visitor = new FindNullParametersVisitor();
            visitor.globals = globals;
            visitor.root = root;
            visitor.testRepositoryManagers = testRepositoryManagers;
            foreach (var step in steps)
            {
                step.Accept(visitor);
            }
            return visitor.result;
        }

        #region ITestStepVisitor Members

        public void AcceptSingle(TestStepSingle test)
        {
            if (IsStepDisabledVisitor.Get(root, test))
            {
                return;
            }

            if (test.Parameters == null)
            {
                return;
            }

            foreach (var param in test.Parameters)
            {
                if (param.IsResult)
                {
                    continue;
                }
                if (param.Value != null)
                {
                    bool badParameter = false;
                    if (param.Value is  GlobalParameter)
                        badParameter = (param.Value as GlobalParameter).Value == null;
                    else if (param.Value is GlobalValue)
                        badParameter = globals.Get((param.Value as GlobalValue)) == null;
                    else if (param.Value is TestResultParameter)
                    {
                        if (IsResultParameterConnectedVisitor.Check((param.Value as TestResultParameter).ResultID, root) == false)
                        {
                            param.Value = null;
                            badParameter = true;    
                        }                        
                    }
                    if (badParameter && testRepositoryManagers.GetManagerByTestUniqueId(test.UniqueTestId).CanParameterBeNull(test.TestCase, param)==false)
                    {
                        result = false;
                    }
                }
                else if (testRepositoryManagers.GetManagerByTestUniqueId(test.UniqueTestId).CanParameterBeNull(test.TestCase, param) == false)
                {
                    result = false;
                }
            }
        }

        public void AcceptComposite(TestStepComposite test)
        {
            if (IsStepDisabledVisitor.Get(root, test))
            {
                return;
            }

            foreach (var step in test.Steps)
            {
                step.Accept(this);
            }

            if (test.Parameters == null)
            {
                return;
            }
            foreach (var param in test.Parameters)
            {
                if (param.Value != null)
                {
                    bool badParameter = false;
                    if (param.Value is GlobalParameter)
                        badParameter = (param.Value as GlobalParameter).Value == null;
                    else if (param.Value is GlobalValue)
                        badParameter = globals.Get((param.Value as GlobalValue)) == null;

                    if (badParameter)
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }
        }

        #endregion

    }
}
