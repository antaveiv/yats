﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.ExecutionQueue.DecisionHandler;

namespace yats.ExecutionQueue
{
	// Estimates the total number of test steps by analyzing a test run hierarchy. An estimate can not be calculated if e.g. the test run contains 'run until cancelled' steps
    public class GetNumberOfStepsVisitor : ITestStepVisitor, IDecisionHandlerVisitor
    {
        int result = 0;
        bool canEstimate = true;
        TestStep root;
        public static bool Get(TestStep root, TestStep step, out int result)
        {
            var visitor = new GetNumberOfStepsVisitor( );
            visitor.root = root;
            step.Accept( visitor );
            result = visitor.result;
            return visitor.canEstimate;
        }

        #region ITestStepVisitor Members

        public void AcceptSingle(TestStepSingle test)
        {
            if (IsStepDisabledVisitor.Get(root, test))
            {
                return;
            }
            test.DecisionHandler.Accept( this );
        }

        public void AcceptComposite(TestStepComposite test)
        {
            if (IsStepDisabledVisitor.Get(root, test))
            {
                return;
            }
            int result = this.result;
            bool canEstimate = this.canEstimate;

            this.result = 0;
            this.canEstimate = true;
            test.DecisionHandler.Accept( this );
            canEstimate &= this.canEstimate;
            int ownRepetitions = this.result;

            int childItems = 0;
            foreach(var step in test.Steps)
            {
                this.result = 0;
                this.canEstimate = true;
            
                step.Accept(this);
                canEstimate &= this.canEstimate;
                childItems += this.result;
            }
            
            this.result = result + (ownRepetitions * childItems); //execution will also report composite step result
            this.canEstimate = canEstimate;
        }

        #endregion

        #region IDecisionHandlerVisitor Members

        public void VisitSingleRun(SingleRun decision)
        {
            result++;
        }

        public void VisitRepeatWhilePass(RepeatWhilePass decision)
        {
            result = decision.Repetitions;
        }

        public void VisitRepeatUntilCancel(RepeatUntilCancel decision)
        {
            canEstimate = false;
        }

        public void VisitRepeatIgnoreFails(RepeatIgnoreFails decision)
        {
            result = decision.Repetitions;
        }

        public void VisitRepeatUntilConfiguredResult(RepeatUntilConfiguredResult decision)
        {
            result = decision.Repetitions;
        }

        #endregion
    }
}
