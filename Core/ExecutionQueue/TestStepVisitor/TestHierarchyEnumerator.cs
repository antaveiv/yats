﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;

namespace yats.ExecutionQueue
{
    //Allows performing a Lambda expression on all test steps starting from a given step
    public class TestHierarchyEnumerator : ITestStepVisitor
    {
        Action<TestStepComposite> CompositeFunc;
        Action<TestStepSingle> SingleFunc;
        Action<TestStep> AnyStepFunc;

        public static void OnComposites(TestStep step, Action<TestStepComposite> objFunc)
        {
            if (step == null)
            {
                return;
            }
            TestHierarchyEnumerator visitor = new TestHierarchyEnumerator();
            visitor.CompositeFunc = objFunc;
            step.Accept(visitor);
        }

        public static void OnComposites(IEnumerable<TestStep> steps, Action<TestStepComposite> objFunc)
        {
            if (steps == null)
            {
                return;
            }
            TestHierarchyEnumerator visitor = new TestHierarchyEnumerator();
            visitor.CompositeFunc = objFunc;
            foreach (var step in steps)
            {
                step.Accept(visitor);
            }
        }

        public static void OnSingle(TestStep step, Action<TestStepSingle> objFunc)
        {
            if (step == null)
            {
                return;
            }
            TestHierarchyEnumerator visitor = new TestHierarchyEnumerator();
            visitor.SingleFunc = objFunc;
            step.Accept(visitor);
        }

        public static void OnSingle(IEnumerable<TestStep> steps, Action<TestStepSingle> objFunc)
        {
            if (steps == null)
            {
                return;
            }
            TestHierarchyEnumerator visitor = new TestHierarchyEnumerator();
            visitor.SingleFunc = objFunc;
            foreach (var step in steps)
            {
                step.Accept(visitor);
            }
        }

        public static void OnAny(TestStep step, Action<TestStep> objFunc)
        {
            if (step == null)
            {
                return;
            }
            TestHierarchyEnumerator visitor = new TestHierarchyEnumerator();
            visitor.AnyStepFunc = objFunc;
            step.Accept(visitor);
        }

        public static void OnAny(IEnumerable<TestStep> steps, Action<TestStep> objFunc)
        {
            if (steps == null)
            {
                return;
            }
            TestHierarchyEnumerator visitor = new TestHierarchyEnumerator();
            visitor.AnyStepFunc = objFunc;
            foreach (var step in steps)
            {
                step.Accept(visitor);
            }
        }

        #region ITestStepVisitor Members

        public void AcceptSingle(TestStepSingle test)
        {
            if (SingleFunc != null)
            {
                SingleFunc(test);
            }
            if (AnyStepFunc != null)
            {
                AnyStepFunc(test);
            }
        }

        public void AcceptComposite(TestStepComposite test)
        {
            if (CompositeFunc != null)
            {
                CompositeFunc(test);
            }
            if (AnyStepFunc != null)
            {
                AnyStepFunc(test);
            }            
            foreach (var step in test.Steps)
            {
                var st = step;
                st.Accept(this);
            }
        }

        #endregion
    }
}
