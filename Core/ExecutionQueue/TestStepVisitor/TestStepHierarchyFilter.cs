﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;

namespace yats.ExecutionQueue
{
    /// <summary>
    /// By accepting several steps forms a filtered list. If a composite test step is in the list, its child nodes are rejected
    /// </summary>
    public class TestStepHierarchyFilter
    {
        public static List<TestStep> Filter(IList<TestStep> steps)
        {
            List<TestStep> filtered = new List<TestStep>( );
            if(steps == null)
            {
                return filtered;
            }

            foreach(var step in steps)
            {
                bool skip = false;
                foreach(var alreadyAdded in filtered)
                {
                    if(IsInHierarchyVisitor.Get( step, alreadyAdded ))
                    {
                        // step is higher in hierarchy than alreadyAdded, replace
                        filtered.Remove( alreadyAdded );
                        filtered.Add( step );
                        break;
                    }
                    if(IsInHierarchyVisitor.Get( alreadyAdded, step ))
                    {
                        // nothing new, alreadyAdded item is in filtered result and step is a child node - dont add
                        skip = true;
                    }
                }

                if(!skip)
                {
                    filtered.Add( step );
                }
            }

            return filtered;
        }
    }
}
