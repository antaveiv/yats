﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using yats.TestCase.Parameter;

namespace yats.ExecutionQueue
{
	// Checks if the test case containing a test result with given GUID is still available
    public class IsResultParameterConnectedVisitor : ITestStepVisitor
    {
        #region ITestStepVisitor Members

        public void AcceptSingle(TestStepSingle test)
        {
            CheckParameters( test );
        }

        private void CheckParameters(TestStep test)
        {
            if(test.Parameters == null)
            {
                return;
            }
            foreach(var p in test.Parameters)
            {
                if(p.IsResult == false)
                {
                    continue;
                }

                TestResultParameter res = p.Value as TestResultParameter;
                if(res != null)
                {
                    if(res.ResultID == resultGuid)
                    {
                        ok = true;
                        return;
                    }
                }
            }
        }
        
        public void AcceptComposite(TestStepComposite test)
        {
            CheckParameters( test );

            foreach(var step in test.Steps)
            {
                if(!ok)
                {
                    step.Accept( this );
                }
                else break;
            }
        }

        #endregion

        Guid resultGuid;
        bool ok = false;
        public static bool Check(Guid resultGuid, TestStep root)
        {
            var visitor = new IsResultParameterConnectedVisitor( );
            visitor.resultGuid = resultGuid;
            root.Accept( visitor );
            return visitor.ok;
        }
    }
}
