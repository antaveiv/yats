﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Linq;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.Interface;
using yats.Utilities;

namespace yats.ExecutionQueue
{
    //when test sequence is deserialized, synchronize parameter lists - remove those not present in the test case and add new parameters. When a test case
    //implementation changes its parameter list, the old parameter list is saved in the XML
    public class SynchronizeLoadedParametersVisitor : ITestStepVisitor
    {
        bool changed = false;
        ITestRepositoryManagerCollection testRepositoryManagers;
        IGlobalParameterCollection globals;
        TestStep root;

        public static bool Process(TestStep loadedTestRoot, ITestRepositoryManagerCollection testRepositoryManagers, IGlobalParameterCollection globals)
        {
            if (loadedTestRoot == null)
            {
                return false;
            }
            var visitor = new SynchronizeLoadedParametersVisitor();
            visitor.testRepositoryManagers = testRepositoryManagers;
            visitor.globals = globals;
            visitor.root = loadedTestRoot;
            loadedTestRoot.Accept(visitor);
            return visitor.changed;
        }

        #region ITestStepVisitor Members

        public void AcceptSingle(TestStepSingle test)
        {
            var loaded = test.Parameters;
            var implementation = testRepositoryManagers.GetManagerByTestUniqueId(test.UniqueTestId).GetParameters(test.TestCase);
            RemoveNonExistingParameters(loaded, implementation);
            AddNewlyImplementedParameters(loaded, implementation);
            ReplaceChangedParameterTypes(loaded, implementation);
            SynchronizeParameters(loaded, implementation);

            // sort parameters alphabetically
            loaded.Sort(delegate(IParameter p1, IParameter p2) { return ((TestParameterInfo)p1).Name.CompareTo(((TestParameterInfo)p2).Name); });
        }

        private void RemoveNonExistingParameters(IList<IParameter> loaded, IList<IParameter> implementation)
        {
            List<IParameter> toRemove = new List<IParameter>();
            foreach (var loadedParam in loaded)
            {
                if (implementation.Count(x => x.Name.Equals(loadedParam.Name)) == 0)
                {
                    //no longer available in test case implementation
                    toRemove.Add(loadedParam);
                }
            }
            if (toRemove.Count > 0)
            {
                //Console.WriteLine("RemoveNonExistingParameters");
                //Console.WriteLine("***Loaded:");
                //foreach (var i in loaded) Console.WriteLine(i);
                //Console.WriteLine("***Implementation:");
                //foreach (var i in implementation) Console.WriteLine(i);
                //Console.WriteLine("***ToRemove:");
                //foreach (var i in toRemove) Console.WriteLine(i);
                
                //TestParameterInfo.Equals does not compare Name property. Need to remove by object reference, otherwise a similar parameter may be removed
                if (loaded.RemoveByReference(toRemove) > 0)
                {
                    changed = true;
                }
            }            
        }

        private void AddNewlyImplementedParameters(IList<IParameter> loaded, IList<IParameter> implementation)
        {
            List<IParameter> toAdd = new List<IParameter>();
            // add parameters that were added to the test case implementation
            foreach (var p in implementation)
            {
                //no longer available in test case implementation
                if (loaded.Count(x => x.Name.Equals(p.Name)) == 0)
                {
                    toAdd.Add(p);
                }
            }

            if (toAdd.Count > 0)
            {
                //Console.WriteLine("AddNewlyImplementedParameters");
                //Console.WriteLine("***Loaded:");
                //foreach (var i in loaded) Console.WriteLine(i);
                //Console.WriteLine("***Implementation:");
                //foreach (var i in implementation) Console.WriteLine(i);
                //Console.WriteLine("***toAdd:");
                //foreach (var i in toAdd) Console.WriteLine(i);

                loaded.AddRange(toAdd);
                changed = true;
            }
        }

        private void ReplaceChangedParameterTypes(IList<IParameter> loaded, IList<IParameter> currentImplementationParameters)
        {
            List<IParameter> toRemove = new List<IParameter>();
            List<IParameter> toAdd = new List<IParameter>();
            foreach (var loadedParam in loaded)
            {
                IParameter currentImplementationParam = null;
                try
                {
                    currentImplementationParam = currentImplementationParameters.First(x => x.Name == loadedParam.Name);
                }
                catch { }
                if (currentImplementationParam == null)
                {
                    //should not happen
                    continue;
                }
                if (currentImplementationParam.Equals(loadedParam))
                {
                    continue;
                }

                toRemove.Add(loadedParam);
#if DEBUG
                currentImplementationParam.Equals(loadedParam);// for breakpoint
#endif
                toAdd.Add(currentImplementationParam);
                if (loadedParam.Value != null)
                {
                    // only warn the user about changed parameters if a value had been assigned before, otherwise update silently
                    changed = true;
                }
            }
            loaded.Remove(toRemove);
            loaded.AddRange(toAdd);
        }

        private void SynchronizeParameters(IList<IParameter> loadedParameters, IList<IParameter> currentImplementationParameters)
        {
            List<IParameter> toRemove = new List<IParameter>();
            List<IParameter> toAdd = new List<IParameter>();
            foreach (var loadedParam in loadedParameters)
            {
                IParameter currentImplementationParam = null;
                try
                {
                    currentImplementationParam = currentImplementationParameters.First(x => x.Name == loadedParam.Name);
                }
                catch { }
                if (currentImplementationParam == null)
                {
                    //should not happen
                    continue;
                }

                // if true, means a value was also loaded - check if it still matches the implementation type
                if (currentImplementationParam.Value != null || loadedParam.Value != null)
                {
                    List<AbstractParameterValue> currentValues;
                    if (currentImplementationParam.Value != null)
                    {
                        currentValues = new List<AbstractParameterValue>();
                        bool found;
                        var val = GetParameterValueVisitor.Get(currentImplementationParam.Value, globals, out found);
                        if (found)
                        {
                            currentValues.Add(val);
                        }
                    }
                    else
                    {
                        currentValues = ParameterTypeCollection.Instance.GetSuitableValueTypes(currentImplementationParam);
                    }

                    List<AbstractParameterValue> loadedValues;
                    if (loadedParam.Value != null)
                    {
                        loadedValues = new List<AbstractParameterValue>();
                        bool found;
                        var val = GetParameterValueVisitor.Get(loadedParam.Value, globals, out found);

                        if (found)
                        {
                            // If the value is configured to take a result of another test,
                            // check that the other test result type is still compatible
                            var resultValue = GetTestResultParameter(root, val);
                            if (resultValue != null)
                            {
                                loadedValues = ParameterTypeCollection.Instance.GetSuitableValueTypes(resultValue);
                            }
                            else
                            {
                                loadedValues.Add(val);
                            }
                        }
                    }
                    else
                    {
                        loadedValues = ParameterTypeCollection.Instance.GetSuitableValueTypes(loadedParam);
                    }

                    if (currentValues.Count > 0 && loadedValues.Count > 0)
                    {
                        bool matchFound = false;
                        // changed this part when a new parameter type supporting the same data type was implemented. That should not break saved test runs - the old implementation is still available and has the same type hash
                        foreach (var i in currentValues)
                        {
                            foreach (var j in loadedValues)
                            {
                                matchFound |= (i.GetParamValueTypeHash() == j.GetParamValueTypeHash());
                                break;
                            }
                        }

                        if (matchFound == false)
                        {
                            toRemove.Add(loadedParam);
                            toAdd.Add(currentImplementationParam);

                            if (loadedParam.Value != null)
                            {
                                // only warn the user about changed parameters if a value had been assigned before
                                changed = true;

                                // for debugging
                                foreach (var i in currentValues)
                                {
                                    foreach (var j in loadedValues)
                                    {
                                        matchFound |= (i.GetParamValueTypeHash() == j.GetParamValueTypeHash());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            loadedParameters.Remove(toRemove);
            loadedParameters.AddRange(toAdd);
        }

        private IParameter GetTestResultParameter(TestStep root, AbstractParameterValue val)
        {
            TestResultParameter resValue = val as TestResultParameter;
            if (resValue == null)
            {
                return null;
            }

            foreach (var param in ParameterEnumerator.GetParameters(root))
            {
                TestResultParameter paramValue = param.Value as TestResultParameter;
                if (param.IsResult && paramValue != null && paramValue.ResultID == resValue.ResultID)
                {
                    return param;
                }
            }

            return null;
        }

        public void AcceptComposite(TestStepComposite test)
        {
            foreach (var step in test.Steps)
            {
                step.Accept(this);
            }
        }

        #endregion
    }
}
