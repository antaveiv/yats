﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


namespace yats.ExecutionQueue
{
    /// <summary>
    /// finds if the given step or any of its parent composite steps have Disabled execution method
    /// </summary>
    public class IsStepDisabledVisitor : ITestStepVisitor
    {
        bool result = false;

        #region ITestStepVisitor Members

        public void AcceptSingle(TestStepSingle test)
        {
            if(test.ExecutionMethod is StepDisabledModifier)
            {
                result = true;
            }
        }

        public void AcceptComposite(TestStepComposite test)
        {
            if(test.ExecutionMethod is StepDisabledModifier)
            {
                result = true;
            }
        }

        #endregion

        /// <summary>
        /// Find if a test step is disabled. This method only checks the given step execution method. The step may still be disabled because its parent is disabled. 
        /// </summary>
        /// <param name="step">Test run to check</param>
        /// <returns></returns>
        public static bool Get (TestStep step)
            {
            IsStepDisabledVisitor visitor = new IsStepDisabledVisitor ();
            if (step != null)
                {
                step.Accept (visitor);
                }
            return visitor.result;
            }

        /// <summary>
        /// Find if a test step is disabled. The step itself may have a non-disabled method but its parent may. This method checks the whole test hierarchy
        /// </summary>
        /// <param name="root">Root test run step</param>
        /// <param name="step">Test run to check</param>
        /// <returns></returns>
        public static bool Get(TestStep root, TestStep step)
        {
            IsStepDisabledVisitor visitor = new IsStepDisabledVisitor( );
            while(step != null && visitor.result == false)
            {
                step.Accept( visitor );
                step = GetParentVisitor.Get( root, step );
            }
            return visitor.result;
        }
    }
}
