﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;

namespace yats.ExecutionQueue
{
	// Finds a direct parent of a given step in a test run hierarchy
    public class GetParentVisitor : ITestStepVisitor
    {
        TestStep child;
        TestStepComposite parent = null;

        public GetParentVisitor(TestStep child)
        {
            this.child = child;
        }

        public static TestStepComposite Get(TestStep root, TestStep child)
        {
            GetParentVisitor visitor = new GetParentVisitor(child);
            root.Accept(visitor);
            return visitor.parent;
        }

        #region ITestStepVisitor Members

        [DebuggerStepThrough]
        public void AcceptSingle(TestStepSingle test)
        {
        }

        public void AcceptComposite(TestStepComposite test)
        {
            if (test.Steps.Contains(child))
            {
                parent = test;
                return;
            }

            foreach (var step in test.Steps)
            {
                if (parent == null)
                {
                    step.Accept(this);
                }
                else
                {
                    break;
                }
            }
        }

        #endregion
    }

}
