﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/


namespace yats.ExecutionQueue
{
    /// <summary>
    /// Given two test step objects, returns if one is other's child step in the test queue hierarchy. Can be used e.g. to check if a selection contains both a child step and its parent
    /// </summary>
    public class IsInHierarchyVisitor : ITestStepVisitor
    {
        bool result = false;
        TestStep possibleChild;
        protected IsInHierarchyVisitor(TestStep possibleChild)
        {
            this.possibleChild = possibleChild;
        }

        #region ITestStepVisitor Members

        public void AcceptSingle(TestStepSingle test)
        {
        }

        public void AcceptComposite(TestStepComposite test)
        {
            foreach (var step in test.Steps)
            {
                if (object.ReferenceEquals(step, possibleChild))
                {
                    this.result = true;
                    return;
                }
                if (result) return;
                step.Accept(this);
            }
        }

        #endregion

        public static bool Get(TestStep possibleParent, TestStep possibleChild)
        {
            if(object.ReferenceEquals( possibleParent, possibleChild ))
            {
                return true;
            }

            IsInHierarchyVisitor visitor = new IsInHierarchyVisitor(possibleChild);
            possibleParent.Accept(visitor);
            return visitor.result;
        }
    }
}
