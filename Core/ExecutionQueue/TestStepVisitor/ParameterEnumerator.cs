﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using yats.TestCase.Parameter;

namespace yats.ExecutionQueue
{
	// Collects all parameters of a given test hierarchy into a list
    public class ParameterEnumerator : ITestStepVisitor
    {
        List<IParameter> result = new List<IParameter>( );
        public static IList<IParameter> GetParameters(List<TestStep> steps)
        {
            ParameterEnumerator visitor = new ParameterEnumerator( );
            foreach(var step in steps)
            {
                step.Accept( visitor );
            }
            return visitor.result;
        }

        public static IList<IParameter> GetParameters(TestStep step)
        {
            ParameterEnumerator visitor = new ParameterEnumerator( );
            step.Accept( visitor );
            return visitor.result;
        }

        public static IList<IParameter> GetParameters(IConfigurable step)
        {
            return step.Parameters;
        }

        public static void GetParameters(TestStep step, Action<IParameter> objFunc)
        {
            ParameterEnumerator visitor = new ParameterEnumerator();
            step.Accept(visitor);
            foreach (var param in visitor.result)
            {
                objFunc(param);
            }
        }

        public static void GetParameters(IEnumerable<TestStep> steps, Action<IParameter> objFunc)
        {
            ParameterEnumerator visitor = new ParameterEnumerator();
            foreach (var step in steps)
            {
                step.Accept(visitor);
            }
            foreach (var param in visitor.result)
            {
                objFunc(param);
            }
        }

        #region ITestStepVisitor Members

        public void AcceptSingle(TestStepSingle test)
        {
            if(test.Parameters != null)
            {
                result.AddRange( test.Parameters );
            }
        }

        public void AcceptComposite(TestStepComposite test)
        {
            if(test.Parameters != null)
            {
                result.AddRange( test.Parameters );
            }
            foreach(var step in test.Steps)
            {
                step.Accept( this );
            }
        }

        #endregion
    }
}
