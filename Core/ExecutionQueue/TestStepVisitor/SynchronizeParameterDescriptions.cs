﻿using yats.TestRepositoryManager.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace yats.ExecutionQueue
{
    /// <summary>
    /// Updates parameter descriptions
    /// </summary>
    public class SynchronizeParameterDescriptions 
    {
        /// <summary>
        /// returns true if any descriptions were updated
        /// </summary>
        /// <param name="root"></param>
        /// <param name="managers"></param>
        /// <returns></returns>
        public static bool Process(TestStep root, List<ITestRepositoryManager> managers)
        {
            if (root == null)
            {
                return false;
            }

            bool result = false;
            TestHierarchyEnumerator.OnSingle(root, (single) => {
                var defaultParams = single.RepositoryManager.GetParameters(single.TestCase);
                foreach (var p in single.Parameters) {
                    var matchingParam = defaultParams.FirstOrDefault(x=>x.Name == p.Name);
                    if (matchingParam != null && matchingParam.DefaultDescription != p.DefaultDescription)
                    {
                        p.DefaultDescription = matchingParam.DefaultDescription;
                        result = true;
                    }
                }
            });
            return result;
        }
    }
}
