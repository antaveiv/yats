﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using yats.ExecutionQueue.ResultHandler;
using yats.Utilities;

namespace yats.ExecutionQueue
{
    public class StateMachineSanityCheckVisitor
    {
        bool isModified = false;

        // throws exceptions if errors are found. Non-essential errors are fixed. Change is indicated by isModified
        public static void CheckStepMachines(TestStep root, out bool isModified)
        {
            isModified = false;
            if (root == null)
            {
                return;
            }
            
            StateMachineSanityCheckVisitor visitor = new StateMachineSanityCheckVisitor();

            TestHierarchyEnumerator.OnComposites(root, (TestStepComposite step) =>
            {
                if (step.ExecutionMethod is StateMachineExecutionModifier)
                {
                    visitor.DoChecks(step, step.ExecutionMethod as StateMachineExecutionModifier);
                }
            });

            isModified = visitor.isModified;
        }

        //WARNING: will perform the checks on the sm parameter, not the test.ExecutionMethod. Should be used to test clones (a clone is not assigned to test step during edit)
        public static void CheckStepMachine(TestStepComposite test, StateMachineExecutionModifier sm, out bool isModified)
        {
            isModified = false;
            if (test == null || sm == null)
            {
                return;
            }
            StateMachineSanityCheckVisitor visitor = new StateMachineSanityCheckVisitor();
            visitor.DoChecks(test, sm);
        }

        private void DoChecks(TestStepComposite step, StateMachineExecutionModifier sm)
        {
            RemoveNonExistingSteps(step, sm);
            RemoveNonExistingStateReferences(step, sm);
            CheckHasStartStep(sm);
            CheckHasFinishStep(sm);
            CheckAllStepsUsedInStates(step, sm);
            ReplaceWrongResultHandler(step);
            CheckAllStatesReachable(sm);
            CheckFinishStatesDoNotHaveSteps(sm);
            CheckDeadlocks(step, sm);
        }

        private void CheckDeadlocks(TestStepComposite testStep, StateMachineExecutionModifier sm)
        {
            HashSet<Guid> reachableStateGuids = new HashSet<Guid>();
            reachableStateGuids.AddAll(sm.States.Where(s => s.IsFinishState).Select(x => x.StateGUID));

            HashSet<Guid> allStateGuids = new HashSet<Guid>();
            allStateGuids.AddAll(sm.States.Select(x => x.StateGUID));
            allStateGuids.Remove(reachableStateGuids);

            bool doContinue = false;
            do
            {
                doContinue = false;
                foreach (var stateGuid in allStateGuids)
                {
                    var state = sm.States.First(s => s.StateGUID == stateGuid);

                    foreach (var step in state.Steps)
                    {
                        if (IsStepDisabledVisitor.Get(testStep, testStep.Steps.First(st => st.Guid == step.StepGuid)))
                        {
                            continue;
                        }
                        HashSet<Guid> stateGuidsReachableFromThisStep = new HashSet<Guid>();
                        stateGuidsReachableFromThisStep.AddAll(step.ResultStateMap.Values);
                        if (reachableStateGuids.Intersect(stateGuidsReachableFromThisStep).Count() > 0)
                        {
                            allStateGuids.Remove(stateGuid);// can not continue foreach-collection is modified
                            reachableStateGuids.Add(stateGuid);
                            doContinue = true;
                            break;
                        }
                    }
                    if (doContinue)
                    {
                        break;
                    }
                }
            }
            while (doContinue);

            if (allStateGuids.Count > 0)
            {
                throw new Exception(string.Format("Deadlock in state machine states [{0}]", string.Join(", ", sm.States.Where(st => allStateGuids.Contains(st.StateGUID)).Select(x => x.Name).ToArray())));
            }
        }

        // if a state is marked as Finish, it should not have steps assigned. These steps would not get executed anyway
        private void CheckFinishStatesDoNotHaveSteps(StateMachineExecutionModifier sm)
        {
            if (sm.States.Where(s => s.IsFinishState).Count(finishState=>finishState.Steps.Count > 0) > 0)
            {
                throw new Exception("Steps should not be added to Finish state(s)");
            }
        }
                
        private void CheckAllStatesReachable(StateMachineExecutionModifier sm)
        {
            HashSet<Guid> allStateGuids = new HashSet<Guid>();
            allStateGuids.AddAll(sm.States.Select(x => x.StateGUID));
            
            HashSet<Guid> reachableStateGuids = new HashSet<Guid>();
            var start = sm.States.First(s => s.IsStartState);
            reachableStateGuids.Add(start.StateGUID);

            RecursiveStateWalk(sm, start, reachableStateGuids);

            var unreachable = allStateGuids.Except(reachableStateGuids);
            if(unreachable.Count() > 0)
            {
                throw new Exception(string.Format("[{0}] state(s) are not reachable", string.Join(", ", sm.States.Where(st => unreachable.Contains(st.StateGUID)).Select(x => x.Name).ToArray())));
            }
        }

        private void RecursiveStateWalk(StateMachineExecutionModifier sm, StateMachineExecutionModifier.State state, HashSet<Guid> reachableStateGuids)
        {
            foreach (var step in state.Steps){
                foreach (var res in step.ResultStateMap)
                {
                    if (reachableStateGuids.Contains(res.Value) == false)
                    {
                        reachableStateGuids.Add(res.Value);
                        RecursiveStateWalk(sm, sm.States.First(st => st.StateGUID == res.Value), reachableStateGuids);
                    }
                }
            }
        }                

        // if state machine execution method is assigned to a composite, the result handler must be of StateMachineResultHandler type
        private void ReplaceWrongResultHandler(TestStepComposite step)
        {
            if (step.CompositeResultHandler is StateMachineResultHandler == false)
            {
                step.CompositeResultHandler = new StateMachineResultHandler();
                isModified = true;
            }
        }

        //all enabled child steps should be reachable from states
        private void CheckAllStepsUsedInStates(TestStepComposite step, StateMachineExecutionModifier sm)
        {
            HashSet<Guid> allStepGuids = new HashSet<Guid>();
            allStepGuids.AddAll(step.Steps.Select(s=>s.Guid));
            HashSet<Guid> reachableStepGuids = new HashSet<Guid>();
            foreach (var state in sm.States)
            {
                reachableStepGuids.AddAll(state.Steps.Select(s=>s.StepGuid));
            }
            if (allStepGuids.SetEquals(reachableStepGuids) == false)
            {
                throw new Exception("Some steps will not be executed by state machine");
            }
        }

        private void CheckHasStartStep(StateMachineExecutionModifier sm)
        {
            if (sm.States.Count(s => s.IsStartState) > 1)
            {
                throw new Exception("State machine should only have one Start state");
            }
            if (sm.States.Count(s => s.IsStartState) != 1)
            {
                throw new Exception("State machine does not have Start state");
            }
        }

        private void CheckHasFinishStep(StateMachineExecutionModifier sm)
        {
            if (sm.States.Count(s => s.IsFinishState) <= 0)
            {
                throw new Exception("State machine does not have Finish state");
            }
        }

		private void RemoveNonExistingSteps(TestStepComposite step, StateMachineExecutionModifier sm)
        {
            foreach (var state in sm.States)
            {
                int numRemoved = state.Steps.RemoveAll(s => step.Steps.Count(childStep => childStep.Guid == s.StepGuid) == 0);
                if (numRemoved > 0)
                {
                    isModified = true;
                }
            }
        }

        private void RemoveNonExistingStateReferences(TestStepComposite step, StateMachineExecutionModifier sm)
        {
            HashSet<Guid> allStateGuids = new HashSet<Guid>();
            allStateGuids.AddAll(sm.States.Select(x => x.StateGUID));
           
            foreach (var state in sm.States)
            {
                foreach (var s in state.Steps)
                {
                    isModified |= s.ResultStateMap.RemoveKeys(s.ResultStateMap.SelectKeys(x => allStateGuids.Contains(x) == false));
                }
            }
        }
    }
}
