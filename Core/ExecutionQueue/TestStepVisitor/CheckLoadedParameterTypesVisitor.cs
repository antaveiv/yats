﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.TestCase.Parameter;

namespace yats.ExecutionQueue
{
    // When a test sequence is serialized, the test parameter type hash codes are saved. If the parameter types are changed (e.g. ranges changed, enum values removed etc), this visitor should find and invalidate such parameters
    public class CheckLoadedParameterTypesVisitor 
    {
        // returns true if parameters were updated
        public static bool Process(TestStep root, IGlobalParameterCollection globalParameters)
        {
            bool result = false;
            foreach (var p in ParameterEnumerator.GetParameters(root))
            {
                if (p.ValueTypeHashCode == null)
                {
                    continue; // was not previously saved?
                }
                if (p.Value == null)
                {
                    continue;
                }

                if (p.ValueTypeHashCode != yats.TestCase.Parameter.UpdateParamTypeHashCode.GetHashCode(p, globalParameters))
                {
#if DEBUG
                    var tmp = yats.TestCase.Parameter.UpdateParamTypeHashCode.GetHashCode(p, globalParameters);
#endif

                    p.Value = null;
                    p.ValueTypeHashCode = null;
                    result = true;
                }
            }

            return result;
        }
    }
}
