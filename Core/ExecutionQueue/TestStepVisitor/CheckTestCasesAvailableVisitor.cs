﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using yats.TestRepositoryManager.Interface;

namespace yats.ExecutionQueue
{
    // When a test sequence is serialized, only UniqueTestId is saved. After loading, the corresponding test case object has to be found and assigned again.
	// This visitor goes through the test run hierarchy and checks that all test cases are still available in test repository managers. If a missing test case is found, an exception is thrown
    public class CheckTestCasesAvailableVisitor : ITestStepVisitor
    {
        public static void Process(TestStep root, List<ITestRepositoryManager> managers)
        {
            if (root == null)
            {
                return;
            }
            var visitor = new CheckTestCasesAvailableVisitor( managers );
            root.Accept( visitor );
        }

        List<ITestRepositoryManager> managers;
        private CheckTestCasesAvailableVisitor(List<ITestRepositoryManager> managers)
        {
            this.managers = managers;
        }


        #region ITestStepVisitor Members

        public void AcceptSingle(TestStepSingle test)
        {
            if(test.TestCase == null)
            {
                foreach(var mgr in managers)
                {
                    var testCase = mgr.GetByUniqueId( test.UniqueTestId );
                    if(testCase != null) // found
                    {
                        test.TestCase = testCase;
                        test.RepositoryManager = mgr;
                        return;
                    }
                }
                throw new Exception( string.Format( "Test case is no longer available: {0}", test.UniqueTestId ) );
            }
        }

        public void AcceptComposite(TestStepComposite test)
        {
            foreach(var step in test.Steps)
            {
                step.Accept( this );
            }
        }

        #endregion
    }
}
