﻿using System;
using System.Collections.Generic;
using System.Text;

namespace yats.ExecutionQueue.ResultHandler
{
    public interface IResultHandlerVisitor
    {
        void VisitCustomResultMapping(CustomResultMapping handler);
        void VisitDefaultResultHandler(DefaultResultHandler handler);
        void VisitWorstCase(WorstCaseCompositeHandler handler);
    }
}
