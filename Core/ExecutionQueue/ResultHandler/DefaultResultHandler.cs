﻿using System;
using System.Collections.Generic;
using System.Text;

namespace yats.ExecutionQueue.ResultHandler
{
    public class DefaultResultHandler : IResultHandler
    {
        public override void Accept(IResultHandlerVisitor visitor)
        {
            visitor.VisitDefaultResultHandler( this );
        }
		
		public override string ToString ()
		{
			return "Default";
		}
    }
}
