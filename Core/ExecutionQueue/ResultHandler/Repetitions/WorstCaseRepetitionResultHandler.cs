/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;
using System.Xml.Serialization;
using yats.TestCase.Interface;
namespace yats.ExecutionQueue.ResultHandler
{
	public class WorstCaseRepetitionResultHandler : IRepetitionResultHandler
	{
		[XmlIgnore]
		ITestResult current = null;
		[XmlIgnore]
        public ITestResult Current
        {
            get { return current; }
            set { current = value; }
        }
		
		#region implemented abstract members of yats.ExecutionQueue.ResultHandler.IRepetitionResultHandler
        [DebuggerStepThrough]
		public override void Accept (IRepetitionResultVisitor visitor)
		{
			visitor.VisitWorstCaseRepetition(this);
		}
		#endregion
		
		public WorstCaseRepetitionResultHandler ()
		{
		}

        public override IRepetitionResultHandler Clone()
        {
            var res = new WorstCaseRepetitionResultHandler();
            res.current = this.current;
            return res;
        }
		
		public void Update(ITestResult result)
        {
            if(this.current == null)
            {
                this.current = result;
                return;
            }

            if(result == null)
            {
                return;
            }

            if(result.Result == ResultEnum.CANCELED)
            {
                this.current = result;
                return;
            }

            if(current.Result == result.Result)
            {
                return;//no change
            }

            if(current.Result == ResultEnum.INCONCLUSIVE && result.Result == ResultEnum.FAIL)
            {
                current = result;
                return;
            }

            if((result.Result == ResultEnum.FAIL || result.Result == ResultEnum.INCONCLUSIVE) && current.Result == ResultEnum.PASS)
            {
                current = result;
                return;
            }

            if(current.Result == ResultEnum.NOT_RUN)
            {
                current = result;
                return;
            }
        }

        public override string ToString()
        {
            return string.Format( "Current={0}", current );
        }
	}
}

