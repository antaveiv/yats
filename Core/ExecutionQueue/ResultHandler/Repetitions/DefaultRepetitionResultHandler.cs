/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;
using System.Xml.Serialization;
using yats.TestCase.Interface;
namespace yats.ExecutionQueue.ResultHandler
{
	/// <summary>
	/// NOT_RUN -> NOT_RUN; CANCELED -> CANCELED; PASS/FAIL/INCONCLUSIVE -> PASS
	/// </summary>
	public class DefaultRepetitionResultHandler : IRepetitionResultHandler
	{
		[XmlIgnore]
		public ResultEnum result = ResultEnum.NOT_RUN;
		
		#region implemented abstract members of yats.ExecutionQueue.ResultHandler.IRepetitionResultHandler
        [DebuggerStepThrough]
		public override void Accept (IRepetitionResultVisitor visitor)
		{
			visitor.VisitDefaultRepetitionResult(this);
		}		
		#endregion
		
		public DefaultRepetitionResultHandler ()
		{
		}
		
		public void Update(ResultEnum newResult)
		{
			if (this.result == newResult)
			{
				return;
			}
			
			if (this.result == ResultEnum.NOT_RUN){
                switch (newResult)
                {
                    case ResultEnum.PASS:
                    case ResultEnum.FAIL:
                    case ResultEnum.INCONCLUSIVE:
                        this.result = ResultEnum.PASS;
                        break;
                    case ResultEnum.CANCELED:
                        this.result = newResult;
                        break;
                }
				return;
			}
			
			if (newResult == ResultEnum.CANCELED)
            {
				this.result = newResult;
			} 
            else if (newResult != ResultEnum.NOT_RUN && this.result != ResultEnum.CANCELED)
            {
				this.result = ResultEnum.PASS;
			} 
		}

        public override IRepetitionResultHandler Clone()
        {
            var res = new DefaultRepetitionResultHandler();
            res.result = this.result;
            return res;
        }
	}
}

