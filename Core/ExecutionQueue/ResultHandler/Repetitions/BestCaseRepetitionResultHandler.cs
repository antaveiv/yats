﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;
using System.Xml.Serialization;
using yats.TestCase.Interface;
using System.Collections.Generic;

namespace yats.ExecutionQueue.ResultHandler
{
    /// <summary>
    /// NOT_RUN, CANCELED or best repetition result 
    /// </summary>
    public class BestCaseRepetitionResultHandler : IRepetitionResultHandler
    {
        //TODO: unit tests
		[XmlIgnore]
        ITestResult current = null;
        [XmlIgnore]
		public ITestResult Current
        {
            get { return current; }
            set { current = value; }
        }

        [DebuggerStepThrough]
        public override void Accept(IRepetitionResultVisitor visitor)
        {
            visitor.VisitBestCaseRepetition( this );
        }

        public BestCaseRepetitionResultHandler()
		{
		}

        protected BestCaseRepetitionResultHandler(ITestResult current)
        {
            this.current = current;
        }

        static Dictionary<ResultEnum, int> ResultSortOrder = new Dictionary<ResultEnum, int>();
        static BestCaseRepetitionResultHandler()
        {
            ResultSortOrder.Add(ResultEnum.NOT_RUN, 1);
            ResultSortOrder.Add(ResultEnum.CANCELED, 2);
            ResultSortOrder.Add(ResultEnum.FAIL, 3);
            ResultSortOrder.Add(ResultEnum.INCONCLUSIVE, 4);
            ResultSortOrder.Add(ResultEnum.PASS, 5);
        }

		public void Update(ITestResult result)
        {
            if(this.current == null)
            {
                this.current = result;
                return;
            }

            if(result == null)
            {
                return;
            }

            if (ResultSortOrder[result.Result] > ResultSortOrder[current.Result])
            {
                current = result;
                return;
            }

            //if(result.Result == ResultEnum.CANCELED)
            //{
            //    this.current = result;
            //    return;
            //}

            //if(current.Result == result.Result)
            //{
            //    return;//no change
            //}

            //if(current.Result == ResultEnum.FAIL && result.Result == ResultEnum.INCONCLUSIVE)
            //{
            //    // promote from FAIL to INCONCLUSIVE
            //    current = result;
            //    return;
            //}

            //if((current.Result == ResultEnum.FAIL || current.Result == ResultEnum.INCONCLUSIVE) && result.Result == ResultEnum.PASS)
            //{
            //    //promote to PASS
            //    current = result;
            //    return;
            //}

            //if(current.Result == ResultEnum.NOT_RUN)
            //{
            //    //promote from NOT_RUN to anything else
            //    current = result;
            //    return;
            //}
        }

        public override IRepetitionResultHandler Clone()
        {
            return new BestCaseRepetitionResultHandler(current);
        }
    }
}
