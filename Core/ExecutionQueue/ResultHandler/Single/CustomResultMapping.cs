﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using yats.TestCase.Interface;

namespace yats.ExecutionQueue.ResultHandler
{
    public class CustomResultMapping : ISingleResultHandler
    {
        public CustomResultMapping()
        {
        }

        public CustomResultMapping(ResultEnum valueCanceled, ResultEnum valuePass, ResultEnum valueFail, ResultEnum valueInconclusive, ResultEnum valueNotRun)
        {
            this.valueCanceled = valueCanceled;
            this.valuePass = valuePass;
            this.valueFail = valueFail;
            this.valueInconclusive = valueInconclusive;
            this.valueNotRun = valueNotRun;
        }

		protected ResultEnum valueCanceled = ResultEnum.CANCELED;
        public ResultEnum ValueCanceled { 
			get {return valueCanceled;} 
			set {valueCanceled = value;}
		}
		
		protected ResultEnum valuePass = ResultEnum.PASS;
        public ResultEnum ValuePass { 
			get {return valuePass;} 
			set {valuePass = value;}
		}
		
		protected ResultEnum valueFail = ResultEnum.FAIL;
        public ResultEnum ValueFail { 
			get {return valueFail;} 
			set {valueFail = value;}
		}
		
		protected ResultEnum valueInconclusive = ResultEnum.INCONCLUSIVE;
        public ResultEnum ValueInconclusive { 
			get {return valueInconclusive;} 
			set {valueInconclusive = value;}
		}
		
		protected ResultEnum valueNotRun = ResultEnum.NOT_RUN;
        public ResultEnum ValueNotRun { 
			get {return valueNotRun;} 
			set {valueNotRun = value;}
		}
		
        public ResultEnum Convert(ResultEnum value)
        {
            switch(value)
            {
                case ResultEnum.CANCELED:
                    return ValueCanceled;
                case ResultEnum.PASS:
                    return ValuePass;
                case ResultEnum.FAIL:
                    return ValueFail;
                case ResultEnum.INCONCLUSIVE:
                    return ValueInconclusive;
				case ResultEnum.NOT_RUN:
                    return ValueNotRun;
                default:
                    throw new Exception( "Invalid input" );
            }
        }

        [DebuggerStepThrough]
        public override void Accept(ISingleResultVisitor visitor)
        {
            visitor.VisitCustomResultMapping( this );
        }

        [DebuggerStepThrough]
		public override string ToString ()
		{
			return string.Format ("CustomResultMapping");
		}

        public override ISingleResultHandler Clone()
        {
            return new CustomResultMapping(this.valueCanceled, this.valuePass, this.valueFail, this.valueInconclusive, this.valueNotRun);
        }
    }
}
