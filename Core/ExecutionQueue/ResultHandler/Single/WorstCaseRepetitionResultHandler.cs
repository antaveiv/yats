using System;
namespace yats.ExecutionQueue.ResultHandler
{
	public class WorstCaseRepetitionResultHandler : IRepetitionResultHandler
	{
		#region implemented abstract members of yats.ExecutionQueue.ResultHandler.IRepetitionResultHandler
		public override void Accept (IRepetitionResultVisitor visitor)
		{
			visitor.VisitWorstCaseRepetition(this);
		}
		#endregion
		
		public WorstCaseRepetitionResultHandler ()
		{
		}
	}
}

