using System;
using System.Xml.Serialization;
namespace yats.ExecutionQueue.ResultHandler
{
	[XmlInclude(typeof(CustomResultMapping))]
	[XmlInclude(typeof(DefaultResultHandler))]
	[XmlInclude(typeof(IgnoreFailure))]
	[XmlInclude(typeof(WorstCaseCompositeHandler))]
	public abstract class IResultHandler
	{
        public abstract void Accept(IResultHandlerVisitor visitor);
	}
}

