﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace yats.ExecutionQueue.ResultHandler
{
    public class SelectedStepResultHandler : ICompositeResultHandler
    {
        protected List<Guid> stepIdsToEvaluate = new List<Guid>();
        public List<Guid> StepIdsToEvaluate
        {
            get { return stepIdsToEvaluate; }
            set { stepIdsToEvaluate = value; }
        }

        public SelectedStepResultHandler()
		{
		}

        public SelectedStepResultHandler(List<Guid> stepIdsToEvaluate)
		{
            this.stepIdsToEvaluate = stepIdsToEvaluate;
		}

        public override ICompositeResultHandler Clone()
        {
            List<Guid> guidClone = new List<Guid>(stepIdsToEvaluate.Count);
            foreach (var g in stepIdsToEvaluate)
            {
                guidClone.Add(new Guid(g.ToByteArray()));
            }
            return new SelectedStepResultHandler(guidClone);
        }
        
        [DebuggerStepThrough]
        public override void Accept(ICompositeResultVisitor visitor)
        {
            visitor.VisitSelectedSteps(this);
        }
    }
}
