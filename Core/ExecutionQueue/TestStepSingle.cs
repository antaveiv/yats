/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Serialization;
using yats.ExecutionQueue.ResultHandler;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.Interface;
	
namespace yats.ExecutionQueue
{
	public class TestStepSingle : TestStep
	{
		[XmlIgnore]
		protected ITestRepositoryManager repositoryManager;
		[XmlIgnore]
		public ITestRepositoryManager RepositoryManager
		{
            [DebuggerStepThrough]
			get {return repositoryManager;}
            [DebuggerStepThrough]
			set {
                repositoryManager = value;
                if(testCase != null && repositoryManager != null)
                {
                    m_uniqueTestId = repositoryManager.GetUniqueTestId( testCase );
                }
            }
		}
		
		[XmlIgnore]
		protected ITestCase testCase;
		[XmlIgnore]
		public ITestCase TestCase
        {
            [DebuggerStepThrough]
			get {return testCase;}
            [DebuggerStepThrough]
			set {
                testCase = value;
                if(testCase != null && repositoryManager != null)
                {
                    m_uniqueTestId = repositoryManager.GetUniqueTestId( testCase );    
                }                
            }
		}
		
		[XmlIgnore]
		protected List<IParameter> parameters;
		public override List<IParameter> Parameters
		{
            [DebuggerStepThrough]
			get {return parameters;}
            [DebuggerStepThrough]
			set {parameters = value;}
		}
        		
		protected ISingleResultHandler singleRunResultHandler = new DefaultSingleResultHandler();
		/// <summary>
		/// Transforms the test result on every test step run (called N times if N repetitions of the step are done).
        /// Set to DefaultSingleResultHandler, CustomResultMapping, IgnoreFailureSingleResultHandler
		/// </summary>
        public ISingleResultHandler SingleRunResultHandler
		{
            [DebuggerStepThrough]
			get {return singleRunResultHandler;}
            [DebuggerStepThrough]
			set {singleRunResultHandler = value;}
		}

		protected IRepetitionResultHandler repetitionResultHandler = new DefaultRepetitionResultHandler();
        /// <summary>
        /// DefaultRepetitionResultHandler or WorstCaseRepetitionResultHandler
        /// </summary>
		public override IRepetitionResultHandler RepetitionResultHandler
		{
            [DebuggerStepThrough]
			get {return repetitionResultHandler;}
            [DebuggerStepThrough]
			set {repetitionResultHandler = value;}
		}

        private string m_uniqueTestId;
		public string UniqueTestId
		{
            [DebuggerStepThrough]
			get 
			{
				return m_uniqueTestId;
			}
            [DebuggerStepThrough]
			set 
            {
                m_uniqueTestId = value;
			}
		}

		public TestStepSingle ()
		{
			this.ExecutionMethod = new SequentialExecutionModifier();
		}

        [DebuggerStepThrough]
		public TestStepSingle (ITestCase testCase, ITestRepositoryManager repositoryManager) : this()
		{
			this.repositoryManager = repositoryManager;
			this.testCase = testCase;
			this.parameters = new List<IParameter>(repositoryManager.GetParameters(testCase));
            this.m_uniqueTestId = repositoryManager.GetUniqueTestId(testCase);
            this.name = repositoryManager.GetTestName( testCase );
		}

        [DebuggerStepThrough]
		public TestStepSingle (string testCaseUniqueId, ITestRepositoryManagerCollection testRepository) : this()
		{
			this.repositoryManager = testRepository.GetManagerByTestUniqueId(testCaseUniqueId);
			this.testCase = testRepository.GetByUniqueId(testCaseUniqueId);
            this.parameters = new List<IParameter>(repositoryManager.GetParameters(testCase));
            this.m_uniqueTestId = testCaseUniqueId;
            this.name = repositoryManager.GetTestName( testCase );
		}
        
        [DebuggerStepThrough]
		public override void Accept(ITestStepVisitor visitor)
		{
			visitor.AcceptSingle(this);
		}

        [DebuggerStepThrough]
        public override string ToString()
        {
            return "Single " + Name;
        }

        public override TestStep Clone()
        {
            TestStepSingle result = new TestStepSingle();
            if (this.decisionHandler != null)
            {
                result.decisionHandler = this.decisionHandler.Clone();
            }
            if (this.executionMethod != null)
            {
                result.executionMethod = this.executionMethod.Clone();
            }
            if (this.parameters != null)
            {
                result.parameters = new List<IParameter>();
                foreach (var param in this.parameters)
                {
                    result.parameters.Add(param.Clone());
                }
            }
            if (this.repetitionResultHandler != null)
            {
                result.repetitionResultHandler = this.repetitionResultHandler.Clone();
            }
            if (this.repositoryManager != null)
            {
                result.repositoryManager = this.repositoryManager;
                result.testCase = repositoryManager.GetByUniqueId(m_uniqueTestId); // same as clone
            }
            if (this.result != null)
            {
                result.result = this.result.Clone();
            }
            if (this.singleRunResultHandler != null)
            {
                result.singleRunResultHandler = this.singleRunResultHandler.Clone();
            }            
            result.m_uniqueTestId = this.m_uniqueTestId;
            if(this.name != null)
            {
                result.name = this.name;
            }
            return result;
        }
	}
}

