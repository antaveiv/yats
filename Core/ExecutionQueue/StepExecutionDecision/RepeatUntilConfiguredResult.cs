﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Serialization;
using yats.TestCase.Interface;

namespace yats.ExecutionQueue.DecisionHandler
{
    /// <summary>
    /// Repetition method: specify what result(s) to wait for. Will repeat execution until the desired result is obtained.
    /// </summary>
    public class RepeatUntilConfiguredResult : IDecisionHandler
    {
        //TODO: unit tests

        protected List<ResultEnum> waitFor = new List<ResultEnum>( );
        public List<ResultEnum> WaitFor
        {
            [DebuggerStepThrough]
            get { return waitFor; }
            [DebuggerStepThrough]
            set { waitFor = value; }
        }

        protected int repetitions = 1;
        public int Repetitions
        {
            [DebuggerStepThrough]
            get { return repetitions; }
            [DebuggerStepThrough]
            set { repetitions = value; }
        }

		[XmlIgnore]
        protected int repetitionsDone = 0;
		[XmlIgnore]
        public int RepetitionsDone
        {
            [DebuggerStepThrough]
            get { return repetitionsDone; }
            [DebuggerStepThrough]
            set { repetitionsDone = value; }
        }

		[XmlIgnore]
        protected bool finished = false;
		[XmlIgnore]
        public bool Finished
        {
            [DebuggerStepThrough]
            get { return finished; }
            [DebuggerStepThrough]
            set { finished = value; }
        }

        [DebuggerStepThrough]
        public RepeatUntilConfiguredResult()
        {
        }

        [DebuggerStepThrough]
        public RepeatUntilConfiguredResult(int repetitions, ResultEnum waitFor) : this()
        {
            this.repetitions = repetitions;
            this.waitFor.Add( waitFor );
        }

        [DebuggerStepThrough]
        public RepeatUntilConfiguredResult(int repetitions, List<ResultEnum> waitFor)
            : this()
        {
            this.repetitions = repetitions;
            this.waitFor = waitFor;
        }

        public override IDecisionHandler Clone()
        {
            var res = new RepeatUntilConfiguredResult();
            res.finished = this.finished;
            res.repetitions = this.repetitions;
            res.repetitionsDone = this.repetitionsDone;
            res.waitFor.AddRange(this.waitFor);
            return res;
        }

        [DebuggerStepThrough]
        public override void Accept(IDecisionHandlerVisitor visitor)
        {
            visitor.VisitRepeatUntilConfiguredResult( this );
        }
                
        public void Reset()
        {
            this.finished = false;
            this.repetitionsDone = 0;
        }
    }
}
