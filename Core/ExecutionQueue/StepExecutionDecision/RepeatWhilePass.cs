﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;
using System.Xml.Serialization;

namespace yats.ExecutionQueue.DecisionHandler
{
    public class RepeatWhilePass : IDecisionHandler
    {
        protected int repetitions = 1;
		public int Repetitions
		{
            [DebuggerStepThrough]
			get {return repetitions;}
            [DebuggerStepThrough]
			set {repetitions = value;}
		}

		[XmlIgnore]
		protected int repetitionsDone = 0;
		[XmlIgnore]
		public int RepetitionsDone
		{
            [DebuggerStepThrough]
			get {return repetitionsDone;}
            [DebuggerStepThrough]
			set {repetitionsDone = value;}
		}
       
		[XmlIgnore]
		protected int numPassResults = 0;
		[XmlIgnore]
		public int NumPassResults
        {
            [DebuggerStepThrough]
			get {return numPassResults;}
            [DebuggerStepThrough]
			set {numPassResults = value;}
		}
	 
		public RepeatWhilePass()
		{
		}
		
		public RepeatWhilePass(int repetitions)
        {
			this.repetitions = repetitions;
		}

        [DebuggerStepThrough]
        public override void Accept(IDecisionHandlerVisitor visitor)
        {
            visitor.VisitRepeatWhilePass( this );
        }

        public void Reset()
        {
            repetitionsDone = numPassResults = 0;
        }

        [DebuggerStepThrough]
		public override string ToString ()
		{
			return string.Format ("Repeat while pass {0}", Repetitions);
		}

        public override IDecisionHandler Clone()
        {
            var res = new RepeatWhilePass();
            res.numPassResults = this.numPassResults;
            res.repetitions = this.repetitions;
            res.repetitionsDone = this.repetitionsDone;
            return res;
        }
    }
}
