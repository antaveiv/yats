/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

namespace yats.ExecutionQueue.DecisionHandler
{
    /// DecisionHandler namespace contains classes that are involved in the "Should the test step be run"
    /// decision. Currently there are two strategies implemented: single step run (SingleRun)
    /// and RepetitionHandler (execute tests a specified number of times unless the test does not pass).
    /// The following methods are defined via visitors:
    /// BeforeRun initializes the test step by resetting counters
    /// AfterRun prepares the step for the next decision by incrementing counters. It takes into account the
    /// result of a recently executed test
    /// Decide returns the next test step decision: either REPEAT or FINISHED

	public abstract class IDecisionHandler
	{
        public abstract void Accept(IDecisionHandlerVisitor visitor);

        public abstract IDecisionHandler Clone();
    }
}

