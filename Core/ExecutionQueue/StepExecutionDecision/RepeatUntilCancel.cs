﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;
using System.Xml.Serialization;

namespace yats.ExecutionQueue.DecisionHandler
{
    public class RepeatUntilCancel : IDecisionHandler
    {
		[XmlIgnore]
        private bool m_isCanceled = false;
		[XmlIgnore]
        public bool Canceled
        {
            [DebuggerStepThrough]
            get { return m_isCanceled; }
            [DebuggerStepThrough]
            set { m_isCanceled = value; }
        }

        [DebuggerStepThrough]
        public override void Accept(IDecisionHandlerVisitor visitor)
        {
            visitor.VisitRepeatUntilCancel( this );
        }

        public void Reset()
        {
            m_isCanceled = false;
        }

        [DebuggerStepThrough]
		public override string ToString ()
		{
			return "Until cancel";
		}

        public RepeatUntilCancel()
        {
        }

        protected RepeatUntilCancel(bool canceled)
        {
            this.m_isCanceled = canceled;
        }

        public override IDecisionHandler Clone()
        {
            return new RepeatUntilCancel(this.m_isCanceled);
        }
    }
}
