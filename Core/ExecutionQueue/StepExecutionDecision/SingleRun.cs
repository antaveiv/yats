﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;
using System.Xml.Serialization;

namespace yats.ExecutionQueue.DecisionHandler
{
    /// <summary>
    /// single run
    /// </summary>
    public class SingleRun : IDecisionHandler
    {
		[XmlIgnore]
        protected bool executed = false;
		[XmlIgnore]
        public bool Executed
        {
            [DebuggerStepThrough]
            get { return executed; }
            [DebuggerStepThrough]
            set { executed = value; }
        }

        [DebuggerStepThrough]
        public override void Accept(IDecisionHandlerVisitor visitor)
        {
            visitor.VisitSingleRun( this );
        }

        public void Reset()
        {
            executed = false;
        }

        [DebuggerStepThrough]
		public override string ToString ()
		{
			return "Default";
		}

        public override IDecisionHandler Clone()
        {
            var res = new SingleRun();
            res.executed = this.executed;
            return res;
        }
    }
}
