/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Serialization;
using yats.ExecutionQueue.CompositeStepExecuteDecision;
using yats.ExecutionQueue.ResultHandler;
using yats.TestCase.Parameter;

namespace yats.ExecutionQueue
{
	public class TestStepComposite : TestStep
	{
		protected List<TestStep> steps;
		public List<TestStep> Steps
		{
            [DebuggerStepThrough]
			get {return steps;}
            [DebuggerStepThrough]
			set {steps = value;}
		}
		
		protected ICompositeDecision stepDecisionHandler;
        /// <summary>
        /// ExecuteUntilCanceled or ExecuteWhilePassing
        /// </summary>
		public ICompositeDecision StepDecisionHandler
		{
            [DebuggerStepThrough]
			get {
				if (stepDecisionHandler == null){
					stepDecisionHandler = new ExecuteUntilCanceled();
				}
				return stepDecisionHandler;
			}
            [DebuggerStepThrough]
			set {stepDecisionHandler = value;}
		}
		
		/// <summary>
        /// Determines the overall result from multiple step repetitions. DefaultRepetitionResultHandler, WorstCaseRepetitionResultHandler, BestCaseRepetitionResultHandler
		/// </summary>
		protected IRepetitionResultHandler repetitionResultHandler = new DefaultRepetitionResultHandler();
		public override IRepetitionResultHandler RepetitionResultHandler
		{
            [DebuggerStepThrough]
			get {return repetitionResultHandler;}
            [DebuggerStepThrough]
			set {repetitionResultHandler = value;}
		}
		
		/// <summary>
        /// Determines the composite test result from its child step results. DefaultCompositeResultHandler, WorstCaseCompositeResultHandler or StateMachineResultHandler
		/// </summary>
		protected ICompositeResultHandler compositeResultHandler = new DefaultCompositeResultHandler();
		public ICompositeResultHandler CompositeResultHandler
		{
            [DebuggerStepThrough]
			get {return compositeResultHandler;}
            [DebuggerStepThrough]
			set {compositeResultHandler = value;}
		}

        [XmlIgnore]
        protected List<IParameter> parameters;
        public override List<IParameter> Parameters
        {
            [DebuggerStepThrough]
            get { return parameters; }
            [DebuggerStepThrough]
            set { parameters = value; }
        }

        [DebuggerStepThrough]
		public TestStepComposite ()
		{
			this.steps = new List<TestStep>();
            this.name = "Group";
		}

        [DebuggerStepThrough]
		public TestStepComposite (List<TestStep> steps)
		{
			this.steps = steps;
		}

        [DebuggerStepThrough]
		public override void Accept(ITestStepVisitor visitor)
		{
			visitor.AcceptComposite(this);
		}

        [DebuggerStepThrough]
        public override string ToString()
        {
            return string.Format("Composite {0}, {1} steps", Name, steps.Count);
        }

        public override TestStep Clone()
        {
            TestStepComposite result = new TestStepComposite();
            if (this.executionMethod != null)
            {
                result.executionMethod = this.executionMethod.Clone();
            }
            if (this.compositeResultHandler != null)
            {
                result.compositeResultHandler = this.compositeResultHandler.Clone();
            }
            if (this.decisionHandler != null)
            {
                result.decisionHandler = this.decisionHandler.Clone();
            }
            if (this.parameters != null)
            {
                result.parameters = new List<IParameter>();
                foreach (var param in this.parameters)
                {
                    result.parameters.Add(param.Clone());
                }
            }
            if (this.repetitionResultHandler != null)
            {
                result.repetitionResultHandler = this.repetitionResultHandler.Clone();
            }
            if (this.result != null)
            {
                result.result = this.result.Clone();
            }
            if (this.stepDecisionHandler != null)
            {
                result.stepDecisionHandler = this.stepDecisionHandler.Clone();
            }
            if (this.steps != null)
            {
                result.steps = new List<TestStep>();
                foreach (var step in this.steps)
                {
                    result.steps.Add(step.Clone());
                }
            }
            result.Name = this.Name;
            return result;
        }
	}
}

