﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;

namespace yats.TestRun.Cancel
{
    public static class CancelHandlerCollection
    {
        private static List<CancelHandler> s_cancelHandlerList = new List<CancelHandler>();

        public static void Cancel (object sender, string reason)
        {
            lock (s_cancelHandlerList)
            {
                List<CancelHandler> copy = new List<CancelHandler>(s_cancelHandlerList);

                foreach (var handler in copy)
                {
                handler.Cancel (sender, reason);
                }
            }
        }

        public static void Add(CancelHandler handler)
        {
            lock (s_cancelHandlerList)
            {
                s_cancelHandlerList.Add(handler);
            }
        }

        public static void Remove(CancelHandler handler)
        {
            lock (s_cancelHandlerList)
            {
                s_cancelHandlerList.Remove(handler);
            }
        }
    }
}
