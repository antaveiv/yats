﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using log4net;
using System;

namespace yats.TestRun.Cancel
{
    public class CancelEventArgs : EventArgs
        {
        public string Reason;
        public CancelEventArgs (string reason)
            {
            Reason = reason;
            }
        public override string ToString ()
            {
            return Reason;
            }
        }

    public delegate void CancelEventDelegate(object reason);

    public class CancelHandler
    {
        public CancelHandler()
        {
            CancelHandlerCollection.Add(this);
        }

        public void Cancel(object sender, string reason)
        {
            if (isCanceled)
            {
                //return;
            }
            LogManager.GetLogger(this.GetType()).ErrorFormat("Canceled: {0}", reason);
            isCanceled = true;
            var copy = OnCancel;
            if (copy != null)
            {
                copy(sender, new CancelEventArgs(reason));
            }
            CancelHandlerCollection.Remove(this);
        }

        public event EventHandler<CancelEventArgs> OnCancel;

        protected bool isCanceled = false;
        public bool IsCanceled
        {
            get { return isCanceled; }
        }

        public void Reset()
        {
            if (isCanceled == false)
            {
                return;
            }
            CancelHandlerCollection.Add(this);
            isCanceled = false;
        }
    }
}
