﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.ExecutionQueue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using yats.Utilities;
using yats.Utilities.Commands;

namespace yats.TestRun.Commands
{
    //TODO: merge with RemoveSteps
    public class InsertSteps : ICommand
    {
        protected readonly ITestRun mTestRun;
        protected readonly IList<TestStep> mStepsToInsert;
        protected readonly InsertPosition mInsertPosition;
        protected TestStep mInsertTo;
        protected readonly bool mNewTestSteps;
        protected readonly Action<TestStepComposite, IEnumerable<TestStep>, int, bool> mAfterStepsInserted;
        protected readonly Action<TestStepComposite, IEnumerable<TestStep>> mAfterStepsRemoved;

        // saved state
        protected TestStepComposite mParent;
        protected int mIndex;

        public InsertSteps(ITestRun testRun, TestStep stepsToInsert, InsertPosition insertPosition, TestStep insertTo, bool newTestSteps, Action<TestStepComposite, IEnumerable<TestStep>, int, bool> afterStepsInserted, Action<TestStepComposite, IEnumerable<TestStep>> afterStepsRemoved)
            : this(testRun, stepsToInsert.ToSingleItemList(), insertPosition, insertTo, newTestSteps, afterStepsInserted, afterStepsRemoved)
        {
        }

        public InsertSteps(ITestRun testRun, IList<TestStep> stepsToInsert, InsertPosition insertPosition, TestStep insertTo, bool newTestSteps, Action<TestStepComposite, IEnumerable<TestStep>, int, bool> afterStepsInserted, Action<TestStepComposite, IEnumerable<TestStep>> afterStepsRemoved)
        {
            mTestRun = testRun;
            mStepsToInsert = stepsToInsert;
            mInsertPosition = insertPosition;
            mNewTestSteps = newTestSteps;
            mAfterStepsInserted = afterStepsInserted;
            mAfterStepsRemoved = afterStepsRemoved;
            mInsertTo = insertTo;

        }

        public virtual bool Execute()
        {
            if (mTestRun.TestSequence is TestStepComposite == false)
            {
                return false; // can not add to a single step
            }
            if (mStepsToInsert.IsNullOrEmpty())
            {
                return false;
            }

            if (mInsertPosition == InsertPosition.Inside && mStepsToInsert.Contains(mInsertTo) == false && mInsertTo is TestStepComposite)
            {
                // dropped into a group node - just append                
                mInsertTo = mParent = (mInsertTo as TestStepComposite);
                mIndex = mParent.Steps.Count;
            }
            else
            {
                mParent = GetParentVisitor.Get(mTestRun.TestSequence, mInsertTo) ?? mTestRun.TestSequence as TestStepComposite;

                mIndex = mParent.Steps.IndexOf(mInsertTo);
                if (mIndex < 0)
                {
                    mIndex = mParent.Steps.Count;
                }
                else if (mInsertPosition != InsertPosition.Before)
                {
                    mIndex++;
                }
                mInsertTo = mParent;
            }
            
            mParent.Steps.InsertRange(mIndex, mStepsToInsert);

            if (mAfterStepsInserted != null)
            {
                mAfterStepsInserted(mParent, mStepsToInsert, mIndex, mNewTestSteps);
            }
            return true;
        }

        public virtual bool Undo()
        {
            if (mParent == null || mStepsToInsert.IsNullOrEmpty())
            {
                return false;
            }
            mParent.Steps.Remove(mStepsToInsert);

            if (mAfterStepsRemoved != null)
            {
                mAfterStepsRemoved(mParent, mStepsToInsert);
            }
            return true;
        }

        public enum InsertPosition
        {
            Inside = 0,
            Before = 1,
            After = 2,
        }
    }
}
