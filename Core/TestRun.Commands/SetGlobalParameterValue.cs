﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.TestCase.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using yats.Utilities.Commands;

namespace yats.TestRun.Commands
{
    public class SetGlobalParameterValue : ICommand
    {
        private GlobalValue m_parameter;
        private AbstractParameterValue m_newValue;
        private AbstractParameterValue m_oldValue;
        private Action m_onTestModify;
        private IGlobalParameterCollection m_globalParameters;

        public SetGlobalParameterValue(AbstractParameterValue parameter, AbstractParameterValue value, IGlobalParameterCollection globalParameters) :this (parameter, value, globalParameters, null){ }

        public SetGlobalParameterValue(AbstractParameterValue parameter, AbstractParameterValue value, IGlobalParameterCollection globalParameters, Action onTestModify) 
        {
            m_newValue = value;
            m_globalParameters = globalParameters;

            m_parameter = parameter as GlobalValue;
            if (m_parameter != null)
            {
                m_oldValue = m_globalParameters.Get(m_parameter).Value;    
            }            

            m_onTestModify = onTestModify;
        }

        public static SetGlobalParameterValue Create(AbstractParameterValue parameter, AbstractParameterValue value, IGlobalParameterCollection globalParameters, Action onTestModify)
        {
            if (parameter is GlobalValue)
            {
                return new SetGlobalParameterValue(parameter, value, globalParameters, onTestModify);
            } 
            return null;
        }

        public bool Execute()
        {
            if (m_globalParameters == null || m_parameter == null)
            {
                return false;
            }

            try
            {
                if (m_parameter == null)
                {
                    return false;
                }
                GlobalParameter existingGlobal = m_globalParameters.Get(m_parameter);
                if (existingGlobal == null)
                {
                    return false;
                }
                existingGlobal.Value = m_newValue;
                m_globalParameters.Managers[m_parameter.StorageId].Set(m_parameter.ParameterID, existingGlobal);
                if (m_onTestModify != null)
                {
                    m_onTestModify();
                }
                return true;
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(GetType()).Error("Set global value", ex);
            }

            return false;
        }

        public bool Undo()
        {
            if (m_globalParameters == null || m_parameter == null)
            {
                return false;
            }

            try
            {
                if (m_parameter == null)
                {
                    return false;
                }
                GlobalParameter existingGlobal = m_globalParameters.Get(m_parameter);
                if (existingGlobal == null)
                {
                    return false;
                }
                existingGlobal.Value = m_oldValue;
                m_globalParameters.Managers[m_parameter.StorageId].Set(m_parameter.ParameterID, existingGlobal);
                if (m_onTestModify != null)
                {
                    m_onTestModify();
                }
                return true;
            }
            catch (Exception ex)
            {
                log4net.LogManager.GetLogger(GetType()).Error("Set global value", ex);
            }

            return false;
        }
    }
}
