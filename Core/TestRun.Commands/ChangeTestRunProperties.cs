﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Utilities.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace yats.TestRun.Commands
{
    public class ChangeTestRunProperties : ICommand
    {
        private ITestRun m_testRun;
        private string m_newName;
        private string m_newDescription;

        private string m_oldName;
        private string m_oldDescription;

        public ChangeTestRunProperties(ITestRun testRun, string newName, string newDescription)
        {
            m_testRun = testRun;
            m_newName = newName;
            m_newDescription = newDescription;
            m_oldName = testRun.Name;
            m_oldDescription = testRun.Description;
        }

        public bool Execute()
        {
            m_testRun.Name = m_newName;
            m_testRun.Description = m_newDescription;
            return true;
        }

        public bool Undo()
        {
            m_testRun.Name = m_oldName;
            m_testRun.Description = m_oldDescription;
            return true;
        }
    }
}
