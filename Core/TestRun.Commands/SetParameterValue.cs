﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.TestCase.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using yats.Utilities.Commands;

namespace yats.TestRun.Commands
{
    //TODO: separate to simple and global parameter commands. not much code reuse
    public class SetParameterValue : ICommand
    {
        private IParameter m_parameter;
        private AbstractParameterValue m_newValue;
        private AbstractParameterValue m_oldValue;
        private Action m_onTestModify;

        public SetParameterValue(IParameter parameter, AbstractParameterValue newValue) : this(parameter, newValue, null) { }
        public SetParameterValue(IParameter parameter, AbstractParameterValue newValue, Action onTestModify)
        {
            m_parameter = parameter;
            m_newValue = newValue;
            if (m_parameter != null)
            {
                m_oldValue = m_parameter.Value;
            }
            m_onTestModify = onTestModify;
        }

        private GlobalParameter m_removedGlobalValue;
        private IGlobalParameters m_globalParameters;
        private Guid m_parameterGuid;

        public SetParameterValue(IParameter parameter, AbstractParameterValue newValue, Guid id, GlobalParameter removedGlobalValue, IGlobalParameters globalParameters) :
            this(parameter, newValue)
        {
            m_removedGlobalValue = removedGlobalValue;
            m_globalParameters = globalParameters;
            m_parameterGuid = id;
        }

        public bool Execute()
        {
            if (m_parameter == null)
            {
                return false;
            }
            m_parameter.Value = m_newValue;
            if (m_onTestModify != null)
            {
                m_onTestModify();
            }
            return true;
        }

        public bool Undo()
        {
            if (m_parameter == null)
            {
                return false;
            }
            if (m_globalParameters != null && m_removedGlobalValue != null && m_globalParameters.Parameters.ContainsKey(m_parameterGuid) == false) {
                m_globalParameters.Add(m_parameterGuid, m_removedGlobalValue);
            }
            
            m_parameter.Value = m_oldValue;
            if (m_onTestModify != null)
            {
                m_onTestModify();
            }
            return true;
        }
    }
}
