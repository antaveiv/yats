﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.ExecutionQueue;
using yats.ExecutionQueue.CompositeStepExecuteDecision;
using yats.ExecutionQueue.DecisionHandler;
using yats.ExecutionQueue.ResultHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using yats.Utilities.Commands;

namespace yats.TestRun.Commands
{
    public class ChangeStepProperties : ICommand
    {
        private TestStep m_copyFrom;
        IEnumerable<TestStep> m_applyTo;
        private Dictionary<TestStep, IDecisionHandler> m_oldDecisionHandlers = new Dictionary<TestStep, IDecisionHandler>();
        private Dictionary<TestStep, IStepExecution> m_oldStepExecution = new Dictionary<TestStep, IStepExecution>();
        private Dictionary<TestStep, IRepetitionResultHandler> m_oldRepetitionResultHandlers = new Dictionary<TestStep, IRepetitionResultHandler>();

        private Dictionary<TestStep, ICompositeResultHandler> m_oldCompositeResultHandlers = new Dictionary<TestStep, ICompositeResultHandler>();
        private Dictionary<TestStep, ICompositeDecision> m_oldCompositeDecisionHandlers = new Dictionary<TestStep, ICompositeDecision>();
        private Dictionary<TestStep, ISingleResultHandler> m_oldSingleResultHandlers = new Dictionary<TestStep, ISingleResultHandler>();

        private Action m_onTestModify;

        public ChangeStepProperties(TestStep copyFrom, IEnumerable<TestStep> applyTo, Action onTestModify)
        {
            m_copyFrom = copyFrom;
            m_applyTo = applyTo;
            m_onTestModify = onTestModify;

            foreach (var step in applyTo)
            {
                m_oldDecisionHandlers.Add(step, step.DecisionHandler);
                m_oldStepExecution.Add(step, step.ExecutionMethod);
                m_oldRepetitionResultHandlers.Add(step, step.RepetitionResultHandler);
                if (step is TestStepComposite)
                {
                    m_oldCompositeResultHandlers.Add(step, (step as TestStepComposite).CompositeResultHandler);
                    m_oldCompositeDecisionHandlers.Add(step, (step as TestStepComposite).StepDecisionHandler);
                }
                else if (step is TestStepSingle)
                {
                    m_oldSingleResultHandlers.Add(step, (step as TestStepSingle).SingleRunResultHandler);
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
        }

        public bool Execute()
        {
            bool modified = false;
            foreach (var step in m_applyTo)
            {
                var clone = m_copyFrom.Clone();
                step.DecisionHandler = clone.DecisionHandler;
                step.ExecutionMethod = clone.ExecutionMethod;
                step.RepetitionResultHandler = clone.RepetitionResultHandler;
                if (step is TestStepComposite)
                {
                    (step as TestStepComposite).CompositeResultHandler = (clone as TestStepComposite).CompositeResultHandler;
                    (step as TestStepComposite).StepDecisionHandler = (clone as TestStepComposite).StepDecisionHandler;
                }
                else if (step is TestStepSingle)
                {
                    (step as TestStepSingle).SingleRunResultHandler = (clone as TestStepSingle).SingleRunResultHandler;
                }
                else
                {
                    throw new NotImplementedException();
                }
                modified = true;
            }
            if (modified && m_onTestModify != null)
            {
                m_onTestModify();
            }
            return modified;
        }

        public bool Undo()
        {
            bool modified = false;
            foreach (var step in m_applyTo)
            {
                step.DecisionHandler = m_oldDecisionHandlers[step];
                step.ExecutionMethod = m_oldStepExecution[step];
                step.RepetitionResultHandler = m_oldRepetitionResultHandlers[step];
                if (step is TestStepComposite)
                {
                    (step as TestStepComposite).CompositeResultHandler = m_oldCompositeResultHandlers[step];
                    (step as TestStepComposite).StepDecisionHandler = m_oldCompositeDecisionHandlers[step];
                }
                else if (step is TestStepSingle)
                {
                    (step as TestStepSingle).SingleRunResultHandler = m_oldSingleResultHandlers[step];
                }
                else
                {
                    throw new NotImplementedException();
                }
                modified = true;
            }
            if (modified && m_onTestModify != null)
            {
                m_onTestModify();
            }
            return modified;
        }
    }
}
