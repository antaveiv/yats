﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.ExecutionQueue;
using yats.Utilities.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using yats.Utilities;

namespace yats.TestRun.Commands
{
    public class RemoveSteps : ICommand
    {
        ITestRun mTestRun;
        IEnumerable<TestStep> mStepsToRemove;
        Action<TestStepComposite, IEnumerable<TestStep>> mOnStepsRemoved;
        Action<TestStepComposite, IEnumerable<TestStep>, int, bool> mOnStepsAdded;
        
        private struct HistoryItem
        {
            public TestStep RemovedStep;
            public TestStepComposite Parent;
            public int IndexInParent;
        }
        List<HistoryItem> mRemovedSteps = new List<HistoryItem>();

        public RemoveSteps(ITestRun testRun, IEnumerable<TestStep> stepsToRemove, Action<TestStepComposite, IEnumerable<TestStep>> onStepsRemoved, Action<TestStepComposite, IEnumerable<TestStep>, int, bool> onStepsAdded)
        {
            mTestRun = testRun;
            mStepsToRemove = stepsToRemove;
            mOnStepsRemoved = onStepsRemoved;
            mOnStepsAdded = onStepsAdded;
        }

        class RemoveStepVisitor : ITestStepVisitor
        {
            TestStep stepToRemove;
            List<HistoryItem> m_removedSteps;
            public RemoveStepVisitor(TestStep stepToRemove, List<HistoryItem> removedSteps)
            {
                this.stepToRemove = stepToRemove;
                this.m_removedSteps = removedSteps;
            }

            #region ITestStepVisitor Members

            public void AcceptSingle(TestStepSingle test)
            {
            }

            public void AcceptComposite(TestStepComposite test)
            {
                if (test.Steps.Contains(stepToRemove))
                {
                    int index = test.Steps.IndexOf(stepToRemove);
                    test.Steps.Remove(stepToRemove);
                    m_removedSteps.Add(new HistoryItem()
                    {
                        RemovedStep = stepToRemove,
                        Parent = test,
                        IndexInParent = index
                    });
                    return;
                }
                foreach (var step in test.Steps)
                {
                    step.Accept(this);
                }
            }

            #endregion
        }

        public bool Execute()
        {
            bool modified = false;
            foreach (var item in mStepsToRemove)
            {
                if (item == mTestRun.TestSequence)
                {
                    mTestRun.TestSequence = null;
                }
                else
                {
                    mTestRun.TestSequence.Accept(new RemoveStepVisitor(item, mRemovedSteps));
                }
                modified = true;
            }
            if (modified && mOnStepsRemoved != null)
            {
                var results = from p in mRemovedSteps
                    group p.RemovedStep by p.Parent into g
                    select new { Parent = g.Key, Steps = g.ToList() };
                foreach (var r in results)
                {
                    mOnStepsRemoved(r.Parent, r.Steps);
                }
            }
            return modified;
        }

        public bool Undo()
        {
            bool modified = false;

            for (int i = mRemovedSteps.Count - 1; i >= 0; i--)
            {
                var item = mRemovedSteps[i];
                item.Parent.Steps.Insert(item.IndexInParent, item.RemovedStep);
                modified = true;
            }            
            
            if (modified && mOnStepsAdded != null)
            {
                foreach (var r in mRemovedSteps)
                {
                    mOnStepsAdded(r.Parent, r.RemovedStep.ToSingleItemList(), r.IndexInParent, false);
                }
            }

            mRemovedSteps.Clear();
            return modified;
        }
    }
}
