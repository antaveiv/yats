﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.ExecutionQueue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using yats.Utilities.Commands;

namespace yats.TestRun.Commands
{
    public class ClearTestRun : ICommand
    {
        ITestRun m_testRun;
        TestStep m_previousRootStep;
        Action m_onTestModify;
        
        public ClearTestRun(ITestRun testRun, Action onTestModify)
        {
            m_testRun = testRun;
            m_onTestModify = onTestModify;
        }

        public bool Execute()
        {
            bool modified = m_testRun.TestSequence != null;
            m_previousRootStep = m_testRun.TestSequence;
            m_testRun.TestSequence = null;
            if (modified && m_onTestModify != null)
            {
                m_onTestModify();
            }
            return modified;
        }

        public bool Undo()
        {
            bool modified = m_testRun.TestSequence != m_previousRootStep;
            m_testRun.TestSequence = m_previousRootStep;

            if (modified && m_onTestModify != null)
            {
                m_onTestModify();
            }
            return modified;
        }
    }
}
