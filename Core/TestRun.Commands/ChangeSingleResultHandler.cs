﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.ExecutionQueue;
using yats.ExecutionQueue.DecisionHandler;
using yats.ExecutionQueue.ResultHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using yats.Utilities.Commands;

namespace yats.TestRun.Commands
{
    public class ChangeSingleResultHandler<T> : ICommand where T : TestStepSingle
    {
        private ISingleResultHandler m_newHandler;
        private Dictionary<T, ISingleResultHandler> m_oldHandlers = new Dictionary<T, ISingleResultHandler>();
        private Action m_onTestModify;

        public ChangeSingleResultHandler(ISingleResultHandler handler, IEnumerable<T> steps, Action onTestModify)
        {
            this.m_newHandler = handler;
            m_onTestModify = onTestModify;
            foreach (var step in steps)
            {
                m_oldHandlers.Add(step, step.SingleRunResultHandler);
            }
        }

        public bool Execute()
        {
            bool modified = false;
            foreach (var step in m_oldHandlers.Keys)
            {
                step.SingleRunResultHandler = m_newHandler.Clone();
                modified = true;
            }
            if (modified && m_onTestModify != null)
            {
                m_onTestModify();
            }
            return modified;
        }

        public bool Undo()
        {
            bool modified = false;
            foreach (var pair in m_oldHandlers)
            {
                pair.Key.SingleRunResultHandler = pair.Value;
                modified = true;
            }
            if (modified && m_onTestModify != null)
            {
                m_onTestModify();
            }
            return modified;
        }
    }
}
