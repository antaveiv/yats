﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.TestCase.Parameter;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;
using yats.ExecutionQueue;
using yats.Utilities.Commands;

namespace yats.TestRun.Commands.Visitors
{    
    public class RemoveNonExistingGlobals 
    {
        /// <summary>
        /// check if all global parameter values can be found. If not found, the corresponding parameter is set to null
        /// </summary>
        /// <param name="globalParameters"></param>
        /// <param name="value"></param>
        /// <returns>true if all global parameters were found</returns>
        public static ICommand Validate(TestStep root, IGlobalParameterCollection globals)
        {
            CompositeCommand result = null;
            var parameters = ParameterEnumerator.GetParameters(root).Where(x => x.Value is GlobalValue);
            foreach (var param in parameters)
            {
                if (globals.Get(param.Value as GlobalValue) == null)
                {
                    if (result == null)
                    {
                        result = new CompositeCommand();
                    }
                    result.Add(new SetParameterValue(param, null));
                }
            }

            return result;
        }

        public static ICommand ValidateWithUndo(TestStep root, IGlobalParameterCollection globalParameterCollection, Guid id, GlobalParameter removedGlobalValue, IGlobalParameters manager, Action<bool> onTestModify)
        {
            CompositeCommand result = null;
            var parameters = ParameterEnumerator.GetParameters(root);
            foreach (var param in parameters)
            {
                GlobalValue asGlobal = param.Value as GlobalValue;
                if (asGlobal != null){
                    if (asGlobal.ParameterID == id && asGlobal.StorageId == manager.ID){
                        result = result ?? new CompositeCommand(onTestModify);
                        result.Add(new SetParameterValue(param, null, id, removedGlobalValue, manager));                    
                    }
                }
            }

            return result;
        }
    }
}
