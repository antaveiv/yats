﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.TestCase.Parameter;
using yats.Utilities;
using yats.Utilities.Commands;
using System;
using System.Collections.Generic;

namespace yats.TestRun.Commands
{
    public class RenameParameter : ICommand
    {
        private string m_newName;
        private Dictionary<IParameter, string> m_oldNames = new Dictionary<IParameter, string>(ObjectReferenceEqualityComparer <IParameter>.Default);
        private Action m_onTestModify;

        public RenameParameter(string newName, IEnumerable<IParameter> parameters, Action onTestModify)
        {
            this.m_newName = newName;
            m_onTestModify = onTestModify;

            foreach (var param in parameters)
            {
                m_oldNames.Add(param, param.UserDescription);
            }
        }

        public bool Execute()
        {
            bool modified = false;
            foreach (var param in m_oldNames.Keys)
            {
                if (param.UserDescription != m_newName)
                {
                    param.UserDescription = m_newName;
                    modified = true;
                }
            }
            if (modified && m_onTestModify != null)
            {
                m_onTestModify();
            }
            return modified;
        }

        public bool Undo()
        {
            bool modified = false;
            foreach (var pair in m_oldNames)
            {
                pair.Key.UserDescription = pair.Value;
                modified = true;
            }
            if (modified && m_onTestModify != null)
            {
                m_onTestModify();
            }
            return modified;
        }
    }
}
