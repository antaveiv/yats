/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using NUnit.Framework;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.ExecutionQueue.CompositeStepExecuteDecision;
using yats.TestCase.Yats;
using yats.TestRepositoryManager.Interface;
using yats.TestRun;

namespace yats.ExecutionTest
{
	[TestFixture]
	public class CompositeStepExecuteDecisionTests
	{
		
		static TestStepComposite SetupTestScenario_no_failures(ITestRepositoryManager manager)
		{
            TestStepComposite R = new TestStepComposite( );
            R.ExecutionMethod = new SequentialExecutionModifier();
			
			var B1 = new TestStepSingle( new AlwaysPass(), manager );
			R.Steps.Add(B1);
			B1 = new TestStepSingle( new AlwaysPass(), manager );
			R.Steps.Add(B1);
			B1 = new TestStepSingle( new AlwaysPass(), manager );
			R.Steps.Add(B1);
			
			return R;
		}
		
		[Test]
		public void WhilePassing_with_no_failures ()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			TestStepComposite root = SetupTestScenario_no_failures( manager );
			root.StepDecisionHandler = new ExecuteWhilePassing();
            ITestRunner testRunner = new TestRunner(new TestRunImplementation(root));
			
            testRunner.TestRunFinished += manager.executeEngine_TestRunFinished;
            testRunner.TestRunStarting += manager.executeEngine_TestRunStarting;
            testRunner.TestStepResult += manager.executeEngine_TestStepResult;
            testRunner.TestStepRepetitionResult += manager.executeEngine_TestStepRepetitionResult;
            testRunner.TestStepStarting += manager.executeEngine_TestStepStarting;

            testRunner.StartTestRun();			

			Assert.AreEqual(3, manager.NumTestCasesRun);
			Assert.AreEqual(1, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Single);
			Assert.AreEqual(0, manager.NumFail_Single);
			Assert.AreEqual(3, manager.NumPass_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Single);
			
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(3, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(3, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(4, manager.NumTestStepStartReported);
		}
		
		static TestStepComposite SetupTestScenario_with_failure(ITestRepositoryManager manager)
		{
            TestStepComposite R = new TestStepComposite( );
            R.ExecutionMethod = new SequentialExecutionModifier();
			
			R.Steps.Add(new TestStepSingle(new AlwaysPass(), manager));
			R.Steps.Add(new TestStepSingle(new AlwaysPass(), manager));
			R.Steps.Add(new TestStepSingle(new AlwaysPass(), manager));
			// should stop execution here
			R.Steps.Add(new TestStepSingle(new AlwaysFail(), manager));
			R.Steps.Add(new TestStepSingle(new AlwaysPass(), manager));
			
			return R;
		}
		
		[Test]
		public void WhilePassing_with_failure ()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			TestStepComposite root = SetupTestScenario_with_failure( manager );
			root.StepDecisionHandler = new ExecuteWhilePassing();
			TestRunner testRunner = new TestRunner( );
			
            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += (manager.executeEngine_TestStepResult);
            testRunner.TestStepRepetitionResult += (manager.executeEngine_TestStepRepetitionResult);
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();			

			Assert.AreEqual(4, manager.NumTestCasesRun);
			Assert.AreEqual(1, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Single);
			Assert.AreEqual(1, manager.NumFail_Single);
			Assert.AreEqual(3, manager.NumPass_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Single);
			
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(1, manager.NumFail_Reported_Single);
			Assert.AreEqual(3, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(1, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(1, manager.NumFail_Processed_Single);
			Assert.AreEqual(3, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(1, manager.NumNotRun_Processed_Single);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(6, manager.NumTestStepStartReported);
		}
		
				
		static TestStepComposite SetupTestScenario_with_cancel(ITestRepositoryManager manager)
		{
            TestStepComposite R = new TestStepComposite( );
            R.ExecutionMethod = new SequentialExecutionModifier();
			
			var B1 = new TestStepSingle( new AlwaysPass(), manager );
			R.Steps.Add(B1);
			B1 = new TestStepSingle( new AlwaysPass(), manager );
			R.Steps.Add(B1);
			B1 = new TestStepSingle( new AlwaysPass(), manager );
			R.Steps.Add(B1);
			B1 = new TestStepSingle( new AlwaysCanceled(), manager ); // should stop execution here
			R.Steps.Add(B1);
			B1 = new TestStepSingle( new AlwaysPass(), manager );
			R.Steps.Add(B1);
			
			return R;
		}
		
		[Test]
		public void WhilePassing_with_cancel ()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			TestStepComposite root = SetupTestScenario_with_cancel( manager );
			root.StepDecisionHandler = new ExecuteWhilePassing();
			TestRunner testRunner = new TestRunner( );
			
            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += (manager.executeEngine_TestStepResult);
            testRunner.TestStepRepetitionResult += (manager.executeEngine_TestStepRepetitionResult);
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();			

			Assert.AreEqual(4, manager.NumTestCasesRun);
			Assert.AreEqual(1, manager.NumCompositesRun);
			
			Assert.AreEqual(1, manager.NumCanceled_Single);
			Assert.AreEqual(0, manager.NumFail_Single);
			Assert.AreEqual(3, manager.NumPass_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Single);
			
			
			Assert.AreEqual(1, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(3, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(1, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(1, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(3, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(1, manager.NumNotRun_Processed_Single);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(6, manager.NumTestStepStartReported);
		}
				
		[Test]
		public void UntilCanceled_not_canceled ()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			TestStepComposite root = SetupTestScenario_with_failure( manager );
			//root.CompositeDecisionHandler = new ExecuteUntilCanceled();//should be default anyway
			TestRunner testRunner = new TestRunner( );
			
            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += (manager.executeEngine_TestStepResult);
            testRunner.TestStepRepetitionResult += (manager.executeEngine_TestStepRepetitionResult);
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();			

			Assert.AreEqual(5, manager.NumTestCasesRun);
			Assert.AreEqual(1, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Single);
			Assert.AreEqual(1, manager.NumFail_Single);
			Assert.AreEqual(4, manager.NumPass_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Single);
			
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(1, manager.NumFail_Reported_Single);
			Assert.AreEqual(4, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(1, manager.NumFail_Processed_Single);
			Assert.AreEqual(4, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(6, manager.NumTestStepStartReported);
		}
		
		[Test]
		public void UntilCanceled_canceled ()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			TestStepComposite root = SetupTestScenario_with_failure( manager );
			// should stop execution here
			root.Steps.Add(new TestStepSingle(new AlwaysCanceled(), manager));
			// should not be executed
			root.Steps.Add(new TestStepSingle(new AlwaysPass(), manager));
			
			//root.CompositeDecisionHandler = new ExecuteUntilCanceled();//should be default anyway
			TestRunner testRunner = new TestRunner( );
			
            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += (manager.executeEngine_TestStepResult);
            testRunner.TestStepRepetitionResult += (manager.executeEngine_TestStepRepetitionResult);
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();			

			Assert.AreEqual(6, manager.NumTestCasesRun);
			Assert.AreEqual(1, manager.NumCompositesRun);
			
			Assert.AreEqual(1, manager.NumCanceled_Single);
			Assert.AreEqual(1, manager.NumFail_Single);
			Assert.AreEqual(4, manager.NumPass_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Single);
			
			
			Assert.AreEqual(1, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(1, manager.NumFail_Reported_Single);
			Assert.AreEqual(4, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(1, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(1, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(1, manager.NumFail_Processed_Single);
			Assert.AreEqual(4, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(1, manager.NumNotRun_Processed_Single);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(8, manager.NumTestStepStartReported);
		}
	}
}

