/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.IO;
using System.Xml.Serialization;
using yats.ExecutionTest;
using NUnit.Framework;
using yats.ExecutionQueue;
using yats.ExecutionQueue.CompositeStepExecuteDecision;
using yats.ExecutionQueue.DecisionHandler;
using yats.ExecutionQueue.ResultHandler;
using yats.TestCase.Yats;

namespace yats.ExecutionTest
{
#if DEBUG
	[TestFixture]
	public class ExecQueueSerializationTest
	{
		[Test]
		public void SerializationTest()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			
			TestStepComposite root = new TestStepComposite();
			root.CompositeResultHandler = new DefaultCompositeResultHandler();
			
			TestStepComposite comp2 = new TestStepComposite();
			comp2.ExecutionMethod = new ParallelExecutionModifier();
			comp2.CompositeResultHandler = new WorstCaseCompositeResultHandler();
			root.Steps.Add(comp2);
			
			TestStepComposite comp3 = new TestStepComposite();
			comp3.ExecutionMethod = new RandomOrderModifier();
			comp3.RepetitionResultHandler = new DefaultRepetitionResultHandler();
			root.Steps.Add(comp3);
			
			TestStepComposite comp4 = new TestStepComposite();
			comp4.ExecutionMethod = new SequentialExecutionModifier();
			comp4.RepetitionResultHandler = new WorstCaseRepetitionResultHandler();
			root.Steps.Add(comp4);
			
			TestStepComposite comp5 = new TestStepComposite();
			comp5.ExecutionMethod = new StepDisabledModifier();
			root.Steps.Add(comp5);
			
			TestStepComposite comp6 = new TestStepComposite();
			comp6.DecisionHandler = new SingleRun();
			root.Steps.Add(comp6);
		
			TestStepComposite comp8 = new TestStepComposite();
			comp8.DecisionHandler = new RepeatIgnoreFails(3);
			root.Steps.Add(comp8);
			
			TestStepComposite comp9 = new TestStepComposite();
			comp9.DecisionHandler = new RepeatUntilCancel();
			root.Steps.Add(comp9);
			
			TestStepComposite comp10 = new TestStepComposite();
			comp10.DecisionHandler = new RepeatWhilePass(2);
			comp9.Steps.Add(comp10);
			
			TestStepComposite comp11 = new TestStepComposite();
			comp11.StepDecisionHandler = new ExecuteUntilCanceled();
			root.Steps.Add(comp11);
			
			TestStepComposite comp12 = new TestStepComposite();
			comp12.StepDecisionHandler = new ExecuteWhilePassing();
			root.Steps.Add(comp12);
			
			TestStepSingle single1 = new TestStepSingle(new AlwaysCrash(), manager);
			single1.DecisionHandler = new SingleRun();
			single1.ExecutionMethod = new StepDisabledModifier();
			comp2.Steps.Add(single1);
			
			single1 = new TestStepSingle(new AlwaysCrash(), manager);
			single1.DecisionHandler = new RepeatIgnoreFails(2);
			single1.SingleRunResultHandler = new CustomResultMapping();
			comp2.Steps.Add(single1);
			
			single1 = new TestStepSingle(new AlwaysCrash(), manager);
			single1.DecisionHandler = new RepeatUntilCancel();
			//single1.SingleRunResultHandler = new DefaultSingleResultHandler();
			comp2.Steps.Add(single1);
			
			single1 = new TestStepSingle(new AlwaysCrash(), manager);
			single1.DecisionHandler = new RepeatWhilePass(2);
			single1.SingleRunResultHandler = new IgnoreFailureSingleResultHandler();
			comp2.Steps.Add(single1);		
			
			//var doc = Utilities.Convert.ObjectToXmlDocument(root);

            XmlSerializer serializer = yats.Utilities.SerializationHelper.GetSerializer(typeof(TestStep));
            TextWriter tw = new StreamWriter( "test.xml" );
            serializer.Serialize( tw, root );
            tw.Close( );
			
		}
	}
#endif
}

