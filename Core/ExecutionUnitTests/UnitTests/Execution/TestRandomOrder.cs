/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using NUnit.Framework;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.ExecutionQueue.DecisionHandler;
using yats.TestCase.Yats;
using yats.TestRepositoryManager.Interface;
using yats.TestRun;

namespace yats.ExecutionTest
{
	[TestFixture]
	public class TestRandomOrder
	{		
		static TestStepComposite SetupTestScenario_RandomOrder(ITestRepositoryManager manager)
		{
            TestStepComposite R = new TestStepComposite( );
            R.ExecutionMethod = new RandomOrderModifier();
			            
			var A1 = new TestStepSingle( new AlwaysPass(), manager );
            A1.DecisionHandler = new RepeatWhilePass(2);
			R.Steps.Add(A1);
						
			var A2 = new TestStepSingle( new AlwaysPass(), manager );
            A2.ExecutionMethod = new StepDisabledModifier();
			A2.DecisionHandler = new RepeatWhilePass(3);
			R.Steps.Add(A2);

			R.Steps.Add(new TestStepSingle( new AlwaysPass(), manager ));
			R.Steps.Add(new TestStepSingle( new AlwaysPass(), manager ));
			
			return R;
		}
		
		[Test]
		public void Test_RandomOrder1 ()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			TestStep root = SetupTestScenario_RandomOrder( manager );
			TestRunner testRunner = new TestRunner( );
			
            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += (manager.executeEngine_TestStepResult);
            testRunner.TestStepRepetitionResult += (manager.executeEngine_TestStepRepetitionResult);
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();
			
			Assert.AreEqual(4, manager.NumTestCasesRun);
			Assert.AreEqual(1, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Single);
			Assert.AreEqual(0, manager.NumFail_Single);
			Assert.AreEqual(4, manager.NumPass_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(4, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(4, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Composite);
			Assert.AreEqual(0, manager.NumFail_Reported_Composite);
			Assert.AreEqual(1, manager.NumPass_Reported_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Composite);
						
			Assert.AreEqual(0, manager.NumCanceled_Processed_Composite);
			Assert.AreEqual(0, manager.NumFail_Processed_Composite);
			Assert.AreEqual(1, manager.NumPass_Processed_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Composite);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(4+1, manager.NumTestStepStartReported);
		}

        static TestStepComposite SetupTestScenario_RandomOrder_with_composites(ITestRepositoryManager manager)
        {
            TestStepComposite R = new TestStepComposite();
            R.ExecutionMethod = new RandomOrderModifier();

            var Group = new TestStepComposite();
            var A1 = new TestStepSingle(new AlwaysPass(), manager);
            Group.Steps.Add(A1);
            R.Steps.Add(Group);

            Group = new TestStepComposite();
            A1 = new TestStepSingle(new AlwaysPass(), manager);
            Group.Steps.Add(A1);
            R.Steps.Add(Group);

            Group = new TestStepComposite();
            A1 = new TestStepSingle(new AlwaysPass(), manager);
            Group.Steps.Add(A1);
            R.Steps.Add(Group);

            
            return R;
        }

        [Test]
        public void Test_RandomOrder2()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStep root = SetupTestScenario_RandomOrder_with_composites(manager);
            TestRunner testRunner = new TestRunner();

            testRunner.TestRunFinished += (manager.executeEngine_TestRunFinished);
            testRunner.TestRunStarting += (manager.executeEngine_TestRunStarting);
            testRunner.TestStepResult += (manager.executeEngine_TestStepResult);
            testRunner.TestStepRepetitionResult += (manager.executeEngine_TestStepRepetitionResult);
            testRunner.TestStepStarting += (manager.executeEngine_TestStepStarting);

            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();

            Assert.AreEqual(3, manager.NumTestCasesRun);
            Assert.AreEqual(4, manager.NumCompositesRun);

            Assert.AreEqual(0, manager.NumCanceled_Single);
            Assert.AreEqual(0, manager.NumFail_Single);
            Assert.AreEqual(3, manager.NumPass_Single);
            Assert.AreEqual(0, manager.NumInconclusive_Single);

            Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
            Assert.AreEqual(0, manager.NumFail_Reported_Single);
            Assert.AreEqual(3, manager.NumPass_Reported_Single);
            Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
            Assert.AreEqual(0, manager.NumNotRun_Reported_Single);

            Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
            Assert.AreEqual(0, manager.NumFail_Processed_Single);
            Assert.AreEqual(3, manager.NumPass_Processed_Single);
            Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
            Assert.AreEqual(0, manager.NumNotRun_Processed_Single);

            Assert.AreEqual(0, manager.NumCanceled_Reported_Composite);
            Assert.AreEqual(0, manager.NumFail_Reported_Composite);
            Assert.AreEqual(4, manager.NumPass_Reported_Composite);
            Assert.AreEqual(0, manager.NumInconclusive_Reported_Composite);
            Assert.AreEqual(0, manager.NumNotRun_Reported_Composite);

            Assert.AreEqual(0, manager.NumCanceled_Processed_Composite);
            Assert.AreEqual(0, manager.NumFail_Processed_Composite);
            Assert.AreEqual(4, manager.NumPass_Processed_Composite);
            Assert.AreEqual(0, manager.NumInconclusive_Processed_Composite);
            Assert.AreEqual(0, manager.NumNotRun_Processed_Composite);

            Assert.AreEqual(1, manager.NumTestRunsStarted);
            Assert.AreEqual(1, manager.NumTestRunsFinished);
            Assert.AreEqual(4 + 3, manager.NumTestStepStartReported);
        }
	}
}

