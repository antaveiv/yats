/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using NUnit.Framework;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.TestCase.Yats;
using yats.TestRun;

namespace yats.ExecutionTest
{
	[TestFixture]
	public class FatalErrorTests
	{
		
		[Test]
		public void TestNullSequence ()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			TestStep root = null;
			TestRunner testRunner = new TestRunner( );
			
            testRunner.TestRunFinished += manager.executeEngine_TestRunFinished;
            testRunner.TestRunStarting += manager.executeEngine_TestRunStarting;
            testRunner.TestStepResult += manager.executeEngine_TestStepResult;
            testRunner.TestStepRepetitionResult += manager.executeEngine_TestStepRepetitionResult;
            testRunner.TestStepStarting += manager.executeEngine_TestStepStarting;

            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();
			
			Assert.AreEqual(0, manager.NumTestCasesRun);
			Assert.AreEqual(1, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Single);
			Assert.AreEqual(0, manager.NumFail_Single);
			Assert.AreEqual(0, manager.NumPass_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(0, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(0, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Composite);
			Assert.AreEqual(0, manager.NumFail_Reported_Composite);
			Assert.AreEqual(1, manager.NumPass_Reported_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Composite);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Composite);
			Assert.AreEqual(0, manager.NumFail_Processed_Composite);
			Assert.AreEqual(1, manager.NumPass_Processed_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Composite);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(1, manager.NumTestStepStartReported);
		}
		
		[Test]
		public void TestCrashBecomesInconclusive ()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			TestStep root = new TestStepSingle(new AlwaysCrash(), manager);
			TestRunner testRunner = new TestRunner( );
			
            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += (manager.executeEngine_TestStepResult);
            testRunner.TestStepRepetitionResult += (manager.executeEngine_TestStepRepetitionResult);
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();
			
			Assert.AreEqual(1, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Single);
			Assert.AreEqual(0, manager.NumFail_Single);
			Assert.AreEqual(0, manager.NumPass_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(0, manager.NumPass_Reported_Single);
			Assert.AreEqual(1, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(0, manager.NumPass_Processed_Single);
			Assert.AreEqual(1, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Composite);
			Assert.AreEqual(0, manager.NumFail_Reported_Composite);
			Assert.AreEqual(0, manager.NumPass_Reported_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Composite);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Composite);
			Assert.AreEqual(0, manager.NumFail_Processed_Composite);
			Assert.AreEqual(0, manager.NumPass_Processed_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Composite);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(1, manager.NumTestStepStartReported);
		}
	}
}

