﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Linq;
using NUnit.Framework;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.ExecutionQueue.ResultHandler;
using yats.TestCase.Yats;
using yats.TestRun;

namespace yats.ExecutionTest
{
    [TestFixture]
    public class StateMachine
    {
        
        [Test]
        public void TestResult()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            StateMachineExecutionModifier sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            R.ExecutionMethod = sm;
            R.CompositeResultHandler = new StateMachineResultHandler();

            var stateStart = sm.States.First(x => x.IsStartState);
            var stateFinish = sm.States.First(x => x.IsFinishState);
            stateFinish.ResultOnFinish = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;
            var stateX = new StateMachineExecutionModifier.State("StateX");
            sm.States.Add(stateX);
            
            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateX.StateGUID);
                stateStart.Steps.Add(step1Map);
                step1.Name = "Step 1";
                R.Steps.Add(step1);
            }
            {
                var step2 = new TestStepSingle(new AlwaysFail(), manager);
                var step2Map = new StateMachineExecutionModifier.TestCaseResultMap(step2);
                step2Map.Probability = 2;
                step2Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateX.StateGUID);//will not happen - always fails and should stay in start state
                stateStart.Steps.Add(step2Map);
                step2.Name = "Step 2"; 
                R.Steps.Add(step2);
            }
            {
                var step3 = new TestStepSingle(new AlwaysPass(), manager);
                var step3Map = new StateMachineExecutionModifier.TestCaseResultMap(step3);
                step3Map.Probability = 13;
                step3Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateFinish.StateGUID);//will not happen - always fails and should stay in start state
                stateX.Steps.Add(step3Map);
                step3.Name = "Step 3";
                R.Steps.Add(step3);
            }
            {
                var step4 = new TestStepSingle(new AlwaysFail(), manager);
                var step4Map = new StateMachineExecutionModifier.TestCaseResultMap(step4);
                step4Map.Probability = 23;
                step4Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateFinish.StateGUID);//will not happen - always fails and should stay in start state
                stateX.Steps.Add(step4Map);
                step4.Name = "Step 4";
                R.Steps.Add(step4);
            }

            TestRunner testRunner = new TestRunner();

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();
            Assert.AreEqual(yats.TestCase.Interface.ResultEnum.INCONCLUSIVE, R.Result.Result);
        }

        [Test]
        [ExpectedException(ExpectedException = typeof(Exception), ExpectedMessage = "Some steps will not be executed by state machine")]// not essential but is thrown
        public void TestSanityCheck_RemoveNonExistingStep()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            StateMachineExecutionModifier sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            R.ExecutionMethod = sm;
            R.CompositeResultHandler = new StateMachineResultHandler();

            var stateStart = sm.States.First(x => x.IsStartState);
            var stateFinish = sm.States.First(x => x.IsFinishState);
            
            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateFinish.StateGUID);
                stateStart.Steps.Add(step1Map);
                step1.Name = "Step 1";
                R.Steps.Add(step1);
            }
            Assert.AreEqual(1, stateStart.Steps.Count);
            bool isModified;
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);
            Assert.IsFalse(isModified);

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.StepGuid = new Guid("dddddddddddddddddddddddddddddddd"); // replaced by non-existing step GUID
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateFinish.StateGUID);
                stateStart.Steps.Add(step1Map);
                step1.Name = "Step 2";
                R.Steps.Add(step1);
            }
            Assert.AreEqual(2, stateStart.Steps.Count);
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);
            Assert.IsTrue(isModified);
            Assert.AreEqual(1, stateStart.Steps.Count);
            // check again, should not modify further
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);
            Assert.IsFalse(isModified);
        }

        [Test]
        [ExpectedException(ExpectedException=typeof(Exception), ExpectedMessage="State machine does not have Start state")]
        public void TestSanityCheck_HasStartState1()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            StateMachineExecutionModifier sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            R.ExecutionMethod = sm;
            R.CompositeResultHandler = new StateMachineResultHandler();

            var stateStart = sm.States.First(x => x.IsStartState);
            var stateFinish = sm.States.First(x => x.IsFinishState);
            sm.States.Remove(stateStart);
            bool isModified;
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);
        }

        [Test]
        public void TestSanityCheck_HasStartState2()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            StateMachineExecutionModifier sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            R.ExecutionMethod = sm;
            R.CompositeResultHandler = new StateMachineResultHandler();

            var stateStart = sm.States.First(x => x.IsStartState);
            var stateFinish = sm.States.First(x => x.IsFinishState);

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateFinish.StateGUID);
                stateStart.Steps.Add(step1Map);
                step1.Name = "Step 1";
                R.Steps.Add(step1);
            }

            bool isModified;
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);
            Assert.IsFalse(isModified);
        }

        [Test]
        [ExpectedException(ExpectedException = typeof(Exception), ExpectedMessage = "State machine should only have one Start state")]
        public void TestSanityCheck_HasStartState3()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            StateMachineExecutionModifier sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            R.ExecutionMethod = sm;
            R.CompositeResultHandler = new StateMachineResultHandler();

            var stateStart = sm.States.First(x => x.IsStartState);
            var stateFinish = sm.States.First(x => x.IsFinishState);
            stateFinish.IsFinishState = false;
            stateFinish.IsStartState = true;
            
            bool isModified;
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);
            Assert.IsFalse(isModified);
        }

        [Test]
        [ExpectedException(ExpectedException = typeof(Exception), ExpectedMessage = "State machine does not have Finish state")]
        public void TestSanityCheck_HasFinishState1()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            StateMachineExecutionModifier sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            R.ExecutionMethod = sm;
            R.CompositeResultHandler = new StateMachineResultHandler();

            var stateStart = sm.States.First(x => x.IsStartState);
            var stateFinish = sm.States.First(x => x.IsFinishState);
            sm.States.Remove(stateFinish);
            bool isModified;
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);
        }

        [Test]
        public void TestSanityCheck_HasFinishState2()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            StateMachineExecutionModifier sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            R.ExecutionMethod = sm;
            R.CompositeResultHandler = new StateMachineResultHandler();

            var stateStart = sm.States.First(x => x.IsStartState);
            var stateFinish = sm.States.First(x => x.IsFinishState);

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateFinish.StateGUID);
                stateStart.Steps.Add(step1Map);
                step1.Name = "Step 1";
                R.Steps.Add(step1);
            }

            bool isModified;
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);
            Assert.IsFalse(isModified);
        }


        [Test]
        [ExpectedException(ExpectedException = typeof(Exception), ExpectedMessage = "Some steps will not be executed by state machine")]
        public void TestSanityCheck_FindNotExecutedStep()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            StateMachineExecutionModifier sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            R.ExecutionMethod = sm;
            R.CompositeResultHandler = new StateMachineResultHandler();

            var stateStart = sm.States.First(x => x.IsStartState);
            var stateFinish = sm.States.First(x => x.IsFinishState);

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateFinish.StateGUID);
                stateStart.Steps.Add(step1Map);
                step1.Name = "Step 1";
                R.Steps.Add(step1);
            }
            Assert.AreEqual(1, stateStart.Steps.Count);
            bool isModified;
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);
            Assert.IsFalse(isModified);

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                step1.Name = "Step 2";
                R.Steps.Add(step1);
            }
            
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);
        }
        
        [Test]
        public void TestSanityCheck_ReplaceWrongResultHandler()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            StateMachineExecutionModifier sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            R.ExecutionMethod = sm;
            var handler = new StateMachineResultHandler();
            R.CompositeResultHandler = handler;

            var stateStart = sm.States.First(x => x.IsStartState);
            var stateFinish = sm.States.First(x => x.IsFinishState);

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateFinish.StateGUID);
                stateStart.Steps.Add(step1Map);
                step1.Name = "Step 1";
                R.Steps.Add(step1);
            }

            bool isModified;
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);
            Assert.IsFalse(isModified);
            Assert.IsTrue(object.ReferenceEquals(R.CompositeResultHandler, handler));

            R.CompositeResultHandler = new SelectedStepResultHandler();
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);
            Assert.IsTrue(isModified);
            Assert.IsTrue(R.CompositeResultHandler is StateMachineResultHandler);
        }

        [Test]
        [ExpectedException(ExpectedException = typeof(Exception), ExpectedMessage = "[Finish] state(s) are not reachable")]
        public void TestSanityCheck_CheckAllStatesReachable1()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            StateMachineExecutionModifier sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            R.ExecutionMethod = sm;
            R.CompositeResultHandler = new StateMachineResultHandler();

            var stateStart = sm.States.First(x => x.IsStartState);
            var stateFinish = sm.States.First(x => x.IsFinishState);
            stateFinish.ResultOnFinish = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;
            var stateX = new StateMachineExecutionModifier.State("StateX");
            sm.States.Add(stateX);

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateX.StateGUID);
                stateStart.Steps.Add(step1Map);
                step1.Name = "Step 1";
                R.Steps.Add(step1);
            }

            bool isModified;
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);           
        }

        [Test]
        public void TestSanityCheck_RemoveNonExistingStateReferences()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            StateMachineExecutionModifier sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            R.ExecutionMethod = sm;
            R.CompositeResultHandler = new StateMachineResultHandler();

            var stateStart = sm.States.First(x => x.IsStartState);
            var stateFinish = sm.States.First(x => x.IsFinishState);
            
            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateFinish.StateGUID);
                stateStart.Steps.Add(step1Map);
                step1.Name = "Step 1";
                R.Steps.Add(step1);
            }

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, new Guid("dddddddddddddddddddddddddddddddd"));
                stateStart.Steps.Add(step1Map);
                step1.Name = "Step 2";
                R.Steps.Add(step1);
            }

            bool isModified;
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);
            Assert.IsTrue(isModified);
        }

        [Test]
        [ExpectedException(ExpectedException = typeof(Exception), ExpectedMessage = "Steps should not be added to Finish state(s)")]
        public void TestSanityCheck_CheckFinishStatesDoNotHaveSteps()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            StateMachineExecutionModifier sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            R.ExecutionMethod = sm;
            R.CompositeResultHandler = new StateMachineResultHandler();

            var stateStart = sm.States.First(x => x.IsStartState);
            var stateFinish = sm.States.First(x => x.IsFinishState);

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateFinish.StateGUID);
                stateStart.Steps.Add(step1Map);
                step1.Name = "Step 1";
                R.Steps.Add(step1);
            }

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateFinish.StateGUID);
                stateFinish.Steps.Add(step1Map); // error here
                step1.Name = "Step 1";
                R.Steps.Add(step1);
            }
            bool isModified;
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);            
        }

        [Test]
        [ExpectedException(ExpectedException = typeof(Exception), ExpectedMessage = "Deadlock in state machine states [A, B]")]
        public void TestSanityCheck_CheckDeadlocks()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            StateMachineExecutionModifier sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            R.ExecutionMethod = sm;
            R.CompositeResultHandler = new StateMachineResultHandler();

            var stateStart = sm.States.First(x => x.IsStartState);
            stateStart.StateGUID = new Guid("00000000000000000000000000000000");
            var stateFinish = sm.States.First(x => x.IsFinishState);
            stateFinish.StateGUID = new Guid("0000000000000000000000000000000F");

            var stateA = new StateMachineExecutionModifier.State("A");
            stateA.StateGUID = new Guid("0000000000000000000000000000000A");
            var stateB = new StateMachineExecutionModifier.State("B");
            stateB.StateGUID = new Guid("0000000000000000000000000000000B");
            var stateC = new StateMachineExecutionModifier.State("C");
            stateC.StateGUID = new Guid("0000000000000000000000000000000C");
            sm.States.Add(stateA);
            sm.States.Add(stateB);
            sm.States.Add(stateC);

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateA.StateGUID);
                stateStart.Steps.Add(step1Map);
                step1.Name = "Step 1";
                R.Steps.Add(step1);
            }

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateB.StateGUID);
                stateA.Steps.Add(step1Map);
                step1.Name = "Step 2";
                R.Steps.Add(step1);
            }

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateA.StateGUID);
                stateB.Steps.Add(step1Map);
                step1.Name = "Step 3";
                R.Steps.Add(step1);
            }

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateC.StateGUID);
                stateStart.Steps.Add(step1Map);
                step1.Name = "Step 4";
                R.Steps.Add(step1);
            }

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateFinish.StateGUID);
                stateC.Steps.Add(step1Map);
                step1.Name = "Step 5";
                R.Steps.Add(step1);
            }
            
            bool isModified;
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);   
        }

        [Test]
        [ExpectedException(ExpectedException = typeof(Exception), ExpectedMessage = "Deadlock in state machine states [Start]")]
        public void TestSanityCheck_CheckDeadlocks2()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            StateMachineExecutionModifier sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            R.ExecutionMethod = sm;
            R.CompositeResultHandler = new StateMachineResultHandler();

            var stateStart = sm.States.First(x => x.IsStartState);
            stateStart.StateGUID = new Guid("00000000000000000000000000000000");
            var stateFinish = sm.States.First(x => x.IsFinishState);
            stateFinish.StateGUID = new Guid("0000000000000000000000000000000F");

            var stateC = new StateMachineExecutionModifier.State("C");
            stateC.StateGUID = new Guid("0000000000000000000000000000000C");
            sm.States.Add(stateC);

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1.ExecutionMethod = new StepDisabledModifier();//Step is disabled - should not reach finish
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateC.StateGUID);
                stateStart.Steps.Add(step1Map);
                step1.Name = "Step 4";
                R.Steps.Add(step1);
            }

            {
                var step1 = new TestStepSingle(new AlwaysPass(), manager);
                var step1Map = new StateMachineExecutionModifier.TestCaseResultMap(step1);
                step1Map.Probability = 1;
                step1Map.ResultStateMap.Add(yats.TestCase.Interface.ResultEnum.PASS, stateFinish.StateGUID);
                stateC.Steps.Add(step1Map);
                step1.Name = "Step 5";
                R.Steps.Add(step1);
            }

            bool isModified;
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);
        }

        [Test]
        [ExpectedException(ExpectedException = typeof(Exception), ExpectedMessage = "[Finish, C] state(s) are not reachable")]
        public void TestSanityCheck_NoChildSteps()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            StateMachineExecutionModifier sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            R.ExecutionMethod = sm;
            R.CompositeResultHandler = new StateMachineResultHandler();

            var stateStart = sm.States.First(x => x.IsStartState);
            stateStart.StateGUID = new Guid("00000000000000000000000000000000");
            var stateFinish = sm.States.First(x => x.IsFinishState);
            stateFinish.StateGUID = new Guid("0000000000000000000000000000000F");

            var stateC = new StateMachineExecutionModifier.State("C");
            stateC.StateGUID = new Guid("0000000000000000000000000000000C");
            sm.States.Add(stateC);
            
            bool isModified;
            StateMachineSanityCheckVisitor.CheckStepMachines(R, out isModified);
        }
    }
}
