/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using NUnit.Framework;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.ExecutionQueue.DecisionHandler;
using yats.TestCase.Yats;
using yats.TestRepositoryManager.Interface;
using yats.TestRun;

namespace yats.ExecutionTest
{
	[TestFixture]
	public class RepetitionTests
	{
		static TestStepComposite SetupTestScenario_Repeats(ITestRepositoryManager manager)
		{
            TestStepComposite R = new TestStepComposite( );
            R.ExecutionMethod = new SequentialExecutionModifier();
			
            var A1 = new TestStepComposite( ); 
			var B2 = new TestStepComposite( ); 
            A1.DecisionHandler = new RepeatWhilePass( 3 );
			B2.DecisionHandler = new RepeatWhilePass( 2 );
#if DEBUG
			R.Name = "R";
			A1.Name = "A1";
			B2.Name = "B2";
#endif
			R.Steps.Add( A1 );
			
			var B1 = new TestStepSingle( new AlwaysPass(), manager );
            B1.DecisionHandler = new RepeatWhilePass( 2 );
			A1.Steps.Add(B1);
			A1.Steps.Add(B2);
			
			var C1 = new TestStepSingle( new AlwaysPass(), manager );
            C1.DecisionHandler = new RepeatWhilePass( 2 );
			B2.Steps.Add(C1);
			
			var C2 = new TestStepSingle( new AlwaysPass(), manager );
            C2.DecisionHandler = new RepeatWhilePass( 3 );
			B2.Steps.Add(C2);
			
			
			var A2 = new TestStepSingle( new AlwaysPass(), manager );
			A2.DecisionHandler = new RepeatWhilePass( 3 );
			R.Steps.Add( A2 );
			
			return R;
		}
		
		[Test]
		public void TestRepetitions_Good_Case ()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			TestStep root = SetupTestScenario_Repeats( manager );
			TestRunner testRunner = new TestRunner( );
			
            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += (manager.executeEngine_TestStepResult);
            testRunner.TestStepRepetitionResult += (manager.executeEngine_TestStepRepetitionResult);
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();
			
			Assert.AreEqual(39, manager.NumTestCasesRun);
			Assert.AreEqual(10, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Single);
			Assert.AreEqual(0, manager.NumFail_Single);
			Assert.AreEqual(39, manager.NumPass_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Single);
	
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(39, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(39, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Composite);
			Assert.AreEqual(0, manager.NumFail_Reported_Composite);
			Assert.AreEqual(10, manager.NumPass_Reported_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Composite);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Composite);
			Assert.AreEqual(0, manager.NumFail_Processed_Composite);
			Assert.AreEqual(10, manager.NumPass_Processed_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Composite);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(39+10, manager.NumTestStepStartReported);
		}
	}
}

