﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using yats.ExecutionTest;
using NUnit.Framework;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.ExecutionQueue.DecisionHandler;
using yats.TestCase.Yats;
using yats.TestRepositoryManager.Interface;
using yats.TestRun;

namespace yats.ExecutionTest
{
    [TestFixture]
    public class RunFromSelectedStepTest
    {
        static TestStepComposite SetupTestScenario(ITestRepositoryManager manager, string scenario, out TestStep selected)
        {
            TestStepComposite R = new TestStepComposite( );
            R.ExecutionMethod = new SequentialExecutionModifier( );
            /*			
                        R 
                            A1              
                                B1          TC
                                B2          
                                    C1      TC x2
                                    C2      TC 
                            A2              TC
            */

            var A1 = new TestStepComposite( );
            var B2 = new TestStepComposite( );
#if DEBUG
            R.Name = "R";
            A1.Name = "A1";
            B2.Name = "B2";
#endif
            R.Steps.Add( A1 );

            var B1 = new TestStepSingle( new AlwaysPass( ), manager );
            A1.Steps.Add( B1 );
            A1.Steps.Add( B2 );

            var C1 = new TestStepSingle( new AlwaysPass( ), manager );
            C1.DecisionHandler = new RepeatWhilePass( 2 );
            B2.Steps.Add( C1 );

            var C2 = new TestStepSingle( new AlwaysPass( ), manager );
            B2.Steps.Add( C2 );


            var A2 = new TestStepSingle( new AlwaysPass( ), manager );
            R.Steps.Add( A2 );

            selected = null;
            switch(scenario)
            {
                case "Null":
                    break;
                case "Root":
                    selected = R;
                    break;
                case "Single":
                    selected = C2;
                    break;                
                case "Composite":
                    selected = B2;
                    break;
                default :
                    throw new Exception( "Undefined scenario" );
            }

            return R;
        }
                
        [Test]
        public void SelectionNull()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( );
            TestStep selected;
            TestStep root = SetupTestScenario( manager, "Null", out selected );
            ITestRun testRun = new TestRunImplementation( root );
            ITestRunner testRunner = new TestRunnerRunFromStep( testRun, selected );

            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += ( manager.executeEngine_TestStepResult );
            testRunner.TestStepRepetitionResult += ( manager.executeEngine_TestStepRepetitionResult );
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.StartTestRun();
            
            Assert.AreEqual( 0, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Single );
            Assert.AreEqual( 0, manager.NumFail_Single );
            Assert.AreEqual( 0, manager.NumPass_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Single );


            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 0, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 0, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 0, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 0, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );

            Assert.AreEqual( 0, manager.NumTestRunsStarted );
            Assert.AreEqual( 0, manager.NumTestRunsFinished );
            Assert.AreEqual( 0, manager.NumTestStepStartReported );
        }

        [Test]
        public void RootSelected()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( );
            TestStep selected; 
            TestStep root = SetupTestScenario( manager, "Root", out selected );
            ITestRun testRun = new TestRunImplementation( root );
            ITestRunner testRunner = new TestRunnerRunFromStep( testRun, selected );

            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += ( manager.executeEngine_TestStepResult );
            testRunner.TestStepRepetitionResult += ( manager.executeEngine_TestStepRepetitionResult );
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.StartTestRun();


            Assert.AreEqual( 5, manager.NumTestCasesRun );
            Assert.AreEqual( 3, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Single );
            Assert.AreEqual( 0, manager.NumFail_Single );
            Assert.AreEqual( 5, manager.NumPass_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Single );


            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 5, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 5, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 3, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 3, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );

            Assert.AreEqual( 1, manager.NumTestRunsStarted );
            Assert.AreEqual( 1, manager.NumTestRunsFinished );
            Assert.AreEqual( 3+5, manager.NumTestStepStartReported );
        }

        [Test]
        public void FromComposite()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( );
            TestStep selected;
            TestStep root = SetupTestScenario( manager, "Composite", out selected );
            ITestRun testRun = new TestRunImplementation( root );
            ITestRunner testRunner = new TestRunnerRunFromStep( testRun, selected );

            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += ( manager.executeEngine_TestStepResult );
            testRunner.TestStepRepetitionResult += ( manager.executeEngine_TestStepRepetitionResult );
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.StartTestRun();


            Assert.AreEqual( 4, manager.NumTestCasesRun );
            Assert.AreEqual( 2, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Single );
            Assert.AreEqual( 0, manager.NumFail_Single );
            Assert.AreEqual( 4, manager.NumPass_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Single );


            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 4, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 4, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 2, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 2, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );

            Assert.AreEqual( 1, manager.NumTestRunsStarted );
            Assert.AreEqual( 1, manager.NumTestRunsFinished );
            Assert.AreEqual( 2 + 4, manager.NumTestStepStartReported );
        }

        [Test]
        public void FromSingle()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( );
            TestStep selected;
            TestStep root = SetupTestScenario( manager, "Single", out selected );
            ITestRun testRun = new TestRunImplementation( root );
            ITestRunner testRunner = new TestRunnerRunFromStep( testRun, selected );

            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += ( manager.executeEngine_TestStepResult );
            testRunner.TestStepRepetitionResult += ( manager.executeEngine_TestStepRepetitionResult );
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.StartTestRun();


            Assert.AreEqual( 2, manager.NumTestCasesRun );
            Assert.AreEqual( 1, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Single );
            Assert.AreEqual( 0, manager.NumFail_Single );
            Assert.AreEqual( 2, manager.NumPass_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Single );


            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 2, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 2, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 1, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 1, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );

            Assert.AreEqual( 1, manager.NumTestRunsStarted );
            Assert.AreEqual( 1, manager.NumTestRunsFinished );
            Assert.AreEqual( 2 + 1, manager.NumTestStepStartReported );
        }
    }
}
