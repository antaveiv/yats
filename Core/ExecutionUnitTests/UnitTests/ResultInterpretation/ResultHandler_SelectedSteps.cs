﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.ExecutionTest;
using NUnit.Framework;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.ExecutionQueue.ResultHandler;
using yats.TestCase.Interface;
using yats.TestRun;

namespace yats.ExecutionTest
{
    [TestFixture]    
    public class ResultHandler_SelectedSteps
    {
        [Test]
        public void IgnoreAFail()
        {
            TestRunner testRunner = new TestRunner();
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
            TestStepComposite root = DefaultResultHandler_Composite.SetupTestScenarioWithFailure(manager);
            var eval = new SelectedStepResultHandler();
            eval.StepIdsToEvaluate.Add(root.Steps[2].Guid);
            root.CompositeResultHandler = eval;

            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();

            Assert.AreEqual(root.Result.Result, ResultEnum.PASS);

            Assert.AreEqual(3, manager.NumTestCasesRun);
            Assert.AreEqual(1, manager.NumCompositesRun);

            Assert.AreEqual(0, manager.NumCanceled_Single);
            Assert.AreEqual(1, manager.NumFail_Single);
            Assert.AreEqual(2, manager.NumPass_Single);
            Assert.AreEqual(0, manager.NumInconclusive_Single);

            Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
            Assert.AreEqual(1, manager.NumFail_Reported_Single);
            Assert.AreEqual(2, manager.NumPass_Reported_Single);
            Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
            Assert.AreEqual(0, manager.NumNotRun_Reported_Single);

            Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
            Assert.AreEqual(1, manager.NumFail_Processed_Single);
            Assert.AreEqual(2, manager.NumPass_Processed_Single);
            Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
            Assert.AreEqual(0, manager.NumNotRun_Processed_Single);

            Assert.AreEqual(0, manager.NumCanceled_Reported_Composite);
            Assert.AreEqual(0, manager.NumFail_Reported_Composite);
            Assert.AreEqual(1, manager.NumPass_Reported_Composite);
            Assert.AreEqual(0, manager.NumInconclusive_Reported_Composite);
            Assert.AreEqual(0, manager.NumNotRun_Reported_Composite);

            Assert.AreEqual(0, manager.NumCanceled_Processed_Composite);
            Assert.AreEqual(0, manager.NumFail_Processed_Composite);
            Assert.AreEqual(1, manager.NumPass_Processed_Composite);
            Assert.AreEqual(0, manager.NumInconclusive_Processed_Composite);
            Assert.AreEqual(0, manager.NumNotRun_Processed_Composite);

            Assert.AreEqual(1, manager.NumTestRunsStarted);
            Assert.AreEqual(1, manager.NumTestRunsFinished);
            Assert.AreEqual(4, manager.NumTestStepStartReported);
        }

        [Test]
        public void PassAndFail()
        {
            TestRunner testRunner = new TestRunner();
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
            TestStepComposite root = DefaultResultHandler_Composite.SetupTestScenarioWithFailure(manager);
            var eval = new SelectedStepResultHandler();
            eval.StepIdsToEvaluate.Add(root.Steps[0].Guid);
            eval.StepIdsToEvaluate.Add(root.Steps[1].Guid);
            root.CompositeResultHandler = eval;

            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();

            Assert.AreEqual(root.Result.Result, ResultEnum.FAIL);

            Assert.AreEqual(3, manager.NumTestCasesRun);
            Assert.AreEqual(1, manager.NumCompositesRun);

            Assert.AreEqual(0, manager.NumCanceled_Single);
            Assert.AreEqual(1, manager.NumFail_Single);
            Assert.AreEqual(2, manager.NumPass_Single);
            Assert.AreEqual(0, manager.NumInconclusive_Single);

            Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
            Assert.AreEqual(1, manager.NumFail_Reported_Single);
            Assert.AreEqual(2, manager.NumPass_Reported_Single);
            Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
            Assert.AreEqual(0, manager.NumNotRun_Reported_Single);

            Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
            Assert.AreEqual(1, manager.NumFail_Processed_Single);
            Assert.AreEqual(2, manager.NumPass_Processed_Single);
            Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
            Assert.AreEqual(0, manager.NumNotRun_Processed_Single);

            Assert.AreEqual(0, manager.NumCanceled_Reported_Composite);
            Assert.AreEqual(1, manager.NumFail_Reported_Composite);
            Assert.AreEqual(0, manager.NumPass_Reported_Composite);
            Assert.AreEqual(0, manager.NumInconclusive_Reported_Composite);
            Assert.AreEqual(0, manager.NumNotRun_Reported_Composite);

            Assert.AreEqual(0, manager.NumCanceled_Processed_Composite);
            Assert.AreEqual(1, manager.NumFail_Processed_Composite);
            Assert.AreEqual(0, manager.NumPass_Processed_Composite);
            Assert.AreEqual(0, manager.NumInconclusive_Processed_Composite);
            Assert.AreEqual(0, manager.NumNotRun_Processed_Composite);

            Assert.AreEqual(1, manager.NumTestRunsStarted);
            Assert.AreEqual(1, manager.NumTestRunsFinished);
            Assert.AreEqual(4, manager.NumTestStepStartReported);
        }
    }
}
