/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using NUnit.Framework;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.ExecutionQueue.ResultHandler;
using yats.TestCase.Yats;
using yats.TestRun;

namespace yats.ExecutionTest
{
	[TestFixture]
	public class DefaultResultHandler_Single
	{
		[Test]
		public void DefaultPass ()
		{
			TestRunner testRunner = new TestRunner( );
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			TestStepSingle R = new TestStepSingle( new AlwaysPass(), manager );
			R.SingleRunResultHandler = new DefaultSingleResultHandler();
            
            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();
			
			Assert.AreEqual(1, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(1, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(1, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
		}
		
		[Test]
		public void DefaultCanceled ()
		{
			TestRunner testRunner = new TestRunner( );
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			TestStepSingle R = new TestStepSingle( new AlwaysCanceled(), manager );
			R.SingleRunResultHandler = new DefaultSingleResultHandler();
            
            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();
			

			Assert.AreEqual(1, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(1, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(0, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(1, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(0, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
		}
		
		[Test]
		public void DefaultFail ()
		{
			TestRunner testRunner = new TestRunner( );
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			TestStepSingle R = new TestStepSingle( new AlwaysFail(), manager );
			R.SingleRunResultHandler = new DefaultSingleResultHandler();

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

			Assert.AreEqual(1, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(1, manager.NumFail_Reported_Single);
			Assert.AreEqual(0, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(1, manager.NumFail_Processed_Single);
			Assert.AreEqual(0, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
		}
		
		[Test]
		public void DefaultInconclusive ()
		{
			TestRunner testRunner = new TestRunner( );
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			TestStepSingle R = new TestStepSingle( new AlwaysCrash(), manager );
			R.SingleRunResultHandler = new DefaultSingleResultHandler();

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

			Assert.AreEqual(1, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(0, manager.NumPass_Reported_Single);
			Assert.AreEqual(1, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(0, manager.NumPass_Processed_Single);
			Assert.AreEqual(1, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
		}
		
		[Test]
		public void DefaultNotRun ()
		{
			TestRunner testRunner = new TestRunner( );
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			TestStepSingle R = new TestStepSingle( new AlwaysCrash(), manager );
			R.ExecutionMethod = new StepDisabledModifier();
			R.SingleRunResultHandler = new DefaultSingleResultHandler();

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

			Assert.AreEqual(0, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(0, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(0, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
		}
	}
	
	[TestFixture]
	public class CustomResultMapHandler_Single
	{
		[Test]
		public void DefaultPass ()
		{
			TestRunner testRunner = new TestRunner( );
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			TestStepSingle R = new TestStepSingle( new AlwaysPass(), manager );
			var map = new CustomResultMapping();
			R.SingleRunResultHandler = map;
			map.ValueCanceled = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;
			map.ValueFail = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;
			map.ValueInconclusive = yats.TestCase.Interface.ResultEnum.PASS;
			map.ValueNotRun = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;
			map.ValuePass = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();			

			Assert.AreEqual(1, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(1, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(0, manager.NumPass_Processed_Single);
			Assert.AreEqual(1, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
		}
		
		[Test]
		public void DefaultCanceled ()
		{
			TestRunner testRunner = new TestRunner( );
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			TestStepSingle R = new TestStepSingle( new AlwaysCanceled(), manager );
			var map = new CustomResultMapping();
			R.SingleRunResultHandler = map;
			map.ValueCanceled = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;
			map.ValueFail = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;
			map.ValueInconclusive = yats.TestCase.Interface.ResultEnum.PASS;
			map.ValueNotRun = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;
			map.ValuePass = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();			

			Assert.AreEqual(1, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(1, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(0, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(0, manager.NumPass_Processed_Single);
			Assert.AreEqual(1, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
		}
		
		[Test]
		public void DefaultFail ()
		{
			TestRunner testRunner = new TestRunner( );
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			TestStepSingle R = new TestStepSingle( new AlwaysFail(), manager );
			var map = new CustomResultMapping();
			R.SingleRunResultHandler = map;
			map.ValueCanceled = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;
			map.ValueFail = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;
			map.ValueInconclusive = yats.TestCase.Interface.ResultEnum.PASS;
			map.ValueNotRun = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;
			map.ValuePass = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();			

			Assert.AreEqual(1, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(1, manager.NumFail_Reported_Single);
			Assert.AreEqual(0, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(0, manager.NumPass_Processed_Single);
			Assert.AreEqual(1, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
		}
		
		[Test]
		public void DefaultInconclusive ()
		{
			TestRunner testRunner = new TestRunner( );
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			TestStepSingle R = new TestStepSingle( new AlwaysInconclusive(), manager );
			var map = new CustomResultMapping();
			R.SingleRunResultHandler = map;
			map.ValueCanceled = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;
			map.ValueFail = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;
			map.ValueInconclusive = yats.TestCase.Interface.ResultEnum.NOT_RUN;
			map.ValueNotRun = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;
			map.ValuePass = yats.TestCase.Interface.ResultEnum.INCONCLUSIVE;

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

			Assert.AreEqual(1, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(0, manager.NumPass_Reported_Single);
			Assert.AreEqual(1, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(0, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(1, manager.NumNotRun_Processed_Single);
		}
	}

	[TestFixture]
	public class IgnoreFailureHandler_Single
	{
		[Test]
		public void DefaultPass ()
		{
			TestRunner testRunner = new TestRunner( );
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			TestStepSingle R = new TestStepSingle( new AlwaysPass(), manager );
			R.SingleRunResultHandler = new IgnoreFailureSingleResultHandler();

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();			

			Assert.AreEqual(1, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(1, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(1, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
		}
		
		[Test]
		public void DefaultCanceled ()
		{
			TestRunner testRunner = new TestRunner( );
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			TestStepSingle R = new TestStepSingle( new AlwaysCanceled(), manager );
			R.SingleRunResultHandler = new IgnoreFailureSingleResultHandler();

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();			

			Assert.AreEqual(1, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(1, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(0, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(1, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(0, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
		}
		
		[Test]
		public void DefaultFail ()
		{
			TestRunner testRunner = new TestRunner( );
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			TestStepSingle R = new TestStepSingle( new AlwaysFail(), manager );
			R.SingleRunResultHandler = new IgnoreFailureSingleResultHandler();

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();			

			Assert.AreEqual(1, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(1, manager.NumFail_Reported_Single);
			Assert.AreEqual(0, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(1, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
		}
		
		[Test]
		public void DefaultInconclusive ()
		{
			TestRunner testRunner = new TestRunner( );
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			TestStepSingle R = new TestStepSingle( new AlwaysCrash(), manager );
			R.SingleRunResultHandler = new IgnoreFailureSingleResultHandler();
            
            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

			Assert.AreEqual(1, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(0, manager.NumPass_Reported_Single);
			Assert.AreEqual(1, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(1, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
		}
		
		[Test]
		public void DefaultNotRun ()
		{
			TestRunner testRunner = new TestRunner( );
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			TestStepSingle R = new TestStepSingle( new AlwaysCrash(), manager );
			R.ExecutionMethod = new StepDisabledModifier();
			R.SingleRunResultHandler = new IgnoreFailureSingleResultHandler();

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

			Assert.AreEqual(0, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(0, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(0, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
		}
	}
}

