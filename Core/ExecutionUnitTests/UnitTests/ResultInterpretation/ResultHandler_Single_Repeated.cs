/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using NUnit.Framework;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.ExecutionQueue.DecisionHandler;
using yats.ExecutionQueue.ResultHandler;
using yats.TestCase.Interface;
using yats.TestCase.Yats;
using yats.TestRun;

namespace yats.ExecutionTest
{
	[TestFixture]
	public class DefaultResultHandler_Single_Repeated
	{
		[Test]
        public void DefaultResultHandler_PassOnly()
		{
			TestRunner testRunner = new TestRunner( );
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			var results = new List<ResultEnum>();
			results.Add(ResultEnum.PASS);
			results.Add(ResultEnum.PASS);
			results.Add(ResultEnum.PASS);
			TestStepSingle R = new TestStepSingle( new PresetResultTest(results), manager );
			R.RepetitionResultHandler = new DefaultRepetitionResultHandler();
            R.DecisionHandler = new RepeatIgnoreFails(3);
            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();
			
			Assert.AreEqual(3, manager.NumTestCasesRun);
			Assert.AreEqual(0, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(3, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(3, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
		}

        [Test]
        public void DefaultResultHandler_Canceled()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            var results = new List<ResultEnum>( );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.CANCELED );
            results.Add( ResultEnum.PASS );
            TestStepSingle R = new TestStepSingle( new PresetResultTest( results ), manager );
            R.RepetitionResultHandler = new DefaultRepetitionResultHandler();
            R.DecisionHandler = new RepeatIgnoreFails( 3 );
            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( 2, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 1, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 1, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 1, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 1, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );
            //no composites in this test - a single step is repeated 3 times
            Assert.AreEqual( 0, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 0, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 0, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );
        }
        
        /// <summary>
        /// With RepeatIgnoreFails all 3 repetitions should be run.
        /// The last repetition passes but there was a fail in the previous runs.
        /// Therefore when the test case is finished, its result shall be FAIL
        /// </summary>
        [Test]
        public void DefaultResultHandler_with_Fail()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            var results = new List<ResultEnum>( );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.FAIL );
            results.Add( ResultEnum.PASS );
            TestStepSingle R = new TestStepSingle( new PresetResultTest( results ), manager );
            R.RepetitionResultHandler = new WorstCaseRepetitionResultHandler();//If set to default handler, test run would pass
            R.DecisionHandler = new RepeatIgnoreFails( 3 );
            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.FAIL, R.Result.Result );
            Assert.AreEqual( 3, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 1, manager.NumFail_Reported_Single );
            Assert.AreEqual( 2, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 1, manager.NumFail_Processed_Single );
            Assert.AreEqual( 2, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );
            //no composites in this test - a single step is repeated 3 times
            Assert.AreEqual( 0, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 0, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 0, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );
        }

        /// <summary>
        /// With RepeatIgnoreFails all 3 repetitions should be run.
        /// The last repetition passes but there was an Inconclusive result in the previous runs.
        /// Therefore when the test case is finished, its result shall be FAIL
        /// </summary>
        [Test]
        public void DefaultResultHandler_with_Inconclusive()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            var results = new List<ResultEnum>( );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.INCONCLUSIVE );
            results.Add( ResultEnum.PASS );
            TestStepSingle R = new TestStepSingle( new PresetResultTest( results ), manager );
            R.RepetitionResultHandler = new WorstCaseRepetitionResultHandler();
            R.DecisionHandler = new RepeatIgnoreFails( 3 );
            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();
            
            Assert.AreEqual( ResultEnum.INCONCLUSIVE, R.Result.Result );
            Assert.AreEqual( 3, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 2, manager.NumPass_Reported_Single );
            Assert.AreEqual( 1, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 2, manager.NumPass_Processed_Single );
            Assert.AreEqual( 1, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );
            //no composites in this test - a single step is repeated 3 times
            Assert.AreEqual( 0, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 0, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 0, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );
        }

        [Test]
        public void RepeatUntilResult_Test1()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            var results = new List<ResultEnum>( );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.FAIL );
            results.Add( ResultEnum.INCONCLUSIVE );

            TestStepSingle R = new TestStepSingle( new PresetResultTest( results ), manager );
            R.RepetitionResultHandler = new DefaultRepetitionResultHandler( );
            R.DecisionHandler = new RepeatUntilConfiguredResult( 5, ResultEnum.FAIL );
            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();
            
            Assert.AreEqual( 4, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 1, manager.NumFail_Reported_Single );
            Assert.AreEqual( 3, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 1, manager.NumFail_Processed_Single );
            Assert.AreEqual( 3, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );
        }

        [Test]
        public void RepeatUntilResult_Test2()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            var results = new List<ResultEnum>( );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.INCONCLUSIVE );
            results.Add( ResultEnum.CANCELED );
            results.Add( ResultEnum.FAIL );            

            TestStepSingle R = new TestStepSingle( new PresetResultTest( results ), manager );
            R.RepetitionResultHandler = new DefaultRepetitionResultHandler( );
            var repeat = new RepeatUntilConfiguredResult( 6, ResultEnum.NOT_RUN );
            repeat.WaitFor.Add( ResultEnum.FAIL );
            R.DecisionHandler = repeat;

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();


            Assert.AreEqual( 6, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 1, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 1, manager.NumFail_Reported_Single );
            Assert.AreEqual( 3, manager.NumPass_Reported_Single );
            Assert.AreEqual( 1, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 1, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 1, manager.NumFail_Processed_Single );
            Assert.AreEqual( 3, manager.NumPass_Processed_Single );
            Assert.AreEqual( 1, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );
        }

        /// <summary>
        /// 3 Repetitions, not enough to reach the configured result. Running should stop after 3 repetitions
        /// </summary>
        [Test]
        public void RepeatUntilResult_Test3()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            var results = new List<ResultEnum>( );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.INCONCLUSIVE );
            results.Add( ResultEnum.CANCELED );
            results.Add( ResultEnum.FAIL );

            TestStepSingle R = new TestStepSingle( new PresetResultTest( results ), manager );
            R.RepetitionResultHandler = new DefaultRepetitionResultHandler( );
            var repeat = new RepeatUntilConfiguredResult( 3, ResultEnum.NOT_RUN );
            repeat.WaitFor.Add( ResultEnum.FAIL );
            R.DecisionHandler = repeat;

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( 3, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 3, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 3, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );
        }
	}

    [TestFixture]
    public class CustomMappingHandler_Single_Repeated
    {
        // Pass and Inconclusive are mapped to Fail, therefore step result after run should be FAIL
        [Test]
        public void PassOnly()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            var results = new List<ResultEnum>( );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.INCONCLUSIVE );
            results.Add( ResultEnum.PASS );
            TestStepSingle R = new TestStepSingle( new PresetResultTest( results ), manager );
            var map = new CustomResultMapping( );
            map.ValueFail = ResultEnum.PASS;
            map.ValueInconclusive = ResultEnum.FAIL;
            map.ValuePass = ResultEnum.FAIL;
            R.SingleRunResultHandler = map;
			R.RepetitionResultHandler = new WorstCaseRepetitionResultHandler();
            R.DecisionHandler = new RepeatIgnoreFails( 3 );

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.FAIL, R.Result.Result );
            Assert.AreEqual( 3, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 2, manager.NumPass_Reported_Single );
            Assert.AreEqual( 1, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 3, manager.NumFail_Processed_Single );
            Assert.AreEqual( 0, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );
        }

        [Test]
        public void Canceled()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            var results = new List<ResultEnum>( );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.CANCELED );
            results.Add( ResultEnum.PASS );
            TestStepSingle R = new TestStepSingle( new PresetResultTest( results ), manager );
            var map = new CustomResultMapping( );
            map.ValueFail = ResultEnum.PASS;
            map.ValueInconclusive = ResultEnum.FAIL;
            map.ValuePass = ResultEnum.FAIL;
            R.SingleRunResultHandler = map;
			R.RepetitionResultHandler = new WorstCaseRepetitionResultHandler();
            R.DecisionHandler = new RepeatIgnoreFails( 3 );

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.CANCELED, R.Result.Result );

            Assert.AreEqual( 2, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 1, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 1, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 1, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 1, manager.NumFail_Processed_Single );
            Assert.AreEqual( 0, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );
        }

        /// <summary>
        /// With RepeatIgnoreFails all 3 repetitions should be run.
        /// The last repetition passes but there was a fail in the previous runs.
        /// Therefore when the test case is finished, its result shall be FAIL
        /// </summary>
        [Test]
        public void Fail_Mapped_To_Pass()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            var results = new List<ResultEnum>( );
            results.Add( ResultEnum.INCONCLUSIVE );
            results.Add( ResultEnum.FAIL );
            results.Add( ResultEnum.INCONCLUSIVE );
            TestStepSingle R = new TestStepSingle( new PresetResultTest( results ), manager );
            var map = new CustomResultMapping( );
            map.ValueFail = ResultEnum.PASS;
            map.ValueInconclusive = ResultEnum.PASS;
            map.ValuePass = ResultEnum.FAIL;
            R.SingleRunResultHandler = map;
			R.RepetitionResultHandler = new WorstCaseRepetitionResultHandler();
            R.DecisionHandler = new RepeatIgnoreFails( 3 );

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.PASS, R.Result.Result );
            Assert.AreEqual( 3, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 1, manager.NumFail_Reported_Single );
            Assert.AreEqual( 0, manager.NumPass_Reported_Single );
            Assert.AreEqual( 2, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 3, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );
        }

        /// <summary>
        /// With RepeatIgnoreFails all 3 repetitions should be run.
        /// The last repetition passes but there was an Inconclusive result in the previous runs.
        /// Therefore when the test case is finished, its result shall be FAIL
        /// </summary>
        [Test]
        public void InconclusiveMappedToPass()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            var results = new List<ResultEnum>( );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.INCONCLUSIVE );
            results.Add( ResultEnum.PASS );
            TestStepSingle R = new TestStepSingle( new PresetResultTest( results ), manager );
            var map = new CustomResultMapping( );
            map.ValueInconclusive = ResultEnum.PASS;
            R.SingleRunResultHandler = map;
			R.RepetitionResultHandler = new WorstCaseRepetitionResultHandler();
            R.DecisionHandler = new RepeatIgnoreFails( 3 );
            
            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();
            
            Assert.AreEqual( ResultEnum.PASS, R.Result.Result );
            Assert.AreEqual( 3, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 2, manager.NumPass_Reported_Single );
            Assert.AreEqual( 1, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 3, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );
            //no composites in this test - a single step is repeated 3 times
        }
    }

    //uses RepeatWhilePass decision
    [TestFixture]
    public class CustomResultMap_Single_Repeated
    {
        [Test]
        public void CustomResultMap_Fail()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            var results = new List<ResultEnum>( );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.INCONCLUSIVE );
            results.Add( ResultEnum.PASS );
            TestStepSingle R = new TestStepSingle( new PresetResultTest( results ), manager );
            var map = new CustomResultMapping( );
            map.ValueInconclusive = ResultEnum.INCONCLUSIVE;
            R.SingleRunResultHandler = map;
			R.RepetitionResultHandler = new WorstCaseRepetitionResultHandler();
            R.DecisionHandler = new RepeatWhilePass( 3 );

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.INCONCLUSIVE, R.Result.Result );

            Assert.AreEqual( 2, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 1, manager.NumPass_Reported_Single );
            Assert.AreEqual( 1, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 1, manager.NumPass_Processed_Single );
            Assert.AreEqual( 1, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );
        }
    }

    //uses RepeatUntilCancel decision
    [TestFixture]
    public class RepeatUntilCancel_Single_Repeated
    {
        [Test]
        public void CustomResultMap_Canceled()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            var results = new List<ResultEnum>( );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.INCONCLUSIVE );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.FAIL ); // mapped to Cancel, should stop here
            results.Add( ResultEnum.PASS );

            TestStepSingle R = new TestStepSingle( new PresetResultTest( results ), manager );
            var map = new CustomResultMapping( );
            map.ValueInconclusive = ResultEnum.INCONCLUSIVE;
            map.ValuePass = ResultEnum.INCONCLUSIVE;
            map.ValueFail = ResultEnum.CANCELED;

            R.SingleRunResultHandler = map;
			R.RepetitionResultHandler = new WorstCaseRepetitionResultHandler();
            R.DecisionHandler = new RepeatUntilCancel( );

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.CANCELED, R.Result.Result );

            Assert.AreEqual( 4, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 1, manager.NumFail_Reported_Single );
            Assert.AreEqual( 2, manager.NumPass_Reported_Single );
            Assert.AreEqual( 1, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 1, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 0, manager.NumPass_Processed_Single );
            Assert.AreEqual( 3, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );
        }
		
		[Test]
        public void WorstCaseInManyRepeats()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            var results = new List<ResultEnum>( );
            results.Add( ResultEnum.PASS );
			results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.INCONCLUSIVE );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.PASS );
			results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.PASS );
			results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.PASS );
			
			TestStepComposite R = new TestStepComposite();
			R.CompositeResultHandler = new WorstCaseCompositeResultHandler();
			R.RepetitionResultHandler = new WorstCaseRepetitionResultHandler();
            R.DecisionHandler = new RepeatIgnoreFails(2);
			
			TestStepComposite A1 = new TestStepComposite();
			A1.DecisionHandler = new RepeatIgnoreFails(2);
			A1.CompositeResultHandler = new WorstCaseCompositeResultHandler();
			A1.RepetitionResultHandler = new WorstCaseRepetitionResultHandler();
            
			R.Steps.Add(A1);
			
            TestStepSingle tc = new TestStepSingle( new PresetResultTest( results ), manager );
            tc.DecisionHandler = new RepeatIgnoreFails(2);
			tc.RepetitionResultHandler = new WorstCaseRepetitionResultHandler();
			A1.Steps.Add(tc);

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.INCONCLUSIVE, R.Result.Result );
			
            Assert.AreEqual( 8, manager.NumTestCasesRun );
            Assert.AreEqual( 6, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 7, manager.NumPass_Reported_Single );
            Assert.AreEqual( 1, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 7, manager.NumPass_Processed_Single );
            Assert.AreEqual( 1, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );
        }
    }

    //uses RepeatWhilePass decision
    [TestFixture]
    public class RepeatWhilePass_Single_Repeated
    {
        [Test]
        public void IgnoreFailure()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            var results = new List<ResultEnum>( );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.INCONCLUSIVE );
            results.Add( ResultEnum.PASS );
            results.Add( ResultEnum.FAIL); 
            results.Add( ResultEnum.PASS );

            TestStepSingle R = new TestStepSingle( new PresetResultTest( results ), manager );
            R.SingleRunResultHandler = new IgnoreFailureSingleResultHandler();
            R.DecisionHandler = new RepeatWhilePass( 5 );

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.PASS, R.Result.Result );

            Assert.AreEqual( 5, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 1, manager.NumFail_Reported_Single );
            Assert.AreEqual( 3, manager.NumPass_Reported_Single );
            Assert.AreEqual( 1, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 5, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );
        }
    }
}

