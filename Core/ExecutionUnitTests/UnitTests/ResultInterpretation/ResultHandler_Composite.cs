﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.ExecutionTest;
using NUnit.Framework;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.ExecutionQueue.ResultHandler;
using yats.TestCase.Interface;
using yats.TestCase.Yats;
using yats.TestRepositoryManager.Interface;
using yats.TestRun;

namespace yats.ExecutionTest
{
    //Default composite step result handler reports NOT_RUN or CANCELED if disabled or step was canceled. All other results are interpreted as PASS
    [TestFixture]
    public class DefaultResultHandler_Composite
    {
        public static TestStepComposite SetupTestScenarioWithFailure(ITestRepositoryManager manager)
        {
            TestStepComposite R = new TestStepComposite( );

            var A1 = new TestStepSingle( new AlwaysPass( ), manager );
            R.Steps.Add( A1 );

            var A2 = new TestStepSingle( new AlwaysFail( ), manager );
            R.Steps.Add( A2 );

            var A3 = new TestStepSingle( new AlwaysPass( ), manager );
            R.Steps.Add( A3 );

            return R;
        }

        public static TestStepComposite SetupTestScenarioWithCancel(ITestRepositoryManager manager)
        {
            TestStepComposite R = new TestStepComposite( );

            var A1 = new TestStepSingle( new AlwaysPass( ), manager );
            R.Steps.Add( A1 );

            var A2 = new TestStepSingle( new AlwaysCanceled( ), manager );
            R.Steps.Add( A2 );

            var A3 = new TestStepSingle( new AlwaysPass( ), manager );
            R.Steps.Add( A3 );

            return R;
        }

        //expect to report the composite run result as NOT_RUN
        [Test]
        public void DefaultResultHandler_NotRun()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );
            TestStepComposite root = SetupTestScenarioWithFailure( manager );
            root.ExecutionMethod = new StepDisabledModifier( );
            
            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();

            Assert.AreEqual( root.Result.Result, ResultEnum.NOT_RUN );

            Assert.AreEqual( 0, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Single );
            Assert.AreEqual( 0, manager.NumFail_Single );
            Assert.AreEqual( 0, manager.NumPass_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 0, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 0, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 0, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 0, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );

            Assert.AreEqual( 1, manager.NumTestRunsStarted );
            Assert.AreEqual( 1, manager.NumTestRunsFinished );
            Assert.AreEqual( 0, manager.NumTestStepStartReported );
        }

        //expect to report the composite run result as CANCELED
        [Test]
        public void DefaultResultHandler_Canceled()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );
            TestStepComposite root = SetupTestScenarioWithCancel( manager );

            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.CANCELED, root.Result.Result);

            Assert.AreEqual( 2, manager.NumTestCasesRun );
            Assert.AreEqual( 1, manager.NumCompositesRun );

            Assert.AreEqual( 1, manager.NumCanceled_Single );
            Assert.AreEqual( 0, manager.NumFail_Single );
            Assert.AreEqual( 1, manager.NumPass_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Single );

            Assert.AreEqual( 1, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 1, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 1, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 1, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 1, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 1, manager.NumNotRun_Processed_Single );

            Assert.AreEqual( 1, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 0, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 1, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 0, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );

            Assert.AreEqual( 1, manager.NumTestRunsStarted );
            Assert.AreEqual( 1, manager.NumTestRunsFinished );
            Assert.AreEqual( 1 + 3, manager.NumTestStepStartReported );
        }

        //Only pass results, expect to report the composite run result as PASS
        [Test]
        public void DefaultResultHandler_PassOnly()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );
            
            TestStepComposite R = new TestStepComposite( );
            R.Steps.Add( new TestStepSingle( new AlwaysPass( ), manager ) );
            R.Steps.Add( new TestStepSingle( new AlwaysPass( ), manager ) );
            R.Steps.Add( new TestStepSingle( new AlwaysPass( ), manager ) );

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.PASS, R.Result.Result );

            Assert.AreEqual( 3, manager.NumTestCasesRun );
            Assert.AreEqual( 1, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Single );
            Assert.AreEqual( 0, manager.NumFail_Single );
            Assert.AreEqual( 3, manager.NumPass_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 3, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 3, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 1, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 1, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );

            Assert.AreEqual( 1, manager.NumTestRunsStarted );
            Assert.AreEqual( 1, manager.NumTestRunsFinished );
            Assert.AreEqual( 1 + 3, manager.NumTestStepStartReported );
        }

        //Only fail results, but Default Composite result handler is used. Expect to report the composite run result as PASS
        [Test]
        public void DefaultResultHandler_FailOnly()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            TestStepComposite R = new TestStepComposite( );
            R.Steps.Add( new TestStepSingle( new AlwaysFail( ), manager ) );
            R.Steps.Add( new TestStepSingle( new AlwaysFail( ), manager ) );
            R.Steps.Add( new TestStepSingle( new AlwaysFail( ), manager ) );

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.PASS, R.Result.Result );

            Assert.AreEqual( 3, manager.NumTestCasesRun );
            Assert.AreEqual( 1, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Single );
            Assert.AreEqual( 3, manager.NumFail_Single );
            Assert.AreEqual( 0, manager.NumPass_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 3, manager.NumFail_Reported_Single );
            Assert.AreEqual( 0, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 3, manager.NumFail_Processed_Single );
            Assert.AreEqual( 0, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 1, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 1, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );

            Assert.AreEqual( 1, manager.NumTestRunsStarted );
            Assert.AreEqual( 1, manager.NumTestRunsFinished );
            Assert.AreEqual( 1 + 3, manager.NumTestStepStartReported );
        }

        //Only inconclusive results, but Default Composite result handler is used. Expect to report the composite run result as PASS
        [Test]
        public void DefaultResultHandler_InconclusiveOnly()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            TestStepComposite R = new TestStepComposite( );
            R.Steps.Add( new TestStepSingle( new AlwaysInconclusive( ), manager ) );
            R.Steps.Add( new TestStepSingle( new AlwaysInconclusive( ), manager ) );
            R.Steps.Add( new TestStepSingle( new AlwaysInconclusive( ), manager ) );

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.PASS, R.Result.Result );

            Assert.AreEqual( 3, manager.NumTestCasesRun );
            Assert.AreEqual( 1, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Single );
            Assert.AreEqual( 0, manager.NumFail_Single );
            Assert.AreEqual( 0, manager.NumPass_Single );
            Assert.AreEqual( 3, manager.NumInconclusive_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 0, manager.NumPass_Reported_Single );
            Assert.AreEqual( 3, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 0, manager.NumPass_Processed_Single );
            Assert.AreEqual( 3, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 1, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 1, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );

            Assert.AreEqual( 1, manager.NumTestRunsStarted );
            Assert.AreEqual( 1, manager.NumTestRunsFinished );
            Assert.AreEqual( 1 + 3, manager.NumTestStepStartReported );
        }
    }

    //"Worst case" composite step result handler reports NOT_RUN or CANCELED if disabled or step was canceled. 
    //Otherwise, the 'worst' of all other results is selected
    public class WorstCaseResultHandler_Composite
    {
        static TestStepComposite SetupTestScenarioWithFailure(ITestRepositoryManager manager)
        {
            TestStepComposite R = new TestStepComposite( );

            var A1 = new TestStepSingle( new AlwaysPass( ), manager );
            R.Steps.Add( A1 );

            var A2 = new TestStepSingle( new AlwaysFail( ), manager );
            R.Steps.Add( A2 );

            var A3 = new TestStepSingle( new AlwaysPass( ), manager );
            R.Steps.Add( A3 );

            return R;
        }

        static TestStepComposite SetupTestScenarioWithCancel(ITestRepositoryManager manager)
        {
            TestStepComposite R = new TestStepComposite( );

            var A1 = new TestStepSingle( new AlwaysPass( ), manager );
            R.Steps.Add( A1 );

            var A2 = new TestStepSingle( new AlwaysCanceled( ), manager );
            R.Steps.Add( A2 );

            var A3 = new TestStepSingle( new AlwaysPass( ), manager );
            R.Steps.Add( A3 );

            return R;
        }

        //expect to report the composite run result as NOT_RUN
        [Test]
        public void DefaultResultHandler_NotRun()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );
            TestStepComposite root = SetupTestScenarioWithFailure( manager );
            root.ExecutionMethod = new StepDisabledModifier( );
            root.CompositeResultHandler = new WorstCaseCompositeResultHandler();

            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();

            Assert.AreEqual( root.Result.Result, ResultEnum.NOT_RUN );

            Assert.AreEqual( 0, manager.NumTestCasesRun );
            Assert.AreEqual( 0, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Single );
            Assert.AreEqual( 0, manager.NumFail_Single );
            Assert.AreEqual( 0, manager.NumPass_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 0, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 0, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 0, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 0, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );

            Assert.AreEqual( 1, manager.NumTestRunsStarted );
            Assert.AreEqual( 1, manager.NumTestRunsFinished );
            Assert.AreEqual( 0, manager.NumTestStepStartReported );
        }

        //expect to report the composite run result as CANCELED
        [Test]
        public void DefaultResultHandler_Canceled()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );
            TestStepComposite root = SetupTestScenarioWithCancel( manager );
            root.CompositeResultHandler = new WorstCaseCompositeResultHandler();
            
            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.CANCELED, root.Result.Result );

            Assert.AreEqual( 2, manager.NumTestCasesRun );
            Assert.AreEqual( 1, manager.NumCompositesRun );

            Assert.AreEqual( 1, manager.NumCanceled_Single );
            Assert.AreEqual( 0, manager.NumFail_Single );
            Assert.AreEqual( 1, manager.NumPass_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Single );

            Assert.AreEqual( 1, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 1, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 1, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 1, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 1, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 1, manager.NumNotRun_Processed_Single );

            Assert.AreEqual( 1, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 0, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 1, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 0, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );

            Assert.AreEqual( 1, manager.NumTestRunsStarted );
            Assert.AreEqual( 1, manager.NumTestRunsFinished );
            Assert.AreEqual( 1 + 3, manager.NumTestStepStartReported );
        }

        //Only pass results, expect to report the composite run result as PASS
        [Test]
        public void DefaultResultHandler_PassOnly()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            TestStepComposite R = new TestStepComposite( );
            R.Steps.Add( new TestStepSingle( new AlwaysPass( ), manager ) );
            R.Steps.Add( new TestStepSingle( new AlwaysPass( ), manager ) );
            R.Steps.Add( new TestStepSingle( new AlwaysPass( ), manager ) );
            R.CompositeResultHandler = new WorstCaseCompositeResultHandler();

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.PASS, R.Result.Result );

            Assert.AreEqual( 3, manager.NumTestCasesRun );
            Assert.AreEqual( 1, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Single );
            Assert.AreEqual( 0, manager.NumFail_Single );
            Assert.AreEqual( 3, manager.NumPass_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 3, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 3, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 1, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 1, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );

            Assert.AreEqual( 1, manager.NumTestRunsStarted );
            Assert.AreEqual( 1, manager.NumTestRunsFinished );
            Assert.AreEqual( 1 + 3, manager.NumTestStepStartReported );
        }

        //Only fail results, but Default Composite result handler is used. Expect to report the composite run result as PASS
        [Test]
        public void DefaultResultHandler_FailOnly()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            TestStepComposite R = new TestStepComposite( );
            R.Steps.Add( new TestStepSingle( new AlwaysFail( ), manager ) );
            R.Steps.Add( new TestStepSingle( new AlwaysFail( ), manager ) );
            R.Steps.Add( new TestStepSingle( new AlwaysFail( ), manager ) );
            R.CompositeResultHandler = new WorstCaseCompositeResultHandler();

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.FAIL, R.Result.Result );

            Assert.AreEqual( 3, manager.NumTestCasesRun );
            Assert.AreEqual( 1, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Single );
            Assert.AreEqual( 3, manager.NumFail_Single );
            Assert.AreEqual( 0, manager.NumPass_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 3, manager.NumFail_Reported_Single );
            Assert.AreEqual( 0, manager.NumPass_Reported_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 3, manager.NumFail_Processed_Single );
            Assert.AreEqual( 0, manager.NumPass_Processed_Single );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 1, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 0, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 1, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 0, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 0, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );

            Assert.AreEqual( 1, manager.NumTestRunsStarted );
            Assert.AreEqual( 1, manager.NumTestRunsFinished );
            Assert.AreEqual( 1 + 3, manager.NumTestStepStartReported );
        }

        //Only inconclusive results, but Default Composite result handler is used. Expect to report the composite run result as PASS
        [Test]
        public void DefaultResultHandler_InconclusiveOnly()
        {
            TestRunner testRunner = new TestRunner( );
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub( testRunner );

            TestStepComposite R = new TestStepComposite( );
            R.Steps.Add( new TestStepSingle( new AlwaysInconclusive( ), manager ) );
            R.Steps.Add( new TestStepSingle( new AlwaysInconclusive( ), manager ) );
            R.Steps.Add( new TestStepSingle( new AlwaysInconclusive( ), manager ) );
            R.CompositeResultHandler = new WorstCaseCompositeResultHandler();

            testRunner.SetTestRun(new TestRunImplementation(R));
            testRunner.StartTestRun();

            Assert.AreEqual( ResultEnum.INCONCLUSIVE, R.Result.Result );

            Assert.AreEqual( 3, manager.NumTestCasesRun );
            Assert.AreEqual( 1, manager.NumCompositesRun );

            Assert.AreEqual( 0, manager.NumCanceled_Single );
            Assert.AreEqual( 0, manager.NumFail_Single );
            Assert.AreEqual( 0, manager.NumPass_Single );
            Assert.AreEqual( 3, manager.NumInconclusive_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Single );
            Assert.AreEqual( 0, manager.NumFail_Reported_Single );
            Assert.AreEqual( 0, manager.NumPass_Reported_Single );
            Assert.AreEqual( 3, manager.NumInconclusive_Reported_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Single );
            Assert.AreEqual( 0, manager.NumFail_Processed_Single );
            Assert.AreEqual( 0, manager.NumPass_Processed_Single );
            Assert.AreEqual( 3, manager.NumInconclusive_Processed_Single );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Single );

            Assert.AreEqual( 0, manager.NumCanceled_Reported_Composite );
            Assert.AreEqual( 0, manager.NumFail_Reported_Composite );
            Assert.AreEqual( 0, manager.NumPass_Reported_Composite );
            Assert.AreEqual( 1, manager.NumInconclusive_Reported_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Reported_Composite );

            Assert.AreEqual( 0, manager.NumCanceled_Processed_Composite );
            Assert.AreEqual( 0, manager.NumFail_Processed_Composite );
            Assert.AreEqual( 0, manager.NumPass_Processed_Composite );
            Assert.AreEqual( 1, manager.NumInconclusive_Processed_Composite );
            Assert.AreEqual( 0, manager.NumNotRun_Processed_Composite );

            Assert.AreEqual( 1, manager.NumTestRunsStarted );
            Assert.AreEqual( 1, manager.NumTestRunsFinished );
            Assert.AreEqual( 1 + 3, manager.NumTestStepStartReported );
        }
    }
}
