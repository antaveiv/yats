﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using NUnit.Framework;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.ExecutionQueue.ResultHandler;
using yats.TestCase.Yats;
using yats.TestRun;
using yats.TestCase.Interface;


namespace yats.ExecutionUnitTests.UnitTests.ResultInterpretation
{
    [TestFixture]
    public class BestCaseRepetitionResultHandlerTests
    {
        [Test]
        public void PassNotOverridden()
        {
            BestCaseRepetitionResultHandler handler = new BestCaseRepetitionResultHandler();
            Assert.IsNull(handler.Current);
            handler.Update(new TestResult(ResultEnum.PASS));
            Assert.NotNull(handler.Current);
            Assert.AreEqual(ResultEnum.PASS, handler.Current.Result);
            handler.Update(new TestResult(ResultEnum.FAIL));
            //handler.Update(new TestResult(ResultEnum.CANCELED));
            handler.Update(new TestResult(ResultEnum.INCONCLUSIVE));
            //handler.Update(new TestResult(ResultEnum.NOT_RUN));
            Assert.AreEqual(ResultEnum.PASS, handler.Current.Result);
        }

        [Test]
        public void NotRun()
        {
            BestCaseRepetitionResultHandler handler = new BestCaseRepetitionResultHandler();
            Assert.IsNull(handler.Current);
            handler.Update(new TestResult(ResultEnum.NOT_RUN));
            handler.Update(new TestResult(ResultEnum.FAIL));
            //handler.Update(new TestResult(ResultEnum.CANCELED));
            handler.Update(new TestResult(ResultEnum.INCONCLUSIVE)); 
            handler.Update(new TestResult(ResultEnum.PASS));
            handler.Update(new TestResult(ResultEnum.FAIL));
            //handler.Update(new TestResult(ResultEnum.CANCELED));
            handler.Update(new TestResult(ResultEnum.INCONCLUSIVE));            
            Assert.NotNull(handler.Current);
            Assert.AreEqual(ResultEnum.PASS, handler.Current.Result);
        }

        [Test]
        public void Inconclusive()
        {
            BestCaseRepetitionResultHandler handler = new BestCaseRepetitionResultHandler();
            Assert.IsNull(handler.Current);
            handler.Update(new TestResult(ResultEnum.CANCELED));
            handler.Update(new TestResult(ResultEnum.INCONCLUSIVE));
            handler.Update(new TestResult(ResultEnum.NOT_RUN));
            Assert.AreEqual(ResultEnum.INCONCLUSIVE, handler.Current.Result);
        }
    }
}
