/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using NUnit.Framework;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.ExecutionQueue.CompositeStepExecuteDecision;
using yats.ExecutionQueue.DecisionHandler;
using yats.ExecutionQueue.ResultHandler;
using yats.TestCase.Interface;
using yats.TestCase.Yats;
using yats.TestRepositoryManager.Interface;
using yats.TestRun;

namespace yats.ExecutionTest
{
#if DEBUG
    [TestFixture]
	public class TestIgnoreFailures
	{
		static TestStepComposite SetupTestScenario(ITestRepositoryManager manager)
		{
            TestStepComposite R = new TestStepComposite( );
			            
			var A1 = new TestStepSingle( new AlwaysInconclusive(), manager );
			A1.SingleRunResultHandler = new IgnoreFailureSingleResultHandler();
            A1.DecisionHandler = new RepeatWhilePass(2);
			R.Steps.Add(A1);
			
			return R;
		}
	
		[Test]
		public void TestIgnoreFailures_1 ()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			TestStepComposite root = SetupTestScenario( manager );

            ITestRunner testRunner = new TestRunner(new TestRunImplementation(root));
			
            testRunner.TestRunFinished += manager.executeEngine_TestRunFinished;
            testRunner.TestRunStarting += manager.executeEngine_TestRunStarting;
            testRunner.TestStepResult += manager.executeEngine_TestStepResult;
            testRunner.TestStepRepetitionResult += manager.executeEngine_TestStepRepetitionResult;
            testRunner.TestStepStarting += manager.executeEngine_TestStepStarting;

            testRunner.StartTestRun();
			
			Assert.AreEqual(2, manager.NumTestCasesRun);
			Assert.AreEqual(1, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Single);
			Assert.AreEqual(0, manager.NumFail_Single);
			Assert.AreEqual(0, manager.NumPass_Single);
			Assert.AreEqual(2, manager.NumInconclusive_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(0, manager.NumPass_Reported_Single);
			Assert.AreEqual(2, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(2, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Composite);
			Assert.AreEqual(0, manager.NumFail_Reported_Composite);
			Assert.AreEqual(1, manager.NumPass_Reported_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Composite);
						
			Assert.AreEqual(0, manager.NumCanceled_Processed_Composite);
			Assert.AreEqual(0, manager.NumFail_Processed_Composite);
			Assert.AreEqual(1, manager.NumPass_Processed_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Composite);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(3, manager.NumTestStepStartReported);
		}
		
		static TestStepComposite SetupTestScenarioWithResultMap(ITestRepositoryManager manager)
		{
            TestStepComposite R = new TestStepComposite( );
			            
			var A1 = new TestStepSingle( new AlwaysInconclusive(), manager );
			var map = new CustomResultMapping();
			map.ValueFail = ResultEnum.PASS;
			map.ValueInconclusive = ResultEnum.PASS;
			A1.SingleRunResultHandler = map;
			R.Steps.Add(A1);

			var A2 = new TestStepSingle( new AlwaysFail(), manager );
			map = new CustomResultMapping();
			map.ValueFail = ResultEnum.PASS;
			map.ValueInconclusive = ResultEnum.PASS;
			A2.SingleRunResultHandler = map;
			R.Steps.Add(A2);

			var A3 = new TestStepSingle( new AlwaysPass(), manager );
			map = new CustomResultMapping();
			map.ValueFail = ResultEnum.PASS;
			map.ValueInconclusive = ResultEnum.PASS;
			A3.SingleRunResultHandler = map;
			R.Steps.Add(A3);

			return R;
		}
	
		[Test]
		public void TestMinPasses_custom_value_map ()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			TestStepComposite root = SetupTestScenarioWithResultMap( manager );

            ITestRunner testRunner = new TestRunner(new TestRunImplementation(root));
			
            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += (manager.executeEngine_TestStepResult);
            testRunner.TestStepRepetitionResult += (manager.executeEngine_TestStepRepetitionResult);
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.StartTestRun();
			
			Assert.AreEqual(3, manager.NumTestCasesRun);
			Assert.AreEqual(1, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Single);
			Assert.AreEqual(1, manager.NumFail_Single);
			Assert.AreEqual(1, manager.NumPass_Single);
			Assert.AreEqual(1, manager.NumInconclusive_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(1, manager.NumFail_Reported_Single);
			Assert.AreEqual(1, manager.NumPass_Reported_Single);
			Assert.AreEqual(1, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(3, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Composite);
			Assert.AreEqual(0, manager.NumFail_Reported_Composite);
			Assert.AreEqual(1, manager.NumPass_Reported_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Composite);
						
			Assert.AreEqual(0, manager.NumCanceled_Processed_Composite);
			Assert.AreEqual(0, manager.NumFail_Processed_Composite);
			Assert.AreEqual(1, manager.NumPass_Processed_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Composite);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(4, manager.NumTestStepStartReported);
		}
		
		static TestStepComposite SetupTestScenarioWithFailure(ITestRepositoryManager manager)
		{
            TestStepComposite R = new TestStepComposite( );
			            
			var A1 = new TestStepSingle( new AlwaysPass(), manager );
			R.Steps.Add(A1);

			var A2 = new TestStepSingle( new AlwaysFail(), manager );
			R.Steps.Add(A2);

			var A3 = new TestStepSingle( new AlwaysPass(), manager );
			R.Steps.Add(A3);

			return R;
		}
		
		//expect to stop executing composite test step on first child failure
		[Test]
		public void CompositeStepExecution_stop_on_fail ()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			TestStepComposite root = SetupTestScenarioWithFailure( manager );
			root.StepDecisionHandler = new ExecuteWhilePassing();
			root.CompositeResultHandler = new WorstCaseCompositeResultHandler();

            ITestRunner testRunner = new TestRunner(new TestRunImplementation(root));
			
            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += (manager.executeEngine_TestStepResult);
            testRunner.TestStepRepetitionResult += (manager.executeEngine_TestStepRepetitionResult);
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.StartTestRun();
			
			Assert.AreEqual(2, manager.NumTestCasesRun);
			Assert.AreEqual(1, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Single);
			Assert.AreEqual(1, manager.NumFail_Single);
			Assert.AreEqual(1, manager.NumPass_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(1, manager.NumFail_Reported_Single);
			Assert.AreEqual(1, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(1, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(1, manager.NumFail_Processed_Single);
			Assert.AreEqual(1, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(1, manager.NumNotRun_Processed_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Composite);
			Assert.AreEqual(1, manager.NumFail_Reported_Composite);
			Assert.AreEqual(0, manager.NumPass_Reported_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Composite);
						
			Assert.AreEqual(0, manager.NumCanceled_Processed_Composite);
			Assert.AreEqual(1, manager.NumFail_Processed_Composite);
			Assert.AreEqual(0, manager.NumPass_Processed_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Composite);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(4, manager.NumTestStepStartReported);
		}
		
		//expected to continue running Root group even when one of the steps fails. Should stop if canceled but it is not done in this scenario
		[Test]
		public void CompositeStepExecution_ignore_fail ()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			TestStepComposite root = SetupTestScenarioWithFailure( manager );
			root.StepDecisionHandler = new ExecuteUntilCanceled();
			root.CompositeResultHandler = new WorstCaseCompositeResultHandler();
            ITestRunner testRunner = new TestRunner(new TestRunImplementation(root));
			
            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += (manager.executeEngine_TestStepResult);
            testRunner.TestStepRepetitionResult += (manager.executeEngine_TestStepRepetitionResult);
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.StartTestRun();
			
			Assert.AreEqual(3, manager.NumTestCasesRun);
			Assert.AreEqual(1, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Single);
			Assert.AreEqual(1, manager.NumFail_Single);
			Assert.AreEqual(2, manager.NumPass_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(1, manager.NumFail_Reported_Single);
			Assert.AreEqual(2, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(1, manager.NumFail_Processed_Single);
			Assert.AreEqual(2, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Composite);
			Assert.AreEqual(1, manager.NumFail_Reported_Composite);
			Assert.AreEqual(0, manager.NumPass_Reported_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Composite);
						
			Assert.AreEqual(0, manager.NumCanceled_Processed_Composite);
			Assert.AreEqual(1, manager.NumFail_Processed_Composite);
			Assert.AreEqual(0, manager.NumPass_Processed_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Composite);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(4, manager.NumTestStepStartReported);
		}
		
		static TestStepComposite SetupTestScenarioWithCancel(ITestRepositoryManager manager)
		{
            TestStepComposite R = new TestStepComposite( );
			            
			var A1 = new TestStepSingle( new AlwaysPass(), manager );
			R.Steps.Add(A1);

			var A2 = new TestStepSingle( new AlwaysCanceled(), manager );
			R.Steps.Add(A2);

			var A3 = new TestStepSingle( new AlwaysPass(), manager );
			R.Steps.Add(A3);

			return R;
		}
		
		//expected to stop after the second step is canceled. The overall result should also be Canceled
		[Test]
		public void CompositeStepExecution_stop_on_cancel ()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			TestStepComposite root = SetupTestScenarioWithCancel( manager );
			root.StepDecisionHandler = new ExecuteUntilCanceled();
			root.CompositeResultHandler = new WorstCaseCompositeResultHandler();

            ITestRunner testRunner = new TestRunner(new TestRunImplementation(root));
			
            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += (manager.executeEngine_TestStepResult);
            testRunner.TestStepRepetitionResult += (manager.executeEngine_TestStepRepetitionResult);
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.StartTestRun(); 
			
			Assert.AreEqual(2, manager.NumTestCasesRun);
			Assert.AreEqual(1, manager.NumCompositesRun);
			
			Assert.AreEqual(1, manager.NumCanceled_Single);
			Assert.AreEqual(0, manager.NumFail_Single);
			Assert.AreEqual(1, manager.NumPass_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Single);
			
			Assert.AreEqual(1, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(0, manager.NumFail_Reported_Single);
			Assert.AreEqual(1, manager.NumPass_Reported_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(1, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(1, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(0, manager.NumFail_Processed_Single);
			Assert.AreEqual(1, manager.NumPass_Processed_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(1, manager.NumNotRun_Processed_Single);
			
			Assert.AreEqual(1, manager.NumCanceled_Reported_Composite);
			Assert.AreEqual(0, manager.NumFail_Reported_Composite);
			Assert.AreEqual(0, manager.NumPass_Reported_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Composite);
						
			Assert.AreEqual(1, manager.NumCanceled_Processed_Composite);
			Assert.AreEqual(0, manager.NumFail_Processed_Composite);
			Assert.AreEqual(0, manager.NumPass_Processed_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Composite);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(4, manager.NumTestStepStartReported);
		}
		
		
		static TestStepComposite SetupRandomTestRepeat(ITestRepositoryManager manager)
		{
            TestStepComposite R = new TestStepComposite( );
			            
			var A1 = new TestStepSingle( new RandomResult(), manager );
			R.Steps.Add(A1);
			A1.DecisionHandler = new RepeatIgnoreFails(10);
			
			A1 = new TestStepSingle( new AlwaysFail(), manager );
			R.Steps.Add(A1);
			A1.DecisionHandler = new RepeatIgnoreFails(10);
            A1.RepetitionResultHandler = new WorstCaseRepetitionResultHandler( );
			
			return R;
		}
		
		//expect to continue executing TC repetitions. Some repetitions may fail, but the results are ignored
		[Test]
		public void SingleStepRepeat_continue_even_if_fails ()
		{
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
			TestStepComposite root = SetupRandomTestRepeat( manager );
			root.CompositeResultHandler = new WorstCaseCompositeResultHandler();
            root.RepetitionResultHandler = new WorstCaseRepetitionResultHandler( );

            ITestRunner testRunner = new TestRunner(new TestRunImplementation(root));
			
            testRunner.TestRunFinished += ( manager.executeEngine_TestRunFinished );
            testRunner.TestRunStarting += ( manager.executeEngine_TestRunStarting );
            testRunner.TestStepResult += (manager.executeEngine_TestStepResult);
            testRunner.TestStepRepetitionResult += (manager.executeEngine_TestStepRepetitionResult);
            testRunner.TestStepStarting += ( manager.executeEngine_TestStepStarting );

            testRunner.StartTestRun();
			
			Assert.AreEqual(20, manager.NumTestCasesRun);
			Assert.AreEqual(1, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Composite);
			Assert.AreEqual(1, manager.NumFail_Reported_Composite);
			Assert.AreEqual(0, manager.NumPass_Reported_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Composite);
						
			Assert.AreEqual(0, manager.NumCanceled_Processed_Composite);
			Assert.AreEqual(1, manager.NumFail_Processed_Composite);
			Assert.AreEqual(0, manager.NumPass_Processed_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Composite);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(21, manager.NumTestStepStartReported);
		}
		
		
		static TestStepComposite SetupRandomTestRepeatWhilePass(ITestRepositoryManager manager)
		{
            TestStepComposite R = new TestStepComposite( );
			            
			var A1 = new TestStepSingle( new RandomResult(), manager );
			R.Steps.Add(A1);
			A1.DecisionHandler = new RepeatWhilePass(2000);
            A1.RepetitionResultHandler = new WorstCaseRepetitionResultHandler( );
			return R;
		}
		
		//expect to stop executing composite test step on first failure
		[Test]
		public void SingleStepRepeat_stop_if_fails ()
		{
            TestRunner testRunner = new TestRunner();
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			TestStepComposite root = SetupRandomTestRepeatWhilePass( manager );
			root.CompositeResultHandler = new WorstCaseCompositeResultHandler();
            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();
			
			Assert.Less(manager.NumTestCasesRun, 2001);
			Assert.AreEqual(1, manager.NumCompositesRun);
            
            Assert.AreEqual(1, manager.NumFail_Reported_Single );

			Assert.AreEqual(0, manager.NumCanceled_Reported_Composite);
			Assert.AreEqual(1, manager.NumFail_Reported_Composite);
			Assert.AreEqual(0, manager.NumPass_Reported_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Composite);
						
			Assert.AreEqual(0, manager.NumCanceled_Processed_Composite);
			Assert.AreEqual(1, manager.NumFail_Processed_Composite);
			Assert.AreEqual(0, manager.NumPass_Processed_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Composite);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.Less(manager.NumTestStepStartReported, 2001);
		}
		
		static TestStepComposite SetupWithCrash(ITestRepositoryManager manager)
		{
            TestStepComposite R = new TestStepComposite( );
			            
			var A1 = new TestStepSingle( new AlwaysFail(), manager );
			R.Steps.Add(A1);
			A1 = new TestStepSingle( new AlwaysCrash(), manager );
			R.Steps.Add(A1);
			A1 = new TestStepSingle( new AlwaysPass(), manager );
			R.Steps.Add(A1);
			R.StepDecisionHandler = new ExecuteUntilCanceled();
			return R;
		}
		
		[Test]
		public void Continues_when_TC_crashes ()
		{
            TestRunner testRunner = new TestRunner();
			TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub(testRunner);
			
			TestStepComposite root = SetupWithCrash( manager );            
			root.CompositeResultHandler = new WorstCaseCompositeResultHandler();
            testRunner.SetTestRun(new TestRunImplementation(root));
            testRunner.StartTestRun();
			
			Assert.AreEqual(3, manager.NumTestCasesRun);
			Assert.AreEqual(1, manager.NumCompositesRun);
			
			Assert.AreEqual(0, manager.NumCanceled_Single);
			Assert.AreEqual(1, manager.NumFail_Single);
			Assert.AreEqual(1, manager.NumPass_Single);
			Assert.AreEqual(0, manager.NumInconclusive_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Single);
			Assert.AreEqual(1, manager.NumFail_Reported_Single);
			Assert.AreEqual(1, manager.NumPass_Reported_Single);
			Assert.AreEqual(1, manager.NumInconclusive_Reported_Single);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Processed_Single);
			Assert.AreEqual(1, manager.NumFail_Processed_Single);
			Assert.AreEqual(1, manager.NumPass_Processed_Single);
			Assert.AreEqual(1, manager.NumInconclusive_Processed_Single);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Single);
			
			Assert.AreEqual(0, manager.NumCanceled_Reported_Composite);
			Assert.AreEqual(1, manager.NumFail_Reported_Composite);
			Assert.AreEqual(0, manager.NumPass_Reported_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Reported_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Reported_Composite);
						
			Assert.AreEqual(0, manager.NumCanceled_Processed_Composite);
			Assert.AreEqual(1, manager.NumFail_Processed_Composite);
			Assert.AreEqual(0, manager.NumPass_Processed_Composite);
			Assert.AreEqual(0, manager.NumInconclusive_Processed_Composite);
			Assert.AreEqual(0, manager.NumNotRun_Processed_Composite);
			
			Assert.AreEqual(1, manager.NumTestRunsStarted);
			Assert.AreEqual(1, manager.NumTestRunsFinished);
			Assert.AreEqual(4, manager.NumTestStepStartReported);
		}
		
	}
#endif
}

