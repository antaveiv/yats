﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using NUnit.Framework;
using yats.ExecutionQueue;
using yats.ExecutionQueue.DecisionHandler;
using yats.ExecutionQueue.ResultHandler;
using yats.TestCase.Yats;

namespace yats.ExecutionTest
{
    [TestFixture]
    public class GetNumberOfStepsVisitorTests
    {
        [Test]
        public void CanNotEstimateRepeatUntilCanceled()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            R.DecisionHandler = new SingleRun();

            var A1 = new TestStepSingle(new AlwaysInconclusive(), manager);
            A1.SingleRunResultHandler = new IgnoreFailureSingleResultHandler();
            A1.DecisionHandler = new RepeatUntilCancel();
            R.Steps.Add(A1);

            int result;
            bool canEstimate = GetNumberOfStepsVisitor.Get(R, R, out result);
            Assert.IsFalse(canEstimate);
        }
        
        [Test]
        public void EstimateRepeatIgnoreFails()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            R.DecisionHandler = new SingleRun();

            TestStepComposite R2 = new TestStepComposite();
            R2.DecisionHandler = new RepeatIgnoreFails(3);
            R.Steps.Add(R2);

            var A1 = new TestStepSingle(new AlwaysInconclusive(), manager);
            A1.SingleRunResultHandler = new IgnoreFailureSingleResultHandler();
            A1.DecisionHandler = new RepeatIgnoreFails(2);
            R2.Steps.Add(A1);

            int result;
            bool canEstimate = GetNumberOfStepsVisitor.Get(R, R, out result);
            Assert.IsTrue(canEstimate);
            Assert.AreEqual(6, result);
        }

        [Test]
        public void EstimateSkipDisabledSteps()
        {
            TestRepositoryManagerTestStub manager = new TestRepositoryManagerTestStub();
            TestStepComposite R = new TestStepComposite();
            R.DecisionHandler = new SingleRun();

            TestStepComposite R2 = new TestStepComposite();
            R2.DecisionHandler = new RepeatIgnoreFails(3);
            R.Steps.Add(R2);
            TestStepComposite R3 = new TestStepComposite();
            R3.ExecutionMethod = new StepDisabledModifier();
            R3.DecisionHandler = new RepeatIgnoreFails(3);
            R.Steps.Add(R3);
            var A1 = new TestStepSingle(new AlwaysInconclusive(), manager);
            A1.SingleRunResultHandler = new IgnoreFailureSingleResultHandler();
            A1.DecisionHandler = new RepeatIgnoreFails(2);
            R2.Steps.Add(A1);

            A1 = new TestStepSingle(new AlwaysInconclusive(), manager);
            A1.ExecutionMethod = new StepDisabledModifier();
            A1.SingleRunResultHandler = new IgnoreFailureSingleResultHandler();
            A1.DecisionHandler = new RepeatIgnoreFails(2);
            R2.Steps.Add(A1);

            A1 = new TestStepSingle(new AlwaysInconclusive(), manager);
            A1.SingleRunResultHandler = new IgnoreFailureSingleResultHandler();
            A1.DecisionHandler = new RepeatIgnoreFails(2);
            R3.Steps.Add(A1);

            int result;
            bool canEstimate = GetNumberOfStepsVisitor.Get(R, R, out result);
            Assert.IsTrue(canEstimate);
            Assert.AreEqual(6, result);
        }
    }
}
