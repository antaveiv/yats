/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.IO;
using System.Xml.Serialization;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.ExecutionQueue.CompositeStepExecuteDecision;
using yats.ExecutionQueue.DecisionHandler;
using yats.ExecutionQueue.ResultHandler;
using yats.TestCase.Yats;
using yats.TestRepositoryManager.YatsNative;
using yats.TestRun;

namespace yats.ExecutionTest
{
    /// <summary>
    ///This example demonstrates how test execution queues can be created and executed programatically. Event handlers are registered to display the progress
    /// </summary>
    class MainClass
    {
        public static void Main(string[] args)
        {
            new StateMachine().TestSanityCheck_CheckDeadlocks2();
            return;
            /*
                        Console.WriteLine( "Execution queue test" );
                        TestStep root = SetupTestScenario_ShouldAllPass( );

                        XmlSerializer serializer = yats.Utilities.SerializationHelper.GetSerializer(typeof(TestStep));
                        TextWriter tw = new StreamWriter( "test.xml" );
                        serializer.Serialize( tw, root );
                        tw.Close( );

                        //var doc = Utilities.Convert.ObjectToXmlDocument(root);
                        //TestSetupLogger.Log(root);
                        ITestRunner testRunner = new TestRunner(new TestRunImplementation(root));

                        testRunner.TestRunFinished += executeEngine_TestRunFinished;
                        testRunner.TestRunStarting += executeEngine_TestRunStarting;
                        testRunner.TestStepResult += executeEngine_TestStepResult;
                        testRunner.TestStepRepetitionResult += executeEngine_TestStepRepetitionResult;
                        testRunner.TestStepStarting += executeEngine_TestStepStarting;

                        testRunner.StartTestRun();
                        Console.ReadLine( );
            */
        }

        static TestStepComposite SetupTestScenario_ShouldAllPass()
        {
            NativeTestRepositoryManager manager = new NativeTestRepositoryManager();

            TestStepComposite R = new TestStepComposite
            {
                ExecutionMethod = new SequentialExecutionModifier(),
                DecisionHandler = new RepeatWhilePass(2),
                RepetitionResultHandler = new DefaultRepetitionResultHandler(),
                StepDecisionHandler = new ExecuteUntilCanceled()
            };

            var A1 = new TestStepSingle(new AlwaysFail(), manager)
            {
                SingleRunResultHandler = new IgnoreFailureSingleResultHandler()
            };
            R.Steps.Add(A1);
            var A2 = new TestStepSingle(new AlwaysFail(), manager)
            {
                SingleRunResultHandler = new IgnoreFailureSingleResultHandler()
            };
            R.Steps.Add(A2);

            return R;
        }

        static TestStep SetupTestScenario_Single()
        {
            NativeTestRepositoryManager manager = new NativeTestRepositoryManager();

            return new TestStepSingle(new RandomResult(), manager);
        }

        static void ExecuteEngine_TestStepStarting(object sender, TestStepStartingEventArgs e)
        {
            Console.WriteLine(string.Format("Starting {0}", e.Step.Name));
        }

        static void ExecuteEngine_TestStepResult(object sender, TestStepResultEventArgs e)
        {
            Console.WriteLine(string.Format("Result {0} : {1}, {2}", e.Step, e.RawResult.Result, e.EvaluatedResult.Result));
        }

        static void ExecuteEngine_TestStepRepetitionResult(object sender, TestStepRepetitionResultEventArgs e)
        {
            Console.WriteLine(string.Format("Repetition result {0} : {1}", e.Step.GetType().Name, e.Result.Result));
        }

        static void ExecuteEngine_TestRunStarting(object sender, TestRunStartEventArgs e)
        {
            Console.WriteLine("TestRunStarting");
        }

        static void ExecuteEngine_TestRunFinished(object sender, TestRunFinishEventArgs e)
        {
            Console.WriteLine("TestRunFinished: {0}", e.Result.Result);
        }
    }
}

