/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using yats.ExecutionQueue;
namespace yats.ExecutionTest
{
	public class TestSetupLogger : ITestStepVisitor
	{
		int level = 0;
		int indent = 2;
		int left = 40;
		
		private TestSetupLogger ()
		{
		}
		
		private TestSetupLogger (int level)
		{
			this.level = level;
		}
		
		public static void Log(TestStep test)
		{
			TestSetupLogger visitor = new TestSetupLogger();
			test.Accept(visitor);
		}
		
		void print(string s)
		{
			Console.WriteLine(s);
		}
		
		#region ITestStepVisitor implementation
		public void AcceptSingle (TestStepSingle test)
		{
			string s;// = string.Format("{0}[{1,-16}]    [{2,-16}]    [{3,-16}]    [{4,-16}]", new string(' ', level * indent), test.TestCase.GetType().Name, test.ExecutionMethod, test.DecisionHandler, test.ResultHandler);
			s = string.Format("{0}[{1,-16}]{2}[{3,-16}]    [{4,-16}]    [{5,-16}]    [{6,-16}]", new string(' ', level * indent), test.TestCase.GetType().Name, new string(' ', Math.Max(0, left-level * indent)), test.DecisionHandler, test.ExecutionMethod, test.SingleRunResultHandler, test.RepetitionResultHandler);
			print(s);
		}

		public void AcceptComposite (TestStepComposite test)
		{
			string s;// = string.Format("{0}{1}    [{2,-16}]    [{3,-16}]    [{4,-16}]", new string(' ', level * indent), new string(' ', 18), test.ExecutionMethod, test.DecisionHandler, test.ResultHandler);
			s = string.Format("{0}+{1}[{2,-16}]    [{3,-16}]    [{4,-16}]    [{5,-16}]", new string(' ', level * indent), new string(' ', Math.Max(0, left + 17 -level * indent)), test.DecisionHandler, test.ExecutionMethod, test.StepDecisionHandler, test.RepetitionResultHandler);
			print(s);
			foreach (var t in test.Steps){
				t.Accept(new TestSetupLogger(level + 1));
			}
		}
		#endregion
	}
}

