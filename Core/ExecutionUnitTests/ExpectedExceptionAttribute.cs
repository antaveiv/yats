﻿using System;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Commands;

//https://github.com/nunit/nunit-csharp-samples/blob/master/ExpectedExceptionExample/ExpectedExceptionAttribute.cs

namespace NUnit.Framework
{
    /// <summary>
    /// A simple ExpectedExceptionAttribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class ExpectedExceptionAttribute : NUnitAttribute, IWrapTestMethod
    {
        private Type _expectedExceptionType;
        public Type ExpectedException
        {
            get { return _expectedExceptionType; }
            set { _expectedExceptionType = value; }
        }

        public string ExpectedMessage
        {
            get;
            set;
        }

        public ExpectedExceptionAttribute()
        {
        }

        public ExpectedExceptionAttribute(Type type)
        {
            _expectedExceptionType = type;
        }

        public ExpectedExceptionAttribute(Type type, string expectedMessage)
        {
            _expectedExceptionType = type;
            ExpectedMessage = expectedMessage;
        }

        public TestCommand Wrap(TestCommand command)
        {
            return new ExpectedExceptionCommand(command, _expectedExceptionType, ExpectedMessage);
        }

        private class ExpectedExceptionCommand : DelegatingTestCommand
        {
            private readonly Type _expectedType;
            private string _expectedMessage;

            public ExpectedExceptionCommand(TestCommand innerCommand, Type expectedType, string expectedMessage)
                : base(innerCommand)
            {
                _expectedType = expectedType;
                _expectedMessage = expectedMessage;
            }

            public override TestResult Execute(TestExecutionContext context)
            {
                Type caughtType = null;
                string caughtMessage = null;

                try
                {
                    innerCommand.Execute(context);
                }
                catch (Exception ex)
                {
                    if (ex is NUnitException)
                        ex = ex.InnerException;
                    caughtType = ex.GetType();
                    caughtMessage = ex.Message;
                }

                if (caughtType == _expectedType)
                    context.CurrentResult.SetResult(ResultState.Success);
                else if (caughtType != null)
                    context.CurrentResult.SetResult(ResultState.Failure,
                        string.Format("Expected {0} but got {1}", _expectedType.Name, caughtType.Name));
                else
                    context.CurrentResult.SetResult(ResultState.Failure,
                        string.Format("Expected {0} but no exception was thrown", _expectedType.Name));

                if (_expectedMessage != null)
                {
                    if (caughtMessage != _expectedMessage)
                    {
                        context.CurrentResult.SetResult(ResultState.Failure,
                        string.Format("Expected message {0} but got {1}", _expectedMessage, caughtMessage));
                    }
                }

                return context.CurrentResult;
            }
        }
    }
}
