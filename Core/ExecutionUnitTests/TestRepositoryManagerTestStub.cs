/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.Interface;
using yats.TestRepositoryManager.YatsNative;
using yats.TestRepositoryManager.YatsNativeTestCase;

namespace yats.ExecutionTest
{
    public class TestRepositoryManagerTestStub : ITestRepositoryManager
    {
        public TestRepositoryManagerTestStub()
        {
        }

        public TestRepositoryManagerTestStub(ITestRunner testRunner)
        {
            testRunner.TestRunFinished += executeEngine_TestRunFinished;
            testRunner.TestRunStarting += executeEngine_TestRunStarting;
            testRunner.TestStepResult += executeEngine_TestStepResult;
            testRunner.TestStepRepetitionResult += executeEngine_TestStepRepetitionResult;
            testRunner.TestStepStarting += executeEngine_TestStepStarting;
        }

        #region ITestRepositoryManager implementation

        // Raised when the repository manager is about to analyze an assembly 
        public event EventHandler<AssemblyLoadingEventArgs> BeforeLoadingTestAssembly;
        // Raised after the repository manager loads a test case assembly 
        public event EventHandler<AssemblyLoadedEventArgs> OnTestAssemblyLoad;
        // Error indication
        public event EventHandler<ExceptionEventArgs> OnError;

        public void Initialize()
        {
            if (OnTestAssemblyLoad != null)//dummy - clear compiler warning about unused event
            {
            }
        }

        public string GetUniqueTestId(yats.TestCase.Interface.ITestCase testCase)
        {
            return this.GetType().FullName + "|" + testCase.GetType().FullName;
        }

        public ITestCase GetByUniqueId(string id)
        {
            return null;
        }

        private object mLockObject = new object();

        public string _Totals
        {
            get { return string.Format("{0} TestCases, {1} composites, {2} step starts, {3} test runs started, {4} test runs ended", NumTestCasesRun, NumCompositesRun, NumTestStepStartReported, NumTestRunsStarted, NumTestRunsFinished); }
        }

        public string _Raw
        {
            get
            {
                return string.Format("Single [{0} pass, {1} fail, {2} inc, {3} NR, {4} canceled] Composite [{5} pass, {6} fail, {7} inc, {8} NR, {9} canceled]",
              NumPass_Reported_Single,
              NumFail_Reported_Single,
              NumInconclusive_Reported_Single,
              NumNotRun_Reported_Single,
              NumCanceled_Reported_Single,

              NumPass_Reported_Composite,
              NumFail_Reported_Composite,
              NumInconclusive_Reported_Composite,
              NumNotRun_Reported_Composite,
              NumCanceled_Reported_Composite
              );
            }
        }

        public string _Evaluated
        {
            get
            {
                return string.Format("Single [{0} pass, {1} fail, {2} inc, {3} NR, {4} canceled] Composite [{5} pass, {6} fail, {7} inc, {8} NR, {9} canceled]",
                    NumPass_Processed_Single,
                    NumFail_Processed_Single,
                    NumInconclusive_Processed_Single,
                    NumNotRun_Processed_Single,
                    NumCanceled_Processed_Single,

                    NumPass_Processed_Composite,
                    NumFail_Processed_Composite,
                    NumInconclusive_Processed_Composite,
                    NumNotRun_Processed_Composite,
                    NumCanceled_Processed_Composite
                    );
            }
        }

        public int NumTestCasesRun = 0;
        public int NumCompositesRun = 0;

        public int NumCanceled_Single = 0;
        public int NumFail_Single = 0;
        public int NumPass_Single = 0;
        public int NumInconclusive_Single = 0;
        //public int NumNotRun_Single = 0; //these are filled during calls to manager.Execute. If test is not run, manager.Execute is not called

        public int NumCanceled_Reported_Single = 0;
        public int NumFail_Reported_Single = 0;
        public int NumPass_Reported_Single = 0;
        public int NumInconclusive_Reported_Single = 0;
        public int NumNotRun_Reported_Single = 0;

        public int NumCanceled_Processed_Single = 0;
        public int NumFail_Processed_Single = 0;
        public int NumPass_Processed_Single = 0;
        public int NumInconclusive_Processed_Single = 0;
        public int NumNotRun_Processed_Single = 0;

        public int NumCanceled_Reported_Composite = 0;
        public int NumFail_Reported_Composite = 0;
        public int NumPass_Reported_Composite = 0;
        public int NumInconclusive_Reported_Composite = 0;
        public int NumNotRun_Reported_Composite = 0;

        public int NumCanceled_Processed_Composite = 0;
        public int NumFail_Processed_Composite = 0;
        public int NumPass_Processed_Composite = 0;
        public int NumInconclusive_Processed_Composite = 0;
        public int NumNotRun_Processed_Composite = 0;

        public int NumTestRunsStarted = 0;
        public int NumTestRunsFinished = 0;
        public int NumTestStepStartReported = 0;

        public void Reset()
        {
            NumTestCasesRun = 0;
            NumCompositesRun = 0;

            NumCanceled_Single = 0;
            NumFail_Single = 0;
            NumPass_Single = 0;
            NumInconclusive_Single = 0;

            NumCanceled_Reported_Single = 0;
            NumFail_Reported_Single = 0;
            NumPass_Reported_Single = 0;
            NumInconclusive_Reported_Single = 0;
            NumNotRun_Reported_Single = 0;

            NumCanceled_Processed_Single = 0;
            NumFail_Processed_Single = 0;
            NumPass_Processed_Single = 0;
            NumInconclusive_Processed_Single = 0;
            NumNotRun_Processed_Single = 0;

            NumCanceled_Reported_Composite = 0;
            NumFail_Reported_Composite = 0;
            NumPass_Reported_Composite = 0;
            NumInconclusive_Reported_Composite = 0;
            NumNotRun_Reported_Composite = 0;

            NumCanceled_Processed_Composite = 0;
            NumFail_Processed_Composite = 0;
            NumPass_Processed_Composite = 0;
            NumInconclusive_Processed_Composite = 0;
            NumNotRun_Processed_Composite = 0;

            NumTestRunsStarted = 0;
            NumTestRunsFinished = 0;
            NumTestStepStartReported = 0;
        }

        public ITestResult Execute(ITestCase testCase, IList<IParameter> testParameters, IGlobalParameterCollection globalParameters, object testRunner, string path)
        {
            lock (mLockObject)
            {
                NumTestCasesRun++;
            }

            var res = ((IYatsTestCase)testCase).Execute();

            lock (mLockObject)
            {
                switch (res.Result)
                {
                    case ResultEnum.CANCELED:
                        NumCanceled_Single++;
                        break;
                    case ResultEnum.FAIL:
                        NumFail_Single++;
                        break;
                    case ResultEnum.INCONCLUSIVE:
                        NumInconclusive_Single++;
                        break;
                    case ResultEnum.PASS:
                        NumPass_Single++;
                        break;
                    case ResultEnum.NOT_RUN:
                        throw new Exception("Not expected");
                }
            }

            return res;
        }

        public IList<ITestCaseInfo> TestCases
        {
            get
            {
                return new List<ITestCaseInfo>(); // Not implemented
            }
        }

        public string Name { get { return "Dummy"; } }

        #endregion

        public void executeEngine_TestStepStarting(object sender, TestStepStartingEventArgs e)
        {
            lock (mLockObject)
            {
                NumTestStepStartReported++;
                if (e.Step is TestStepComposite)
                {
                    NumCompositesRun++;
                }
            }
        }

        public void executeEngine_TestStepResult(object sender, TestStepResultEventArgs e)
        {
            lock (mLockObject)
            {
                if (e.Step is TestStepComposite)
                {
                    switch (e.RawResult.Result)
                    {
                        case ResultEnum.CANCELED:
                            NumCanceled_Reported_Composite++;
                            break;
                        case ResultEnum.FAIL:
                            NumFail_Reported_Composite++;
                            break;
                        case ResultEnum.INCONCLUSIVE:
                            NumInconclusive_Reported_Composite++;
                            break;
                        case ResultEnum.PASS:
                            NumPass_Reported_Composite++;
                            break;
                        case ResultEnum.NOT_RUN:
                            NumNotRun_Reported_Composite++;
                            break;
                    }

                    switch (e.EvaluatedResult.Result)
                    {
                        case ResultEnum.CANCELED:
                            NumCanceled_Processed_Composite++;
                            break;
                        case ResultEnum.FAIL:
                            NumFail_Processed_Composite++;
                            break;
                        case ResultEnum.INCONCLUSIVE:
                            NumInconclusive_Processed_Composite++;
                            break;
                        case ResultEnum.PASS:
                            NumPass_Processed_Composite++;
                            break;
                        case ResultEnum.NOT_RUN:
                            NumNotRun_Processed_Composite++;
                            break;
                    }
                }
                else
                {
                    switch (e.RawResult.Result)
                    {
                        case ResultEnum.CANCELED:
                            NumCanceled_Reported_Single++;
                            break;
                        case ResultEnum.FAIL:
                            NumFail_Reported_Single++;
                            break;
                        case ResultEnum.INCONCLUSIVE:
                            NumInconclusive_Reported_Single++;
                            break;
                        case ResultEnum.PASS:
                            NumPass_Reported_Single++;
                            break;
                        case ResultEnum.NOT_RUN:
                            NumNotRun_Reported_Single++;
                            break;
                    }

                    switch (e.EvaluatedResult.Result)
                    {
                        case ResultEnum.CANCELED:
                            NumCanceled_Processed_Single++;
                            break;
                        case ResultEnum.FAIL:
                            NumFail_Processed_Single++;
                            break;
                        case ResultEnum.INCONCLUSIVE:
                            NumInconclusive_Processed_Single++;
                            break;
                        case ResultEnum.PASS:
                            NumPass_Processed_Single++;
                            break;
                        case ResultEnum.NOT_RUN:
                            NumNotRun_Processed_Single++;
                            break;
                    }
                }
            }
        }

        public void executeEngine_TestStepRepetitionResult(object sender, TestStepRepetitionResultEventArgs e)
        {
        }

        public void executeEngine_TestRunStarting(object sender, TestRunStartEventArgs e)
        {
            lock (mLockObject)
            {
                NumTestRunsStarted++;
            }
        }

        public void executeEngine_TestRunFinished(object sender, TestRunFinishEventArgs e)
        {
            lock (mLockObject)
            {
                NumTestRunsFinished++;
            }
        }

        public IList<IParameter> GetParameters(ITestCase testCase)
        {
            List<IParameter> result = new List<IParameter>();
            return result;
        }

        public void SetParameter(ITestCase testCase, IParameter parameter)
        {
            throw new NotImplementedException();
        }

        public string GetTestDescription(ITestCase testCase)
        {
            return testCase.GetType().FullName;
        }

        public string GetTestName(ITestCase testCase)
        {
            return testCase.GetType().Name;
        }

        public void RaiseLoadingAssemblyEvent(string fullAssemblyPath)
        {
            if (BeforeLoadingTestAssembly != null)
            {
                BeforeLoadingTestAssembly(this, new AssemblyLoadingEventArgs() { FullAssemblyPath = fullAssemblyPath, Manager = this, CancelEvent = new System.ComponentModel.CancelEventArgs() });
            }
        }

        public void RaiseOnErrorEvent(Exception ex)
        {
            if (OnError != null)
            {
                OnError(this, new ExceptionEventArgs() { Exception = ex });
            }
        }

        public bool GetTestResult(ITestCase testCase, IParameter param, out object value)
        {
            throw new NotImplementedException();
        }

        public bool GetParameterValue(ITestCase testCase, IParameter param, out object value)
        {
            throw new NotImplementedException();
        }

        public void SetParameterValue(ITestCase testCase, IParameter param, object value)
        {
            throw new NotImplementedException();
        }

        public bool CanParameterBeNull(ITestCase testCase, IParameter param)
        {
            return false;
        }

        public bool Cancel(ITestCase testCase)
        {
            throw new NotImplementedException();
        }

        public bool IsMyTestCase(ITestCase testCase)
        {
            return true;
        }

        public bool GetParameterDefault(ITestCase testCase, IParameter param, out object value)
        {
            throw new NotImplementedException();
        }

        public bool IsMyTestCase(string uniqueId)
        {
            throw new NotImplementedException();
        }

        public string GetGroupName(ITestCase testCase)
        {
            throw new NotImplementedException();
        }
    }
}

