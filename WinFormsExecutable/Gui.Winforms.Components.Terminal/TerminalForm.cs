﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using yats.Ports;
using yats.Ports.Utilities;
using yats.TestCase.Parameter;

namespace Gui.Winforms.Components.Terminal
{
    public partial class TerminalForm : DockContent
    {
        private AbstractPort mPort;

        public AbstractPort Port
        {
            get { return mPort; }
            set
            {
                if (mPort != null)
                {
                    //TODO unsubscribe old port events
                }
                mPort = value;
                if (mPort != null)
                {
                    //TODO subscribe new port events
                }
            }
        }

        public TerminalForm()
        {
            InitializeComponent();
        }

        public TerminalForm(PortSettings portSettings)
        {
            //PortReusePool.
            //Port = PortOpenHelper.Open(portSettings);
        }
    }
}
