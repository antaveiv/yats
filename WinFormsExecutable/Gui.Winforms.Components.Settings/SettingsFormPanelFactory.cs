﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Gui.Winforms.Components.Settings;
using yats.Utilities;
using System;
using System.Collections.Generic;
using Gui.Winforms.PublicAPI;

namespace yats.Gui.Winforms.Components.Settings
{
    public class SettingsFormPanelFactory
    {
        public List<SettingsTabPanel> GetSettingsPanels(int defaultPanelRequest, out int defaultTabIndex)
        {
            var types = AssemblyHelper.Instance.GetDerivedTypes(typeof(SettingsTabPanel));
            
            List<SettingsTabPanel> panels = new List<SettingsTabPanel>();
            foreach (var t in types)
            {
                panels.Add((SettingsTabPanel)Activator.CreateInstance(t));
            }
                       
            panels.Sort( (emp1,emp2)=>emp1.GetSortIndex().CompareTo(emp2.GetSortIndex()) );
            defaultTabIndex = 0;

            foreach (var panel in panels)
            {
                panel.LoadSettings();
                if (panel.GetSortIndex() == defaultPanelRequest)
                {
                    defaultTabIndex = panels.IndexOf(panel);
                }
            }

            return panels;
        }
    }
}