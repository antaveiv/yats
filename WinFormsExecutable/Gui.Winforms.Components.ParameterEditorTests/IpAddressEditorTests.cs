﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Windows.Forms;
using yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors;
using yats.TestCase.Parameter;
using System.Net;

namespace Gui.Winforms.Components.ParameterEditorTests
{
    [TestFixture]
    public class IpAddressEditorTests
    {
        [Test]
        public void TestDifferentIpAddresses()
        {
            Form form = new Form();
            IpAddressEditor editor = new IpAddressEditor();
            form.Controls.Add(editor);
            IPAddress ip1 = new IPAddress(new byte []{0, 0, 0, 0});
            IPAddress ip2 = new IPAddress(new byte[] { 127, 128, 1, 238 });
            IPAddress ip3 = new IPAddress(new byte[] { 127, 128, 1, 238 });
            editor.ParameterToEditor(new IPAddressParameterValue(ip1));
            form.Show();
            editor.Set(ip2);
            var result = new IPAddressParameterValue();
            editor.EditorToParameter(result);
            form.Close();
            Assert.AreEqual(result.IP, ip3);
        }
    }
}
