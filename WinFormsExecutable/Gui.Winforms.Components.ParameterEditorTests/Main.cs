/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
namespace Gui.Winforms.Components.ParameterEditorTests
{
	public class MainClass
	{
        [STAThread]
		public static void Main (string[] args)
		{
            new IntegerHexEditorTests().DecimalToHexStringTests();
            new IntegerHexEditorTests().DisplayNegativeSignedShortTest();
            new IntegerHexEditorTests().DisplayNegativeInt64Test();
            new EnumEditorTests().SelectNonNullableEditor();
            new EnumEditorTests().SelectNullableEditor();
		}
	}
}


			