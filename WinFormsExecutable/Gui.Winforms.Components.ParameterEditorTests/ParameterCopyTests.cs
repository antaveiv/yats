﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#if DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Aga.Controls.Tree;
using NUnit.Framework;
using yats.ExecutionQueue;
using yats.Gui.Winforms.Components.ClipboardObjects;
using yats.Gui.Winforms.Components.ParameterEditor;
using yats.TestCase.Parameter;
using yats.TestCase.Yats;
using yats.Utilities;
using yats.TestRun.Commands;
using System.Security.Permissions;
using yats.Utilities.Commands;
using System.Threading;

namespace yats.Gui.Winforms.Components.ParameterEditorTests
{
    [TestFixture]
    [Apartment(ApartmentState.STA)]
    public class ParameterCopyTests
    {
        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        [SetUp]
        public void Setup()
        {
            AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;
        }

        private static Assembly OnAssemblyResolve(object sender, ResolveEventArgs e)
        {
            if (e.Name.StartsWith("Gui.Winforms.Components.ClipboardObjects,", StringComparison.OrdinalIgnoreCase))
            {
                return typeof(ClipboardTestStepContents).Assembly;
            }
            return null;
        }

        //TODO copy/paste TestRun parameter within the same test run + between test runs
        //TODO copy/paste Global parameter

        [Test]
        public void PasteToMultipleIncompatibleParameters()
        {
            //copy/paste parameter to multiple parameters, some incompatible. Must not change anything
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();
            ParameterTypeTest testCase = new ParameterTypeTest();
            TestStepSingle testStep = new TestStepSingle(testCase, manager);
            var parameterFrom = manager.GetParameters(testCase).First(x => x.Name == "ByteListParameter");
            var parameterTo1 = manager.GetParameters(testCase).First(x => x.Name == "ByteArrayParameter");
            var parameterTo2 = manager.GetParameters(testCase).First(x => x.Name == "ByteParameter");

            parameterFrom.Value = new ByteArrayValue(new byte[] { 1, 2, 3 });
            parameterTo1.Value = null;
            parameterTo2.Value = null;
            new ClipboardParameterContents(parameterFrom.ToSingleItemList(), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsFalse(parameterTree.Paste(ListExtensions.GetList(new TreeNodeAdv(new ParameterNode(null, null, parameterTo1, testStep)),
                new TreeNodeAdv(new ParameterNode(null, null, parameterTo2, testStep))), null, new CommandStack()));
            Assert.IsNull(parameterTo1.Value);
            Assert.IsNull(parameterTo2.Value);
        }

        [Test]
        public void PasteToMultipleToMultipleParameters()
        {
            //TODO paste multiple parameters to multiple selected parameters - no changes
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();
            ParameterTypeTest testCase = new ParameterTypeTest();
            TestStepSingle testStep = new TestStepSingle(testCase, manager);
            var parameterFrom1 = manager.GetParameters(testCase).First(x => x.Name == "ByteArrayParameter");
            var parameterFrom2 = manager.GetParameters(testCase).First(x => x.Name == "ByteListParameter");
            
            var parameterTo1 = manager.GetParameters(testCase).First(x => x.Name == "ByteArrayParameter");
            var parameterTo2 = manager.GetParameters(testCase).First(x => x.Name == "ByteListParameter");

            parameterFrom1.Value = new ByteArrayValue(new byte[] { 1, 2, 3 });
            parameterFrom2.Value = new ByteArrayValue(new byte[] { 1, 2, 3 });
            var r1 = new ByteArrayValue(new byte[] { 1, 2, 3 });
            var r2 = new ByteArrayValue(new byte[] { 1, 2, 3 });
            parameterTo1.Value = r1;
            parameterTo2.Value = r2;
            new ClipboardParameterContents(ListExtensions.GetList(parameterFrom1, parameterFrom2), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsFalse(parameterTree.Paste(ListExtensions.GetList(new TreeNodeAdv(new ParameterNode(null, null, parameterTo1, testStep)),
                new TreeNodeAdv(new ParameterNode(null, null, parameterTo2, testStep))), null, new CommandStack()));
            Assert.IsTrue(object.ReferenceEquals(r1, parameterTo1.Value));
            Assert.IsTrue(object.ReferenceEquals(r2, parameterTo2.Value));
        }

        [Test]
        public void PasteNullToMultipleIncompatibleParameters()
        {
            //paste parameter with NULL result - should paste to an incompatible parameter
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();
            ParameterTypeTest testCase = new ParameterTypeTest();
            TestStepSingle testStep = new TestStepSingle(testCase, manager);
            var parameterFrom = manager.GetParameters(testCase).First(x => x.Name == "ByteListParameter");
            var parameterTo1 = manager.GetParameters(testCase).First(x => x.Name == "StringParameter");
            var parameterTo2 = manager.GetParameters(testCase).First(x => x.Name == "NullableByteParameter");

            parameterFrom.Value = null;
            parameterTo1.Value = new SimpleStringValue("t");
            parameterTo2.Value = new ByteArrayValue(new byte[] { 1, 2, 3 });
            new ClipboardParameterContents(ListExtensions.GetList(parameterFrom), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsTrue(parameterTree.Paste(ListExtensions.GetList(new TreeNodeAdv(new ParameterNode(null, null, parameterTo1, testStep)),
                new TreeNodeAdv(new ParameterNode(null, null, parameterTo2, testStep))), null, new CommandStack()));
            Assert.IsNull(parameterTo1.Value);
            Assert.IsNull(parameterTo2.Value);
        }

        [Test]
        public void PasteToMultipleParametersToSingleParameter()
        {
            //paste multiple parameters to a single parameter - no changes
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();
            ParameterTypeTest testCase = new ParameterTypeTest();
            TestStepSingle testStep = new TestStepSingle(testCase, manager);
            var parameterFrom1 = manager.GetParameters(testCase).First(x => x.Name == "ByteListParameter");
            var parameterFrom2 = manager.GetParameters(testCase).First(x => x.Name == "ByteListParameter");
            var parameterTo = manager.GetParameters(testCase).First(x => x.Name == "ByteArrayParameter");

            parameterFrom1.Value = new ByteArrayValue(new byte[] { 1, 2, 3 });
            parameterFrom2.Value = new ByteArrayValue(new byte[] { 1, 2, 3 });
            parameterTo.Value = null;
            new ClipboardParameterContents(ListExtensions.GetList(parameterFrom1, parameterFrom2), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsFalse(parameterTree.Paste(ListExtensions.GetList(new TreeNodeAdv(new ParameterNode(null, null, parameterTo, testStep))), null, new CommandStack()));
            Assert.IsNull(parameterTo.Value);
        }

        [Test]
        public void PasteCompatibleParameter()
        {
            //copy/paste compatible parameter
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();
            ParameterTypeTest testCase = new ParameterTypeTest();
            TestStepSingle testStep = new TestStepSingle(testCase, manager);
            var parameterFrom = manager.GetParameters(testCase).First(x => x.Name == "ByteListParameter");
            var parameterTo = manager.GetParameters(testCase).First(x => x.Name == "ByteArrayParameter");

            parameterFrom.Value = new ByteArrayValue(new byte[] { 1, 2, 3 });
            parameterTo.Value = null;
            new ClipboardParameterContents(ListExtensions.GetList(parameterFrom), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsTrue(parameterTree.Paste(ListExtensions.GetList(new TreeNodeAdv(new ParameterNode(null, null, parameterTo, testStep))), null, new CommandStack()));
            Assert.IsNotNull(parameterTo.Value);
            Assert.IsTrue(parameterTo.Value is ByteArrayValue);
            Assert.IsNotNull((parameterTo.Value as ByteArrayValue).Value);
        }

        [Test]
        public void PasteIncompatibleParameter()
        {
            //copy/paste incompatible parameter
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();
            ParameterTypeTest testCase = new ParameterTypeTest();
            TestStepSingle testStep = new TestStepSingle(testCase, manager);
            var parameterFrom = manager.GetParameters(testCase).First(x => x.Name == "ByteListParameter");
            var parameterTo = manager.GetParameters(testCase).First(x => x.Name == "ByteParameter");

            parameterFrom.Value = new ByteArrayValue(new byte[] { 1, 2, 3 });
            parameterTo.Value = null;
            new ClipboardParameterContents(ListExtensions.GetList(parameterFrom), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsFalse(parameterTree.Paste(ListExtensions.GetList(new TreeNodeAdv(new ParameterNode(null, null, parameterTo, testStep))), null, new CommandStack()));
            Assert.IsNull(parameterTo.Value);
        }

        [Test]
        public void ClipboardEmpty()
        {
            // no crash when paste is done with empty clipboard 
            System.Windows.Clipboard.Clear();
            List<TreeNodeAdv> targetNodes = new List<TreeNodeAdv>();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();
            ParameterTypeTest testCase = new ParameterTypeTest();
            TestStepSingle testStep = new TestStepSingle(testCase, manager);
            var parameter = manager.GetParameters(testCase).First(x => x.Name == "BoolParameter");
            TreeNodeAdv node = new TreeNodeAdv(new ParameterNode(null, null, parameter, testStep));
            targetNodes.Add(node);
            ParameterTree parameterTree = new ParameterTree();
            parameterTree.Paste(targetNodes, null, new CommandStack());
        }


        [Test]
        public void PasteCompatibleResult()
        {
            //copy/paste result to a compatible parameter
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();
            ParameterTypeTest testCase = new ParameterTypeTest();
            TestStepSingle testStep = new TestStepSingle(testCase, manager);
            var parameterFrom = manager.GetParameters(testCase).First(x => x.Name == "ByteArrayResult");
            var parameterTo = manager.GetParameters(testCase).First(x => x.Name == "ByteArrayParameter");

            parameterFrom.Value = new TestResultParameter(Guid.NewGuid());
            parameterTo.Value = null;
            new ClipboardParameterContents(ListExtensions.GetList(parameterFrom), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsTrue(parameterTree.Paste(ListExtensions.GetList(new TreeNodeAdv(new ParameterNode(null, null, parameterTo, testStep))), null, new CommandStack()));
            Assert.IsNotNull(parameterTo.Value);
            Assert.IsTrue(parameterTo.Value is TestResultParameter);
            Assert.AreEqual((parameterFrom.Value as TestResultParameter).ResultID, (parameterTo.Value as TestResultParameter).ResultID);
        }


        [Test]
        public void PasteIncompatibleResult()
        {
            //copy/paste result to a compatible parameter
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();
            ParameterTypeTest testCase = new ParameterTypeTest();
            TestStepSingle testStep = new TestStepSingle(testCase, manager);
            var parameterFrom = manager.GetParameters(testCase).First(x => x.Name == "ByteArrayResult");
            var parameterTo = manager.GetParameters(testCase).First(x => x.Name == "IP");

            parameterFrom.Value = new TestResultParameter(Guid.NewGuid());
            parameterTo.Value = null;
            new ClipboardParameterContents(ListExtensions.GetList(parameterFrom), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsFalse(parameterTree.Paste(ListExtensions.GetList(new TreeNodeAdv(new ParameterNode(null, null, parameterTo, testStep))), null, new CommandStack()));
            Assert.IsNull(parameterTo.Value);
        }

        [Test]
        public void PasteToResult()
        {
            //pasting into a result should fail
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();
            ParameterTypeTest testCase = new ParameterTypeTest();
            TestStepSingle testStep = new TestStepSingle(testCase, manager);
            var parameterFrom = manager.GetParameters(testCase).First(x => x.Name == "ByteArrayParameter");
            var parameterTo = manager.GetParameters(testCase).First(x => x.Name == "ByteArrayResult");

            parameterTo.Value = new TestResultParameter(Guid.NewGuid());
            parameterFrom.Value = null;
            new ClipboardParameterContents(ListExtensions.GetList(parameterFrom), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsFalse(parameterTree.Paste(ListExtensions.GetList(new TreeNodeAdv(new ResultNode(parameterTo, testStep))), null, new CommandStack()));
        }

        [Test]
        public void PasteToComposite()
        {
            //pasting into a composite step should fail
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();
            ParameterTypeTest testCase = new ParameterTypeTest();
            TestStepComposite testStep = new TestStepComposite();
            var parameterFrom = manager.GetParameters(testCase).First(x => x.Name == "ByteArrayParameter");
            parameterFrom.Value = null;
            new ClipboardParameterContents(ListExtensions.GetList(parameterFrom), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsFalse(parameterTree.Paste(ListExtensions.GetList(new TreeNodeAdv(new CompositeStepTreeNode(testStep))), null, new CommandStack()));
        }

        [Test]
        public void PasteToNoSelectedNodes()
        {
            //pasting into a composite step should fail
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();
            ParameterTypeTest testCase = new ParameterTypeTest();
            TestStepComposite testStep = new TestStepComposite();
            var parameterFrom = manager.GetParameters(testCase).First(x => x.Name == "ByteArrayParameter");
            parameterFrom.Value = null;
            new ClipboardParameterContents(ListExtensions.GetList(parameterFrom), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsFalse(parameterTree.Paste(null, null, new CommandStack()));
            Assert.IsFalse(parameterTree.Paste(new List<TreeNodeAdv>(), null, new CommandStack()));
        }

        [Test]
        public void PasteToMixedNodes()
        {
            //pasting into a both test steps and parameter nodes should fail
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();
            ParameterTypeTest testCase = new ParameterTypeTest();
            TestStepSingle testStep = new TestStepSingle(testCase, manager);
            var parameterFrom = manager.GetParameters(testCase).First(x => x.Name == "ByteArrayResult");
            var parameterTo = manager.GetParameters(testCase).First(x => x.Name == "ByteArrayParameter");

            parameterFrom.Value = new TestResultParameter(Guid.NewGuid());
            parameterTo.Value = null;
            new ClipboardParameterContents(ListExtensions.GetList(parameterFrom), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsFalse(parameterTree.Paste(ListExtensions.GetList(new TreeNodeAdv(new ParameterNode(null, null, parameterTo, testStep)),
                new TreeNodeAdv(new SingleStepTreeNode(testStep))), null, new CommandStack()));
            Assert.IsNull(parameterTo.Value);
        }

        [Test]
        public void PasteMultipleStepsToStep()
        {
            // should fail - only one test step may be pasted to other step(s)
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();

            TestStepSingle testStepFrom1 = new TestStepSingle(new ParameterTypeTest(), manager);
            TestStepSingle testStepFrom2 = new TestStepSingle(new ParameterTypeTest(), manager);
            TestStepSingle testStepTo = new TestStepSingle(new ParameterTypeTest(), manager);
            
            new ClipboardTestStepContents(ListExtensions.GetList<TestStep>(testStepFrom1, testStepFrom2), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsFalse(parameterTree.Paste(ListExtensions.GetList(new TreeNodeAdv(new SingleStepTreeNode(testStepTo))), null, new CommandStack()));
        }

        [Test]
        public void PasteStepToCompatibleStep()
        {
            //paste a TestStep - replace parameters only if the test step has same list of parameters 
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();

            TestStepSingle testStepFrom1 = new TestStepSingle(new ParameterTypeTest(), manager);
            TestStepSingle testStepTo1 = new TestStepSingle(new ParameterTypeTest(), manager);
            TestStepSingle testStepTo2 = new TestStepSingle(new ParameterTypeTest(), manager);

            new ClipboardTestStepContents(ListExtensions.GetList<TestStep>(testStepFrom1), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsTrue(parameterTree.Paste(ListExtensions.GetList(new TreeNodeAdv(new SingleStepTreeNode(testStepTo1)), new TreeNodeAdv(new SingleStepTreeNode(testStepTo2))), null, new CommandStack()));
        }

        [Test]
        public void PasteStepToIncompatibleStep()
        {
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();

            TestStepSingle testStepFrom1 = new TestStepSingle(new ParameterTypeTest(), manager);
            TestStepSingle testStepTo1 = new TestStepSingle(new ParameterTypeTest(), manager);
            TestStepSingle testStepTo2 = new TestStepSingle(new AlwaysCanceled(), manager);

            new ClipboardTestStepContents(ListExtensions.GetList<TestStep>(testStepFrom1), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsFalse(parameterTree.Paste(ListExtensions.GetList(new TreeNodeAdv(new SingleStepTreeNode(testStepTo1)), new TreeNodeAdv(new SingleStepTreeNode(testStepTo2))), null, new CommandStack()));
        }

        [Test]
        public void PasteParametersToCompatibleSteps()
        {
            //paste multiple parameters to test case - paste by matching parameter name and type (match name and type)
            System.Windows.Clipboard.Clear();
            TestRepositoryManager.YatsNative.NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            manager.Initialize();

            
            TestStepSingle testStepTo1 = new TestStepSingle(new ParameterTypeTest(), manager);
            TestStepSingle testStepTo2 = new TestStepSingle(new ParameterTypeTest(), manager);

            ParameterTypeTest testCase = new ParameterTypeTest();
            TestStepSingle testStep = new TestStepSingle(testCase, manager);
            var parameterFrom1 = manager.GetParameters(testCase).First(x => x.Name == "ByteListParameter");
            var parameterFrom2 = manager.GetParameters(testCase).First(x => x.Name == "ByteArrayParameter");
            
            parameterFrom1.Value = new ByteArrayValue(new byte[] { 1, 2, 3 });
            parameterFrom2.Value = new ByteArrayValue(new byte[] { 1, 2, 3 });
            
            new ClipboardParameterContents(ListExtensions.GetList(parameterFrom1, parameterFrom2), null).CopyToClipboard();
            ParameterTree parameterTree = new ParameterTree();
            Assert.IsTrue(parameterTree.Paste(ListExtensions.GetList(new TreeNodeAdv(new SingleStepTreeNode(testStepTo1)), new TreeNodeAdv(new SingleStepTreeNode(testStepTo2))), null, new CommandStack()));

            Assert.NotNull((testStepTo1.Parameters).First(x => x.Name == "ByteListParameter").Value);
            Assert.NotNull((testStepTo2.Parameters).First(x => x.Name == "ByteListParameter").Value);
            Assert.NotNull((testStepTo1.Parameters).First(x => x.Name == "ByteArrayParameter").Value);
            Assert.NotNull((testStepTo2.Parameters).First(x => x.Name == "ByteArrayParameter").Value);
        }

        
        //TODO paste multiple parameters to test case - paste by matching parameter name and type (same name, incompatible type)
        //TODO paste multiple parameters to test case - paste by matching parameter name and type (different name)

    }
}
#endif