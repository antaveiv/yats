﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using yats.TestCase.Parameter;
using yats.TestCase.Interface;
using yats.Attributes;
using yats.TestRepositoryManager.YatsNative;
using yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors;
using yats.Utilities;

namespace Gui.Winforms.Components.ParameterEditorTests
{
#if DEBUG
    [TestFixture]
    public class IntegerHexEditorTests
    {
        [Test]
        public void DisplayZeroSignedByteTest()
        {
            IntegerParameterValue parameter = new IntegerParameterValue(0, SByte.MinValue, SByte.MaxValue);
            IntegerHexEditor editor = new IntegerHexEditor();
            editor.ParameterToEditor(parameter);

            Assert.AreEqual(SByte.MinValue.ToString(), editor.lbMin.Text);
            Assert.AreEqual(SByte.MaxValue.ToString(), editor.lbMax.Text);
            Assert.AreEqual("0", editor.tbDecimal.Text);
            Assert.AreEqual("0", editor.tbHex.Text);
            Assert.AreEqual(8, editor.numBits);

            for (int i = 0; i < 8; i++)
            {
                Assert.IsTrue(editor.binLabels[i].Enabled);
                Assert.AreEqual("0", editor.binLabels[i].Text);
            }

            for (int i = 8; i < editor.binLabels.Length; i++)
            {
                Assert.IsFalse(editor.binLabels[i].Enabled);
                Assert.AreEqual("0", editor.binLabels[i].Text);
            }
        }

        [Test]
        public void DisplayZeroUnsignedByteTest()
        {
            IntegerParameterValue parameter = new IntegerParameterValue(0, Byte.MinValue, Byte.MaxValue);
            IntegerHexEditor editor = new IntegerHexEditor();
            editor.ParameterToEditor(parameter);

            Assert.AreEqual(Byte.MinValue.ToString(), editor.lbMin.Text);
            Assert.AreEqual(Byte.MaxValue.ToString(), editor.lbMax.Text);
            Assert.AreEqual("0", editor.tbDecimal.Text);
            Assert.AreEqual("0", editor.tbHex.Text);
            Assert.AreEqual(8, editor.numBits);

            for (int i = 0; i < 8; i++)
            {
                Assert.IsTrue(editor.binLabels[i].Enabled);
                Assert.AreEqual("0", editor.binLabels[i].Text);
            }

            for (int i = 8; i < editor.binLabels.Length; i++)
            {
                Assert.IsFalse(editor.binLabels[i].Enabled);
                Assert.AreEqual("0", editor.binLabels[i].Text);
            }
        }

        [Test]
        public void DisplayPositiveSignedByteTest()
        {
            IntegerParameterValue parameter = new IntegerParameterValue(33, SByte.MinValue, SByte.MaxValue);
            IntegerHexEditor editor = new IntegerHexEditor();
            editor.ParameterToEditor(parameter);

            Assert.AreEqual(SByte.MinValue.ToString(), editor.lbMin.Text);
            Assert.AreEqual(SByte.MaxValue.ToString(), editor.lbMax.Text);
            Assert.AreEqual("33", editor.tbDecimal.Text);
            Assert.AreEqual("21", editor.tbHex.Text);
            Assert.AreEqual(8, editor.numBits);

            for (int i = 0; i < 8; i++)
            {
                Assert.IsTrue(editor.binLabels[i].Enabled);
            }

            Assert.AreEqual("1", editor.binLabels[0].Text);
            Assert.AreEqual("0", editor.binLabels[1].Text);
            Assert.AreEqual("0", editor.binLabels[2].Text);
            Assert.AreEqual("0", editor.binLabels[3].Text);
            Assert.AreEqual("0", editor.binLabels[4].Text);
            Assert.AreEqual("1", editor.binLabels[5].Text);
            Assert.AreEqual("0", editor.binLabels[6].Text);
            Assert.AreEqual("0", editor.binLabels[7].Text);

            for (int i = 8; i < editor.binLabels.Length; i++)
            {
                Assert.IsFalse(editor.binLabels[i].Enabled);
                Assert.AreEqual("0", editor.binLabels[i].Text);
            }
        }

        [Test]
        public void DisplayNegativeSignedByteTest()
        {
            sbyte value = -100;
            IntegerParameterValue parameter = new IntegerParameterValue(value, SByte.MinValue, SByte.MaxValue);
            IntegerHexEditor editor = new IntegerHexEditor();
            editor.ParameterToEditor(parameter);

            Assert.AreEqual(SByte.MinValue.ToString(), editor.lbMin.Text);
            Assert.AreEqual(SByte.MaxValue.ToString(), editor.lbMax.Text);
            Assert.AreEqual(value.ToString(), editor.tbDecimal.Text);
            Assert.AreEqual(value.ToString("X"), editor.tbHex.Text);
            Assert.AreEqual(8, editor.numBits);

            for (int i = 0; i < 8; i++)
            {
                Assert.IsTrue(editor.binLabels[i].Enabled);
            }

            Assert.AreEqual("0", editor.binLabels[0].Text);
            Assert.AreEqual("0", editor.binLabels[1].Text);
            Assert.AreEqual("1", editor.binLabels[2].Text);
            Assert.AreEqual("1", editor.binLabels[3].Text);
            Assert.AreEqual("1", editor.binLabels[4].Text);
            Assert.AreEqual("0", editor.binLabels[5].Text);
            Assert.AreEqual("0", editor.binLabels[6].Text);
            Assert.AreEqual("1", editor.binLabels[7].Text);

            for (int i = 8; i < editor.binLabels.Length; i++)
            {
                Assert.IsFalse(editor.binLabels[i].Enabled);
                Assert.AreEqual("0", editor.binLabels[i].Text);
            }
        }

        [Test]
        public void DisplayNegativeSignedShortTest()
        {
            short value = -900;
            IntegerParameterValue parameter = new IntegerParameterValue(value, short.MinValue, short.MaxValue);
            IntegerHexEditor editor = new IntegerHexEditor();
            editor.ParameterToEditor(parameter);

            Assert.AreEqual(short.MinValue.ToString(), editor.lbMin.Text);
            Assert.AreEqual(short.MaxValue.ToString(), editor.lbMax.Text);
            Assert.AreEqual(value.ToString(), editor.tbDecimal.Text);
            Assert.AreEqual(value.ToString("X"), editor.tbHex.Text);
            Assert.AreEqual(16, editor.numBits);

            for (int i = 0; i < editor.numBits; i++)
            {
                Assert.IsTrue(editor.binLabels[i].Enabled);
            }

            Assert.AreEqual("0", editor.binLabels[0].Text);
            Assert.AreEqual("0", editor.binLabels[1].Text);
            Assert.AreEqual("1", editor.binLabels[2].Text);
            Assert.AreEqual("1", editor.binLabels[3].Text);
            Assert.AreEqual("1", editor.binLabels[4].Text);
            Assert.AreEqual("1", editor.binLabels[5].Text);
            Assert.AreEqual("1", editor.binLabels[6].Text);
            Assert.AreEqual("0", editor.binLabels[7].Text);
            Assert.AreEqual("0", editor.binLabels[8].Text);
            Assert.AreEqual("0", editor.binLabels[9].Text);
            Assert.AreEqual("1", editor.binLabels[10].Text);
            Assert.AreEqual("1", editor.binLabels[11].Text);
            Assert.AreEqual("1", editor.binLabels[12].Text);
            Assert.AreEqual("1", editor.binLabels[13].Text);
            Assert.AreEqual("1", editor.binLabels[14].Text);
            Assert.AreEqual("1", editor.binLabels[15].Text);

            for (int i = editor.numBits; i < editor.binLabels.Length; i++)
            {
                Assert.IsFalse(editor.binLabels[i].Enabled);
                Assert.AreEqual("0", editor.binLabels[i].Text);
            }
        }

        [Test]
        public void DecimalToHexStringTests()
        {
            int valueInt = 1024;
            decimal value = valueInt;
            Assert.AreEqual(valueInt.ToString("X"), value.ToHexString(32, true));
            valueInt = -1024;
            value = valueInt;
            Assert.AreEqual(valueInt.ToString("X"), value.ToHexString(32, true));
            byte b = 0xF4;
            value = b;
            Assert.AreEqual(b.ToString("X"), value.ToHexString(8, false));
        }

        [Test]
        public void DisplayNegativeInt64Test()
        {
            decimal value = Int64.MinValue;
            IntegerParameterValue parameter = new IntegerParameterValue(value, Int64.MinValue, Int64.MaxValue);
            IntegerHexEditor editor = new IntegerHexEditor();
            editor.ParameterToEditor(parameter);

            Assert.AreEqual(Int64.MinValue.ToString(), editor.lbMin.Text);
            Assert.AreEqual(Int64.MaxValue.ToString(), editor.lbMax.Text);
            Assert.AreEqual(value.ToString(), editor.tbDecimal.Text);
            Assert.AreEqual(value.ToHexString(64, true), editor.tbHex.Text);
            Assert.AreEqual(64, editor.numBits);

            for (int i = 0; i < editor.numBits; i++)
            {
                Assert.IsTrue(editor.binLabels[i].Enabled);                
            }

            Assert.AreEqual("1", editor.binLabels[editor.numBits-1].Text);
            for (int i = 0; i < editor.numBits-1; i++)
            {
                Assert.AreEqual("0", editor.binLabels[0].Text);
            }

            for (int i = editor.numBits; i < editor.binLabels.Length; i++)
            {
                Assert.IsFalse(editor.binLabels[i].Enabled);
                Assert.AreEqual("0", editor.binLabels[i].Text);
            }
        }
    }
#endif
}
