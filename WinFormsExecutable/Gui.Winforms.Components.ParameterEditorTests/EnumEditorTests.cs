﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using yats.TestCase.Parameter;
using yats.TestCase.Interface;
using yats.Attributes;
using yats.TestRepositoryManager.YatsNative;

namespace Gui.Winforms.Components.ParameterEditorTests
{
    [TestFixture]
    public class EnumEditorTests
    {
        [Test]
        public void SelectNonNullableEditor()
        {
            NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            var tc = new EnumParameterTestStub();
            var discoveredParams = manager.GetParameters(tc);
            ParameterTypeCollection.Instance.Initialize();
            List<AbstractParameterValue> suitableTypes = ParameterTypeCollection.Instance.GetSuitableValueTypes(discoveredParams.First(x => x.Name == "CantBeNull1"));
            Assert.IsNotEmpty(suitableTypes);
            Assert.AreEqual(1, suitableTypes.Count);
            foreach (var paramType in suitableTypes)
            {
                var editors = ParameterTypeCollection.Instance.GetEditors(paramType);
                Assert.IsNotEmpty(editors);
            }
        }

        [Test]
        public void SelectNullableEditor()
        {
            NativeTestRepositoryManager manager = new yats.TestRepositoryManager.YatsNative.NativeTestRepositoryManager();
            var tc = new EnumParameterTestStub();
            var discoveredParams = manager.GetParameters(tc);
            ParameterTypeCollection.Instance.Initialize();
            List<AbstractParameterValue> suitableTypes = ParameterTypeCollection.Instance.GetSuitableValueTypes(discoveredParams.First(x => x.Name == "CanBeNull4"));
            Assert.IsNotEmpty(suitableTypes);
            foreach (var paramType in suitableTypes)
            {
                var editors = ParameterTypeCollection.Instance.GetEditors(paramType);
                Assert.IsNotEmpty(editors);
            }
        }
    }

    public class EnumParameterTestStub : ITestCase
    {
        public enum ParamEnum : byte
        {
            ONE,
            TWO,
            WTF
        }
        public ParamEnum CantBeNull1
        {
            get;
            set;
        }

        [Parameter]
        public Nullable<ParamEnum> CanBeNull4;
    }
}
