﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using yats.TestRepositoryManager.Interface;
using System.Diagnostics;
using System.Linq;
using yats.Utilities;
using System.IO;
using System;

namespace yats.Gui.Winforms.Components.TestCaseOrganizer
{
    public class PluginFacade
    {
        internal static List<TestLocationConfiguration> TestConfig = new List<TestLocationConfiguration>();
        const string settingFileName = "testConfig.xml";

        static ITestRepositoryManagerCollection testRepositoryManagers = null;
        public static ITestRepositoryManagerCollection TestRepositoryManagers
        {
            [DebuggerStepThrough]
            get { return testRepositoryManagers; }
            set
            {
                testRepositoryManagers = value;
                if (testRepositoryManagers != null)
                {
                    testRepositoryManagers.InitializationStarting += TestRepositoryManagers_InitializationStarting;
                    testRepositoryManagers.InitializationFinished += TestRepositoryManagers_InitializationFinished;
                    testRepositoryManagers.OnTestAssemblyLoad += OnTestAssemblyLoad;
                    testRepositoryManagers.BeforeLoadingTestAssembly += BeforeLoadingTestAssembly;
                }
            }
        }

        private static void TestRepositoryManagers_InitializationStarting(object sender, RepositoryManagerCollectionEventArgs e)
        {
            try
            {
                string fileName = FileUtil.GetApplicationDataPath(settingFileName);
                if (File.Exists(fileName))
                {
                    TestConfig = SerializationHelper.DeserializeFile<List<TestLocationConfiguration>>(fileName);
                   // TestConfig.RemoveRange(TestConfig.Where(x => x.State == TestLocationConfiguration.TestState.Discovered).ToArray());//TODO remove
                }
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex);
            }
        }

        private static void TestRepositoryManagers_InitializationFinished(object sender, RepositoryManagerCollectionEventArgs e)
        {
            SaveSettings(); // save any new discovered items
        }

        private static void BeforeLoadingTestAssembly(object sender, AssemblyLoadingEventArgs e)
        {
            var foundEntry = TestConfig.FirstOrDefault(c => c.ManagerType == e.Manager.GetType().FullName && c.TestLocation == e.FullAssemblyPath);
            if (foundEntry != null)
            {
                e.CancelEvent.Cancel = foundEntry.State == TestLocationConfiguration.TestState.Disabled;
                return;
            }
            var sameName = TestConfig.Where(c => c.ManagerType == e.Manager.GetType().FullName && c.TestLocationFileName == Path.GetFileName(e.FullAssemblyPath)).ToList();
            if (sameName.Count > 0)
            {
                TestLocationConfiguration newEntry = new TestLocationConfiguration() { InUse = false, ManagerType = e.Manager.GetType().FullName, State = TestLocationConfiguration.TestState.Disabled, TestLocation = e.FullAssemblyPath };
                TestConfig.Add(newEntry);
                e.CancelEvent.Cancel = true;
            }
        }

        private static void OnTestAssemblyLoad(object sender, AssemblyLoadedEventArgs e)
        {
            var foundEntry = TestConfig.FirstOrDefault(c => c.ManagerType == e.Manager.GetType().FullName && c.TestLocation == e.FullAssemblyPath);
            if (foundEntry == null)
            {
                TestLocationConfiguration newEntry = new TestLocationConfiguration() { InUse = e.IsLoaded, ManagerType = e.Manager.GetType().FullName, State = TestLocationConfiguration.TestState.Discovered, TestLocation = e.FullAssemblyPath };
                TestConfig.Add(newEntry);
                return;
            }
            foundEntry.InUse |= e.IsLoaded;
        }

        internal static void SaveSettings()
        {
            try {
                string fileName = FileUtil.GetApplicationDataPath(settingFileName);
                SerializationHelper.SerializeToFile<List<TestLocationConfiguration>>(TestConfig, fileName);
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex);
            }
        }
    }
}
