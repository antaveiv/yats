﻿using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace yats.Gui.Winforms.Components.TestCaseOrganizer
{
    [DebuggerDisplay("{State}, {TestLocation} {ManagerType}")]
    public class TestLocationConfiguration // public because of serialization
    {
        public enum TestState
        {
            Discovered, // test suite was found, user has not enabled or disabled it
            Used, // user has enabled the test suite
            Disabled
        }

        public string TestLocation { get; set; }
        public string ManagerType { get; set; }
        public TestState State { get; set; }
        [XmlIgnore]
        public bool InUse { get; set; }
        [XmlIgnore]
        public string TestLocationFileName { get { return Path.GetFileName(TestLocation); } }

        // for serialization
        public TestLocationConfiguration()
        {
        }
    }
}
