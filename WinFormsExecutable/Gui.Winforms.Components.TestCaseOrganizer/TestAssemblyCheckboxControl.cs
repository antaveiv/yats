﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.IO;
using System.Linq;
using System.Windows.Forms;
using System;
using yats.Utilities;
using System.Collections.Specialized;
using Gui.Winforms.PublicAPI;

namespace yats.Gui.Winforms.Components.TestCaseOrganizer
{
    public partial class TestAssemblyCheckboxControl : SettingsTabPanel
    {
        public TestAssemblyCheckboxControl()
        {
            yats.Gui.Winforms.Components.TestCaseOrganizer.Properties.Settings.Default.PropertyChanged += OnSettingPropertyChanged;
            InitializeComponent();
        }

        void OnSettingPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SkipAssemblies")
            {
                //reinitialize the test case repositories after chaning settings 
                PluginFacade.TestRepositoryManagers.Initialize(true);
            }
        }

        public override string GetText()
        { 
            return "Test cases"; 
        }

        public override void LoadSettings() 
        {
            if (PluginFacade.TestRepositoryManagers == null)
            {
                return;
            }
            foreach(var entry in PluginFacade.TestConfig.OrderBy(x=>x.TestLocationFileName)){
                var listItem = new ListViewItem(entry.TestLocationFileName);
                listItem.Checked = entry.State != TestLocationConfiguration.TestState.Disabled;
                listItem.Tag = entry;
                listItem.ToolTipText = string.Format("Location: {0}\r\nStatus: {1}\r\nLoaded: {2}\r\nTest type: {3}", entry.TestLocation, entry.State, entry.InUse, entry.ManagerType);
                listView.Items.Add(listItem);
            }
        }

        public override void SaveSettings() 
        {
            //TODO settings may be lost when a test case is temporarily unavailable - will overwrite the list 
            StringCollection tmpList = new StringCollection();
            bool changed = false;
            foreach (var o in listView.Items)
            {
                ListViewItem listItem = (o as ListViewItem);
                TestLocationConfiguration entry = (TestLocationConfiguration)listItem.Tag;

                if (((entry.State != TestLocationConfiguration.TestState.Disabled) != listItem.Checked)) // state changed
                {
                    changed = true;
                    if (listItem.Checked)
                    {
                        entry.State = TestLocationConfiguration.TestState.Used;
                    }
                    else
                    {
                        entry.State = TestLocationConfiguration.TestState.Disabled;
                    }
                }
            }

            if (changed)
            {
                PluginFacade.SaveSettings();
            }
        }

        public const int SORT_INDEX = 900;
        public override int GetSortIndex()
        {
            return SORT_INDEX;
        }
    }
}
