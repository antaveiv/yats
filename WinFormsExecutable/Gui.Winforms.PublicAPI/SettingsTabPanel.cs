﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Windows.Forms;

namespace Gui.Winforms.PublicAPI
{
    public partial class SettingsTabPanel : UserControl
    {
        public SettingsTabPanel()
        {
            InitializeComponent( );
        }

        public virtual string GetText()
        {
            throw new NotImplementedException("Override this method");
        }

        public virtual void LoadSettings()
        {
            throw new NotImplementedException("Override this method");
        }

        public virtual void SaveSettings()
        {
            throw new NotImplementedException("Override this method");
        }
        
        /// <summary>
        /// Return a preferred tab sort index. Low number = first tab. When implementing this method, please leave 'gaps' so that new panels can be inserted without updating other code
        /// </summary>
        /// <returns></returns>
        public virtual int GetSortIndex()
        {
            return int.MaxValue; 
        }
    }
}
