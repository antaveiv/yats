﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.Interface;
using System;

namespace yats.Gui.Winforms.Components.TestCaseTree
{
    /// <summary>
    /// Returns a sorted test case list without hierarchy
    /// </summary>
    class PlainTestCaseHierarchy: ITestCaseHierarchyProvider
    {
        protected ITestCasePropertiesProvider properties;
        protected ITestRepositoryManagerCollection testRepository;
        public PlainTestCaseHierarchy(ITestCasePropertiesProvider properties, ITestRepositoryManagerCollection testRepository)
        {
            this.properties = properties;
            this.testRepository = testRepository;
            testRepository.InitializationFinished += testRepository_LoadFinished;
        }

        void testRepository_LoadFinished (object sender, RepositoryManagerCollectionEventArgs e)
        {
            RaiseChangedEvent(null);
        }

        #region ITestCaseHierarchyProvider Members

        public IEnumerable<ITestCaseInfo> GetTestsInGroup(ITestCaseGroup parent)
        {
            var result = new List<ITestCaseInfo>( );
            if (parent != null)
            {
                //should not happen
                return result;
            }

            foreach (var mgr in testRepository.Managers)
            {
                result.AddRange( mgr.TestCases );
            }
            result.Sort( delegate(ITestCaseInfo p1, ITestCaseInfo p2) { return (properties.GetShortName( p1 )).CompareTo( properties.GetShortName( p2 ) ); } );
            return result;
        }

        static List<ITestCaseGroup> emptyGroups = new List<ITestCaseGroup>(0);
        public IEnumerable<ITestCaseGroup> GetGroupsInGroup(ITestCaseGroup parent)
        {
            return emptyGroups;
        }

        public event EventHandler<TestCaseEventArgs> Changed;
        
        #endregion
        
        public void RaiseChangedEvent(ITestCase testCase)
        {
            if(Changed != null)
            {
                Changed(this, new TestCaseEventArgs() { TestCase = testCase });
            }
        }
    }
}
