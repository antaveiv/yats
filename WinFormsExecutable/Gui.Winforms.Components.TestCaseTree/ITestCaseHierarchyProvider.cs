﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Drawing;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.Interface;
using System;

namespace yats.Gui.Winforms.Components.TestCaseTree
{
    public interface ITestCaseGroup
    {
        string Name { get; }
        string Description { get; }
        Image Icon { get; }
    }
    //An interface that provides methods to group test cases into a tree
    internal interface ITestCaseHierarchyProvider
    {
        /// <summary>
        /// Returns test cases in a group
        /// </summary>
        /// <param name="parent">If null - top level</param>
        /// <returns></returns>
        IEnumerable<ITestCaseInfo> GetTestsInGroup(ITestCaseGroup parent);
        /// <summary>
        /// Returns test case group in a group
        /// </summary>
        /// <param name="parent">If null - top level</param>
        /// <returns></returns>
        IEnumerable<ITestCaseGroup> GetGroupsInGroup(ITestCaseGroup parent);
        event EventHandler<TestCaseEventArgs> Changed;
    }
}
