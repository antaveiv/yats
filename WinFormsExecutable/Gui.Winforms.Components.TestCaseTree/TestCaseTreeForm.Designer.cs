﻿namespace yats.Gui.Winforms.Components.TestCaseTree
{
    partial class TestCaseTreeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( TestCaseTreeForm ) );
            this.testCaseTreePanel = new yats.Gui.Winforms.Components.TestCaseTree.TestCaseTreePanel( );
            this.SuspendLayout( );
            // 
            // testCaseTreePanel
            // 
            this.testCaseTreePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.testCaseTreePanel.Location = new System.Drawing.Point( 0, 0 );
            this.testCaseTreePanel.Name = "testCaseTreePanel";
            this.testCaseTreePanel.Size = new System.Drawing.Size( 292, 266 );
            this.testCaseTreePanel.TabIndex = 0;
            // 
            // TestCaseTreeForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 292, 266 );
            this.Controls.Add( this.testCaseTreePanel );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject( "$this.Icon" )));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TestCaseTreeForm";
            this.ShowInTaskbar = false;
            this.Text = "Test case repository";
            this.ResumeLayout( false );

        }

        #endregion

        private TestCaseTreePanel testCaseTreePanel;
    }
}