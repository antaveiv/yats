﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using WeifenLuo.WinFormsUI.Docking;
using yats.TestRepositoryManager.Interface;
using System;
using yats.Utilities;

namespace yats.Gui.Winforms.Components.TestCaseTree
{
    public partial class TestCaseTreeForm : DockContent
    {
        public ITestCaseEditHandler TestCaseEditHandler
        {
            get { return testCaseTreePanel.TestCaseEditHandler; }
            set { testCaseTreePanel.TestCaseEditHandler = value; }
        }

        public TestCaseTreeForm()
        {
            InitializeComponent();
            testCaseTreePanel.TestDoubleClick += testCaseTreePanel_TestDoubleClick;
            testCaseTreePanel.TestSelectionChanged += testCaseTreePanel_TestSelectionChanged;
            testCaseTreePanel.OptionsClicked += new System.EventHandler(testCaseTreePanel_OptionsClicked);
            this.TabText = this.Text = "Test cases";
        }

        public TestCaseTreeForm(ITestRepositoryManagerCollection testRepository):this()
        {
            testCaseTreePanel.Initialize(testRepository);
        }

        public event EventHandler<TestCaseInfoEventArgs> OnTestDoubleClick;
        /// <summary>
        /// Reports test cases selected in the tree
        /// </summary>
        public event EventHandler<TestCasesInfoEventArgs> OnTestSelectionChanged;
        public event EventHandler OnOptionsClicked;

        void testCaseTreePanel_TestSelectionChanged (object sender, TestCasesInfoEventArgs e)
        {
            // just forward the panel event 
            if(OnTestSelectionChanged != null)
            {
                OnTestSelectionChanged( this, new TestCasesInfoEventArgs(e.TestCaseInfo) );
            }
        }

        void testCaseTreePanel_TestDoubleClick (object sender, TestCaseInfoEventArgs e)
        {
            if(OnTestDoubleClick != null)
            {
                OnTestDoubleClick( this, new TestCaseInfoEventArgs(e.TestCaseInfo) );
            }
        }

        void testCaseTreePanel_OptionsClicked(object sender, System.EventArgs e)
        {
            if (OnOptionsClicked != null)
            {
                foreach (EventHandler handler in OnOptionsClicked.GetInvocationList())
                {
                    try
                    {
                        handler(this, e);
                    }
                    catch (Exception ex)
                    {
                        CrashLog.Write(ex, "Error in TestCaseTreeForm.OptionsClicked " + handler.Method.Name);
                    }
                }
            }
        }
    }
}
