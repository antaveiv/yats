﻿namespace yats.Gui.Winforms.Components.TestCaseTree
{
    partial class TestCaseTreePanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestCaseTreePanel));
            this.tree = new Aga.Controls.Tree.TreeViewAdv();
            this.testNameColumn = new Aga.Controls.Tree.TreeColumn();
            this.descriptionColumn = new Aga.Controls.Tree.TreeColumn();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nodeTextBox1 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.nodeTextBox2 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.searchPanel = new System.Windows.Forms.Panel();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btShowHierarchical = new System.Windows.Forms.ToolStripButton();
            this.btShowAlphabetical = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btRefresh = new System.Windows.Forms.ToolStripButton();
            this.btShowDescription = new System.Windows.Forms.ToolStripButton();
            this.btOptions = new System.Windows.Forms.ToolStripButton();
            this.contextMenuStrip.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.searchPanel.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tree
            // 
            this.tree.AccessibleName = "Test cases";
            this.tree.BackColor = System.Drawing.SystemColors.Window;
            this.tree.Columns.Add(this.testNameColumn);
            this.tree.Columns.Add(this.descriptionColumn);
            this.tree.ContextMenuStrip = this.contextMenuStrip;
            this.tree.DefaultToolTipProvider = null;
            this.tree.DisplayDraggingNodes = true;
            this.tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tree.DragDropMarkColor = System.Drawing.Color.Black;
            this.tree.FullRowSelect = true;
            this.tree.LineColor = System.Drawing.SystemColors.ControlDark;
            this.tree.Location = new System.Drawing.Point(0, 0);
            this.tree.Model = null;
            this.tree.Name = "tree";
            this.tree.NodeControls.Add(this.nodeTextBox1);
            this.tree.NodeControls.Add(this.nodeTextBox2);
            this.tree.SelectedNode = null;
            this.tree.SelectionMode = Aga.Controls.Tree.TreeSelectionMode.MultiSameParent;
            this.tree.Size = new System.Drawing.Size(494, 342);
            this.tree.TabIndex = 0;
            this.tree.Text = "treeViewAdv1";
            this.tree.UseColumns = true;
            this.tree.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tree_ItemDrag);
            this.tree.NodeMouseDoubleClick += new System.EventHandler<Aga.Controls.Tree.TreeNodeAdvMouseEventArgs>(this.tree_NodeMouseDoubleClick);
            this.tree.SelectionChanged += new System.EventHandler(this.tree_SelectionChanged);
            this.tree.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tree_KeyUp);
            // 
            // testNameColumn
            // 
            this.testNameColumn.Header = "Test case";
            this.testNameColumn.SortOrder = System.Windows.Forms.SortOrder.None;
            this.testNameColumn.TooltipText = null;
            this.testNameColumn.Width = 180;
            // 
            // descriptionColumn
            // 
            this.descriptionColumn.Header = "Description";
            this.descriptionColumn.SortOrder = System.Windows.Forms.SortOrder.None;
            this.descriptionColumn.TooltipText = null;
            this.descriptionColumn.Width = 250;
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem,
            this.editToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(145, 48);
            this.contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip_Opening);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Visible = false;
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // nodeTextBox1
            // 
            this.nodeTextBox1.DataPropertyName = "Name";
            this.nodeTextBox1.DisplayHiddenContentInToolTip = false;
            this.nodeTextBox1.IncrementalSearchEnabled = true;
            this.nodeTextBox1.LeftMargin = 3;
            this.nodeTextBox1.ParentColumn = this.testNameColumn;
            this.nodeTextBox1.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeTextBox2
            // 
            this.nodeTextBox2.DataPropertyName = "Description";
            this.nodeTextBox2.IncrementalSearchEnabled = true;
            this.nodeTextBox2.LeftMargin = 3;
            this.nodeTextBox2.ParentColumn = this.descriptionColumn;
            this.nodeTextBox2.Trimming = System.Drawing.StringTrimming.EllipsisWord;
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.tree);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.searchPanel);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(494, 373);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(494, 398);
            this.toolStripContainer1.TabIndex = 2;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // searchPanel
            // 
            this.searchPanel.Controls.Add(this.tbSearch);
            this.searchPanel.Controls.Add(this.label1);
            this.searchPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.searchPanel.Location = new System.Drawing.Point(0, 342);
            this.searchPanel.Name = "searchPanel";
            this.searchPanel.Size = new System.Drawing.Size(494, 31);
            this.searchPanel.TabIndex = 1;
            this.searchPanel.Visible = false;
            // 
            // tbSearch
            // 
            this.tbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearch.Location = new System.Drawing.Point(44, 3);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(447, 20);
            this.tbSearch.TabIndex = 3;
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            this.tbSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbSearch_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Search";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btShowHierarchical,
            this.btShowAlphabetical,
            this.toolStripSeparator1,
            this.btRefresh,
            this.btShowDescription,
            this.btOptions});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(133, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // btShowHierarchical
            // 
            this.btShowHierarchical.Checked = true;
            this.btShowHierarchical.CheckOnClick = true;
            this.btShowHierarchical.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btShowHierarchical.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btShowHierarchical.Image = ((System.Drawing.Image)(resources.GetObject("btShowHierarchical.Image")));
            this.btShowHierarchical.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btShowHierarchical.Name = "btShowHierarchical";
            this.btShowHierarchical.Size = new System.Drawing.Size(23, 22);
            this.btShowHierarchical.Text = "Group by test case assembly";
            this.btShowHierarchical.CheckedChanged += new System.EventHandler(this.btShowHierarchical_CheckedChanged);
            // 
            // btShowAlphabetical
            // 
            this.btShowAlphabetical.CheckOnClick = true;
            this.btShowAlphabetical.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btShowAlphabetical.Image = ((System.Drawing.Image)(resources.GetObject("btShowAlphabetical.Image")));
            this.btShowAlphabetical.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btShowAlphabetical.Name = "btShowAlphabetical";
            this.btShowAlphabetical.Size = new System.Drawing.Size(23, 22);
            this.btShowAlphabetical.Text = "List test cases by name";
            this.btShowAlphabetical.CheckedChanged += new System.EventHandler(this.btShowAlphabetical_CheckedChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btRefresh
            // 
            this.btRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btRefresh.Image")));
            this.btRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btRefresh.Name = "btRefresh";
            this.btRefresh.Size = new System.Drawing.Size(23, 22);
            this.btRefresh.Text = "Reload test cases";
            this.btRefresh.Click += new System.EventHandler(this.btRefresh_Click);
            // 
            // btShowDescription
            // 
            this.btShowDescription.Checked = true;
            this.btShowDescription.CheckOnClick = true;
            this.btShowDescription.CheckState = System.Windows.Forms.CheckState.Checked;
            this.btShowDescription.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btShowDescription.Image = ((System.Drawing.Image)(resources.GetObject("btShowDescription.Image")));
            this.btShowDescription.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btShowDescription.Name = "btShowDescription";
            this.btShowDescription.Size = new System.Drawing.Size(23, 22);
            this.btShowDescription.Text = "Show type details";
            this.btShowDescription.ToolTipText = "Show description";
            this.btShowDescription.CheckedChanged += new System.EventHandler(this.btShowDescription_CheckedChanged);
            // 
            // btOptions
            // 
            this.btOptions.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btOptions.Image = ((System.Drawing.Image)(resources.GetObject("btOptions.Image")));
            this.btOptions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btOptions.Name = "btOptions";
            this.btOptions.Size = new System.Drawing.Size(23, 22);
            this.btOptions.Text = "Options";
            this.btOptions.Click += new System.EventHandler(this.btOptions_Click);
            // 
            // TestCaseTreePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "TestCaseTreePanel";
            this.Size = new System.Drawing.Size(494, 398);
            this.contextMenuStrip.ResumeLayout(false);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.searchPanel.ResumeLayout(false);
            this.searchPanel.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Aga.Controls.Tree.TreeViewAdv tree;
        private Aga.Controls.Tree.TreeColumn testNameColumn;
        private Aga.Controls.Tree.TreeColumn descriptionColumn;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox1;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btShowHierarchical;
        private System.Windows.Forms.ToolStripButton btShowAlphabetical;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btShowDescription;
        private System.Windows.Forms.ToolStripButton btOptions;
        private System.Windows.Forms.ToolStripButton btRefresh;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.Panel searchPanel;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label label1;
    }
}
