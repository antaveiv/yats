﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.Interface;
using System;

namespace yats.Gui.Winforms.Components.TestCaseTree
{
    class UnfilteredTestCaseProvider: ITestCaseProvider
    {
        protected ITestRepositoryManagerCollection testRepository;

        public UnfilteredTestCaseProvider(ITestRepositoryManagerCollection testRepository)
        {
            this.testRepository = testRepository;
        }

        #region ITestCaseProvider Members

        public IEnumerable<ITestCaseInfo> GetTestCases()
        {
            var result = new List<ITestCaseInfo>( );

            foreach (var mgr in testRepository.Managers)
            {
                result.AddRange(mgr.TestCases);
            }
            return result;
        }

        public event EventHandler<TestCaseEventArgs> Added;
        public event EventHandler<TestCaseEventArgs> Removed;
        public event EventHandler<TestCaseEventArgs> Changed;

        #endregion

        public void RaiseAddedEvent(ITestCase testCase)
        {
            if(Added != null)
            {
                Added(this, new TestCaseEventArgs() { TestCase = testCase });
            }
        }

        public void RaiseRemovedEvent(ITestCase testCase)
        {
            if(Removed != null)
            {
                Removed(this, new TestCaseEventArgs() { TestCase = testCase });
            }
        }

        public void RaiseChangedEvent(ITestCase testCase)
        {
            if(Changed != null)
            {
                Changed(this, new TestCaseEventArgs() { TestCase = testCase });
            }
        }
    }
}
