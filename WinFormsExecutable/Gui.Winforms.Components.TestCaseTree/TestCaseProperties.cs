﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Drawing;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.Interface;
using System;

namespace yats.Gui.Winforms.Components.TestCaseTree
{
    class TestCaseProperties : ITestCasePropertiesProvider
    {
        #region ITestCasePropertiesProvider Members

        public string GetShortName(ITestCaseInfo test)
        {
            return test.RepositoryManager.GetTestName(test.TestCase);// test.TestCase.GetType().Name;
        }

        public string GetDescription(ITestCaseInfo test)
        {
            return test.RepositoryManager.GetTestDescription(test.TestCase); 
        }

        public Image GetIcon(ITestCaseInfo test)
        {
            return null;
        }

        public event EventHandler<TestCaseEventArgs> Changed;

        #endregion

        public void RaiseChangedEvent(ITestCase testCase)
        {
            if(Changed != null)
            {
                Changed(this, new TestCaseEventArgs() { TestCase = testCase });
            }
        }
    }
}
