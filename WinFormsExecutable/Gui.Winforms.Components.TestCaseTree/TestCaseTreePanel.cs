﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using yats.Gui.Winforms.Components.ClipboardObjects;
using yats.TestRepositoryManager.Interface;
using yats.Utilities;
using yats.TestRepositoryManager.Discovery;
using yats.TestCase.Interface;

namespace yats.Gui.Winforms.Components.TestCaseTree
{
    //TODO: try out tree AsyncExpanding property
    public partial class TestCaseTreePanel : UserControl
    {        
        //allows external code to control the Edit functionality
        internal ITestCaseEditHandler TestCaseEditHandler { get; set; }
        protected ITestRepositoryManagerCollection testRepository;
        private TreeModel treeModel;

        public TestCaseTreePanel()
        {
            InitializeComponent();
            this.descriptionColumn.IsVisible = this.btShowDescription.Checked = Properties.Settings.Default.ShowDetails; ;
        }

        internal void Initialize(ITestRepositoryManagerCollection testRepository)
        {
            this.testRepository = testRepository;
            var properties = new TestCaseProperties() ;
            ITestCaseHierarchyProvider hierarchy;
            ITestCaseProvider TestCases = new UnfilteredTestCaseProvider(testRepository);

            if (Properties.Settings.Default.HierarchyProviderClassName == typeof(PlainTestCaseHierarchy).AssemblyQualifiedName)
            {
                hierarchy = new PlainTestCaseHierarchy(properties, testRepository);
                treeModel = new TreeModel(hierarchy, properties, TestCases, tree);
                btShowAlphabetical.Checked = true;
                btShowHierarchical.Checked = false;
            }
            else
            { 
                hierarchy = new AssemblyTestCaseHierarchy(properties, testRepository);
                treeModel = new TreeModel(hierarchy, properties, TestCases, tree);
                btShowAlphabetical.Checked = false;
                btShowHierarchical.Checked = true;
            }
                                    
            tree.Model = treeModel;
            hierarchy.Changed += hierarchy_Changed;
            tree.ExpandAll();
        }

        void hierarchy_Changed(object sender, TestCaseEventArgs e)
        {
            tree.ExpandAll();
        }

        public event EventHandler<TestCaseInfoEventArgs> TestDoubleClick;
        
        /// <summary>
        /// Reports test cases selected in the tree
        /// </summary>
        public event EventHandler<TestCasesInfoEventArgs> TestSelectionChanged;

        private void tree_SelectionChanged(object sender, EventArgs e)
        {
            if(TestSelectionChanged != null)
            {
                List<ITestCaseInfo> selected = GetSelectedTestCases(false);
                TestSelectionChanged( this, new TestCasesInfoEventArgs(selected) );
            }
        }

        private void tree_NodeMouseDoubleClick(object sender, Aga.Controls.Tree.TreeNodeAdvMouseEventArgs e)
        {
            if(TestDoubleClick != null) // is event subscribed?
            {
                if(e.Node == null)
                { // nothing selected
                    return;
                }

                TestCaseNode node = e.Node.Tag as TestCaseNode;

                if(node == null)
                { // not a test case node
                    return;
                }

                TestDoubleClick( this, new TestCaseInfoEventArgs(node.TestCaseInfo) ); // raise event
            }
        }

        private void tree_ItemDrag(object sender, ItemDragEventArgs e)
        {
            System.Diagnostics.Debug.Assert( !this.InvokeRequired, "InvokeRequired" );

            List<ITestCaseInfo> clones = GetSelectedTestCases(true);
            tree.DoDragDrop( clones, DragDropEffects.Move | DragDropEffects.Copy );
        }

        protected List<ITestCaseInfo> GetSelectedTestCases(bool returnClones)
        {
            System.Diagnostics.Debug.Assert(!this.InvokeRequired, "InvokeRequired");

            List<ITestCaseInfo> result = new List<ITestCaseInfo>();
            foreach (var selNode in tree.SelectedNodes)
            {
                TestCaseNode node = selNode.Tag as TestCaseNode;

                if (node != null)
                {
                    if (returnClones)
                    {
                        result.Add(node.TestCaseInfo.Clone());
                    }
                    else
                    {
                        result.Add(node.TestCaseInfo);
                    }
                }
            }
            return result;
        }

        private void contextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            copyToolStripMenuItem.Enabled = tree.SelectedNodes.Count > 0;
            foreach (var node in tree.SelectedNodes) {
                if (node.Tag is TestCaseNode == false)
                {
                    copyToolStripMenuItem.Enabled = false;
                }
            }

            editToolStripMenuItem.Visible = tree.SelectedNodes.Count == 1 && tree.SelectedNodes[0].Tag is TestCaseNode && (TestCaseEditHandler != null)? TestCaseEditHandler.CanEdit((tree.SelectedNodes[0].Tag as TestCaseNode).TestCaseInfo):false;
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                List<ITestCaseInfo> selected = GetSelectedTestCases(false);
                if (selected.Count == 0)
                {
                    return;
                }
                new ClipboardTestCaseContents(selected).CopyToClipboard();
            }
            catch (Exception ex)
            {
                yats.Utilities.CrashLog.Write(ex, "copyToolStripMenuItem_Click");
            }
        }

        private void btShowDescription_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.ShowDetails = this.descriptionColumn.IsVisible = btShowDescription.Checked;
            Properties.Settings.Default.Save();
        }

        private void btShowAlphabetical_CheckedChanged(object sender, EventArgs e)
        {
            if (btShowAlphabetical.Checked && !(treeModel.Hierarchy is PlainTestCaseHierarchy))
            {
                Properties.Settings.Default.HierarchyProviderClassName = typeof(PlainTestCaseHierarchy).AssemblyQualifiedName;
                Properties.Settings.Default.Save();
                Initialize(this.testRepository);
                btShowHierarchical.Checked = false;
                tree.ShowPlusMinus = tree.ShowLines = false;
            }
            else
            {
                btShowHierarchical.Checked = true;
            }
        }

        private void btShowHierarchical_CheckedChanged(object sender, EventArgs e)
        {
            if (btShowHierarchical.Checked && !(treeModel.Hierarchy is AssemblyTestCaseHierarchy))
            {
                Properties.Settings.Default.HierarchyProviderClassName = typeof(AssemblyTestCaseHierarchy).AssemblyQualifiedName;
                Properties.Settings.Default.Save();
                Initialize(this.testRepository);
                btShowAlphabetical.Checked = false;
                tree.ShowPlusMinus = tree.ShowLines = true;
            }
            else
            {
                btShowAlphabetical.Checked = true;
            }
        }

        public event EventHandler OptionsClicked;

        private void btOptions_Click(object sender, EventArgs e)
        {
            if (OptionsClicked != null)
            {
                foreach (EventHandler handler in OptionsClicked.GetInvocationList())
                {
                    try
                    {
                        handler(this, new EventArgs());
                    }
                    catch (Exception ex)
                    {
                        CrashLog.Write(ex, "Error in TestCaseTreePanel.OptionsClicked " + handler.Method.Name);
                    }
                }
            }
        }

        private void btRefresh_Click(object sender, EventArgs e)
        {
            TestCaseRepository.Instance.Managers.Initialize(true);
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (TestCaseEditHandler != null)
            {
                foreach (var item in tree.SelectedNodes)
                {
                    
                    if (tree.SelectedNodes[0].Tag is TestCaseNode) 
                    {
                        TestCaseEditHandler.HandleEditRequest((tree.SelectedNodes[0].Tag as TestCaseNode).TestCaseInfo);
                    }
                }
            }
        }

        private void tree_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Control && (char)e.KeyValue == 'F')
            {
                // ctrl+f
                if (searchPanel.Visible)
                {
                    HideSearchField();
                }
                else
                {
                    ShowSearchField();
                }
                e.Handled = true;
            }
            else if (!e.Control && !e.Alt)
            {
                if (e.KeyCode == Keys.Escape)
                {
                    HideSearchField();
                }
                else
                {
                    ShowSearchField();
                    tbSearch.Text += ((char)e.KeyValue).ToString().ToLower();
                    tbSearch.SelectionStart = tbSearch.Text.Length;
                    tbSearch.DeselectAll();
                }
                e.Handled = true;
            }
        }

        private void HideSearchField()
        {
            tbSearch.Text = "";
            searchPanel.Visible = false;
            tree.Focus();
        }

        private void ShowSearchField()
        {
            searchPanel.Visible = true;
            tbSearch.Focus();
        }

        private void tbSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Control && (char)e.KeyValue == 'F')
            {
                // ctrl+f - select all text
                tbSearch.SelectAll();
                e.Handled = true;
            }
            if (e.KeyCode == Keys.Escape)
            {
                HideSearchField();
                e.Handled = true;
            }
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbSearch.Text.Trim()))
            {
                treeModel.SetSearchString(null);
                return;
            }
            string[] parts = tbSearch.Text.Trim().ToLower().Split(' ');
            treeModel.SetSearchString(parts);
        }
    }
}
