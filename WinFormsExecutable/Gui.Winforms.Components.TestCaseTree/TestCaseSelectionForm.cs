﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Windows.Forms;
using yats.TestRepositoryManager.Interface;

namespace yats.Gui.Winforms.Components.TestCaseTree
{
    public partial class TestCaseSelectionForm : Form
    {
        public TestCaseSelectionForm()
        {
            InitializeComponent( );
            testCaseTreePanel.TestSelectionChanged += testCaseTreePanel_TestSelectionChanged;
            testCaseTreePanel.TestDoubleClick += testCaseTreePanel_TestDoubleClick;
        }

        public TestCaseSelectionForm(ITestRepositoryManagerCollection testRepository)
            : this( )
        {
            testCaseTreePanel.Initialize( testRepository );
        }

        List<ITestCaseInfo> result = new List<ITestCaseInfo>( );

        void testCaseTreePanel_TestDoubleClick(object sender, TestCaseInfoEventArgs e)
        {
            result.Clear( );
            result.Add( e.TestCaseInfo );
            DialogResult = DialogResult.OK;
        }

        void testCaseTreePanel_TestSelectionChanged (object sender, TestCasesInfoEventArgs e)
        {
            if(e.TestCaseInfo != null)
            {
                result = e.TestCaseInfo;
            }
            else
            {
                result.Clear( );
            }

            btOK.Enabled = e.TestCaseInfo != null && e.TestCaseInfo.Count > 0;
        }
                
        // call to override the test case order
        public static List<ITestCaseInfo> Get(ITestRepositoryManagerCollection testRepository)
        {
            TestCaseSelectionForm form = new TestCaseSelectionForm( testRepository );
            List<ITestCaseInfo> result = new List<ITestCaseInfo>( );
            
            if(form.ShowDialog() != DialogResult.OK)
            {
                return result;
            }
            
            //Make clones, otherwise same test case object reference is returned
            foreach(var r in form.result)
            {
                result.Add( r.Clone( ) );
            }
            return result;
        }
    }
}
