﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Linq;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.Interface;
using System;

namespace yats.Gui.Winforms.Components.TestCaseTree
{
    class AssemblyTestCaseHierarchy : ITestCaseHierarchyProvider
    {
        protected ITestCasePropertiesProvider properties;
        protected ITestRepositoryManagerCollection testRepository;
        public AssemblyTestCaseHierarchy(ITestCasePropertiesProvider properties, ITestRepositoryManagerCollection testRepository)
        {
            this.properties = properties;
            this.testRepository = testRepository;
            testRepository.InitializationFinished += testRepository_LoadFinished;
        }

        void testRepository_LoadFinished (object sender, RepositoryManagerCollectionEventArgs e)
        {
            RaiseChangedEvent(null);
        }

        #region ITestCaseHierarchyProvider Members

        public IEnumerable<ITestCaseInfo> GetTestsInGroup(ITestCaseGroup parent)
        {
            var result = new List<ITestCaseInfo>( );
            if (parent == null)
            {
                return result;                                
            }
            else
            {
                foreach (var mgr in testRepository.Managers)
                {
                    foreach(var tc in mgr.TestCases)
                    {
                        if (mgr.GetGroupName(tc.TestCase) == parent.Name)
                        {
                            result.Add(tc);
                        }
                    }
                    //result.AddRange(mgr.TestCases.Where(mgr.GetGroupName( x=>x.TestCase.GetType().Assembly.GetName().Name == parent.Name));
                }
            }
            result.Sort( delegate(ITestCaseInfo p1, ITestCaseInfo p2) { return (properties.GetShortName( p1 )).CompareTo( properties.GetShortName( p2 ) ); } );
            return result;
        }

        static List<ITestCaseGroup> emptyGroups = new List<ITestCaseGroup>(0);
        public IEnumerable<ITestCaseGroup> GetGroupsInGroup(ITestCaseGroup parent)
        {
            List<ITestCaseGroup> result = new List<ITestCaseGroup>();
            
            if (parent == null)
            {
                HashSet<string> uniqueGroups = new HashSet<string>();

                foreach (var mgr in testRepository.Managers)
                {
                    foreach (var tc in mgr.TestCases)
                    {
                        var name = mgr.GetGroupName(tc.TestCase);
                        if (uniqueGroups.Add(name))
                        {
                            result.Add(new TestCaseGroup(name));
                        }
                    }
                }

                result.Sort((ITestCaseGroup g1, ITestCaseGroup g2) => { return g1.Name.CompareTo(g2.Name); });
            }            
            
            return result;
        }

        public event EventHandler<TestCaseEventArgs> Changed;
        
        #endregion
        
        public void RaiseChangedEvent(ITestCase testCase)
        {
            if(Changed != null)
            {
                Changed(this, new TestCaseEventArgs() { TestCase = testCase });
            }
        }

        class TestCaseGroup : ITestCaseGroup
        {
            public TestCaseGroup(string name)
            {
                this.Name = name;
            }

            public string Name
            {
                get;
                private set;
            }

            public string Description
            {
                get;
                private set;
            }

            public System.Drawing.Image Icon
            {
                get { return null; }
            }
        }
    }
}
