﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using Aga.Controls.Tree;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.Interface;
using System.Linq;

namespace yats.Gui.Winforms.Components.TestCaseTree
{
    internal class TestCaseTreeNode : Aga.Controls.Tree.Node<TestCaseTreeNode>
    {
        public virtual String Name
        {
            get { return ""; }
        }

        public virtual String Description
        {
            get { return ""; }
        }

        protected TreeModel model;
        public TestCaseTreeNode(TreeModel model)
        {
            this.model = model;
        }
        public TestCaseTreeNode()
        {
        }
    }

    internal class TestCaseNode : TestCaseTreeNode
    {
        public override String Name
        {
            get { return model.Properties.GetShortName(TestCaseInfo); }
        }

        public override String Description
        {
            get { return model.Properties.GetDescription(TestCaseInfo); }
        }

        public ITestCaseInfo TestCaseInfo;
        public TestCaseNode(TreeModel model, ITestCaseInfo test)
            : base( model )
        {
            this.TestCaseInfo = test;
        }
    }

    internal class TestGroupNode : TestCaseTreeNode
    {
        public override String Name
        {
            get { return TestCaseGroup.Name; }
        }

        public override String Description
        {
            get { return TestCaseGroup.Description; }
        }

        internal ITestCaseGroup TestCaseGroup;
        public TestGroupNode(TreeModel model, ITestCaseGroup group) : base(model)
        {
            this.TestCaseGroup = group;
        }
    }

    internal class TreeModel : ITreeModel
    {
        internal ITestCaseHierarchyProvider Hierarchy;
        internal ITestCasePropertiesProvider Properties;
        internal ITestCaseProvider TestCases;
        TreeViewAdv tree;

        public TreeModel(ITestCaseHierarchyProvider Hierarchy, ITestCasePropertiesProvider Properties, ITestCaseProvider TestCases, TreeViewAdv tree)
        {
            this.Hierarchy = Hierarchy;
            this.Properties = Properties;
            this.TestCases = TestCases;
            Hierarchy.Changed += Model_Changed;
            TestCases.Added += Model_Changed;
            TestCases.Changed += Model_Changed;
            TestCases.Removed += Model_Changed;
            Properties.Changed += Model_Changed;
            this.tree = tree;
        }

        void Model_Changed(object sender, TestCaseEventArgs e)
        {
            OnStructureChanged();
        }

        private TreePath GetPath(TestCaseTreeNode item)
        {
            if (item == null)
                return TreePath.Empty;
            else
            {
                Stack<object> stack = new Stack<object>();
                while (item != null)
                {
                    stack.Push(item);
                    item = item.Parent;
                }
                return new TreePath(stack.ToArray());
            }
        }

        public System.Collections.IEnumerable GetChildren(TreePath treePath)
        {
            List<TestCaseTreeNode> result = new List<TestCaseTreeNode>();

            if (treePath.LastNode is TestCaseNode)
            {
                return result;
            }

            ITestCaseGroup group = null;
            if (treePath.LastNode is TestGroupNode)//otherwise top level - leave GroupNode as null to query top level
            {
                group = (treePath.LastNode as TestGroupNode).TestCaseGroup;
            }

            foreach (var testGroup in Hierarchy.GetGroupsInGroup(group)) // query the interface object for children nodes and add them to the result
            {
                result.Add(new TestGroupNode(this, testGroup));
            }

            foreach (var testCase in Hierarchy.GetTestsInGroup(group)) // query the interface object for children nodes and add them to the result
            {
                if (search != null && search.Count() > 0)
                {
                    string name = testCase.RepositoryManager.GetTestName(testCase.TestCase);
                    if (search.Where(s => name.ToLower().Contains(s)).Count() > 0)
                    {
                        result.Add(new TestCaseNode(this, testCase));
                    }
                    else
                    {
                        name = testCase.RepositoryManager.GetTestDescription(testCase.TestCase);
                        if (search.Where(s => name.ToLower().Contains(s)).Count() > 0)
                        {
                            result.Add(new TestCaseNode(this, testCase));
                        }
                    }
                } else
                {
                    result.Add(new TestCaseNode(this, testCase));
                }
            }
            
            return result;
        }

        public bool IsLeaf(TreePath treePath)
        {
            return treePath.LastNode is TestCaseNode;
        }

        public event EventHandler<TreeModelEventArgs> NodesChanged;

        internal void OnNodesChanged(TestCaseTreeNode item)
        {
            if (NodesChanged != null)
            {
                TreePath path = GetPath(item.Parent);

                NodesChanged(this, new TreeModelEventArgs(path, new object[] { item }));
            }
        }

        private void Test()
        {
            if (NodesInserted != null)
            {
                NodesInserted(this, null);
            }

            if (NodesRemoved != null)
            {
                NodesRemoved(this, null);
            }
        }

        public event EventHandler<TreeModelEventArgs> NodesInserted;

        public event EventHandler<TreeModelEventArgs> NodesRemoved;

        public event EventHandler<TreePathEventArgs> StructureChanged;

        public void OnStructureChanged()
        {
            if (StructureChanged != null)
                StructureChanged(this, new TreePathEventArgs());
        }

        private string[] search;

        public void SetSearchString(string[] searchFor)
        {
            this.search = searchFor;
            Model_Changed(this, null);
        }
    }
}
