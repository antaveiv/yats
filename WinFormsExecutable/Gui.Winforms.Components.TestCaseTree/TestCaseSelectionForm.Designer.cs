﻿namespace yats.Gui.Winforms.Components.TestCaseTree
{
    partial class TestCaseSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( TestCaseSelectionForm ) );
            this.testCaseTreePanel = new yats.Gui.Winforms.Components.TestCaseTree.TestCaseTreePanel( );
            this.btOK = new System.Windows.Forms.Button( );
            this.btCancel = new System.Windows.Forms.Button( );
            this.SuspendLayout( );
            // 
            // testCaseTreePanel
            // 
            this.testCaseTreePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.testCaseTreePanel.Location = new System.Drawing.Point( 12, 12 );
            this.testCaseTreePanel.Name = "testCaseTreePanel";
            this.testCaseTreePanel.Size = new System.Drawing.Size( 560, 529 );
            this.testCaseTreePanel.TabIndex = 0;
            // 
            // btOK
            // 
            this.btOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btOK.Enabled = false;
            this.btOK.Location = new System.Drawing.Point( 416, 547 );
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size( 75, 23 );
            this.btOK.TabIndex = 4;
            this.btOK.Text = "OK";
            this.btOK.UseVisualStyleBackColor = true;
            // 
            // btCancel
            // 
            this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point( 497, 547 );
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size( 75, 23 );
            this.btCancel.TabIndex = 3;
            this.btCancel.Text = "Cancel";
            this.btCancel.UseVisualStyleBackColor = true;
            // 
            // TestCaseSelectionForm
            // 
            this.AcceptButton = this.btOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btCancel;
            this.ClientSize = new System.Drawing.Size( 584, 582 );
            this.Controls.Add( this.btOK );
            this.Controls.Add( this.btCancel );
            this.Controls.Add( this.testCaseTreePanel );
            this.Icon = ((System.Drawing.Icon)(resources.GetObject( "$this.Icon" )));
            this.Name = "TestCaseSelectionForm";
            this.ShowInTaskbar = false;
            this.Text = "Test cases";
            this.ResumeLayout( false );

        }

        #endregion

        private TestCaseTreePanel testCaseTreePanel;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.Button btCancel;
    }
}