﻿using yats.TestRepositoryManager.Interface;

namespace yats.Gui.Winforms.Components.TestCaseTree
{
    public interface ITestCaseEditHandler
    {
        bool CanEdit(ITestCaseInfo testCaseInfo);
        void HandleEditRequest(ITestCaseInfo testCaseInfo);
    }
}
