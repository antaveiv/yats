﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using yats.TestCase.Interface;
using yats.TestRepositoryManager.Interface;
using System;

namespace yats.Gui.Winforms.Components.TestCaseTree
{
    /// <summary>
    /// An interface to provide a test case list and list change events
    /// </summary>
    public interface ITestCaseProvider
    {
        IEnumerable<ITestCaseInfo> GetTestCases();
        
        event EventHandler<TestCaseEventArgs> Added;
        event EventHandler<TestCaseEventArgs> Removed;
        event EventHandler<TestCaseEventArgs> Changed;
    }
}
