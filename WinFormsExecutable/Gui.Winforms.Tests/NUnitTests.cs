﻿#if !__MonoCS__
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using yats.ExecutionQueue;
using System.Windows;
using yats.TestRepositoryManager.Interface;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using System.Reflection;
using System.IO;
using System.Windows.Forms;
using System.Security.Permissions;
using System.Threading;

namespace yats.Gui.Winforms.Components.ClipboardObjects
{
#if DEBUG
    [TestFixture]
    [Apartment(ApartmentState.STA)]
    public class NUnitTests
    {
        class TestRepositoryManagerTestStub : ITestRepositoryManager
        {
        // Raised when the repository manager is about to analyze an assembly 
        public event EventHandler<AssemblyLoadingEventArgs> BeforeLoadingTestAssembly;
        // Raised after the repository manager loads a test case assembly 
        public event EventHandler<AssemblyLoadedEventArgs> OnTestAssemblyLoad;
        // Error indication
        public event EventHandler<ExceptionEventArgs> OnError;

            public void Initialize()
            {
                throw new NotImplementedException();
            }

            public IList<ITestCaseInfo> TestCases
            {
                get { throw new NotImplementedException(); }
            }

            public string Name { get { return "Dummy"; } }

            public string GetUniqueTestId(ITestCase testCase)
            {
                return testCase.GetType().AssemblyQualifiedName;
            }

            public ITestCase GetByUniqueId(string testCaseUniqueId)
            {
                throw new NotImplementedException();
            }

            public ITestResult Execute(ITestCase testCase, IList<IParameter> testParameters, IGlobalParameterCollection globalParameters, object testRunner, string path)
            {
                throw new NotImplementedException();
            }

            public bool Cancel(ITestCase testCase)
            {
                throw new NotImplementedException();
            }

            public string GetTestName(ITestCase testCase)
            {
                return testCase.GetType().Name;
            }
            
            public string GetTestDescription(ITestCase testCase)
            {
                return testCase.GetType().FullName;
            }

            public IList<IParameter> GetParameters(ITestCase testCase)
            {
                return new List<IParameter>();
            }

            public bool GetTestResult(ITestCase testCase, IParameter param, out object value)
            {
                throw new NotImplementedException();
            }

            public void SetParameterValue(ITestCase testCase, IParameter param, object value)
            {
                throw new NotImplementedException();
            }

            public bool GetParameterValue(ITestCase testCase, IParameter param, out object value)
            {
                throw new NotImplementedException();
            }

            public bool GetParameterDefault(ITestCase testCase, IParameter param, out object value)
            {
                throw new NotImplementedException();
            }

            public bool CanParameterBeNull(ITestCase testCase, IParameter param)
            {
                throw new NotImplementedException();
            }

            public bool IsMyTestCase(ITestCase testCase)
            {
                return true;
            }

            public bool IsMyTestCase(string testCaseUniqueId)
            {
                return true;
            }

            public string GetGroupName(ITestCase testCase)
            {
                throw new NotImplementedException();
            }
        }

        class DummyTestCase : ITestCase
        {
        }

        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        [Test]
        public void TestStepContentsSerialization()
        {
            List<TestStep> steps = new List<TestStep>();

            TestStepComposite R = new TestStepComposite
            {
                ExecutionMethod = new SequentialExecutionModifier()
            };
            var manager = new TestRepositoryManagerTestStub();

            var B1 = new TestStepSingle(new DummyTestCase(), manager);
			R.Steps.Add(B1);
            B1 = new TestStepSingle(new DummyTestCase(), manager);
			R.Steps.Add(B1);
            B1 = new TestStepSingle(new DummyTestCase(), manager);
			R.Steps.Add(B1);
			
			steps.Add(R);
            AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;

            new ClipboardTestStepContents(steps, null).CopyToClipboard();

            var res = ClipboardTestStepContents.GetFromClipboard();

            Clipboard.Clear();

            Assert.NotNull(res.GetTestSteps());
            Assert.AreEqual(1, res.GetTestSteps().Count);
            Assert.AreEqual(3, (res.GetTestSteps()[0] as TestStepComposite).Steps.Count());
        }

        //TODO: when running from VisualNunitRunner.exe, the assembly is not found. WTF?
        private static Assembly OnAssemblyResolve(object sender, ResolveEventArgs e)
        {
            if (e.Name.StartsWith("Gui.Winforms.Components.ClipboardObjects,", StringComparison.OrdinalIgnoreCase))
            {
                return typeof(ClipboardTestStepContents).Assembly;
            }
            return null;
        }

        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        [Test]
        public void TestCaseContentsSerialization()
        {
            List<ITestCaseInfo> testCaseInfoList = new List<ITestCaseInfo>();
            var manager = new TestRepositoryManagerTestStub();

            testCaseInfoList.Add(new TestCaseInfo(new DummyTestCase(), manager));
            testCaseInfoList.Add(new TestCaseInfo(new DummyTestCase(), manager));

            AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;

            Clipboard.Clear();
            new ClipboardTestCaseContents(testCaseInfoList).CopyToClipboard();
            var res = ClipboardTestCaseContents.GetFromClipboard();
            Clipboard.Clear();

            Assert.AreEqual(2, res.UniqueTestCaseIds.Count);
            Assert.AreEqual(manager.GetUniqueTestId(new DummyTestCase()), res.UniqueTestCaseIds[0]);
        }

        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        [Test]
        public void TestParameterContentsSerialization()
        {
            List<IParameter> parameters = new List<IParameter>();
            GlobalParameterCollection globals = new GlobalParameterCollection();
            var mgr1 = new GlobalParameters(ParameterScope.TEST_RUN);
            globals.Add(mgr1);
            var mgr2 = new GlobalParameters(ParameterScope.GLOBAL);
            globals.Add(mgr2);


            AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;

            Clipboard.Clear();
            new ClipboardParameterContents(parameters, globals).CopyToClipboard();
            var res = ClipboardParameterContents.GetFromClipboard();
            Clipboard.Clear();

            Assert.AreEqual(parameters.Count, res.GetParameters().Count);
            Assert.IsTrue(res.GetGlobals().Managers[mgr1.ID].Scope == mgr1.Scope);
            Assert.IsTrue(res.GetGlobals().Managers[mgr2.ID].Scope == mgr2.Scope);
        }

        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        [Test]
        public void TestGlobalValueContentsSerialization()
        {
            List<GlobalValue> parameters = new List<GlobalValue>();
            Guid g1 = Guid.NewGuid();
            Guid g2 = Guid.NewGuid();
            parameters.Add(new GlobalValue(g1, g2));

            AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;

            Clipboard.Clear();

            new ClipboardGlobalValueContents(parameters).CopyToClipboard();
            var res = ClipboardGlobalValueContents.GetFromClipboard();
            Clipboard.Clear();

            Assert.AreEqual(parameters.Count, res.GetGlobalValues().Count);
            Assert.IsTrue(res.GetGlobalValues()[0].StorageId == g1);
            Assert.IsTrue(res.GetGlobalValues()[0].ParameterID == g2);
        }

        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        [Test]
        public void TestParameterContentsDefaults()
        {
            AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;

            Clipboard.Clear();
            new ClipboardParameterContents(null, null).CopyToClipboard();
            var res = ClipboardParameterContents.GetFromClipboard();
            Clipboard.Clear();
            Assert.IsNotNull(res);
            Assert.IsNotNull(res.GetParameters());
            Assert.IsEmpty(res.GetParameters());
        }
    }
#endif
}
#endif