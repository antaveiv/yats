﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using yats.Gui.Winforms.Components.DbBrowser;
using System.Threading;
using yats.Utilities;

namespace Gui.Winforms.Tests
{
#if DEBUG
    [TestFixture]
    public class LoggerFilterTests
    {
        [Test]
        public void AfterConstructor()
        {
            LoggerFilter filter = new LoggerFilter();
            Assert.IsFalse(filter.IsModified);
            filter.Deserialize(null);
            filter.Deserialize("");
            filter.Serialize();
            Assert.AreEqual(0, filter.GetLoggers().Count);
        }

        [Test]
        public void ModifiedTest()
        {
            LoggerFilter filter = new LoggerFilter();
            Assert.IsFalse(filter.IsModified);
            filter.OnFilterChanged += new EventHandler(filter_OnFilterChanged);
            Assert.IsTrue(filter.IsEnabled("unknown", "DEBUG"));
            Assert.IsTrue(filter.IsModified);
            Assert.IsTrue(filter_OnFilterChanged_called);
        }

        [Test]
        public void NotModifiedIfSetToSameValue()
        {
            LoggerFilter filter = new LoggerFilter();
            Assert.IsFalse(filter.IsModified);
            filter.Set("TEST", "FATAL");
            Assert.IsTrue(filter.IsModified);
            filter.IsModified = false;
            filter.Set("TEST", "FATAL");
            Assert.IsFalse(filter.IsModified);
            Assert.AreEqual("FATAL", filter.Get("TEST"));
        }

        [Test]
        public void HandleOnFilterChangedHandlerException()
        {
            LoggerFilter filter = new LoggerFilter();            
            filter.OnFilterChanged += new EventHandler(filter_OnFilterChanged2);
            filter.OnFilterChanged += new EventHandler(filter_OnFilterChanged3);
            Assert.IsTrue(filter.IsEnabled("unknown", "DEBUG"));
            Assert.IsTrue(filter.IsModified);
            Assert.IsTrue(filter_OnFilterChanged2_called);
            Assert.IsTrue(filter_OnFilterChanged3_called);
        }

        private bool filter_OnFilterChanged_called = false;
        void filter_OnFilterChanged(object sender, EventArgs e)
        {
            filter_OnFilterChanged_called = true;
        }

        private bool filter_OnFilterChanged2_called = false;
        void filter_OnFilterChanged2(object sender, EventArgs e)
        {
            filter_OnFilterChanged2_called = true;
        }

        private bool filter_OnFilterChanged3_called = false;
        void filter_OnFilterChanged3(object sender, EventArgs e)
        {
            filter_OnFilterChanged3_called = true;
        }
        
        const int NUM_REPETITIONS = 100;
        bool serializeThreadFunctionException = false;
        void SerializeThreadFunction(object param)
        {
            try
            {
                LoggerFilter filter = (LoggerFilter)param;
                for (int i = 0; i < NUM_REPETITIONS; i++)
                {
                    Assert.IsNotEmpty(filter.Serialize());
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex);
                serializeThreadFunctionException = true;
            }
        }

        bool deserializeThreadFunctionException = false;
        void DeserializeThreadFunction(object param)
        {
            try
            {
                LoggerFilter filter = (LoggerFilter)param;
                for (int i = 0; i < NUM_REPETITIONS; i++)
                {
                    filter.Deserialize("");
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex);
                deserializeThreadFunctionException = true;
            }
        }

        bool getThreadFunctionException = false;
        void GetThreadFunction(object param)
        {
            try
            {
                LoggerFilter filter = (LoggerFilter)param;
                for (int i = 0; i < NUM_REPETITIONS * 1000; i++)
                {
                    filter.Get(StringExtensions.RandomAlphanumericString(3));
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex);
                getThreadFunctionException = true;
            }
        }

        bool setThreadFunctionException = false;
        void SetThreadFunction(object param)
        {
            try
            {
                LoggerFilter filter = (LoggerFilter)param;
                for (int i = 0; i < NUM_REPETITIONS * 1000; i++)
                {
                    filter.Set(StringExtensions.RandomAlphanumericString(3), "INFO");
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex);
                setThreadFunctionException = true;
            }
        }

        bool isEnabledThreadFunctionException = false;
        void IsEnabledThreadFunction(object param)
        {
            try
            {
                LoggerFilter filter = (LoggerFilter)param;
                for (int i = 0; i < NUM_REPETITIONS * 1000; i++)
                {
                    filter.IsEnabled(StringExtensions.RandomAlphanumericString(3), "INFO");
                }
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.Write(ex);
                isEnabledThreadFunctionException = true;
            }
        }

        bool getLoggersThreadFunctionException = false;
        void GetLoggersThreadFunction(object param)
        {
            try
            {
                LoggerFilter filter = (LoggerFilter)param;
                for (int i = 0; i < NUM_REPETITIONS; i++)
                {
                    filter.GetLoggers();
                    Thread.Sleep(0);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex);
                getLoggersThreadFunctionException = true;
            }
        }

        [Test]
        public void MultithreadedAccess()
        {
            LoggerFilter filter = new LoggerFilter();
            var t1 = new Thread(new ParameterizedThreadStart(SerializeThreadFunction));
            var t2 = new Thread(new ParameterizedThreadStart(DeserializeThreadFunction));
            var t3 = new Thread(new ParameterizedThreadStart(GetThreadFunction));
            var t4 = new Thread(new ParameterizedThreadStart(SetThreadFunction));
            var t5 = new Thread(new ParameterizedThreadStart(IsEnabledThreadFunction));
            var t6 = new Thread(new ParameterizedThreadStart(GetLoggersThreadFunction));

            t1.Start(filter);
            t2.Start(filter);
            t3.Start(filter);
            t4.Start(filter);
            t5.Start(filter);
            t6.Start(filter);

            t1.Join();
            t2.Join();
            t3.Join();
            t4.Join();
            t5.Join();
            t6.Join();

            Assert.IsFalse(serializeThreadFunctionException);
            Assert.IsFalse(deserializeThreadFunctionException);
            Assert.IsFalse(getThreadFunctionException);
            Assert.IsFalse(setThreadFunctionException);
            Assert.IsFalse(isEnabledThreadFunctionException);
            Assert.IsFalse(getLoggersThreadFunctionException);
        }
    }
#endif
}
