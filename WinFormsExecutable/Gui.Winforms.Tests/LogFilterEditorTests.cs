﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using yats.Gui.Winforms.Components.DbBrowser;
using System.Threading;
using yats.Utilities;
using System.Windows.Forms;

namespace Gui.Winforms.Tests
{
    [TestFixture]
    public class LogFilterEditorTests
    {
        [Test]
        public void MultithreadedUpdates()
        {
            LoggerFilter filter = new LoggerFilter();            
            LogFilterEditor editor = new LogFilterEditor(filter);
            editor.Show();

            var t1 = new Thread(new ParameterizedThreadStart(SetThreadFunction));
            var t2 = new Thread(new ParameterizedThreadStart(SetThreadFunction));
            t1.Start(filter);
            t2.Start(filter);
            t1.Join();
            t2.Join();
            editor.Close();
            Assert.IsFalse(setThreadFunctionException);
        }

        const int NUM_REPETITIONS = 1000;
        bool setThreadFunctionException = false;
        void SetThreadFunction(object param)
        {
            try
            {
                LoggerFilter filter = (LoggerFilter)param;
                for (int i = 0; i < NUM_REPETITIONS; i++)
                {
                    filter.Set(StringExtensions.RandomAlphanumericString(3), "INFO");
                    Thread.Sleep(0);
                }
            }
            catch
            {
                setThreadFunctionException = true;
            }
        }
    }
}
