﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Linq;
using WeifenLuo.WinFormsUI.Docking;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.Gui.Winforms.Components.DbBrowser;
using yats.Gui.Winforms.Components.LogView;
using yats.Gui.Winforms.Components.ParameterEditor;
using yats.Gui.Winforms.Components.ParameterEditor.GlobalParameterEditor;
using yats.Gui.Winforms.Components.Settings;
using yats.Gui.Winforms.Components.TestCaseTree;
using yats.Gui.Winforms.Components.TestRunTree;
using yats.Gui.Winforms.Components.Util;
using yats.Gui.Winforms.YatsApplication.Configuration;
using yats.Gui.Winforms.YatsApplication.Controls;
using yats.Logging.Interface;
using yats.Logging.Util;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.Discovery;
using yats.TestRepositoryManager.Interface;
using yats.TestRun;
using yats.TestRun.Cancel;
using yats.Utilities;
using yats.Gui.Winforms.YatsApplication.Properties;
using log4net;
using System.Diagnostics;
using yats.TestRepositoryManager.YatsComposite;
using yats.Logging.SQLite;
using yats.Logging.XmlLogSerializer;
using yats.TestRun.Commands;
using yats.TestRun.Commands.Visitors;
using System.Threading.Tasks;
using Gui.Winforms.PublicAPI;
using yats.Utilities.Commands;

namespace yats.Gui.Winforms.YatsApplication
{
    public partial class MainForm : Form
    {
        public static readonly string DOCKPANEL_CONFIG_FILE = FileUtil.GetApplicationDataPath("DockPanel.config");
        internal readonly object DockPanelLock = new object ();
        //NOTE: When adding more dock forms, do not forget to handle them in GetContentFromPersistString()
        internal TestCaseTreeForm m_TestCaseTreeForm = null;
        internal GlobalParameterEditorForm m_GlobalParameterEditorForm = null;
        internal LogViewForm m_LogViewForm = null;
        internal FilteredLogForm m_FilteredLogForm = null;
        internal TestRunListForm m_TestRunListForm = null;
        internal ParameterTreeForm m_ParameterTreeForm = null;
        internal LogFilterEditor m_LogFilterEditor = null;
        private DeserializeDockContent m_deserializeDockContent;
        private bool m_appStartedWithTestRunParameter = false;
        public MainForm()
        {
            InitializeComponent();
            m_deserializeDockContent = new DeserializeDockContent(GetContentFromPersistString);

            InitGlobalParametersTask = InitGlobalParameters();
        }

        public MainForm(string[] args)
            : this()
        {
            try
            {
                if (args.Length != 1)
                {
                    return;
                }
                m_appStartedWithTestRunParameter = true;

                Load += async (sender, e) =>
                {
                    await Task.WhenAll(Program.InitParameterTypesTask, Program.InitTestRepositoryManagerTask, InitGlobalParametersTask);

                    LoadFiles(args);                    
                };
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex, "MainForm(args[])");
            }
        }

        Task InitGlobalParametersTask;
        async Task InitGlobalParameters()
        {
            await Task.Run(() =>
            {
                GlobalParameterSingleton.OnLoadError += (sender, e) =>
                {
                    FileUtil.BackupPush(e.FileName, 10);
                    LogManager.GetLogger(this.GetType()).Fatal("Load", e.Exception);
                    MessageBox.Show("Failed to load the global parameters. The file was backed up." + Environment.NewLine + "Error: " + Environment.NewLine + e.Exception.Message,
                        "Global parameter error"
                    );
                };

                GlobalParameterSingleton.Instance.ToString();
            });
        }

        void TestRun_ImportGlobalParameters(ITestRun loadedTestRun)
        {
            // import global parameters from a loaded test run file
            var loadedGlobals = yats.TestCase.Parameter.ExtractGlobalParameters.GetGlobals(loadedTestRun.GlobalParameters, ParameterEnumerator.GetParameters(loadedTestRun.TestSequence));

            Debug.Assert(GlobalParameterSingleton.Instance.GlobalParameters.Managers.Values.Count(x => x.Scope != ParameterScope.GLOBAL) == 0);

            ParameterImporter.ImportGlobalParameters(loadedTestRun.GlobalParameters, loadedGlobals, GlobalParameterSingleton.Instance.GlobalParameters, false, ParameterScope.GLOBAL);

            Debug.Assert(GlobalParameterSingleton.Instance.GlobalParameters.Managers.Values.Count(x => x.Scope != ParameterScope.GLOBAL) == 0);
        }

        void TestRun_AfterLoadFromFile(object sender, TestRunLoadEventArgs e)
        {
            if (e.TestRun.GlobalParameters == null)
            {
                e.TestRun.GlobalParameters = new GlobalParameterCollection();
            }

            TestRun_ImportGlobalParameters(e.TestRun);
            // all previously unseen global parameters are imported to the application's parameter store. 
            // Remove irrelevant global parameter storage from the loaded test run.
            foreach (var m in e.TestRun.GlobalParameters.Managers.Values.Where(x => x.Scope == ParameterScope.GLOBAL).ToArray())
            {
                e.TestRun.GlobalParameters.Remove(m);
            }
            foreach ( var m in GlobalParameterSingleton.Instance.GlobalParameters.Managers.Values.Where(x => x.Scope == ParameterScope.GLOBAL))
            {
                e.TestRun.GlobalParameters.Add(m);
            }
            
            // creates test case instances from their stored unique IDs. 
            if (e.TestRun.TestSequence != null)
            {
                CheckTestCasesAvailableVisitor.Process(e.TestRun.TestSequence, TestCaseRepository.Instance.Managers.Managers);
                if (SynchronizeParameterDescriptions.Process(e.TestRun.TestSequence, TestCaseRepository.Instance.Managers.Managers))
                {
                    e.TestRun.IsModified = true;
                }
                bool parametersChanged = SynchronizeLoadedParametersVisitor.Process(e.TestRun.TestSequence, TestCaseRepository.Instance.Managers, e.TestRun.GlobalParameters);
                parametersChanged |= CheckLoadedParameterTypesVisitor.Process(e.TestRun.TestSequence, e.TestRun.GlobalParameters);
                if (parametersChanged)
                {
                    e.TestRun.IsModified = true;
                    MessageBox.Show("Some test case parameters were updated since the test run was saved. Please review the test parameters", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                try
                {
                    bool modified;
                    StateMachineSanityCheckVisitor.CheckStepMachines(e.TestRun.TestSequence, out modified);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            ICommand cleanupCommand = RemoveNonExistingGlobals.Validate(e.TestRun.TestSequence, e.TestRun.GlobalParameters);
            if (cleanupCommand != null)
            {
                cleanupCommand.Execute();
            }
        }

        void TestRunForm_AddTestCasesRequest(object sender, yats.Gui.Winforms.Components.TestRunTree.TestRunTreeForm.AddTestCasesRequestEventArgs e)
        {
            e.SelectedTestCases = TestCaseSelectionForm.Get(TestCaseRepository.Instance.Managers);
            e.Selected = e.SelectedTestCases.Count > 0;
        }

        void TestRunForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            TestRunForm_StepsChanged(sender, new TestStepEventArgs(sender as TestRunTreeForm, null, new List<TestStep>()));
        }

        void TestRunForm_StepsAdded (object sender, TestStepEventArgs e)
        {
            if (Settings.Default.AssignDefaultParameterValues == false)
            {
                return;
            }

            ParameterProperties parameterProperties = new ParameterProperties();// reuse Reset method
            foreach (var step in e.Steps.OfType<TestStepSingle>())
            {
                foreach (var param in step.Parameters)
                {
                    try
                    {
                        parameterProperties.ResetToDefaultValue(step, param);
                    }
                    catch (Exception ex)
                    {
                        yats.Utilities.CrashLog.Write(ex, "Reset parameter value");
                    }
                }
            }
        }

        // refresh parameter window (if is open) when test steps are renamed
        void TestRunForm_StepsChanged (object sender, TestStepEventArgs e)
        {
        SetParameterTreeFormSteps (m_ParameterTreeForm, e.Steps, e.TestRun, (e.Sender != null) ? e.Sender.ExecutionStatus : new TestExecutionStatus(), e.Sender.CommandStack);
        }

        //shows parameter editor
        void TestRunForm_OnEditParameters(object sender, TestStepEventArgs e)
        {
            SetStepsToParamForm(e.TestRun, e.Steps, e.Sender.ExecutionStatus, e.Sender.CommandStack);
        }

        private void testParametersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { testParametersToolStripMenuItem_Click(sender, e); }));
                return;
            }
            lock (DockPanelLock)
            {
                TestRunTreeForm testRunTreeForm = DockPanel.ActiveDocument as TestRunTreeForm;
                if (testRunTreeForm != null)
                {
                    SetStepsToParamForm(testRunTreeForm.TestRun, testRunTreeForm.SelectedSteps, testRunTreeForm.ExecutionStatus, testRunTreeForm.CommandStack);
                }
                else
                {
                    SetStepsToParamForm(null, null, new TestExecutionStatus(), null);
                }    
            }            
        }

        private void SetStepsToParamForm(ITestRun testRun, IList<TestStep> steps, ITestExecutionStatus executionStatus, ICommandStack commandStack)
        {
            try
            {                
                m_ParameterTreeForm = GetParameterTreeForm();
                if (m_ParameterTreeForm == null || m_ParameterTreeForm.IsDisposed)
                {
                    return;
                }
                lock (DockPanelLock)
                {
                    m_ParameterTreeForm.Show(DockPanel);
                }
                if (DockHelper.IsDockStateAutoHide(m_ParameterTreeForm.DockState))
                {
                    DockPanel.ActiveAutoHideContent = m_ParameterTreeForm; // causes auto-hidden form to pop up
                }
                SetParameterTreeFormSteps(m_ParameterTreeForm, steps, testRun, executionStatus, commandStack);
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex, "Test parameters form show");
            }
        }

        void TestRunForm_RunScheduledPressed(object sender, EventArgs e)
        {
            TestRunTreeForm testForm = (sender as TestRunTreeForm);

            using (ScheduledRunCountdownForm form = new ScheduledRunCountdownForm())
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    switch (form.RunMode)
                    {
                        case ScheduledRunCountdownForm.ModeEnum.All:
                            TestRunForm_RunPressed(testForm, new RunPressedEventArgs(testForm, testForm.TestRun, testForm.SelectedSteps, true));
                            break;
                        case ScheduledRunCountdownForm.ModeEnum.Selected:
                            TestRunForm_OnRunSelectedSteps(testForm, new RunPressedEventArgs(testForm, testForm.TestRun, testForm.SelectedSteps, true));
                            break;
                        case ScheduledRunCountdownForm.ModeEnum.FromSelected:
                            TestRunForm_OnRunFromSelectedStep(testForm, new RunPressedEventArgs(testForm, testForm.TestRun, testForm.SelectedSteps, true));
                            break;
                    }
                }
            }
            //310: [Usability] Delayed execution - start test run at specified time
        }

        //TODO: remove dependency from TestRunTreeForm, use event interface
        void TestRunForm_RunPressed (object sender, RunPressedEventArgs e)
        {
            var testRunner = new TestRunner(e.TestRun);
            StartTest(new TestRunThread(this, e.Sender, testRunner, e.Log));
        }

        private readonly List<TestRunThread> runningTests = new List<TestRunThread>();
        void StartTest(TestRunThread testRunThread)
        {
            testRunThread.Start();
            testRunThread.OnTestRunFinished += (sender, e) => {
                lock (runningTests)
                {
                    runningTests.Remove(sender as TestRunThread);
                }
            };
            runningTests.Add(testRunThread);
        }
                
        //TODO testrun form should wait for test to finish before closing?
        void WaitForTestsToFinish()
        {
            TestRunThread first;
            do {                 // complicated because of the lock. Also, want to avoid the "collection modified" exception
                lock(runningTests) {
                    first = runningTests.FirstOrDefault();
                }
                if (first != null){
                    first.Join();
                    runningTests.Remove(first);// caught a situation that a thread was not removed from runningTests even if able to join the thread. This is a workaround
                }
            } while(first != null);
        }

        void TestRunForm_OnRunSelectedSteps(object sender, TestStepEventArgs e)
        {
            TestRunner testRunner;
            if (Util.IsNullOrEmpty(e.Steps))
            {
                return;
            }
            // run selected
            testRunner = new TestRunnerRunSelectedSteps(e.Sender.TestRun, e.Steps);
            StartTest(new TestRunThread(this, e.Sender, testRunner, false)); // no logging on middle click
        }
        
        void TestRunForm_OnRunSelectedSteps(object sender, RunPressedEventArgs e)
        {
            TestRunner testRunner;
            if (Util.IsNullOrEmpty(e.Steps))
            {
                // run all
                testRunner = new TestRunner(e.TestRun);
            }
            else
            {
                // run selected
                testRunner = new TestRunnerRunSelectedSteps(e.Sender.TestRun, e.Steps);
            }

            StartTest(new TestRunThread(this, e.Sender, testRunner, e.Log));
        }

        void TestRunForm_OnRunFromSelectedStep(object sender, RunPressedEventArgs e)
        {
            if (e.Steps.Count != 1)
            {
                return;
            }

            var testRunner = new TestRunnerRunFromStep (e.TestRun, e.Steps[0]);
            StartTest(new TestRunThread (this, e.Sender, testRunner, e.Log));
        }

        private void OptionsMenuItem_Click(object sender, EventArgs e)
        {
            int defaultTabIndex;
            int defaultPanelRequest = 0;
            if (sender is TestCaseTreeForm)
            {
                defaultPanelRequest = yats.Gui.Winforms.Components.TestCaseOrganizer.TestAssemblyCheckboxControl.SORT_INDEX;
            }

            List<SettingsTabPanel> panels = new SettingsFormPanelFactory().GetSettingsPanels(defaultPanelRequest, out defaultTabIndex);

            using (var options = new yats.Gui.Winforms.Components.Settings.OptionsForm(defaultTabIndex, panels.ToArray()))
            {
                if (options.ShowDialog() == DialogResult.OK)
                {
                    foreach (var panel in panels)
                    {
                        panel.SaveSettings();
                    }
                }
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            CancelHandlerCollection.Cancel(this, e.CloseReason.ToString());

            WaitForTestsToFinish();

            SaveFormLayout();

            //unregister event handlers - the contents will be closed soon
            DockPanel.ContentAdded -= DockPanel_ContentAdded;
            DockPanel.ContentRemoved -= DockPanel_ContentAdded;
            if (LoggerFilterSingleton.Instance.IsModified)
            {
                Settings.Default.LogFilter = LoggerFilterSingleton.Instance.Serialize();
                LoggerFilterSingleton.Instance.IsModified = false;
            }
            Settings.Default.Save();
        }

        private void SaveFormLayout()
        {
            Settings.Default.PreviousDisplayResolution = SystemInformation.VirtualScreen.Size;

            if (Settings.Default.SaveFormLayout)
            {
                // http://blogs.msdn.com/b/rprabhu/archive/2005/11/28/497792.aspx
                Settings.Default.MainFormState = this.WindowState;

                if (this.WindowState == FormWindowState.Normal)
                {
                    Settings.Default.MainFormSize = this.Size;
                    Settings.Default.MainFormLocation = this.Location;
                }
                else
                {
                    Settings.Default.MainFormSize = this.RestoreBounds.Size;
                    Settings.Default.MainFormLocation = this.RestoreBounds.Location;
                }
                Settings.Default.MainFormLocationSaved = true;

                // Save Docked forms
                DockPanel.SaveAsXml(DOCKPANEL_CONFIG_FILE);
            }
            else if (File.Exists(DOCKPANEL_CONFIG_FILE))
            {
                File.Delete(DOCKPANEL_CONFIG_FILE);
            }
        }

        private void DockPanel_ContentAdded(object sender, DockContentEventArgs e)
        {
            SaveFormLayout();
        }

        #region Loading test run, showing test run child form

        private void newTestRunBtn_Click(object sender, EventArgs e)
        {
            CreateNewTestRunForm(true);
        }

        private TestRunTreeForm CreateNewTestRunForm(bool show)
        {
            ITestRun testRun = new TestRunImplementation(null, GlobalParameterSingleton.Instance.GlobalParameters); 

            return ShowTestRunForm(testRun, null, show);
        }

        private TestRunTreeForm ShowTestRunForm(ITestRun testRun, string fileName)
        {
            return ShowTestRunForm(testRun, fileName, true);
        }

        private TestRunTreeForm ShowTestRunForm(ITestRun testRun, string fileName, bool show)
        {
            var childForm = new TestRunTreeForm(testRun, fileName, (testRun is SavedCompositeTestStep) == false);
            // pass options
            childForm.SavedToFile += TestRunForm_SavedToFile;
            childForm.RunPressed += TestRunForm_RunPressed;
            childForm.RunScheduledPressed += TestRunForm_RunScheduledPressed;
            childForm.OnRunFromSelectedStep += TestRunForm_OnRunFromSelectedStep;
            childForm.OnRunSelectedSteps += TestRunForm_OnRunSelectedSteps;
            childForm.OnEditParameters += TestRunForm_OnEditParameters;
            childForm.OnStepsMiddleClick += TestRunForm_OnRunSelectedSteps;
            childForm.FormClosed += TestRunForm_FormClosed;
            childForm.AddTestCasesRequest += TestRunForm_AddTestCasesRequest;
            childForm.OpenClicked += openButton_Click;
            childForm.ReloadFromDiskClicked += TestRunForm_ReloadFromDiskClicked;
            childForm.OnFileDrop += TestRunForm_OnFileDrop;
            childForm.StepsChanged += TestRunForm_StepsChanged;
            childForm.StepsAdded += TestRunForm_StepsAdded;
            childForm.SelectionChanged += TestRunForm_SelectionChanged;
            childForm.CloseOtherTabsPressed += TestRunForm_CloseOtherTabs;

            // this method is also used during layout loading - Show is handled by DockPanel, not here
            if (show)
            {
                ShowChildForm(childForm);
            }

            return childForm;
        }

        private void ShowChildForm(DockContent childForm)
        {
            if (childForm == null)
            {
                return;
            }
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ShowChildForm(childForm); }));
                return;
            }

            if (DockPanel.DocumentStyle == DocumentStyle.SystemMdi)
            {
                childForm.MdiParent = this;
                childForm.Show();
            }
            else
            {
            lock (DockPanelLock)
                {
                    childForm.Show(DockPanel);
                }
            }
        }
        
        void TestRunForm_CloseOtherTabs(object sender, EventArgs e)
        {
            foreach (var testRunForm in GetFormsByType<TestRunTreeForm>())
            {
                if (object.ReferenceEquals(sender, testRunForm) == false)
                {
                    testRunForm.Close();
                }
            }
        }

        //handler for Test Run form step selection change event. If parameter editor is opened, updates its step list
        void TestRunForm_SelectionChanged (object sender, TestStepEventArgs e)
        {
            SetParameterTreeFormSteps(m_ParameterTreeForm, e.Steps, e.TestRun, (e.Sender != null) ? e.Sender.ExecutionStatus : new TestExecutionStatus(), e.Sender.CommandStack);
        }

        void TestRunForm_OnFileDrop (object sender, FileDropEventArgs e)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { TestRunForm_OnFileDrop(sender, e); }));
                return;
            }

            if (e.Form == null)
            {
                CrashLog.Write("Test run file drop: Form is null");
                return;
            }

            if (e.FileNames.Length == 1)
            {
                // try loading into the form that received the drop, not opening in a new form
                try
                {
                    var loader = new TestRunLoadHelper();
                    loader.AfterLoad += TestRun_AfterLoadFromFile;
                    ITestRun loadedTestRun = loader.LoadTestRun(e.FileNames[0]);
                    e.Form.SetTestRun(loadedTestRun, e.FileNames[0], (loadedTestRun is SavedCompositeTestStep) == false);
                    return;
                }
                catch (Exception ex)
                {
                }
            }

            // error happened - could not load a file. Let's try a normal "Open"
            LoadFiles(e.FileNames);
        }

        void TestRunForm_ReloadFromDiskClicked(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { TestRunForm_ReloadFromDiskClicked(sender, e); }));
                return;
            }

            TestRunTreeForm form = sender as TestRunTreeForm;
            if (form == null)
            {
                return;
            }

            string fileName = form.FileName;
            if (string.IsNullOrEmpty(fileName))
            {
                return;
            }

            ITestRun loadedTestRun = null;
            try
            {
                var loader = new TestRunLoadHelper();
                loader.AfterLoad += TestRun_AfterLoadFromFile;
                loadedTestRun = loader.LoadTestRun(fileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (loadedTestRun == null)
            {
                return;
            }

            form.SetTestRun(loadedTestRun, fileName, (loadedTestRun is SavedCompositeTestStep) == false);
        }

        private string[] GetOpenFileNamesFromDialog()
        {
            if (Settings.Default.TestRunLRU != null)
            {
                bool found = false;
                foreach (var fileName in Settings.Default.TestRunLRU)
                {
                    if (Directory.Exists(Path.GetDirectoryName(fileName)))
                    {
                        openFileDialog.InitialDirectory = Path.GetDirectoryName(fileName);
                    }

                    if (File.Exists(fileName)) // the file still exists
                    {
                        openFileDialog.FileName = Path.GetFileName(fileName);
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    openFileDialog.FileName = "";
                }
            }
            if (openFileDialog.ShowDialog() != DialogResult.OK)
            {
                return null;
            }
            return openFileDialog.FileNames;
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            LoadFiles(GetOpenFileNamesFromDialog());
        }

        internal void LoadFiles(params string[] fileNames)
        {
            if (fileNames.IsNullOrEmpty())
            {
                return;
            }
            foreach (var fileName in fileNames)
            {
                if (Path.GetFileName(fileName).ToLower().Contains(".ylog"))
                {
                    LoadExportedLogFile(fileName);
                }
                else
                {
                    LoadTestRunFile(fileName);
                }
            }
        }

        private async void LoadTestRunFile(string fileName)
        {
            await Task.WhenAll(Program.InitParameterTypesTask, Program.InitTestRepositoryManagerTask, InitGlobalParametersTask);
                    
            // load, process 
            ITestRun loadedTestRun = null;
            try
            {
                var loader = new TestRunLoadHelper();
                loader.AfterLoad += TestRun_AfterLoadFromFile;
                loadedTestRun = loader.LoadTestRun(fileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (loadedTestRun == null)
            {
                return;
            }
            //create test run form
            ShowTestRunForm(loadedTestRun, fileName);
            AddToTestRunLRU(fileName);
        }

        private async void LoadExportedLogFile(string fileName)
        {
            await Task.WhenAll(Program.InitParameterTypesTask, Program.InitTestRepositoryManagerTask, InitGlobalParametersTask);

            TestRunLog testRunLog;
            try
            {
                testRunLog = new XmlLogImporter(fileName).Import();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Import failed: " + ex.Message);
                return;
            }

            var cachedLog = new CachedLog(testRunLog);
            cachedLog.MessageChanged += (sender, e) => { new XmlLogExporter(fileName).Export(testRunLog); }; // save back to XML when notes are added etc.

            TestRunAnalysisForm childForm = new TestRunAnalysisForm(
                    string.Format("Test run {0:g} {1}", testRunLog.StartTime, testRunLog.Name),
                    cachedLog,
                    true,
                    LoggerFilterSingleton.Instance, new DummyTestRunningStatus());

            childForm.RepeatPressed += delegate(object s, EventArgs evt)
            {
                try
                {
                    if (childForm.DataSource == null || childForm.DataSource.Settings == null)
                    {
                        return;
                    }
                    var loader = new TestRunLoadHelper();
                    loader.AfterLoad += TestRun_AfterLoadFromFile;
                    ITestRun loadedTestRun = loader.LoadTestRun(childForm.DataSource.Settings);
                    ShowTestRunForm(loadedTestRun, null);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            };

            ShowChildForm(childForm);
        }

        private void AddToTestRunLRU(string fileName)
        {
            var lru = LRUList<string>.FromStringCollection(Settings.Default.TestRunLRU, Settings.Default.TestRunLRUSize);
            lru.Add(fileName);
            Settings.Default.TestRunLRU = LRUList<string>.ToStringCollection(lru);
            Settings.Default.Save();
            SaveFormLayout();
        }

        void TestRunForm_SavedToFile (object sender, TestRunFileEventArgs e)
        {
            AddToTestRunLRU(e.FileName);
            if (e.TestRun is SavedCompositeTestStep)
            {
                if (yats.TestRepositoryManager.YatsComposite.Properties.Settings.Default.TestSearchPaths == null)
                {
                    yats.TestRepositoryManager.YatsComposite.Properties.Settings.Default.TestSearchPaths = new System.Collections.Specialized.StringCollection();
                }
                if (yats.TestRepositoryManager.YatsComposite.Properties.Settings.Default.TestSearchPaths.Contains(Path.GetDirectoryName(e.FileName)) == false){
                    // the test case was exported to a directory that is not configured for test case storage. Add silently
                    yats.TestRepositoryManager.YatsComposite.Properties.Settings.Default.TestSearchPaths.Add(Path.GetDirectoryName(e.FileName));
                    yats.TestRepositoryManager.YatsComposite.Properties.Settings.Default.Save();
                }
                // update test case tree to show the new test case
                TestCaseRepository.Instance.Managers.Initialize(true);
            }
        }

        private void HandleDroppedFile(string fileName)
        {
            ITestRun loadedTestRun = null;
            try
            {
                var loader = new TestRunLoadHelper();
                loader.AfterLoad += TestRun_AfterLoadFromFile;
                loadedTestRun = loader.LoadTestRun(fileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (loadedTestRun == null)
            {
                return;
            }

            //create test run form
            ShowTestRunForm(loadedTestRun, fileName);
        }

        #endregion

        #region File drag and drop support
        private void MainForm_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] fileNames = (string[])e.Data.GetData(DataFormats.FileDrop);
                LoadFiles(fileNames);
            }
        }

        private void MainForm_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                //allow dropping an XML file to load it
                e.Effect = DragDropEffects.Move;
                return;
            }
            e.Effect = DragDropEffects.None;
        }
        #endregion

        #region View Log buttons


        private void openLogDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string logDirectory = yats.Logging.File.Properties.Settings.Default.FileLogPath;

            logDirectory = yats.Utilities.PlatformHelper.ExpandEnvironmentVariables(logDirectory);
            
            string command = "/n,/e,/root,\"" +
                       logDirectory +
                       "\"";
            System.Diagnostics.Process.Start("explorer", command);
        }

        private void HideLogButtons()
        {
            if (this.InvokeRequired)
            {
                Invoke(new MethodInvoker(delegate() { HideLogButtons(); }));
                return;
            }
            //even if the file logging is disabled, allow browsing results
            openLogDirectoryMenuItem.Visible = yats.Logging.File.Properties.Settings.Default.FileLogEnabled && Directory.Exists(yats.Logging.File.FileAppenderHandler.Directory);
            viewDatabaseMenuItem.Visible = yats.Logging.SQLite.Properties.Settings.Default.SQLiteLoggingEnabled && LoggingInit.IsDatabaseInitializedSuccessfully;
            if (yats.Logging.SQLite.Properties.Settings.Default.SQLiteLoggingEnabled && yats.Logging.SQLite.Properties.Settings.Default.AsynchronousLogging)
            {
                btnDbWrites.Visible = true;
                btnDbWrites.Image = dbLoadImageList.Images[0]; // TODO is the icon ever updated?
                //LoggingInit.GetDatabaseAppender().OnQueueSizeChange += new QueueSizeChangeDelegate(MainForm_OnQueueSizeChange);
            }
            else
            {
                btnDbWrites.Visible = false;
            }
            yats.Logging.SQLite.Properties.Settings.Default.PropertyChanged += (sender, e) => {HideLogButtons();}; // apply main form view on settings change
        }
        
        void MainForm_OnQueueSizeChange(int pendingWrites)
        {
            try
            {
                if (this.InvokeRequired)
                {
                    Invoke(new MethodInvoker(delegate() { MainForm_OnQueueSizeChange(pendingWrites); }));
                    return;
                }
                int imageIndex = MathUtil.Constrain((int)Math.Floor(Math.Log10(pendingWrites)) - 1, 0, dbLoadImageList.Images.Count - 1);
                btnDbWrites.Image = dbLoadImageList.Images[imageIndex];
            }
            catch { }
        }

        #endregion

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (Settings.Default.SaveFormLayout)
                {
                    if (Settings.Default.MainFormLocationSaved)
                    {
                        // If the screen resolution is changed between program runs, do NOT restore the form position.
                        var lastRunDesktop = Settings.Default.PreviousDisplayResolution;
                        if (lastRunDesktop.IsEmpty || lastRunDesktop.Equals(SystemInformation.VirtualScreen.Size))
                        {
                            //http://blogs.msdn.com/b/rprabhu/archive/2005/11/28/497792.aspx
                            this.Size = Settings.Default.MainFormSize;
                            this.Location = Settings.Default.MainFormLocation;
                            this.WindowState = Settings.Default.MainFormState;
                        }
                    }
                    try
                    {
                        if (File.Exists(DOCKPANEL_CONFIG_FILE))
                        {
                            DockPanel.LoadFromXml(DOCKPANEL_CONFIG_FILE, m_deserializeDockContent);
                        }
                    }
                    catch
                    {
                    }
                    // enable layout auto-save
                    DockPanel.ContentAdded += DockPanel_ContentAdded;
                    DockPanel.ContentRemoved += DockPanel_ContentAdded;
                }

                // If no test runs are reopened automaticallly - open a new test run form. Unless disabled in settings
                if (GetFormsByType<TestRunTreeForm>().Count == 0 && Settings.Default.OpenNewTest)
                {
                    newTestRunBtn_Click(sender, e);
                }

                TestRunThread.BeforeRun += (ITestRunner testRunner)=>{
                    if (FindNullParametersVisitor.Validate(testRunner.TestRun.TestSequence, testRunner.GetStepsToRun(), testRunner.TestRun.GlobalParameters, TestCaseRepository.Instance.Managers) == false)
                    {
                        MessageBox.Show("Some parameters are not set, can not continue", "Error");
                        return false;
                    }
                    return true;
                };

                TestRunThread.BeforeRun += (ITestRunner testRunner) =>
                {
                    try
                    {
                        bool modified;
                        StateMachineSanityCheckVisitor.CheckStepMachines(testRunner.TestRun.TestSequence, out modified);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                };

                LoggerFilterSingleton.Instance.Deserialize(Settings.Default.LogFilter);
                HideLogButtons();
                yats.Logging.SQLite.Properties.Settings.Default.PropertyChanged += (s, ev) => { HideLogButtons(); };
                AppenderFactory.OnInitialized += (s, ev) => { HideLogButtons(); };
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex, "MainForm_Load");
            }            
        }

        private void UpdateTitleWithAssemblyVersion()
        {
            System.Reflection.Assembly thisAssembly = this.GetType().Assembly;
            var fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(thisAssembly.Location);
            this.Text = string.Format("{0} v{1}", fvi.FileDescription, fvi.FileVersion);
#if DEBUG
            this.Text += " DEBUG";
#endif
        }

        private async void globalParametersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            await InitGlobalParametersTask;            

            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { globalParametersToolStripMenuItem_Click(sender, e); }));
                return;
            }

            lock (DockPanelLock)
            {
                GetGlobalParameterEditorForm().Show(DockPanel);
            }
        }

        private GlobalParameterEditorForm GetGlobalParameterEditorForm()
        {
            if (m_GlobalParameterEditorForm == null || m_GlobalParameterEditorForm.IsDisposed)
            {
                m_GlobalParameterEditorForm = new GlobalParameterEditorForm(GlobalParameterSingleton.Instance.GlobalParameters);
                m_GlobalParameterEditorForm.DockPanel = DockPanel;
                m_GlobalParameterEditorForm.DockState = WeifenLuo.WinFormsUI.Docking.DockState.DockLeftAutoHide;
            }

            return m_GlobalParameterEditorForm;
        }

        private async void testCasesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            await Program.InitTestRepositoryManagerTask;

            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { testCasesToolStripMenuItem_Click(sender, e); }));
                return;
            }
            lock (DockPanelLock)
            {
                GetTestCaseTreeForm().Show(DockPanel);
            }
        }

        private TestCaseTreeForm GetTestCaseTreeForm()
        {
            if (m_TestCaseTreeForm == null || m_TestCaseTreeForm.IsDisposed)
            {
                m_TestCaseTreeForm = new TestCaseTreeForm(TestCaseRepository.Instance.Managers);
                m_TestCaseTreeForm.DockPanel = DockPanel;
                m_TestCaseTreeForm.DockState = WeifenLuo.WinFormsUI.Docking.DockState.DockLeft;
                m_TestCaseTreeForm.OnOptionsClicked += OptionsMenuItem_Click;
                m_TestCaseTreeForm.TestCaseEditHandler = new TestCaseEditHandler(this);
                m_TestCaseTreeForm.OnTestDoubleClick += TestCaseTree_OnTestDoubleClick;
            }
            return m_TestCaseTreeForm;
        }

        private void TestCaseTree_OnTestDoubleClick(object sender, TestCaseInfoEventArgs e)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate () { TestCaseTree_OnTestDoubleClick(sender, e); }));
                return;
            }
            lock (DockPanelLock)
            {
                TestRunTreeForm testRunTreeForm = DockPanel.ActiveDocument as TestRunTreeForm;
                if (testRunTreeForm == null)
                {
                    return;
                }

                testRunTreeForm.AddTestCases(e.TestCaseInfo);
            }
        }

        private class TestCaseEditHandler : ITestCaseEditHandler
        {
            MainForm mainForm;
            public TestCaseEditHandler(MainForm mainForm)
            {
                this.mainForm = mainForm;
            }

            bool ITestCaseEditHandler.CanEdit(ITestCaseInfo testCaseInfo)
            {
                return testCaseInfo is CompositeTestCase;
            }

            void ITestCaseEditHandler.HandleEditRequest(ITestCaseInfo testCaseInfo)
            {
                mainForm.LoadTestRunFile((testCaseInfo as CompositeTestCase).FileName);
            }
        }

        private LogFilterEditor GetLogFilterEditor()
        {
            if (m_LogFilterEditor == null || m_LogFilterEditor.IsDisposed)
            {
                m_LogFilterEditor = new LogFilterEditor();
                m_LogFilterEditor.DockPanel = DockPanel;
                m_LogFilterEditor.DockState = WeifenLuo.WinFormsUI.Docking.DockState.DockLeftAutoHide;
            }
            return m_LogFilterEditor;
        }


        private void logViewBtn_Click(object sender, EventArgs e)
        {
        lock (DockPanelLock)
            {
                GetLogViewForm().Show(DockPanel);
            }
        }

        private LogViewForm GetLogViewForm()
        {
            if (m_LogViewForm == null || m_LogViewForm.IsDisposed)
            {
                m_LogViewForm = new LogViewForm();
                m_LogViewForm.DockPanel = DockPanel;
                m_LogViewForm.DockState = WeifenLuo.WinFormsUI.Docking.DockState.DockBottom;
            }
            return m_LogViewForm;
        }

        private void filterLogViewMenu_Click(object sender, EventArgs e)
        {
            if (m_FilteredLogForm == null || m_FilteredLogForm.IsDisposed)
            {
                m_FilteredLogForm = new FilteredLogForm();
                m_FilteredLogForm.DockPanel = DockPanel;
                m_FilteredLogForm.DockState = WeifenLuo.WinFormsUI.Docking.DockState.DockBottom;
            }
            lock (DockPanelLock)
            {
                m_FilteredLogForm.Show(DockPanel);
            }
        }

        private FilteredLogForm GetFilteredLogForm()
        {
            if (m_FilteredLogForm == null || m_FilteredLogForm.IsDisposed)
            {
                m_FilteredLogForm = new FilteredLogForm();
                m_FilteredLogForm.DockPanel = DockPanel;
                m_FilteredLogForm.DockState = WeifenLuo.WinFormsUI.Docking.DockState.DockBottom;
            }
            return m_FilteredLogForm;
        }

        private void viewDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
            lock (DockPanelLock)
                {
                    GetTestRunListForm().Show(DockPanel);
                }
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex, "GetTestRunListForm.Show()");
            }
        }

        private TestRunListForm GetTestRunListForm()
        {
            Task.WaitAll(Program.InitLoggingTask);
            if (AppenderFactory.StorageInstance == null)
            {
                throw new NullReferenceException("AppenderFactory.StorageInstance is null");
            }
            if (m_TestRunListForm == null || m_TestRunListForm.IsDisposed)
            {
                m_TestRunListForm = new TestRunListForm(AppenderFactory.StorageInstance);
                m_TestRunListForm.DockPanel = DockPanel;
                m_TestRunListForm.DockState = WeifenLuo.WinFormsUI.Docking.DockState.DockLeft;
                m_TestRunListForm.OnTestRunView += TestRunListForm_OnTestRunView;
                m_TestRunListForm.OnRepeatTestRun += TestRunListForm_OnRepeatTestRun;
                AppenderFactory.StorageInstance.LogRemoved += StorageInstance_LogRemoved;
                //m_TestRunListForm.OnTestRunDeleted += new TestRunListForm.TestRunDeletedDelegate(TestRunListForm_OnTestRunDeleted);
            }
            return m_TestRunListForm;
        }

        // When a test run log is deleted from database and is still opened in a form - close the form
        void StorageInstance_LogRemoved(object sender, TestRunLogInfoEventArgs e)
        {
            foreach (var analysisForm in GetFormsByType<TestRunAnalysisForm>())
            {
                //TODO
                //IDataSourceWithID dataSource = analysisForm.DataSource as IDataSourceWithID;
                //if (dataSource != null && dataSource.TestRunDbId == testRunDbId)
                //{
                //    analysisForm.Close();
                //}
            }
        }

        async void TestRunListForm_OnRepeatTestRun(object sender, TestRunLogEventArgs e)
        {
            try
            {
                if (e.TestRunLog.Settings == null)
                {
                    MessageBox.Show("Test run settings not available");
                    return;
                }
                
                await Task.WhenAll(Program.InitParameterTypesTask, Program.InitTestRepositoryManagerTask, InitGlobalParametersTask);

                ITestRun loadedTestRun = null;
                try
                {
                    var loader = new TestRunLoadHelper();
                    loader.AfterLoad += TestRun_AfterLoadFromFile;
                    loadedTestRun = loader.LoadTestRun(e.TestRunLog.Settings);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                if (loadedTestRun != null)
                {
                    TestRunTreeForm form = ShowTestRunForm(loadedTestRun, null);
                    if (form == null || form.Results == null)
                    {
                        return;
                    }
                    foreach (var stepEntry in e.TestRunLog.Steps)
                    {
                        if (stepEntry.RowType != StepInfoTypeEnum.STEP_RESULT)
                        {
                            continue;
                        }
                        ResultEnum stepResult = (ResultEnum)Enum.Parse(typeof(ResultEnum), stepEntry.EvaluatedResult);
                        var step = FindTestStepByPath(loadedTestRun, stepEntry.Path);
                        form.Results.AddResult(step, stepResult);
                    }                    
                }
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex);
            }
        }

        private static TestStep FindTestStepByPath(ITestRun testRun, StepPath path)
        {
            try
            {
                if (testRun == null || path == null || testRun.TestSequence == null)
                {
                    return null;
                }
                TestStep result = testRun.TestSequence;

                for (int i = 1; i < path.Segments.Length; i++)
                {
                    result = (result as TestStepComposite).Steps[path.Segments[i].Index];
                }
                return result;
            }
            catch { }
            return null;
        }

        void TestRunListForm_OnTestRunView(object sender, TestRunLogEventArgs e)
        {
            var cachedLog = new CachedLog(e.TestRunLog);
            cachedLog.MessageChanged += (s, _e) => { e.TestRunLog.Storage.Update(_e.Message); };//note added, edited etc

            var childForm = new TestRunAnalysisForm(
                (e.TestRunLog.StartTime.Date == DateTime.Now.Date) ? string.Format("{0} {1:HH:mm}", e.TestRunLog.Name, e.TestRunLog.StartTime) : string.Format("{0} {1:g}", e.TestRunLog.Name, e.TestRunLog.StartTime),
                cachedLog,
                true,
                LoggerFilterSingleton.Instance,
                new DummyTestRunningStatus()
            );

            childForm.CloseAllResultsPressed += TestRunAnalysisForm_CloseAllResults;
            ShowChildForm(childForm);
        }

        internal void TestRunAnalysisForm_OnLogLevelFilterMenu(object sender, EventArgs e)
        {
            try
            {
            lock (DockPanelLock)
                {
                    GetLogFilterEditor().Show(DockPanel);
                }
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex, "GetTestRunListForm.Show()");
            }
            var childForm = new LogFilterEditor();            
        }
                
        /// <summary>
        /// Handles "Close all results" tab context menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void TestRunAnalysisForm_CloseAllResults(object sender, EventArgs e)
        {
            foreach (var form in GetFormsByType<TestRunAnalysisForm>())
            {
                form.Close();
            }
        }

        private ParameterTreeForm GetParameterTreeForm()
        {
            if (m_ParameterTreeForm == null || m_ParameterTreeForm.IsDisposed)
            {
                m_ParameterTreeForm = new ParameterTreeForm();
                m_ParameterTreeForm.ParameterProperties = new ParameterProperties();
                LoadGlobalParamsAsync();
                m_ParameterTreeForm.DockPanel = DockPanel;
                m_ParameterTreeForm.DockState = WeifenLuo.WinFormsUI.Docking.DockState.DockBottom;
                m_ParameterTreeForm.ValueChanged += m_ParameterTreeForm_ValueChanged;
            }
            return m_ParameterTreeForm;
        }
        
        async void LoadGlobalParamsAsync()
        {
            // This method runs asynchronously.
            await Task.Run(() => GlobalParameterSingleton.Instance.GlobalParameters.ValueUpdated += (sender, e) => { m_ParameterTreeForm.Invalidate(true); });
        }

        // Event handler for Global parameter removed.
        // Check all parameters in all open test runs. If any of them is using the deleted global value, set it to null.
        internal void GlobalParameterRemoved (object sender, GlobalParameterEventArgs e)
        {
            bool changed = false;
            ParameterTreeForm paramForm = GetFormsByType<ParameterTreeForm>().FirstOrDefault();
            foreach (var testRunForm in GetFormsByType<TestRunTreeForm>())
            {
                ICommand cleanupCommand = RemoveNonExistingGlobals.ValidateWithUndo(testRunForm.TestRun.TestSequence, testRunForm.TestRun.GlobalParameters, e.Id, e.Value, e.Manager, (bool execute) => { if (paramForm != null) { paramForm.Refresh(); } });
                
                if (cleanupCommand != null)
                {
                    testRunForm.CommandStack.Execute(cleanupCommand);
                    changed = true;
                }                
            }

            // refresh parameter editor
            if (changed && m_ParameterTreeForm != null && m_ParameterTreeForm.IsDisposed == false)
            {
                m_ParameterTreeForm.Invalidate(true);
            }
        }

        internal List<T> GetFormsByType<T>() where T : Form
        {
            List<T> result = new List<T>();

            if (DockPanel.DocumentStyle == DocumentStyle.SystemMdi)
            {
                foreach (Form frm in MdiChildren)
                {
                    T form = frm as T;
                    result.AddIfNotNull(form);
                }
            }
            else
            {
                for (int index = DockPanel.Contents.Count - 1; index >= 0; index--)
                {
                    T form = DockPanel.Contents[index] as T;
                    result.AddIfNotNull(form);
                }
            }
            return result;
        }

        void m_ParameterTreeForm_ValueChanged(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { m_ParameterTreeForm_ValueChanged(sender, e); }));
                return;
            }

            ParameterTreeForm form = sender as ParameterTreeForm;
            if (form == null)
            {
                return;
            }

            //disable parameter editors
            foreach (var testRunForm in GetFormsByType<TestRunTreeForm>())
            {
                if (object.ReferenceEquals(form.EditedObject, testRunForm.TestRun) == false)
                {
                    continue;
                }
                testRunForm.TestRun.IsModified = true;
            }
        }

        private void CloseAllDocuments()
        {
            if (DockPanel.DocumentStyle == DocumentStyle.SystemMdi)
            {
                foreach (Form form in MdiChildren)
                    form.Close();
            }
            else
            {
            lock (DockPanelLock)
                {
                    for (int index = DockPanel.Contents.Count - 1; index >= 0; index--)
                    {
                        if (DockPanel.Contents[index] is IDockContent)
                        {
                            IDockContent content = (IDockContent)DockPanel.Contents[index];
                            content.DockHandler.Close();
                        }
                    }
                }
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*if(DockPanel.ActiveAutoHideContent != null && DockPanel.ActiveAutoHideContent.DockHandler != null)
            {
                DockPanel.ActiveAutoHideContent.DockHandler.Close( );
            }
            else */
            if (DockPanel.ActiveContent != null && DockPanel.ActiveContent.DockHandler != null)
            {
                DockPanel.ActiveContent.DockHandler.Close();
            }
        }

        private void DockPanel_ActiveDocumentChanged(object sender, EventArgs e)
        {
            var testRunForm = DockPanel.ActiveDocument as TestRunTreeForm;
            if (testRunForm == null || m_ParameterTreeForm == null)
            {
                return;
            }
           
            if (object.ReferenceEquals(m_ParameterTreeForm.EditedObject, testRunForm.TestRun))
            {
                // already focused on this test run
                return;
            }

            SetParameterTreeFormSteps(m_ParameterTreeForm, testRunForm.SelectedSteps, testRunForm.TestRun, testRunForm.ExecutionStatus, testRunForm.CommandStack);
        }

        private void SetParameterTreeFormSteps(ParameterTreeForm form, IList<TestStep> steps, ITestRun testRun, ITestExecutionStatus executionStatus, ICommandStack commandStack)
        {
            if (form == null || form.IsDisposed)
            {
                return;
            }
            if (steps == null || steps.Count == 0)
            {
                steps = new List<TestStep>();
                if (testRun != null)
                {
                    steps.Add(testRun.TestSequence);
                }
            }
            else if (steps.Count > 1) // do not show parent ant child steps together 
            {
                steps = TestStepHierarchyFilter.Filter(steps);
            }
            form.SetSteps(testRun, steps, (testRun != null) ? testRun.TestSequence : null, (testRun != null) ? testRun.GlobalParameters : null, executionStatus, commandStack);
        }

        private void fullScreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (sender as ToolStripMenuItem).Checked = Gui.Winforms.Components.Util.FormUtil.ToggleFullScreen(this);
        }

        private void alwaysOnTopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (sender as ToolStripMenuItem).Checked = Gui.Winforms.Components.Util.FormUtil.ToggleOnTop(this);
        }

        protected override void OnMdiChildActivate(EventArgs e)
        {
            base.OnMdiChildActivate(e); //REQUIRED
            HandleChildMerge(); //Handle merging
        }

        private void HandleChildMerge()
        {
            ToolStripManager.RevertMerge(this.mainToolStrip);
            IMergeableToolStripForm ChildForm = ActiveMdiChild as IMergeableToolStripForm;
            if (ChildForm != null)
            {
                ToolStripManager.Merge(ChildForm.ChildToolStrip, mainToolStrip);
            }
        }

        private void FillRecentFileDropDown(ToolStripItemCollection dropdown)
        {
            try
            {
                dropdown.Clear();
                if (Settings.Default.TestRunLRU == null)
                {
                    return;
                }
                int index = 1;
                foreach (var fileName in Settings.Default.TestRunLRU)
                {
                    if (File.Exists(fileName) == false)
                    {
                        continue;
                    }
                    var buttonDropDownItem = dropdown.Add(string.Format("&{0} {1}", index, Path.GetFileNameWithoutExtension(fileName)));
                    buttonDropDownItem.Tag = fileName;
                    buttonDropDownItem.Click += delegate(object sender2, EventArgs e2)
                    {
                        LoadTestRunFile((sender2 as ToolStripItem).Tag as string);
                    };
                    index++;
                }
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex);
            }
        }

        private void openToolStripButton_DropDownOpening(object sender, EventArgs e)
        {
            FillRecentFileDropDown(openToolStripButton.DropDown.Items);
        }

        private void fileMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            FillRecentFileDropDown(recentTestsToolStripMenuItem.DropDownItems);
            recentTestsToolStripMenuItem.Visible = (recentTestsToolStripMenuItem.DropDownItems.Count > 0);
        }

        private void openApplicationDataDirectoryMenuItem_Click(object sender, EventArgs e)
        {
            PlatformHelper.OpenFileBrowser(CrashLog.GetLogFileName());
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            UpdateTitleWithAssemblyVersion();
            
            testCasesToolStripMenuItem_Click(null, null);
            //{
            //    ThreadStart threadDelegate = new ThreadStart(delegate() { testCasesToolStripMenuItem_Click(null, null); });
            //    Thread newThread = new Thread(threadDelegate);
            //    newThread.Name = "testCasesToolStrip";
            //    newThread.Priority = ThreadPriority.BelowNormal;
            //    newThread.Start();
            //}

            //TODO refactor with async/await
            {
                ThreadStart threadDelegate = new ThreadStart(delegate() { testParametersToolStripMenuItem_Click(null, null); });
                Thread newThread = new Thread(threadDelegate);
                newThread.Name = "testParametersToolStripMenuItem_Click";
                newThread.Priority = ThreadPriority.BelowNormal;
                newThread.Start();
            }

            globalParametersToolStripMenuItem_Click(null, null); 

            //{
            //    ThreadStart threadDelegate = new ThreadStart(delegate() { globalParametersToolStripMenuItem_Click(null, null); });
            //    Thread newThread = new Thread(threadDelegate);
            //    newThread.Name = "globalParametersToolStrip";
            //    newThread.Priority = ThreadPriority.BelowNormal;
            //    newThread.Start();
            //}

            {
                // 	547: Speed up Open operation - cache serializers
                ThreadStart threadDelegate = new ThreadStart(delegate() { SerializationHelper.GetSerializer(typeof(ITestRun)); });
                Thread newThread = new Thread(threadDelegate);
                newThread.Name = "ITestRun serializer";
                newThread.Priority = ThreadPriority.BelowNormal;
                newThread.Start();
            }

            GlobalParameterSingleton.Instance.GlobalParameters.ValueRemoved += GlobalParameterRemoved;
        }

        private IDockContent GetContentFromPersistString(string persistString)
        {
            try
            {
                if (persistString == typeof(TestCaseTreeForm).ToString())
                {
                    return GetTestCaseTreeForm();
                }
                if (persistString == typeof(GlobalParameterEditorForm).ToString())
                {
                    return GetGlobalParameterEditorForm();
                }
                if (persistString == typeof(LogViewForm).ToString())
                {
                    return GetLogViewForm();
                }
                if (persistString == typeof(FilteredLogForm).ToString())
                {
                    return GetFilteredLogForm();
                }
                if (persistString == typeof(TestRunListForm).ToString())
                {
                    return GetTestRunListForm();
                }
                if (persistString == typeof(ParameterTreeForm).ToString())
                {
                    return GetParameterTreeForm();
                }
                if (persistString == typeof(LogFilterEditor).ToString())
                {
                    return GetLogFilterEditor();
                }

                string[] parsedStrings = persistString.Split(new char[] { ',' }, 2, StringSplitOptions.RemoveEmptyEntries);

                if (parsedStrings.Length < 1)
                {
                    return null;
                }

                if (parsedStrings[0] == typeof(TestRunTreeForm).ToString() && Settings.Default.RememberTestRuns && m_appStartedWithTestRunParameter == false)
                {
                    if (parsedStrings.Length != 2)
                    {
                        return CreateNewTestRunForm(false);
                    }

                    string fileName = parsedStrings[1];
                    if (File.Exists(fileName) == false)
                    {
                        return null;
                    }

                    ITestRun loadedTestRun = null;
                    try
                    {
                        var loader = new TestRunLoadHelper();
                        loader.AfterLoad += TestRun_AfterLoadFromFile;
                        loadedTestRun = loader.LoadTestRun(fileName);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    if (loadedTestRun != null)
                    {
                        return ShowTestRunForm(loadedTestRun, fileName, false);
                    }
                }
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex);
            }
            return null;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string versionText = string.Format("v{0}", AssemblyHelper.GetAssemblyVersion(this));

            MessageBox.Show("Yet Another Test System\r\nwww.veiverys.com\r\n" + versionText, "About");
        }

    }
}
