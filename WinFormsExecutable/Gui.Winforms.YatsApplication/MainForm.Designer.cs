﻿namespace yats.Gui.Winforms.YatsApplication
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            //WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
            WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin1 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient1 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient2 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient2 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient3 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient1 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient4 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient5 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient3 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient6 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient7 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.openToolStripButton = new System.Windows.Forms.ToolStripSplitButton();
            this.newTestRunBtn = new System.Windows.Forms.ToolStripButton();
            this.btnDbWrites = new System.Windows.Forms.ToolStripButton();
            this.openTextViewMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filterLogViewMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewDatabaseMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLogDirectoryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openTestSequenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recentTestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.testCasesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testParametersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.globalParametersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.logLevelFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.alwaysOnTopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fullScreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openApplicationDataDirectoryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.DockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.dbLoadImageList = new System.Windows.Forms.ImageList(this.components);
            this.closeAllResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainToolStrip.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size(880, 551);
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripButton,
            this.newTestRunBtn,
            this.btnDbWrites});
            this.mainToolStrip.Location = new System.Drawing.Point(0, 24);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Size = new System.Drawing.Size(880, 25);
            this.mainToolStrip.TabIndex = 0;
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(32, 22);
            this.openToolStripButton.Text = "Open";
            this.openToolStripButton.ButtonClick += new System.EventHandler(this.openButton_Click);
            this.openToolStripButton.DropDownOpening += new System.EventHandler(this.openToolStripButton_DropDownOpening);
            // 
            // newTestRunBtn
            // 
            this.newTestRunBtn.Image = ((System.Drawing.Image)(resources.GetObject("newTestRunBtn.Image")));
            this.newTestRunBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newTestRunBtn.Name = "newTestRunBtn";
            this.newTestRunBtn.Size = new System.Drawing.Size(73, 22);
            this.newTestRunBtn.Text = "New test";
            this.newTestRunBtn.Click += new System.EventHandler(this.newTestRunBtn_Click);
            // 
            // btnDbWrites
            // 
            this.btnDbWrites.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDbWrites.Image = ((System.Drawing.Image)(resources.GetObject("btnDbWrites.Image")));
            this.btnDbWrites.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDbWrites.Name = "btnDbWrites";
            this.btnDbWrites.Size = new System.Drawing.Size(23, 22);
            this.btnDbWrites.Text = "Pending log writes";
            // 
            // openTextViewMenuItem
            // 
            this.openTextViewMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openTextViewMenuItem.Image")));
            this.openTextViewMenuItem.Name = "openTextViewMenuItem";
            this.openTextViewMenuItem.Size = new System.Drawing.Size(173, 22);
            this.openTextViewMenuItem.Text = "Open text view";
            this.openTextViewMenuItem.Click += new System.EventHandler(this.logViewBtn_Click);
            // 
            // filterLogViewMenuItem
            // 
            this.filterLogViewMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("filterLogViewMenuItem.Image")));
            this.filterLogViewMenuItem.Name = "filterLogViewMenuItem";
            this.filterLogViewMenuItem.Size = new System.Drawing.Size(173, 22);
            this.filterLogViewMenuItem.Text = "Filtered text log";
            this.filterLogViewMenuItem.Click += new System.EventHandler(this.filterLogViewMenu_Click);
            // 
            // viewDatabaseMenuItem
            // 
            this.viewDatabaseMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("viewDatabaseMenuItem.Image")));
            this.viewDatabaseMenuItem.Name = "viewDatabaseMenuItem";
            this.viewDatabaseMenuItem.Size = new System.Drawing.Size(173, 22);
            this.viewDatabaseMenuItem.Text = "View database";
            this.viewDatabaseMenuItem.Click += new System.EventHandler(this.viewDatabaseToolStripMenuItem_Click);
            // 
            // openLogDirectoryMenuItem
            // 
            this.openLogDirectoryMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openLogDirectoryMenuItem.Image")));
            this.openLogDirectoryMenuItem.Name = "openLogDirectoryMenuItem";
            this.openLogDirectoryMenuItem.Size = new System.Drawing.Size(173, 22);
            this.openLogDirectoryMenuItem.Text = "Open log directory";
            this.openLogDirectoryMenuItem.Click += new System.EventHandler(this.openLogDirectoryToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolStripMenuItem2,
            this.toolsToolStripMenuItem,
            this.windowToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.MdiWindowListItem = this.windowToolStripMenuItem;
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(880, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newTestToolStripMenuItem,
            this.openTestSequenceToolStripMenuItem,
            this.recentTestsToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.exitToolStripMenuItem,
            this.closeAllResultsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            this.fileToolStripMenuItem.DropDownOpening += new System.EventHandler(this.fileMenuItem_DropDownOpening);
            // 
            // newTestToolStripMenuItem
            // 
            this.newTestToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newTestToolStripMenuItem.Image")));
            this.newTestToolStripMenuItem.Name = "newTestToolStripMenuItem";
            this.newTestToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newTestToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.newTestToolStripMenuItem.Text = "&New test";
            this.newTestToolStripMenuItem.Click += new System.EventHandler(this.newTestRunBtn_Click);
            // 
            // openTestSequenceToolStripMenuItem
            // 
            this.openTestSequenceToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openTestSequenceToolStripMenuItem.Image")));
            this.openTestSequenceToolStripMenuItem.Name = "openTestSequenceToolStripMenuItem";
            this.openTestSequenceToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openTestSequenceToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.openTestSequenceToolStripMenuItem.Text = "&Open";
            this.openTestSequenceToolStripMenuItem.ToolTipText = "Open test or log";
            this.openTestSequenceToolStripMenuItem.Click += new System.EventHandler(this.openButton_Click);
            // 
            // recentTestsToolStripMenuItem
            // 
            this.recentTestsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3});
            this.recentTestsToolStripMenuItem.Name = "recentTestsToolStripMenuItem";
            this.recentTestsToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.recentTestsToolStripMenuItem.Text = "R&ecent tests";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(77, 22);
            this.toolStripMenuItem3.Text = " ";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.closeToolStripMenuItem.Text = "&Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testCasesToolStripMenuItem,
            this.testParametersToolStripMenuItem,
            this.globalParametersToolStripMenuItem,
            this.toolStripSeparator2,
            this.logLevelFilterToolStripMenuItem,
            this.openTextViewMenuItem,
            this.filterLogViewMenuItem,
            this.openLogDirectoryMenuItem,
            this.viewDatabaseMenuItem,
            this.toolStripSeparator1,
            this.alwaysOnTopToolStripMenuItem,
            this.fullScreenToolStripMenuItem});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(44, 20);
            this.toolStripMenuItem2.Text = "&View";
            // 
            // testCasesToolStripMenuItem
            // 
            this.testCasesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("testCasesToolStripMenuItem.Image")));
            this.testCasesToolStripMenuItem.Name = "testCasesToolStripMenuItem";
            this.testCasesToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.testCasesToolStripMenuItem.Text = "Test cases";
            this.testCasesToolStripMenuItem.Click += new System.EventHandler(this.testCasesToolStripMenuItem_Click);
            // 
            // testParametersToolStripMenuItem
            // 
            this.testParametersToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("testParametersToolStripMenuItem.Image")));
            this.testParametersToolStripMenuItem.Name = "testParametersToolStripMenuItem";
            this.testParametersToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.testParametersToolStripMenuItem.Text = "Test parameters";
            this.testParametersToolStripMenuItem.Click += new System.EventHandler(this.testParametersToolStripMenuItem_Click);
            // 
            // globalParametersToolStripMenuItem
            // 
            this.globalParametersToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("globalParametersToolStripMenuItem.Image")));
            this.globalParametersToolStripMenuItem.Name = "globalParametersToolStripMenuItem";
            this.globalParametersToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.globalParametersToolStripMenuItem.Text = "Global parameters";
            this.globalParametersToolStripMenuItem.Click += new System.EventHandler(this.globalParametersToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(170, 6);
            // 
            // logLevelFilterToolStripMenuItem
            // 
            this.logLevelFilterToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("logLevelFilterToolStripMenuItem.Image")));
            this.logLevelFilterToolStripMenuItem.Name = "logLevelFilterToolStripMenuItem";
            this.logLevelFilterToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.logLevelFilterToolStripMenuItem.Text = "Log level filter";
            this.logLevelFilterToolStripMenuItem.Click += new System.EventHandler(this.TestRunAnalysisForm_OnLogLevelFilterMenu);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(170, 6);
            // 
            // alwaysOnTopToolStripMenuItem
            // 
            this.alwaysOnTopToolStripMenuItem.Name = "alwaysOnTopToolStripMenuItem";
            this.alwaysOnTopToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.alwaysOnTopToolStripMenuItem.Text = "Always on top";
            this.alwaysOnTopToolStripMenuItem.Click += new System.EventHandler(this.alwaysOnTopToolStripMenuItem_Click);
            // 
            // fullScreenToolStripMenuItem
            // 
            this.fullScreenToolStripMenuItem.Name = "fullScreenToolStripMenuItem";
            this.fullScreenToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F11;
            this.fullScreenToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.fullScreenToolStripMenuItem.Text = "Full screen";
            this.fullScreenToolStripMenuItem.Click += new System.EventHandler(this.fullScreenToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("optionsToolStripMenuItem.Image")));
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.optionsToolStripMenuItem.Text = "Options";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.OptionsMenuItem_Click);
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.windowToolStripMenuItem.Text = "&Window";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openApplicationDataDirectoryMenuItem,
            this.toolStripSeparator3,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // openApplicationDataDirectoryMenuItem
            // 
            this.openApplicationDataDirectoryMenuItem.Name = "openApplicationDataDirectoryMenuItem";
            this.openApplicationDataDirectoryMenuItem.Size = new System.Drawing.Size(241, 22);
            this.openApplicationDataDirectoryMenuItem.Text = "Open application data directory";
            this.openApplicationDataDirectoryMenuItem.Click += new System.EventHandler(this.openApplicationDataDirectoryMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(238, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "testRun.testrun";
            this.openFileDialog.Filter = "Test run or log (*.testrun;*.ylog;*.ylogz)|*.testrun;*.ylog;*.ylogz|Yats log file" +
    "s (*.ylog)|*.ylog|Test run files (*.testrun)|*.testrun|XML files|*.xml|All files" +
    "|*.*";
            this.openFileDialog.Multiselect = true;
            this.openFileDialog.Title = "Open test run";
            // 
            // DockPanel
            // 
            this.DockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DockPanel.DockBackColor = System.Drawing.SystemColors.AppWorkspace;
            this.DockPanel.Location = new System.Drawing.Point(0, 49);
            this.DockPanel.Name = "DockPanel";
            this.DockPanel.Size = new System.Drawing.Size(880, 527);
            dockPanelGradient1.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient1.StartColor = System.Drawing.SystemColors.ControlLight;
            autoHideStripSkin1.DockStripGradient = dockPanelGradient1;
            tabGradient1.EndColor = System.Drawing.SystemColors.Control;
            tabGradient1.StartColor = System.Drawing.SystemColors.Control;
            tabGradient1.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            autoHideStripSkin1.TabGradient = tabGradient1;
            autoHideStripSkin1.TextFont = new System.Drawing.Font("Tahoma", 8.25F);
            //dockPanelSkin1.AutoHideStripSkin = autoHideStripSkin1;
            tabGradient2.EndColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.StartColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient2.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.ActiveTabGradient = tabGradient2;
            dockPanelGradient2.EndColor = System.Drawing.SystemColors.Control;
            dockPanelGradient2.StartColor = System.Drawing.SystemColors.Control;
            dockPaneStripGradient1.DockStripGradient = dockPanelGradient2;
            tabGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            tabGradient3.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient1.InactiveTabGradient = tabGradient3;
            dockPaneStripSkin1.DocumentGradient = dockPaneStripGradient1;
            dockPaneStripSkin1.TextFont = new System.Drawing.Font("Tahoma", 8.25F);
            tabGradient4.EndColor = System.Drawing.SystemColors.ActiveCaption;
            tabGradient4.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient4.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
            tabGradient4.TextColor = System.Drawing.SystemColors.ActiveCaptionText;
            dockPaneStripToolWindowGradient1.ActiveCaptionGradient = tabGradient4;
            tabGradient5.EndColor = System.Drawing.SystemColors.Control;
            tabGradient5.StartColor = System.Drawing.SystemColors.Control;
            tabGradient5.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient1.ActiveTabGradient = tabGradient5;
            dockPanelGradient3.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient3.StartColor = System.Drawing.SystemColors.ControlLight;
            dockPaneStripToolWindowGradient1.DockStripGradient = dockPanelGradient3;
            tabGradient6.EndColor = System.Drawing.SystemColors.InactiveCaption;
            tabGradient6.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient6.StartColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient6.TextColor = System.Drawing.SystemColors.InactiveCaptionText;
            dockPaneStripToolWindowGradient1.InactiveCaptionGradient = tabGradient6;
            tabGradient7.EndColor = System.Drawing.Color.Transparent;
            tabGradient7.StartColor = System.Drawing.Color.Transparent;
            tabGradient7.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            dockPaneStripToolWindowGradient1.InactiveTabGradient = tabGradient7;
            dockPaneStripSkin1.ToolWindowGradient = dockPaneStripToolWindowGradient1;
            //dockPanelSkin1.DockPaneStripSkin = dockPaneStripSkin1;
            //this.DockPanel.Skin = dockPanelSkin1;
            this.DockPanel.TabIndex = 10;
            this.DockPanel.ActiveDocumentChanged += new System.EventHandler(this.DockPanel_ActiveDocumentChanged);
            this.DockPanel.ActiveContentChanged += new System.EventHandler(this.DockPanel_ActiveDocumentChanged);
            // 
            // dbLoadImageList
            // 
            this.dbLoadImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("dbLoadImageList.ImageStream")));
            this.dbLoadImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.dbLoadImageList.Images.SetKeyName(0, "load_bar_0.png");
            this.dbLoadImageList.Images.SetKeyName(1, "load_bar_1.png");
            this.dbLoadImageList.Images.SetKeyName(2, "load_bar_2.png");
            this.dbLoadImageList.Images.SetKeyName(3, "load_bar_3.png");
            this.dbLoadImageList.Images.SetKeyName(4, "load_bar_4.png");
            // 
            // closeAllResultsToolStripMenuItem
            // 
            this.closeAllResultsToolStripMenuItem.Name = "closeAllResultsToolStripMenuItem";
            this.closeAllResultsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.W)));
            this.closeAllResultsToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.closeAllResultsToolStripMenuItem.Text = "Close all results";
            this.closeAllResultsToolStripMenuItem.Click += new System.EventHandler(this.TestRunAnalysisForm_CloseAllResults);
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 576);
            this.Controls.Add(this.DockPanel);
            this.Controls.Add(this.mainToolStrip);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Yats - Yet Another Test System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.MainForm_DragDrop);
            this.DragOver += new System.Windows.Forms.DragEventHandler(this.MainForm_DragOver);
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripButton newTestRunBtn;
        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openTextViewMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filterLogViewMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openTestSequenceToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripMenuItem viewDatabaseMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openLogDirectoryMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem globalParametersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testCasesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testParametersToolStripMenuItem;
        internal WeifenLuo.WinFormsUI.Docking.DockPanel DockPanel;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem alwaysOnTopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fullScreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem recentTestsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openApplicationDataDirectoryMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logLevelFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnDbWrites;
        private System.Windows.Forms.ImageList dbLoadImageList;
        private System.Windows.Forms.ToolStripSplitButton openToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem closeAllResultsToolStripMenuItem;
    }
}

