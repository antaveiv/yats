﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using yats.Logging.Util;
using yats.Gui.Winforms.YatsApplication.Properties;
using System.Configuration;
using yats.Utilities;

namespace yats.Gui.Winforms.YatsApplication
{
    class LogSettings : ILogSettings
    {
        public string ConnectionString
        {
            get { return FileUtil.ReplaceEnvironmentVariables(ConfigurationManager.AppSettings["ConnectionString"]); }
        }

        public string ConnectionStringDefault
        {
            get { return FileUtil.ReplaceEnvironmentVariables(ConfigurationManager.AppSettings["ConnectionStringDefault"]); }
        }

        public string SQLiteDbPath
        {
            get { return FileUtil.ReplaceEnvironmentVariables(ConfigurationManager.AppSettings["SQLiteDbPath"]); }
        }
    }
}
