﻿namespace yats.Gui.Winforms.YatsApplication.Configuration
{
    partial class UserInterfaceSettingsPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbOpenNewTest = new System.Windows.Forms.CheckBox();
            this.cbRememberTestRuns = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbAssignDefaultParameterValues = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbUploadCrashLog = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cbOpenNewTest);
            this.groupBox1.Controls.Add(this.cbRememberTestRuns);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(334, 72);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Main form";
            // 
            // cbOpenNewTest
            // 
            this.cbOpenNewTest.AutoSize = true;
            this.cbOpenNewTest.Location = new System.Drawing.Point(6, 42);
            this.cbOpenNewTest.Name = "cbOpenNewTest";
            this.cbOpenNewTest.Size = new System.Drawing.Size(188, 17);
            this.cbOpenNewTest.TabIndex = 4;
            this.cbOpenNewTest.Text = "Open new test scenario on startup";
            this.cbOpenNewTest.UseVisualStyleBackColor = true;
            // 
            // cbRememberTestRuns
            // 
            this.cbRememberTestRuns.AutoSize = true;
            this.cbRememberTestRuns.Location = new System.Drawing.Point(6, 19);
            this.cbRememberTestRuns.Name = "cbRememberTestRuns";
            this.cbRememberTestRuns.Size = new System.Drawing.Size(157, 17);
            this.cbRememberTestRuns.TabIndex = 3;
            this.cbRememberTestRuns.Text = "Reopen test runs on startup";
            this.cbRememberTestRuns.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.cbAssignDefaultParameterValues);
            this.groupBox3.Location = new System.Drawing.Point(3, 81);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(334, 45);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Test run editor";
            // 
            // cbAssignDefaultParameterValues
            // 
            this.cbAssignDefaultParameterValues.AutoSize = true;
            this.cbAssignDefaultParameterValues.Location = new System.Drawing.Point(6, 19);
            this.cbAssignDefaultParameterValues.Name = "cbAssignDefaultParameterValues";
            this.cbAssignDefaultParameterValues.Size = new System.Drawing.Size(254, 17);
            this.cbAssignDefaultParameterValues.TabIndex = 2;
            this.cbAssignDefaultParameterValues.Text = "Set default parameter values to added test steps";
            this.cbAssignDefaultParameterValues.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.cbUploadCrashLog);
            this.groupBox4.Location = new System.Drawing.Point(3, 132);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(334, 50);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Misc.";
            // 
            // cbUploadCrashLog
            // 
            this.cbUploadCrashLog.AutoSize = true;
            this.cbUploadCrashLog.Location = new System.Drawing.Point(6, 19);
            this.cbUploadCrashLog.Name = "cbUploadCrashLog";
            this.cbUploadCrashLog.Size = new System.Drawing.Size(176, 17);
            this.cbUploadCrashLog.TabIndex = 0;
            this.cbUploadCrashLog.Text = "Submit crash logs to developers";
            this.cbUploadCrashLog.UseVisualStyleBackColor = true;
            // 
            // UserInterfaceSettingsPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Name = "UserInterfaceSettingsPanel";
            this.Size = new System.Drawing.Size(340, 236);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbOpenNewTest;
        private System.Windows.Forms.CheckBox cbRememberTestRuns;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox cbAssignDefaultParameterValues;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox cbUploadCrashLog;
    }
}
