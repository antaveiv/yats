﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using Gui.Winforms.PublicAPI;
using yats.Gui.Winforms.YatsApplication.Properties;

namespace yats.Gui.Winforms.YatsApplication.Configuration
{
    public partial class UserInterfaceSettingsPanel : SettingsTabPanel
    {
        public UserInterfaceSettingsPanel()
        {
            InitializeComponent( );
        }

        public override string GetText()
        {
            return "User interface";
        }

        public override void LoadSettings()
        {
            cbRememberTestRuns.Checked = Settings.Default.RememberTestRuns;
            cbOpenNewTest.Checked = Settings.Default.OpenNewTest;
            cbAssignDefaultParameterValues.Checked = Settings.Default.AssignDefaultParameterValues;
            cbUploadCrashLog.Checked = Settings.Default.UploadCrashLogs;
        }

        public override void SaveSettings()
        {
            Settings.Default.RememberTestRuns = cbRememberTestRuns.Checked;
            Settings.Default.OpenNewTest = cbOpenNewTest.Checked;
            Settings.Default.AssignDefaultParameterValues = cbAssignDefaultParameterValues.Checked;
            Settings.Default.UploadCrashLogs = cbUploadCrashLog.Checked;
            Settings.Default.Save( );
        }

        public override int GetSortIndex()
        {
            return 100;
        }
    }
}
