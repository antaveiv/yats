﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using yats.Logging.Util;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.Discovery;
using yats.TestRun.Cancel;
using yats.Utilities;
using yats.Gui.Winforms.YatsApplication.Properties;
using yats.Logging.Interface;
using System.Threading.Tasks;

namespace yats.Gui.Winforms.YatsApplication
{
    static class Program
    {
        static bool DELETE_DOCKPANEL_CONFIG = false;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string [] args)
        {            
            FileStream fs = null;
            try
            {
                // create directory in AppData if does not exist yet
                Directory.CreateDirectory(yats.Utilities.FileUtil.GetApplicationDataPath());

                //lock file - prevent multiple instance access to database, configuration etc.
                try
                {
                    fs = new FileStream( yats.Utilities.FileUtil.GetApplicationDataPath( "lock" ),
                        FileMode.Create,
                        FileAccess.ReadWrite,
                        FileShare.None );
                }
                catch(System.IO.IOException)
                {
                    MessageBox.Show( "Another instance is running" );
                    return;
                }
                catch { }

                #region Write unhandled exceptions to crash.log
                Application.ThreadException += CrashLog.WriteThreadException;
                AppDomain.CurrentDomain.UnhandledException += CrashLog.WriteUnhandledException;
                if (Settings.Default.UploadCrashLogs)
                {
                    CrashLog.TryUploadLog("http://yats.veiverys.com/crash");
                }
                #endregion

                Application.EnableVisualStyles( );
                Application.SetCompatibleTextRenderingDefault( false );

                Microsoft.Win32.SystemEvents.SessionEnding += (sender, e) => SessionEndingHandler( );

                if (DELETE_DOCKPANEL_CONFIG)
                {
                    FileUtil.Delete(MainForm.DOCKPANEL_CONFIG_FILE);
                }

                Application.Run( new MainForm( args ) );

                GlobalParameterSingleton.Instance.Save( );
            }
            catch
            {
                throw;
            }
            finally
            {
                if(fs != null)
                {
                    fs.Close( );
                    yats.Utilities.FileUtil.Delete( yats.Utilities.FileUtil.GetApplicationDataPath( "lock" ) );
                }
            }
        }
                
        // Log Connection error handler
        static void Connection_OnError (object sender, ConnectionErrorEventArgs e)
        {
            MessageBox.Show(e.Message);
        }

        public static readonly Task InitLoggingTask = InitLogging();
        static async Task InitLogging()
        {
            await Task.Run(() => {
                LoggingInit.OnError += Connection_OnError;
                LoggingInit.Init(new LogSettings());
                Application.ApplicationExit += LoggingInit.ApplicationExitHandler;
            });
        }

        public static readonly Task InitTestRepositoryManagerTask = InitTestRepositoryManager();
        static async Task InitTestRepositoryManager()
        {
            await Task.Run(() =>
            {
                // init global test case managers
                yats.Gui.Winforms.Components.TestCaseOrganizer.PluginFacade.TestRepositoryManagers = TestCaseRepository.Instance.Managers;

                // show error message if test cases with identical UniqueIds are loaded. Should not happen
                TestCaseRepository.Instance.Managers.InitializationFinished += new DuplicateTestCaseChecker((string testCaseId) => { MessageBox.Show("Duplicate test case type detected:" + Environment.NewLine + testCaseId); }).Check;
                TestCaseRepository.Instance.Managers.Initialize(true);
            });
        }

        public static readonly Task InitParameterTypesTask = InitParameterTypes();
        static async Task InitParameterTypes()
        {
            await Task.Run(() =>
            {
                ParameterTypeCollection.Instance.Initialize();
            });
        }
        
        private static object SessionEndingHandler()
        {
            CancelHandlerCollection.Cancel (null, "Microsoft.Win32.SystemEvents.SessionEnding");
            return null; 
        }
    }
}
