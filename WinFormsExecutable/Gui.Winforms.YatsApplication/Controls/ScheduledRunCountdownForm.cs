﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using yats.Utilities;

namespace yats.Gui.Winforms.YatsApplication.Controls
{
    public partial class ScheduledRunCountdownForm : Form
    {
        private const int MINUTE_STEP = 5;
        public ScheduledRunCountdownForm()
        {
            InitializeComponent( );
            List<int> hours = new List<int>();
            for (int i = 0; i < 24; i++)
            {
                hours.Add(i);
            }
            List<int> minutes = new List<int>();
            for (int i = 0; i < 60; i += MINUTE_STEP)
            {
                minutes.Add(i);
            }
            cbHours.DataSource = hours;
            cbMinutes.DataSource = minutes;

            //initialize controls, current time
            btCancel_Click(this, null);
        }

        public enum ModeEnum
        {
            All, 
            Selected, 
            FromSelected
        }

        /// <summary>
        /// Get the selected Run mode (all/selected/from selected)
        /// </summary>
        public ModeEnum RunMode
        {
            get
            {
                return mode;
            }
        }
        private ModeEnum mode;

        private void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                if(IsDisposed)
                {
                    return;
                }

                var remaining = runAt.Subtract( DateTime.Now );
                if(remaining.TotalSeconds <= 1)
                {
                    timer.Stop( );

                    DialogResult = DialogResult.OK;

                    if(rbFromSelected.Checked)
                    {
                        mode = ModeEnum.FromSelected;
                    }
                    else if(rbSelected.Checked)
                    {
                        mode = ModeEnum.Selected;
                    }
                    else
                    {
                        mode = ModeEnum.All;
                    }

                    Close( );
                }
                lbCountdown.Text = string.Format( "Starting after {0}", remaining.ToFriendlyDisplay( 2, TimeSpanExtensions.TimeSpanField.Second ) );
            }
            catch(Exception ex)
            {
                CrashLog.Write( ex, "No crash" );
            }
        }

        DateTime runAt;
        private void btOk_Click(object sender, EventArgs e)
        {
            runAt = monthCalendar.SelectionStart.Date.Add( new TimeSpan((int)cbHours.SelectedItem, (int)cbMinutes.SelectedItem, 0));
            timer.Start( );
            groupBox1.Enabled = groupBox2.Enabled = groupBox3.Enabled = false;
            lbCountdown.Visible = true;
            lbCountdown.Text = ""; // will be updated by the timer
            btOk.Enabled = false;
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            // can enter this method during form constructor (e == null) or button press (timer.enabled)
            if(timer.Enabled || e == null) // only cancel the timer, leave the form
            {
                lbCountdown.Visible = false;
                timer.Enabled = false;
                groupBox1.Enabled = groupBox2.Enabled = groupBox3.Enabled = true;
                lbCountdown.Visible = false;
                btOk.Enabled = true;
                if(e == null) // don't overwrite user settings, set values only on first form show
                {
                    DateTime time = DateTime.Now;
                    monthCalendar.MinDate = time.Date;
                    int suggestedTime = (time.Hour * 60 + time.Minute + 15) / MINUTE_STEP * MINUTE_STEP; // start in 15min by default

                    cbHours.SelectedItem = suggestedTime / 60;
                    cbMinutes.SelectedItem = suggestedTime % 60;
                }
            }
            else
            {
                DialogResult = DialogResult.Cancel;
                Close( );
            }
        }
    }
}
