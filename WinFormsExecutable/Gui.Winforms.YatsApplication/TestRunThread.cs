﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Threading;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using yats.ExecutionEngine;
using yats.ExecutionQueue;
using yats.Gui.Winforms.Components.DbBrowser;
using yats.Gui.Winforms.Components.ParameterEditor;
using yats.Gui.Winforms.Components.TestRunTree;
using yats.Gui.Winforms.YatsApplication.Properties;
using yats.Logging.Interface;
using yats.TestCase.Interface;
using yats.TestCase.Parameter;
using yats.TestRun;
using yats.Utilities;
using yats.TestRepositoryManager.Discovery;

namespace yats.Gui.Winforms.YatsApplication
{
    internal class TestRunThread
    {
        TestRunTreeForm testRunTreeForm;
        bool FormActive = true;
        ITestRunner testRunner;
        MainForm mainForm; //TODO: refactor, does not belong here
        bool LogTestRun;
        public delegate bool BeforeRunDelegate(ITestRunner testRunner);
        public static event BeforeRunDelegate BeforeRun;
        Thread testThread;

        public TestRunThread(MainForm mainForm, TestRunTreeForm testRunTreeForm, ITestRunner testRunner, bool logTestRun)
        {
            this.testRunTreeForm = testRunTreeForm;
            this.testRunTreeForm.OnCancel += new EventHandler(TestRunTreeForm_OnCancel);
            this.testRunTreeForm.FormClosing += TestRunTreeForm_FormClosing;
            this.testRunner = testRunner;
            this.mainForm = mainForm;
            this.LogTestRun = logTestRun;
        }

        private void TestRunTreeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            FormActive = false;
            testRunner.CancelHandler.Cancel(this, "form closed");
        }

        public void Start()
        {
            if (BeforeRun != null)
            {
                foreach (BeforeRunDelegate handler in BeforeRun.GetInvocationList())
                {
                    try
                    {
                        if (handler(testRunner) == false)
                        {
                            return;
                        }
                    }
                    catch (Exception)
                    {
                        return;
                    }
                }
            }

            ThreadStart threadDelegate = new ThreadStart(this.DoWork);
            testThread = new Thread(threadDelegate);
            testThread.Name = string.Format("TestRun {0}", testRunner.TestRun.Name);
            //testThread.Priority = ThreadPriority.BelowNormal;
            testThread.Start();
        }
        
        void TestRunTreeForm_OnCancel(object sender, EventArgs e)
        {
            testRunner.CancelHandler.Cancel (this, "user cancel");
        }

        void DoWork()
        {
            testRunner.TestRunFinished += executeEngine_TestRunFinished;
            testRunner.TestRunStarting += executeEngine_TestRunStarting;
            testRunner.TestStepResult += executeEngine_TestStepResult;
            testRunner.TestStepRepetitionResult += executeEngine_TestStepRepetitionResult;
            testRunner.TestStepStarting += executeEngine_TestStepStarting;

            PlatformHelper.PreventPcSleep();
            testRunner.StartTestRun();
            PlatformHelper.AllowPcSleep();
            testRunner.TestRunFinished -= executeEngine_TestRunFinished;
            testRunner.TestRunStarting -= executeEngine_TestRunStarting;
            testRunner.TestStepResult -= executeEngine_TestStepResult;
            testRunner.TestStepRepetitionResult -= executeEngine_TestStepRepetitionResult;
            testRunner.TestStepStarting -= executeEngine_TestStepStarting;
        }

        void executeEngine_TestStepStarting (object sender, TestStepStartingEventArgs e)
        {
            if (FormActive)
            {
                testRunTreeForm.SetStepRunning(e.Step, true);
            }

            TestStepSingle single = e.Step as TestStepSingle;
            TestStepComposite composite = e.Step as TestStepComposite;

            StepInfo stepInfo = null;
            if (single != null)
            {
                stepInfo = StepInfo.GetStepStart(e.Step.Name, e.Path, TestCaseRepository.Instance.Managers.GetManagerByTestUniqueId(single.UniqueTestId).GetUniqueTestId(single.TestCase));
            }
            else if (composite != null)
            {
                stepInfo = StepInfo.GetStepStart(e.Step.Name, e.Path);
            }

            //log4net.LogManager.GetLogger("Execute").Info("Starting test " + step.Name);

            YatsLogAppenderCollection.Instance.UpdateStep(e.TestRunId, stepInfo);

            //Log test parameters - can be disabled in settings
            if (yats.Logging.Interface.Properties.Settings.Default.LogTestParameters)
            {
                if (e.Step.Parameters != null)
                {
                    foreach (var param in e.Step.Parameters)
                    {
                        if (param.IsResult)
                        {
                            continue;
                        }
                        var found = false;
                        var value = GetParameterValueVisitor.Get(param.Value, testRunner.TestRun.GlobalParameters, out found);
                        if (found)
                        {
                            var useResult = value as TestResultParameter;
                            if (useResult != null && single != null)
                            {
                                object paramValue;
                                if (TestCaseRepository.Instance.Managers.GetManagerByTestUniqueId(single.UniqueTestId).GetParameterValue(single.TestCase, param, out paramValue))
                                {
                                    YatsLogAppenderCollection.Instance.WriteBinaryData(e.TestRunId, param.Name, "DEBUG", paramValue.ToString(), paramValue);
                                }
                            }
                            else
                            {
                                string valueStr = ParamValueToString.Get(param.Value, testRunner.TestRun.GlobalParameters);
                                AbstractParameterValue actualValue = GetParameterValueVisitor.Get(param.Value, testRunner.TestRun.GlobalParameters, out found);
                                YatsLogAppenderCollection.Instance.WriteBinaryData(e.TestRunId, param.Name, "DEBUG",
                                    valueStr,  // user-friendly display
                                    ((actualValue != null)? actualValue.GetValue(null) : null)); // for object dump - try to get the 'actual' data object, not wrapped into AbstractParameterValue
                            }
                            //log4net.LogManager.GetLogger("Parameter").DebugFormat("{0} [{1}]", param.Name, valueStr);
                            //log4net.LogManager.GetLogger("Parameter").Debug(value);
                        }

                    }
                }
            }
        }

        void executeEngine_TestStepRepetitionResult(object sender, TestStepRepetitionResultEventArgs e)
        {
            if (FormActive)
            {
                testRunTreeForm.SetStepRunning(e.Step, false);
            }
            StepInfo stepInfo = StepInfo.GetRepetitionResult(e.Step.Name, e.Path, e.Result.Result.ToString());
            YatsLogAppenderCollection.Instance.UpdateStep(e.TestRunId, stepInfo);
            //log4net.LogManager.GetLogger( "Execute" ).InfoFormat( "Evaluated result: {0} {1}", step.Name, result.Result);
        }

        void executeEngine_TestStepResult(object sender, TestStepResultEventArgs e)
        {
            if (FormActive)
            {
                testRunTreeForm.Results.AddResult(e.Step, e.EvaluatedResult);
                testRunTreeForm.SetStepRunning(e.Step, false);
            }
            // when test step is finished, transfer its results to other test parameters
            yats.ExecutionEngine.AssignTestResultsToParametersVisitor.Process(testRunner.TestRun, e.Step);
            if (FormActive && ProgressEstimated && e.Step is TestStepSingle)
            {
                testRunTreeForm.IncrementProgress();
            }

            TestStepSingle single = e.Step as TestStepSingle;
            StepInfo stepInfo = StepInfo.GetStepResult(e.Step.Name, e.Path, e.RawResult.Result.ToString(), e.EvaluatedResult.Result.ToString());

            if (single != null)
            {
                stepInfo.testCaseUniqueId = TestCaseRepository.Instance.Managers.GetManagerByTestUniqueId(single.UniqueTestId).GetUniqueTestId(single.TestCase);
            }

            //Log test result values - can be disabled in settings
            if (yats.Logging.Interface.Properties.Settings.Default.LogTestResults)
            {
                var singleStep = e.Step as TestStepSingle;
                if (singleStep != null && e.Step.Parameters != null)
                {
                    foreach (var param in e.Step.Parameters)
                    {
                        try
                        {
                            if (param.IsResult == false)
                            {
                                continue;
                            }
                            object value = null;
                            var found = TestCaseRepository.Instance.Managers.GetManagerByTestUniqueId(singleStep.UniqueTestId).GetTestResult(singleStep.TestCase, param, out value);
                            if (!found)
                            {
                                continue;
                            }


                            var valueAsAbstractParam = value as AbstractParameterValue;
                            if (valueAsAbstractParam != null)
                            {
                                string valueStr = ParamValueToString.Get(valueAsAbstractParam, testRunner.TestRun.GlobalParameters);
                                YatsLogAppenderCollection.Instance.WriteBinaryData(e.TestRunId, param.Name, "DEBUG", string.Format("{0} [{1}]", param.Name, valueStr), valueAsAbstractParam.GetValue(null));
                            }
                            else if (value != null)
                            {
                                YatsLogAppenderCollection.Instance.WriteBinaryData(e.TestRunId, param.Name, "DEBUG", value.ToString(), value);

                                //byte[] valueAsBytes = value as byte[];
                                //if (valueAsBytes != null && valueAsBytes.Length < 64)
                                //{
                                //    YatsLogAppenderCollection.Instance.WriteBinaryData(e.TestRunId, "Result", "DEBUG", string.Format("{0} [{1}]", param.Name, ByteArray.ToStringHex(valueAsBytes)), value);
                                //}
                                //else
                                //{
                                //    YatsLogAppenderCollection.Instance.WriteBinaryData(e.TestRunId, "Result", "DEBUG", string.Format("{0} [{1}]", param.Name, value), value);
                                //}
                            }
                            else
                            {
                                YatsLogAppenderCollection.Instance.WriteBinaryData(e.TestRunId, param.Name, "DEBUG", "null", null);
                            }
                        }
                        catch
                        {
                        }
                    }
                }
            }

            if (e.RawResult.GetType() != typeof(TestResult))
            {
                if (e.RawResult is ExceptionResult)
                {
                    log4net.LogManager.GetLogger("TestResult").Debug((e.RawResult as ExceptionResult).Exception);
                }
                else
                {
                    log4net.LogManager.GetLogger("TestResult").Debug(e.RawResult);
                }
            }


            YatsLogAppenderCollection.Instance.UpdateStep(e.TestRunId, stepInfo);

            //log4net.LogManager.GetLogger( "Execute" ).InfoFormat( "Result: {0} raw {1} evaluated {2}", step.Name, rawResult.Result, evaluatedResult.Result);
        }

        byte[] SerializeTestSettings()
        {
            // serialize test run
            try
            {
                return yats.Utilities.SerializationHelper.SerializeToArray<ITestRun>(testRunner.TestRun);
            }
            catch (Exception ex)
            {
                Utilities.CrashLog.Write(ex, "Test run serialization");
            }
            return null;
        }

        bool ProgressEstimated = false;
        void executeEngine_TestRunStarting (object sender, TestRunStartEventArgs e)
        {
            if (FormActive)
            {
                testRunTreeForm.ExecutionStatus.IsRunning = true;
                testRunTreeForm.Results.Reset();

                int progressMax;
                ProgressEstimated = GetNumberOfStepsVisitor.Get(testRunner.TestRun.TestSequence, testRunner.TestRun.TestSequence, out progressMax);
                testRunTreeForm.SetProgressMaxItems(ProgressEstimated, progressMax);
            }

            if (yats.Logging.Interface.Properties.Settings.Default.OpenLogOnRun && LogTestRun)
            {
                // open log form
                CachedLog source = new CachedLog(DateTime.Now, testRunner.TestRun.Name, testRunner.TestRun.Description, SerializeTestSettings());
                source.MessageChanged += (s, _e) => { };//TODO
                YatsLogAppenderCollection.Instance.AddEventSink(source, e.TestRunId);
                
                if (mainForm.IsDisposed)
                {
                    return;
                }
                
                //TODO refactor, should not handle mainform here
                mainForm.Invoke(new MethodInvoker(delegate()
                {
                    string title = (string.IsNullOrEmpty(testRunner.TestRun.Name)) ? "Log" : "Log - " + testRunner.TestRun.Name;
                    var childForm = new TestRunAnalysisForm(title, source, true, LoggerFilterSingleton.Instance, new LogBrowserRunningStatus(testRunTreeForm.ExecutionStatus));

                    childForm.FormClosing += new FormClosingEventHandler(childForm_FormClosing);
                    childForm.CloseAllResultsPressed += mainForm.TestRunAnalysisForm_CloseAllResults;

                    // event handler for Log View form's Cancel and Repeat buttons
                    childForm.CancelPressed += delegate(object sender_, EventArgs e_)
                    {
                        testRunner.CancelHandler.Cancel(sender_, "user cancel");
                    };

                    childForm.RepeatPressed += delegate(object sender_, EventArgs e_)
                    {
                        var testRunnerClone = testRunner.Clone();
                        var thread = new TestRunThread(mainForm, testRunTreeForm, testRunnerClone, LogTestRun);
                        thread.Start();
                    };

                    if (mainForm.DockPanel.DocumentStyle == DocumentStyle.SystemMdi)
                    {
                        childForm.MdiParent = mainForm;
                        childForm.Show();
                    }
                    else
                    {
                        lock (mainForm.DockPanelLock)
                        {
                            childForm.Show(mainForm.DockPanel);
                        }
                    }
                }));
            }

            if (LogTestRun)
            {
                YatsLogAppenderCollection.Instance.StartTestRun(testRunner.TestRun.Name, testRunner.TestRun.Description, SerializeTestSettings(), e.TestRunId);
            }
        }

        private class LogBrowserRunningStatus : ITestRunningStatus
        {
            private ITestExecutionStatus testExecutionStatus;

            public LogBrowserRunningStatus(ITestExecutionStatus testExecutionStatus)
            {
                this.testExecutionStatus = testExecutionStatus;
                testExecutionStatus.OnStatusChange += testExecutionStatus_OnStatusChange;
            }

            void testExecutionStatus_OnStatusChange(object sender, TestExecutionStatusEventArgs e)
            {
                if (OnStatusChange != null)
                {
                    OnStatusChange(this, null);
                }
            }

            public bool IsRunning
            {
                get {return testExecutionStatus.IsRunning;}
            }

            public event EventHandler OnStatusChange;
        }

        void childForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            yats.Gui.Winforms.Components.DbBrowser.TestRunAnalysisForm form = sender as yats.Gui.Winforms.Components.DbBrowser.TestRunAnalysisForm;
            YatsLogAppenderCollection.Instance.RemoveEventSink(form.DataSource as ILogEventSink);
        }

        void executeEngine_TestRunFinished (object sender, TestRunFinishEventArgs e)
        {
            if (FormActive)
            {
                testRunTreeForm.ExecutionStatus.IsRunning = false;
            }
            YatsLogAppenderCollection.Instance.EndTestRun(e.TestRunId, e.Result);
                        
            if (OnTestRunFinished != null)
            {
                OnTestRunFinished(this, null);
            }
        }

        public event EventHandler OnTestRunFinished;

        internal void Join()
        {
            if (testThread != null)
            {
                testThread.Join();
            }
        }
    }
}
