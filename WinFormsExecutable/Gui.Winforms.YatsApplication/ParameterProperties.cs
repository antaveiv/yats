﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using yats.ExecutionQueue;
using yats.Gui.Winforms.Components.ParameterEditor;
using yats.TestCase.Parameter;
using yats.Gui.Winforms.YatsApplication.Properties;
using yats.TestRun.Commands;
using yats.Utilities.Commands;
using yats.TestRepositoryManager.Discovery;

namespace yats.Gui.Winforms.YatsApplication
{
    //Test Repository facade for Parameter editor 
    class ParameterProperties : IParameterProperties
    {
        public bool CanParameterBeNull(TestStepSingle testCase, IParameter param)
        {
            return TestCaseRepository.Instance.Managers.GetManagerByTestUniqueId(testCase.UniqueTestId).CanParameterBeNull(testCase.TestCase, param);
        }

        public ICommand ResetToDefaultValue(TestStepSingle testCase, IParameter param)
        {
            object defaultValue;
            if (TestCaseRepository.Instance.Managers.GetManagerByTestUniqueId(testCase.UniqueTestId).GetParameterDefault(testCase.TestCase, param, out defaultValue) == false)
            {
                return null;
            }
            if (defaultValue == null)
            {
                return new SetParameterValue(param, null);
            }

            if (defaultValue is AbstractParameterValue)
            {
                return new SetParameterValue(param, defaultValue as AbstractParameterValue);
            }

            List<AbstractParameterValue> suitableTypes = ParameterTypeCollection.Instance.GetSuitableValueTypes(param);
            if (suitableTypes == null)
            {
                //no parameter value class is suitable for this parameter
                return null;
            }

            foreach (var suitableType in suitableTypes)
            {
                if (suitableType.SetValue(param, defaultValue))
                {
                    return new SetParameterValue(param, suitableType);
                }
            }
			return null;
        }
    }
}
