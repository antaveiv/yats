﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using yats.Logging.Interface;
using yats.Utilities;
using Aga.Controls.Tree;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace yats.Gui.Winforms.Components.DbBrowser
{
    public partial class TestRunTestsView : UserControl
    {
        //private TestRunTestsTreeModel tmp_model;
        private TreeModel<TestStepNode> m_model;
        private BatchProducerConsumer<LogStepAddedEventArgs> logConsumer;
        public TestRunTestsView()
        {            
            InitializeComponent( );
            logConsumer = new BatchProducerConsumer<LogStepAddedEventArgs>(300, 500, ConsumptionAction);
        }

        internal TestRunTestsView(CachedLog dataSource)
            : this()
        {
            SetDataSource(dataSource);
        }

        public CachedLog DataSource
        {
            get;
            private set;
        }

        internal void SetDataSource(CachedLog dataSource)
        {
            if (IsDisposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            m_model = new TreeModel<TestStepNode>();
            tree.Model = m_model;

            if (this.DataSource != null)
            {
                dataSource.TestStepAdded -= dataSource_TestStepAdded;
            }
            this.DataSource = dataSource;
            OnStepsAdded(dataSource.Steps);
            dataSource.TestStepAdded += dataSource_TestStepAdded;
            
            tree.ExpandAll();
        }

        private void ConsumptionAction(LogStepAddedEventArgs[] events)
        {
            OnStepsAdded(events.Select(evt => evt.Step));
        }

        private void OnStepsAdded(IEnumerable<StepInfo> steps)
        {
            if (IsDisposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            tree.BeginUpdate();
            foreach (var step in steps)
            {
                //Debug.Print(e.Step.Path.ToString());
                PathSegment currentPathSegment = new PathSegment();
                step.Path.Reset();

                if (step.Path.Length == 1)
                {
                    if (step.RowType != StepInfoTypeEnum.STEP_START)
                    {
                        return;
                    }

                    if (step.IsTestCase) // test case
                    {
                        var newNode = new TestStepNode(NodeKind.TEST, step, currentPathSegment, null);
                        m_model.Nodes.Add(newNode);
                    }
                    else
                    {
                        m_model.Nodes.Add(new TestStepNode(NodeKind.GROUP, step, currentPathSegment, null));
                    }
                }
                else
                {
                    switch (step.RowType)
                    {
                        case StepInfoTypeEnum.STEP_START:
                            TryCreateHierarchy(m_model.Root.Nodes[0], step);
                            tree.ExpandAll();
                            break;
                        case StepInfoTypeEnum.STEP_RESULT:
                        case StepInfoTypeEnum.REPETITION_RESULT:
                            TryUpdateStepResults(m_model.Root.Nodes[0], step);
                            break;
                    }
                }
            }
            tree.EndUpdate();
        }        

        void dataSource_TestStepAdded(object sender, LogStepAddedEventArgs e)
        {
            if (IsDisposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }         
            logConsumer.EnqueueTask(e);
        }

        private bool TryCreateHierarchy(TestStepNode root, StepInfo step)
        {
            PathSegment currentPathSegment = new PathSegment();
            step.Path.Reset();
            if (step.Path.Next(ref currentPathSegment))
            {
                return false; // end already, unexpected
            }
            if (currentPathSegment != root.Path)
            {
                return false;
            }
            while (step.Path.Next(ref currentPathSegment) == false)
            {
                var tmp = root.FindChild(currentPathSegment);
                if (tmp != null)
                {
                    root = tmp;
                    continue;
                }

                foreach (var ch in root.Nodes)
                {
                    if (ch.Path.Index == currentPathSegment.Index)
                    {
                        if ((ch.Nodes.Count == 1 && ch.Nodes[0].Kind == NodeKind.REPETITION))
                        {
                            foreach (var rep in ch.Nodes[0].Nodes)
                            {
                                if (rep.Path == currentPathSegment)
                                {
                                    root = rep;
                                    goto continueLoop;
                                }
                            }
                        }
                    }
                }

                return false;

            continueLoop:
                continue;
            }

            var newNode = root.GetOrCreate(step, currentPathSegment);
            return newNode != null;
        }


        private bool TryUpdateStepResults(TestStepNode root, StepInfo step)
        {
            PathSegment currentPathSegment = new PathSegment();
            if (step.Path.Next(ref currentPathSegment))
            {
                if (currentPathSegment != root.Path)
                {
                    return false;
                }
                return root.UpdateResult(step);
            }

            if (currentPathSegment != root.Path)
            {
                return false;
            }

            while (step.Path.Finished == false)
            {
                step.Path.Next(ref currentPathSegment);
                var tmp = root.FindChild(currentPathSegment);
                if (tmp != null)
                {
                    if ((tmp.Nodes.Count == 1 && tmp.Nodes[0].Kind == NodeKind.REPETITION))
                    {
                        // update repetition result of the parent instead of analyzing repetitions
                        if (step.RowType == StepInfoTypeEnum.REPETITION_RESULT && step.Path.Finished)
                        {
                            tmp.UpdateResult(step);
                            return true;
                        }

                        bool found = false;
                        foreach (var rep in tmp.Nodes[0].Nodes)
                        {
                            if (rep.Path == currentPathSegment)
                            {
                                root = rep;
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        root = tmp;
                    }
                    continue;
                }

                foreach (var ch in root.Nodes)
                {
                    if (ch.Path.Index == currentPathSegment.Index)
                    {
                        if (ch.Nodes.Count == 1 && ch.Nodes[0].Kind == NodeKind.REPETITION)
                        {
                            // update repetition result of the parent instead of analyzing repetitions
                            if (step.RowType == StepInfoTypeEnum.REPETITION_RESULT && step.Path.Finished)
                            {
                                ch.UpdateResult(step);
                                return true;
                            }
                            foreach (var rep in ch.Nodes[0].Nodes)
                            {
                                if (rep.Path == currentPathSegment)
                                {
                                    root = rep;
                                    goto continueLoop;
                                }
                            }
                        }
                    }
                }

                return false;
            continueLoop:
                continue;
            }

            return root.UpdateResult(step);
        }

        private void tree_SelectionChanged(object sender, EventArgs e)
        {
            if (IsDisposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            if (StepSelectionChanged == null)
            {
                return;
            }

            List<TestStepNode> selected = new List<TestStepNode>();
            foreach (var selNode in tree.SelectedNodes)
            {
                selected.AddIfNotNull(selNode.Tag as TestStepNode);
            }
            StepSelectionChanged(this, new TestStepNodeEventArgs(selected));
        }

        public event EventHandler<TestStepNodeEventArgs> StepSelectionChanged;

        // Assign text color according to test step result
        private void nodeTextBox1_DrawText(object sender, Aga.Controls.Tree.NodeControls.DrawEventArgs e)
        {
            TestStepNode step = e.Node.Tag as TestStepNode;
            if (step == null)
            {
                return;
            }
            switch (step.EvaluatedResult)
            {
                case "CANCELED":
                    break;
                case "FAIL":
                    e.TextColor = Color.DarkRed;
                    break;
                case "INCONCLUSIVE":
                    e.TextColor = Color.RoyalBlue;
                    break;
                case "PASS":
                    e.TextColor = Color.SeaGreen;
                    break;
                case "NOT_RUN":
                    break;
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            if (disposing)
            {
                if (logConsumer != null)
                {
                    logConsumer.Dispose();
                    logConsumer = null;
                }            
            }
            base.Dispose(disposing);
        }

        internal void GotoNextFail()
        {
            var currentNode = tree.SelectedNodes.IsNullOrEmpty() ? tree.Root : tree.SelectedNodes[0];
            if (currentNode == null)
            {
                return;
            }
            var allNodes = TreeHelper.AsDepthFirstEnumerable<TreeNodeAdv>(tree.Root, (node) => { return node.Children; }).ToList();
            var currentIndex = allNodes.IndexOf(currentNode);
            var nextFailNode = allNodes.Skip(currentIndex + 1).FirstOrDefault(x => x.Tag != null && (x.Tag as TestStepNode).IsFailNode()); //x.Tag is null on root node
            if (nextFailNode != null)
            {
                tree.SelectedNode = nextFailNode;
            }
        }


        internal void GotoPreviousFail()
        {
            var currentNode = tree.SelectedNodes.IsNullOrEmpty() ? tree.Root : tree.SelectedNodes[0];
            if (currentNode == null)
            {
                return;
            }
            var allNodes = TreeHelper.AsDepthFirstEnumerable<TreeNodeAdv>(tree.Root, (node) => { return node.Children; }).ToList();
            var currentIndex = allNodes.IndexOf(currentNode);
            var nextFailNode = allNodes.Take(currentIndex).LastOrDefault(x => x.Tag != null && (x.Tag as TestStepNode).IsFailNode());
            if (nextFailNode != null)
            {
                tree.SelectedNode = nextFailNode;
            }
        }
        
        private void selectAllRepetitionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (tree.SelectedNodes.Count != 1)
            {
                return;
            }
            var currentNode = tree.SelectedNodes[0];
            var path = new StepPath((currentNode.Tag as TestStepNode).FullPath);
            string pathMatchRegex = "^" + string.Join(@"\|", path.Segments.Select(p=>string.Format(@"{0}\.\d+", p.Index))) + "$";
            
            Regex regex = new Regex(pathMatchRegex);
            selectAllRepetitions(tree.Root, regex);
        }

        private void selectAllRepetitions(TreeNodeAdv root, Regex pathRegex)
        {
            tree.BeginUpdate();
            foreach (var node in tree.AllNodes)
            {
                TestStepNode testStepNode = (node.Tag as TestStepNode);
                
                if (testStepNode != null)
                {
                    node.IsSelected = pathRegex.IsMatch(testStepNode.FullPath);
                }
            }
            tree.EndUpdate();
        }
        
        private void selectAllFailedStepsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (tree.SelectedNodes.Count != 1)
            {
                return;
            }
            var currentNode = tree.SelectedNodes[0];
            var path = new StepPath((currentNode.Tag as TestStepNode).FullPath);
            string pathMatchRegex = "^" + string.Join(@"\|", path.Segments.Select(p => string.Format(@"{0}\.\d+", p.Index))) + "$";

            Regex regex = new Regex(pathMatchRegex);

            tree.BeginUpdate();
            foreach (var node in tree.AllNodes)
            {

                TestStepNode testStepNode = (node.Tag as TestStepNode);
                if (testStepNode != null)
                {
                    node.IsSelected = regex.IsMatch(testStepNode.FullPath) && testStepNode.IsFailNode();
                }
            }
            tree.EndUpdate();
        }

        internal void SelectStepByPath(string path)
        {
            tree.BeginUpdate();

            this.tree.SelectionChanged -= new System.EventHandler(this.tree_SelectionChanged);

            foreach (var node in tree.AllNodes)
            {
                TestStepNode testStepNode = (node.Tag as TestStepNode);
                if (testStepNode != null && testStepNode.FullPath == path)
                {
                    node.IsSelected = true;
                    tree.EnsureVisible(node);
                } else
                {
                    node.IsSelected = false;
                }
            }

            tree.EndUpdate();

            this.tree.SelectionChanged += new System.EventHandler(this.tree_SelectionChanged);
            
            tree.Focus();
        }
    }
}
