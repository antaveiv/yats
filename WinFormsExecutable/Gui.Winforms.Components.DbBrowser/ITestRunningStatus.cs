﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace yats.Gui.Winforms.Components.DbBrowser
{
    public interface ITestRunningStatus
    {
        bool IsRunning { get; }
        event EventHandler OnStatusChange;
    }

    // always reports 'not running'
    public class DummyTestRunningStatus : ITestRunningStatus
    {
        public bool IsRunning
        {
            get { return false; }
        }

        public event EventHandler OnStatusChange
        {
            add { }
            remove { }
        }
    }
}
