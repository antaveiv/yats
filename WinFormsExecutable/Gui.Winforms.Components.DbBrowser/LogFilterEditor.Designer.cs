﻿namespace yats.Gui.Winforms.Components.DbBrowser
{
    partial class LogFilterEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogFilterEditor));
            this.tree = new Aga.Controls.Tree.TreeViewAdv();
            this.treeColumn1 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumn2 = new Aga.Controls.Tree.TreeColumn();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.resetAllToDEBUGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loggerText = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.levelComboBox = new Aga.Controls.Tree.NodeControls.NodeComboBox();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tree
            // 
            this.tree.BackColor = System.Drawing.SystemColors.Window;
            this.tree.Columns.Add(this.treeColumn1);
            this.tree.Columns.Add(this.treeColumn2);
            this.tree.ContextMenuStrip = this.contextMenu;
            this.tree.DefaultToolTipProvider = null;
            this.tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tree.DragDropMarkColor = System.Drawing.Color.Black;
            this.tree.FullRowSelect = true;
            this.tree.GridLineStyle = ((Aga.Controls.Tree.GridLineStyle)((Aga.Controls.Tree.GridLineStyle.Horizontal | Aga.Controls.Tree.GridLineStyle.Vertical)));
            this.tree.LineColor = System.Drawing.SystemColors.ControlDark;
            this.tree.Location = new System.Drawing.Point(0, 0);
            this.tree.Model = null;
            this.tree.Name = "tree";
            this.tree.NodeControls.Add(this.loggerText);
            this.tree.NodeControls.Add(this.levelComboBox);
            this.tree.SelectedNode = null;
            this.tree.ShowLines = false;
            this.tree.ShowPlusMinus = false;
            this.tree.Size = new System.Drawing.Size(458, 170);
            this.tree.TabIndex = 0;
            this.tree.Text = "treeViewAdv1";
            this.tree.UseColumns = true;
            this.tree.NodeMouseDoubleClick += new System.EventHandler<Aga.Controls.Tree.TreeNodeAdvMouseEventArgs>(this.tree_NodeMouseDoubleClick);
            // 
            // treeColumn1
            // 
            this.treeColumn1.Header = "Logger";
            this.treeColumn1.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn1.TooltipText = null;
            this.treeColumn1.Width = 250;
            // 
            // treeColumn2
            // 
            this.treeColumn2.Header = "Level";
            this.treeColumn2.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn2.TooltipText = null;
            this.treeColumn2.Width = 80;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetAllToDEBUGToolStripMenuItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(172, 26);
            // 
            // resetAllToDEBUGToolStripMenuItem
            // 
            this.resetAllToDEBUGToolStripMenuItem.Name = "resetAllToDEBUGToolStripMenuItem";
            this.resetAllToDEBUGToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.resetAllToDEBUGToolStripMenuItem.Text = "Reset all to DEBUG";
            this.resetAllToDEBUGToolStripMenuItem.Click += new System.EventHandler(this.resetAllToDEBUGToolStripMenuItem_Click);
            // 
            // loggerText
            // 
            this.loggerText.DataPropertyName = "Logger";
            this.loggerText.IncrementalSearchEnabled = true;
            this.loggerText.LeftMargin = 3;
            this.loggerText.ParentColumn = this.treeColumn1;
            // 
            // levelComboBox
            // 
            this.levelComboBox.DataPropertyName = "Level";
            this.levelComboBox.DropDownItems.Add("DEBUG");
            this.levelComboBox.DropDownItems.Add("INFO");
            this.levelComboBox.DropDownItems.Add("WARN");
            this.levelComboBox.DropDownItems.Add("ERROR");
            this.levelComboBox.DropDownItems.Add("FATAL");
            this.levelComboBox.DropDownItems.Add("OFF");
            this.levelComboBox.EditEnabled = true;
            this.levelComboBox.EditOnClick = true;
            this.levelComboBox.IncrementalSearchEnabled = true;
            this.levelComboBox.LeftMargin = 3;
            this.levelComboBox.ParentColumn = this.treeColumn2;
            // 
            // LogFilterEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 170);
            this.Controls.Add(this.tree);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LogFilterEditor";
            this.Text = "Log filter";
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Aga.Controls.Tree.TreeViewAdv tree;
        private Aga.Controls.Tree.TreeColumn treeColumn1;
        private Aga.Controls.Tree.TreeColumn treeColumn2;
        private Aga.Controls.Tree.NodeControls.NodeTextBox loggerText;
        private Aga.Controls.Tree.NodeControls.NodeComboBox levelComboBox;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem resetAllToDEBUGToolStripMenuItem;
    }
}
