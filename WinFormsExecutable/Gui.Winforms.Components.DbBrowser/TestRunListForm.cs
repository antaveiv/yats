﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using yats.Logging.Interface;
using System.Linq;
using yats.Gui.Winforms.Components.Util;

namespace yats.Gui.Winforms.Components.DbBrowser
{
    public partial class TestRunListForm : DockContent
    {
        public TestRunListForm()
        {
            InitializeComponent();
            btDelete.Enabled = false;
            btRepeatTest.Enabled = false;
            btViewLog.Enabled = false;
            testRunView.TreeSelectionChanged += new EventHandler( testRunView_TreeSelectionChanged );
            testRunView.TestRunDoubleClick += testRunView_TestRunDoubleClick;
            testRunView.TreeKeyUp += tree_KeyUp;
            this.TabText = this.Text;
        }

        public TestRunListForm(ILogStorage db)
            : this()
        {
            testRunView.SetStorageAccess(db);
        }

        public event EventHandler<TestRunLogEventArgs> OnTestRunView;
        public event EventHandler<TestRunLogEventArgs> OnRepeatTestRun;
        
        // View test log
        void testRunView_TestRunDoubleClick(TestRunLogInfo testRunLogInfo)
        {
            if(OnTestRunView != null)
            {
                var fullLog = testRunView.Storage.GetFullLogData(testRunLogInfo);
                if (fullLog == null)
                {
                    return;
                }
                OnTestRunView(this, new TestRunLogEventArgs() { TestRunLog = fullLog });
            }
        }

        private void btViewLog_Click(object sender, EventArgs e)
        {
            foreach (var testRunLogInfo in testRunView.SelectedTestRunLogs)
            {
                testRunView_TestRunDoubleClick(testRunLogInfo);
            }
        }
        
        void testRunView_TreeSelectionChanged(object sender, EventArgs e)
        {
            if(this.InvokeRequired)
            {
                BeginInvoke( new MethodInvoker( delegate() { testRunView_TreeSelectionChanged(sender, e); } ) );
                return;
            }  
            btDelete.Enabled = testRunView.SelectedTestRunLogs.Count > 0;
            btViewLog.Enabled = testRunView.SelectedTestRunLogs.Count > 0;
            btRepeatTest.Enabled = testRunView.SelectedTestRunLogs.Count == 1;
            renameToolStripMenuItem.Enabled = removeToolStripMenuItem.Enabled = loadTestRunToolStripMenuItem.Enabled = viewTestLogToolStripMenuItem.Enabled = testRunView.SelectedTestRunLogs.Count > 0;
        }
        
        private void btDelete_Click(object sender, EventArgs e)
        {
            foreach(var id in testRunView.SelectedTestRunLogs)
            {
                testRunView.Storage.Delete( id );
            }
        }

        private void btRepeatTest_Click(object sender, EventArgs e)
        {
            foreach(var tr in testRunView.SelectedTestRunLogs)
            {
                if(OnRepeatTestRun != null)
                {
                    TestRunLog row = testRunView.Storage.GetFullLogData( tr );
                    if(row != null)
                    {
                        OnRepeatTestRun( this, new TestRunLogEventArgs(){TestRunLog = row} );
                    }
                }
            }
        }

        private void renameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string newName = testRunView.SelectedTestRunLogs.First().Name;
            if (InputBox.Get("Name", "", ref newName) == System.Windows.Forms.DialogResult.OK)
            {
                foreach (var tr in testRunView.SelectedTestRunLogs)
                {
                    testRunView.Storage.Rename(tr, newName);
                }    
            }
        }

        private void contextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            testRunView_TreeSelectionChanged(sender, e);
        }
        
        #region
        private void tree_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Control && (char)e.KeyValue == 'F')
            {
                // ctrl+f
                if (searchPanel.Visible)
                {
                    HideSearchField();
                }
                else
                {
                    ShowSearchField();
                }
                e.Handled = true;
            }
            else if (!e.Control && !e.Alt)
            {
                if (e.KeyCode == Keys.Escape)
                {
                    HideSearchField();
                    e.Handled = true;
                }
                else if (char.IsLetterOrDigit((char)e.KeyValue))
                {
                    ShowSearchField();
                    tbSearch.Text += ((char)e.KeyValue).ToString().ToLower();
                    tbSearch.SelectionStart = tbSearch.Text.Length;
                    tbSearch.DeselectAll();
                    e.Handled = true;
                }                
            }
        }

        private void HideSearchField()
        {
            tbSearch.Text = "";
            searchPanel.Visible = false;
            testRunView.Focus();
        }

        private void ShowSearchField()
        {
            searchPanel.Visible = true;
            tbSearch.Focus();
        }

        private void tbSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Control && (char)e.KeyValue == 'F')
            {
                // ctrl+f - select all text
                tbSearch.SelectAll();
                e.Handled = true;
            }
            if (e.KeyCode == Keys.Escape)
            {
                HideSearchField();
                e.Handled = true;
            }
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbSearch.Text.Trim()))
            {
                testRunView.SetSearchString(null);
                return;
            }
            string[] parts = tbSearch.Text.Trim().ToLower().Split(' ');
            testRunView.SetSearchString(parts);
        }
#endregion
    }

    public class TestRunDeletedEventArgs : EventArgs
    {
        public long TestRunDbId;
    }
}
