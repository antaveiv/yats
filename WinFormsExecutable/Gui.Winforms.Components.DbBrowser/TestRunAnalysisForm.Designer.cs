﻿namespace yats.Gui.Winforms.Components.DbBrowser
{
    partial class TestRunAnalysisForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestRunAnalysisForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.testRunTestsView = new yats.Gui.Winforms.Components.DbBrowser.TestRunTestsView();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.btRepeat = new System.Windows.Forms.ToolStripButton();
            this.btCancel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btGotoNextFail = new System.Windows.Forms.ToolStripButton();
            this.btGotoPrevFail = new System.Windows.Forms.ToolStripButton();
            this.btAutoScroll = new System.Windows.Forms.ToolStripButton();
            this.messageView = new yats.Gui.Winforms.Components.DbBrowser.MessageView();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.repeatMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.cancelMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markProblemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.TabContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.closeTabToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllResultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.TabContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.testRunTestsView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.toolStrip);
            this.splitContainer1.Panel2.Controls.Add(this.messageView);
            this.splitContainer1.Panel2.Controls.Add(this.menuStrip);
            this.splitContainer1.Size = new System.Drawing.Size(1112, 531);
            this.splitContainer1.SplitterDistance = 631;
            this.splitContainer1.TabIndex = 0;
            // 
            // testRunTestsView
            // 
            this.testRunTestsView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.testRunTestsView.Location = new System.Drawing.Point(0, 0);
            this.testRunTestsView.Name = "testRunTestsView";
            this.testRunTestsView.Size = new System.Drawing.Size(631, 531);
            this.testRunTestsView.TabIndex = 0;
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btRepeat,
            this.btCancel,
            this.toolStripSeparator1,
            this.btGotoNextFail,
            this.btGotoPrevFail,
            this.btAutoScroll});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(477, 25);
            this.toolStrip.TabIndex = 2;
            this.toolStrip.Text = "toolStrip1";
            this.toolStrip.Visible = false;
            // 
            // btRepeat
            // 
            this.btRepeat.Image = ((System.Drawing.Image)(resources.GetObject("btRepeat.Image")));
            this.btRepeat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btRepeat.Name = "btRepeat";
            this.btRepeat.Size = new System.Drawing.Size(63, 22);
            this.btRepeat.Text = "Repeat";
            this.btRepeat.Click += new System.EventHandler(this.btRepeat_Click);
            // 
            // btCancel
            // 
            this.btCancel.Image = ((System.Drawing.Image)(resources.GetObject("btCancel.Image")));
            this.btCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(63, 22);
            this.btCancel.Text = "Cancel";
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btGotoNextFail
            // 
            this.btGotoNextFail.Image = ((System.Drawing.Image)(resources.GetObject("btGotoNextFail.Image")));
            this.btGotoNextFail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btGotoNextFail.Name = "btGotoNextFail";
            this.btGotoNextFail.Size = new System.Drawing.Size(70, 22);
            this.btGotoNextFail.Text = "Next fail";
            this.btGotoNextFail.Click += new System.EventHandler(this.btGotoNextFail_Click);
            // 
            // btGotoPrevFail
            // 
            this.btGotoPrevFail.Image = ((System.Drawing.Image)(resources.GetObject("btGotoPrevFail.Image")));
            this.btGotoPrevFail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btGotoPrevFail.Name = "btGotoPrevFail";
            this.btGotoPrevFail.Size = new System.Drawing.Size(91, 22);
            this.btGotoPrevFail.Text = "Previous fail";
            this.btGotoPrevFail.Click += new System.EventHandler(this.btGotoPrevFail_Click);
            // 
            // btAutoScroll
            // 
            this.btAutoScroll.CheckOnClick = true;
            this.btAutoScroll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btAutoScroll.Image = ((System.Drawing.Image)(resources.GetObject("btAutoScroll.Image")));
            this.btAutoScroll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btAutoScroll.Name = "btAutoScroll";
            this.btAutoScroll.Size = new System.Drawing.Size(23, 22);
            this.btAutoScroll.Text = "Auto-scroll messages";
            this.btAutoScroll.CheckedChanged += new System.EventHandler(this.btAutoScroll_CheckedChanged);
            // 
            // messageView
            // 
            this.messageView.AutoScroll = true;
            this.messageView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.messageView.Location = new System.Drawing.Point(0, 0);
            this.messageView.Name = "messageView";
            this.messageView.Size = new System.Drawing.Size(477, 531);
            this.messageView.TabIndex = 0;
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.runToolStripMenuItem,
            this.notesToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(477, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip1";
            this.menuStrip.Visible = false;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportLogToolStripMenuItem});
            this.fileToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.MatchOnly;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exportLogToolStripMenuItem
            // 
            this.exportLogToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.exportLogToolStripMenuItem.MergeIndex = 3;
            this.exportLogToolStripMenuItem.Name = "exportLogToolStripMenuItem";
            this.exportLogToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.exportLogToolStripMenuItem.Text = "&Export Log...";
            this.exportLogToolStripMenuItem.Click += new System.EventHandler(this.exportLogToolStripMenuItem_Click);
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.repeatMenuItem,
            this.toolStripSeparator5,
            this.cancelMenuItem});
            this.runToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.runToolStripMenuItem.MergeIndex = 3;
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.runToolStripMenuItem.Text = "&Run";
            // 
            // repeatMenuItem
            // 
            this.repeatMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("repeatMenuItem.Image")));
            this.repeatMenuItem.Name = "repeatMenuItem";
            this.repeatMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.repeatMenuItem.Size = new System.Drawing.Size(129, 22);
            this.repeatMenuItem.Text = "Repeat";
            this.repeatMenuItem.Click += new System.EventHandler(this.btRepeat_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(126, 6);
            // 
            // cancelMenuItem
            // 
            this.cancelMenuItem.Enabled = false;
            this.cancelMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cancelMenuItem.Image")));
            this.cancelMenuItem.Name = "cancelMenuItem";
            this.cancelMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.cancelMenuItem.Size = new System.Drawing.Size(129, 22);
            this.cancelMenuItem.Text = "Cancel";
            this.cancelMenuItem.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // notesToolStripMenuItem
            // 
            this.notesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNoteToolStripMenuItem,
            this.markProblemToolStripMenuItem});
            this.notesToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.notesToolStripMenuItem.MergeIndex = 4;
            this.notesToolStripMenuItem.Name = "notesToolStripMenuItem";
            this.notesToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.notesToolStripMenuItem.Text = "&Notes";
            // 
            // addNoteToolStripMenuItem
            // 
            this.addNoteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("addNoteToolStripMenuItem.Image")));
            this.addNoteToolStripMenuItem.Name = "addNoteToolStripMenuItem";
            this.addNoteToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.addNoteToolStripMenuItem.Text = "Add note...";
            // 
            // markProblemToolStripMenuItem
            // 
            this.markProblemToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("markProblemToolStripMenuItem.Image")));
            this.markProblemToolStripMenuItem.Name = "markProblemToolStripMenuItem";
            this.markProblemToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.markProblemToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.markProblemToolStripMenuItem.Text = "Mark problem";
            this.markProblemToolStripMenuItem.Click += new System.EventHandler(this.markProblemToolStripMenuItem_Click);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "Compressed log|*.ylogz|Yats log files|*.ylog|All files|*.*";
            // 
            // TabContextMenu
            // 
            this.TabContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeTabToolStripMenuItem,
            this.closeAllResultsToolStripMenuItem});
            this.TabContextMenu.Name = "TabContextMenu";
            this.TabContextMenu.Size = new System.Drawing.Size(161, 48);
            // 
            // closeTabToolStripMenuItem
            // 
            this.closeTabToolStripMenuItem.Name = "closeTabToolStripMenuItem";
            this.closeTabToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.closeTabToolStripMenuItem.Text = "Close Tab";
            this.closeTabToolStripMenuItem.Click += new System.EventHandler(this.closeTabToolStripMenuItem_Click);
            // 
            // closeAllResultsToolStripMenuItem
            // 
            this.closeAllResultsToolStripMenuItem.Name = "closeAllResultsToolStripMenuItem";
            this.closeAllResultsToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.closeAllResultsToolStripMenuItem.Text = "Close All Results";
            this.closeAllResultsToolStripMenuItem.Click += new System.EventHandler(this.closeAllResultsToolStripMenuItem_Click);
            // 
            // TestRunAnalysisForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1112, 531);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "TestRunAnalysisForm";
            this.ShowInTaskbar = false;
            this.TabPageContextMenuStrip = this.TabContextMenu;
            this.Text = "TestRunAnalysisForm";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.TabContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private TestRunTestsView testRunTestsView;
        private MessageView messageView;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton btRepeat;
        private System.Windows.Forms.ToolStripButton btCancel;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportLogToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem repeatMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem cancelMenuItem;
        private System.Windows.Forms.ContextMenuStrip TabContextMenu;
        private System.Windows.Forms.ToolStripMenuItem closeTabToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllResultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btGotoNextFail;
        private System.Windows.Forms.ToolStripButton btGotoPrevFail;
        private System.Windows.Forms.ToolStripButton btAutoScroll;
        private System.Windows.Forms.ToolStripMenuItem notesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markProblemToolStripMenuItem;
    }
}