﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Windows.Forms;
using yats.Gui.Winforms.Components.DbBrowser.Properties;
using yats.Utilities;

namespace yats.Gui.Winforms.Components.DbBrowser.MessageDataPanels
{
    public partial class HexView : UserControl
    {
        protected static SerializableDictionary<string, int> activeTabSettings = new SerializableDictionary<string,int>();

        static HexView()
        {
            if (string.IsNullOrEmpty(Settings.Default.HexViewActiveTab) == false)
            {
                activeTabSettings = SerializableDictionary<string, int>.ReadString(Settings.Default.HexViewActiveTab);
            }
        }
        
        string DataType = null;

        public HexView()
        {
            InitializeComponent( );
        }

        public HexView(byte[] bytes, string dataType, string asText) : this()
        {
            UpdateData(bytes, dataType, asText);
        }

        public void UpdateData(byte[] bytes, string dataType, string asText)
        {
            if (string.IsNullOrEmpty(asText) == false)
            {
                stringView.Data = asText;
            }
            else
            {
                this.tabControl.TabPages.Remove(tabPageText);
            }

            this.hexBox.SetBytes(bytes);

            this.DataType = dataType;
            tabControl.SelectedIndexChanged -= this.tabControl_SelectedIndexChanged;
            ApplySavedTabIndex();
            tabControl.SelectedIndexChanged += this.tabControl_SelectedIndexChanged;
        }
                
        private int getActiveTabIndex(String dataType)
        {
            if (activeTabSettings.ContainsKey(dataType))
            {
                return activeTabSettings[dataType];
            }
            return 1;
        }

        public void ApplySavedTabIndex()
        {
            int tabIndex = getActiveTabIndex(DataType);
            if (Settings.Default.RememberHexViewTab && tabIndex < tabControl.TabCount)
            {
                tabControl.SelectedIndex = tabIndex;
            }
        }

        public void TemporarilyOverrideToHex()
        {
            tabControl.SelectedIndexChanged -= this.tabControl_SelectedIndexChanged; // do not remember the change
            tabControl.SelectedIndex = 0;
            tabControl.SelectedIndexChanged += this.tabControl_SelectedIndexChanged;
        }

        public void TemporarilyOverrideToText()
        {
            tabControl.SelectedIndexChanged -= this.tabControl_SelectedIndexChanged;
            tabControl.SelectedIndex = 1;
            tabControl.SelectedIndexChanged += this.tabControl_SelectedIndexChanged;
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Settings.Default.RememberHexViewTab) // TODO this setting is never changed
            {
                activeTabSettings[DataType] = tabControl.SelectedIndex;
                Settings.Default.HexViewActiveTab = activeTabSettings.WriteString();
                Settings.Default.Save();
            }
        }
    }
}
