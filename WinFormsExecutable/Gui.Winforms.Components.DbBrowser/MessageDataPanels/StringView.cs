﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Utilities;
using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using yats.Gui.Winforms.Components.DbBrowser.Properties;

namespace yats.Gui.Winforms.Components.DbBrowser.MessageDataPanels
{
    public partial class StringView : UserControl
    {
        public StringView()
        {
            InitializeComponent( );
            textBox.WordWrap = Settings.Default.StringViewWordWrap;
        }

        public StringView(byte[] bytes, string typeName)
            : this( )
        {
            UpdateValue( bytes, typeName );
        }

        public void UpdateValue(byte[] bytes, string typeName)
        {
            string text = bytes == null? "" : Encoding.UTF8.GetString( bytes );
            UpdateValue( text, typeName );
        }

        public void UpdateValue(string value, string typeName)
        {
            if(this.InvokeRequired)
            {
                BeginInvoke( new MethodInvoker( delegate() { UpdateValue( value, typeName ); } ) );
                return;
            }
            textBox.Text = value;
        }

        public string Data
        {
            set { textBox.Text = value; }
        }

        #region context menu
        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText( textBox.SelectedText );
        }

        private void copyAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText( textBox.Text );
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox.SelectAll( );
        }
        
        private void wordWrapToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.StringViewWordWrap = wordWrapToolStripMenuItem.Checked;
            Settings.Default.Save();
            textBox.WordWrap = wordWrapToolStripMenuItem.Checked;
        }
        #endregion

        public bool ReadOnly
        {
            get 
            { 
                return textBox.ReadOnly; 
            }
            set
            {
                textBox.ReadOnly = value;
            }
        }

        private void contextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string file;
            int lineNumber;
            openInVisualStudioToolStripMenuItem.Enabled = GetSourceCodeLocation(out file, out lineNumber);
        }

        static Regex stackTraceRegex = new Regex(@".*at .* in (.*):line (\d+)");

        private bool GetSourceCodeLocation(out string file, out int lineNumber)
        {
            file = "";
            lineNumber = 0;
            try
            {
                string str = textBox.Lines[textBox.GetLineFromCharIndex(textBox.SelectionStart)];
                Match match = stackTraceRegex.Match(str);
                if (match.Success)
                {
                    file = match.Groups[1].Value;
                    lineNumber = int.Parse(match.Groups[2].Value);
                    return File.Exists(file);
                }
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex);
            }
            return false; 
        }

        private void openInVisualStudioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string file;
                int lineNumber;
                if (GetSourceCodeLocation(out file, out lineNumber))
                {
                    FileUtil.OpenInVisualStudio(file, lineNumber);
                }
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex);
            }
        }
    }
}
