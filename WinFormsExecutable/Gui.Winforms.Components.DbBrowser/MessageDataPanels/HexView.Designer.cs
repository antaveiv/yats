﻿namespace yats.Gui.Winforms.Components.DbBrowser.MessageDataPanels
{
    partial class HexView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new yats.Gui.Winforms.Components.Util.TabControlAdv();
            this.tabPageHex = new System.Windows.Forms.TabPage();
            this.tabPageText = new System.Windows.Forms.TabPage();
            this.stringView = new yats.Gui.Winforms.Components.DbBrowser.MessageDataPanels.StringView();
            this.hexBox = new yats.Gui.Winforms.Components.Util.ExtendedHexEditor();
            this.tabControl.SuspendLayout();
            this.tabPageHex.SuspendLayout();
            this.tabPageText.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageHex);
            this.tabControl.Controls.Add(this.tabPageText);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(658, 266);
            this.tabControl.TabIndex = 3;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabPageHex
            // 
            this.tabPageHex.Controls.Add(this.hexBox);
            this.tabPageHex.Location = new System.Drawing.Point(4, 22);
            this.tabPageHex.Name = "tabPageHex";
            this.tabPageHex.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageHex.Size = new System.Drawing.Size(650, 240);
            this.tabPageHex.TabIndex = 0;
            this.tabPageHex.Text = "Hex view";
            this.tabPageHex.UseVisualStyleBackColor = true;
            // 
            // tabPageText
            // 
            this.tabPageText.Controls.Add(this.stringView);
            this.tabPageText.Location = new System.Drawing.Point(4, 22);
            this.tabPageText.Name = "tabPageText";
            this.tabPageText.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageText.Size = new System.Drawing.Size(650, 240);
            this.tabPageText.TabIndex = 1;
            this.tabPageText.Text = "Text view";
            this.tabPageText.UseVisualStyleBackColor = true;
            // 
            // stringView
            // 
            this.stringView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stringView.Location = new System.Drawing.Point(3, 3);
            this.stringView.Name = "stringView";
            this.stringView.ReadOnly = true;
            this.stringView.Size = new System.Drawing.Size(644, 234);
            this.stringView.TabIndex = 0;
            // 
            // hexBox
            // 
            this.hexBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hexBox.Location = new System.Drawing.Point(3, 3);
            this.hexBox.Name = "hexBox";
            this.hexBox.ReadOnly = true;
            this.hexBox.Size = new System.Drawing.Size(644, 234);
            this.hexBox.TabIndex = 0;
            // 
            // HexView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl);
            this.Name = "HexView";
            this.Size = new System.Drawing.Size(658, 266);
            this.tabControl.ResumeLayout(false);
            this.tabPageHex.ResumeLayout(false);
            this.tabPageText.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private yats.Gui.Winforms.Components.Util.TabControlAdv tabControl;
        private System.Windows.Forms.TabPage tabPageHex;
        private System.Windows.Forms.TabPage tabPageText;
        private StringView stringView;
        private yats.Gui.Winforms.Components.Util.ExtendedHexEditor hexBox;

    }
}
