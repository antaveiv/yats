﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using yats.Gui.Winforms.Components.DbBrowser.MessageDataPanels;
using yats.Gui.Winforms.Components.DbBrowser.Properties;
using yats.Gui.Winforms.Components.Util;
using yats.Logging.Interface;
using yats.Utilities;

namespace yats.Gui.Winforms.Components.DbBrowser
{
    public partial class MessageView : UserControl
    {
        internal MessageViewTreeModel model;

        StringView stringView = new StringView( );
        HexView hexView = new HexView();

        public override bool AutoScroll
        {
            get { return Settings.Default.MessageAutoscroll; }
            set
            {
                if (Settings.Default.MessageAutoscroll != value)
                {
                    Settings.Default.MessageAutoscroll = value;
                    Settings.Default.Save();
                    if (value)
                    {
                        UnselectAll();
                    }
                }
            }
        }

        private void UnselectAll()
        {
            foreach (int idx in messageList.SelectedIndices) // can not use foreach when list is in virtual mode
            {
                messageList.Items[idx].Selected = false;
            }
        }

        public MessageView()
        {
            InitializeComponent();
            stringView.Dock = DockStyle.Fill;
            hexView.Dock = DockStyle.Fill;
            ListViewHelper.EnableDoubleBuffer(messageList);
            noToolStripMenuItem.Checked = Settings.Default.ShowNumberColumn;
            timeToolStripMenuItem.Checked = Settings.Default.ShowTimeColumn;
            loggerToolStripMenuItem.Checked = Settings.Default.ShowLoggerColumn;
            levelToolStripMenuItem.Checked = Settings.Default.ShowLevelColumn;
        }

        internal void SetDataSource(CachedLog dataSource, LoggerFilter filter)
        {
            if (IsDisposed)
            {
                return;
            }

            this.model = new MessageViewTreeModel(dataSource, null, filter);
            this.model.Updated += new EventHandler( model_Updated );
            messageList.VirtualListSize = model.Size;
        }

        void model_Updated(object sender, EventArgs e)
        {
            if (IsDisposed)
            {
                return;
            }

            if(this.InvokeRequired)
            {
                BeginInvoke( new MethodInvoker( delegate() { model_Updated(sender, e); } ) );
                return;
            }  

            messageList.VirtualListSize = model.Size;
            if (AutoScroll && messageList.Items.Count > 0 && messageList.SelectedIndices.Count == 0) // autoscroll when nothing is selected
            {
                messageList.Items[messageList.Items.Count - 1].EnsureVisible();
            }
        }

        internal void SetSelectedSteps(IList<TestStepNode> selectedSteps)
        {
            if (IsDisposed)
            {
                return;
            }

            if(this.InvokeRequired)
            {
                BeginInvoke( new MethodInvoker( delegate() { SetSelectedSteps( selectedSteps ); } ) );
                return;
            }  

            this.model.SetSelectedSteps(selectedSteps);
            if(messageList.VirtualListSize == model.Size)
            {
                messageList.VirtualListSize = 0; // if VirtualListSize does not change, the list is not refreshed
            }
            messageList.VirtualListSize = model.Size;
            if (model.Size > 0 && !AutoScroll)
            {
                messageList.Items[0].Selected = true;
            }                
        }
                
        private void messageList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {                
                splitContainer.Panel2.Controls.Clear( );
                if(messageList.SelectedIndices.Count != 1)
                {
                    return;
                }

                var node = model.GetItem( messageList.SelectedIndices[0] );
                if(node == null)
                {
                    return;
                }

                byte[] binary = node.BinaryData;
                string binaryDataType = "null";
                if (string.IsNullOrEmpty(node.BinaryDataType) == false)
                {
                    binaryDataType = node.BinaryDataType;
                }

                if (binary == null)
                {
                    binary = Encoding.ASCII.GetBytes(node.Message);
                }

                if(node.BinaryData == null)
                {
                    string message = node.Message;

                    message = message.NormalizeNewLines();//E.g. SslCommand returns a multi-line string from Linux machine while Yats is running in Windows. Replace line end characters so that the string view displays it correctly
                    
                    hexView.UpdateData(binary, binaryDataType, message);
                    splitContainer.Panel2.Controls.Add( hexView );
                    return;
                }

                if(node.BinaryDataType == "Exception")
                {
                    stringView.UpdateValue(node.BinaryData, binaryDataType);
                    splitContainer.Panel2.Controls.Add( stringView );
                }
                else //if (node.BinaryDataType == typeof(string).AssemblyQualifiedName || node.BinaryDataType == "string" || node.BinaryDataType == "byte[]")
                {
                    string asText = node.Message;
                    if (string.IsNullOrEmpty(asText))
                    {
                        try
                        {
                            asText = Encoding.UTF8.GetString(node.BinaryData).NormalizeNewLines();
                        }
                        catch { }
                    }
                    else
                    {
                        asText = asText.NormalizeNewLines();
                    }
                    
                    hexView.UpdateData(node.BinaryData, binaryDataType, asText);
                    if (node.BinaryDataType != "byte[]")
                    {
                        hexView.TemporarilyOverrideToText();
                    }
                    splitContainer.Panel2.Controls.Add(hexView);
                }
            }
            catch { }
            
        }

        // called with a keyboard shortcut when a problem is observed. If a single message is selected at the moment, add a note to it. If none or more than one are selected, add to last visible message
        internal void MarkProblem()
        {
            LogMessage logMessage;
            if (messageList.SelectedIndices.Count == 1)
            {
                logMessage = model.GetItem(messageList.SelectedIndices[0]);
            }
            else
            {
                logMessage = model.GetItem(model.Size - 1);
            }
            if (logMessage == null || logMessage.Note != null) // perhaps already contains a note, don't overwrite
            {
                return;
            }
            logMessage.Note = LogMessage.MESSAGE_BUG;
            model_Updated(this, null);
            model.DataSource.UpdateMessage(logMessage);
        }

        private void messageList_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            var message = model.GetItem(e.ItemIndex);
            if(message == null){
                CrashLog.Write("messageList_RetrieveVirtualItem: index=" + e.ItemIndex + ", model size: " + model.Size);
                e.Item = new ListViewItem("wtf");
                e.Item.SubItems.AddRange(new string[] { "", "", "", "" });
                return;
            }

            List<string> items = new List<string>();
            if (messageList.Columns.Contains(columnHeader1))
            {
                items.Add(message.SequenceNumber.ToString());
            }
            if (messageList.Columns.Contains(columnHeader2))
            {
                items.Add(timeReference.HasValue ? message.Timestamp.Subtract(timeReference.Value).ToShortFriendlyDisplay2(2) : message.Timestamp.ToString("HH:mm:ss.fff"));
            }
            if (messageList.Columns.Contains(columnHeader3))
            {
                items.Add(message.Logger);
            }
            if (messageList.Columns.Contains(columnHeader4))
            {
                items.Add(message.Level);
            }
            items.Add((message.Message != null) ? message.Message.ReplaceControlCharacters(" ") : string.Empty );
            e.Item = new ListViewItem(items.ToArray());
            if (message.Note == "#bug")
            {
                e.Item.ImageIndex = 1;
            }
            else if (message.Note != null)
            {
                e.Item.ImageIndex = 0;
            }

            return;

/*            e.Item = new ListViewItem(message.SequenceNumber.ToString()); 

            string[] subItems = new string[] {
                timeReference.HasValue? message.Timestamp.Subtract(timeReference.Value).ToShortFriendlyDisplay() : message.Timestamp.ToString( "HH:mm:ss.fff" ),
                message.Logger, 
                message.Level, 
                (message.Message != null)? message.Message.ReplaceControlCharacters( " " ) : string.Empty };
#if DEBUG
            foreach(var s in subItems)
            {
                if(s == null)
                {
                    CrashLog.Write( "messageList_RetrieveVirtualItem: subItem is null" );
                }
            }
#endif
            e.Item.SubItems.AddRange(subItems);
*/
        }

        #region Copy
        private void copyFullToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string result = "";
            foreach(var index in messageList.SelectedIndices)
            {
                var message = model.GetItem( (int)index );
                result = result + string.Format( "{0}\t{1}\t{2}\t{3}\t{4}{5}", message.SequenceNumber, message.Timestamp.ToString( "HH:mm:ss" ), message.Logger, message.Level, message.Message, Environment.NewLine );
            }
            if (string.IsNullOrEmpty(result)==false)
            {
                Clipboard.SetText(result);
            }
        }
        private void copyMessagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string result = "";
            foreach (var index in messageList.SelectedIndices)
            {
                var message = model.GetItem((int)index);
                result = result + string.Format("{0}{1}", message.Message, Environment.NewLine);
            }
            if (string.IsNullOrEmpty(result) == false)
            {
                Clipboard.SetText(result);
            }
        }
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            copyFullToolStripMenuItem.Enabled = messageList.SelectedIndices.Count > 0;
            copyMessagesToolStripMenuItem.Enabled = messageList.SelectedIndices.Count > 0;
            setTimeReferenceToolStripMenuItem.Visible = messageList.SelectedIndices.Count == 1;
            clearTimeReferenceToolStripMenuItem.Visible = timeReference.HasValue;
            if (messageList.SelectedIndices.Count == 1){
                var node = model.GetItem(messageList.SelectedIndices[0]);
                hideMessagesToolStripMenuItem.Visible = true;
                hideMessagesToolStripMenuItem.Text = string.Format("Hide '{0}' messages", node.Logger);
            }
            else
            {
                hideMessagesToolStripMenuItem.Visible = false;
            }
            showAllMessagesToolStripMenuItem.Visible = model.Filter.IsFilterConfigured();
        }
        #endregion

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < messageList.Items.Count; i++ ) // can not use foreach when list is in virtual mode
            {
                messageList.Items[i].Selected = true;
            }
        }

        protected DateTime? timeReference = null;
        private void setTimeReferenceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (messageList.SelectedIndices.Count != 1)
            {
                return;
            }

            var node = model.GetItem(messageList.SelectedIndices[0]);
            if (node == null)
            {
                return;
            }
            timeReference = node.Timestamp;
            messageList.Invalidate();
        }

        private void clearTimeReferenceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timeReference = null;
            messageList.Invalidate();
        }

        private void noToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (noToolStripMenuItem.Checked)
            {
                if (!messageList.Columns.Contains(columnHeader1))
                {
                    messageList.Columns.Insert(0, columnHeader1);
                }
            }
            else
            {
                messageList.Columns.Remove(columnHeader1);
            }
            Settings.Default.ShowNumberColumn = noToolStripMenuItem.Checked;
            Settings.Default.Save();
        }

        private void timeToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (timeToolStripMenuItem.Checked)
            {
                if (!messageList.Columns.Contains(columnHeader2))
                {
                    int idx = 0;
                    if (messageList.Columns.Contains(columnHeader1)) idx++;
                    messageList.Columns.Insert(idx, columnHeader2);
                }
            }
            else
            {
                messageList.Columns.Remove(columnHeader2);
            }
            Settings.Default.ShowTimeColumn = timeToolStripMenuItem.Checked;
            Settings.Default.Save();
        }

        private void loggerToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (loggerToolStripMenuItem.Checked)
            {
                if (!messageList.Columns.Contains(columnHeader3))
                {
                    int idx = 0;
                    if (messageList.Columns.Contains(columnHeader1)) idx++;
                    if (messageList.Columns.Contains(columnHeader2)) idx++;
                    messageList.Columns.Insert(idx, columnHeader3);
                }
            }
            else
            {
                messageList.Columns.Remove(columnHeader3);
            }
            Settings.Default.ShowLoggerColumn = loggerToolStripMenuItem.Checked;
            Settings.Default.Save();
        }

        private void levelToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (levelToolStripMenuItem.Checked)
            {
                if (!messageList.Columns.Contains(columnHeader4))
                {
                    int idx = 0;
                    if (messageList.Columns.Contains(columnHeader1)) idx++;
                    if (messageList.Columns.Contains(columnHeader2)) idx++;
                    if (messageList.Columns.Contains(columnHeader3)) idx++;
                    messageList.Columns.Insert(idx, columnHeader4);
                }
            }
            else
            {
                messageList.Columns.Remove(columnHeader4);
            }
            Settings.Default.ShowLevelColumn = levelToolStripMenuItem.Checked;
            Settings.Default.Save();
        }

        private void hideMessagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = model.GetItem(messageList.SelectedIndices[0]);
            model.Filter.Set(node.Logger, "OFF");
        }

        private void showAllMessagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            model.Filter.SetAll("DEBUG");
        }

        public event EventHandler<LogMessageAddedEventArgs> OnTestStepFind;
        private void findTestStepToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = model.GetItem(messageList.SelectedIndices[0]);
            if (node == null)
            {
                return;
            }
            if (OnTestStepFind == null)
            {
                return;
            }
            OnTestStepFind(this, new LogMessageAddedEventArgs(-1, node));
        }

        private void messageList_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                UnselectAll();
                e.Handled = true;
            }
        }
    }
}
