﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Linq;
using Aga.Controls.Tree;
using WeifenLuo.WinFormsUI.Docking;
using System.Windows.Forms;

namespace yats.Gui.Winforms.Components.DbBrowser
{
    public partial class LogFilterEditor : DockContent
    {
        LoggerFilter filter;
        LoggerFilter Filter
        {
            get { return filter; }
            set
            {
                if (filter != null)
                {
                    filter.OnFilterChanged -= UpdateTreeModel;
                }
                filter = value;
                if (filter != null)
                {
                    filter.OnFilterChanged += UpdateTreeModel;
                }
                UpdateTreeModel(this, null);
            }
        }

        public LogFilterEditor()
        {
            InitializeComponent();
            Filter = LoggerFilterSingleton.Instance;            
        }

        public LogFilterEditor(LoggerFilter filter)
        {
            InitializeComponent();
            Filter = filter;
        }

        SortedTreeModel model;

        void UpdateTreeModel(object sender, EventArgs e)
        {
            var nodes = Filter.GetLoggers().Select(logger => new LogFilterTreeNode(filter, logger)).ToList();
            model = new SortedTreeModel(new ListModel(nodes));
            model.Comparer = new NodeComparer();
            this.tree.Model = model;    
        }

        private class NodeComparer : System.Collections.IComparer
        {
            public int Compare(object x, object y)
            {
                return (x as LogFilterTreeNode).Logger.CompareTo((y as LogFilterTreeNode).Logger); // by date, descending
            }
        }

        internal class LogFilterTreeNode : Aga.Controls.Tree.Node<LogFilterTreeNode>
        {
            LoggerFilter filter;
            public string Logger
            {
                get;
                set;
            }

            public string Level
            {
                get
                {
                    return filter.Get(Logger);
                }
                set
                {
                    filter.Set(Logger, value);                    
                }
            }

            public LogFilterTreeNode(LoggerFilter filter, string logger)
            {
                this.filter = filter;
                this.Logger = logger;                
            }

            public LogFilterTreeNode()
            {
            }
        }

        private void resetAllToDEBUGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var logger in Filter.GetLoggers())
            {
                Filter.Set(logger, "DEBUG");
            }
        }

        private void tree_NodeMouseDoubleClick(object sender, TreeNodeAdvMouseEventArgs e)
        {
            LogFilterTreeNode node = e.Node.Tag as LogFilterTreeNode;
            if (node == null)
            {
                return;
            }
            if (node.Level == "OFF")
            {
                node.Level = "DEBUG";
            }
            else 
            {
                node.Level = "OFF";
            }
        }
    }
}
