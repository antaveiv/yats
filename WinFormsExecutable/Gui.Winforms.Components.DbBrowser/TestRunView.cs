﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Aga.Controls.Tree;
using yats.Logging.Interface;
using yats.Logging.Util;
using System.Threading.Tasks;
using yats.Utilities;
using System.Collections;

namespace yats.Gui.Winforms.Components.DbBrowser
{
    partial class TestRunView : UserControl
    {
        protected ListModel innerModel;
        protected SortedTreeModel sortedModel;

        public TestRunView()
        {
            InitializeComponent();
        }

        protected class TestRunTreeNode : Aga.Controls.Tree.Node<TestRunTreeNode>
        {
            internal TestRunLogInfo testRunLogInfo;
            public DateTime TestTime
            {
                get { return testRunLogInfo.StartTime; }
            }
            
            public string Date
            {
                get { return testRunLogInfo.StartTime.ToString("g"); }
            }

            public string Name
            {
                get { return testRunLogInfo.Name; }
            }

            public string Description
            {
                get { return testRunLogInfo.Description; }
            }

            public int Size
            {
                get { return testRunLogInfo.Size; }
            }

            public TestRunTreeNode(TestRunLogInfo testRunLogInfo)
            {
                this.testRunLogInfo = testRunLogInfo;
            }

            public TestRunTreeNode() { 
            }
        }

        async Task ReadTestRunList()
        {
            await Task.Run(() =>
            {
                try
                {
                    testRunList.Clear();
                    if (storage == null)
                    {
                        return;
                    }
                                        
                    int maxProgress = (int)storage.GetStoredLogCount();
                    int progress = 0;
                    foreach (var log in storage.GetStoredLogs())
                    {
                        testRunList.Add(new TestRunTreeNode(log));
                        progress++;
                        BeginInvoke(new MethodInvoker(delegate () {
                            progressBar.Maximum = maxProgress;
                            progressBar.Value = Math.Min(testRunList.Count, progressBar.Maximum);
                        }));                        
                    }
                }
                catch (Exception ex)
                {
                    CrashLog.Write(ex);
                }
                finally
                {
                    
                }
            });
        }

        protected List<TestRunTreeNode> testRunList = new List<TestRunTreeNode>( );
        protected ILogStorage storage;
        public ILogStorage Storage
        {
            get { return storage; }
        }

        private Task ReadTestRunListTask;
        
        internal async void SetStorageAccess(ILogStorage storage)
        {
            this.storage = storage;
            if (storage != null)
            {
                storage.LogAdded += AddTestRun;
                storage.LogRemoved += RemoveTestRun;
                storage.LogChanged += LogChanged;
            }

            progressBar.Visible = true;
            tree.Visible = false;
            
            ReadTestRunListTask = ReadTestRunList();
            await ReadTestRunListTask;
            //await Task.WhenAll(ReadTestRunListTask);

            this.innerModel = new ListModel( testRunList );
            FilteredTreeModel filter = new FilteredTreeModel(innerModel);
            filter.FilterMatch = (object x) =>
            {
                if (SearchStrings != null && SearchStrings.Count() > 0)
                {
                    string description = (x as TestRunTreeNode).Description;
                    if (string.IsNullOrEmpty(description) == false)
                    {
                        if ((SearchStrings.Where(s => description.ToLower().Contains(s)).Count() > 0))
                        {
                            return true;
                        }
                    }
                    string name = (x as TestRunTreeNode).Name;
                    if (string.IsNullOrEmpty(name) == false)
                    {
                        if ((SearchStrings.Where(s => name.ToLower().Contains(s)).Count() > 0))
                        {
                            return true;
                        }
                    }
                    return false;
                }
                return true;
            };
            this.sortedModel = new Aga.Controls.Tree.SortedTreeModel( filter );
            this.sortedModel.Comparer = new DateComparer( );

            tree.Model = sortedModel;

            progressBar.Visible = false;
            tree.Visible = true;
        }
        
        private class DateComparer : System.Collections.IComparer
        {
            public int Compare(object x, object y)
            {
                return (x as TestRunTreeNode).TestTime.CompareTo( (y as TestRunTreeNode).TestTime) * -1; // by date, descending
            }
        }

        public event EventHandler TreeSelectionChanged;
        private void tree_SelectionChanged(object sender, EventArgs e)
        {
            if(TreeSelectionChanged != null)
            {
                TreeSelectionChanged( sender, e );
            }
        }

        // refresh test run list when the DB appender reports ended test run. Previously, the testRunner event caused the refresh but the DB write may not be finished at that time
        internal void AddTestRun(object sender, TestRunLogInfoEventArgs e)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { AddTestRun(sender, e); }));
                return;
            }
            try
            {
                innerModel.Add( new TestRunTreeNode( e.TestRunLogInfo ) );
            }
            catch (Exception ex)
            {
                yats.Utilities.CrashLog.Write(ex);
            }
        }

        internal void LogChanged(object sender, TestRunLogInfoEventArgs e)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { LogChanged(sender, e); }));
                return;
            }
            try
            {
                tree.Invalidate();
            }
            catch (Exception ex)
            {
                yats.Utilities.CrashLog.Write(ex);
            }
        }
                        
        internal void RemoveTestRun(object sender, TestRunLogInfoEventArgs e)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { RemoveTestRun(sender, e); }));
                return;
            }
            try
            {
                innerModel.Remove(testRunList.First(x => x.testRunLogInfo == e.TestRunLogInfo));
            }
            catch (Exception ex)
            {
                yats.Utilities.CrashLog.Write( ex );
            }
        }        

        public List<TestRunLogInfo> SelectedTestRunLogs
        {
            get
            {
                List<TestRunLogInfo> result = new List<TestRunLogInfo>();
                foreach(var node in tree.SelectedNodes)
                {
                    result.Add( (node.Tag as TestRunTreeNode).testRunLogInfo );
                }
                return result;
            }
        }

        public delegate void TestRunLogEventDelegate(TestRunLogInfo testRunLogInfo);
        public event TestRunLogEventDelegate TestRunDoubleClick;

        private void tree_NodeMouseDoubleClick(object sender, TreeNodeAdvMouseEventArgs e)
        {
            try
            {
                if(TestRunDoubleClick != null)
                {
                    TestRunDoubleClick( (e.Node.Tag as TestRunTreeNode).testRunLogInfo);
                }
            }
            catch { }
        }

        private string m_nodeToolTip;

        private void tree_MouseMove(object sender, MouseEventArgs e)
        {
            // Get the node under the mouse pointer
            TreeNodeAdv node = this.tree.GetNodeAt(tree.PointToClient(MousePosition));

            string newToolTip = string.Empty;

            if (node != null && node.Tag != null)
            {
                newToolTip = (node.Tag as TestRunTreeNode).Description;
            }

            // Text changed?
            if (m_nodeToolTip != newToolTip)
            {
                this.toolTip.SetToolTip(this.tree, newToolTip);

                // Hide the tooltip in order to refresh.
                // If hiding is not done, the tooltip refreshes immediately.
                // Oherwise, it is shown again after the delay configured in tooltip.InitialDelay

                this.toolTip.Hide(this.tree);

                m_nodeToolTip = newToolTip;
            }
        }

        public event EventHandler<KeyEventArgs> TreeKeyUp;

        private void tree_KeyUp(object sender, KeyEventArgs e)
        {
            if (TreeKeyUp != null)
            {
                TreeKeyUp(this, e);
            }
        }

        protected string[] SearchStrings;
        internal void SetSearchString(string[] searchStrings)
        {
            this.SearchStrings = searchStrings;
            this.innerModel.Refresh();
        }
    }

    public class FilteredTreeModel : TreeModelBase
    {
        private ITreeModel _innerModel;
        public ITreeModel InnerModel
        {
            get { return _innerModel; }
        }

        private Predicate<object> filterMatch;
        public Predicate<object> FilterMatch
        {
            get { return filterMatch; }
            set
            {
                filterMatch = value;
                OnStructureChanged(new TreePathEventArgs(TreePath.Empty));
            }
        }

        public FilteredTreeModel(ITreeModel innerModel)
        {
            _innerModel = innerModel;
            _innerModel.NodesChanged += new EventHandler<TreeModelEventArgs>(_innerModel_NodesChanged);
            _innerModel.NodesInserted += new EventHandler<TreeModelEventArgs>(_innerModel_NodesInserted);
            _innerModel.NodesRemoved += new EventHandler<TreeModelEventArgs>(_innerModel_NodesRemoved);
            _innerModel.StructureChanged += new EventHandler<TreePathEventArgs>(_innerModel_StructureChanged);
        }

        void _innerModel_StructureChanged(object sender, TreePathEventArgs e)
        {
            OnStructureChanged(e);
        }

        void _innerModel_NodesRemoved(object sender, TreeModelEventArgs e)
        {
            OnStructureChanged(new TreePathEventArgs(e.Path));
        }

        void _innerModel_NodesInserted(object sender, TreeModelEventArgs e)
        {
            OnStructureChanged(new TreePathEventArgs(e.Path));
        }

        void _innerModel_NodesChanged(object sender, TreeModelEventArgs e)
        {
            OnStructureChanged(new TreePathEventArgs(e.Path));
        }

        public override IEnumerable GetChildren(TreePath treePath)
        {
            if (FilterMatch != null)
            {
                ArrayList list = new ArrayList();
                IEnumerable res = InnerModel.GetChildren(treePath);
                if (res != null)
                {
                    foreach (object obj in res)
                    {
                        if (FilterMatch(obj))
                        {
                            list.Add(obj);
                        }
                    }
                    return list;
                }
                else
                    return null;
            }
            else
                return InnerModel.GetChildren(treePath);
        }

        public override bool IsLeaf(TreePath treePath)
        {
            return InnerModel.IsLeaf(treePath);
        }
    }
}
