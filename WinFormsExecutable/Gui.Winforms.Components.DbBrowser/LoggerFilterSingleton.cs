﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace yats.Gui.Winforms.Components.DbBrowser
{
    public class LoggerFilterSingleton
    {
        private LoggerFilterSingleton()
        {
        }

        private static LoggerFilter s_instance;
        
        //TODO refactor singleton
        public static LoggerFilter Instance
        {
            get
            {
                if (s_instance == null)
                {
                    s_instance = new LoggerFilter();
                }
                return s_instance;
            }
        }
    }
}
