﻿namespace yats.Gui.Winforms.Components.DbBrowser
{
    partial class LogFilterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logFilterEditor1 = new yats.Gui.Winforms.Components.DbBrowser.LogFilterEditor();
            this.SuspendLayout();
            // 
            // logFilterEditor1
            // 
            this.logFilterEditor1.Location = new System.Drawing.Point(12, 12);
            this.logFilterEditor1.Name = "logFilterEditor1";
            this.logFilterEditor1.Size = new System.Drawing.Size(466, 204);
            this.logFilterEditor1.TabIndex = 0;
            // 
            // LogFilterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 235);
            this.Controls.Add(this.logFilterEditor1);
            this.Name = "LogFilterForm";
            this.ShowInTaskbar = false;
            this.Text = "LogFilterForm";
            this.ResumeLayout(false);

        }

        #endregion

        private LogFilterEditor logFilterEditor1;
    }
}