﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using yats.Logging.Interface;

namespace yats.Gui.Winforms.Components.DbBrowser
{    
    internal class MessageViewTreeModel 
    {
        internal CachedLog DataSource;
        IList<TestStepNode> SelectedSteps;
        List<LogMessage> filteredMessages = new List<LogMessage>();
        internal ILoggerFilter Filter;

        public event EventHandler Updated;

        public MessageViewTreeModel(CachedLog dataSource, IList<TestStepNode> selectedSteps, ILoggerFilter filter)
        {
            this.DataSource = dataSource;
            dataSource.MessageAdded += dataSource_MessageAdded;
            SetFilter(filter);
            SetSelectedSteps( selectedSteps );            
        }

        internal void SetFilter(ILoggerFilter filter)
        {
            this.Filter = filter;
            filter.OnFilterChanged += new EventHandler(filter_OnFilterChanged);
        }

        void filter_OnFilterChanged(object sender, EventArgs e)
        {
            SetSelectedSteps(SelectedSteps);
        }

        internal void SetSelectedSteps(IList<TestStepNode> steps)
        {
            this.SelectedSteps = steps;
            this.filteredMessages.Clear( );

            try
            {
                if (SelectedSteps == null || SelectedSteps.Count == 0) // filter is not set
                {
                    var messages = DataSource.Messages;
                    foreach (var msg in messages)
                    {
                        if (Filter == null || Filter.IsEnabled(msg.Logger, msg.Level))
                        {
                            filteredMessages.Add(msg);
                        }
                    }
                }
                else
                {
                    var messages = DataSource.Messages;
                    foreach (var msg in messages)
                    {
                        foreach (var f in SelectedSteps) // check if matches any filter step
                        {
                            if (string.IsNullOrEmpty(f.FullPath) == false && LogMessage.PathMatches(f.FullPath, msg.Path) && Filter.IsEnabled(msg.Logger, msg.Level))
                            {
                                filteredMessages.Add(msg);
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                yats.Utilities.CrashLog.Write(ex);
            }
            if(Updated != null)
            {
                Updated( this, null );
            }
        }

        void dataSource_MessageAdded (object sender, LogMessageAddedEventArgs e)
        {
            if(SelectedSteps == null || SelectedSteps.Count == 0) // filter is not set
            {
            if (Filter.IsEnabled (e.Message.Logger, e.Message.Level))
                {
                filteredMessages.Add (e.Message);
                    if (Updated != null)
                    {
                        Updated(this, null);
                    }
                }
                return;
            }
            
            foreach(var filterItem in SelectedSteps) // check if matches any filter step
            {
            if (string.IsNullOrEmpty (filterItem.FullPath) == false && e.Message.Path.StartsWith (filterItem.FullPath) && Filter.IsEnabled (e.Message.Logger, e.Message.Level))
                {
                filteredMessages.Add (e.Message);
                    if(Updated != null)
                    {
                        Updated( this, null );
                    }
                    return;
                }
            }
        }

        public int Size
        {
            get
            {
                return filteredMessages.Count;
            }
        }

        internal LogMessage GetItem(int index)
        {
            if(index < filteredMessages.Count)
            {
                return filteredMessages[index];
            }
            return null;
        }

        /// <summary>
        /// text search 
        /// </summary>
        /// <param name="searchFor">Text to search for. Case insensitive</param>
        /// <param name="startIndex">Entry index to start the search from (0-from start)</param>
        /// <returns>Index of found message or -1</returns>
        internal int FindNext(string searchFor, int startIndex)
        {
            searchFor = searchFor.ToLower();
            for(int i = startIndex; i < filteredMessages.Count; i++ )
            {
                if(filteredMessages[i].Message.ToLower( ).Contains( searchFor ))
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
