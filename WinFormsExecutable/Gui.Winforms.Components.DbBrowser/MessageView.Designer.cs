﻿namespace yats.Gui.Winforms.Components.DbBrowser
{
    partial class MessageView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageView));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.messageList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyFullToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyMessagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.columnsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.noToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loggerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.levelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.setTimeReferenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearTimeReferenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.hideMessagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showAllMessagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findTestStepToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.treeColumn1 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumn2 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumn3 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumn4 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumn5 = new Aga.Controls.Tree.TreeColumn();
            this.nodeIntegerTextBox1 = new Aga.Controls.Tree.NodeControls.NodeIntegerTextBox();
            this.nodeTextBox1 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.nodeTextBox2 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.nodeTextBox3 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.nodeTextBox4 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.messageList);
            this.splitContainer.Size = new System.Drawing.Size(1019, 634);
            this.splitContainer.SplitterDistance = 434;
            this.splitContainer.TabIndex = 0;
            // 
            // messageList
            // 
            this.messageList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.messageList.ContextMenuStrip = this.contextMenuStrip1;
            this.messageList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.messageList.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageList.FullRowSelect = true;
            this.messageList.HideSelection = false;
            this.messageList.Location = new System.Drawing.Point(0, 0);
            this.messageList.Name = "messageList";
            this.messageList.Size = new System.Drawing.Size(1019, 434);
            this.messageList.SmallImageList = this.imageList1;
            this.messageList.TabIndex = 0;
            this.messageList.UseCompatibleStateImageBehavior = false;
            this.messageList.View = System.Windows.Forms.View.Details;
            this.messageList.VirtualMode = true;
            this.messageList.RetrieveVirtualItem += new System.Windows.Forms.RetrieveVirtualItemEventHandler(this.messageList_RetrieveVirtualItem);
            this.messageList.SelectedIndexChanged += new System.EventHandler(this.messageList_SelectedIndexChanged);
            this.messageList.KeyUp += new System.Windows.Forms.KeyEventHandler(this.messageList_KeyUp);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "No.";
            this.columnHeader1.Width = 55;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Time";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Logger";
            this.columnHeader3.Width = 160;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Level";
            this.columnHeader4.Width = 50;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Message";
            this.columnHeader5.Width = 500;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAllToolStripMenuItem,
            this.copyFullToolStripMenuItem,
            this.copyMessagesToolStripMenuItem,
            this.toolStripSeparator1,
            this.columnsToolStripMenuItem,
            this.toolStripSeparator2,
            this.setTimeReferenceToolStripMenuItem,
            this.clearTimeReferenceToolStripMenuItem,
            this.toolStripSeparator3,
            this.hideMessagesToolStripMenuItem,
            this.showAllMessagesToolStripMenuItem,
            this.findTestStepToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(199, 220);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.selectAllToolStripMenuItem.Text = "Select all";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // copyFullToolStripMenuItem
            // 
            this.copyFullToolStripMenuItem.Name = "copyFullToolStripMenuItem";
            this.copyFullToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.copyFullToolStripMenuItem.Text = "Copy full info";
            this.copyFullToolStripMenuItem.Click += new System.EventHandler(this.copyFullToolStripMenuItem_Click);
            // 
            // copyMessagesToolStripMenuItem
            // 
            this.copyMessagesToolStripMenuItem.Name = "copyMessagesToolStripMenuItem";
            this.copyMessagesToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyMessagesToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.copyMessagesToolStripMenuItem.Text = "Copy messages";
            this.copyMessagesToolStripMenuItem.Click += new System.EventHandler(this.copyMessagesToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(195, 6);
            // 
            // columnsToolStripMenuItem
            // 
            this.columnsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.noToolStripMenuItem,
            this.timeToolStripMenuItem,
            this.loggerToolStripMenuItem,
            this.levelToolStripMenuItem});
            this.columnsToolStripMenuItem.Name = "columnsToolStripMenuItem";
            this.columnsToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.columnsToolStripMenuItem.Text = "Columns";
            // 
            // noToolStripMenuItem
            // 
            this.noToolStripMenuItem.Checked = true;
            this.noToolStripMenuItem.CheckOnClick = true;
            this.noToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.noToolStripMenuItem.Name = "noToolStripMenuItem";
            this.noToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.noToolStripMenuItem.Text = "No.";
            this.noToolStripMenuItem.CheckedChanged += new System.EventHandler(this.noToolStripMenuItem_CheckedChanged);
            // 
            // timeToolStripMenuItem
            // 
            this.timeToolStripMenuItem.Checked = true;
            this.timeToolStripMenuItem.CheckOnClick = true;
            this.timeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.timeToolStripMenuItem.Name = "timeToolStripMenuItem";
            this.timeToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.timeToolStripMenuItem.Text = "Time";
            this.timeToolStripMenuItem.CheckedChanged += new System.EventHandler(this.timeToolStripMenuItem_CheckedChanged);
            // 
            // loggerToolStripMenuItem
            // 
            this.loggerToolStripMenuItem.Checked = true;
            this.loggerToolStripMenuItem.CheckOnClick = true;
            this.loggerToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.loggerToolStripMenuItem.Name = "loggerToolStripMenuItem";
            this.loggerToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.loggerToolStripMenuItem.Text = "Logger";
            this.loggerToolStripMenuItem.CheckedChanged += new System.EventHandler(this.loggerToolStripMenuItem_CheckedChanged);
            // 
            // levelToolStripMenuItem
            // 
            this.levelToolStripMenuItem.Checked = true;
            this.levelToolStripMenuItem.CheckOnClick = true;
            this.levelToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.levelToolStripMenuItem.Name = "levelToolStripMenuItem";
            this.levelToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.levelToolStripMenuItem.Text = "Level";
            this.levelToolStripMenuItem.CheckedChanged += new System.EventHandler(this.levelToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(195, 6);
            // 
            // setTimeReferenceToolStripMenuItem
            // 
            this.setTimeReferenceToolStripMenuItem.Name = "setTimeReferenceToolStripMenuItem";
            this.setTimeReferenceToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.setTimeReferenceToolStripMenuItem.Text = "Set time reference";
            this.setTimeReferenceToolStripMenuItem.Click += new System.EventHandler(this.setTimeReferenceToolStripMenuItem_Click);
            // 
            // clearTimeReferenceToolStripMenuItem
            // 
            this.clearTimeReferenceToolStripMenuItem.Name = "clearTimeReferenceToolStripMenuItem";
            this.clearTimeReferenceToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.clearTimeReferenceToolStripMenuItem.Text = "Clear time reference";
            this.clearTimeReferenceToolStripMenuItem.Click += new System.EventHandler(this.clearTimeReferenceToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(195, 6);
            // 
            // hideMessagesToolStripMenuItem
            // 
            this.hideMessagesToolStripMenuItem.Name = "hideMessagesToolStripMenuItem";
            this.hideMessagesToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.hideMessagesToolStripMenuItem.Text = "Hide messages";
            this.hideMessagesToolStripMenuItem.Click += new System.EventHandler(this.hideMessagesToolStripMenuItem_Click);
            // 
            // showAllMessagesToolStripMenuItem
            // 
            this.showAllMessagesToolStripMenuItem.Name = "showAllMessagesToolStripMenuItem";
            this.showAllMessagesToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.showAllMessagesToolStripMenuItem.Text = "Show all messages";
            this.showAllMessagesToolStripMenuItem.Click += new System.EventHandler(this.showAllMessagesToolStripMenuItem_Click);
            // 
            // findTestStepToolStripMenuItem
            // 
            this.findTestStepToolStripMenuItem.Name = "findTestStepToolStripMenuItem";
            this.findTestStepToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.findTestStepToolStripMenuItem.Text = "Select test step";
            this.findTestStepToolStripMenuItem.Click += new System.EventHandler(this.findTestStepToolStripMenuItem_Click);
            // 
            // treeColumn1
            // 
            this.treeColumn1.Header = "No.";
            this.treeColumn1.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn1.TooltipText = null;
            this.treeColumn1.Width = 55;
            // 
            // treeColumn2
            // 
            this.treeColumn2.Header = "Time";
            this.treeColumn2.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn2.TooltipText = null;
            this.treeColumn2.Width = 90;
            // 
            // treeColumn3
            // 
            this.treeColumn3.Header = "Logger";
            this.treeColumn3.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn3.TooltipText = null;
            this.treeColumn3.Width = 160;
            // 
            // treeColumn4
            // 
            this.treeColumn4.Header = "Level";
            this.treeColumn4.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn4.TooltipText = null;
            // 
            // treeColumn5
            // 
            this.treeColumn5.Header = "Message";
            this.treeColumn5.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn5.TooltipText = null;
            this.treeColumn5.Width = 500;
            // 
            // nodeIntegerTextBox1
            // 
            this.nodeIntegerTextBox1.DataPropertyName = "Number";
            this.nodeIntegerTextBox1.IncrementalSearchEnabled = true;
            this.nodeIntegerTextBox1.LeftMargin = 3;
            this.nodeIntegerTextBox1.ParentColumn = this.treeColumn1;
            // 
            // nodeTextBox1
            // 
            this.nodeTextBox1.DataPropertyName = "Timestamp";
            this.nodeTextBox1.IncrementalSearchEnabled = true;
            this.nodeTextBox1.LeftMargin = 3;
            this.nodeTextBox1.ParentColumn = this.treeColumn2;
            // 
            // nodeTextBox2
            // 
            this.nodeTextBox2.DataPropertyName = "Logger";
            this.nodeTextBox2.IncrementalSearchEnabled = true;
            this.nodeTextBox2.LeftMargin = 3;
            this.nodeTextBox2.ParentColumn = this.treeColumn3;
            // 
            // nodeTextBox3
            // 
            this.nodeTextBox3.DataPropertyName = "Level";
            this.nodeTextBox3.IncrementalSearchEnabled = true;
            this.nodeTextBox3.LeftMargin = 3;
            this.nodeTextBox3.ParentColumn = this.treeColumn4;
            // 
            // nodeTextBox4
            // 
            this.nodeTextBox4.DataPropertyName = "Message";
            this.nodeTextBox4.IncrementalSearchEnabled = true;
            this.nodeTextBox4.LeftMargin = 3;
            this.nodeTextBox4.ParentColumn = this.treeColumn5;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "note_information.png");
            this.imageList1.Images.SetKeyName(1, "apport.png");
            this.imageList1.Images.SetKeyName(2, "bug.png");
            this.imageList1.Images.SetKeyName(3, "flag_red.png");
            // 
            // MessageView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer);
            this.Name = "MessageView";
            this.Size = new System.Drawing.Size(1019, 634);
            this.splitContainer.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private Aga.Controls.Tree.TreeColumn treeColumn1;
        private Aga.Controls.Tree.TreeColumn treeColumn2;
        private Aga.Controls.Tree.TreeColumn treeColumn3;
        private Aga.Controls.Tree.TreeColumn treeColumn4;
        private Aga.Controls.Tree.TreeColumn treeColumn5;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox1;
        private Aga.Controls.Tree.NodeControls.NodeIntegerTextBox nodeIntegerTextBox1;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox2;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox3;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox4;
        private System.Windows.Forms.ListView messageList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem copyFullToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyMessagesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem setTimeReferenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearTimeReferenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem columnsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem noToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loggerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem levelToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem hideMessagesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showAllMessagesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem findTestStepToolStripMenuItem;
        private System.Windows.Forms.ImageList imageList1;
    }
}
