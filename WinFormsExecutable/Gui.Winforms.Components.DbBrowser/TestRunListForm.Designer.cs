﻿namespace yats.Gui.Winforms.Components.DbBrowser
{
    partial class TestRunListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestRunListForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btDelete = new System.Windows.Forms.ToolStripButton();
            this.btViewLog = new System.Windows.Forms.ToolStripButton();
            this.btRepeatTest = new System.Windows.Forms.ToolStripButton();
            this.testRunView = new yats.Gui.Winforms.Components.DbBrowser.TestRunView();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.viewTestLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadTestRunToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchPanel = new System.Windows.Forms.Panel();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.searchPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btDelete,
            this.btViewLog,
            this.btRepeatTest});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(845, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btDelete
            // 
            this.btDelete.Image = ((System.Drawing.Image)(resources.GetObject("btDelete.Image")));
            this.btDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(70, 22);
            this.btDelete.Text = "Remove";
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // btViewLog
            // 
            this.btViewLog.Image = ((System.Drawing.Image)(resources.GetObject("btViewLog.Image")));
            this.btViewLog.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btViewLog.Name = "btViewLog";
            this.btViewLog.Size = new System.Drawing.Size(75, 22);
            this.btViewLog.Text = "View Log";
            this.btViewLog.ToolTipText = "Open test log for analysis";
            this.btViewLog.Click += new System.EventHandler(this.btViewLog_Click);
            // 
            // btRepeatTest
            // 
            this.btRepeatTest.Image = ((System.Drawing.Image)(resources.GetObject("btRepeatTest.Image")));
            this.btRepeatTest.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btRepeatTest.Name = "btRepeatTest";
            this.btRepeatTest.Size = new System.Drawing.Size(85, 22);
            this.btRepeatTest.Text = "Repeat test";
            this.btRepeatTest.ToolTipText = "Load test run - repeat the test or review settings";
            this.btRepeatTest.Click += new System.EventHandler(this.btRepeatTest_Click);
            // 
            // testRunView
            // 
            this.testRunView.ContextMenuStrip = this.contextMenuStrip;
            this.testRunView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.testRunView.Location = new System.Drawing.Point(0, 25);
            this.testRunView.Name = "testRunView";
            this.testRunView.Size = new System.Drawing.Size(845, 378);
            this.testRunView.TabIndex = 0;
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewTestLogToolStripMenuItem,
            this.loadTestRunToolStripMenuItem,
            this.removeToolStripMenuItem,
            this.renameToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(149, 92);
            this.contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip_Opening);
            // 
            // viewTestLogToolStripMenuItem
            // 
            this.viewTestLogToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("viewTestLogToolStripMenuItem.Image")));
            this.viewTestLogToolStripMenuItem.Name = "viewTestLogToolStripMenuItem";
            this.viewTestLogToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.viewTestLogToolStripMenuItem.Text = "View Test Log";
            this.viewTestLogToolStripMenuItem.Click += new System.EventHandler(this.btViewLog_Click);
            // 
            // loadTestRunToolStripMenuItem
            // 
            this.loadTestRunToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("loadTestRunToolStripMenuItem.Image")));
            this.loadTestRunToolStripMenuItem.Name = "loadTestRunToolStripMenuItem";
            this.loadTestRunToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.loadTestRunToolStripMenuItem.Text = "Load Test Run";
            this.loadTestRunToolStripMenuItem.Click += new System.EventHandler(this.btRepeatTest_Click);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("removeToolStripMenuItem.Image")));
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // renameToolStripMenuItem
            // 
            this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
            this.renameToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.renameToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.renameToolStripMenuItem.Text = "Rename";
            this.renameToolStripMenuItem.Click += new System.EventHandler(this.renameToolStripMenuItem_Click);
            // 
            // searchPanel
            // 
            this.searchPanel.Controls.Add(this.tbSearch);
            this.searchPanel.Controls.Add(this.label1);
            this.searchPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.searchPanel.Location = new System.Drawing.Point(0, 403);
            this.searchPanel.Name = "searchPanel";
            this.searchPanel.Size = new System.Drawing.Size(845, 31);
            this.searchPanel.TabIndex = 2;
            this.searchPanel.Visible = false;
            // 
            // tbSearch
            // 
            this.tbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearch.Location = new System.Drawing.Point(44, 3);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(798, 20);
            this.tbSearch.TabIndex = 3;
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            this.tbSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbSearch_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Search";
            // 
            // TestRunListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 434);
            this.Controls.Add(this.testRunView);
            this.Controls.Add(this.searchPanel);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TestRunListForm";
            this.ShowInTaskbar = false;
            this.Text = "Test run history";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.searchPanel.ResumeLayout(false);
            this.searchPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TestRunView testRunView;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btDelete;
        private System.Windows.Forms.ToolStripButton btRepeatTest;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem viewTestLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadTestRunToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btViewLog;
        private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem;
        private System.Windows.Forms.Panel searchPanel;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label label1;
    }
}