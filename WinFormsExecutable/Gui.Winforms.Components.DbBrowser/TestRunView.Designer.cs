﻿using System;

namespace yats.Gui.Winforms.Components.DbBrowser
{
    partial class TestRunView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tree = new Aga.Controls.Tree.TreeViewAdv();
            this.treeColumn1 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumn2 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumnSize = new Aga.Controls.Tree.TreeColumn();
            this.nodeTextBox1 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.nodeTextBox2 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.nodeIntegerTextBox1 = new Aga.Controls.Tree.NodeControls.NodeIntegerTextBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // tree
            // 
            this.tree.BackColor = System.Drawing.SystemColors.Window;
            this.tree.Columns.Add(this.treeColumn1);
            this.tree.Columns.Add(this.treeColumn2);
            this.tree.Columns.Add(this.treeColumnSize);
            this.tree.DefaultToolTipProvider = null;
            this.tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tree.DragDropMarkColor = System.Drawing.Color.Black;
            this.tree.FullRowSelect = true;
            this.tree.LineColor = System.Drawing.SystemColors.ControlDark;
            this.tree.Location = new System.Drawing.Point(0, 0);
            this.tree.Model = null;
            this.tree.Name = "tree";
            this.tree.NodeControls.Add(this.nodeTextBox1);
            this.tree.NodeControls.Add(this.nodeTextBox2);
            this.tree.NodeControls.Add(this.nodeIntegerTextBox1);
            this.tree.SelectedNode = null;
            this.tree.SelectionMode = Aga.Controls.Tree.TreeSelectionMode.MultiSameParent;
            this.tree.Size = new System.Drawing.Size(511, 447);
            this.tree.TabIndex = 0;
            this.tree.Text = "treeViewAdv1";
            this.tree.UseColumns = true;
            this.tree.NodeMouseDoubleClick += new System.EventHandler<Aga.Controls.Tree.TreeNodeAdvMouseEventArgs>(this.tree_NodeMouseDoubleClick);
            this.tree.SelectionChanged += new System.EventHandler(this.tree_SelectionChanged);
            this.tree.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tree_KeyUp);
            this.tree.MouseMove += new System.Windows.Forms.MouseEventHandler(this.tree_MouseMove);
            // 
            // treeColumn1
            // 
            this.treeColumn1.Header = "Date";
            this.treeColumn1.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn1.TooltipText = null;
            this.treeColumn1.Width = 180;
            // 
            // treeColumn2
            // 
            this.treeColumn2.Header = "Test name";
            this.treeColumn2.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn2.TooltipText = null;
            this.treeColumn2.Width = 150;
            // 
            // treeColumnSize
            // 
            this.treeColumnSize.Header = "Size";
            this.treeColumnSize.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumnSize.TooltipText = null;
            // 
            // nodeTextBox1
            // 
            this.nodeTextBox1.DataPropertyName = "Date";
            this.nodeTextBox1.IncrementalSearchEnabled = true;
            this.nodeTextBox1.LeftMargin = 3;
            this.nodeTextBox1.ParentColumn = this.treeColumn1;
            // 
            // nodeTextBox2
            // 
            this.nodeTextBox2.DataPropertyName = "Name";
            this.nodeTextBox2.IncrementalSearchEnabled = true;
            this.nodeTextBox2.LeftMargin = 3;
            this.nodeTextBox2.ParentColumn = this.treeColumn2;
            // 
            // nodeIntegerTextBox1
            // 
            this.nodeIntegerTextBox1.DataPropertyName = "Size";
            this.nodeIntegerTextBox1.IncrementalSearchEnabled = true;
            this.nodeIntegerTextBox1.LeftMargin = 3;
            this.nodeIntegerTextBox1.ParentColumn = this.treeColumnSize;
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(116, 212);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(278, 23);
            this.progressBar.TabIndex = 1;
            this.progressBar.Visible = false;
            // 
            // TestRunView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.tree);
            this.Name = "TestRunView";
            this.Size = new System.Drawing.Size(511, 447);
            this.ResumeLayout(false);

        }

        #endregion

        private Aga.Controls.Tree.TreeViewAdv tree;
        private Aga.Controls.Tree.TreeColumn treeColumn1;
        private Aga.Controls.Tree.TreeColumn treeColumn2;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox1;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox2;
        private System.Windows.Forms.ToolTip toolTip;
        private Aga.Controls.Tree.TreeColumn treeColumnSize;
        private Aga.Controls.Tree.NodeControls.NodeIntegerTextBox nodeIntegerTextBox1;
        private System.Windows.Forms.ProgressBar progressBar;
    }
}
