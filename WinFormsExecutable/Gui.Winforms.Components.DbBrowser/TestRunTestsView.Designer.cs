﻿using System;

namespace yats.Gui.Winforms.Components.DbBrowser
{
    partial class TestRunTestsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        
        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tree = new Aga.Controls.Tree.TreeViewAdv();
            this.treeColumn5 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumn1 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumn2 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumn3 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumn4 = new Aga.Controls.Tree.TreeColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.selectAllRepetitionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllFailedStepsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nodeIcon4 = new Aga.Controls.Tree.NodeControls.NodeIcon();
            this.nodeTextBox1 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.nodeIcon1 = new Aga.Controls.Tree.NodeControls.NodeIcon();
            this.nodeIcon2 = new Aga.Controls.Tree.NodeControls.NodeIcon();
            this.nodeIcon3 = new Aga.Controls.Tree.NodeControls.NodeIcon();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tree
            // 
            this.tree.BackColor = System.Drawing.SystemColors.Window;
            this.tree.Columns.Add(this.treeColumn5);
            this.tree.Columns.Add(this.treeColumn1);
            this.tree.Columns.Add(this.treeColumn2);
            this.tree.Columns.Add(this.treeColumn3);
            this.tree.Columns.Add(this.treeColumn4);
            this.tree.ContextMenuStrip = this.contextMenuStrip1;
            this.tree.DefaultToolTipProvider = null;
            this.tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tree.DragDropMarkColor = System.Drawing.Color.Black;
            this.tree.FullRowSelect = true;
            this.tree.Indent = 7;
            this.tree.LineColor = System.Drawing.SystemColors.ControlDark;
            this.tree.Location = new System.Drawing.Point(0, 0);
            this.tree.Model = null;
            this.tree.Name = "tree";
            this.tree.NodeControls.Add(this.nodeIcon4);
            this.tree.NodeControls.Add(this.nodeTextBox1);
            this.tree.NodeControls.Add(this.nodeIcon1);
            this.tree.NodeControls.Add(this.nodeIcon2);
            this.tree.NodeControls.Add(this.nodeIcon3);
            this.tree.SelectedNode = null;
            this.tree.SelectionMode = Aga.Controls.Tree.TreeSelectionMode.MultiSameParent;
            this.tree.Size = new System.Drawing.Size(813, 284);
            this.tree.TabIndex = 0;
            this.tree.Text = "treeViewAdv1";
            this.tree.UseColumns = true;
            this.tree.SelectionChanged += new System.EventHandler(this.tree_SelectionChanged);
            // 
            // treeColumn5
            // 
            this.treeColumn5.Header = "";
            this.treeColumn5.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn5.TooltipText = null;
            this.treeColumn5.Width = 200;
            // 
            // treeColumn1
            // 
            this.treeColumn1.Header = "Test case";
            this.treeColumn1.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn1.TooltipText = null;
            this.treeColumn1.Width = 250;
            // 
            // treeColumn2
            // 
            this.treeColumn2.Header = "Reported result";
            this.treeColumn2.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn2.TooltipText = null;
            // 
            // treeColumn3
            // 
            this.treeColumn3.Header = "Evaluated result";
            this.treeColumn3.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn3.TooltipText = null;
            // 
            // treeColumn4
            // 
            this.treeColumn4.Header = "Repetition result";
            this.treeColumn4.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn4.TooltipText = null;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAllRepetitionsToolStripMenuItem,
            this.selectAllFailedStepsToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(183, 70);
            // 
            // selectAllRepetitionsToolStripMenuItem
            // 
            this.selectAllRepetitionsToolStripMenuItem.Name = "selectAllRepetitionsToolStripMenuItem";
            this.selectAllRepetitionsToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.selectAllRepetitionsToolStripMenuItem.Text = "Select all repetitions";
            this.selectAllRepetitionsToolStripMenuItem.Click += new System.EventHandler(this.selectAllRepetitionsToolStripMenuItem_Click);
            // 
            // selectAllFailedStepsToolStripMenuItem
            // 
            this.selectAllFailedStepsToolStripMenuItem.Name = "selectAllFailedStepsToolStripMenuItem";
            this.selectAllFailedStepsToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.selectAllFailedStepsToolStripMenuItem.Text = "Select all fails";
            this.selectAllFailedStepsToolStripMenuItem.Click += new System.EventHandler(this.selectAllFailedStepsToolStripMenuItem_Click);
            // 
            // nodeIcon4
            // 
            this.nodeIcon4.DataPropertyName = "Icon";
            this.nodeIcon4.LeftMargin = 1;
            this.nodeIcon4.ParentColumn = this.treeColumn5;
            this.nodeIcon4.ScaleMode = Aga.Controls.Tree.ImageScaleMode.Clip;
            // 
            // nodeTextBox1
            // 
            this.nodeTextBox1.DataPropertyName = "Text";
            this.nodeTextBox1.IncrementalSearchEnabled = true;
            this.nodeTextBox1.LeftMargin = 3;
            this.nodeTextBox1.ParentColumn = this.treeColumn1;
            this.nodeTextBox1.DrawText += new System.EventHandler<Aga.Controls.Tree.NodeControls.DrawEventArgs>(this.nodeTextBox1_DrawText);
            // 
            // nodeIcon1
            // 
            this.nodeIcon1.DataPropertyName = "RawResultIcon";
            this.nodeIcon1.LeftMargin = 1;
            this.nodeIcon1.ParentColumn = this.treeColumn2;
            this.nodeIcon1.ScaleMode = Aga.Controls.Tree.ImageScaleMode.Clip;
            // 
            // nodeIcon2
            // 
            this.nodeIcon2.DataPropertyName = "EvaluatedResultIcon";
            this.nodeIcon2.LeftMargin = 1;
            this.nodeIcon2.ParentColumn = this.treeColumn3;
            this.nodeIcon2.ScaleMode = Aga.Controls.Tree.ImageScaleMode.Clip;
            // 
            // nodeIcon3
            // 
            this.nodeIcon3.DataPropertyName = "RepetitionsResultIcon";
            this.nodeIcon3.LeftMargin = 1;
            this.nodeIcon3.ParentColumn = this.treeColumn4;
            this.nodeIcon3.ScaleMode = Aga.Controls.Tree.ImageScaleMode.Clip;
            // 
            // TestRunTestsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tree);
            this.Name = "TestRunTestsView";
            this.Size = new System.Drawing.Size(813, 284);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Aga.Controls.Tree.TreeViewAdv tree;
        private Aga.Controls.Tree.TreeColumn treeColumn1;
        private Aga.Controls.Tree.TreeColumn treeColumn2;
        private Aga.Controls.Tree.TreeColumn treeColumn3;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox1;
        private Aga.Controls.Tree.NodeControls.NodeIcon nodeIcon1;
        private Aga.Controls.Tree.NodeControls.NodeIcon nodeIcon2;
        private Aga.Controls.Tree.TreeColumn treeColumn4;
        private Aga.Controls.Tree.NodeControls.NodeIcon nodeIcon3;
        private Aga.Controls.Tree.NodeControls.NodeIcon nodeIcon4;
        private Aga.Controls.Tree.TreeColumn treeColumn5;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem selectAllRepetitionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllFailedStepsToolStripMenuItem;
    }
}
