﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using WeifenLuo.WinFormsUI.Docking;
using yats.Gui.Winforms.Components.Util;
using yats.Logging.Interface;
using yats.Logging.Util;
using System.Drawing;
using yats.Logging.XmlLogSerializer;
using yats.Utilities;

namespace yats.Gui.Winforms.Components.DbBrowser
{
    public partial class TestRunAnalysisForm : DockContent, IMergeableToolStripForm
    {
        public CachedLog DataSource
        {
            get { return testRunTestsView.DataSource; }
        }
        private ITestRunningStatus executionStatus;

        public TestRunAnalysisForm()
        {
            InitializeComponent( );
            //DockHandler.TabColor = Color.Ivory;
        }

        public TestRunAnalysisForm(string title, CachedLog log, bool canRepeatTest, LoggerFilter filter, ITestRunningStatus executionStatus)
            : this( )
        {
            this.Text = title;
            
            this.testRunTestsView.SetDataSource(log);
            this.testRunTestsView.StepSelectionChanged += testRunTestsView_StepSelectionChanged;
            this.messageView.SetDataSource(log, filter);
            this.messageView.OnTestStepFind += MessageView_OnTestStepFind;

            this.btCancel.Visible = this.btRepeat.Visible = canRepeatTest;
            this.executionStatus = executionStatus;
            executionStatus.OnStatusChange += executionStatus_OnStatusChange;
            executionStatus_OnStatusChange(executionStatus, null);
            btAutoScroll.Checked = messageView.AutoScroll;
        }

        private void MessageView_OnTestStepFind(object sender, LogMessageAddedEventArgs e)
        {
            this.testRunTestsView.SelectStepByPath(e.Message.Path);
        }

        void executionStatus_OnStatusChange(object sender, EventArgs e)
        {
            if (IsDisposed)
            {
                return;
            }

            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { executionStatus_OnStatusChange(sender, e); }));
                return;
            }
            btCancel.Enabled = cancelMenuItem.Enabled = executionStatus.IsRunning;
            btRepeat.Enabled = repeatMenuItem.Enabled = !executionStatus.IsRunning;
            exportLogToolStripMenuItem.Enabled = !executionStatus.IsRunning;
        }

        void testRunTestsView_StepSelectionChanged(object sender, TestStepNodeEventArgs e)
        {
            if (IsDisposed)
            {
                return;
            }

            this.messageView.SetSelectedSteps(e.Steps);
        }

        public event EventHandler CancelPressed;
        public event EventHandler RepeatPressed;


        #region IMergeableToolStripForm Members

        public ToolStrip ChildToolStrip
        {
            get
            {
                return toolStrip;
            }
            set
            {
                toolStrip = value;
            }
        }

        #endregion

        private void btRepeat_Click(object sender, EventArgs e)
        {
            if (RepeatPressed != null)
            {
                RepeatPressed(sender, e);
            }
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            btCancel.Enabled = false;
            if (CancelPressed != null)
            {
                CancelPressed(sender, e);
            }
        }

        private void exportLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (executionStatus.IsRunning)
                {
                    MessageBox.Show("Please wait for the test to finish");
                    return; // export menu item should be disabled
                }
                string suggestedFileName = FileUtil.StripInvalidFileCharacters(string.Format("{0:MMM d HH_mm} - {1}", DataSource.StartTime, DataSource.Name));
                saveFileDialog.FileName = suggestedFileName;
                if (saveFileDialog.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                
                new XmlLogExporter(saveFileDialog.FileName).Export(DataSource);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Export failed: " + ex.Message);
            }
        }

        #region Tab context menu

        public event EventHandler CloseAllResultsPressed;

        private void closeTabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void closeAllResultsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CloseAllResultsPressed != null)
            {
                CloseAllResultsPressed(this, e);
            }
        }
        #endregion

        TestRunLog testRunLog = null;
        public TestRunLog TestRunLog
        {
            get { return testRunLog; }
            set { this.testRunLog = value; }
        }

        private void btGotoNextFail_Click(object sender, EventArgs e)
        {
            testRunTestsView.GotoNextFail();
        }

        private void btGotoPrevFail_Click(object sender, EventArgs e)
        {
            testRunTestsView.GotoPreviousFail();
        }

        private void btAutoScroll_CheckedChanged(object sender, EventArgs e)
        {
            messageView.AutoScroll = btAutoScroll.Checked;
        }

        private void markProblemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            messageView.MarkProblem();
        }
    }
}
