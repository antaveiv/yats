﻿using System;
namespace yats.Gui.Winforms.Components.DbBrowser
{
    public interface ILoggerFilter
    {
        event EventHandler OnFilterChanged;
        bool IsEnabled(string logger, string level);
        void Set(string logger, string level);
        void SetAll(string level);
        // return true if any message is filtered, i.e. above DEBUG level
        bool IsFilterConfigured();
    }
}
