﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using yats.Gui.Winforms.Components.DbBrowser.Properties;
using yats.Logging.Interface;

namespace yats.Gui.Winforms.Components.DbBrowser
{
    public enum NodeKind
    {
        GROUP,
        TEST,
        REPETITION
    }

    public class TestStepNode : Aga.Controls.Tree.Node<TestStepNode>
    {
        public NodeKind Kind;
        public PathSegment Path;
        public string FullPath;

        public string RepetitionResult;
        public string RawResult;
        public string EvaluatedResult;
        public int RepetitionNumber;

        public TestStepNode()
        {
        }

        public TestStepNode(string fullPath, NodeKind kind)
        {
            this.Kind = kind;
            this.FullPath = fullPath;
        }

        public TestStepNode(NodeKind kind, string name)
        {
            this.Kind = kind;
            this.Text = name;
        }

        public TestStepNode(NodeKind kind, StepInfo row, PathSegment path, TestStepNode parent)
            : this( kind, row.Name )
        {
            if (Settings.Default.ShowRepetitionNumberInLog)
            {
                try
                {
                    if (kind != NodeKind.REPETITION)
                    {
                        int repetition = row.Path.Segments[row.Path.Segments.Length - 1].Repetition;
                        if (repetition != 0)
                        {
                            int repNumber = repetition + 1; // repetition zero based, show as 1-based
                            Text += string.Format(" [{0}]", repNumber);
                            if (repNumber == 2 && parent != null)
                            {
                                //also set repetition to the 1st item
                                parent.Nodes[0].Text += " [1]";
                            }
                        }
                    }
                }
                catch { }
            }
            this.Path = path;
            this.FullPath = row.TreePath;
            this.RawResult = row.RawResult;
            this.EvaluatedResult = row.EvaluatedResult;
            this.RepetitionResult = row.RepetitionResult;
        }

        public TestStepNode(NodeKind kind, string name, string rawResult, string evaluatedResult, string repetitionResult)
            : this(kind, name)
        {
            this.RawResult = rawResult;
            this.EvaluatedResult = evaluatedResult;
            this.RepetitionResult = repetitionResult;
        }

        public TestStepNode AddChild(TestStepNode child)
        {
            Nodes.Add(child);
            return child;
        }

        public TestStepNode Clone()
        {
            TestStepNode clone = new TestStepNode(Kind, Text);
            clone.Path = this.Path;
            clone.FullPath = this.FullPath;
            clone.EvaluatedResult = this.EvaluatedResult;
            clone.RawResult = this.RawResult;
            clone.RepetitionNumber = this.RepetitionNumber;
            clone.RepetitionResult = this.RepetitionResult;

            foreach (var child in Nodes)
            {
                clone.AddChild(child.Clone());
            }
            return clone;
        }

        internal TestStepNode GetOrCreate(StepInfo step, PathSegment path)
        {
            var found = Nodes.Where(x => x.Path == path);
            if (found.Count() == 0)
            {
                // no exact child node found, check for repetitions
                foreach (var node in Nodes)
                {
                    if (node.Path.Index == path.Index)
                    {
                        if (!(node.Nodes.Count == 1 && node.Nodes[0].Kind == NodeKind.REPETITION))
                        { // need to create repetitions node inside the child node, add the node itself and the current new node to it
                            string fullPathWithoutRepetition = node.FullPath.Substring(0, node.FullPath.LastIndexOfAny(StepPath.RepetitionSeparators) + 1);
                            TestStepNode rep = new TestStepNode(fullPathWithoutRepetition, NodeKind.REPETITION);
                            rep.Path = path;
                            rep.AddChild(node.Clone());
                            node.Nodes.Clear();
                            node.AddChild(rep);
                            node.FullPath = fullPathWithoutRepetition;

                            if (step.IsTestCase) // test case
                            {
                                return rep.AddChild(new TestStepNode(NodeKind.TEST, step, path, rep));
                            }
                            else
                            {
                                return rep.AddChild(new TestStepNode(NodeKind.GROUP, step, path, rep));
                            }
                        }
                        else
                        {
                            // ch is already has a RepetitionsNode, just add a child node
                            if (step.IsTestCase) 
                            {
                                return node.Nodes[0].AddChild(new TestStepNode(NodeKind.TEST, step, path, node.Nodes[0]));
                            }
                            else
                            {
                                return node.Nodes[0].AddChild(new TestStepNode(NodeKind.GROUP, step, path, node.Nodes[0]));
                            }
                        }
                    }
                }

                if (step.IsTestCase) // test case
                {
                    return AddChild(new TestStepNode(NodeKind.TEST, step, path, this));
                }
                else
                {
                    return AddChild(new TestStepNode(NodeKind.GROUP, step, path, this));
                }
            }
            if (found.Count() == 1)
            {
                found.First().UpdateResult(step);
                return found.First();
            }
            if (found.Count() > 1)
            {
                throw new Exception("hierarchy error");
            }
            return null;
        }

        public virtual bool UpdateResult(StepInfo updateFrom)
        {
            if (string.IsNullOrEmpty(updateFrom.RepetitionResult) == false)
            {
                RepetitionResult = updateFrom.RepetitionResult;
            }
            if (string.IsNullOrEmpty(updateFrom.RawResult) == false)
            {
                RawResult = updateFrom.RawResult;
            }
            if (string.IsNullOrEmpty(updateFrom.EvaluatedResult) == false)
            {
                EvaluatedResult = updateFrom.EvaluatedResult;
            }
            return true;
        }

        internal TestStepNode FindChild(PathSegment currentPathSegment)
        {
            foreach (var node in Nodes)
            {
                if (node.Path == currentPathSegment)
                {
                    return node;
                }
            }

            return null;
        }

        public override string ToString()
        {
            switch (Kind)
            {
                case NodeKind.GROUP:
                    return string.Format("Group {0} [{1} {2} {3}]", Text, RawResult, EvaluatedResult, RepetitionResult);
                case NodeKind.TEST:
                    return string.Format("Test case {0} [{1} {2} {3}]", Text, RawResult, EvaluatedResult, RepetitionResult);
                case NodeKind.REPETITION:
                    return string.Format("{0} repetitions {1}.* [{2} {3} {4}]", Nodes.Count, Path, RawResult, EvaluatedResult, RepetitionResult);
                default:
                    return "ERROR";
            }
        }

        public bool IsFailNode()
        {
            if (Kind != NodeKind.TEST)
            {
                return false;
            }
            switch (EvaluatedResult)
            {                
                case "FAIL":
                case "INCONCLUSIVE":
                    return true;
                case "CANCELED":
                case "PASS":
                case "NOT_RUN":
                default: return false;
            }
        }

        #region Icons
        public Image Icon
        {
            get
            {
                switch(Kind)
                {
                    case NodeKind.GROUP:
                        return Resources.arrow_turn_270;
                    case NodeKind.REPETITION:
                        return Resources.view_refresh2_16;
                    case NodeKind.TEST:
                        return Resources.play_green_16;
                }
                return null;
            }
        }

        public Image RawResultIcon
        {
            get
            {
                return GetResultImage( RawResult );
            }
        }

        public Image RepetitionsResultIcon
        {
            get
            {
                return GetResultImage( RepetitionResult );
            }
        }

        public Image EvaluatedResultIcon
        {
            get
            {
                return GetResultImage( EvaluatedResult );
            }
        }

        static Image GetResultImage(string result)
        {
            switch(result)
            {
                case "CANCELED":
                    return Resources.minus_circle_green_16;
                case "FAIL":
                    return Resources.circle_red_16;
                case "INCONCLUSIVE":
                    return Resources.help_circle_blue_16;
                case "PASS":
                    return Resources.circle_green_16;
                case "NOT_RUN":
                    return Resources.circle_grey_16;
                default: return null;
            }
        }
        #endregion
    }

    public class TestStepNodeEventArgs : EventArgs
        {
        public IList<TestStepNode> Steps;
        public TestStepNodeEventArgs(IList<TestStepNode> steps)
            {
            Steps = steps;
            }
        }
}
