﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using yats.Utilities;

namespace yats.Gui.Winforms.Components.DbBrowser
{
    public class LoggerFilter : yats.Gui.Winforms.Components.DbBrowser.ILoggerFilter
    {
        yats.Utilities.SerializableDictionary<string, string> levelMap = new SerializableDictionary<string, string>();
        bool isModified = false;
        ReaderWriterLock lck = new ReaderWriterLock();
        protected const int LOCK_TIMEOUT = 10000;
                
        public event EventHandler OnFilterChanged;

        public void Deserialize(string serialized)
        {
            try
            {
                lck.AcquireWriterLock(LOCK_TIMEOUT);
                try
                {
                    levelMap = SerializableDictionary<string, string>.ReadString(serialized);
                }
                finally
                {
                    lck.ReleaseWriterLock();
                }

                RaiseFilterChanged();
            }
            catch { }
            isModified = false;
        }

        private void RaiseFilterChanged()
        {
            if (OnFilterChanged != null)
            {
                foreach (EventHandler handler in OnFilterChanged.GetInvocationList())
                {
                    try
                    {
                        handler(this, new EventArgs());
                    }
                    catch (Exception e)
                    {
                        CrashLog.Write(e, "Error in OnFilterChanged " + handler.Method.Name);
                    }
                }
            }
        }

        public string Serialize()
        {
            try
            {
                lck.AcquireReaderLock(LOCK_TIMEOUT);
                try
                {
                    string result = levelMap.WriteString();
                    return result;
                }
                finally
                {
                    lck.ReleaseReaderLock();
                }                
            }
            catch { }
            return "";
        }

        public string Get(string logger)
        {
            try
            {
                string value = "DEBUG";
                lck.AcquireReaderLock(LOCK_TIMEOUT);
                try
                {
                    if (levelMap.TryGetValue(logger, out value))
                    {
                        return value;
                    }
                }
                finally
                {
                    lck.ReleaseReaderLock();
                }
                lck.AcquireWriterLock(LOCK_TIMEOUT);
                try
                {
                    levelMap.Add(logger, "DEBUG");
                }
                finally
                {
                    lck.ReleaseWriterLock();
                }
                RaiseFilterChanged();

                isModified = true;
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex);
            }

            return "DEBUG";
        }

        public void Set(string logger, string level)
        {
            try
            {
                lck.AcquireReaderLock(LOCK_TIMEOUT);
                try
                {
                    string value;
                    if (levelMap.TryGetValue(logger, out value))
                    {
                        if (value == level)
                        {
                            return;
                        }
                    }
                }
                finally
                {
                    lck.ReleaseReaderLock();
                }

                lck.AcquireWriterLock(LOCK_TIMEOUT);
                
                try
                {
                    levelMap[logger] = level;
                }
                finally
                {
                    lck.ReleaseWriterLock();
                }

                isModified = true;

                RaiseFilterChanged();
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex);
            }
        }

        public void SetAll(string level)
        {
            try
            {
                bool modified = false;
                lck.AcquireWriterLock(LOCK_TIMEOUT);
                try
                {
                    foreach (var key in levelMap.Keys.ToList())
                    {
                        if (levelMap[key] != level)
                        {
                            modified = true;
                            levelMap[key] = level;
                        }
                    }
                }
                finally
                {
                    lck.ReleaseWriterLock();
                }

                if (modified)
                {
                    isModified = true;
                    RaiseFilterChanged();
                }
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex);
            }
        }

        public bool IsEnabled(string logger, string level)
        {
            return (LevelToInt(level) >= LevelToInt(Get(logger)));
        }

        private int LevelToInt(string level)
        {
            if (level.Equals("DEBUG"))
            {
                return 0;
            }
            if (level.Equals("INFO"))
            {
                return 1;
            }
            if (level.Equals("WARN"))
            {
                return 2;
            }
            if (level.Equals("ERROR"))
            {
                return 3;
            }
            if (level.Equals("FATAL"))
            {
                return 4;
            }
            if (level.Equals("OFF"))
            {
                return 5;
            }
            return 0;
        }

        public List<string> GetLoggers()
        {
            try
            {
                lck.AcquireReaderLock(LOCK_TIMEOUT);
                try
                {
                    var result = new List<string>(levelMap.Keys);
                    return result;
                }
                finally
                {
                    lck.ReleaseReaderLock();
                }
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex);
            }
            return new List<string>();
        }

        public bool IsModified
        {
            get
            {
                return isModified;
            }
            set
            {
                isModified = value;
            }
        }


        public bool IsFilterConfigured()
        {
            try
            {
                lck.AcquireReaderLock(LOCK_TIMEOUT);
                try
                {
                    return levelMap.Values.Count(x => x != "DEBUG") > 0;
                }
                finally
                {
                    lck.ReleaseReaderLock();
                }
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex);
                throw;
            }
        }
    }
}
