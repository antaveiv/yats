﻿namespace yats.Gui.Winforms.Components.LogView
{
    partial class FilteredLogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( FilteredLogForm ) );
            this.filteredLogView = new yats.Gui.Winforms.Components.LogView.FilteredLogView( );
            this.SuspendLayout( );
            // 
            // filteredLogView
            // 
            this.filteredLogView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filteredLogView.Location = new System.Drawing.Point( 0, 0 );
            this.filteredLogView.Name = "filteredLogView";
            this.filteredLogView.Size = new System.Drawing.Size( 786, 358 );
            this.filteredLogView.TabIndex = 0;
            // 
            // FilteredLogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 786, 358 );
            this.Controls.Add( this.filteredLogView );
            this.Icon = ((System.Drawing.Icon)(resources.GetObject( "$this.Icon" )));
            this.Name = "FilteredLogForm";
            this.ShowInTaskbar = false;
            this.Text = "Filtered Log";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.FilteredLogForm_FormClosing );
            this.ResumeLayout( false );

        }

        #endregion

        private FilteredLogView filteredLogView;
    }
}