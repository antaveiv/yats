﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Windows.Forms;

namespace yats.Gui.Winforms.Components.LogView
{
    public partial class LogViewControl : UserControl
    {
        log4net.Core.Level CurrentLevel = log4net.Core.Level.Warn;

        public LogViewControl()
        {
            InitializeComponent();
            
            filterLevelCombo.Items.Add(log4net.Core.Level.Off);
            filterLevelCombo.Items.Add(log4net.Core.Level.Fatal);
            filterLevelCombo.Items.Add(log4net.Core.Level.Error);
            filterLevelCombo.Items.Add(log4net.Core.Level.Warn);
            filterLevelCombo.Items.Add(log4net.Core.Level.Info);
            filterLevelCombo.Items.Add(log4net.Core.Level.Debug);
            filterLevelCombo.SelectedItem = CurrentLevel;
        }

        internal void LogEvent(log4net.Core.LoggingEvent loggingEvent)
        {
            if (loggingEvent.Level.CompareTo(CurrentLevel) < 0)
            {
                return;
            }

            if(this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { LogEvent(loggingEvent); }));
            }
            else
            {
                try
                {
                    textBox.AppendText( string.Format( "{0:-30}\t{1}\r\n", loggingEvent.LoggerName, loggingEvent.RenderedMessage ) );
                    Exception ex = loggingEvent.ExceptionObject;
                    int depth = 0;
                    while(ex != null && depth < 3)
                    {
                        depth++;
                        textBox.AppendText( string.Format( "{0:-30}\t{1}\r\n", loggingEvent.LoggerName, ex.Message ) );
                        if(ex.StackTrace != null)
                        {
                            textBox.AppendText( string.Format( "{0:-30}\t{1}\r\n", loggingEvent.LoggerName, "Stack trace" ) );
                            foreach(var s in ex.StackTrace.Split( new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries ))
                            {
                                textBox.AppendText( string.Format( "{0:-30}\t{1}\r\n", loggingEvent.LoggerName, s ) );
                            }
                        }
                        ex = ex.InnerException;
                        if(ex != null)
                        {
                            textBox.AppendText( string.Format( "{0:-30}\t{1}\r\n", loggingEvent.LoggerName, "Inner exception" ) );
                        }
                    }
                }
                catch { }
            }
        }

        private void filterLevelCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            CurrentLevel = filterLevelCombo.SelectedItem as log4net.Core.Level;
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            textBox.Clear();
        }

        private void openFilteredView_Click(object sender, EventArgs e)
        {
            var childForm = new FilteredLogForm();
            childForm.MdiParent = this.ParentForm.MdiParent;
            childForm.Show();
        }
    }
}
