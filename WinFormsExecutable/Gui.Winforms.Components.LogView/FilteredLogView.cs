﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Windows.Forms;

namespace yats.Gui.Winforms.Components.LogView
{
    public partial class FilteredLogView : UserControl
    {
        public FilteredLogView()
        {
            InitializeComponent();
        }

        internal void LogEvent(log4net.Core.LoggingEvent loggingEvent)
        {
            if (string.IsNullOrEmpty(findText.Text)) // do not display if filter is not set
            {
                return;
            }

            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { LogEvent(loggingEvent); }));
            }
            else
            {
                string text = string.Format("{0:-30}\t{1}\r\n", loggingEvent.LoggerName, loggingEvent.RenderedMessage);
                if (btRegexp.Checked) // use regular expression
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(text, findText.Text, ((btMatchCase.Checked) ? System.Text.RegularExpressions.RegexOptions.None : System.Text.RegularExpressions.RegexOptions.IgnoreCase)))
                    {
                        AddResult(text);
                    }
                }
                else // do not use regular expression
                {
                    if (btMatchCase.Checked)//case - insensitive
                    {
                        if (text.Contains(findText.Text))
                        {
                            AddResult(text);
                        }
                    }
                    else
                    {
                        bool contains = text.IndexOf(findText.Text, StringComparison.OrdinalIgnoreCase) >= 0;
                        if (contains)
                        {
                            AddResult(text);
                        }
                    }
                }
                
            }
        }

        protected int counter = 0;

        private void AddResult(string text)
        {
            textBox.AppendText(text);
            counter++;
            counterLabel.Text = string.Format("{0} found", counter);
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            textBox.Clear();
            counter = 0;
            counterLabel.Text = "";
        }
    }
}
