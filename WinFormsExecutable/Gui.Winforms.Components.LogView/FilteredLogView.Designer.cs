﻿namespace yats.Gui.Winforms.Components.LogView
{
    partial class FilteredLogView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FilteredLogView));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.textBox = new yats.Gui.Winforms.Components.Util.TextBoxAdv();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.clearButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.findText = new System.Windows.Forms.ToolStripTextBox();
            this.btMatchCase = new System.Windows.Forms.ToolStripButton();
            this.btRegexp = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.counterLabel = new System.Windows.Forms.ToolStripLabel();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.textBox);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(690, 490);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(690, 515);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // textBox
            // 
            this.textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox.Location = new System.Drawing.Point(0, 0);
            this.textBox.Multiline = true;
            this.textBox.Name = "textBox";
            this.textBox.ReadOnly = true;
            this.textBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox.Size = new System.Drawing.Size(690, 490);
            this.textBox.TabIndex = 4;
            this.textBox.WordWrap = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AllowMerge = false;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearButton,
            this.toolStripLabel1,
            this.findText,
            this.btRegexp,
            this.btMatchCase,
            this.toolStripSeparator1,
            this.counterLabel});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(516, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // clearButton
            // 
            this.clearButton.Image = ((System.Drawing.Image)(resources.GetObject("clearButton.Image")));
            this.clearButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(52, 22);
            this.clearButton.Text = "Clear";
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(35, 22);
            this.toolStripLabel1.Text = "Filter:";
            // 
            // findText
            // 
            this.findText.Name = "findText";
            this.findText.Size = new System.Drawing.Size(200, 25);
            // 
            // btMatchCase
            // 
            this.btMatchCase.CheckOnClick = true;
            this.btMatchCase.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btMatchCase.Image = ((System.Drawing.Image)(resources.GetObject("btMatchCase.Image")));
            this.btMatchCase.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btMatchCase.Name = "btMatchCase";
            this.btMatchCase.Size = new System.Drawing.Size(65, 22);
            this.btMatchCase.Text = "Match case";
            // 
            // btRegexp
            // 
            this.btRegexp.CheckOnClick = true;
            this.btRegexp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btRegexp.Image = ((System.Drawing.Image)(resources.GetObject("btRegexp.Image")));
            this.btRegexp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btRegexp.Name = "btRegexp";
            this.btRegexp.Size = new System.Drawing.Size(103, 22);
            this.btRegexp.Text = "Regular expression";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // counterLabel
            // 
            this.counterLabel.Name = "counterLabel";
            this.counterLabel.Size = new System.Drawing.Size(10, 22);
            this.counterLabel.Text = " ";
            // 
            // FilteredLogView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "FilteredLogView";
            this.Size = new System.Drawing.Size(690, 515);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ContentPanel.PerformLayout();
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private yats.Gui.Winforms.Components.Util.TextBoxAdv textBox;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton clearButton;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox findText;
        private System.Windows.Forms.ToolStripButton btMatchCase;
        private System.Windows.Forms.ToolStripButton btRegexp;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel counterLabel;


    }
}
