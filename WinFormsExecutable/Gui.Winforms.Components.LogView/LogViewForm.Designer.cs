﻿namespace yats.Gui.Winforms.Components.LogView
{
    partial class LogViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( LogViewForm ) );
            this.logViewControl = new yats.Gui.Winforms.Components.LogView.LogViewControl( );
            this.SuspendLayout( );
            // 
            // logViewControl
            // 
            this.logViewControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logViewControl.Location = new System.Drawing.Point( 0, 0 );
            this.logViewControl.Name = "logViewControl";
            this.logViewControl.Size = new System.Drawing.Size( 529, 266 );
            this.logViewControl.TabIndex = 0;
            // 
            // LogViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 529, 266 );
            this.Controls.Add( this.logViewControl );
            this.Icon = ((System.Drawing.Icon)(resources.GetObject( "$this.Icon" )));
            this.Name = "LogViewForm";
            this.ShowInTaskbar = false;
            this.Text = "Log";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.LogViewForm_FormClosing );
            this.ResumeLayout( false );

        }

        #endregion

        private LogViewControl logViewControl;
    }
}