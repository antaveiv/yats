﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Windows.Forms;
using log4net;
using WeifenLuo.WinFormsUI.Docking;
using yats.Logging.Interface;
using yats.TestCase.Interface;

namespace yats.Gui.Winforms.Components.LogView
{
    public partial class LogViewForm : DockContent, ILogEventSink 
    {
        //TODO: icon
        public LogViewForm()
        {
            InitializeComponent();
            LogManager.GetRepository();
            YatsLogAppenderCollection.Instance.AddEventSink(this);
            this.TabText = this.Text;
        }
        
        private void LogViewForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            YatsLogAppenderCollection.Instance.RemoveEventSink(this);
        }

        public void StartTestRun(string name, string description, byte[] settings, int testRunId)
        {
        }

        public void EndTestRun(int testRunId, ITestResult result)
        {
        }

        public void UpdateStep(int testRunId, StepInfo stepInfo)
        {
        }

        public void DoAppend(int? testRunId, string stepPath, log4net.Core.LoggingEvent loggingEvent, ref LogMessage logMessage)
        {
            logViewControl.LogEvent(loggingEvent);
        }

        public void WriteBinaryData(int testRunId, string stepPath, string logger, string level, string message, object value)
        {
            logViewControl.LogEvent(new log4net.Core.LoggingEvent(null, null, logger, LogLevelHelper.Parse(level), message, null));
        }
    }
}
