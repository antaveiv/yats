﻿namespace yats.Gui.Winforms.Components.Util
{
    partial class ExtendedHexEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExtendedHexEditor));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbSelection = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbOffset = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.valueSignedBE = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyLabelMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteLabelMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.valueSignedLE = new System.Windows.Forms.Label();
            this.valueUnsignedBE = new System.Windows.Forms.Label();
            this.valueUnsignedLE = new System.Windows.Forms.Label();
            this.lbTotalSize = new System.Windows.Forms.Label();
            this.labelSignedBE = new System.Windows.Forms.Label();
            this.labelSignedLE = new System.Windows.Forms.Label();
            this.labelUnsignedBE = new System.Windows.Forms.Label();
            this.labelUnsignedLE = new System.Windows.Forms.Label();
            this.labelTotalSize = new System.Windows.Forms.Label();
            this.hexBox = new Be.Windows.Forms.HexBox();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyASCIIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.noSeparatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.separateBySpaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.separateByCommaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cByteArrayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cArrayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.javaArrayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteBinaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteHEXTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteDecimalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadFromFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbSelection);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lbOffset);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.valueSignedBE);
            this.panel1.Controls.Add(this.valueSignedLE);
            this.panel1.Controls.Add(this.valueUnsignedBE);
            this.panel1.Controls.Add(this.valueUnsignedLE);
            this.panel1.Controls.Add(this.lbTotalSize);
            this.panel1.Controls.Add(this.labelSignedBE);
            this.panel1.Controls.Add(this.labelSignedLE);
            this.panel1.Controls.Add(this.labelUnsignedBE);
            this.panel1.Controls.Add(this.labelUnsignedLE);
            this.panel1.Controls.Add(this.labelTotalSize);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 167);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(814, 59);
            this.panel1.TabIndex = 3;
            // 
            // lbSelection
            // 
            this.lbSelection.AutoSize = true;
            this.lbSelection.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSelection.Location = new System.Drawing.Point(418, 3);
            this.lbSelection.Name = "lbSelection";
            this.lbSelection.Size = new System.Drawing.Size(49, 14);
            this.lbSelection.TabIndex = 5;
            this.lbSelection.Text = "label4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(358, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Selection:";
            // 
            // lbOffset
            // 
            this.lbOffset.AutoSize = true;
            this.lbOffset.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOffset.Location = new System.Drawing.Point(274, 3);
            this.lbOffset.Name = "lbOffset";
            this.lbOffset.Size = new System.Drawing.Size(49, 14);
            this.lbOffset.TabIndex = 3;
            this.lbOffset.Text = "label3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(230, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Offset:";
            // 
            // valueSignedBE
            // 
            this.valueSignedBE.AutoSize = true;
            this.valueSignedBE.ContextMenuStrip = this.contextMenuStrip1;
            this.valueSignedBE.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueSignedBE.Location = new System.Drawing.Point(335, 37);
            this.valueSignedBE.Name = "valueSignedBE";
            this.valueSignedBE.Size = new System.Drawing.Size(35, 14);
            this.valueSignedBE.TabIndex = 1;
            this.valueSignedBE.Text = "1234";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyLabelMenuItem,
            this.pasteLabelMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(145, 48);
            // 
            // copyLabelMenuItem
            // 
            this.copyLabelMenuItem.Name = "copyLabelMenuItem";
            this.copyLabelMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyLabelMenuItem.Size = new System.Drawing.Size(144, 22);
            this.copyLabelMenuItem.Text = "Copy";
            this.copyLabelMenuItem.Click += new System.EventHandler(this.copyLabelMenuItem_Click);
            // 
            // pasteLabelMenuItem
            // 
            this.pasteLabelMenuItem.Name = "pasteLabelMenuItem";
            this.pasteLabelMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteLabelMenuItem.Size = new System.Drawing.Size(144, 22);
            this.pasteLabelMenuItem.Text = "Paste";
            this.pasteLabelMenuItem.Click += new System.EventHandler(this.pasteLabelMenuItem_Click);
            // 
            // valueSignedLE
            // 
            this.valueSignedLE.AutoSize = true;
            this.valueSignedLE.ContextMenuStrip = this.contextMenuStrip1;
            this.valueSignedLE.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueSignedLE.Location = new System.Drawing.Point(335, 20);
            this.valueSignedLE.Name = "valueSignedLE";
            this.valueSignedLE.Size = new System.Drawing.Size(35, 14);
            this.valueSignedLE.TabIndex = 1;
            this.valueSignedLE.Text = "1234";
            // 
            // valueUnsignedBE
            // 
            this.valueUnsignedBE.AutoSize = true;
            this.valueUnsignedBE.ContextMenuStrip = this.contextMenuStrip1;
            this.valueUnsignedBE.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueUnsignedBE.Location = new System.Drawing.Point(120, 37);
            this.valueUnsignedBE.Name = "valueUnsignedBE";
            this.valueUnsignedBE.Size = new System.Drawing.Size(35, 14);
            this.valueUnsignedBE.TabIndex = 1;
            this.valueUnsignedBE.Text = "1234";
            // 
            // valueUnsignedLE
            // 
            this.valueUnsignedLE.AutoSize = true;
            this.valueUnsignedLE.ContextMenuStrip = this.contextMenuStrip1;
            this.valueUnsignedLE.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valueUnsignedLE.Location = new System.Drawing.Point(120, 20);
            this.valueUnsignedLE.Name = "valueUnsignedLE";
            this.valueUnsignedLE.Size = new System.Drawing.Size(35, 14);
            this.valueUnsignedLE.TabIndex = 1;
            this.valueUnsignedLE.Text = "1234";
            // 
            // lbTotalSize
            // 
            this.lbTotalSize.AutoSize = true;
            this.lbTotalSize.ContextMenuStrip = this.contextMenuStrip1;
            this.lbTotalSize.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalSize.Location = new System.Drawing.Point(64, 3);
            this.lbTotalSize.Name = "lbTotalSize";
            this.lbTotalSize.Size = new System.Drawing.Size(49, 14);
            this.lbTotalSize.TabIndex = 1;
            this.lbTotalSize.Text = "label2";
            // 
            // labelSignedBE
            // 
            this.labelSignedBE.AutoSize = true;
            this.labelSignedBE.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelSignedBE.Location = new System.Drawing.Point(230, 37);
            this.labelSignedBE.Name = "labelSignedBE";
            this.labelSignedBE.Size = new System.Drawing.Size(95, 13);
            this.labelSignedBE.TabIndex = 0;
            this.labelSignedBE.Text = "Signed big endian:";
            this.labelSignedBE.DoubleClick += new System.EventHandler(this.labelSignedBE_DoubleClick);
            // 
            // labelSignedLE
            // 
            this.labelSignedLE.AutoSize = true;
            this.labelSignedLE.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelSignedLE.Location = new System.Drawing.Point(230, 20);
            this.labelSignedLE.Name = "labelSignedLE";
            this.labelSignedLE.Size = new System.Drawing.Size(99, 13);
            this.labelSignedLE.TabIndex = 0;
            this.labelSignedLE.Text = "Signed little endian:";
            this.labelSignedLE.DoubleClick += new System.EventHandler(this.labelSignedLE_DoubleClick);
            // 
            // labelUnsignedBE
            // 
            this.labelUnsignedBE.AutoSize = true;
            this.labelUnsignedBE.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelUnsignedBE.Location = new System.Drawing.Point(3, 37);
            this.labelUnsignedBE.Name = "labelUnsignedBE";
            this.labelUnsignedBE.Size = new System.Drawing.Size(107, 13);
            this.labelUnsignedBE.TabIndex = 0;
            this.labelUnsignedBE.Text = "Unsigned big endian:";
            this.labelUnsignedBE.DoubleClick += new System.EventHandler(this.labelUnsignedBE_DoubleClick);
            // 
            // labelUnsignedLE
            // 
            this.labelUnsignedLE.AutoSize = true;
            this.labelUnsignedLE.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelUnsignedLE.Location = new System.Drawing.Point(3, 20);
            this.labelUnsignedLE.Name = "labelUnsignedLE";
            this.labelUnsignedLE.Size = new System.Drawing.Size(111, 13);
            this.labelUnsignedLE.TabIndex = 0;
            this.labelUnsignedLE.Text = "Unsigned little endian:";
            this.labelUnsignedLE.DoubleClick += new System.EventHandler(this.labelUnsignedLE_DoubleClick);
            // 
            // labelTotalSize
            // 
            this.labelTotalSize.AutoSize = true;
            this.labelTotalSize.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelTotalSize.Location = new System.Drawing.Point(3, 3);
            this.labelTotalSize.Name = "labelTotalSize";
            this.labelTotalSize.Size = new System.Drawing.Size(55, 13);
            this.labelTotalSize.TabIndex = 0;
            this.labelTotalSize.Text = "Total size:";
            this.labelTotalSize.DoubleClick += new System.EventHandler(this.labelTotalSize_DoubleClick);
            // 
            // hexBox
            // 
            this.hexBox.ContextMenuStrip = this.contextMenuStrip;
            this.hexBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hexBox.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hexBox.LineInfoVisible = true;
            this.hexBox.Location = new System.Drawing.Point(0, 0);
            this.hexBox.Name = "hexBox";
            this.hexBox.ShadowSelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(60)))), ((int)(((byte)(188)))), ((int)(((byte)(255)))));
            this.hexBox.Size = new System.Drawing.Size(814, 167);
            this.hexBox.StringViewVisible = true;
            this.hexBox.TabIndex = 4;
            this.hexBox.UseFixedBytesPerLine = true;
            this.hexBox.VScrollBarVisible = true;
            this.hexBox.SelectionStartChanged += new System.EventHandler(this.hexBox_SelectionStartChanged);
            this.hexBox.SelectionLengthChanged += new System.EventHandler(this.HexBox_SelectionLengthChanged);
            this.hexBox.TextChanged += new System.EventHandler(this.hexBox_TextChanged);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyASCIIToolStripMenuItem,
            this.toolStripMenuItem2,
            this.pasteBinaryToolStripMenuItem,
            this.pasteHEXTextToolStripMenuItem,
            this.pasteDecimalToolStripMenuItem,
            this.saveToFileToolStripMenuItem,
            this.loadFromFileToolStripMenuItem,
            this.toolStripSeparator1,
            this.toolStripMenuItem3});
            this.contextMenuStrip.Name = "contextMenuStrip1";
            this.contextMenuStrip.Size = new System.Drawing.Size(225, 208);
            // 
            // copyASCIIToolStripMenuItem
            // 
            this.copyASCIIToolStripMenuItem.Name = "copyASCIIToolStripMenuItem";
            this.copyASCIIToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyASCIIToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.copyASCIIToolStripMenuItem.Text = "Copy as Text";
            this.copyASCIIToolStripMenuItem.Click += new System.EventHandler(this.copyASCIIToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.noSeparatorToolStripMenuItem,
            this.separateBySpaceToolStripMenuItem,
            this.separateByCommaToolStripMenuItem,
            this.cByteArrayToolStripMenuItem,
            this.cArrayToolStripMenuItem,
            this.javaArrayToolStripMenuItem});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(224, 22);
            this.toolStripMenuItem2.Text = "Copy as HEX Text";
            // 
            // noSeparatorToolStripMenuItem
            // 
            this.noSeparatorToolStripMenuItem.Name = "noSeparatorToolStripMenuItem";
            this.noSeparatorToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.C)));
            this.noSeparatorToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.noSeparatorToolStripMenuItem.Text = "No Separator";
            this.noSeparatorToolStripMenuItem.ToolTipText = "e.g. 00F0FF";
            this.noSeparatorToolStripMenuItem.Click += new System.EventHandler(this.copyHexNoSeparator_Click);
            // 
            // separateBySpaceToolStripMenuItem
            // 
            this.separateBySpaceToolStripMenuItem.Name = "separateBySpaceToolStripMenuItem";
            this.separateBySpaceToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.H)));
            this.separateBySpaceToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.separateBySpaceToolStripMenuItem.Text = "Separate by Space";
            this.separateBySpaceToolStripMenuItem.ToolTipText = "e.g. 00 0F FF";
            this.separateBySpaceToolStripMenuItem.Click += new System.EventHandler(this.copyHexSpaceSeparator_Click);
            // 
            // separateByCommaToolStripMenuItem
            // 
            this.separateByCommaToolStripMenuItem.Name = "separateByCommaToolStripMenuItem";
            this.separateByCommaToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.separateByCommaToolStripMenuItem.Text = "Separate by Comma";
            this.separateByCommaToolStripMenuItem.Click += new System.EventHandler(this.separateByCommaToolStripMenuItem_Click);
            // 
            // cByteArrayToolStripMenuItem
            // 
            this.cByteArrayToolStripMenuItem.Name = "cByteArrayToolStripMenuItem";
            this.cByteArrayToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.cByteArrayToolStripMenuItem.Text = "C Byte Array";
            this.cByteArrayToolStripMenuItem.ToolTipText = "e.g. 0x00, 0xF0, 0xFF";
            this.cByteArrayToolStripMenuItem.Click += new System.EventHandler(this.CByteArray_Click);
            // 
            // cArrayToolStripMenuItem
            // 
            this.cArrayToolStripMenuItem.Name = "cArrayToolStripMenuItem";
            this.cArrayToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.cArrayToolStripMenuItem.Text = "C# Array";
            this.cArrayToolStripMenuItem.Click += new System.EventHandler(this.cArrayToolStripMenuItem_Click);
            // 
            // javaArrayToolStripMenuItem
            // 
            this.javaArrayToolStripMenuItem.Name = "javaArrayToolStripMenuItem";
            this.javaArrayToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.javaArrayToolStripMenuItem.Text = "Java Array";
            this.javaArrayToolStripMenuItem.Click += new System.EventHandler(this.javaArrayToolStripMenuItem_Click);
            // 
            // pasteBinaryToolStripMenuItem
            // 
            this.pasteBinaryToolStripMenuItem.Name = "pasteBinaryToolStripMenuItem";
            this.pasteBinaryToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteBinaryToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.pasteBinaryToolStripMenuItem.Text = "Paste";
            this.pasteBinaryToolStripMenuItem.Click += new System.EventHandler(this.pasteBinaryToolStripMenuItem_Click);
            // 
            // pasteHEXTextToolStripMenuItem
            // 
            this.pasteHEXTextToolStripMenuItem.Name = "pasteHEXTextToolStripMenuItem";
            this.pasteHEXTextToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.V)));
            this.pasteHEXTextToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.pasteHEXTextToolStripMenuItem.Text = "Paste HEX Text";
            this.pasteHEXTextToolStripMenuItem.Click += new System.EventHandler(this.pasteHEXTextToolStripMenuItem_Click);
            // 
            // pasteDecimalToolStripMenuItem
            // 
            this.pasteDecimalToolStripMenuItem.Name = "pasteDecimalToolStripMenuItem";
            this.pasteDecimalToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.pasteDecimalToolStripMenuItem.Text = "Paste Decimal";
            this.pasteDecimalToolStripMenuItem.Click += new System.EventHandler(this.pasteDecimalToolStripMenuItem_Click);
            // 
            // saveToFileToolStripMenuItem
            // 
            this.saveToFileToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToFileToolStripMenuItem.Image")));
            this.saveToFileToolStripMenuItem.Name = "saveToFileToolStripMenuItem";
            this.saveToFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToFileToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.saveToFileToolStripMenuItem.Text = "Save to File";
            this.saveToFileToolStripMenuItem.Click += new System.EventHandler(this.saveToFileToolStripMenuItem_Click);
            // 
            // loadFromFileToolStripMenuItem
            // 
            this.loadFromFileToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("loadFromFileToolStripMenuItem.Image")));
            this.loadFromFileToolStripMenuItem.Name = "loadFromFileToolStripMenuItem";
            this.loadFromFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.loadFromFileToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.loadFromFileToolStripMenuItem.Text = "Load from File";
            this.loadFromFileToolStripMenuItem.Click += new System.EventHandler(this.loadFromFileToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(221, 6);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.toolStripMenuItem3.Size = new System.Drawing.Size(224, 22);
            this.toolStripMenuItem3.Text = "Select All";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // ExtendedHexEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.hexBox);
            this.Controls.Add(this.panel1);
            this.Name = "ExtendedHexEditor";
            this.Size = new System.Drawing.Size(814, 226);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbSelection;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbOffset;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbTotalSize;
        private System.Windows.Forms.Label labelTotalSize;
        private Be.Windows.Forms.HexBox hexBox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem copyASCIIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem noSeparatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem separateBySpaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteBinaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteHEXTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadFromFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem cByteArrayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cArrayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem javaArrayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem separateByCommaToolStripMenuItem;
        private System.Windows.Forms.Label valueSignedLE;
        private System.Windows.Forms.Label valueUnsignedLE;
        private System.Windows.Forms.Label labelSignedLE;
        private System.Windows.Forms.Label labelUnsignedLE;
        private System.Windows.Forms.Label valueSignedBE;
        private System.Windows.Forms.Label valueUnsignedBE;
        private System.Windows.Forms.Label labelSignedBE;
        private System.Windows.Forms.Label labelUnsignedBE;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem copyLabelMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteLabelMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteDecimalToolStripMenuItem;
    }
}
