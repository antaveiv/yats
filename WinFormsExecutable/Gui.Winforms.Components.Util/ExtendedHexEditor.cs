﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Be.Windows.Forms;
using yats.Utilities;
using System.IO;

namespace yats.Gui.Winforms.Components.Util
{
    public partial class ExtendedHexEditor : UserControl
    {
        private byte[] bytes;

        public ExtendedHexEditor()
        {
            InitializeComponent();
            lbOffset.Text = "";
            lbSelection.Text = "";
            lbTotalSize.Text = "";
            if (this.DesignMode == false)
            {
                valueSignedBE.Text = valueUnsignedBE.Text = valueSignedLE.Text = valueUnsignedLE.Text = "";
            }
        }

        void ByteProvider_LengthChanged(object sender, EventArgs e)
        {
            if (hexBox.ByteProvider != null)
            {
                lbTotalSize.Text = string.Format("{0}B (0x{0:X})", hexBox.ByteProvider.Length);
            }
            else
            {
                lbTotalSize.Text = string.Format("{0}B (0x{0:X})", 0);
            }
        }

        public ExtendedHexEditor(byte[] bytes) : this()
        {
            SetBytes(bytes);
        }

        public void SetBytes(byte[] bytes)
        {
            this.bytes = bytes;
            this.ByteProvider = new DynamicByteProvider(bytes);
        }

        public bool ReadOnly
        {
            get
            {
                return hexBox.ReadOnly;
            }
            set
            {
                pasteBinaryToolStripMenuItem.Visible = !value;
                pasteHEXTextToolStripMenuItem.Visible = !value;
                loadFromFileToolStripMenuItem.Visible = !value;
                pasteLabelMenuItem.Visible = !value;
                pasteDecimalToolStripMenuItem.Visible = !value;
                hexBox.ReadOnly = value;
            }
        }

        public IByteProvider ByteProvider
        {
            get { return hexBox.ByteProvider; }
            set
            {
                if (hexBox.ByteProvider != null)
                {
                    hexBox.ByteProvider.LengthChanged -= ByteProvider_LengthChanged;
                }
                hexBox.ByteProvider = value;
                if (hexBox.ByteProvider != null)
                {
                    hexBox.ByteProvider.LengthChanged += ByteProvider_LengthChanged;
                    ByteProvider_LengthChanged(this, null);
                }
            }
        }

        private void HexBox_SelectionLengthChanged(object sender, EventArgs e)
        {
            lbSelection.Text = string.Format("{0}B (0x{0:X})", hexBox.SelectionLength);
            copyASCIIToolStripMenuItem.Enabled = hexBox.SelectionLength > 0;
            noSeparatorToolStripMenuItem.Enabled = hexBox.SelectionLength > 0;
            separateBySpaceToolStripMenuItem.Enabled = hexBox.SelectionLength > 0;
            if (hexBox.SelectionLength > 0 && hexBox.SelectionLength <= 8)
            {
                byte [] bytes = GetSelection();
                valueSignedBE.Text = bytes.InterpretByteArray(true, false).ToString();
                valueUnsignedBE.Text = bytes.InterpretByteArray(false, false).ToString();
                valueSignedLE.Text = bytes.InterpretByteArray(true, true).ToString();
                valueUnsignedLE.Text = bytes.InterpretByteArray(false, true).ToString();
            }
            else
            {
                valueSignedLE.Text = valueUnsignedLE.Text = valueSignedBE.Text = valueUnsignedBE.Text = "";
            }
        }

        private void hexBox_SelectionStartChanged(object sender, EventArgs e)
        {
            lbOffset.Text = string.Format("{0}B (0x{0:X})", hexBox.SelectionStart);
        }
        
        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            hexBox.SelectAll();
        }

        private void copyASCIIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var bt = GetSelection();
            if (bt.IsNullOrEmpty()) return;

            Clipboard.SetData(System.Windows.Forms.DataFormats.Text, Encoding.ASCII.GetString(bt));
        }

        private byte[] GetSelection()
        {
            var bt = (hexBox.ByteProvider as DynamicByteProvider).Bytes.ToArray();
            if (hexBox.SelectionLength == bt.Length || hexBox.SelectionLength == 0)
            {
                // no selection - return all
                return bt;
            }
            return ByteArray.Copy(bt, (int)hexBox.SelectionStart, (int)hexBox.SelectionLength);
        }

        private void copyHexNoSeparator_Click(object sender, EventArgs e)
        {
            var bt = GetSelection();
            if (bt.IsNullOrEmpty()) return;

            Clipboard.SetData(System.Windows.Forms.DataFormats.Text, bt.ToHexString());
        }

        private void copyHexSpaceSeparator_Click(object sender, EventArgs e)
        {
            var bt = GetSelection();
            if (bt.IsNullOrEmpty()) return;

            Clipboard.SetData(System.Windows.Forms.DataFormats.Text, bt.ToHexString(" "));
        }
        
        private void CByteArray_Click(object sender, EventArgs e)
        {
            var bt = GetSelection();
            if (bt.IsNullOrEmpty()) return;

            Clipboard.SetData(System.Windows.Forms.DataFormats.Text, ByteArray.ToCArrayFull(bt));
        }

        private void cArrayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var bt = GetSelection();
            if (bt.IsNullOrEmpty()) return;

            Clipboard.SetData(System.Windows.Forms.DataFormats.Text, ByteArray.ToCSharpArrayFull(bt));
        }

        private void javaArrayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var bt = GetSelection();
            if (bt.IsNullOrEmpty()) return;

            Clipboard.SetData(System.Windows.Forms.DataFormats.Text, ByteArray.ToJavaArrayFull(bt));
        }

        private void separateByCommaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var bt = GetSelection();
            if (bt.IsNullOrEmpty()) return;

            Clipboard.SetData(System.Windows.Forms.DataFormats.Text, ByteArray.ToCHexArray(bt));
        }

        private void paste(byte[] bytes)
        {
            if (ReadOnly)
            {
                return;
            }
            try
            {
                hexBox.ByteProvider.DeleteBytes(hexBox.SelectionStart, hexBox.SelectionLength);
                hexBox.ByteProvider.InsertBytes(hexBox.SelectionStart, bytes);
                hexBox.Invalidate();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pasteBinaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var bytes = Encoding.ASCII.GetBytes(Clipboard.GetText());
            paste(bytes);
        }

        private void pasteHEXTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var bytes = ByteArray.HexStringToBytes(Clipboard.GetText());
                paste(bytes);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void saveToFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var bt = GetSelection();
                if (bt.IsNullOrEmpty()) return;

                var dlg = new SaveFileDialog();
                if (dlg.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                File.WriteAllBytes(dlg.FileName, bt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void loadFromFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var dlg = new OpenFileDialog();
                if (dlg.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                byte[] bytes = File.ReadAllBytes(dlg.FileName);
                DynamicByteProvider provider = new DynamicByteProvider(bytes);
                hexBox.ByteProvider = provider;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ProcessValueDoubleClick(bool signed, bool littleEndian)
        {
            if (ReadOnly)
            {
                return;
            }
            if (hexBox.SelectionLength > 0 && hexBox.SelectionLength <= 8)
            {
                byte[] bytes = GetSelection();

                decimal currentValue = bytes.InterpretByteArray(signed, littleEndian);
                string valueAsString = currentValue.ToString();

                decimal min = 0;
                decimal max = (decimal)(Math.Pow(2, bytes.Length * 8 - 0) - 1);

                if (signed)
                {
                    min = -(decimal)(Math.Pow(2, bytes.Length * 8 - 1));
                    max = (decimal)(Math.Pow(2, bytes.Length * 8 - 1) - 1);
                }

                do
                {
                    if (InputBox.GetDecimal("Edit", "Decimal value", min, max, ref currentValue) != DialogResult.OK)
                    {
                        return;
                    }
                    try
                    {
                        bytes = currentValue.ToByteArray(bytes.Length, signed, littleEndian);
                        break;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                } while (true);

                paste(bytes);
                HexBox_SelectionLengthChanged(null, null);
            }
        }

        private void labelUnsignedBE_DoubleClick(object sender, EventArgs e)
        {
            ProcessValueDoubleClick(false, false);
        }

        private void labelUnsignedLE_DoubleClick(object sender, EventArgs e)
        {
            ProcessValueDoubleClick(false, true);
        }

        private void labelSignedLE_DoubleClick(object sender, EventArgs e)
        {
            ProcessValueDoubleClick(true, true);
        }

        private void labelSignedBE_DoubleClick(object sender, EventArgs e)
        {
            ProcessValueDoubleClick(true, false);
        }

        private void labelTotalSize_DoubleClick(object sender, EventArgs e)
        {
            if (ReadOnly)
            {
                return;
            }
            int newLength = (int)hexBox.ByteProvider.Length;
            if (InputBox.GetInt("Length", "Data length", 0, int.MaxValue, ref newLength) != DialogResult.OK)
            {
                return; // canceled
            }
            ChangeEditedContentsSize(newLength);
        }

        private void copyLabelMenuItem_Click(object sender, EventArgs e)
        {
            // Try to cast the sender to a ToolStripItem
            ToolStripItem menuItem = sender as ToolStripItem;
            if (menuItem == null)
            {
                return;
            }
            // Retrieve the ContextMenuStrip that owns this ToolStripItem
            ContextMenuStrip owner = menuItem.Owner as ContextMenuStrip;
            if (owner == null)
            {
                return;
            }
            // Get the control that is displaying this context menu
            Control sourceControl = owner.SourceControl;
            Label sourceLabel = sourceControl as Label;

            Clipboard.SetText(sourceLabel.Text);
        }

        private void pasteLabelMenuItem_Click(object sender, EventArgs e)
        {
            if (ReadOnly)
            {
                return;
            }
            // Try to cast the sender to a ToolStripItem
            ToolStripItem menuItem = sender as ToolStripItem;
            if (menuItem == null)
            {
                return;
            }
            // Retrieve the ContextMenuStrip that owns this ToolStripItem
            ContextMenuStrip owner = menuItem.Owner as ContextMenuStrip;
            if (owner == null)
            {
                return;
            }
            // Get the control that is displaying this context menu
            Control sourceControl = owner.SourceControl;
            Label sourceLabel = sourceControl as Label;
            
            if (sourceLabel == valueSignedBE || sourceLabel == valueSignedLE || sourceLabel == valueUnsignedBE|| sourceLabel == valueUnsignedLE){
                bool signed = sourceLabel == valueSignedBE || sourceLabel == valueSignedLE ;
                bool littleEndian = sourceLabel == valueSignedLE || sourceLabel == valueUnsignedLE;
                if (hexBox.SelectionLength > 0 && hexBox.SelectionLength <= 8)
                {
                    byte[] bytes = GetSelection();
                    try
                    {
                        decimal pastedValue = decimal.Parse(Clipboard.GetText());
                        bytes = pastedValue.ToByteArray(bytes.Length, signed, littleEndian);
                        paste(bytes);
                        HexBox_SelectionLengthChanged(null, null);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                return;
            }
            if (sourceLabel == lbTotalSize)
            {
                try
                {
                    int pastedValue = int.Parse(Clipboard.GetText());
                    ChangeEditedContentsSize(pastedValue);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ChangeEditedContentsSize(int newSize)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() {  ChangeEditedContentsSize(newSize); }));
                return;
            }
            if (newSize > hexBox.ByteProvider.Length)
            {
                hexBox.ByteProvider.InsertBytes(hexBox.ByteProvider.Length, new byte[newSize - hexBox.ByteProvider.Length]);
                hexBox.Invalidate();
            }
            else if (newSize < hexBox.ByteProvider.Length)
            {
                hexBox.ByteProvider.DeleteBytes(newSize, hexBox.ByteProvider.Length - newSize);
                hexBox.Invalidate();
            }  
        }

        public event EventHandler<EventArgs> HexTextChanged;

        private void hexBox_TextChanged(object sender, EventArgs e)
        {
            if (HexTextChanged != null)
            {
                HexTextChanged(sender, e);
            }
        }

        private void pasteDecimalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ReadOnly)
            {
                return;
            }
            try
            {
                var bytes = ByteArray.DecimalStringToBytes(Clipboard.GetText());
                paste(bytes);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
