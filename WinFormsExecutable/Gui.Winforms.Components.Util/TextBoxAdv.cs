﻿using System.Windows.Forms;

namespace yats.Gui.Winforms.Components.Util
{
    public class TextBoxAdv : TextBox
    {
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.Control && (e.KeyCode == Keys.A))
            {
                SelectAll();
                e.SuppressKeyPress = true;
                e.Handled = true;
                return;
            }
            if (e.Control && (e.KeyCode == Keys.C))
            {
                if (SelectionLength > 0)
                {
                    Clipboard.SetText(SelectedText);
                }
                e.SuppressKeyPress = true;
                e.Handled = true;
                return;
            }
            if (e.Control && (e.KeyCode == Keys.V) && (this.ReadOnly == false))
            {
                this.Paste();
                e.SuppressKeyPress = true;
                e.Handled = true;
                return;
            }
            
            base.OnKeyDown(e);
        }
    }
}
