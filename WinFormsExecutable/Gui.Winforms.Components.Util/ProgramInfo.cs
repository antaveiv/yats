﻿/** 
 * Project:         ProgramInfo 
 * Copyright:       Copyright (C) 2009, Rob Kennedy
 * Create Date:     2008-14-07 4:00 PM
 * Modified Date:   2008-14-07 4:00 PM
 * Comments:        2008-14-07 4:00 PM RK  Ignore SA1633
 *
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>. 
 * 
 * NOTE: This class and other code can be found at http://robkennedy.com/
 */

namespace yats.Gui.Winforms.Components.Util
{

    using System.Reflection;



    /// <summary>
    /// ProgramInfo provides access to the assemblies product information
    /// </summary>

    public class ProgramInfo
    {

        /// <summary>

        /// Gets the product name

        /// </summary>

        public static string ProductName
        {

            get
            {

                string resultValue = string.Empty;



                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);



                if (attributes != null && attributes.Length > 0)
                {

                    resultValue = ((AssemblyProductAttribute)attributes[0]).Product;

                }



                return resultValue;

            }

        }



        /// <summary>

        /// Gets the product description

        /// </summary>

        public static string Description
        {

            get
            {

                string resultValue = string.Empty;



                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);



                if (attributes != null && attributes.Length > 0)
                {

                    resultValue = ((AssemblyDescriptionAttribute)attributes[0]).Description;

                }



                return resultValue;

            }

        }



        /// <summary>

        /// Gets the product trademark

        /// </summary>

        public static string Trademark
        {

            get
            {

                string resultValue = string.Empty;



                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTrademarkAttribute), false);



                if (attributes != null && attributes.Length > 0)
                {

                    resultValue = ((AssemblyTrademarkAttribute)attributes[0]).Trademark;

                }



                return resultValue;

            }

        }



        /// <summary>

        /// Gets the product copyright

        /// </summary>

        public static string Copyright
        {

            get
            {

                string resultValue = string.Empty;



                // Get all Copyright attributes on this assembly

                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);



                if (attributes != null && attributes.Length > 0)
                {

                    // If there is a Copyright attribute, return its value

                    resultValue = ((AssemblyCopyrightAttribute)attributes[0]).Copyright;

                }



                return resultValue;

            }

        }



        /// <summary>

        /// Gets the product version

        /// </summary>

        public static string Version
        {

            get
            {

                string resultValue = string.Empty;



                // Get all Copyright attributes on this assembly

                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyVersionAttribute), false);



                if (attributes != null && attributes.Length > 0)
                {

                    // If there is a Copyright attribute, return its value

                    resultValue = ((AssemblyVersionAttribute)attributes[0]).Version;

                }



                return resultValue;

            }

        }



        /// <summary>

        /// Gets the product file version

        /// </summary>

        public static string FileVersion
        {

            get
            {

                string resultValue = string.Empty;



                // Get all Copyright attributes on this assembly

                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyFileVersionAttribute), false);



                if (attributes != null && attributes.Length > 0)
                {

                    // If there is a Copyright attribute, return its value

                    resultValue = ((AssemblyFileVersionAttribute)attributes[0]).Version;

                }



                return resultValue;

            }

        }
        static public string AssemblyGuid
        {
            get
            {
                object[] attributes = Assembly.GetEntryAssembly().GetCustomAttributes(typeof(System.Runtime.InteropServices.GuidAttribute), false);
                if (attributes.Length == 0)
                {
                    return string.Empty;
                }
                return ((System.Runtime.InteropServices.GuidAttribute)attributes[0]).Value;
            }
        }

    }

}