﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Windows.Forms;

namespace yats.Gui.Winforms.Components.Util
{
    public class FormUtil
    {
        /// <summary>
        /// Toggle FullScreen state of a form
        /// </summary>
        /// <param name="form"></param>
        /// <returns>New full-screen status (true if was set to full screen mode)</returns>
        public static bool ToggleFullScreen(Form form)
        {
            bool isFullScreen = form.FormBorderStyle == FormBorderStyle.None && form.WindowState == FormWindowState.Maximized;
            if(!isFullScreen)
            {
                form.FormBorderStyle = FormBorderStyle.None;
                form.WindowState = FormWindowState.Maximized;
            }
            else
            {
                form.FormBorderStyle = FormBorderStyle.Sizable;
                form.WindowState = FormWindowState.Normal;
            }
            return !isFullScreen;
        }

        public static bool ToggleOnTop(Form form)
        {
            bool isOnTop = form.TopMost;
            form.TopMost = !isOnTop;
            return !isOnTop;
        }
    }
}
