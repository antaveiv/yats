﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace yats.Gui.Winforms.Components.Util
{
    public class DoubleBufferPanel : Panel
    {
        public DoubleBufferPanel()
        {
            // Set the value of the double-buffering style bits to true.
            this.SetStyle(ControlStyles.DoubleBuffer |
            ControlStyles.UserPaint |
            ControlStyles.AllPaintingInWmPaint |
            ControlStyles.ResizeRedraw,
            true);

            this.UpdateStyles();
        }
    }
}
