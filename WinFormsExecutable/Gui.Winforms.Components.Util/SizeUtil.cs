﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Drawing;

namespace yats.Gui.Winforms.Components.Util
{
    public static class SizeUtil
    {
        public static Size Min(Size a, Size b)
        {
            return new Size(Math.Min(a.Width, b.Width), Math.Min(a.Height, b.Height));
        }

        public static Size Max(Size a, Size b)
        {
            return new Size(Math.Max(a.Width, b.Width), Math.Max(a.Height, b.Height));
        }

        public static Size Add(this Size a, Size b)
        {
            return new Size(a.Width + b.Width, a.Height + b.Height);
        }

        public static Size Add(this Size a, Point b)
        {
            return a.Add(new Size(b));
        }

        public static Size Add(this Size a, int width, int heigth)
        {
            return a.Add(new Size(width, heigth));
        }

        public static Size Subtract(this Size a, Size b)
        {
            return new Size(a.Width - b.Width, a.Height - b.Height);
        }
    }
}
