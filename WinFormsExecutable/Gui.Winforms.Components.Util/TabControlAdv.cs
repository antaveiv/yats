﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Security.Permissions;
using System.Windows.Forms;

namespace yats.Gui.Winforms.Components.Util
{
    public class TabControlAdv : TabControl
    {
        private int mPages = 0;
        private void checkOnePage()
        {
            if (IsHandleCreated)
            {
                int pages = mPages;
                mPages = this.TabCount;
                if ((pages == 1 && mPages > 1) || (pages > 1 && mPages == 1))
                    this.RecreateHandle();
            }
        }
        protected override void OnControlAdded(ControlEventArgs e)
        {
            base.OnControlAdded(e);
            checkOnePage();
        }
        protected override void OnControlRemoved(ControlEventArgs e)
        {
            base.OnControlRemoved(e);
            checkOnePage();
        }

        [SecurityPermissionAttribute(SecurityAction.InheritanceDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        [SecurityPermissionAttribute(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        protected override void WndProc(ref Message m)
        {
            // Hide tabs by trapping the TCM_ADJUSTRECT message
            if (m.Msg == 0x1328 && !DesignMode && this.TabCount == 1) m.Result = (IntPtr)1;
            else base.WndProc(ref m);
        }
    }
}
