﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace yats.Gui.Winforms.Components.Util
{
    public static class InputBox
    {
        public static DialogResult Get(string title, string promptText, ref string value)
        {
            Form form = new Form( );
            Label label = new Label( );
            TextBox textBox = new TextBox( );
            Button buttonOk = new Button( );
            Button buttonCancel = new Button( );

            form.Text = title;
            form.ShowInTaskbar = false;
            label.Text = promptText;
            textBox.Text = value;
            
            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds( 9, 20, 372, 13 );
            textBox.SetBounds( 12, 36, 372, 20 );
            buttonOk.SetBounds( 228, 72, 75, 23 );
            buttonCancel.SetBounds( 309, 72, 75, 23 );

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size( 396, 107 );
            form.Controls.AddRange( new Control[] { label, textBox, buttonOk, buttonCancel } );
            form.ClientSize = new Size( Math.Max( 300, label.Right + 10 ), form.ClientSize.Height );
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog( );
            value = textBox.Text;
            return dialogResult;
        }

        public static DialogResult Get(string title, string promptText, string[] choices, ref string value)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = title;
            form.ShowInTaskbar = false;
            label.Text = promptText;
            textBox.Text = value;

            textBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            textBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            var source = new AutoCompleteStringCollection();
            source.AddRange(choices);
            textBox.AutoCompleteCustomSource = source;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }

        public static DialogResult GetMultiLine(string title, string promptText, ref string value)
        {
            Form form = new Form( );
            Label label = new Label( );
            TextBox textBox = new TextBox( );
            Button buttonOk = new Button( );
            Button buttonCancel = new Button( );

            form.Text = title;
            form.ShowInTaskbar = false;
            label.Text = promptText;
            textBox.Multiline = true;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            textBox.SetBounds( 12, 36, 400, 300 );
            label.SetBounds( 9, 20, textBox.Width, 13 );

            buttonOk.SetBounds( textBox.Left + textBox.Width - buttonCancel.Width - 10 - buttonOk.Width, textBox.Top + textBox.Height + 10, buttonOk.Width, 23 );
            buttonCancel.SetBounds( textBox.Left + textBox.Width - buttonCancel.Width, textBox.Top + textBox.Height + 10, buttonCancel.Width, 23 );

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Top;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size( textBox.Width + 20, textBox.Height + 76 );
            form.Controls.AddRange( new Control[] { label, textBox, buttonOk, buttonCancel } );
            //form.ClientSize = new Size( Math.Max( 300, label.Right + 10 ), form.ClientSize.Height );
            form.FormBorderStyle = FormBorderStyle.Sizable;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimumSize = new Size( 300, 200 );
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog( );
            value = textBox.Text;
            return dialogResult;
        }

        /// <summary>
        /// Show dialog to input an int value. Simple expressions can be evaluated
        /// </summary>
        /// <param name="title">Dialog text</param>
        /// <param name="promptText">Prompt inside the dialog</param>
        /// <param name="min">Minimum expected value</param>
        /// <param name="max">Maximum expected value</param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DialogResult GetInt(string title, string promptText, int min, int max, ref int value)
        {
            string stringValue = value.ToString();
            do
            {
                DialogResult dlgResult = Get(title, promptText, ref stringValue);
                if (dlgResult != DialogResult.OK)
                {
                    return dlgResult;
                }
                
                bool parseOk = false;
                parseOk = int.TryParse(stringValue, out value);

                if (!parseOk)
                {
                    try
                    {
                        value = ExpressionEvaluator.EvaluateToRoundedInt(stringValue);
                        parseOk = true;
                    }
                    catch (Exception)
                    {                        
                    }
                }
                if (!parseOk)
                {
                    MessageBox.Show("Invalid format");
                    continue;
                }
                if (value < min)
                {
                    MessageBox.Show("Value should be more or equal to " + min);
                    continue;
                }
                if (value > max)
                {
                    MessageBox.Show("Value should be less or equal to " + max);
                    continue;
                }
                return DialogResult.OK;
            } while (true);
        }

        /// <summary>
        /// Show dialog to input a decimal value. Simple expressions can be evaluated
        /// </summary>
        /// <param name="title">Dialog text</param>
        /// <param name="promptText">Prompt inside the dialog</param>
        /// <param name="min">Minimum expected value</param>
        /// <param name="max">Maximum expected value</param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DialogResult GetDecimal(string title, string promptText, decimal min, decimal max, ref decimal value)
        {
            string stringValue = value.ToString();
            do
            {
                DialogResult dlgResult = Get(title, promptText, ref stringValue);
                if (dlgResult != DialogResult.OK)
                {
                    return dlgResult;
                }

                bool parseOk = false;
                parseOk = decimal.TryParse(stringValue, out value);

                if (!parseOk)
                {
                    try
                    {
                        value = ExpressionEvaluator.EvaluateToDecimal(stringValue);
                        parseOk = true;
                    }
                    catch (Exception)
                    {
                    }
                }
                if (!parseOk)
                {
                    MessageBox.Show("Invalid format");
                    continue;
                }
                if (value < min)
                {
                    MessageBox.Show("Value should be more or equal to " + min);
                    continue;
                }
                if (value > max)
                {
                    MessageBox.Show("Value should be less or equal to " + max);
                    continue;
                }
                return DialogResult.OK;
            } while (true);
        }
    }
}