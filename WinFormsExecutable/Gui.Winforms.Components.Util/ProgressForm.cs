﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Windows.Forms;

namespace yats.Gui.Winforms.Components.Util
{
    public partial class ProgressForm : Form
    {
        public ProgressForm()
        {
            InitializeComponent( );
        }

        public static ProgressForm Show(string title, string text, int min, int max, int progress)
        {
            ProgressForm dlg = new ProgressForm( );
            dlg.Update( title, text, min, max, progress );
            dlg.Show( );
            return dlg;
        }


        public void Update(int progress)
        {
            if(this.IsDisposed)
            {
                return;
            }
            if(this.InvokeRequired)
            {
                BeginInvoke( new MethodInvoker( delegate() { Update(progress); } ) );
                return;
            }

            progressBar1.Value = Between( progress );
        }

        public void Update(int max, int progress)
        {
            if(this.IsDisposed)
            {
                return;
            }
            if(this.InvokeRequired)
            {
                BeginInvoke( new MethodInvoker( delegate() { Update( max, progress ); } ) );
                return;
            }

            progressBar1.Maximum = max;
            progressBar1.Value = Between( progress );
        }

        public void Update(string title, string text, int progress)
        {
            if(this.IsDisposed)
            {
                return;
            }
            if(this.InvokeRequired)
            {
                BeginInvoke( new MethodInvoker( delegate() { Update( progress ); } ) );
                return;
            }

            this.Text = title;
            label1.Text = text;
            progressBar1.Value = Between( progress );
        }

        public void Update(string title, string text, int min, int max, int progress)
        {
            if(this.IsDisposed)
            {
                return;
            }
            if(this.InvokeRequired)
            {
                BeginInvoke( new MethodInvoker( delegate() { Update( progress ); } ) );
                return;
            }

            this.Text = title;
            label1.Text = text;
            progressBar1.Minimum = min;
            progressBar1.Maximum = max;
            progressBar1.Value = Between( progress );
        }

        int Between(int value)
        {
            if(value < progressBar1.Minimum)
            {
                return progressBar1.Minimum;
            }
            if(value > progressBar1.Maximum)
            {
                return progressBar1.Maximum;
            }
            return value;
        }
    }
}
