﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using yats.ExecutionQueue;
using yats.TestCase.Parameter;
using yats.TestRun;

namespace yats.Gui.Winforms.Components.ParameterEditor
{
    public partial class ParameterEditorForm : Form
    {
        public ParameterEditorForm()
        {
            InitializeComponent();
        }

        public ParameterEditorForm(List<TestStep> steps, IGlobalParameterCollection globals, ITestRun testRun)
            : this()
        {
            this.parameterEditorPanel.SetSteps( steps, globals, testRun );
            globals.Changed += globals_Changed;
        }


        public ITestRun TestRun
        {
            get { return parameterEditorPanel.TestRun; }
        }

        void globals_Changed(object sender, EventArgs e)
        {
            if(this.InvokeRequired)
            {
                BeginInvoke( new MethodInvoker( delegate() { globals_Changed( sender, e ); } ) );
                return;
            }  

            parameterEditorPanel.UpdateModel( );
        }

		private void ParameterEditorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            parameterEditorPanel.globals.Changed -= globals_Changed;
        }

        public void SetSteps(List<TestStep> steps, IGlobalParameterCollection globals, ITestRun testRun)
        {
            if(this.InvokeRequired)
            {
                BeginInvoke( new MethodInvoker( delegate() { SetSteps( steps, globals, testRun); } ) );
                return;
            }  

            this.parameterEditorPanel.SetSteps( steps, globals, testRun );
        }

        
        public void UpdateModel(List<TestStep> steps)
        {
            if(this.InvokeRequired)
            {
                BeginInvoke( new MethodInvoker( delegate() { UpdateModel( steps ); } ) );
                return;
            }  

            parameterEditorPanel.UpdateModel();
        }

        public void SetRunning(bool isRunning)
        {
            if(this.InvokeRequired)
            {
                Invoke( new MethodInvoker( delegate() { SetRunning( isRunning ); } ) );
                return;
            }
            
            if(isRunning)
            {
                this.Text = "Parameters - running";
            } 
            else
            {
                this.Text = "Parameters";
            }

            parameterEditorPanel.SetRunning( isRunning );
        }

        private void ParameterEditorForm_ResizeBegin(object sender, EventArgs e)
        {
            parameterEditorPanel.ResizeBegin();
        }

        private void ParameterEditorForm_ResizeEnd(object sender, EventArgs e)
        {
            parameterEditorPanel.ResizeEnd();
        }

        private void ParameterEditorForm_SizeChanged(object sender, EventArgs e)
        {
            parameterEditorPanel.ResizeEnd();
        }

        private void ParameterEditorForm_Layout(object sender, LayoutEventArgs e)
        {
            parameterEditorPanel.ResizeBegin();
        }
    }
}
