﻿namespace yats.Gui.Winforms.Components.ParameterEditor
{
    partial class ParameterEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ParameterEditorForm));
            this.parameterEditorPanel = new yats.Gui.Winforms.Components.ParameterEditor.ParameterEditorPanel();
            this.SuspendLayout();
            // 
            // parameterEditorPanel
            // 
            this.parameterEditorPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parameterEditorPanel.Location = new System.Drawing.Point(0, 0);
            this.parameterEditorPanel.MinimumSize = new System.Drawing.Size(0, 55);
            this.parameterEditorPanel.Name = "parameterEditorPanel";
            this.parameterEditorPanel.Size = new System.Drawing.Size(841, 438);
            this.parameterEditorPanel.TabIndex = 0;
            // 
            // ParameterEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 438);
            this.Controls.Add(this.parameterEditorPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ParameterEditorForm";
            this.Text = "Parameters";
            this.ResizeBegin += new System.EventHandler(this.ParameterEditorForm_ResizeBegin);
            this.SizeChanged += new System.EventHandler(this.ParameterEditorForm_SizeChanged);
            this.Layout += new System.Windows.Forms.LayoutEventHandler(this.ParameterEditorForm_Layout);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ParameterEditorForm_FormClosing);
            this.ResizeEnd += new System.EventHandler(this.ParameterEditorForm_ResizeEnd);
            this.ResumeLayout(false);

        }

        #endregion

        private ParameterEditorPanel parameterEditorPanel;
    }
}