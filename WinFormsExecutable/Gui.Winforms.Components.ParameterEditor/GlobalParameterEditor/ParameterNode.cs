﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using Aga.Controls.Tree;
using yats.TestCase.Parameter;

namespace yats.Gui.Winforms.Components.ParameterEditor.GlobalParameterEditor
{
    internal class ParameterNode : Aga.Controls.Tree.Node<ParameterNode>
    {
        public string ParamName
        {
            get
            {
                return Parameter.Name;
            }
        }

        public string ParamType
        {
            get
            {
                return ParameterTypeCollection.Instance.GetDisplayName(Parameter.Value);
            }
        }

        public string ParamValue
        {
            get
            {
                return ParamValueToString.Get(Parameter.Value, null);
            }
        }

        public GlobalParameter Parameter;
        public IGlobalParameters Manager;
        public Guid ValueGuid;
        public ParameterNode(Guid valueGuid, GlobalParameter parameter, IGlobalParameters manager)
        {
            this.ValueGuid = valueGuid;
            this.Parameter = parameter;
            this.Manager = manager;
        }

        public ParameterNode()
        {
        }
    }
}
