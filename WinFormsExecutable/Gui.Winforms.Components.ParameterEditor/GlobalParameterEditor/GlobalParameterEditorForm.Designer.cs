﻿namespace yats.Gui.Winforms.Components.ParameterEditor.GlobalParameterEditor
{
    partial class GlobalParameterEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( GlobalParameterEditorForm ) );
            this.globalParameterListPanel = new yats.Gui.Winforms.Components.ParameterEditor.GlobalParameterEditor.GlobalParameterListPanel( );
            this.SuspendLayout( );
            // 
            // globalParameterListPanel
            // 
            this.globalParameterListPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.globalParameterListPanel.Location = new System.Drawing.Point( 0, 0 );
            this.globalParameterListPanel.Name = "globalParameterListPanel";
            this.globalParameterListPanel.Size = new System.Drawing.Size( 767, 283 );
            this.globalParameterListPanel.TabIndex = 0;
            // 
            // GlobalParameterEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 767, 283 );
            this.Controls.Add( this.globalParameterListPanel );
            this.Icon = ((System.Drawing.Icon)(resources.GetObject( "$this.Icon" )));
            this.Name = "GlobalParameterEditorForm";
            this.ShowInTaskbar = false;
            this.Text = "Global parameters ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.GlobalParameterEditorForm_FormClosing );
            this.ResumeLayout( false );

        }

        #endregion

        private GlobalParameterListPanel globalParameterListPanel;
    }
}