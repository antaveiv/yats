﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using yats.Gui.Winforms.Components.Util;
using yats.TestCase.Parameter;
using System.Reflection;
using Aga.Controls.Tree;

namespace yats.Gui.Winforms.Components.ParameterEditor.GlobalParameterEditor
{
    public partial class GlobalParameterListPanel : UserControl
    {
        private TreeModel<ParameterNode> innerModel;
        private Aga.Controls.Tree.SortedTreeModel sortedModel;

        protected IGlobalParameterCollection mGlobals;

        public GlobalParameterListPanel()
        {
            InitializeComponent();
        }


        public GlobalParameterListPanel(IGlobalParameterCollection globals)
            : this()
        {
            SetParamCollection(globals);
        }

        public void SetParamCollection(IGlobalParameterCollection globals)
        {
            innerModel = new TreeModel<ParameterNode>();
            sortedModel = new Aga.Controls.Tree.SortedTreeModel(innerModel);
            sortedModel.Comparer = new NodeComparer();
            this.mGlobals = globals;
            mGlobals.ValueAdded += mgr_ValueAdded;
            mGlobals.ValueRemoved += mgr_ValueRemoved;
            mGlobals.ValueUpdated += mgr_ValueUpdated;
            tree.Model = sortedModel;

            foreach (var manager in globals.Managers.Values.Where(x => x.Scope == ParameterScope.GLOBAL))
            {
                foreach (var param in manager.Parameters)
                {
                    var node = new ParameterNode(param.Key, param.Value, manager);
                    innerModel.Root.Nodes.Add(node);
                }
            }
        }

        private class NodeComparer : System.Collections.IComparer
        {
            public int Compare(object x, object y)
            {
                return (x as ParameterNode).ParamName.CompareTo((y as ParameterNode).ParamName);
            }
        }

        private void mgr_ValueUpdated(object sender, GlobalParameterEventArgs e)
        {
            try
            {
                if (e.Manager.Scope != ParameterScope.GLOBAL)
                {
                    return;
                }
                innerModel.OnNodesChanged(innerModel.Root.Nodes.First(x => (x as ParameterNode).ValueGuid == e.Id));
            }
            catch { }
        }

        private void mgr_ValueRemoved(object sender, GlobalParameterEventArgs e)
        {
            try
            {
                if (e.Manager.Scope != ParameterScope.GLOBAL)
                {
                    return;
                }
                var node = innerModel.Root.Nodes.First(x => (x as ParameterNode).ValueGuid == e.Id);
                int index = innerModel.Root.Nodes.IndexOf(node);
                innerModel.Root.Nodes.Remove(node);
            }
            catch { }
        }

        private void mgr_ValueAdded(object sender, GlobalParameterEventArgs e)
        {
            try
            {
                if (e.Manager.Scope != ParameterScope.GLOBAL)
                {
                    return;
                }
                var node = new ParameterNode(e.Id, e.Value, e.Manager);
                innerModel.Root.Nodes.Add(node);
            }
            catch { }
        }

        internal void Close()
        {
            tree.Model = null;

            mGlobals.ValueAdded -= mgr_ValueAdded;
            mGlobals.ValueRemoved -= mgr_ValueRemoved;
            mGlobals.ValueUpdated -= mgr_ValueUpdated;
        }

        private void tree_DoubleClick(object sender, EventArgs e)
        {
            editValueToolStripMenuItem_Click(sender, e);
        }

        private void editValueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (tree.SelectedNode == null)
            {
                return;
            }
            ParameterNode node = (tree.SelectedNode.Tag as ParameterNode);
            GlobalParameter parameter = node.Parameter as GlobalParameter;
            if (parameter == null)
            {
                return;
            }
            var clone = parameter.Value.Clone();
            // parameter.value is unwrapped from its global parameter, therefore suitable for editing

            // can not do a full support for multiple parameter value types because the IParameter information is lost in global values. Just the current type is edited
            Dictionary<AbstractParameterValue, AbstractParameterValue> map = new Dictionary<AbstractParameterValue, AbstractParameterValue>();
            map.Add(clone, clone);
            if (ParameterTabEditorForm.Edit(map, out clone))
            {
                parameter.Value = clone;
                node.Manager.Set(node.ValueGuid, parameter);
            }
        }

        private void renameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ParameterNode node = (tree.SelectedNode.Tag as ParameterNode);
            GlobalParameter parameter = node.Parameter as GlobalParameter;
            if (parameter == null)
            {
                return;
            }

            string name = parameter.Name;
            if (InputBox.Get("Global parameter name", "Enter parameter name", ref name) == DialogResult.OK)
            {
                parameter.Name = name;
                mGlobals.RaiseValueUpdated(this, new GlobalParameterEventArgs(node.ValueGuid, parameter, node.Manager));
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ParameterNode node = (tree.SelectedNode.Tag as ParameterNode);
            node.Manager.Remove(node.ValueGuid);
        }

        private void tree_SelectionChanged(object sender, EventArgs e)
        {
            editValueToolStripMenuItem.Enabled = tree.SelectedNodes.Count == 1;
            renameToolStripMenuItem.Enabled = tree.SelectedNodes.Count == 1;
            deleteToolStripMenuItem.Enabled = tree.SelectedNodes.Count == 1;
            //copyToolStripMenuItem.Enabled = tree.SelectedNodes.Count == 1; uncomment when Paste is implemented
        }

        private void tree_ItemDrag(object sender, ItemDragEventArgs e)
        {
            ParameterNode node = (tree.SelectedNode.Tag as ParameterNode);
            if (node != null)
            {
                tree.DoDragDrop(new GlobalValue(node.Manager.ID, node.ValueGuid), DragDropEffects.Link | DragDropEffects.Copy | DragDropEffects.Move);
            }
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                // Try copying selected parameters.
                List<GlobalValue> parametersToCopy = new List<GlobalValue>();
                foreach (var selNode in tree.SelectedNodes)
                {
                    ParameterNode node = (selNode.Tag as ParameterNode);
                    GlobalValue value = new GlobalValue(node.Manager.ID, node.ValueGuid);
                    parametersToCopy.Add(value);
                }
                if (parametersToCopy.Count > 0)
                {
                    new ClipboardObjects.ClipboardGlobalValueContents(parametersToCopy).CopyToClipboard();
                    return;
                }
            }
            catch (Exception ex)
            {
                yats.Utilities.CrashLog.Write(ex, "GlobalParameterListPanel.copyToolStripMenuItem_Click");
            }
        }
    }
}
