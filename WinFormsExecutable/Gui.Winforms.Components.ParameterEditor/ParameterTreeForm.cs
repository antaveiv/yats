﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using yats.ExecutionQueue;
using yats.TestCase.Parameter;
using yats.TestRun.Commands;
using yats.Utilities.Commands;


namespace yats.Gui.Winforms.Components.ParameterEditor
{
    public partial class ParameterTreeForm : DockContent
    {
        public ParameterTreeForm()
        {
            InitializeComponent();
            this.TabText = this.Text;
            parameterTree.ValueChanged += parameterTree_ValueChanged;
        }

        public void SetSteps(object editedObject, IList<TestStep> steps, TestStep topLevelStep, IGlobalParameterCollection globalParameters, ITestExecutionStatus executionStatus, ICommandStack commandStack)
        {
            if (IsDisposed)
            {
                return;
            }

            if(this.InvokeRequired)
            {
                Invoke(new MethodInvoker(delegate() { SetSteps(editedObject, steps, topLevelStep, globalParameters, executionStatus, commandStack); }));
                return;
            }

            this.EditedObject = editedObject;
            parameterTree.SetSteps(steps, topLevelStep, globalParameters, executionStatus, commandStack);
        }
        
        [Obsolete]
        public void UpdateModel(List<TestStep> steps)
        {
            if(this.InvokeRequired)
            {
                BeginInvoke( new MethodInvoker( delegate() { UpdateModel( steps ); } ) );
                return;
            }  
            parameterTree.Invalidate( );
        }
        
        public event EventHandler ValueChanged;

        void parameterTree_ValueChanged(object sender, EventArgs e)
        {
            if(ValueChanged != null)
            {
                ValueChanged( this, null );
            }
        }

        public IParameterProperties ParameterProperties
        {
            get { return parameterTree.mParameterProperties; }
            set { parameterTree.mParameterProperties = value; }
        }

        // Used to identify the TestRun being edited. Do not want a project reference just for that
        public object EditedObject
        {
            get;
            set;
        }
    }
}
