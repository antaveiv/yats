﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Aga.Controls.Tree;
using yats.ExecutionQueue;
using yats.Gui.Winforms.Components.Util;
using yats.TestCase.Parameter;
using yats.Utilities;
using yats.TestRun.Commands;
using System.Diagnostics;
using yats.Utilities.Commands;

namespace yats.Gui.Winforms.Components.ParameterEditor
{
    public partial class ParameterTree : UserControl
    {
        internal IParameterProperties mParameterProperties;//TODO what is this?
        private ITestExecutionStatus mExecutionStatus;
        private ICommandStack mCommandStack = new CommandStack();//normally passed by SetSteps(), initialized here for unit tests
        private IGlobalParameterCollection mGlobalParameters;

        public ParameterTree()
        {
            InitializeComponent();
        }
        
        private void ParameterTree_Load(object sender, EventArgs e)
        {
            if (DesignMode == false)
            {
                btShowTypeDetails.Checked = Properties.Settings.Default.ShowParameterDetailsColumn;
            }
        }

        internal void SetSteps(IList<TestStep> steps, TestStep topLevelStep, IGlobalParameterCollection globalParameters, ITestExecutionStatus executionStatus, ICommandStack commandStack)
        {
            this.mCommandStack = commandStack;
            this.mGlobalParameters = globalParameters;
            if (mExecutionStatus != null)
            {
                mExecutionStatus.OnStatusChange -= RunningStatusChange;
            }
            this.mExecutionStatus = executionStatus;
            mExecutionStatus.OnStatusChange += RunningStatusChange;

            tree.Model = TreeGenerator.GenerateTree(mParameterProperties, globalParameters, steps, topLevelStep);
            tree.ExpandAll();
            UpdateEnabledControls();
        }

        void RunningStatusChange(object sender, TestExecutionStatusEventArgs e)
        {
            if (IsDisposed)
            {
                return;
            }
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(delegate() { RunningStatusChange(sender, e); }));
                return;
            }
            UpdateEnabledControls();
        }
        
        private void tree_DragDrop(object sender, DragEventArgs e)
        {
            if (mExecutionStatus.IsRunning)
            {
                e.Effect = DragDropEffects.None;
                return;
            }

            if (e.Data.GetDataPresent(typeof(ParameterNode)))
            {
                ParameterNode draggedParam = (ParameterNode)(e.Data.GetData(typeof(ParameterNode)));
                ParameterNode targetNode = tree.DropPosition.Node.Tag as ParameterNode;
                if (draggedParam == null || targetNode == null || object.ReferenceEquals(draggedParam, targetNode))
                {
                    return;
                }
                if (!ParamHelper.IsAssignable(draggedParam.Parameter, targetNode.Parameter))
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                switch (e.Effect)
                {
                    case DragDropEffects.Move:
                        mCommandStack.Execute(new CompositeCommand(
                            (bool execute) => { RaiseValueChanged(); },
                            new SetParameterValue(targetNode.Parameter, draggedParam.Parameter.Value),
                            new SetParameterValue(draggedParam.Parameter, null)
                            ));
                        break;
                    case DragDropEffects.Copy:
                        if (draggedParam.Parameter.Value != null)
                        {
                            mCommandStack.Execute(
                                new SetParameterValue(
                                    targetNode.Parameter,
                                    draggedParam.Parameter.Value.Clone(),
                                    () => { RaiseValueChanged(); }
                                    )
                            );
                        }
                        else
                        {
                            mCommandStack.Execute(new SetParameterValue(targetNode.Parameter, null, () => { RaiseValueChanged(); }));
                        }
                        break;
                    case DragDropEffects.Link:
                        mCommandStack.Execute(LinkParameter(draggedParam.Parameter, targetNode.Parameter, true));
                        break;
                }
            }
            else if (e.Data.GetDataPresent(typeof(ResultNode)))
            {
                ResultNode draggedParam = (ResultNode)(e.Data.GetData(typeof(ResultNode)));
                ParameterNode targetNode = tree.DropPosition.Node.Tag as ParameterNode;
                if (draggedParam == null || targetNode == null || object.ReferenceEquals(draggedParam, targetNode))
                {
                    return;
                }
                if (!ParamHelper.IsAssignable(draggedParam.Parameter, targetNode.Parameter))
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                switch (e.Effect)
                {
                    case DragDropEffects.Link:
                        mCommandStack.Execute(LinkResultToParameter(draggedParam.Parameter, targetNode.Parameter, true));
                        break;
                }
            }
            else if (e.Data.GetDataPresent(typeof(SingleStepTreeNode)))
            {
                SingleStepTreeNode draggedNode = (SingleStepTreeNode)(e.Data.GetData(typeof(SingleStepTreeNode)));
                SingleStepTreeNode targetNode = tree.DropPosition.Node.Tag as SingleStepTreeNode;
                if (draggedNode == null || targetNode == null || draggedNode == targetNode)
                {
                    return;
                }
                
                Dictionary<IParameter, IParameter> matchingParameters = GetPossibleParameterMatch(draggedNode.SingleTestStep.Parameters, targetNode.SingleTestStep.Parameters);
                
                switch (e.Effect)
                {
                    case DragDropEffects.Move:
                        {
                            CompositeCommand cmd = new CompositeCommand((bool execute) => { tree.Invalidate(); });
                            foreach (var pair in matchingParameters)
                            {
                                cmd.Add(new SetParameterValue(pair.Value, pair.Key.Value));
                                cmd.Add(new SetParameterValue(pair.Key, null));
                            }
                            mCommandStack.Execute(cmd);
                            break;
                        }
                    case DragDropEffects.Copy:
                        {
                            CompositeCommand cmd = new CompositeCommand((bool execute) => { tree.Invalidate(); });
                            foreach (var pair in matchingParameters)
                            {
                                if (pair.Key.Value != null)
                                {
                                    cmd.Add(new SetParameterValue(pair.Value, pair.Key.Value.Clone()));
                                }
                                else
                                {
                                    cmd.Add(new SetParameterValue(pair.Value, null));
                                }
                            }
                            mCommandStack.Execute(cmd);
                            break;
                        }
                    case DragDropEffects.Link:
                        {
                            CompositeCommand cmd = new CompositeCommand((bool execute) => { tree.Invalidate(); });
                            foreach (var pair in matchingParameters)
                            {
                                cmd.Add(LinkParameter(pair.Key, pair.Value, true));
                            }
                            mCommandStack.Execute(cmd);
                            break;
                        }
                }
            }
            else if (e.Data.GetDataPresent(typeof(GlobalValue)))//drag global parameter - parameter type must match
            {
                GlobalValue draggedParam = (GlobalValue)(e.Data.GetData(typeof(GlobalValue)));
                ParameterNode targetNode = tree.DropPosition.Node.Tag as ParameterNode;
                if (draggedParam == null || targetNode == null)
                {
                    return;
                }
                mCommandStack.Execute(
                                new SetParameterValue(
                                    targetNode.Parameter,
                                    draggedParam,
                                    () => { tree.Invalidate(); }
                                    )
                            );
            }
        }

        private void tree_DragOver(object sender, DragEventArgs e)
        {
            if (mExecutionStatus.IsRunning)
            {
                e.Effect = DragDropEffects.None;
                return;
            }

            if (tree.DropPosition.Node == null || tree.DropPosition.Position != Aga.Controls.Tree.NodePosition.Inside)
            { // not dragged onto a node
                e.Effect = DragDropEffects.None;
                return;
            }

            if ((e.KeyState & 0x0C) == 0x0C)//CTRL+SHIFT
            {
                e.Effect = DragDropEffects.Link;
            }
            else if ((e.KeyState & 0x08) == 0x08)//CTRL
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.Move;
            }



            if (e.Data.GetDataPresent(typeof(ParameterNode))) //drag parameter to parameter - types must match
            {
                ParameterNode draggedParam = (ParameterNode)(e.Data.GetData(typeof(ParameterNode)));
                ParameterNode targetNode = tree.DropPosition.Node.Tag as ParameterNode;
                if (draggedParam == null || targetNode == null)
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                if (draggedParam.Parameter.ParamType != targetNode.Parameter.ParamType) // dragging onto a incompatible parameter
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                if (draggedParam.Parameter.Value is TestResultParameter && e.Effect == DragDropEffects.Link)
                {
                    // can only copy a parameter value that uses a test result
                    e.Effect = DragDropEffects.Copy;
                    return;
                }
            }
            else if (e.Data.GetDataPresent(typeof(ResultNode))) //drag parameter to parameter - types must match
            {
                ResultNode draggedParam = (ResultNode)(e.Data.GetData(typeof(ResultNode)));
                ParameterNode targetNode = tree.DropPosition.Node.Tag as ParameterNode;
                if (draggedParam == null || targetNode == null)
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                if (draggedParam.Parameter.IsResult == false || targetNode.Parameter.IsParameter == false)
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                if (!ParamHelper.IsAssignable(draggedParam.Parameter, targetNode.Parameter))
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                if (object.ReferenceEquals(targetNode.TestStep, draggedParam.TestStep))
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                e.Effect = DragDropEffects.Link;
            }
            else if (e.Data.GetDataPresent(typeof(SingleStepTreeNode))) // drag test case onto a test case - test Unique IDs must match
            {
                SingleStepTreeNode draggedNode = (SingleStepTreeNode)(e.Data.GetData(typeof(SingleStepTreeNode)));
                SingleStepTreeNode targetNode = tree.DropPosition.Node.Tag as SingleStepTreeNode;
                if (draggedNode == null || targetNode == null || draggedNode == targetNode)
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                if (CanMatchParameterSets(draggedNode.SingleTestStep.Parameters, targetNode.SingleTestStep.Parameters) == false)
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
            }
            else if (e.Data.GetDataPresent(typeof(GlobalValue)))//drag global parameter - parameter type must match
            {
                GlobalValue draggedParam = (GlobalValue)(e.Data.GetData(typeof(GlobalValue)));
                ParameterNode targetNode = tree.DropPosition.Node.Tag as ParameterNode;
                if (draggedParam == null || targetNode == null)
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                AbstractParameterValue draggedParamValue = mGlobalParameters.Get(draggedParam);
                if (draggedParamValue == null)
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                if (IsParameterTypeMatch(draggedParamValue, targetNode.Parameter) == false) // dragging onto a incompatible parameter
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                e.Effect = DragDropEffects.Link;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        /// <summary>
        /// Compares two sets of parameters (e.g. from two test steps). Finds if any of them have same name and are assignable
        /// </summary>
        private bool CanMatchParameterSets(List<IParameter> paramList1, List<IParameter> paramList2)
        {
            bool result = false;
            foreach (var p1 in paramList1)
            {
                var matchByName = paramList2.FirstOrDefault(p => p.Name == p1.Name);
                if (matchByName == null)
                {
                    continue;
                }
                if (ParamHelper.IsAssignable(p1, matchByName))
                {
                    result = true;//TODO just return here
                }
            }
            return result;
        }

        private Dictionary<IParameter, IParameter> GetPossibleParameterMatch(List<IParameter> paramList1, List<IParameter> paramList2)
        {
            Dictionary<IParameter, IParameter> result = new Dictionary<IParameter,IParameter>();
            foreach (var p1 in paramList1)
            {
                var matchByName = paramList2.FirstOrDefault(p => p.Name == p1.Name);
                if (matchByName != null)
                {
                    if (ParamHelper.IsAssignable(p1, matchByName))
                    {
                        result.Add(p1, matchByName);
                    }
                }
            }
            return result;
        }

        //allow dragging a single parameter node - it can be moved, copied (make a local parameter) or linked (make a common test run parameter)
        //allow draging a test case node - allow dragging to another test case with the same test case type
        private void tree_ItemDrag(object sender, ItemDragEventArgs e)
        {
            System.Diagnostics.Debug.Assert(!this.InvokeRequired, "InvokeRequired");
            if (mExecutionStatus.IsRunning)
            {
                return;
            }

            if (tree.SelectedNodes.Count != 1)
            {
                return; // does not support dragging multiple nodes yet
            }

            //List<ParameterNode> parameters = new List<ParameterNode>( );
            //List<SingleStepTreeNode> testCases = new List<SingleStepTreeNode>( );
            foreach (var selNode in tree.SelectedNodes)
            {
                ParameterNode paramNode = selNode.Tag as ParameterNode;
                ResultNode resultNode = selNode.Tag as ResultNode;
                SingleStepTreeNode testCaseNode = selNode.Tag as SingleStepTreeNode;

                if (paramNode != null)
                {
                    tree.DoDragDrop(paramNode, DragDropEffects.Move | DragDropEffects.Copy | DragDropEffects.Link);
                    return;
                }

                if (resultNode != null)
                {
                    tree.DoDragDrop(resultNode, DragDropEffects.Link);
                    return;
                }

                if (testCaseNode != null)
                {
                    tree.DoDragDrop(testCaseNode, DragDropEffects.Move | DragDropEffects.Copy | DragDropEffects.Link);
                    return;
                }
            }
        }

        private List<AbstractParameterValue> GetParameterValueTypes(out List<ParameterNode> nodes)
        {
            if (tree.SelectedNodes.Where(x => ((x.Tag is ParameterNode) == false)).Count() > 0)
            {
                // if selection contains groups, test cases etc - do not continue
                nodes = new List<ParameterNode>();
                return null;
            }
            nodes = tree.SelectedNodes.Select(x => x.Tag as ParameterNode).ToList();
            if (nodes.Where(x => x.Parameter.IsResult).Count() > 0)
            {
                // should not allow editing test result parameter (shows an empty dialog + throws an exception)
                return null;
            }
            if (nodes.Count == 0) // no parameter nodes selected
            {
                return null;
            }

            List<AbstractParameterValue> result = ParameterTypeCollection.Instance.GetSuitableValueTypes(nodes[0].Parameter);
            var hashes = new HashSet<int>(result.Select(x => x.GetParamValueTypeHash()));
            foreach (var node in nodes)
            {
                var currentNodePossibleValues = ParameterTypeCollection.Instance.GetSuitableValueTypes(node.Parameter);
                var currentHashes = new HashSet<int>(currentNodePossibleValues.Select(x => x.GetParamValueTypeHash()));
                if (currentHashes.SetEquals(hashes) == false)
                {
                    return null;
                }
            }

            return new List<AbstractParameterValue>(result);
        }

        private void tree_SelectionChanged(object sender, EventArgs e)
        {
            UpdateEnabledControls();
        }

        private void removeValueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CompositeCommand cmd = new CompositeCommand((bool execute) => { tree.Invalidate(); });
            foreach (var param in GetSelectedParameters(true))
            {
                cmd.Add(new SetParameterValue(param.Parameter, null));
            }
            mCommandStack.Execute(cmd);            
        }

        private void UpdateEnabledControls()
        {
            List<ParameterNode> selectedParameters = GetSelectedParameters(true);
            removeValueToolStripMenuItem.Enabled = removeValuesToolstripButton.Enabled = localToolStripMenuItem.Enabled = testRunToolStripMenuItem.Enabled = !mExecutionStatus.IsRunning && selectedParameters.Count > 0;

            createGlobalParameterToolStripMenuItem.Enabled = !mExecutionStatus.IsRunning && selectedParameters.Count == 1;
            selectedParameters = GetSelectedParameters(false);
            removeValueToolStripMenuItem.Enabled = editValueToolStripMenuItem.Enabled = editValueToolstripButton.Enabled = assignDefaultValueToolStripMenuItem.Enabled = assignDefaultValueToolbarButton.Enabled = !mExecutionStatus.IsRunning && selectedParameters.Count > 0;

            cutToolStripMenuItem.Enabled = !mExecutionStatus.IsRunning && selectedParameters.Count > 0;
            copyToolStripMenuItem.Enabled = !mExecutionStatus.IsRunning && selectedParameters.Count > 0;
            pasteToolStripMenuItem.Enabled = !mExecutionStatus.IsRunning && (ClipboardObjects.ClipboardParameterContents.ClipboardContainsItems() || ClipboardObjects.ClipboardTestStepContents.ClipboardContainsItems());
            editDescriptionToolStripMenuItem.Enabled = GetSelectedParameters(false).Count > 0;
        }

        private void contextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            UpdateEnabledControls();
        }

        private void localToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CompositeCommand cmd = new CompositeCommand((bool execute) => { RaiseValueChanged(); tree.Invalidate(); });
            foreach (var param in GetSelectedParameters(true))
            {
                if (GetParameterScopeVisitor.Get(param.Parameter.Value, mGlobalParameters) != ParameterScope.LOCAL)
                {
                    bool found;
                    var value = GetParameterValueVisitor.Get(param.Parameter.Value, mGlobalParameters, out found);
                    if (found)
                    {
                        cmd.Add(new SetParameterValue(param.Parameter, value.Clone()));
                    }
                }
            }
            mCommandStack.Execute(cmd);    
        }

        private void testRunToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CompositeCommand cmd = new CompositeCommand((bool execute) => { RaiseValueChanged(); tree.Invalidate(); });
            
            foreach (var param in GetSelectedParameters(true))
            {
                if (GetParameterScopeVisitor.Get(param.Parameter.Value, mGlobalParameters) == ParameterScope.TEST_RUN)
                {
                    continue;
                }

                var manager = mGlobalParameters.GetManagers(ParameterScope.TEST_RUN, true)[0];
                bool found;
                var value = GetParameterValueVisitor.Get(param.Parameter.Value, mGlobalParameters, out found);
                if (found)
                {
                    GlobalParameter globalParam = new GlobalParameter(value);
                    Guid valueId = manager.Set(globalParam);
                    cmd.Add(new SetParameterValue(param.Parameter, new GlobalValue(manager.ID, valueId)));
                }                
            }
            mCommandStack.Execute(cmd);  
        }

        private void createGlobalParameterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CompositeCommand cmd = new CompositeCommand((bool execute) => { RaiseValueChanged(); tree.Invalidate(); });
            
            var selected = GetSelectedParameters(true);
            foreach (var param in selected)
            {
                if (GetParameterScopeVisitor.Get(param.Parameter.Value, mGlobalParameters) == ParameterScope.GLOBAL) // already global
                {
                    continue;
                }

                var manager = mGlobalParameters.GetManagers(ParameterScope.GLOBAL, true)[0];

                bool found;
                var value = GetParameterValueVisitor.Get(param.Parameter.Value, mGlobalParameters, out found); // get parameter value
                if (found)
                {
                    string name = param.ParamName;
                    if (InputBox.Get("Global parameter name", "Enter parameter name", ref name) == DialogResult.OK)
                    {
                        GlobalParameter globalParam = new GlobalParameter(name, value);
                        Guid valueId = manager.Set(globalParam);
                        cmd.Add(new SetParameterValue(param.Parameter, new GlobalValue(manager.ID, valueId)));
                    }
                    break;
                }
            }
            mCommandStack.Execute(cmd);  
        }

        private List<ParameterNode> GetSelectedParameters(bool nonNullValuesOnly)
        {
            List<ParameterNode> selectedParameters = new List<ParameterNode>();
            foreach (var selNode in tree.SelectedNodes)
            {
                ParameterNode paramNode = selNode.Tag as ParameterNode;
                ResultNode resultNode = selNode.Tag as ResultNode;
                //SingleStepTreeNode testCaseNode = selNode.Tag as SingleStepTreeNode;

                if (paramNode != null)
                {
                    if (nonNullValuesOnly)
                    {
                        bool found;
                        var value = GetParameterValueVisitor.Get(paramNode.Parameter.Value, mGlobalParameters, out found);
                        if (found && value != null)
                        {
                            selectedParameters.Add(paramNode);
                        }
                    }
                    else
                    {
                        selectedParameters.Add(paramNode);
                    }
                }
            }
            return selectedParameters;
        }

        private List<ResultNode> GetSelectedResults()
        {
            List<ResultNode> result = new List<ResultNode>();
            foreach (var selNode in tree.SelectedNodes)
            {
                result.AddIfNotNull(selNode.Tag as ResultNode);
            }
            return result;
        }

        private List<SingleStepTreeNode> GetSelectedSteps()
        {
            List<SingleStepTreeNode> result = new List<SingleStepTreeNode>();
            foreach (var selNode in tree.SelectedNodes)
            {
                result.AddIfNotNull(selNode.Tag as SingleStepTreeNode);
            }
            return result;
        }

        private bool IsParameterTypeMatch(AbstractParameterValue draggedParamValue, IParameter paramType)
        {
            try
            {
                if (draggedParamValue is GlobalValue)
                {
                    draggedParamValue = mGlobalParameters.Get(draggedParamValue as GlobalValue).Value;
                }
            }
            catch
            {
            }

            if (draggedParamValue is GlobalParameter)
            {
                draggedParamValue = (draggedParamValue as GlobalParameter).Value;
            }

            //TODO if dragging an integer value and paramType should be integer too and the draggedvalue could fit into min/max of param type, allow...

            foreach (var suitable in ParameterTypeCollection.Instance.GetSuitableValueTypes(paramType))
            {
                if (suitable.GetParamValueTypeHash() == draggedParamValue.GetParamValueTypeHash())
                {
                    return true;
                }
            }

            return false;
        }

        private void editValueToolstripButton_Click(object sender, EventArgs e)
        {
            if (this.editValueToolstripButton.Enabled == false)
            {
                return;
            }

            AbstractParameterValue cloneToEdit = null;
            List<ParameterNode> nodes;
            List<AbstractParameterValue> parameterValueTypes = GetParameterValueTypes(out nodes);

            if (parameterValueTypes == null)
            {
                return;
            }
            if (parameterValueTypes.Count == 0)
            {
                MessageBox.Show("No suitable parameter editor was found", "Error");
                return;
            }

            ParameterNode node = null;
            try
            {
                nodes.Last(x => x.Parameter.Value != null);
            }
            catch (InvalidOperationException) { }

            if (node == null) // all selected nodes do not have values
            {
                node = nodes.Last();
            }
            if (node.Parameter.Value != null)
            {
                bool found;
                // make a clone for an existing value in case the editor is canceled
                AbstractParameterValue value = GetParameterValueVisitor.Get(node.Parameter.Value, mGlobalParameters, out found);
                if (value != null && found)
                {
                    try
                    {
                        cloneToEdit = value.Clone();
                    }
                    catch (Exception ex){
                        CrashLog.Write(ex);
                    }
                }
            }

            Dictionary<AbstractParameterValue, AbstractParameterValue> paramTypeValueMap = new Dictionary<AbstractParameterValue, AbstractParameterValue>();
            foreach (var paramType in parameterValueTypes)
            {
                if (cloneToEdit != null && paramType.GetType() == cloneToEdit.GetType())
                {
                    paramTypeValueMap.Add(paramType, cloneToEdit);
                }
                else
                {
                    paramTypeValueMap.Add(paramType, null);
                }
            }

            if (ParameterTabEditorForm.Edit(paramTypeValueMap, out cloneToEdit))
            {
                CompositeCommand cmd = new CompositeCommand((bool execute) => { RaiseValueChanged(); tree.Invalidate(); });
            
                foreach (var nodeToUpdate in nodes)
                {
                    // SetGlobalParameterValue.Create returns not null if the value is global etc.
                    if (cmd.Add(SetGlobalParameterValue.Create(nodeToUpdate.Parameter.Value, cloneToEdit.Clone(), mGlobalParameters, null)) == false)
                    {
                        //otherwise assign local value
                        cmd.Add(new SetParameterValue(nodeToUpdate.Parameter, cloneToEdit.Clone()));
                    }
                }
                
                mCommandStack.Execute(cmd);              
            }
        }

        public event EventHandler ValueChanged;
        
        private void RaiseValueChanged()
        {
            if (ValueChanged != null)
            {
                ValueChanged(this, null);
            }
        }

        private ICommand LinkParameter(IParameter from, IParameter to, bool addRefreshAction)
        {
            if (from.Value == null)
            {
                return null;
            }
            if (IsGlobalParameter.Get(from.Value))
            {
                //already global parameter, just set the same to "To" parameter
                return new SetParameterValue(to, from.Value.Clone(),  ()=>{ if (addRefreshAction){RaiseValueChanged(); tree.Invalidate();}});
            }
            IGlobalParameters manager = mGlobalParameters.GetManagers(ParameterScope.TEST_RUN, true)[0];
            GlobalParameter globalParam = new GlobalParameter(from.Value);
            Guid valueId = manager.Set(globalParam);

            CompositeCommand cmd = new CompositeCommand((bool execute) => { if (addRefreshAction) { RaiseValueChanged(); tree.Invalidate(); } });
            cmd.Add(new SetParameterValue(from, new GlobalValue(manager.ID, valueId)));
            cmd.Add(new SetParameterValue(to, new GlobalValue(manager.ID, valueId)));
            return cmd;
        }

        private ICommand LinkResultToParameter(IParameter from, IParameter to, bool addRefreshAction)
        {
            Debug.Assert(from.IsResult);
            Debug.Assert(to.IsResult == false);

            var res = from.Value as TestResultParameter;
            if (res == null)
            {
                from.Value = res = new TestResultParameter(Guid.NewGuid());
            }
            
            var id = res.ResultID;
            return new SetParameterValue(to, new TestResultParameter(id), () => { if (addRefreshAction) { RaiseValueChanged(); tree.Invalidate(); } });
        }

        private void tree_NodeMouseDoubleClick(object sender, Aga.Controls.Tree.TreeNodeAdvMouseEventArgs e)
        {
            editValueToolstripButton_Click(sender, e);
        }

        private void nodeTextBox1_DrawText(object sender, Aga.Controls.Tree.NodeControls.DrawEventArgs e)
        {
            ParameterNode param = e.Node.Tag as ParameterNode;
            if (param == null)
            {
                return;
            }
            if (param.CanBeNull == false)
            {
                bool found;
                var x = GetParameterValueVisitor.Get(param.Parameter.Value, mGlobalParameters, out found);
                if (x == null || !found)
                {
                    e.TextColor = System.Drawing.Color.Blue;
                }
            }
        }

        private void btShowTypeDetails_CheckedChanged(object sender, EventArgs e)
        {
            this.treeColumnTypeDetails.IsVisible = (sender as ToolStripButton).Checked;
            this.treeColumnTypeDescr.IsVisible = (sender as ToolStripButton).Checked;
            //save setting
            Properties.Settings.Default.ShowParameterDetailsColumn = (sender as ToolStripButton).Checked;
            Properties.Settings.Default.Save();
        }

        private void assignDefaultValueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CompositeCommand cmd = new CompositeCommand((bool execute) => { RaiseValueChanged(); tree.Invalidate(); });

            List<ParameterNode> parameters = GetSelectedParameters(false);
            foreach (var param in parameters)
            {
                try
                {
                    if (param.TestStep is TestStepSingle)
                    {
                        cmd.Add(mParameterProperties.ResetToDefaultValue((param.TestStep as TestStepSingle), param.Parameter));
                    }
                }
                catch (Exception ex)
                {
                    yats.Utilities.CrashLog.Write(ex, "Reset parameter value");
                }
            }
            
            mCommandStack.Execute(cmd); 
        }

        private void tree_KeyDown(object sender, KeyEventArgs e)
        {
            //TODO: hint with shortcut list
            if (!e.Control && !e.Alt && !e.Shift)
            {
                //edit
                if (e.KeyCode == Keys.Enter)
                {
                    editValueToolstripButton_Click(sender, e);
                    e.Handled = true;
                }
                //remove
                else if (e.KeyCode == Keys.Delete)
                {
                    removeValueToolStripMenuItem_Click(sender, e);
                    e.Handled = true;
                }
            }
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CompositeCommand cmd = new CompositeCommand((bool execute) => { RaiseValueChanged(); tree.Invalidate(); });

            List<IParameter> parametersToCopy = new List<IParameter>();
            foreach (var param in GetSelectedParameters(false))
            {
                parametersToCopy.Add(param.Parameter);
                cmd.Add(new SetParameterValue(param.Parameter, null));
                //param.Parameter.Value = null;
            }
            foreach (var param in GetSelectedResults())
            {
                if (param.Parameter.Value == null)
                {
                    cmd.Add(new SetParameterValue(param.Parameter, new TestResultParameter(Guid.NewGuid())));
                    //param.Parameter.Value = new TestResultParameter(Guid.NewGuid());
                }
                parametersToCopy.Add(param.Parameter);
            }
            new ClipboardObjects.ClipboardParameterContents(parametersToCopy, mGlobalParameters).CopyToClipboard();
            
            mCommandStack.Execute(cmd);           
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                // Try copying selected parameters.
                List<IParameter> parametersToCopy = new List<IParameter>();
                foreach (var param in GetSelectedParameters(false))
                {
                    parametersToCopy.Add(param.Parameter);
                }
                foreach (var param in GetSelectedResults())
                {
                    if (param.Parameter.Value == null)
                    {
                        param.Parameter.Value = new TestResultParameter(Guid.NewGuid());
                    }
                    parametersToCopy.Add(param.Parameter);
                }
                if (parametersToCopy.Count > 0)
                {
                    new ClipboardObjects.ClipboardParameterContents(parametersToCopy, mGlobalParameters).CopyToClipboard();
                    return;
                }

                // If nothing selected, look for selected steps
                List<TestStep> steps = GetSelectedSteps().Select(x=>x.SingleTestStep as TestStep).ToList();
                if (steps.Count > 0)
                {
                    foreach (var step in steps)
                    {
                        ParameterEnumerator.GetParameters(step, (IParameter param) => {
                            if (param.IsResult && param.Value == null)
                            {
                                // assign GUIDs to parameters before pasting
                                param.Value = new TestResultParameter(Guid.NewGuid());
                            }
                        });
                    }
                    new ClipboardObjects.ClipboardTestStepContents(steps, mGlobalParameters).CopyToClipboard();
                    return;
                }
            }
            catch (Exception ex)
            {
                yats.Utilities.CrashLog.Write(ex, "copyToolStripMenuItem_Click");
            }
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Paste(tree.SelectedNodes, mGlobalParameters, mCommandStack))
                {
                    RaiseValueChanged();
                }
            }
            catch (Exception ex)
            {
                yats.Utilities.CrashLog.Write(ex, "pasteToolStripMenuItem_Click");
            }
            tree.Invalidate(true);
        }

        // data to be pasted should be in system clipboard
        // targetNodes are the selected parameter tree nodes to paste into
        public bool Paste(ICollection<TreeNodeAdv> targetNodes, IGlobalParameterCollection testRunGlobals, ICommandStack commandStack)
        {
            if (targetNodes.IsNullOrEmpty())
            {
                return false;
            }

            // stage 1 - find out what and where is pasted
            List<ParameterNode> targetParameters = new List<ParameterNode>();
            List<IConfigurable> targetSteps = new List<IConfigurable>();
            
            foreach (var targetNode in targetNodes)
            {
                if (targetNode.Tag is ResultNode)
                {
                    return false; // can not paste into results
                } 
                else if (targetNode.Tag is CompositeStepTreeNode)
                {
                    return false;
                }
                targetParameters.AddIfNotNull(targetNode.Tag as ParameterNode);
                if (targetNode.Tag is SingleStepTreeNode)
                {
                    targetSteps.AddIfNotNull((targetNode.Tag as SingleStepTreeNode).SingleTestStep);
                }
            }
            return Paste2(targetParameters, targetSteps, testRunGlobals, commandStack );
        }

        internal bool Paste2(IList<ParameterNode> targetParameters, IList<IConfigurable> targetSteps, IGlobalParameterCollection testRunGlobals, ICommandStack commandStack)
        {
            if (targetParameters.Count > 0 && targetSteps.Count > 0)
            {
                return false;//should paste either to test cases or parameters, not both
            }

            var clipboardParameters = ClipboardObjects.ClipboardParameterContents.GetFromClipboard();
            if (clipboardParameters != null)
            {
                int numPastedItems = clipboardParameters.GetParameters().Count;
                if (numPastedItems == 1)
                {
                    if (targetParameters.Count > 0)
                    {
                        return PasteParameterToParameter(targetParameters, testRunGlobals, clipboardParameters.GetParameters()[0], clipboardParameters.GetGlobals());
                    }
                    if (targetSteps.Count > 0)
                    {
                        return PasteParametersToSteps(targetSteps, testRunGlobals, clipboardParameters.GetParameters(), clipboardParameters.GetGlobals());
                    }
                }
                else if (numPastedItems > 1)
                {
                    if (targetSteps.Count > 0)
                    {
                        return PasteParametersToSteps(targetSteps, testRunGlobals, clipboardParameters.GetParameters(), clipboardParameters.GetGlobals());
                    }
                }
                return false;
            }

            var clipboardSteps = ClipboardObjects.ClipboardTestStepContents.GetFromClipboard();
            if (clipboardSteps != null) 
            {
                var stepsFromClipboard = clipboardSteps.GetTestSteps();
                if (targetSteps.Count > 0 && stepsFromClipboard.Count == 1)
                {
                    return PasteStepsToSteps(targetSteps, testRunGlobals, stepsFromClipboard[0], clipboardSteps.GetGlobals(), commandStack);
                }
                return false;
            }
            return false;
        }

        private bool PasteStepsToSteps(IList<IConfigurable> targetSteps, IGlobalParameterCollection testRunGlobals, IConfigurable stepFromClipboard, IGlobalParameterCollection globalsFromClipboard, ICommandStack commandStack)
        {
            //check that every parameter is compatible
            foreach (var step in targetSteps)
            {
                if (step.Parameters.Count != stepFromClipboard.Parameters.Count)
                {
                    return false;
                }
                int index = 0;
                foreach(var param in step.Parameters){
                    var fromClipboard = stepFromClipboard.Parameters[index++];
                    if (param.IsResult){
                        continue;
                    }
                    if (ParamHelper.IsAssignable(param, fromClipboard) == false)
                    {
                        return false;
                    }
                }
            }

            var loadedGlobals = yats.TestCase.Parameter.ExtractGlobalParameters.GetGlobals(globalsFromClipboard, ParameterEnumerator.GetParameters(stepFromClipboard));
            ParameterImporter.ImportGlobalParameters(globalsFromClipboard, loadedGlobals, testRunGlobals);

            CompositeCommand cmd = new CompositeCommand((bool execute) => { tree.Invalidate(true); });

            foreach (var step in targetSteps)
            {
                int index = 0;
                foreach (var param in step.Parameters)
                {
                    var fromClipboard = stepFromClipboard.Parameters[index++];
                    if (param.IsResult)
                    {
                        continue;
                    }

                    cmd.Add(new SetParameterValue(param, (fromClipboard.Value == null) ? null : fromClipboard.Value.Clone()));
                    //param.Value = (fromClipboard.Value == null) ? null : fromClipboard.Value.Clone();
                }
            }
            commandStack.Execute(cmd); 
            return true;
        }

        private bool PasteParametersToSteps(IList<IConfigurable> targetSteps, IGlobalParameterCollection testRunGlobals, List<IParameter> paramsFromClipboard, IGlobalParameterCollection globalsFromClipboard)
        {
            HashSet<string> uniqueNames = new HashSet<string>(paramsFromClipboard.Select(p => p.Name));
            if (uniqueNames.Count != paramsFromClipboard.Count)
            {
                // copied tests contain duplicate names
                return false;
            }

            //check that every parameter is compatible
            foreach (var step in targetSteps)
            {
                foreach (var param in paramsFromClipboard)
                {
                    var matchingParam = step.Parameters.FirstOrDefault(p => p.Name == param.Name);
                    if (matchingParam == null)
                    {
                        return false;
                    }
                    if (matchingParam.IsResult)
                    {
                        continue;
                    }
                    if (ParamHelper.IsAssignable(param, matchingParam) == false)
                    {
                        return false;
                    }
                }
            }

            var loadedGlobals = yats.TestCase.Parameter.ExtractGlobalParameters.GetGlobals(globalsFromClipboard, paramsFromClipboard);
            ParameterImporter.ImportGlobalParameters(globalsFromClipboard, loadedGlobals, testRunGlobals);
            CompositeCommand cmd = new CompositeCommand((bool execute) => { tree.Invalidate(true); });
            foreach (var step in targetSteps)
            {
                foreach (var param in paramsFromClipboard)
                {
                    var matchingParam = step.Parameters.FirstOrDefault(p => p.Name == param.Name);
                    if (matchingParam.IsResult)
                    {
                        continue;
                    }
                    cmd.Add(new SetParameterValue(matchingParam, (param.Value == null) ? null : param.Value.Clone()));
                }
            }
            mCommandStack.Execute(cmd); 
            return true;
        }

        private bool PasteParameterToParameter(IList<ParameterNode> targetParameters, IGlobalParameterCollection testRunGlobals, IParameter clipboardParameter, IGlobalParameterCollection globalsFromClipboard)
        {
            if (clipboardParameter.Value != null)
            {
                // do not check for type match if pasting null
                IParameter compareTo = targetParameters[0].Parameter;
                if (ParamHelper.IsAssignable(clipboardParameter, compareTo) == false)
                {
                    return false;
                }
                foreach (var target in targetParameters)
                {
                    if (ParamHelper.IsAssignable(clipboardParameter, target.Parameter) == false)
                    {
                        return false;
                    }
                }
            }

            var loadedGlobals = yats.TestCase.Parameter.ExtractGlobalParameters.GetGlobals(globalsFromClipboard, clipboardParameter);
            ParameterImporter.ImportGlobalParameters(globalsFromClipboard, loadedGlobals, testRunGlobals);
            CompositeCommand cmd = new CompositeCommand((bool execute) => { tree.Invalidate(true); });
            foreach (var target in targetParameters)
            {
                cmd.Add(new SetParameterValue(target.Parameter, (clipboardParameter.Value == null) ? null : clipboardParameter.Value.Clone()));
            }
            mCommandStack.Execute(cmd); 
            return true;
        }

        private void editDescriptionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var parameters = GetSelectedParameters(false);
            if (parameters.Count == 0)
            {
                return;
            }
            var fp = parameters.First().Parameter;
            string value = fp.ActiveDescription;
            if (yats.Gui.Winforms.Components.Util.InputBox.Get("Rename", "New name:", ref value) == DialogResult.OK)
            {
                mCommandStack.Execute(new RenameParameter(value, parameters.Select(x=>x.Parameter), () =>
                {
                    tree.Invalidate();
                }));
            }
        }
    }
}
