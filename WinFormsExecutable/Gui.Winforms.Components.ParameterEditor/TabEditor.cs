﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using yats.Gui.Winforms.Components.Util;
using yats.TestCase.Parameter;
using System.Linq;

namespace yats.Gui.Winforms.Components.ParameterEditor
{
    public partial class TabEditor : UserControl
    {
        public TabEditor()
        {
            InitializeComponent();
        }

        [DebuggerStepThrough]
        public void Clear()
        {
            this.editorTabs.TabPages.Clear();
        }

        private Dictionary<IEditor, AbstractParameterValue> EditorParameterMap = new Dictionary<IEditor, AbstractParameterValue>();

        public Size Edit(Dictionary<AbstractParameterValue, AbstractParameterValue> paramTypeValueMap)
        {
            Size minSize = new Size();
            try
            {
                this.editorTabs.TabPages.Clear();
                int selectedIndex = 0;
                HashSet<Type> editorTypesInUse = new HashSet<Type>();

                foreach (var typeValuePair in paramTypeValueMap)
                {                    
                    Dictionary<IEditor, int> editors = ParameterTypeCollection.Instance.GetEditors(typeValuePair.Key);
                    if (typeValuePair.Value != null)
                    {
                        selectedIndex = editorTabs.TabCount; // assign before adding - will contain the current index
                    }

                    var sortedEditors = (from pair in editors
                                        orderby pair.Value descending
                                        select pair.Key).ToList();

                    //var sortedEditors = editors.OrderByDescending(x => x.Value).Select(x => x.Key).ToList();

                    foreach (var editor in sortedEditors)
                    {
                        if (editorTypesInUse.Contains(editor.GetType()))
                        {
                            continue;
                        }
                        try
                        {
                            AbstractParameterValue valueOrDefault = (typeValuePair.Value != null) ? typeValuePair.Value : typeValuePair.Key;
                            editor.ParameterToEditor(valueOrDefault);
                            EditorParameterMap.Add(editor, valueOrDefault);
                            var panel = editor as UserControl;

                            if (panel.MinimumSize.IsEmpty == false)
                            {
                                minSize = yats.Gui.Winforms.Components.Util.SizeUtil.Max(minSize, panel.MinimumSize);
                            }
                            TabPage tab = new TabPage();
                            editorTabs.TabPages.Add(tab);
                            tab.Controls.Add(panel);
                            tab.Text = ((IEditor)panel).TabName();
                            panel.Dock = DockStyle.Fill;
                            editor.OnConfirm += editor_OnConfirm;
                            editorTypesInUse.Add(editor.GetType());
                        }
                        catch (Exception ex)
                        {
                            yats.Utilities.CrashLog.Write(ex, "Error populating parameter editor");
                        }

                        try
                        {
                            (sortedEditors.First() as Control).GetNextControl(null, true).Select();
                        }
                        catch { }//ignore errors - this is an attempt to select the first control in the tab - seems fishy
                    }                    
                }

                try
                {
                    editorTabs.SelectedIndex = selectedIndex;
                }
                catch { }//ignore errors - this is an attempt to select the first control in the tab - seems fishy

                if (minSize.IsEmpty == false && editorTabs.TabPages.Count > 1)
                {
                    minSize = minSize.Add(0, editorTabs.ItemSize.Height);
                    //editorTabs.MinimumSize = minSize.Add(new Size(14, 32));
                }                
            }
            catch (Exception ex)
            {
                yats.Utilities.CrashLog.Write(ex, "Parameter editor");
            }
            return minSize;
        }

        private void editorTabs_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (e.TabPage == null) // null if tabs are cleared
            {
                return;
            }
            
            IEditor newEditor = e.TabPage.Controls[0] as IEditor;
            newEditor.ParameterToEditor(EditorParameterMap[newEditor]);
        }

        private void editorTabs_Deselecting(object sender, TabControlCancelEventArgs e)
        {
            if (e.TabPage == null) // null if tabs are cleared
            {
                return;
            }
            IEditor oldEditor = e.TabPage.Controls[0] as IEditor;
            try
            {
                oldEditor.ValidateEditor(EditorParameterMap[oldEditor], false);
                EditorToParameter(oldEditor, EditorParameterMap[oldEditor]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                e.Cancel = true; // do not switch to another tab if current one has errors
            }            
        }

        internal void Save()
        {
            AbstractParameterValue result;
            Save(out result);
        }

        internal bool Save(out AbstractParameterValue result)
        {
            result = null;
            if (editorTabs.SelectedTab == null || editorTabs.SelectedTab.Controls.Count == 0)
            {                
                return false;
            }
            
            IEditor currentEditor = editorTabs.SelectedTab.Controls[0] as IEditor;
            
            if (currentEditor == null)
            {
                return false;
            }

            EditorToParameter(currentEditor, EditorParameterMap[currentEditor]);
            result = EditorParameterMap[currentEditor];
            return true;
        }

		private void EditorToParameter (IEditor editor, AbstractParameterValue parameter)
		{
			if (parameter is GlobalParameter){
                throw new Exception("global parameter not expected");
			} else if (parameter is GlobalValue){
                throw new Exception("global parameter not expected");
			} else if (parameter != null) {
				editor.EditorToParameter(parameter);
			}
		}

        public event EventHandler OnConfirm;

        private void editor_OnConfirm(object sender, EventArgs e)
        {
            if (OnConfirm != null)
            {
                OnConfirm(sender, e);
            }
        }

        internal void ValidateCurrentTab(bool validatingForSave)
        {
            if (editorTabs.SelectedTab == null){
                return;
            }

            IEditor currentEditor = editorTabs.SelectedTab.Controls[0] as IEditor;

            if (currentEditor == null)
            {
                return;
            }

            currentEditor.ValidateEditor(EditorParameterMap[currentEditor], validatingForSave);
        }
    }
}
