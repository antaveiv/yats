﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using Aga.Controls.Tree;
using yats.ExecutionQueue;
using yats.Gui.Winforms.Components.ParameterEditor.Resources;
using yats.TestCase.Parameter;

namespace yats.Gui.Winforms.Components.ParameterEditor
{
    //TODO why public? used in unit tests
    public class ParameterTreeNode : Aga.Controls.Tree.Node<ParameterTreeNode>
    {
        private Image mParamIcon;
        public virtual Image ParamIcon
        {
            get { return mParamIcon; }
            protected set { mParamIcon = value; }
        }

        private string mParamName = "";
        public virtual string ParamName
        {
            get { return mParamName; }
            protected set { mParamName = value; }
        }

        public virtual string Description
        {
            get;
            set;
        }

        private string mParamType = "";
        public virtual string ParamType
        {
            get { return mParamType; }
            protected set { mParamType = value; }
        }

        private string mParamValue = "";
        public virtual string ParamValue
        {
            get { return mParamValue; }
            protected set { mParamValue = value; }
        }

        private string mType = "";
        public virtual string Type
        {
            get { return mType; }
            protected set { mType = value; }
        }

        protected string mTypeDescr = "";
        public virtual string TypeDescr
        {
            get { return mTypeDescr; }
            protected set { mTypeDescr = value; }
        }

        [DebuggerStepThrough]
        public ParameterTreeNode(Image paramIcon)
        {
            mParamIcon = paramIcon;
        }

        public ParameterTreeNode() { }
    }

    public class CompositeStepTreeNode : ParameterTreeNode
    {
        public override string ParamName
        {
            [DebuggerStepThrough]
            get { return CompositeTestStep.Name; }
        }
                
        public TestStepComposite CompositeTestStep;
        [DebuggerStepThrough]
        public CompositeStepTreeNode(TestStepComposite compositeTestStep)
            : base(Resource.arrow_turn_270)
        {
            this.CompositeTestStep = compositeTestStep;
        }
    }

    public class SingleStepTreeNode : ParameterTreeNode
    {
        public override string ParamName
        {
            [DebuggerStepThrough]
            get { return SingleTestStep.Name; }
        }
                
        public TestStepSingle SingleTestStep;
        
        [DebuggerStepThrough]
        public SingleStepTreeNode(TestStepSingle singleTestStep):base(Resource.arrow_skip)
        {
            this.SingleTestStep = singleTestStep;
        }
    }

    public class ParameterNode : ParameterTreeNode
    {
        public override string ParamName
        {
            [DebuggerStepThrough]
            get
            {
                return Parameter.Name;
            }
        }

        public override string ParamType
        {
            [DebuggerStepThrough]
            get
            {
                return ParamSimpleTypeVisitor.Get(Parameter.Value, GlobalParameters);
            }
        }

        public override string Type
        {
            get {
                string mult = "";
                if (Parameter.IsArray){
                    mult = "array of ";
                } else if (Parameter.IsList){
                    mult = "list of ";
                }
                System.Type t = System.Type.GetType( Parameter.ParamType );
                string result = string.Format( "Parameter {0}{1}", mult, t.FullName);
                return result;            
            }
        }

        public override string TypeDescr
        {
            get
            {
                if (this.CanBeNull)
                {
                    return "optional parameter";
                }
                return "parameter";
            }
        }
        
        public override string ParamValue
        {
            get { 
                var useResult = Parameter.Value as TestResultParameter;
                if(useResult != null)
                {
                    return GetTestResultNameVisitor.GetName(useResult, TopLevelStep);
                }
                return ParamValueToString.Get(Parameter.Value, GlobalParameters); 
            }
        }

        public override string Description
        {
            get
            {
                return Parameter.ActiveDescription;
            }
        }

        public IParameter Parameter;
        public bool CanBeNull = false;
        public IConfigurable TestStep;
        IGlobalParameterCollection GlobalParameters;
        TestStep TopLevelStep;

        [DebuggerStepThrough]
        public ParameterNode(IGlobalParameterCollection globalParameters, TestStep topLevelStep, IParameter parameter, IConfigurable testStep)
            : this(globalParameters, topLevelStep, parameter, testStep, false)
        {
        }

        public ParameterNode(IGlobalParameterCollection globalParameters, TestStep topLevelStep, IParameter parameter, IConfigurable testStep, bool canBeNull)
            : base(Resource.bullet_green_16)
        {
            this.TopLevelStep = topLevelStep;
            this.GlobalParameters = globalParameters;
            this.Parameter = parameter;
            this.CanBeNull = canBeNull;
            this.TestStep = testStep;
        }
    }

    public class ResultNode : ParameterTreeNode
    {
        public override string ParamName
        {
            [DebuggerStepThrough]
            get
            {
                return Parameter.Name;
            }
        }
        
        public override string Type
        {
            get
            {
                string mult = "";
                if(Parameter.IsArray)
                {
                    mult = "array of ";
                }
                else if(Parameter.IsList)
                {
                    mult = "list of ";
                }
                System.Type t = System.Type.GetType( Parameter.ParamType );
                string result = string.Format( "Result {0}{1}", mult, t.FullName );

                return result;
            }
        }

        public override string Description
        {
            get
            {
                return Parameter.ActiveDescription;
            }
        }

        public IParameter Parameter;
        public TestStep TestStep;

        [DebuggerStepThrough]
        public ResultNode(IParameter parameter, TestStep testStep)
            : base(Resource.bullet_pink_16)
        {
            mTypeDescr = "result";
            this.Parameter = parameter;
            this.TestStep = testStep;
        }
    }

    internal class TreeGenerator
    {
        internal static ITreeModel GenerateTree(IParameterProperties parameterProperties, IGlobalParameterCollection globalParameters, IList<TestStep> steps, TestStep topLevelStep)
        {
            var model = new TreeModel<ParameterTreeNode>();
            foreach (var step in steps)
            {
                if (IsInHierarchyVisitor.Get(topLevelStep, step))
                {
                    step.Accept(new StepToParameterTreeNodeVisitor(parameterProperties, globalParameters, topLevelStep, model.Root));
                }
            }
            return model;
        }
    }

    internal class StepToParameterTreeNodeVisitor : ITestStepVisitor
    {
        ParameterTreeNode mParent;
        TestStep mTopLevelStep;
        IGlobalParameterCollection mGlobalParameters;
        IParameterProperties mParameterProperties;
        public StepToParameterTreeNodeVisitor(IParameterProperties parameterProperties, IGlobalParameterCollection globalParameters, TestStep topLevelStep, ParameterTreeNode parent)
        {
            this.mParameterProperties = parameterProperties;
            this.mParent = parent;
            this.mTopLevelStep = topLevelStep;
            this.mGlobalParameters = globalParameters;
        }

        public void AcceptSingle(TestStepSingle test)
        {            
            if (test.Parameters != null && test.Parameters.Count > 0)
            {
                var node = new SingleStepTreeNode(test);
                mParent.Nodes.Add(node);

                foreach (var param in test.Parameters)
                {
                    if (param.IsParameter)
                    {
                        bool canBeNull = mParameterProperties.CanParameterBeNull(test, param);
                        node.Nodes.Add(new ParameterNode(mGlobalParameters, mTopLevelStep, param, test, canBeNull));
                    }
                    else if (param.IsResult)
                    {
                        node.Nodes.Add(new ResultNode(param, test));
                    }
                }
            }
        }

        public void AcceptComposite(TestStepComposite test)
        {
            ParameterTreeNode node = new CompositeStepTreeNode(test);
            if (test != mTopLevelStep)
            {
                mParent.Nodes.Add(node);
            }
            else
            {
                node = mParent;
            }
            if (test.Parameters != null && test.Parameters.Count > 0) {
                foreach (var param in test.Parameters)
                {
                    if (param.IsParameter)
                    {
                        node.Nodes.Add(new ParameterNode(mGlobalParameters, mTopLevelStep, param, test));
                    }
                    else if (param.IsResult)
                    {
                        node.Nodes.Add(new ResultNode(param, test));
                    }
                }
            }

            foreach (var step in test.Steps)
            {
                step.Accept(new StepToParameterTreeNodeVisitor(mParameterProperties, mGlobalParameters, mTopLevelStep, node));
            }
        }
    }

    /*
    public class TreeModel : ITreeModel
    {
        internal TreeViewAdv tree;
        IList<TestStep> steps;
        internal IParameterProperties ParameterProperties;
        internal IGlobalParameterCollection GlobalParameters;
        internal TestStep TopLevelStep;

        public TreeModel(TreeViewAdv tree, IList<TestStep> steps, IParameterProperties paramProperties, IGlobalParameterCollection globalParameters, TestStep topLevelStep)
        {
            this.steps = steps;
            this.tree = tree;
            this.ParameterProperties = paramProperties;
            this.GlobalParameters = globalParameters;
            this.TopLevelStep = topLevelStep;
        }

        private TreePath GetPath(ParameterTreeNode item)
        {
            if (item == null)
                return TreePath.Empty;
            else
            {
                Stack<object> stack = new Stack<object>();
                while (item != null)
                {
                    stack.Push(item);
                    item = item.Parent;
                }
                return new TreePath(stack.ToArray());
            }
        }


        public System.Collections.IEnumerable GetChildren(TreePath treePath)
        {
            ParameterTestStepStepVisitor visitor = new ParameterTestStepStepVisitor(TopLevelStep);

            if (treePath.IsEmpty())
            {
                foreach (var step in this.steps)
                {
                    if(IsInHierarchyVisitor.Get( TopLevelStep, step ))
                    {
                        step.Accept( visitor );
                    }
                }
            }

            {
                CompositeStepTreeNode compositeNode = treePath.LastNode as CompositeStepTreeNode;
                if (compositeNode != null)
                {
                    if (compositeNode.CompositeTestStep.Parameters != null)
                    {
                        foreach (var param in compositeNode.CompositeTestStep.Parameters)
                        {
                            if (param.IsParameter)
                            {
                                visitor.result.Add(new ParameterNode(GlobalParameters, TopLevelStep, param, compositeNode.CompositeTestStep));
                            }
                            else if (param.IsResult)
                            {
                                visitor.result.Add(new ResultNode(param, compositeNode.CompositeTestStep));
                            }
                        }
                    }
                    foreach (var step in compositeNode.CompositeTestStep.Steps)
                    {
                        step.Accept(visitor);
                    }
                }
            }
            {
                SingleStepTreeNode singleNode = treePath.LastNode as SingleStepTreeNode;
                if (singleNode != null)
                {
                    foreach (var param in singleNode.SingleTestStep.Parameters)
                    {
                        if (param.IsParameter)
                        {
                            bool canBeNull = ParameterProperties.CanParameterBeNull( singleNode.SingleTestStep, param );
                            visitor.result.Add(new ParameterNode(GlobalParameters, TopLevelStep, param, singleNode.SingleTestStep, canBeNull));
                        }
                        else if (param.IsResult)
                        {
                            visitor.result.Add(new ResultNode(param, singleNode.SingleTestStep));
                        }
                    }
                }
            }

            tree.ExpandAll();
            return visitor.result;
        }

        public bool IsLeaf(TreePath treePath)
        {
            return treePath.LastNode is ParameterNode;
        }

        public event EventHandler<TreeModelEventArgs> NodesChanged;

        internal void OnNodesChanged(ParameterTreeNode item)
        {
            if (NodesChanged != null)
            {
                TreePath path = GetPath(item.Parent);

                NodesChanged(this, new TreeModelEventArgs(path, new object[] { item }));
            }
        }

        private void Test()
        {
            if (NodesInserted != null)
            {
                NodesInserted(this, null);
            }

            if (NodesRemoved != null)
            {
                NodesRemoved(this, null);
            }
        }

        public event EventHandler<TreeModelEventArgs> NodesInserted;

        public event EventHandler<TreeModelEventArgs> NodesRemoved;

        public event EventHandler<TreePathEventArgs> StructureChanged;
        
        protected void OnStructureChanged()
        {
            if (StructureChanged != null)
                StructureChanged(this, new TreePathEventArgs());
        }
    }*/
}
