﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using yats.TestCase.Parameter;

namespace yats.Gui.Winforms.Components.ParameterEditor
{
    public partial class ValueTypeSelectionForm : Form
    {
        public ValueTypeSelectionForm()
        {
            InitializeComponent();
        }

        public static AbstractParameterValue Select(List<AbstractParameterValue> selectFrom)
        {
            ValueTypeSelectionForm form = new ValueTypeSelectionForm();
            foreach(var item in selectFrom){
                form.listBox.Items.Add(ParameterTypeCollection.GetDisplayName(item));
            }
            if (form.ShowDialog() == DialogResult.OK)
            {
                return selectFrom[form.listBox.SelectedIndex];
            }
            return null;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            btOK.Enabled = listBox.SelectedIndex != -1;
        }

        private void listBox_DoubleClick(object sender, EventArgs e)
        {
            if (listBox.SelectedIndex != -1)
            {
                DialogResult = DialogResult.OK;
            }
        }
    }
}
