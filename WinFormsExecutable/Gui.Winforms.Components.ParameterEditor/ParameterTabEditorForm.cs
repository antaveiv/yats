﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using yats.Gui.Winforms.Components.Util;
using yats.TestCase.Parameter;

namespace yats.Gui.Winforms.Components.ParameterEditor
{
    public partial class ParameterTabEditorForm : Form
    {
        public ParameterTabEditorForm()
        {
            InitializeComponent( );
        }

        public static bool Edit(AbstractParameterValue parameter, out AbstractParameterValue result)
        {
            Dictionary<AbstractParameterValue, AbstractParameterValue> dict = new Dictionary<AbstractParameterValue, AbstractParameterValue>();
            dict.Add(parameter, parameter);
            return Edit(dict, out result);
        }

        // Parameters should be 'unwrapped', i.e. not global value
        public static bool Edit(Dictionary<AbstractParameterValue, AbstractParameterValue> paramTypeValueMap, out AbstractParameterValue result)
        {
            ParameterTabEditorForm form = new ParameterTabEditorForm();
            form.tabEditor.OnConfirm += form.tabEditor_OnConfirm;
            var minSize = form.tabEditor.Edit( paramTypeValueMap );
            if (minSize.IsEmpty == false)
            {
                form.Size = minSize.Add(14, 75); // TODO: calculate form size from control sizes
                form.MinimumSize = form.Size;
            }

            if(form.ShowDialog( ) == DialogResult.OK)
            {
                try
                {
                    return form.tabEditor.Save(out result);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Value not saved: " + ex.Message);
                }
            }
            result = null;
            return false;
        }

        // when an editor tab raises OnConfirm, do the same as OK button
        void tabEditor_OnConfirm(object sender, System.EventArgs e)
        {
            btOK_Click(sender, e);
        }

        private void btOK_Click(object sender, System.EventArgs e)
        {
            try
            {
                tabEditor.ValidateCurrentTab(true);// if errors are found in the active tab, the validation method should throw an exception
                DialogResult = DialogResult.OK; // no exceptions - close dialog
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
