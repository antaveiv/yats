﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using yats.ExecutionQueue;
using yats.TestCase.Parameter;
using yats.Gui.Winforms.Components.Util;
using yats.TestRun;

namespace yats.Gui.Winforms.Components.ParameterEditor
{
    public partial class ParameterEditorPanel : UserControl
    {
        TreeModel model;
        internal IGlobalParameterCollection globals;

        public ParameterEditorPanel()
        {
            InitializeComponent();
            EnableParamValueEditor( false );
        }

        internal void SetSteps(List<TestStep> steps, IGlobalParameterCollection globals, ITestRun testRun)
        {
            model = new TreeModel( tree, steps, globals, testRun );
            tree.Model = model;
            this.globals = globals;
            EnableParamValueEditor( false );
        }

        public ITestRun TestRun
        {
            get { return model.testRun; }
        }

        internal void UpdateModel()
        {
            //var selected = tree.SelectedNodes; //TODO: keep selection after refresh
            //var firstNode = tree.GetNodeAt(new System.Drawing.Point(0,0));
            model.OnStructureChanged();
            foreach(var node in tree.AllNodes)
            {
               // node.IsSelected = (selected.Contains( (node.Tag as ParameterTreeNode).TestStep ));
            }
            //if (firstNode != null)
            {
                //tree.EnsureVisible(firstNode);
            }
        }
		
		private void tree_DragDrop(object sender, DragEventArgs e)
        {
            if(isRunning)
            {
                e.Effect = DragDropEffects.None;
                return;
            }

            if(e.Data.GetDataPresent( typeof( ParameterNode ) ))
            {
                ParameterNode draggedParam = (ParameterNode)(e.Data.GetData( typeof( ParameterNode ) ));
                ParameterNode targetNode = tree.DropPosition.Node.Tag as ParameterNode;
                if(draggedParam == null || targetNode == null || object.ReferenceEquals(draggedParam, targetNode))
                {
                    return;
                }
                if (!ParamHelper.IsAssignable(draggedParam.parameter, targetNode.parameter))
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                switch(e.Effect)
                {
                    case DragDropEffects.Move:
                        targetNode.parameter.Value = draggedParam.parameter.Value;
                        draggedParam.parameter.Value = null;
                        break;
                    case DragDropEffects.Copy:
                        if(draggedParam.parameter.Value != null)
                        {
                            targetNode.parameter.Value = draggedParam.parameter.Value.Clone( );
                        }
                        else
                        {
                            targetNode.parameter.Value = null;
                        }
                        break;
                    case DragDropEffects.Link:
                        LinkParameter( draggedParam.parameter, targetNode.parameter );
                        break;
                }
            }
            else if (e.Data.GetDataPresent(typeof(ResultNode)))
            {
                ResultNode draggedParam = (ResultNode)(e.Data.GetData(typeof(ResultNode)));
                ParameterNode targetNode = tree.DropPosition.Node.Tag as ParameterNode;
                if (draggedParam == null || targetNode == null || object.ReferenceEquals(draggedParam, targetNode))
                {
                    return;
                }
                if (!ParamHelper.IsAssignable(draggedParam.parameter, targetNode.parameter))
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                switch (e.Effect)
                {
                    case DragDropEffects.Link:
                        LinkResultToParameter(draggedParam.parameter, targetNode.parameter);
                        break;
                }
            }
            else if(e.Data.GetDataPresent( typeof( SingleStepTreeNode ) ))
            {
                SingleStepTreeNode draggedTest = (SingleStepTreeNode)(e.Data.GetData( typeof( SingleStepTreeNode ) ));
                SingleStepTreeNode targetNode = tree.DropPosition.Node.Tag as SingleStepTreeNode;
                if(draggedTest == null || targetNode == null)
                {
                    return;
                }
                if(draggedTest.step.UniqueTestId != targetNode.step.UniqueTestId) // dragging onto a incompatible test case
                {
                    return;
                }
                switch(e.Effect)
                {
                    case DragDropEffects.Move:
                        for(int i = 0; i < draggedTest.step.Parameters.Count; i++)
                        {
                            targetNode.step.Parameters[i].Value = draggedTest.step.Parameters[i].Value;
                            draggedTest.step.Parameters[i].Value = null;
                        }
                        break;
                    case DragDropEffects.Copy:
                        for(int i = 0; i < draggedTest.step.Parameters.Count; i++)
                        {
                            if(draggedTest.step.Parameters[i].Value != null)
                            {
                                targetNode.step.Parameters[i].Value = draggedTest.step.Parameters[i].Value.Clone( );
                            }
                            else
                            {
                                targetNode.step.Parameters[i].Value = null;
                            }
                        }
                        break;
                    case DragDropEffects.Link:
                        for(int i = 0; i < draggedTest.step.Parameters.Count; i++)
                        {
                            LinkParameter( draggedTest.step.Parameters[i], targetNode.step.Parameters[i] );
                        }
                        break;
                }
            }
            else if(e.Data.GetDataPresent( typeof( GlobalValue ) ))//drag global parameter - parameter type must match
            {
                GlobalValue draggedParam = (GlobalValue)(e.Data.GetData( typeof( GlobalValue ) ));
                ParameterNode targetNode = tree.DropPosition.Node.Tag as ParameterNode;
                if(draggedParam == null || targetNode == null)
                {
                    return;
                }
                targetNode.parameter.Value = draggedParam;
            }
            UpdateModel( );
        }

		private void tree_DragOver(object sender, DragEventArgs e)
        {
            if(isRunning)
            {
                e.Effect = DragDropEffects.None;
                return;
            }

            if (tree.DropPosition.Node == null || tree.DropPosition.Position != Aga.Controls.Tree.NodePosition.Inside){ // not dragged onto a node
                e.Effect = DragDropEffects.None;
                return;
            }

            if((e.KeyState & 0x0C) == 0x0C)//CTRL+SHIFT
            {
                e.Effect = DragDropEffects.Link;
            }
            else if((e.KeyState & 0x08) == 0x08)//CTRL
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.Move;
            } 

            
            
            if(e.Data.GetDataPresent( typeof( ParameterNode ) )) //drag parameter to parameter - types must match
            {
                ParameterNode draggedParam = (ParameterNode)(e.Data.GetData( typeof( ParameterNode ) ));
                ParameterNode targetNode = tree.DropPosition.Node.Tag as ParameterNode;
                if (draggedParam == null || targetNode == null){
                    e.Effect = DragDropEffects.None;
                    return;
                }
                if(draggedParam.parameter.ParamType != targetNode.parameter.ParamType) // dragging onto a incompatible parameter
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                if(draggedParam.parameter.Value is TestResultParameter && e.Effect == DragDropEffects.Link)
                {
                    // can only copy a parameter value that uses a test result
                    e.Effect = DragDropEffects.Copy;
                    return;
                }
            }
            else if (e.Data.GetDataPresent(typeof(ResultNode))) //drag parameter to parameter - types must match
            {
                ResultNode draggedParam = (ResultNode)(e.Data.GetData(typeof(ResultNode)));
                ParameterNode targetNode = tree.DropPosition.Node.Tag as ParameterNode;
                if (draggedParam == null || targetNode == null)
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                if (draggedParam.parameter.IsResult == false || targetNode.parameter.IsParameter == false)
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                if (!ParamHelper.IsAssignable(draggedParam.parameter, targetNode.parameter))
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                
                
                
                e.Effect = DragDropEffects.Link;
            }
            else if (e.Data.GetDataPresent(typeof(SingleStepTreeNode))) // drag test case onto a test case - test Unique IDs must match
            {
                SingleStepTreeNode draggedTest = (SingleStepTreeNode)(e.Data.GetData( typeof( SingleStepTreeNode ) ));
                SingleStepTreeNode targetNode = tree.DropPosition.Node.Tag as SingleStepTreeNode;
                if(draggedTest == null || targetNode == null)
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                if(draggedTest.step.UniqueTestId != targetNode.step.UniqueTestId) // dragging onto a incompatible test case
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
            }
            else if(e.Data.GetDataPresent( typeof( GlobalValue ) ))//drag global parameter - parameter type must match
            {
                GlobalValue draggedParam = (GlobalValue)(e.Data.GetData( typeof( GlobalValue ) ));
                ParameterNode targetNode = tree.DropPosition.Node.Tag as ParameterNode;
                if(draggedParam == null || targetNode == null)
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                AbstractParameterValue draggedParamValue = globals.Get(draggedParam);
                if (draggedParamValue == null)
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                if(IsParameterTypeMatch(draggedParamValue, targetNode.parameter) == false) // dragging onto a incompatible parameter
                {
                    e.Effect = DragDropEffects.None;
                    return;
                }
                e.Effect = DragDropEffects.Link;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
		
		//allow dragging a single parameter node - it can be moved, copied (make a local parameter) or linked (make a common test run parameter)
        //allow draging a test case node - allow dragging to another test case with the same test case type
        private void tree_ItemDrag(object sender, ItemDragEventArgs e)
        {
            System.Diagnostics.Debug.Assert( !this.InvokeRequired, "InvokeRequired" );
            if(isRunning)
            {
                return;
            }

            if(tree.SelectedNodes.Count != 1)
            {
                return; // does not support dragging multiple nodes yet
            }

            //List<ParameterNode> parameters = new List<ParameterNode>( );
            //List<SingleStepTreeNode> testCases = new List<SingleStepTreeNode>( );
            foreach(var selNode in tree.SelectedNodes)
            {
                ParameterNode paramNode = selNode.Tag as ParameterNode;
                ResultNode resultNode = selNode.Tag as ResultNode;
                SingleStepTreeNode testCaseNode = selNode.Tag as SingleStepTreeNode;

                if(paramNode != null)
                {
                    tree.DoDragDrop( paramNode, DragDropEffects.Move | DragDropEffects.Copy | DragDropEffects.Link );
                    return;
                }

                if (resultNode != null)
                {
                    tree.DoDragDrop(resultNode, DragDropEffects.Link);
                    return;
                }

                if(testCaseNode != null)
                {
                    tree.DoDragDrop( testCaseNode, DragDropEffects.Move | DragDropEffects.Copy | DragDropEffects.Link );
                    return;
                }
            }            
        }
		
        private void tree_SelectionChanged(object sender, EventArgs e)
        {
            if(isRunning)
            {
                return;
            }

            List<ParameterNode> nodes = new List<ParameterNode>();
            foreach (var node in tree.SelectedNodes)
            {
                ParameterNode parameter = node.Tag as ParameterNode;
                if (parameter == null)
                { // if selection contains groups, test cases etc - do not continue
                    EnableParamValueEditor(false);
                    return;
                }
                nodes.Add(parameter);
            }
            if (nodes.Count == 0) // no parameter nodes selected
            {
                EnableParamValueEditor(false);
                return;
            }

            IParameter commonType;
            {
                HashSet<IParameter> selectedTypes = new HashSet<IParameter>();
                foreach (var node in nodes)
                {
                    selectedTypes.Add(node.parameter);

                    if (selectedTypes.Count > 1) //different types of parameters selected
                    {
                        EnableParamValueEditor(false);
                        return;
                    }
                }
                
                commonType = selectedTypes.First();
            }

            AbstractParameterValue parameterValueType = null;

            {
                HashSet<Type> valueTypes = new HashSet<Type>();
                foreach (var node in nodes)
                {
                    if (node.parameter.Value != null)
                    {
                        //parameterValueType = node.parameter.Value; 
                        bool found;
                        var val = GetParameterValueVisitor.Get(node.parameter.Value, globals, out found);
                        if (found)
                        {
                            parameterValueType = val;// if one valueType is found after this loop, this will be the only parameter value type to choose from.
                            valueTypes.Add(val.GetType());
                        }
                    }
                }

                if (valueTypes.Count > 1) //different types of parameters selected
                {
                    EnableParamValueEditor(false);
                    return;
                }
                else if (valueTypes.Count == 0)
                {
                    //note to self: using system.linq provides HashSet.First()... 
                    List<AbstractParameterValue> suitableTypes = SuitableParameterTypeHelper.Get(commonType);
                    if (suitableTypes == null || suitableTypes.Count == 0)
                    {//no parameter value class is suitable for this parameter
                        EnableParamValueEditor(false);
                        return;
                    }
                    if (suitableTypes.Count == 1)
                    {
                        parameterValueType = suitableTypes[0];
                    }
                    else // more than one parameter value type is suitable for this value type, make the user think about it...
                    {
                        var type = ValueTypeSelectionForm.Select(suitableTypes, globals);
                        if (type != null)
                        {
                            parameterValueType = type;
                        }
                        else
                        {
                            EnableParamValueEditor(false);
                            return;
                        }
                    }
                }
                else // 1
                {
                    //do nothing - already set above
                    //parameterValueType = valueTypes.First();
                }
            }

            // dont edit if the multiple-selection editing is not supported
            if (nodes.Count > 1 && ParameterTypeCollection.CanMultiEdit(parameterValueType) == false)
            {
                EnableParamValueEditor(false);
                return;
            }

            CurrentlyEditedValues.Clear();
            List<AbstractParameterValue> values = new List<AbstractParameterValue>();
            foreach (var node in nodes)
            {
                if (node.parameter.Value != null)
                {
                    bool found;
                    // make clones for existing values in case they are not saved
                    var value = GetParameterValueVisitor.Get(node.parameter.Value, globals, out found);
                    if (value != null && found)
                    {
                        var clone = value.Clone();
                        values.Add(clone);
                        CurrentlyEditedValues.Add(new EditingValueItem(node.parameter, clone));
                    }
                    else
                    {
                        var clone = parameterValueType.Clone();
                        values.Add(clone);
                        CurrentlyEditedValues.Add(new EditingValueItem(node.parameter, clone));
                    }
                }
                else
                {
                    var clone = parameterValueType.Clone();
                    values.Add(clone);
                    CurrentlyEditedValues.Add(new EditingValueItem(node.parameter, clone));
                }
            }

            EnableParamValueEditor(true);
            this.tabEditor.Edit(values, parameterValueType, globals);
        }

        private class EditingValueItem
        {
            public IParameter parameter;
            public AbstractParameterValue value;
            public EditingValueItem(IParameter parameter, AbstractParameterValue value)
            {
                this.parameter = parameter;
                this.value = value;
            }

            public void Assign()
            {
                parameter.Value = value;
            }
        }

        List<EditingValueItem> CurrentlyEditedValues = new List<EditingValueItem>();
                
        public void EnableParamValueEditor(bool enable)
        {
            groupParamValue.Enabled = enable;
            if(!enable)
            {
                tabEditor.Clear( );
            }
        }

        private void setButton_Click(object sender, EventArgs e)
        {
            this.tabEditor.Save();
            foreach (var val in CurrentlyEditedValues)
            {
                if(SetGlobalValueVisitor.Set( val.parameter.Value, val.value, globals ))
                {
                    //global value updated. 
                }
                else
                {
                    //assign local value
                    val.Assign();
                }
            }
            tree.Invalidate();
        }

        #region Drag and Drop
        

        

        private bool IsParameterTypeMatch(AbstractParameterValue draggedParamValue, IParameter paramType)
        {
            try {
                if (draggedParamValue is GlobalValue)
                {
                    draggedParamValue = globals.Get(draggedParamValue as GlobalValue).Value;
                }
            }
            catch
            {
            }

            if (draggedParamValue is GlobalParameter){
                draggedParamValue = (draggedParamValue as GlobalParameter).Value;
            }

            //TODO if dragging an integer value and paramType should be integer too and the draggedvalue could fit into min/max of param type, allow...

            foreach (var suitable in ParameterTypeCollection.GetSuitableValueTypes(paramType)){
                if (suitable.GetParamValueTypeHash() == draggedParamValue.GetParamValueTypeHash()){
                    return true;
                }
            }
             
            return false;
        }

        

        private void LinkParameter(IParameter from, IParameter to)
        {
            if (from.Value == null)
            {
                return;
            }
            if (IsGlobalParameter.Get( from.Value ))
            {
                //already global parameter, just set the same to "To" parameter
                to.Value = from.Value.Clone( );
                return;
            }
            foreach (var manager in globals.Managers.Values){
                if (manager.Scope == ParameterScope.TEST_RUN) {
                    GlobalParameter param = new GlobalParameter( from.Value );
                    Guid valueId = manager.Set( param );
                    from.Value = new GlobalValue( manager.ID, valueId );
                    to.Value = new GlobalValue( manager.ID, valueId );
                    return;
                }
            }
        }

        private void LinkResultToParameter(IParameter from, IParameter to)
        {
            if (from.Value == null)
            {
                from.Value = new TestResultParameter(Guid.NewGuid());
            }
            var res = from.Value as TestResultParameter;
            if (res == null)
            {
                return;
            }
            var id = res.ResultID;
            to.Value = new TestResultParameter(id);
        }

#endregion
        
        #region Context menu - change parameter scope

        private List<ParameterNode> GetSelectedParameters(bool notNullValueOnly)
        {
            List<ParameterNode> selectedParameters = new List<ParameterNode>( );
            foreach(var selNode in tree.SelectedNodes)
            {
                ParameterNode paramNode = selNode.Tag as ParameterNode;
                SingleStepTreeNode testCaseNode = selNode.Tag as SingleStepTreeNode;

                if(paramNode != null)
                {
                    if(notNullValueOnly)
                    {
                        bool found;
                        var value = GetParameterValueVisitor.Get( paramNode.parameter.Value, globals, out found );
                        if(found && value != null)
                        {
                            selectedParameters.Add( paramNode );
                        }
                    }
                    else
                    {
                        selectedParameters.Add( paramNode );
                    }
                }
            }
            return selectedParameters;
        }

        private void contextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            List<ParameterNode> selectedParameters = GetSelectedParameters( true );
            localToolStripMenuItem.Enabled = !isRunning && selectedParameters.Count > 0;
            testRunToolStripMenuItem.Enabled = !isRunning && selectedParameters.Count > 0;
            createGlobalParameterToolStripMenuItem.Enabled = !isRunning && selectedParameters.Count == 1;
            removeValueToolStripMenuItem.Enabled = !isRunning && selectedParameters.Count > 0;
        }

        private void localToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach(var param in GetSelectedParameters(true))
            {
                if(GetParameterScopeVisitor.Get( param.parameter.Value, globals ) != ParameterScope.LOCAL)
                {
                    bool found;
                    var value = GetParameterValueVisitor.Get(param.parameter.Value, globals, out found);
                    if (found){
                        param.parameter.Value = value.Clone( );
                    }                    
                }
            }
            tree.Invalidate( );
        }

        private void testRunToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach(var param in GetSelectedParameters(true))
            {
                if(GetParameterScopeVisitor.Get( param.parameter.Value, globals ) == ParameterScope.TEST_RUN)
                {
                    continue;
                }

                foreach(var manager in globals.Managers.Values)
                {
                    if(manager.Scope == ParameterScope.TEST_RUN)
                    {
                        bool found;
                        var value = GetParameterValueVisitor.Get(param.parameter.Value, globals, out found);
                        if(found)
                        {
                            GlobalParameter globalParam = new GlobalParameter( value );
                            Guid valueId = manager.Set( globalParam );
                            param.parameter.Value = new GlobalValue( manager.ID, valueId );
                        }
                        break;
                    }
                }
            }
            tree.Invalidate( );
        }

        private void createGlobalParameterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var selected = GetSelectedParameters( true );
            foreach(var param in selected)
            {
                if(GetParameterScopeVisitor.Get( param.parameter.Value, globals ) == ParameterScope.GLOBAL) // already global
                {
                    continue;
                }

                foreach(var manager in globals.Managers.Values)
                {
                    if(manager.Scope == ParameterScope.GLOBAL) // find global parameter manager in collection
                    {
                        bool found;
                        var value = GetParameterValueVisitor.Get( param.parameter.Value, globals, out found ); // get parameter value
                        if(found)
                        {
                            string name = param.ParamName;
                            if(InputBox.Get( "Global parameter name", "Enter parameter name", ref name ) == DialogResult.OK)
                            {
                                GlobalParameter globalParam = new GlobalParameter( name, value );
                                Guid valueId = manager.Set( globalParam );
                                param.parameter.Value = new GlobalValue( manager.ID, valueId );
                            }
                            break;
                        }
                    }
                }
            }
            tree.Invalidate( );     
        }
        #endregion

        private void removeValueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var param in GetSelectedParameters(true))
            {
                param.parameter.Value = null;
            }
            tree.Invalidate( );
        }
                
        private void nodeTextBox1_DrawText(object sender, Aga.Controls.Tree.NodeControls.DrawEventArgs e)
        {
            ParameterNode param = e.Node.Tag as ParameterNode;
            if(param == null)
            {
                return;
            }
            if(param.canBeNull == false)
            {
                bool found;
                var x = GetParameterValueVisitor.Get( param.parameter.Value, param.model.globals, out found );
                if( x == null || !found)
                {
                    e.TextColor = System.Drawing.Color.Blue;
                }
            }
        }

        bool isRunning = false;
        internal void SetRunning(bool isRunning)
        {
            this.isRunning = isRunning;
            EnableParamValueEditor( !isRunning );
        }

        int? oldPanel2Height;
        internal void ResizeBegin()
        {
            oldPanel2Height = splitContainer.Panel2.Height;
        }

        internal void ResizeEnd()
        {
            if (oldPanel2Height.HasValue)
            {
                splitContainer.SplitterDistance = Math.Max(0, splitContainer.Height - splitContainer.SplitterWidth - oldPanel2Height.Value);
            }
        }
    }
}
