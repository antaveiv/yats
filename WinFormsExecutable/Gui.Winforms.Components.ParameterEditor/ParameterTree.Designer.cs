﻿namespace yats.Gui.Winforms.Components.ParameterEditor
{
    partial class ParameterTree
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ParameterTree));
            this.nodeIcon1 = new Aga.Controls.Tree.NodeControls.NodeIcon();
            this.treeColumn1 = new Aga.Controls.Tree.TreeColumn();
            this.tree = new Aga.Controls.Tree.TreeViewAdv();
            this.treeColumn2 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumnTypeDescr = new Aga.Controls.Tree.TreeColumn();
            this.treeColumn3 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumn4 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumnTypeDetails = new Aga.Controls.Tree.TreeColumn();
            this.treeColumnDescr = new Aga.Controls.Tree.TreeColumn();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parameterScopeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.localToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testRunToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createGlobalParameterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assignDefaultValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nodeTextBox1 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.nodeTextBox5 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.nodeTextBox2 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.nodeTextBox3 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.nodeTextBox4 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.nodeTextBox6 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.editValueToolstripButton = new System.Windows.Forms.ToolStripButton();
            this.removeValuesToolstripButton = new System.Windows.Forms.ToolStripButton();
            this.assignDefaultValueToolbarButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btShowTypeDetails = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.editDescriptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // nodeIcon1
            // 
            this.nodeIcon1.DataPropertyName = "ParamIcon";
            this.nodeIcon1.LeftMargin = 1;
            this.nodeIcon1.ParentColumn = this.treeColumn1;
            this.nodeIcon1.ScaleMode = Aga.Controls.Tree.ImageScaleMode.Clip;
            // 
            // treeColumn1
            // 
            this.treeColumn1.Header = "";
            this.treeColumn1.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn1.TooltipText = null;
            this.treeColumn1.Width = 150;
            // 
            // tree
            // 
            this.tree.AllowDrop = true;
            this.tree.BackColor = System.Drawing.SystemColors.Window;
            this.tree.Columns.Add(this.treeColumn1);
            this.tree.Columns.Add(this.treeColumn2);
            this.tree.Columns.Add(this.treeColumnTypeDescr);
            this.tree.Columns.Add(this.treeColumn3);
            this.tree.Columns.Add(this.treeColumn4);
            this.tree.Columns.Add(this.treeColumnTypeDetails);
            this.tree.Columns.Add(this.treeColumnDescr);
            this.tree.ContextMenuStrip = this.contextMenuStrip;
            this.tree.DefaultToolTipProvider = null;
            this.tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tree.DragDropMarkColor = System.Drawing.Color.Black;
            this.tree.FullRowSelect = true;
            this.tree.LineColor = System.Drawing.SystemColors.ControlDark;
            this.tree.Location = new System.Drawing.Point(0, 25);
            this.tree.Model = null;
            this.tree.Name = "tree";
            this.tree.NodeControls.Add(this.nodeIcon1);
            this.tree.NodeControls.Add(this.nodeTextBox1);
            this.tree.NodeControls.Add(this.nodeTextBox5);
            this.tree.NodeControls.Add(this.nodeTextBox2);
            this.tree.NodeControls.Add(this.nodeTextBox3);
            this.tree.NodeControls.Add(this.nodeTextBox4);
            this.tree.NodeControls.Add(this.nodeTextBox6);
            this.tree.SelectedNode = null;
            this.tree.SelectionMode = Aga.Controls.Tree.TreeSelectionMode.Multi;
            this.tree.Size = new System.Drawing.Size(1136, 528);
            this.tree.TabIndex = 0;
            this.tree.Text = "Parameters";
            this.tree.UseColumns = true;
            this.tree.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tree_ItemDrag);
            this.tree.NodeMouseDoubleClick += new System.EventHandler<Aga.Controls.Tree.TreeNodeAdvMouseEventArgs>(this.tree_NodeMouseDoubleClick);
            this.tree.SelectionChanged += new System.EventHandler(this.tree_SelectionChanged);
            this.tree.DragDrop += new System.Windows.Forms.DragEventHandler(this.tree_DragDrop);
            this.tree.DragOver += new System.Windows.Forms.DragEventHandler(this.tree_DragOver);
            this.tree.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tree_KeyDown);
            // 
            // treeColumn2
            // 
            this.treeColumn2.Header = "Name";
            this.treeColumn2.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn2.TooltipText = null;
            this.treeColumn2.Width = 200;
            // 
            // treeColumnTypeDescr
            // 
            this.treeColumnTypeDescr.Header = "Type";
            this.treeColumnTypeDescr.IsVisible = false;
            this.treeColumnTypeDescr.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumnTypeDescr.TooltipText = null;
            this.treeColumnTypeDescr.Width = 100;
            // 
            // treeColumn3
            // 
            this.treeColumn3.Header = "Scope";
            this.treeColumn3.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn3.TooltipText = null;
            this.treeColumn3.Width = 100;
            // 
            // treeColumn4
            // 
            this.treeColumn4.Header = "Value";
            this.treeColumn4.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn4.TooltipText = null;
            this.treeColumn4.Width = 200;
            // 
            // treeColumnTypeDetails
            // 
            this.treeColumnTypeDetails.Header = "Type";
            this.treeColumnTypeDetails.IsVisible = false;
            this.treeColumnTypeDetails.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumnTypeDetails.TooltipText = null;
            this.treeColumnTypeDetails.Width = 200;
            // 
            // treeColumnDescr
            // 
            this.treeColumnDescr.Header = "Description";
            this.treeColumnDescr.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumnDescr.TooltipText = null;
            this.treeColumnDescr.Width = 200;
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editValueToolStripMenuItem,
            this.editDescriptionToolStripMenuItem,
            this.parameterScopeToolStripMenuItem,
            this.removeValueToolStripMenuItem,
            this.assignDefaultValueToolStripMenuItem,
            this.toolStripSeparator3,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip1";
            this.contextMenuStrip.Size = new System.Drawing.Size(223, 208);
            this.contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip_Opening);
            // 
            // editValueToolStripMenuItem
            // 
            this.editValueToolStripMenuItem.Name = "editValueToolStripMenuItem";
            this.editValueToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.editValueToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.editValueToolStripMenuItem.Text = "Edit value...";
            this.editValueToolStripMenuItem.Click += new System.EventHandler(this.editValueToolstripButton_Click);
            // 
            // parameterScopeToolStripMenuItem
            // 
            this.parameterScopeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.localToolStripMenuItem,
            this.testRunToolStripMenuItem,
            this.createGlobalParameterToolStripMenuItem});
            this.parameterScopeToolStripMenuItem.Name = "parameterScopeToolStripMenuItem";
            this.parameterScopeToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.parameterScopeToolStripMenuItem.Text = "Parameter scope";
            // 
            // localToolStripMenuItem
            // 
            this.localToolStripMenuItem.Name = "localToolStripMenuItem";
            this.localToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.localToolStripMenuItem.Text = "Local";
            this.localToolStripMenuItem.Click += new System.EventHandler(this.localToolStripMenuItem_Click);
            // 
            // testRunToolStripMenuItem
            // 
            this.testRunToolStripMenuItem.Name = "testRunToolStripMenuItem";
            this.testRunToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.testRunToolStripMenuItem.Text = "Test run";
            this.testRunToolStripMenuItem.Click += new System.EventHandler(this.testRunToolStripMenuItem_Click);
            // 
            // createGlobalParameterToolStripMenuItem
            // 
            this.createGlobalParameterToolStripMenuItem.Name = "createGlobalParameterToolStripMenuItem";
            this.createGlobalParameterToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.createGlobalParameterToolStripMenuItem.Text = "Create global parameter...";
            this.createGlobalParameterToolStripMenuItem.Click += new System.EventHandler(this.createGlobalParameterToolStripMenuItem_Click);
            // 
            // removeValueToolStripMenuItem
            // 
            this.removeValueToolStripMenuItem.Name = "removeValueToolStripMenuItem";
            this.removeValueToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.removeValueToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.removeValueToolStripMenuItem.Text = "Remove value";
            this.removeValueToolStripMenuItem.Click += new System.EventHandler(this.removeValueToolStripMenuItem_Click);
            // 
            // assignDefaultValueToolStripMenuItem
            // 
            this.assignDefaultValueToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("assignDefaultValueToolStripMenuItem.Image")));
            this.assignDefaultValueToolStripMenuItem.Name = "assignDefaultValueToolStripMenuItem";
            this.assignDefaultValueToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.assignDefaultValueToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.assignDefaultValueToolStripMenuItem.Text = "Assign default value";
            this.assignDefaultValueToolStripMenuItem.Click += new System.EventHandler(this.assignDefaultValueToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(219, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.cutToolStripMenuItem.Text = "Cut";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // nodeTextBox1
            // 
            this.nodeTextBox1.DataPropertyName = "ParamName";
            this.nodeTextBox1.IncrementalSearchEnabled = true;
            this.nodeTextBox1.LeftMargin = 3;
            this.nodeTextBox1.ParentColumn = this.treeColumn2;
            this.nodeTextBox1.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            this.nodeTextBox1.DrawText += new System.EventHandler<Aga.Controls.Tree.NodeControls.DrawEventArgs>(this.nodeTextBox1_DrawText);
            // 
            // nodeTextBox5
            // 
            this.nodeTextBox5.DataPropertyName = "TypeDescr";
            this.nodeTextBox5.IncrementalSearchEnabled = true;
            this.nodeTextBox5.LeftMargin = 3;
            this.nodeTextBox5.ParentColumn = this.treeColumnTypeDescr;
            this.nodeTextBox5.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeTextBox2
            // 
            this.nodeTextBox2.DataPropertyName = "ParamType";
            this.nodeTextBox2.IncrementalSearchEnabled = true;
            this.nodeTextBox2.LeftMargin = 3;
            this.nodeTextBox2.ParentColumn = this.treeColumn3;
            this.nodeTextBox2.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeTextBox3
            // 
            this.nodeTextBox3.DataPropertyName = "ParamValue";
            this.nodeTextBox3.IncrementalSearchEnabled = true;
            this.nodeTextBox3.LeftMargin = 3;
            this.nodeTextBox3.ParentColumn = this.treeColumn4;
            this.nodeTextBox3.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeTextBox4
            // 
            this.nodeTextBox4.DataPropertyName = "Type";
            this.nodeTextBox4.IncrementalSearchEnabled = true;
            this.nodeTextBox4.LeftMargin = 3;
            this.nodeTextBox4.ParentColumn = this.treeColumnTypeDetails;
            this.nodeTextBox4.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeTextBox6
            // 
            this.nodeTextBox6.DataPropertyName = "Description";
            this.nodeTextBox6.IncrementalSearchEnabled = true;
            this.nodeTextBox6.LeftMargin = 3;
            this.nodeTextBox6.ParentColumn = this.treeColumnDescr;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editValueToolstripButton,
            this.removeValuesToolstripButton,
            this.assignDefaultValueToolbarButton,
            this.toolStripSeparator1,
            this.btShowTypeDetails,
            this.toolStripSeparator2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1136, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // editValueToolstripButton
            // 
            this.editValueToolstripButton.Image = ((System.Drawing.Image)(resources.GetObject("editValueToolstripButton.Image")));
            this.editValueToolstripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editValueToolstripButton.Name = "editValueToolstripButton";
            this.editValueToolstripButton.Size = new System.Drawing.Size(47, 22);
            this.editValueToolstripButton.Text = "Edit";
            this.editValueToolstripButton.ToolTipText = "Edit value (Ctrl+E or double-click)";
            this.editValueToolstripButton.Click += new System.EventHandler(this.editValueToolstripButton_Click);
            // 
            // removeValuesToolstripButton
            // 
            this.removeValuesToolstripButton.Enabled = false;
            this.removeValuesToolstripButton.Image = ((System.Drawing.Image)(resources.GetObject("removeValuesToolstripButton.Image")));
            this.removeValuesToolstripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.removeValuesToolstripButton.Name = "removeValuesToolstripButton";
            this.removeValuesToolstripButton.Size = new System.Drawing.Size(70, 22);
            this.removeValuesToolstripButton.Text = "Remove";
            this.removeValuesToolstripButton.ToolTipText = "Remove value (Del)";
            this.removeValuesToolstripButton.Click += new System.EventHandler(this.removeValueToolStripMenuItem_Click);
            // 
            // assignDefaultValueToolbarButton
            // 
            this.assignDefaultValueToolbarButton.Image = ((System.Drawing.Image)(resources.GetObject("assignDefaultValueToolbarButton.Image")));
            this.assignDefaultValueToolbarButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.assignDefaultValueToolbarButton.Name = "assignDefaultValueToolbarButton";
            this.assignDefaultValueToolbarButton.Size = new System.Drawing.Size(96, 22);
            this.assignDefaultValueToolbarButton.Text = "Default value";
            this.assignDefaultValueToolbarButton.ToolTipText = "Set default parameter value, as declared in test case (Ctrl+D)";
            this.assignDefaultValueToolbarButton.Click += new System.EventHandler(this.assignDefaultValueToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btShowTypeDetails
            // 
            this.btShowTypeDetails.CheckOnClick = true;
            this.btShowTypeDetails.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btShowTypeDetails.Image = ((System.Drawing.Image)(resources.GetObject("btShowTypeDetails.Image")));
            this.btShowTypeDetails.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btShowTypeDetails.Name = "btShowTypeDetails";
            this.btShowTypeDetails.Size = new System.Drawing.Size(23, 22);
            this.btShowTypeDetails.Text = "Show type details";
            this.btShowTypeDetails.ToolTipText = "Show parameter type column";
            this.btShowTypeDetails.CheckedChanged += new System.EventHandler(this.btShowTypeDetails_CheckedChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            this.toolStripSeparator2.Visible = false;
            // 
            // editDescriptionToolStripMenuItem
            // 
            this.editDescriptionToolStripMenuItem.Name = "editDescriptionToolStripMenuItem";
            this.editDescriptionToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.editDescriptionToolStripMenuItem.Text = "Edit description...";
            this.editDescriptionToolStripMenuItem.Click += new System.EventHandler(this.editDescriptionToolStripMenuItem_Click);
            // 
            // ParameterTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tree);
            this.Controls.Add(this.toolStrip1);
            this.Name = "ParameterTree";
            this.Size = new System.Drawing.Size(1136, 553);
            this.Load += new System.EventHandler(this.ParameterTree_Load);
            this.contextMenuStrip.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Aga.Controls.Tree.NodeControls.NodeIcon nodeIcon1;
        private Aga.Controls.Tree.TreeColumn treeColumn1;
        private Aga.Controls.Tree.TreeViewAdv tree;
        private Aga.Controls.Tree.TreeColumn treeColumn2;
        private Aga.Controls.Tree.TreeColumn treeColumn3;
        private Aga.Controls.Tree.TreeColumn treeColumn4;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem parameterScopeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem localToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testRunToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createGlobalParameterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeValueToolStripMenuItem;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox1;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox2;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox3;
        private System.Windows.Forms.ToolStripMenuItem editValueToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton editValueToolstripButton;
        private System.Windows.Forms.ToolStripButton removeValuesToolstripButton;
        private Aga.Controls.Tree.TreeColumn treeColumnTypeDetails;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btShowTypeDetails;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox4;
        private System.Windows.Forms.ToolStripMenuItem assignDefaultValueToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton assignDefaultValueToolbarButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private Aga.Controls.Tree.TreeColumn treeColumnTypeDescr;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox5;
        private Aga.Controls.Tree.TreeColumn treeColumnDescr;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox6;
        private System.Windows.Forms.ToolStripMenuItem editDescriptionToolStripMenuItem;

    }
}
