﻿namespace yats.Gui.Winforms.Components.ParameterEditor
{
    partial class ParameterEditorPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.groupBoxParamTree = new System.Windows.Forms.GroupBox();
            this.tree = new Aga.Controls.Tree.TreeViewAdv();
            this.treeColumn1 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumn2 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumn3 = new Aga.Controls.Tree.TreeColumn();
            this.treeColumn4 = new Aga.Controls.Tree.TreeColumn();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.parameterScopeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.localToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testRunToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createGlobalParameterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nodeIcon1 = new Aga.Controls.Tree.NodeControls.NodeIcon();
            this.nodeTextBox1 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.nodeTextBox2 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.nodeTextBox3 = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.groupParamValue = new System.Windows.Forms.GroupBox();
            this.setButton = new System.Windows.Forms.Button();
            this.tabEditor = new yats.Gui.Winforms.Components.ParameterEditor.TabEditor();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.groupBoxParamTree.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.groupParamValue.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.groupBoxParamTree);
            this.splitContainer.Panel1MinSize = 0;
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.groupParamValue);
            this.splitContainer.Panel2MinSize = 0;
            this.splitContainer.Size = new System.Drawing.Size(751, 282);
            this.splitContainer.SplitterDistance = 144;
            this.splitContainer.TabIndex = 0;
            // 
            // groupBoxParamTree
            // 
            this.groupBoxParamTree.Controls.Add(this.tree);
            this.groupBoxParamTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxParamTree.Location = new System.Drawing.Point(0, 0);
            this.groupBoxParamTree.Name = "groupBoxParamTree";
            this.groupBoxParamTree.Size = new System.Drawing.Size(751, 144);
            this.groupBoxParamTree.TabIndex = 1;
            this.groupBoxParamTree.TabStop = false;
            this.groupBoxParamTree.Text = "Parameters";
            // 
            // tree
            // 
            this.tree.AllowDrop = true;
            this.tree.BackColor = System.Drawing.SystemColors.Window;
            this.tree.Columns.Add(this.treeColumn1);
            this.tree.Columns.Add(this.treeColumn2);
            this.tree.Columns.Add(this.treeColumn3);
            this.tree.Columns.Add(this.treeColumn4);
            this.tree.ContextMenuStrip = this.contextMenuStrip;
            this.tree.DefaultToolTipProvider = null;
            this.tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tree.DragDropMarkColor = System.Drawing.Color.Black;
            this.tree.FullRowSelect = true;
            this.tree.LineColor = System.Drawing.SystemColors.ControlDark;
            this.tree.Location = new System.Drawing.Point(3, 16);
            this.tree.Model = null;
            this.tree.Name = "tree";
            this.tree.NodeControls.Add(this.nodeIcon1);
            this.tree.NodeControls.Add(this.nodeTextBox1);
            this.tree.NodeControls.Add(this.nodeTextBox2);
            this.tree.NodeControls.Add(this.nodeTextBox3);
            this.tree.SelectedNode = null;
            this.tree.SelectionMode = Aga.Controls.Tree.TreeSelectionMode.Multi;
            this.tree.Size = new System.Drawing.Size(745, 125);
            this.tree.TabIndex = 0;
            this.tree.Text = "Parameters";
            this.tree.UseColumns = true;
            this.tree.SelectionChanged += new System.EventHandler(this.tree_SelectionChanged);
            this.tree.DragOver += new System.Windows.Forms.DragEventHandler(this.tree_DragOver);
            this.tree.DragDrop += new System.Windows.Forms.DragEventHandler(this.tree_DragDrop);
            this.tree.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tree_ItemDrag);
            // 
            // treeColumn1
            // 
            this.treeColumn1.Header = "";
            this.treeColumn1.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn1.TooltipText = null;
            this.treeColumn1.Width = 150;
            // 
            // treeColumn2
            // 
            this.treeColumn2.Header = "Name";
            this.treeColumn2.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn2.TooltipText = null;
            this.treeColumn2.Width = 200;
            // 
            // treeColumn3
            // 
            this.treeColumn3.Header = "Scope";
            this.treeColumn3.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn3.TooltipText = null;
            this.treeColumn3.Width = 100;
            // 
            // treeColumn4
            // 
            this.treeColumn4.Header = "Value";
            this.treeColumn4.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumn4.TooltipText = null;
            this.treeColumn4.Width = 200;
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.parameterScopeToolStripMenuItem,
            this.removeValueToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip1";
            this.contextMenuStrip.Size = new System.Drawing.Size(167, 48);
            this.contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip_Opening);
            // 
            // parameterScopeToolStripMenuItem
            // 
            this.parameterScopeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.localToolStripMenuItem,
            this.testRunToolStripMenuItem,
            this.createGlobalParameterToolStripMenuItem});
            this.parameterScopeToolStripMenuItem.Name = "parameterScopeToolStripMenuItem";
            this.parameterScopeToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.parameterScopeToolStripMenuItem.Text = "Parameter scope";
            // 
            // localToolStripMenuItem
            // 
            this.localToolStripMenuItem.Name = "localToolStripMenuItem";
            this.localToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.localToolStripMenuItem.Text = "Local";
            this.localToolStripMenuItem.Click += new System.EventHandler(this.localToolStripMenuItem_Click);
            // 
            // testRunToolStripMenuItem
            // 
            this.testRunToolStripMenuItem.Name = "testRunToolStripMenuItem";
            this.testRunToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.testRunToolStripMenuItem.Text = "Test run";
            this.testRunToolStripMenuItem.Click += new System.EventHandler(this.testRunToolStripMenuItem_Click);
            // 
            // createGlobalParameterToolStripMenuItem
            // 
            this.createGlobalParameterToolStripMenuItem.Name = "createGlobalParameterToolStripMenuItem";
            this.createGlobalParameterToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.createGlobalParameterToolStripMenuItem.Text = "Create global parameter...";
            this.createGlobalParameterToolStripMenuItem.Click += new System.EventHandler(this.createGlobalParameterToolStripMenuItem_Click);
            // 
            // removeValueToolStripMenuItem
            // 
            this.removeValueToolStripMenuItem.Name = "removeValueToolStripMenuItem";
            this.removeValueToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.removeValueToolStripMenuItem.Text = "Remove value";
            this.removeValueToolStripMenuItem.Click += new System.EventHandler(this.removeValueToolStripMenuItem_Click);
            // 
            // nodeIcon1
            // 
            this.nodeIcon1.DataPropertyName = "ParamIcon";
            this.nodeIcon1.LeftMargin = 1;
            this.nodeIcon1.ParentColumn = this.treeColumn1;
            this.nodeIcon1.ScaleMode = Aga.Controls.Tree.ImageScaleMode.Clip;
            // 
            // nodeTextBox1
            // 
            this.nodeTextBox1.DataPropertyName = "ParamName";
            this.nodeTextBox1.IncrementalSearchEnabled = true;
            this.nodeTextBox1.LeftMargin = 3;
            this.nodeTextBox1.ParentColumn = this.treeColumn2;
            this.nodeTextBox1.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            this.nodeTextBox1.DrawText += new System.EventHandler<Aga.Controls.Tree.NodeControls.DrawEventArgs>(this.nodeTextBox1_DrawText);
            // 
            // nodeTextBox2
            // 
            this.nodeTextBox2.DataPropertyName = "ParamType";
            this.nodeTextBox2.IncrementalSearchEnabled = true;
            this.nodeTextBox2.LeftMargin = 3;
            this.nodeTextBox2.ParentColumn = this.treeColumn3;
            this.nodeTextBox2.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // nodeTextBox3
            // 
            this.nodeTextBox3.DataPropertyName = "ParamValue";
            this.nodeTextBox3.IncrementalSearchEnabled = true;
            this.nodeTextBox3.LeftMargin = 3;
            this.nodeTextBox3.ParentColumn = this.treeColumn4;
            this.nodeTextBox3.Trimming = System.Drawing.StringTrimming.EllipsisCharacter;
            // 
            // groupParamValue
            // 
            this.groupParamValue.Controls.Add(this.tabEditor);
            this.groupParamValue.Controls.Add(this.setButton);
            this.groupParamValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupParamValue.Location = new System.Drawing.Point(0, 0);
            this.groupParamValue.Name = "groupParamValue";
            this.groupParamValue.Size = new System.Drawing.Size(751, 134);
            this.groupParamValue.TabIndex = 0;
            this.groupParamValue.TabStop = false;
            this.groupParamValue.Text = "Value";
            // 
            // setButton
            // 
            this.setButton.Location = new System.Drawing.Point(6, 19);
            this.setButton.Name = "setButton";
            this.setButton.Size = new System.Drawing.Size(75, 23);
            this.setButton.TabIndex = 3;
            this.setButton.Text = "&Set value";
            this.setButton.UseVisualStyleBackColor = true;
            this.setButton.Click += new System.EventHandler(this.setButton_Click);
            // 
            // tabEditor
            // 
            this.tabEditor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabEditor.Location = new System.Drawing.Point(87, 19);
            this.tabEditor.Name = "tabEditor";
            this.tabEditor.Size = new System.Drawing.Size(658, 108);
            this.tabEditor.TabIndex = 4;
            // 
            // ParameterEditorPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer);
            this.Name = "ParameterEditorPanel";
            this.Size = new System.Drawing.Size(751, 282);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.ResumeLayout(false);
            this.groupBoxParamTree.ResumeLayout(false);
            this.contextMenuStrip.ResumeLayout(false);
            this.groupParamValue.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.GroupBox groupBoxParamTree;
        private Aga.Controls.Tree.TreeViewAdv tree;
        private Aga.Controls.Tree.TreeColumn treeColumn1;
        private Aga.Controls.Tree.TreeColumn treeColumn2;
        private Aga.Controls.Tree.TreeColumn treeColumn3;
        private Aga.Controls.Tree.TreeColumn treeColumn4;
        private System.Windows.Forms.GroupBox groupParamValue;
        private Aga.Controls.Tree.NodeControls.NodeIcon nodeIcon1;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox1;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox2;
        private Aga.Controls.Tree.NodeControls.NodeTextBox nodeTextBox3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.Button setButton;
        private TabEditor tabEditor;
        private System.Windows.Forms.ToolStripMenuItem parameterScopeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem localToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testRunToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createGlobalParameterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeValueToolStripMenuItem;
    }
}
