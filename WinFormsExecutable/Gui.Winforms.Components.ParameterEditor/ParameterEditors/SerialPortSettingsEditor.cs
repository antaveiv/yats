﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using InTheHand.Net;
using InTheHand.Net.Sockets;
using InTheHand.Windows.Forms;
using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using yats.TestCase.Parameter;
using yats.Utilities;
using System.IO;
using Be.Windows.Forms;

namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    public partial class SerialPortSettingsEditor : UserControl, IEditor
    {
        [DebuggerStepThrough]
        public SerialPortSettingsEditor()
        {
            InitializeComponent();
        }

        #region IEditor Members

        string[] currentSerialPortNames = new string[0];

        public void ParameterToEditor(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ParameterToEditor(parameter); }));
            }
            else
            {
                portName.Items.Clear();
                currentSerialPortNames = SerialPortExtension.OrderedPortNames(false);
                portName.Items.AddRange(currentSerialPortNames);
                stopBits.DataSource = Enum.GetValues(typeof(StopBits));
                parity.DataSource = Enum.GetValues(typeof(Parity));
                handshake.DataSource = Enum.GetValues(typeof(Handshake));
                baudRate.DataSource = new int[]{110,
                    300,
                    600,
                    1200,
                    2400,
                    4800,
                    9600,
                    14400,
                    19200,
                    28800,
                    38400,
                    56000,
                    57600,
                    115200,
                    128000,
                    153600,
                    230400,
                    256000,
                    460800,
                    921600};
                dataBits.DataSource = new int[] { 8, 7, 6, 5 };

                
                PortSettings param = parameter as PortSettings;
                if (param != null)
                {
                    cbDiscardDataWithoutParsers.Checked = param.DiscardDataWithoutParsers;
                    //serial port settings
                    baudRate.SelectedItem = param.SerialPort.BaudRate;
                    portName.SelectedItem = param.SerialPort.PortName;
                    dataBits.SelectedItem = param.SerialPort.DataBits;
                    stopBits.SelectedItem = param.SerialPort.StopBits;
                    parity.SelectedItem = param.SerialPort.Parity;
                    handshake.SelectedItem = param.SerialPort.Handshake;
                    switch (param.SerialPort.RtsEnable)
                    {
                        case RtsEnableEnum.ON:
                            rbRtsOn.Checked = true;
                            break;
                        case RtsEnableEnum.OFF:
                            rbRtsOff.Checked = true;
                            break;
                        case RtsEnableEnum.DONT_CHANGE:
                            rbRtsNoChange.Checked = true;
                            break;
                    }
                    dtrEnable.Checked = param.SerialPort.DtrEnable;

                    //tcp-ip client
                    if (param.TcpIpClient.IsConfigured)
                    {
                        tcpClientAddress.Text = param.TcpIpClient.Address;
                        tcpClientPort.Text = param.TcpIpClient.PortNumber.ToString();
                        tcpClientKeepAlive.Checked = param.TcpIpClient.KeepAlive;
                    }

                    //tcp-ip server
                    if (param.TcpIpServer.IsConfigured)
                    {
                        tcpServerPort.Value = param.TcpIpServer.PortNumber;
                        tcpServerConnectTimeout.Value = 0;
                        if (param.TcpIpServer.ClientConnectTimeoutMs.HasValue)
                        {
                            tcpServerConnectTimeout.Value = (param.TcpIpServer.ClientConnectTimeoutMs.Value / 1000);
                        }
                        tcpServerAcceptLaterConnections.Checked = param.TcpIpServer.ReplaceSocket;
                        tcpServerKeepAlive.Checked = param.TcpIpServer.KeepAlive;
                    }

                    //UDP socket
                    if (param.UdpSocket.IsConfigured)
                    {
                        udpLocalPort.Value = param.UdpSocket.LocalPortNumber;
                        if (param.UdpSocket.Broadcast)
                        {
                            udpBroadcast.Checked = true;
                            if (param.UdpSocket.RemoteIp != null && param.UdpSocket.RemoteIp.ToString() != "255.255.255.255")
                            {
                                udpBroadcastIpCustom.Checked = true;                                
                            }
                            else
                            {
                                udpBroadcastIpDefault.Checked = true;
                            }
                        }
                        else if (param.UdpSocket.OverwriteIP)
                        {
                            udpOverwritePort.Checked = true;
                        }
                        else
                        {
                            udpUnicast.Checked = true;
                        }
                        udpRemotePort.Value = param.UdpSocket.RemotePortNumber;

                        if (param.UdpSocket.RemoteIp != null)
                        {
                            udpIpUnicastRemoteAddress.SetAddressBytes(param.UdpSocket.RemoteIp.GetAddressBytes());
                            udpIpBroadcastRemoteAddress.SetAddressBytes(param.UdpSocket.RemoteIp.GetAddressBytes());
                        }
                    }

                    if (param.BluetoothPort != null && param.BluetoothPort.Address != null) {
                        try
                        {
                            selectedBluetoothDevice = new BluetoothDeviceInfo(param.BluetoothPort.Address);
                            lbSelectedBluetoothDevice.Text = selectedBluetoothDevice.DeviceName;
                            tbBluetoothPin.Text = param.BluetoothPort.PIN;
                        }
                        catch (PlatformNotSupportedException ex)
                        {
                            tabs.TabPages.Remove(tabPageBluetooth);
                            if (param.Selected == PortSettings.SelectedSetup.BLUETOOTH)
                            {
                                MessageBox.Show(ex.Message);
                                param.Selected = PortSettings.SelectedSetup.SERIAL;
                            }
                        }
                    }

                    switch (param.Selected)
                    {
                        case PortSettings.SelectedSetup.SERIAL:
                            tabs.SelectedTab = tabPageSerial;
                            break;
                        case PortSettings.SelectedSetup.TCP_IP_CLIENT:
                            tabs.SelectedTab = tabPageTcpIpClient;
                            break;
                        case PortSettings.SelectedSetup.TCP_IP_SERVER:
                            tabs.SelectedTab = tabPageTcpIpServer;
                            break;
                        case PortSettings.SelectedSetup.SSH_CLIENT:
                            tabs.SelectedTab = tabPageSshClient;
                            break;
                        case PortSettings.SelectedSetup.UDP_SOCKET:
                            tabs.SelectedTab = tabPageUdpSocket;
                            break;
                        case PortSettings.SelectedSetup.BLUETOOTH:
                            tabs.SelectedTab = tabPageBluetooth;
                            break;
                        case PortSettings.SelectedSetup.EMULATOR:
                            tabs.SelectedTab = tabPageEmulator;
                            break;
                    }

                    //SSH client
                    sshClientHost.Text = param.SshClient.Host;
                    sshClientUser.Text = param.SshClient.User;
                    sshClientPassword.Text = param.SshClient.Password;

                    if (param.Emulator != null)
                    {
                        rbEmulatorFromFile.Checked = param.Emulator.DataSource == PortSettings.PortEmulatorInfo.DataSouceType.FILE;
                        rbEmulatorFromBytes.Checked = param.Emulator.DataSource == PortSettings.PortEmulatorInfo.DataSouceType.BYTE_ARRAY;
                        tbEmulatorFileName.Text = param.Emulator.FileName;
                        emulatorHexEditor.SetBytes(param.Emulator.Data ?? new byte[]{} );
                    }
                }
            }
        }

        public void EditorToParameter(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameter); }));
            }
            else
            {
                PortSettings param = parameter as PortSettings;
                if (param != null)
                {
                    param.DiscardDataWithoutParsers = cbDiscardDataWithoutParsers.Checked;
                    //serial port settings
                    if (tabs.SelectedTab == tabPageSerial)
                    {
                        param.Selected = PortSettings.SelectedSetup.SERIAL;
                        param.SerialPort.BaudRate = (int)baudRate.SelectedItem;
                        param.SerialPort.PortName = (string)portName.SelectedItem;
                        param.SerialPort.DataBits = (int)dataBits.SelectedItem;
                        param.SerialPort.StopBits = (StopBits)stopBits.SelectedItem;
                        param.SerialPort.Parity = (Parity)parity.SelectedItem;
                        param.SerialPort.Handshake = (Handshake)handshake.SelectedItem;
                        if (rbRtsOn.Checked)
                        {
                            param.SerialPort.RtsEnable = RtsEnableEnum.ON;
                        }
                        else if (rbRtsOff.Checked)
                        {
                            param.SerialPort.RtsEnable = RtsEnableEnum.OFF;
                        }
                        else if (rbRtsNoChange.Checked)
                        {
                            param.SerialPort.RtsEnable = RtsEnableEnum.DONT_CHANGE;
                        }
                        param.SerialPort.DtrEnable = dtrEnable.Checked;
                    }

                    //tcp-ip
                    if (tabs.SelectedTab == tabPageTcpIpClient)
                    {
                        param.Selected = PortSettings.SelectedSetup.TCP_IP_CLIENT;
                        param.TcpIpClient.Address = tcpClientAddress.Text;
                        int result;
                        if (int.TryParse(tcpClientPort.Text, out result))
                        {
                            param.TcpIpClient.PortNumber = result;
                        }
                        param.TcpIpClient.KeepAlive = tcpClientKeepAlive.Checked;
                    }

                    if (tabs.SelectedTab == tabPageTcpIpServer)
                    {
                        param.Selected = PortSettings.SelectedSetup.TCP_IP_SERVER;
                        param.TcpIpServer.PortNumber = (int)tcpServerPort.Value;
                        if (tcpServerConnectTimeout.Value == 0)
                        {
                            param.TcpIpServer.ClientConnectTimeoutMs = null;
                        }
                        else
                        {
                            param.TcpIpServer.ClientConnectTimeoutMs = (int)tcpServerConnectTimeout.Value * 1000;
                        }
                        param.TcpIpServer.ReplaceSocket = tcpServerAcceptLaterConnections.Checked;
                        param.TcpIpServer.KeepAlive = tcpServerKeepAlive.Checked;
                    }

                    if (tabs.SelectedTab == tabPageSshClient)
                    {
                        param.Selected = PortSettings.SelectedSetup.SSH_CLIENT;
                        //SSH client
                        param.SshClient.Host = sshClientHost.Text;
                        param.SshClient.User = sshClientUser.Text;
                        param.SshClient.Password = sshClientPassword.Text;
                    }

                    if (tabs.SelectedTab == tabPageUdpSocket)
                    {
                        param.Selected = PortSettings.SelectedSetup.UDP_SOCKET;
                        if (udpOverwritePort.Checked)
                        {
                            param.UdpSocket = new PortSettings.UdpSocketInfo(null, (int)udpLocalPort.Value, 1, true, false);
                        }
                        else if (udpUnicast.Checked)
                        {
                            param.UdpSocket = new PortSettings.UdpSocketInfo(new IPAddress(udpIpUnicastRemoteAddress.GetAddressBytes()), (int)udpLocalPort.Value, (int)udpRemotePort.Value, false, false);
                        }
                        else if (udpBroadcast.Checked)
                        {
                            param.UdpSocket = new PortSettings.UdpSocketInfo((udpBroadcastIpCustom.Checked) ? new IPAddress(udpIpBroadcastRemoteAddress.GetAddressBytes()) : IPAddress.Broadcast, (int)udpLocalPort.Value, (int)udpRemotePort.Value, false, true);
                        }
                    }

                    if (tabs.SelectedTab == tabPageBluetooth)
                    {
                        param.Selected = PortSettings.SelectedSetup.BLUETOOTH;
                        param.BluetoothPort = new PortSettings.BluetoothPortInfo(selectedBluetoothDevice.DeviceAddress, tbBluetoothPin.Text);
                    }

                    if (tabs.SelectedTab == tabPageEmulator)
                    {
                        param.Selected = PortSettings.SelectedSetup.EMULATOR;
                        param.Emulator = new PortSettings.PortEmulatorInfo(rbEmulatorFromFile.Checked ? PortSettings.PortEmulatorInfo.DataSouceType.FILE : PortSettings.PortEmulatorInfo.DataSouceType.BYTE_ARRAY,
                            tbEmulatorFileName.Text,
                            (emulatorHexEditor.ByteProvider as DynamicByteProvider).Bytes.ToArray());
                    }
                    //NOTE: update the validation method when more controls or tabs are added
                }
            }
        }


        [DebuggerStepThrough]
        public string TabName()
        {
            return "Port properties";
        }

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(typeof(PortSettings), this.GetType(), 10);
        }

        public event EventHandler OnConfirm;

        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }

        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
        {
            if (validatingForSave == false)
            {
                return;
            }
            //serial port settings
            if (tabs.SelectedTab == tabPageSerial)
            {
                if (baudRate.SelectedItem == null)
                {
                    throw new Exception("Baud rate is not set");
                }
                if (portName.SelectedItem == null)
                {
                    throw new Exception("Port is not set");
                }
                if (dataBits.SelectedItem == null)
                {
                    throw new Exception("Data bits not set");
                }
                if (stopBits.SelectedItem == null)
                {
                    throw new Exception("Stop bits not set");
                }
                if (parity.SelectedItem == null)
                {
                    throw new Exception("Parity is not selected");
                }
                if (handshake.SelectedItem == null)
                {
                    throw new Exception("Handshake is not selected");
                }
            }

            //tcp-ip
            if (tabs.SelectedTab == tabPageTcpIpClient)
            {
                int result;
                if (int.TryParse(tcpClientPort.Text, out result) == false)
                {//TODO change to numericUpDown editor
                    throw new Exception("Invalid port number");
                }
            }

            if (tabs.SelectedTab == tabPageTcpIpServer)
            {
                if (tcpServerPort.Validate() == false)
                {
                    throw new Exception("Invalid port");
                }
                if (tcpServerConnectTimeout.Validate() == false)
                {
                    throw new Exception("Invalid timeout value");
                }
            }

            if (tabs.SelectedTab == tabPageSshClient)
            {
                //TODO
            }

            if (tabs.SelectedTab == tabPageUdpSocket)
            {
                //TODO: endpoint instead of IP address
                new IPAddress(udpIpUnicastRemoteAddress.GetAddressBytes());
                if (udpLocalPort.Validate() == false)
                {
                    throw new Exception("Invalid UDP local port");
                }
                if (udpRemotePort.Validate() == false)
                {
                    throw new Exception("Invalid UDP remote port");
                }
            }

            if (tabs.SelectedTab == tabPageBluetooth)
            {
                if (selectedBluetoothDevice == null)
                {
                    throw new Exception("Bluetooth device not selected");
                }
            }

            if (tabs.SelectedTab == tabPageEmulator)
            {
                if (rbEmulatorFromFile.Checked)
                {
                    if (File.Exists(tbEmulatorFileName.Text) == false)
                    {
                        throw new Exception("File does not exist");
                    }
                }
            }
        }

        #endregion

        private void serialPortListRefreshTimer_Tick(object sender, EventArgs e)
        {
            string[] updatedPorts = SerialPortExtension.OrderedPortNames(false);

            bool match = true;
            if (updatedPorts.Length != currentSerialPortNames.Length)
            {
                match = false;
            }
            foreach (var upd in updatedPorts)
            {
                if (currentSerialPortNames.Contains(upd) == false)
                {
                    match = false;
                    break;
                }
            }
            if (!match)
            {
                string current = portName.SelectedItem as string;
                portName.Items.Clear();
                currentSerialPortNames = updatedPorts;
                portName.Items.AddRange(currentSerialPortNames);
                portName.SelectedItem = current;
            }
        }

        BluetoothDeviceInfo selectedBluetoothDevice;

        private void btSelectBluetoothDevice_Click(object sender, EventArgs e)
        {
            SelectBluetoothDeviceDialog sbtdd = new SelectBluetoothDeviceDialog();
            sbtdd.ShowAuthenticated = true;
            sbtdd.ShowRemembered = true;
            //sbtdd.ShowUnknown = true;
            var dlgRes = sbtdd.ShowDialog();
            if (dlgRes == DialogResult.OK)
            {
                selectedBluetoothDevice = sbtdd.SelectedDevice;
                lbSelectedBluetoothDevice.Text = selectedBluetoothDevice.DeviceName;
            }
        }

        private void btAddBluetoothDevice_Click(object sender, EventArgs e)
        {
            SelectBluetoothDeviceDialog sbtdd = new SelectBluetoothDeviceDialog();
            sbtdd.ShowAuthenticated = true;
            sbtdd.ShowRemembered = true;
            sbtdd.ShowUnknown = true;
            sbtdd.AddNewDeviceWizard = true;
            if (sbtdd.ShowDialog() == DialogResult.OK)
            {
                selectedBluetoothDevice = sbtdd.SelectedDevice;
                lbSelectedBluetoothDevice.Text = selectedBluetoothDevice.DeviceName;
            }
        }

        private void udpConfiguredEndpoint_CheckedChanged(object sender, EventArgs e)
        {
            groupBox15.Enabled = udpUnicast.Checked;
            groupBox21.Enabled = udpBroadcast.Checked;
            groupBox20.Enabled = udpUnicast.Checked || udpBroadcast.Checked;
            groupRemote.Enabled = udpConfigureRemote.Checked;
        }

        private void udpBroadcastIpRadiobox_CheckedChanged(object sender, EventArgs e)
        {
            udpIpBroadcastRemoteAddress.Enabled = udpBroadcastIpCustom.Checked;
        }

        private void btBrowseForFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    tbEmulatorFileName.Text = dlg.FileName;
                }
            }
        }

        private void rbEmulatorFromFile_CheckedChanged(object sender, EventArgs e)
        {
            tbEmulatorFileName.Enabled = btBrowseForFile.Enabled = rbEmulatorFromFile.Checked;
            emulatorHexEditor.Enabled = rbEmulatorFromBytes.Checked;
        }

        private void portName_DoubleClick(object sender, EventArgs e)
        {
            OnConfirm(sender, e);
        }
        
        private void baudRate_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            OnConfirm(sender, e);
        }
    }
}
