﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.Windows.Forms;
using Be.Windows.Forms;
using yats.TestCase.Parameter;

namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    public partial class HexEditor : UserControl, IEditor
    {
        [DebuggerStepThrough]
        public HexEditor()
        {
            InitializeComponent();
        }

        #region IEditor Members

        public void ParameterToEditor(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ParameterToEditor(parameter); }));
            }
            else
            {
                SimpleStringValue paramString = parameter as SimpleStringValue;
                if (paramString != null)
                {
                    byte[] bytes = new byte[0];
                    if (string.IsNullOrEmpty(paramString.Value) == false)
                    {
                        bytes = Utilities.ByteArray.ToArray(paramString.Value);
                    }
                    DynamicByteProvider provider = new DynamicByteProvider(bytes);
                    hexBox.ByteProvider = provider;
                }
                ByteArrayValue paramByteArray = parameter as ByteArrayValue;
                if (paramByteArray != null)
                {
                    byte[] bytes = paramByteArray.Value ?? new byte[0];
                    hexBox.SetBytes(bytes);
                    DynamicByteProvider provider = new DynamicByteProvider(bytes);
                    hexBox.ByteProvider = provider;
                }
            }
        }

        public void EditorToParameter(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameter); }));
            }
            else
            {
                SimpleStringValue paramString = parameter as SimpleStringValue;
                if (paramString != null)
                {
                    paramString.Value = Utilities.ByteArray.ToString((hexBox.ByteProvider as DynamicByteProvider).Bytes.ToArray());
                }
                ByteArrayValue paramByteArray = parameter as ByteArrayValue;
                if (paramByteArray != null)
                {
                    paramByteArray.Value = (hexBox.ByteProvider as DynamicByteProvider).Bytes.ToArray();
                }              
            }
        }

        [DebuggerStepThrough]
        public string TabName()
        {
            return "Hex editor";
        }

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(typeof(SimpleStringValue), this.GetType(), 0);
            parameterTypes.RegisterEditorType(typeof(ByteArrayValue), this.GetType(), 300);
        }

        public event EventHandler OnConfirm;

        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }

        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
		{
		}

        #endregion

    }
}
