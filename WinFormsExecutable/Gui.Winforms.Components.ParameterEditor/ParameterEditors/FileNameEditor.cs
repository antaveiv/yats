﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.Windows.Forms;
using yats.TestCase.Parameter;
using yats.Utilities;

namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    public partial class FileNameEditor : UserControl, IEditor
    {
        public FileNameEditor()
        {
            InitializeComponent();
        }

        private void btBrowse_Click(object sender, EventArgs e)
        {            
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                tbFilePath.Text = openFileDialog.FileName;
            }
        }

        #region IEditor Members

        public void ParameterToEditor(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ParameterToEditor(parameter); }));
            }
            else
            {
                if (parameter is FilenameParameter)
                {
                    FilenameParameter param = parameter as FilenameParameter;
                    tbFilePath.Text = param.FileName;
                    openFileDialog.CheckFileExists = param.MustExist;
                }
                else if (parameter is ByteArrayFromFile)
                {
                    ByteArrayFromFile param = parameter as ByteArrayFromFile;
                    tbFilePath.Text = param.FileName;
                    openFileDialog.CheckFileExists = true;
                }
                else if (parameter is StringFromFile)
                {
                    StringFromFile param = parameter as StringFromFile;
                    tbFilePath.Text = param.FileName;
                    openFileDialog.CheckFileExists = true;
                }
            }
        }

        public void EditorToParameter(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameter); }));
            }
            else
            {
                if (parameter is FilenameParameter)
                {
                    FilenameParameter param = parameter as FilenameParameter;
                    param.FileName = tbFilePath.Text;
                }
                else if (parameter is ByteArrayFromFile)
                {
                    ByteArrayFromFile param = parameter as ByteArrayFromFile;
                    param.FileName = tbFilePath.Text;
                }
                else if (parameter is StringFromFile)
                {
                    StringFromFile param = parameter as StringFromFile;
                    param.FileName = tbFilePath.Text;
                }
            }
        }
        
        [DebuggerStepThrough]
        public string TabName()
        {
            return "File";
        }

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(typeof(FilenameParameter), this.GetType(), 10);
            parameterTypes.RegisterEditorType(typeof(ByteArrayFromFile), this.GetType(), 10);
            parameterTypes.RegisterEditorType(typeof(StringFromFile), this.GetType(), 10);
        }

        public event EventHandler OnConfirm;

        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }

        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
		{
            if (validatingForSave == false)
            {
                return;
            }
            string fileName = tbFilePath.Text;
            bool mustExist = true;
            if (parameter is FilenameParameter)
            {
                mustExist = (parameter as FilenameParameter).MustExist;
            }

			if (mustExist && (System.IO.File.Exists (fileName) == false)) {
				throw new Exception (string.Format("File {0} does not exist", fileName));
			}
            System.IO.Path.Combine(fileName, "");
            System.IO.Path.GetFileName(fileName);

            if (FileUtil.IsValidFilename(fileName) == false || FileUtil.FileNameContainsInvalidChars(fileName) || FileUtil.FilePathContainsInvalidChars(fileName))
            {
                throw new Exception("Path contains illegal characters");
            }
		}

        #endregion
    }
}
