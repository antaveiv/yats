﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using yats.TestCase.Parameter;

namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    public partial class RegexEditor : UserControl, IEditor
    {
        [DebuggerStepThrough]
        public RegexEditor()
        {
            InitializeComponent();
        }

        #region IEditor Members

        public void ParameterToEditor(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ParameterToEditor(parameter); }));
            }
            else
            {
                expression.Text = (parameter as RegexParameterValue).Value;
            }
        }

        public void EditorToParameter(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameter); }));
            }
            else
            {
                (parameter as RegexParameterValue).Value = expression.Text;
            }
        }


        [DebuggerStepThrough]
        public string TabName()
        {
            return "Regular expression";
        }

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(typeof(RegexParameterValue), this.GetType(), 10);
        }

        public event EventHandler OnConfirm;

        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }

        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
		{
			//TODO: test if an exception is thrown if the expression is invalid
			new Regex(expression.Text);
		}

        #endregion

        private void expression_TextChanged(object sender, EventArgs e)
        {
            try
            {
                matchTest.Text = "";
                var reg = new Regex(expression.Text);
                if (reg.IsMatch(testString.Text) == false)
                {
                    matchTest.Text = "no match";
                    return;
                }
                var match = reg.Match(testString.Text);
                foreach (var group in match.Groups)
                {
                    matchTest.Text = matchTest.Text + (group.ToString()) + Environment.NewLine;
                }
            }
            catch (Exception ex)
            {
                matchTest.Text = ex.Message;
            }
        }
    }
}
