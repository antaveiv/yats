﻿namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    partial class EndpointAddressEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbAddress = new yats.Gui.Winforms.Components.Util.TextBoxAdv();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.tbAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAddress.Location = new System.Drawing.Point(38, 6);
            this.tbAddress.Name = "textBox1";
            this.tbAddress.Size = new System.Drawing.Size(452, 20);
            this.tbAddress.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "URI:";
            // 
            // EndpointAddressEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbAddress);
            this.MinimumSize = new System.Drawing.Size(493, 35);
            this.Name = "EndpointAddressEditor";
            this.Size = new System.Drawing.Size(493, 35);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private yats.Gui.Winforms.Components.Util.TextBoxAdv tbAddress;
        private System.Windows.Forms.Label label1;
    }
}
