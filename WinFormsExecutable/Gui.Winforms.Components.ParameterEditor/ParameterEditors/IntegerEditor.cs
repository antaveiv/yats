﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.Windows.Forms;
using yats.TestCase.Parameter;

namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    public partial class IntegerEditor : UserControl, IEditor
    {
        [DebuggerStepThrough]
        public IntegerEditor()
        {
            InitializeComponent();
            valueUpDown.Minimum = decimal.MinValue;
            valueUpDown.Maximum = decimal.MaxValue;
        }

        #region IEditor Members

        public void ParameterToEditor(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ParameterToEditor(parameter); }));
            }
            else
            {
                if (parameter is IntegerParameterValue)
                {
                    valueUpDown.Value = (parameter as IntegerParameterValue).DecimalValue;
                    valueUpDown.Minimum = (parameter as IntegerParameterValue).Min;
                    valueUpDown.Maximum = (parameter as IntegerParameterValue).Max;
                }

                if (parameter is TimespanParameterValue)
                {
                    dimensionLabel.Text = "ms";
                    if ((parameter as TimespanParameterValue).Value.HasValue)
                    {
                        valueUpDown.Value = (decimal)Math.Round((parameter as TimespanParameterValue).Value.Value.TotalMilliseconds);
                    }
                    valueUpDown.Minimum = (decimal)-1000 * 60 * 60 * 24 * 366;
                    valueUpDown.Maximum = (decimal)1000 * 60 * 60 * 24 * 366;
                    valueUpDown.Increment = 1000;
                }
            }
        }

        public void EditorToParameter(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameter); }));
            }
            else
            {
                if (parameter is IntegerParameterValue)
                {
                    (parameter as IntegerParameterValue).DecimalValue = valueUpDown.Value;
                }
                if (parameter is TimespanParameterValue)
                {
                    int ms = (int)(valueUpDown.Value % 1000);
                    decimal tmp = valueUpDown.Value / 1000;
                    int seconds = (int)tmp % 60;
                    tmp = tmp / 60; //minutes
                    int minutes = (int)tmp % 60;
                    tmp = tmp / 60; //hours
                    int hours = (int)tmp % 24;
                    tmp = tmp / 24;
                    int days = (int)tmp;
                    (parameter as TimespanParameterValue).Value = new TimeSpan(days, hours, minutes, seconds, ms);
                }
            }
        }


        [DebuggerStepThrough]
        public string TabName()
        {
            return "Decimal";
        }

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(typeof(IntegerParameterValue), this.GetType(), 10);
            parameterTypes.RegisterEditorType(typeof(TimespanParameterValue), this.GetType(), 0);
        }

        public event EventHandler OnConfirm;

        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }

        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
		{
			if (valueUpDown.Validate() == false) {
				throw new Exception ("Invalid value");
			}
		}

        #endregion
    }
}
