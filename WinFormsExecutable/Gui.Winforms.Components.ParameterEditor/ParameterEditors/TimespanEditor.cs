﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using yats.TestCase.Parameter;
using yats.Utilities;

namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    public partial class TimespanEditor : UserControl, IEditor
    {
        [DebuggerStepThrough]
        public TimespanEditor()
        {
            InitializeComponent();
            trackBar.MaxValue = trackBarSteps.Count - 1;
        }

        bool canBeNegative = true;
        public bool CanBeNegative
        {
            get { return canBeNegative; }
            set
            {
                if (value == canBeNegative)
                {
                    return;
                }
                canBeNegative = value;
                groupBoxSign.Enabled = canBeNegative;
                if (canBeNegative == false)
                {
                    rbPositive.Checked = true;
                }
            }
        }

        #region IEditor Members

        public void ParameterToEditor(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ParameterToEditor(parameter); }));
            }
            else
            {
                TimespanParameterValue param = parameter as TimespanParameterValue;
                if (param != null)
                {
                    TimeSpan tmp;
                    if (param.Value.HasValue)
                    {
                        tmp = param.Value.Value;
                    }
                    else
                    {
                        tmp = TimeSpan.Zero;
                    }

                    numDays.ValueChanged -= editorUpDown_ValueChanged;
                    numHours.ValueChanged -= editorUpDown_ValueChanged;
                    numMin.ValueChanged -= editorUpDown_ValueChanged;
                    numSec.ValueChanged -= editorUpDown_ValueChanged;
                    numMs.ValueChanged -= editorUpDown_ValueChanged;
                    rbNegative.CheckedChanged -= editorUpDown_ValueChanged;
                    rbPositive.CheckedChanged -= editorUpDown_ValueChanged;

                    rbNegative.Checked = tmp.TotalMilliseconds < 0;
                    rbPositive.Checked = tmp.TotalMilliseconds >= 0;
                    if (tmp.TotalMilliseconds < 0)
                    {
                        tmp = TimeSpan.FromMilliseconds(tmp.TotalMilliseconds * -1);
                    }
                    
                    numDays.Value = tmp.Days;
                    numHours.Value = tmp.Hours;
                    numMin.Value = tmp.Minutes;
                    numSec.Value = tmp.Seconds;
                    numMs.Value = tmp.Milliseconds;

                    numDays.ValueChanged += editorUpDown_ValueChanged;
                    numHours.ValueChanged += editorUpDown_ValueChanged;
                    numMin.ValueChanged += editorUpDown_ValueChanged;
                    numSec.ValueChanged += editorUpDown_ValueChanged;
                    numMs.ValueChanged += editorUpDown_ValueChanged;
                    rbNegative.CheckedChanged += editorUpDown_ValueChanged;
                    rbPositive.CheckedChanged += editorUpDown_ValueChanged;

                    editorUpDown_ValueChanged(this, null);
                }
            }
        }

        public void EditorToParameter(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameter); }));
            }
            else
            {
                TimespanParameterValue param = parameter as TimespanParameterValue;
                if (param != null)
                {
                    param.Value = new TimeSpan((int)numDays.Value, (int)numHours.Value, (int)numMin.Value, (int)numSec.Value, (int)numMs.Value);
                    if (rbNegative.Checked)
                    {
                        param.Value = TimeSpan.FromMilliseconds(-1 * param.Value.Value.TotalMilliseconds);
                    }
                }
            }
        }

        [DebuggerStepThrough]
        public string TabName()
        {
            return "Timespan";
        }

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(typeof(TimespanParameterValue), this.GetType(), 10);
        }

        public event EventHandler OnConfirm;

        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }

        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
		{
		}

        #endregion

        static List<TimeSpan> trackBarSteps;
        static TimespanEditor()
        {
			//TODO: make configurable in Application settings. If configured, should override this initialization on application startup
            trackBarSteps = new List<TimeSpan>( );
            trackBarSteps.Add( TimeSpan.FromMilliseconds( 0 ) );
            trackBarSteps.Add( TimeSpan.FromMilliseconds( 100 ) );
            trackBarSteps.Add( TimeSpan.FromMilliseconds( 500 ) );
            trackBarSteps.Add( TimeSpan.FromSeconds( 1 ) );
            trackBarSteps.Add( TimeSpan.FromSeconds( 2 ) );
            trackBarSteps.Add( TimeSpan.FromSeconds( 5 ) );
            trackBarSteps.Add( TimeSpan.FromSeconds( 10 ) );
            trackBarSteps.Add( TimeSpan.FromSeconds( 30 ) );
            trackBarSteps.Add( TimeSpan.FromMinutes( 1 ) );
            trackBarSteps.Add( TimeSpan.FromMinutes( 5 ) );
            trackBarSteps.Add( TimeSpan.FromMinutes( 10 ) );
            trackBarSteps.Add( TimeSpan.FromMinutes( 30 ) );
            trackBarSteps.Add( TimeSpan.FromMinutes( 60 ) );
        }

        private void trackBar_ValueChanged(object sender, EventArgs e)
        {
            TimeSpan tmp = trackBarSteps[trackBar.Value];
            numDays.Value = tmp.Days;
            numHours.Value = tmp.Hours;
            numMin.Value = tmp.Minutes;
            numSec.Value = tmp.Seconds;
            numMs.Value = tmp.Milliseconds;
        }

        private void editorUpDown_ValueChanged(object sender, EventArgs e)
        {
            trackBar.ValueChanged -= trackBar_ValueChanged;

            var ts = new TimeSpan( (int)numDays.Value, (int)numHours.Value, (int)numMin.Value, (int)numSec.Value, (int)numMs.Value );

            TimeSpan minDiff = TimeSpan.MaxValue;
            int index = 0;
            for( int i = 0; i < trackBarSteps.Count; i++)
            {
                TimeSpan diff = ts.Subtract( trackBarSteps[i] );
                if(System.Math.Abs( diff.TotalMilliseconds ) < minDiff.TotalMilliseconds)
                {
                    minDiff = diff;
                    index = i;
                }
            }

            trackBar.Value = index;
            if(rbNegative.Checked)
            {
                ts = TimeSpan.FromMilliseconds( -1 * ts.TotalMilliseconds );
            }
            valueLabel.Text = ts.ToFriendlyDisplay( 2 );
            trackBar.ValueChanged += trackBar_ValueChanged;
        }
    }
}
