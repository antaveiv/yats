﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Forms;
using yats.TestCase.Parameter;

namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    public partial class FloatEditor : UserControl, IEditor
    {
        public FloatEditor()
        {
            InitializeComponent();
        }

        #region IEditor Members

        public void ParameterToEditor(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ParameterToEditor(parameter); }));
            }
            else
            {
                DoubleParameter paramDouble = parameter as DoubleParameter;
                FloatParameter paramFloat = parameter as FloatParameter;
                if (paramDouble != null)
                {
                    textBox.Text = paramDouble.Value.ToString(CultureInfo.InvariantCulture);
                }

                if (paramFloat != null)
                {
                    textBox.Text = paramFloat.Value.ToString(CultureInfo.InvariantCulture);
                }
            }
        }

        public void EditorToParameter(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameter); }));
            }
            else
            {
                FilenameParameter param = parameter as FilenameParameter;
                DoubleParameter paramDouble = parameter as DoubleParameter;
                FloatParameter paramFloat = parameter as FloatParameter;
                if (paramDouble != null)
                {
                    paramDouble.Value = Double.Parse(textBox.Text, CultureInfo.InvariantCulture);
                }

                if (paramFloat != null)
                {
                    paramFloat.Value = float.Parse(textBox.Text, CultureInfo.InvariantCulture);
                }
            }
        }


        [DebuggerStepThrough]
        public string TabName()
        {
            return "floating point";
        }

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(typeof(FloatParameter), this.GetType(), 10);
            parameterTypes.RegisterEditorType(typeof(DoubleParameter), this.GetType(), 10);
        }

        public event EventHandler OnConfirm;

        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }

        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
		{
            if (parameter is FloatParameter)
            {
                float.Parse(textBox.Text);
            }
            else if (parameter is DoubleParameter)
            {
                double.Parse(textBox.Text);
            }
		}

        #endregion
    }
}
