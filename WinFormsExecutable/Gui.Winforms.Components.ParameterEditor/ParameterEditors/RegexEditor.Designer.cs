﻿namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    partial class RegexEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.expression = new yats.Gui.Winforms.Components.Util.TextBoxAdv();
            this.matchText = new System.Windows.Forms.GroupBox();
            this.matchTest = new yats.Gui.Winforms.Components.Util.TextBoxAdv();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.testString = new yats.Gui.Winforms.Components.Util.TextBoxAdv();
            this.groupBox1.SuspendLayout();
            this.matchText.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.expression);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(504, 47);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Regular expression";
            // 
            // expression
            // 
            this.expression.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.expression.Location = new System.Drawing.Point(6, 19);
            this.expression.Name = "expression";
            this.expression.Size = new System.Drawing.Size(492, 20);
            this.expression.TabIndex = 0;
            this.expression.TextChanged += new System.EventHandler(this.expression_TextChanged);
            // 
            // matchText
            // 
            this.matchText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.matchText.Controls.Add(this.matchTest);
            this.matchText.Location = new System.Drawing.Point(3, 109);
            this.matchText.Name = "matchText";
            this.matchText.Size = new System.Drawing.Size(504, 143);
            this.matchText.TabIndex = 1;
            this.matchText.TabStop = false;
            this.matchText.Text = "Match test";
            // 
            // matchTest
            // 
            this.matchTest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.matchTest.Location = new System.Drawing.Point(6, 19);
            this.matchTest.Multiline = true;
            this.matchTest.Name = "matchTest";
            this.matchTest.ReadOnly = true;
            this.matchTest.Size = new System.Drawing.Size(492, 118);
            this.matchTest.TabIndex = 0;
            this.matchTest.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.testString);
            this.groupBox3.Location = new System.Drawing.Point(3, 56);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(504, 47);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Test string";
            // 
            // testString
            // 
            this.testString.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.testString.Location = new System.Drawing.Point(6, 19);
            this.testString.Name = "testString";
            this.testString.Size = new System.Drawing.Size(492, 20);
            this.testString.TabIndex = 0;
            this.testString.TextChanged += new System.EventHandler(this.expression_TextChanged);
            // 
            // RegexEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.matchText);
            this.Controls.Add(this.groupBox1);
            this.MinimumSize = new System.Drawing.Size(510, 255);
            this.Name = "RegexEditor";
            this.Size = new System.Drawing.Size(510, 255);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.matchText.ResumeLayout(false);
            this.matchText.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private yats.Gui.Winforms.Components.Util.TextBoxAdv expression;
        private System.Windows.Forms.GroupBox matchText;
        private yats.Gui.Winforms.Components.Util.TextBoxAdv matchTest;
        private System.Windows.Forms.GroupBox groupBox3;
        private yats.Gui.Winforms.Components.Util.TextBoxAdv testString;
    }
}
