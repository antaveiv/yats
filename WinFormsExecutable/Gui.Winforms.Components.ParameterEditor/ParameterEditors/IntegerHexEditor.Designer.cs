﻿namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    partial class IntegerHexEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbDecimal = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbHex = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lbBin63 = new System.Windows.Forms.Label();
            this.lbBin62 = new System.Windows.Forms.Label();
            this.lbBin61 = new System.Windows.Forms.Label();
            this.lbBin60 = new System.Windows.Forms.Label();
            this.lbBin47 = new System.Windows.Forms.Label();
            this.lbBin46 = new System.Windows.Forms.Label();
            this.lbBin45 = new System.Windows.Forms.Label();
            this.lbBin44 = new System.Windows.Forms.Label();
            this.lbBin15 = new System.Windows.Forms.Label();
            this.lbBin14 = new System.Windows.Forms.Label();
            this.lbBin13 = new System.Windows.Forms.Label();
            this.lbBin12 = new System.Windows.Forms.Label();
            this.lbBin31 = new System.Windows.Forms.Label();
            this.lbBin30 = new System.Windows.Forms.Label();
            this.lbBin29 = new System.Windows.Forms.Label();
            this.lbBin28 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lbBin59 = new System.Windows.Forms.Label();
            this.lbBin58 = new System.Windows.Forms.Label();
            this.lbBin43 = new System.Windows.Forms.Label();
            this.lbBin42 = new System.Windows.Forms.Label();
            this.lbBin11 = new System.Windows.Forms.Label();
            this.lbBin57 = new System.Windows.Forms.Label();
            this.lbBin27 = new System.Windows.Forms.Label();
            this.lbBin10 = new System.Windows.Forms.Label();
            this.lbBin26 = new System.Windows.Forms.Label();
            this.lbBin41 = new System.Windows.Forms.Label();
            this.lbBin09 = new System.Windows.Forms.Label();
            this.lbBin25 = new System.Windows.Forms.Label();
            this.lbBin56 = new System.Windows.Forms.Label();
            this.lbBin40 = new System.Windows.Forms.Label();
            this.lbBin08 = new System.Windows.Forms.Label();
            this.lbBin24 = new System.Windows.Forms.Label();
            this.lbBin55 = new System.Windows.Forms.Label();
            this.lbBin54 = new System.Windows.Forms.Label();
            this.lbBin39 = new System.Windows.Forms.Label();
            this.lbBin38 = new System.Windows.Forms.Label();
            this.lbBin07 = new System.Windows.Forms.Label();
            this.lbBin53 = new System.Windows.Forms.Label();
            this.lbBin23 = new System.Windows.Forms.Label();
            this.lbBin06 = new System.Windows.Forms.Label();
            this.lbBin22 = new System.Windows.Forms.Label();
            this.lbBin37 = new System.Windows.Forms.Label();
            this.lbBin05 = new System.Windows.Forms.Label();
            this.lbBin21 = new System.Windows.Forms.Label();
            this.lbBin52 = new System.Windows.Forms.Label();
            this.lbBin36 = new System.Windows.Forms.Label();
            this.lbBin04 = new System.Windows.Forms.Label();
            this.lbBin20 = new System.Windows.Forms.Label();
            this.lbBin51 = new System.Windows.Forms.Label();
            this.lbBin50 = new System.Windows.Forms.Label();
            this.lbBin35 = new System.Windows.Forms.Label();
            this.lbBin34 = new System.Windows.Forms.Label();
            this.lbBin03 = new System.Windows.Forms.Label();
            this.lbBin49 = new System.Windows.Forms.Label();
            this.lbBin19 = new System.Windows.Forms.Label();
            this.lbBin02 = new System.Windows.Forms.Label();
            this.lbBin18 = new System.Windows.Forms.Label();
            this.lbBin33 = new System.Windows.Forms.Label();
            this.lbBin01 = new System.Windows.Forms.Label();
            this.lbBin17 = new System.Windows.Forms.Label();
            this.lbBin48 = new System.Windows.Forms.Label();
            this.lbBin32 = new System.Windows.Forms.Label();
            this.lbBin00 = new System.Windows.Forms.Label();
            this.lbBin16 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbMin = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbMax = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbDecimal
            // 
            this.tbDecimal.Location = new System.Drawing.Point(83, 13);
            this.tbDecimal.Name = "tbDecimal";
            this.tbDecimal.Size = new System.Drawing.Size(241, 20);
            this.tbDecimal.TabIndex = 0;
            this.tbDecimal.Text = "0";
            this.tbDecimal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Decimal";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Hexadecimal";
            // 
            // tbHex
            // 
            this.tbHex.Location = new System.Drawing.Point(83, 39);
            this.tbHex.Name = "tbHex";
            this.tbHex.Size = new System.Drawing.Size(241, 20);
            this.tbHex.TabIndex = 0;
            this.tbHex.Text = "0000000000000000";
            this.tbHex.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Binary";
            // 
            // lbBin63
            // 
            this.lbBin63.AutoSize = true;
            this.lbBin63.Location = new System.Drawing.Point(81, 70);
            this.lbBin63.Name = "lbBin63";
            this.lbBin63.Size = new System.Drawing.Size(13, 13);
            this.lbBin63.TabIndex = 2;
            this.lbBin63.Text = "0";
            // 
            // lbBin62
            // 
            this.lbBin62.AutoSize = true;
            this.lbBin62.Location = new System.Drawing.Point(93, 70);
            this.lbBin62.Name = "lbBin62";
            this.lbBin62.Size = new System.Drawing.Size(13, 13);
            this.lbBin62.TabIndex = 2;
            this.lbBin62.Text = "0";
            // 
            // lbBin61
            // 
            this.lbBin61.AutoSize = true;
            this.lbBin61.Location = new System.Drawing.Point(106, 70);
            this.lbBin61.Name = "lbBin61";
            this.lbBin61.Size = new System.Drawing.Size(13, 13);
            this.lbBin61.TabIndex = 2;
            this.lbBin61.Text = "0";
            // 
            // lbBin60
            // 
            this.lbBin60.AutoSize = true;
            this.lbBin60.Location = new System.Drawing.Point(118, 70);
            this.lbBin60.Name = "lbBin60";
            this.lbBin60.Size = new System.Drawing.Size(13, 13);
            this.lbBin60.TabIndex = 2;
            this.lbBin60.Text = "0";
            // 
            // lbBin47
            // 
            this.lbBin47.AutoSize = true;
            this.lbBin47.Location = new System.Drawing.Point(81, 103);
            this.lbBin47.Name = "lbBin47";
            this.lbBin47.Size = new System.Drawing.Size(13, 13);
            this.lbBin47.TabIndex = 2;
            this.lbBin47.Text = "0";
            // 
            // lbBin46
            // 
            this.lbBin46.AutoSize = true;
            this.lbBin46.Location = new System.Drawing.Point(93, 103);
            this.lbBin46.Name = "lbBin46";
            this.lbBin46.Size = new System.Drawing.Size(13, 13);
            this.lbBin46.TabIndex = 2;
            this.lbBin46.Text = "0";
            // 
            // lbBin45
            // 
            this.lbBin45.AutoSize = true;
            this.lbBin45.Location = new System.Drawing.Point(106, 103);
            this.lbBin45.Name = "lbBin45";
            this.lbBin45.Size = new System.Drawing.Size(13, 13);
            this.lbBin45.TabIndex = 2;
            this.lbBin45.Text = "0";
            // 
            // lbBin44
            // 
            this.lbBin44.AutoSize = true;
            this.lbBin44.Location = new System.Drawing.Point(118, 103);
            this.lbBin44.Name = "lbBin44";
            this.lbBin44.Size = new System.Drawing.Size(13, 13);
            this.lbBin44.TabIndex = 2;
            this.lbBin44.Text = "0";
            // 
            // lbBin15
            // 
            this.lbBin15.AutoSize = true;
            this.lbBin15.Location = new System.Drawing.Point(81, 169);
            this.lbBin15.Name = "lbBin15";
            this.lbBin15.Size = new System.Drawing.Size(13, 13);
            this.lbBin15.TabIndex = 2;
            this.lbBin15.Text = "0";
            // 
            // lbBin14
            // 
            this.lbBin14.AutoSize = true;
            this.lbBin14.Location = new System.Drawing.Point(93, 169);
            this.lbBin14.Name = "lbBin14";
            this.lbBin14.Size = new System.Drawing.Size(13, 13);
            this.lbBin14.TabIndex = 2;
            this.lbBin14.Text = "0";
            // 
            // lbBin13
            // 
            this.lbBin13.AutoSize = true;
            this.lbBin13.Location = new System.Drawing.Point(106, 169);
            this.lbBin13.Name = "lbBin13";
            this.lbBin13.Size = new System.Drawing.Size(13, 13);
            this.lbBin13.TabIndex = 2;
            this.lbBin13.Text = "0";
            // 
            // lbBin12
            // 
            this.lbBin12.AutoSize = true;
            this.lbBin12.Location = new System.Drawing.Point(118, 169);
            this.lbBin12.Name = "lbBin12";
            this.lbBin12.Size = new System.Drawing.Size(13, 13);
            this.lbBin12.TabIndex = 2;
            this.lbBin12.Text = "0";
            // 
            // lbBin31
            // 
            this.lbBin31.AutoSize = true;
            this.lbBin31.Location = new System.Drawing.Point(81, 136);
            this.lbBin31.Name = "lbBin31";
            this.lbBin31.Size = new System.Drawing.Size(13, 13);
            this.lbBin31.TabIndex = 2;
            this.lbBin31.Text = "0";
            // 
            // lbBin30
            // 
            this.lbBin30.AutoSize = true;
            this.lbBin30.Location = new System.Drawing.Point(93, 136);
            this.lbBin30.Name = "lbBin30";
            this.lbBin30.Size = new System.Drawing.Size(13, 13);
            this.lbBin30.TabIndex = 2;
            this.lbBin30.Text = "0";
            // 
            // lbBin29
            // 
            this.lbBin29.AutoSize = true;
            this.lbBin29.Location = new System.Drawing.Point(106, 136);
            this.lbBin29.Name = "lbBin29";
            this.lbBin29.Size = new System.Drawing.Size(13, 13);
            this.lbBin29.TabIndex = 2;
            this.lbBin29.Text = "0";
            // 
            // lbBin28
            // 
            this.lbBin28.AutoSize = true;
            this.lbBin28.Location = new System.Drawing.Point(118, 136);
            this.lbBin28.Name = "lbBin28";
            this.lbBin28.Size = new System.Drawing.Size(13, 13);
            this.lbBin28.TabIndex = 2;
            this.lbBin28.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label19.Location = new System.Drawing.Point(81, 182);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 13);
            this.label19.TabIndex = 3;
            this.label19.Text = "15";
            // 
            // lbBin59
            // 
            this.lbBin59.AutoSize = true;
            this.lbBin59.Location = new System.Drawing.Point(146, 70);
            this.lbBin59.Name = "lbBin59";
            this.lbBin59.Size = new System.Drawing.Size(13, 13);
            this.lbBin59.TabIndex = 2;
            this.lbBin59.Text = "0";
            // 
            // lbBin58
            // 
            this.lbBin58.AutoSize = true;
            this.lbBin58.Location = new System.Drawing.Point(158, 70);
            this.lbBin58.Name = "lbBin58";
            this.lbBin58.Size = new System.Drawing.Size(13, 13);
            this.lbBin58.TabIndex = 2;
            this.lbBin58.Text = "0";
            // 
            // lbBin43
            // 
            this.lbBin43.AutoSize = true;
            this.lbBin43.Location = new System.Drawing.Point(146, 103);
            this.lbBin43.Name = "lbBin43";
            this.lbBin43.Size = new System.Drawing.Size(13, 13);
            this.lbBin43.TabIndex = 2;
            this.lbBin43.Text = "0";
            // 
            // lbBin42
            // 
            this.lbBin42.AutoSize = true;
            this.lbBin42.Location = new System.Drawing.Point(158, 103);
            this.lbBin42.Name = "lbBin42";
            this.lbBin42.Size = new System.Drawing.Size(13, 13);
            this.lbBin42.TabIndex = 2;
            this.lbBin42.Text = "0";
            // 
            // lbBin11
            // 
            this.lbBin11.AutoSize = true;
            this.lbBin11.Location = new System.Drawing.Point(146, 169);
            this.lbBin11.Name = "lbBin11";
            this.lbBin11.Size = new System.Drawing.Size(13, 13);
            this.lbBin11.TabIndex = 2;
            this.lbBin11.Text = "0";
            // 
            // lbBin57
            // 
            this.lbBin57.AutoSize = true;
            this.lbBin57.Location = new System.Drawing.Point(171, 70);
            this.lbBin57.Name = "lbBin57";
            this.lbBin57.Size = new System.Drawing.Size(13, 13);
            this.lbBin57.TabIndex = 2;
            this.lbBin57.Text = "0";
            // 
            // lbBin27
            // 
            this.lbBin27.AutoSize = true;
            this.lbBin27.Location = new System.Drawing.Point(146, 136);
            this.lbBin27.Name = "lbBin27";
            this.lbBin27.Size = new System.Drawing.Size(13, 13);
            this.lbBin27.TabIndex = 2;
            this.lbBin27.Text = "0";
            // 
            // lbBin10
            // 
            this.lbBin10.AutoSize = true;
            this.lbBin10.Location = new System.Drawing.Point(158, 169);
            this.lbBin10.Name = "lbBin10";
            this.lbBin10.Size = new System.Drawing.Size(13, 13);
            this.lbBin10.TabIndex = 2;
            this.lbBin10.Text = "0";
            // 
            // lbBin26
            // 
            this.lbBin26.AutoSize = true;
            this.lbBin26.Location = new System.Drawing.Point(158, 136);
            this.lbBin26.Name = "lbBin26";
            this.lbBin26.Size = new System.Drawing.Size(13, 13);
            this.lbBin26.TabIndex = 2;
            this.lbBin26.Text = "0";
            // 
            // lbBin41
            // 
            this.lbBin41.AutoSize = true;
            this.lbBin41.Location = new System.Drawing.Point(170, 103);
            this.lbBin41.Name = "lbBin41";
            this.lbBin41.Size = new System.Drawing.Size(13, 13);
            this.lbBin41.TabIndex = 2;
            this.lbBin41.Text = "0";
            // 
            // lbBin09
            // 
            this.lbBin09.AutoSize = true;
            this.lbBin09.Location = new System.Drawing.Point(170, 169);
            this.lbBin09.Name = "lbBin09";
            this.lbBin09.Size = new System.Drawing.Size(13, 13);
            this.lbBin09.TabIndex = 2;
            this.lbBin09.Text = "0";
            // 
            // lbBin25
            // 
            this.lbBin25.AutoSize = true;
            this.lbBin25.Location = new System.Drawing.Point(170, 136);
            this.lbBin25.Name = "lbBin25";
            this.lbBin25.Size = new System.Drawing.Size(13, 13);
            this.lbBin25.TabIndex = 2;
            this.lbBin25.Text = "0";
            // 
            // lbBin56
            // 
            this.lbBin56.AutoSize = true;
            this.lbBin56.Location = new System.Drawing.Point(183, 70);
            this.lbBin56.Name = "lbBin56";
            this.lbBin56.Size = new System.Drawing.Size(13, 13);
            this.lbBin56.TabIndex = 2;
            this.lbBin56.Text = "0";
            // 
            // lbBin40
            // 
            this.lbBin40.AutoSize = true;
            this.lbBin40.Location = new System.Drawing.Point(183, 103);
            this.lbBin40.Name = "lbBin40";
            this.lbBin40.Size = new System.Drawing.Size(13, 13);
            this.lbBin40.TabIndex = 2;
            this.lbBin40.Text = "0";
            // 
            // lbBin08
            // 
            this.lbBin08.AutoSize = true;
            this.lbBin08.Location = new System.Drawing.Point(183, 169);
            this.lbBin08.Name = "lbBin08";
            this.lbBin08.Size = new System.Drawing.Size(13, 13);
            this.lbBin08.TabIndex = 2;
            this.lbBin08.Text = "0";
            // 
            // lbBin24
            // 
            this.lbBin24.AutoSize = true;
            this.lbBin24.Location = new System.Drawing.Point(183, 136);
            this.lbBin24.Name = "lbBin24";
            this.lbBin24.Size = new System.Drawing.Size(13, 13);
            this.lbBin24.TabIndex = 2;
            this.lbBin24.Text = "0";
            // 
            // lbBin55
            // 
            this.lbBin55.AutoSize = true;
            this.lbBin55.Location = new System.Drawing.Point(211, 70);
            this.lbBin55.Name = "lbBin55";
            this.lbBin55.Size = new System.Drawing.Size(13, 13);
            this.lbBin55.TabIndex = 2;
            this.lbBin55.Text = "0";
            // 
            // lbBin54
            // 
            this.lbBin54.AutoSize = true;
            this.lbBin54.Location = new System.Drawing.Point(223, 70);
            this.lbBin54.Name = "lbBin54";
            this.lbBin54.Size = new System.Drawing.Size(13, 13);
            this.lbBin54.TabIndex = 2;
            this.lbBin54.Text = "0";
            // 
            // lbBin39
            // 
            this.lbBin39.AutoSize = true;
            this.lbBin39.Location = new System.Drawing.Point(211, 103);
            this.lbBin39.Name = "lbBin39";
            this.lbBin39.Size = new System.Drawing.Size(13, 13);
            this.lbBin39.TabIndex = 2;
            this.lbBin39.Text = "0";
            // 
            // lbBin38
            // 
            this.lbBin38.AutoSize = true;
            this.lbBin38.Location = new System.Drawing.Point(223, 103);
            this.lbBin38.Name = "lbBin38";
            this.lbBin38.Size = new System.Drawing.Size(13, 13);
            this.lbBin38.TabIndex = 2;
            this.lbBin38.Text = "0";
            // 
            // lbBin07
            // 
            this.lbBin07.AutoSize = true;
            this.lbBin07.Location = new System.Drawing.Point(211, 169);
            this.lbBin07.Name = "lbBin07";
            this.lbBin07.Size = new System.Drawing.Size(13, 13);
            this.lbBin07.TabIndex = 2;
            this.lbBin07.Text = "0";
            // 
            // lbBin53
            // 
            this.lbBin53.AutoSize = true;
            this.lbBin53.Location = new System.Drawing.Point(235, 70);
            this.lbBin53.Name = "lbBin53";
            this.lbBin53.Size = new System.Drawing.Size(13, 13);
            this.lbBin53.TabIndex = 2;
            this.lbBin53.Text = "0";
            // 
            // lbBin23
            // 
            this.lbBin23.AutoSize = true;
            this.lbBin23.Location = new System.Drawing.Point(211, 136);
            this.lbBin23.Name = "lbBin23";
            this.lbBin23.Size = new System.Drawing.Size(13, 13);
            this.lbBin23.TabIndex = 2;
            this.lbBin23.Text = "0";
            // 
            // lbBin06
            // 
            this.lbBin06.AutoSize = true;
            this.lbBin06.Location = new System.Drawing.Point(223, 169);
            this.lbBin06.Name = "lbBin06";
            this.lbBin06.Size = new System.Drawing.Size(13, 13);
            this.lbBin06.TabIndex = 2;
            this.lbBin06.Text = "0";
            // 
            // lbBin22
            // 
            this.lbBin22.AutoSize = true;
            this.lbBin22.Location = new System.Drawing.Point(223, 136);
            this.lbBin22.Name = "lbBin22";
            this.lbBin22.Size = new System.Drawing.Size(13, 13);
            this.lbBin22.TabIndex = 2;
            this.lbBin22.Text = "0";
            // 
            // lbBin37
            // 
            this.lbBin37.AutoSize = true;
            this.lbBin37.Location = new System.Drawing.Point(235, 103);
            this.lbBin37.Name = "lbBin37";
            this.lbBin37.Size = new System.Drawing.Size(13, 13);
            this.lbBin37.TabIndex = 2;
            this.lbBin37.Text = "0";
            // 
            // lbBin05
            // 
            this.lbBin05.AutoSize = true;
            this.lbBin05.Location = new System.Drawing.Point(235, 169);
            this.lbBin05.Name = "lbBin05";
            this.lbBin05.Size = new System.Drawing.Size(13, 13);
            this.lbBin05.TabIndex = 2;
            this.lbBin05.Text = "0";
            // 
            // lbBin21
            // 
            this.lbBin21.AutoSize = true;
            this.lbBin21.Location = new System.Drawing.Point(235, 136);
            this.lbBin21.Name = "lbBin21";
            this.lbBin21.Size = new System.Drawing.Size(13, 13);
            this.lbBin21.TabIndex = 2;
            this.lbBin21.Text = "0";
            // 
            // lbBin52
            // 
            this.lbBin52.AutoSize = true;
            this.lbBin52.Location = new System.Drawing.Point(247, 70);
            this.lbBin52.Name = "lbBin52";
            this.lbBin52.Size = new System.Drawing.Size(13, 13);
            this.lbBin52.TabIndex = 2;
            this.lbBin52.Text = "0";
            // 
            // lbBin36
            // 
            this.lbBin36.AutoSize = true;
            this.lbBin36.Location = new System.Drawing.Point(247, 103);
            this.lbBin36.Name = "lbBin36";
            this.lbBin36.Size = new System.Drawing.Size(13, 13);
            this.lbBin36.TabIndex = 2;
            this.lbBin36.Text = "0";
            // 
            // lbBin04
            // 
            this.lbBin04.AutoSize = true;
            this.lbBin04.Location = new System.Drawing.Point(247, 169);
            this.lbBin04.Name = "lbBin04";
            this.lbBin04.Size = new System.Drawing.Size(13, 13);
            this.lbBin04.TabIndex = 2;
            this.lbBin04.Text = "0";
            // 
            // lbBin20
            // 
            this.lbBin20.AutoSize = true;
            this.lbBin20.Location = new System.Drawing.Point(247, 136);
            this.lbBin20.Name = "lbBin20";
            this.lbBin20.Size = new System.Drawing.Size(13, 13);
            this.lbBin20.TabIndex = 2;
            this.lbBin20.Text = "0";
            // 
            // lbBin51
            // 
            this.lbBin51.AutoSize = true;
            this.lbBin51.Location = new System.Drawing.Point(275, 70);
            this.lbBin51.Name = "lbBin51";
            this.lbBin51.Size = new System.Drawing.Size(13, 13);
            this.lbBin51.TabIndex = 2;
            this.lbBin51.Text = "0";
            // 
            // lbBin50
            // 
            this.lbBin50.AutoSize = true;
            this.lbBin50.Location = new System.Drawing.Point(287, 70);
            this.lbBin50.Name = "lbBin50";
            this.lbBin50.Size = new System.Drawing.Size(13, 13);
            this.lbBin50.TabIndex = 2;
            this.lbBin50.Text = "0";
            // 
            // lbBin35
            // 
            this.lbBin35.AutoSize = true;
            this.lbBin35.Location = new System.Drawing.Point(275, 103);
            this.lbBin35.Name = "lbBin35";
            this.lbBin35.Size = new System.Drawing.Size(13, 13);
            this.lbBin35.TabIndex = 2;
            this.lbBin35.Text = "0";
            // 
            // lbBin34
            // 
            this.lbBin34.AutoSize = true;
            this.lbBin34.Location = new System.Drawing.Point(287, 103);
            this.lbBin34.Name = "lbBin34";
            this.lbBin34.Size = new System.Drawing.Size(13, 13);
            this.lbBin34.TabIndex = 2;
            this.lbBin34.Text = "0";
            // 
            // lbBin03
            // 
            this.lbBin03.AutoSize = true;
            this.lbBin03.Location = new System.Drawing.Point(275, 169);
            this.lbBin03.Name = "lbBin03";
            this.lbBin03.Size = new System.Drawing.Size(13, 13);
            this.lbBin03.TabIndex = 2;
            this.lbBin03.Text = "0";
            // 
            // lbBin49
            // 
            this.lbBin49.AutoSize = true;
            this.lbBin49.Location = new System.Drawing.Point(299, 70);
            this.lbBin49.Name = "lbBin49";
            this.lbBin49.Size = new System.Drawing.Size(13, 13);
            this.lbBin49.TabIndex = 2;
            this.lbBin49.Text = "0";
            // 
            // lbBin19
            // 
            this.lbBin19.AutoSize = true;
            this.lbBin19.Location = new System.Drawing.Point(275, 136);
            this.lbBin19.Name = "lbBin19";
            this.lbBin19.Size = new System.Drawing.Size(13, 13);
            this.lbBin19.TabIndex = 2;
            this.lbBin19.Text = "0";
            // 
            // lbBin02
            // 
            this.lbBin02.AutoSize = true;
            this.lbBin02.Location = new System.Drawing.Point(287, 169);
            this.lbBin02.Name = "lbBin02";
            this.lbBin02.Size = new System.Drawing.Size(13, 13);
            this.lbBin02.TabIndex = 2;
            this.lbBin02.Text = "0";
            // 
            // lbBin18
            // 
            this.lbBin18.AutoSize = true;
            this.lbBin18.Location = new System.Drawing.Point(287, 136);
            this.lbBin18.Name = "lbBin18";
            this.lbBin18.Size = new System.Drawing.Size(13, 13);
            this.lbBin18.TabIndex = 2;
            this.lbBin18.Text = "0";
            // 
            // lbBin33
            // 
            this.lbBin33.AutoSize = true;
            this.lbBin33.Location = new System.Drawing.Point(299, 103);
            this.lbBin33.Name = "lbBin33";
            this.lbBin33.Size = new System.Drawing.Size(13, 13);
            this.lbBin33.TabIndex = 2;
            this.lbBin33.Text = "0";
            // 
            // lbBin01
            // 
            this.lbBin01.AutoSize = true;
            this.lbBin01.Location = new System.Drawing.Point(299, 169);
            this.lbBin01.Name = "lbBin01";
            this.lbBin01.Size = new System.Drawing.Size(13, 13);
            this.lbBin01.TabIndex = 2;
            this.lbBin01.Text = "0";
            // 
            // lbBin17
            // 
            this.lbBin17.AutoSize = true;
            this.lbBin17.Location = new System.Drawing.Point(299, 136);
            this.lbBin17.Name = "lbBin17";
            this.lbBin17.Size = new System.Drawing.Size(13, 13);
            this.lbBin17.TabIndex = 2;
            this.lbBin17.Text = "0";
            // 
            // lbBin48
            // 
            this.lbBin48.AutoSize = true;
            this.lbBin48.Location = new System.Drawing.Point(311, 70);
            this.lbBin48.Name = "lbBin48";
            this.lbBin48.Size = new System.Drawing.Size(13, 13);
            this.lbBin48.TabIndex = 2;
            this.lbBin48.Text = "0";
            // 
            // lbBin32
            // 
            this.lbBin32.AutoSize = true;
            this.lbBin32.Location = new System.Drawing.Point(311, 103);
            this.lbBin32.Name = "lbBin32";
            this.lbBin32.Size = new System.Drawing.Size(13, 13);
            this.lbBin32.TabIndex = 2;
            this.lbBin32.Text = "0";
            // 
            // lbBin00
            // 
            this.lbBin00.AutoSize = true;
            this.lbBin00.Location = new System.Drawing.Point(311, 169);
            this.lbBin00.Name = "lbBin00";
            this.lbBin00.Size = new System.Drawing.Size(13, 13);
            this.lbBin00.TabIndex = 2;
            this.lbBin00.Text = "0";
            // 
            // lbBin16
            // 
            this.lbBin16.AutoSize = true;
            this.lbBin16.Location = new System.Drawing.Point(311, 136);
            this.lbBin16.Name = "lbBin16";
            this.lbBin16.Size = new System.Drawing.Size(13, 13);
            this.lbBin16.TabIndex = 2;
            this.lbBin16.Text = "0";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label68.Location = new System.Drawing.Point(311, 182);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(13, 13);
            this.label68.TabIndex = 3;
            this.label68.Text = "0";
            this.label68.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label69.Location = new System.Drawing.Point(81, 149);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(19, 13);
            this.label69.TabIndex = 3;
            this.label69.Text = "31";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label70.Location = new System.Drawing.Point(305, 149);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(19, 13);
            this.label70.TabIndex = 3;
            this.label70.Text = "16";
            this.label70.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label71.Location = new System.Drawing.Point(80, 116);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(19, 13);
            this.label71.TabIndex = 3;
            this.label71.Text = "47";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label72.Location = new System.Drawing.Point(305, 116);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(19, 13);
            this.label72.TabIndex = 3;
            this.label72.Text = "32";
            this.label72.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label73.Location = new System.Drawing.Point(80, 83);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(19, 13);
            this.label73.TabIndex = 3;
            this.label73.Text = "63";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label74.Location = new System.Drawing.Point(305, 83);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(19, 13);
            this.label74.TabIndex = 3;
            this.label74.Text = "48";
            this.label74.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Min";
            // 
            // lbMin
            // 
            this.lbMin.AutoSize = true;
            this.lbMin.Location = new System.Drawing.Point(35, 16);
            this.lbMin.Name = "lbMin";
            this.lbMin.Size = new System.Drawing.Size(35, 13);
            this.lbMin.TabIndex = 5;
            this.lbMin.Text = "label5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(132, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Max";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.lbMax);
            this.groupBox1.Controls.Add(this.lbMin);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(333, 36);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Limits";
            // 
            // lbMax
            // 
            this.lbMax.AutoSize = true;
            this.lbMax.Location = new System.Drawing.Point(170, 16);
            this.lbMax.Name = "lbMax";
            this.lbMax.Size = new System.Drawing.Size(35, 13);
            this.lbMax.TabIndex = 5;
            this.lbMax.Text = "label5";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.tbDecimal);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label74);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.tbHex);
            this.groupBox2.Controls.Add(this.label72);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label70);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label68);
            this.groupBox2.Controls.Add(this.lbBin63);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label73);
            this.groupBox2.Controls.Add(this.lbBin62);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label71);
            this.groupBox2.Controls.Add(this.lbBin59);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label69);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.lbBin47);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.lbBin55);
            this.groupBox2.Controls.Add(this.lbBin16);
            this.groupBox2.Controls.Add(this.lbBin58);
            this.groupBox2.Controls.Add(this.lbBin20);
            this.groupBox2.Controls.Add(this.lbBin51);
            this.groupBox2.Controls.Add(this.lbBin24);
            this.groupBox2.Controls.Add(this.lbBin46);
            this.groupBox2.Controls.Add(this.lbBin28);
            this.groupBox2.Controls.Add(this.lbBin54);
            this.groupBox2.Controls.Add(this.lbBin00);
            this.groupBox2.Controls.Add(this.lbBin43);
            this.groupBox2.Controls.Add(this.lbBin04);
            this.groupBox2.Controls.Add(this.lbBin50);
            this.groupBox2.Controls.Add(this.lbBin08);
            this.groupBox2.Controls.Add(this.lbBin15);
            this.groupBox2.Controls.Add(this.lbBin12);
            this.groupBox2.Controls.Add(this.lbBin39);
            this.groupBox2.Controls.Add(this.lbBin32);
            this.groupBox2.Controls.Add(this.lbBin42);
            this.groupBox2.Controls.Add(this.lbBin36);
            this.groupBox2.Controls.Add(this.lbBin35);
            this.groupBox2.Controls.Add(this.lbBin40);
            this.groupBox2.Controls.Add(this.lbBin38);
            this.groupBox2.Controls.Add(this.lbBin44);
            this.groupBox2.Controls.Add(this.lbBin34);
            this.groupBox2.Controls.Add(this.lbBin48);
            this.groupBox2.Controls.Add(this.lbBin61);
            this.groupBox2.Controls.Add(this.lbBin52);
            this.groupBox2.Controls.Add(this.lbBin11);
            this.groupBox2.Controls.Add(this.lbBin56);
            this.groupBox2.Controls.Add(this.lbBin07);
            this.groupBox2.Controls.Add(this.lbBin17);
            this.groupBox2.Controls.Add(this.lbBin03);
            this.groupBox2.Controls.Add(this.lbBin21);
            this.groupBox2.Controls.Add(this.lbBin31);
            this.groupBox2.Controls.Add(this.lbBin25);
            this.groupBox2.Controls.Add(this.lbBin57);
            this.groupBox2.Controls.Add(this.lbBin60);
            this.groupBox2.Controls.Add(this.lbBin53);
            this.groupBox2.Controls.Add(this.lbBin01);
            this.groupBox2.Controls.Add(this.lbBin49);
            this.groupBox2.Controls.Add(this.lbBin05);
            this.groupBox2.Controls.Add(this.lbBin14);
            this.groupBox2.Controls.Add(this.lbBin09);
            this.groupBox2.Controls.Add(this.lbBin27);
            this.groupBox2.Controls.Add(this.lbBin29);
            this.groupBox2.Controls.Add(this.lbBin23);
            this.groupBox2.Controls.Add(this.lbBin33);
            this.groupBox2.Controls.Add(this.lbBin19);
            this.groupBox2.Controls.Add(this.lbBin37);
            this.groupBox2.Controls.Add(this.lbBin30);
            this.groupBox2.Controls.Add(this.lbBin41);
            this.groupBox2.Controls.Add(this.lbBin10);
            this.groupBox2.Controls.Add(this.lbBin13);
            this.groupBox2.Controls.Add(this.lbBin06);
            this.groupBox2.Controls.Add(this.lbBin18);
            this.groupBox2.Controls.Add(this.lbBin02);
            this.groupBox2.Controls.Add(this.lbBin22);
            this.groupBox2.Controls.Add(this.lbBin45);
            this.groupBox2.Controls.Add(this.lbBin26);
            this.groupBox2.Location = new System.Drawing.Point(3, 45);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(333, 205);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Value";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(183, 182);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "8";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label7.Location = new System.Drawing.Point(177, 149);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "24";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label8.Location = new System.Drawing.Point(177, 116);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "40";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label9.Location = new System.Drawing.Point(177, 83);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "56";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label10.Location = new System.Drawing.Point(211, 182);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "7";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label11.Location = new System.Drawing.Point(211, 149);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "23";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label12.Location = new System.Drawing.Point(210, 116);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "39";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label13.Location = new System.Drawing.Point(210, 83);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "55";
            // 
            // IntegerHexEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MinimumSize = new System.Drawing.Size(339, 256);
            this.Name = "IntegerHexEditor";
            this.Size = new System.Drawing.Size(339, 256);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox tbDecimal;
        public System.Windows.Forms.TextBox tbHex;
        public System.Windows.Forms.Label lbBin63;
        public System.Windows.Forms.Label lbBin62;
        public System.Windows.Forms.Label lbBin61;
        public System.Windows.Forms.Label lbBin60;
        public System.Windows.Forms.Label lbBin47;
        public System.Windows.Forms.Label lbBin46;
        public System.Windows.Forms.Label lbBin45;
        public System.Windows.Forms.Label lbBin44;
        public System.Windows.Forms.Label lbBin15;
        public System.Windows.Forms.Label lbBin14;
        public System.Windows.Forms.Label lbBin13;
        public System.Windows.Forms.Label lbBin12;
        public System.Windows.Forms.Label lbBin31;
        public System.Windows.Forms.Label lbBin30;
        public System.Windows.Forms.Label lbBin29;
        public System.Windows.Forms.Label lbBin28;
        public System.Windows.Forms.Label lbBin59;
        public System.Windows.Forms.Label lbBin58;
        public System.Windows.Forms.Label lbBin43;
        public System.Windows.Forms.Label lbBin42;
        public System.Windows.Forms.Label lbBin11;
        public System.Windows.Forms.Label lbBin57;
        public System.Windows.Forms.Label lbBin27;
        public System.Windows.Forms.Label lbBin10;
        public System.Windows.Forms.Label lbBin26;
        public System.Windows.Forms.Label lbBin41;
        public System.Windows.Forms.Label lbBin09;
        public System.Windows.Forms.Label lbBin25;
        public System.Windows.Forms.Label lbBin56;
        public System.Windows.Forms.Label lbBin40;
        public System.Windows.Forms.Label lbBin08;
        public System.Windows.Forms.Label lbBin24;
        public System.Windows.Forms.Label lbBin55;
        public System.Windows.Forms.Label lbBin54;
        public System.Windows.Forms.Label lbBin39;
        public System.Windows.Forms.Label lbBin38;
        public System.Windows.Forms.Label lbBin07;
        public System.Windows.Forms.Label lbBin53;
        public System.Windows.Forms.Label lbBin23;
        public System.Windows.Forms.Label lbBin06;
        public System.Windows.Forms.Label lbBin22;
        public System.Windows.Forms.Label lbBin37;
        public System.Windows.Forms.Label lbBin05;
        public System.Windows.Forms.Label lbBin21;
        public System.Windows.Forms.Label lbBin52;
        public System.Windows.Forms.Label lbBin36;
        public System.Windows.Forms.Label lbBin04;
        public System.Windows.Forms.Label lbBin20;
        public System.Windows.Forms.Label lbBin51;
        public System.Windows.Forms.Label lbBin50;
        public System.Windows.Forms.Label lbBin35;
        public System.Windows.Forms.Label lbBin34;
        public System.Windows.Forms.Label lbBin03;
        public System.Windows.Forms.Label lbBin49;
        public System.Windows.Forms.Label lbBin19;
        public System.Windows.Forms.Label lbBin02;
        public System.Windows.Forms.Label lbBin18;
        public System.Windows.Forms.Label lbBin33;
        public System.Windows.Forms.Label lbBin01;
        public System.Windows.Forms.Label lbBin17;
        public System.Windows.Forms.Label lbBin48;
        public System.Windows.Forms.Label lbBin32;
        public System.Windows.Forms.Label lbBin00;
        public System.Windows.Forms.Label lbBin16;
        public System.Windows.Forms.Label lbMin;
        public System.Windows.Forms.Label lbMax;
        public System.Windows.Forms.GroupBox groupBox2;


    }
}
