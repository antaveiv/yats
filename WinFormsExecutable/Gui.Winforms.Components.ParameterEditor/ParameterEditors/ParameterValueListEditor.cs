﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using yats.TestCase.Parameter;

namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    public partial class ParameterValueListEditor : UserControl, IEditor
    {
        ParameterValueList valueListParameter;
        string defaultGroupBoxTitle;

        public ParameterValueListEditor()
        {
            InitializeComponent();
            tabEditor.Clear();
            defaultGroupBoxTitle = groupBox.Text;
        }

        #region IEditor Members

        public void ParameterToEditor(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ParameterToEditor(parameter); }));
                return;
            }

            if (parameter is ByteArrayValue)
            {
                ByteArrayValue byteArray = parameter as ByteArrayValue;
                this.valueListParameter = new ParameterValueList(new TestParameterInfo(null, typeof(byte).AssemblyQualifiedName, false, false, System.Data.ParameterDirection.InputOutput));
                if (byteArray.Value != null)
                {
                    foreach (var b in byteArray.Value)
                    {
                        valueListParameter.Values.Add(new IntegerParameterValue(b, byte.MinValue, byte.MaxValue));
                    }
                }
            }
            else
            {
                this.valueListParameter = (parameter as ParameterValueList);
            }
            UpdateList( );
        }

        public void EditorToParameter(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameter); }));
                return;
            }

            int index = list.SelectedIndex;
            if (index != -1 && index < valueListParameter.Values.Count)
            {
                AbstractParameterValue previousEditedValue;
                tabEditor.Save(out previousEditedValue);
                valueListParameter.Values[index] = previousEditedValue;
            }

            if (object.ReferenceEquals(parameter, valueListParameter))
            {
                return; // all done, editing the actual parameter object not a clone. Is that possible?
            }

	        if (parameter is ParameterValueList)
            {
                (parameter as ParameterValueList).Values.Clear( );
                foreach(var v in valueListParameter.Values)
                {
                    (parameter as ParameterValueList).Values.Add( v.Clone( ) );
                }
            }
            else if (parameter is ByteArrayValue) {
                ByteArrayValue byteArray = parameter as ByteArrayValue;
                byteArray.Value = valueListParameter.Values.Select(v => (byte)((decimal)v.GetValue(null))).ToArray(); // somehow can not cast directly from decimal to byte
            }
        }

        [DebuggerStepThrough]
        public string TabName()
        {
            return "Multiple values";
        }

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(typeof(ParameterValueList), this.GetType(), 10);
            parameterTypes.RegisterEditorType(typeof(ByteArrayValue), this.GetType(), 30);
        }

        public event EventHandler OnConfirm;

        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }

        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
		{
            tabEditor.ValidateCurrentTab(validatingForSave);
		}
        #endregion
		        
        private void UpdateList()
        {
            groupBox.Text = string.Format("{0} - {1} item(s)", defaultGroupBoxTitle, valueListParameter.Values.Count);
            if(valueListParameter == null)
            {
                return;
            }

            list.SelectedIndexChanged -= list_SelectedIndexChanged;
            while(list.Items.Count > valueListParameter.Values.Count)
            {
                list.Items.RemoveAt( list.Items.Count - 1 );
            }
            while(list.Items.Count < valueListParameter.Values.Count)
            {
                list.Items.Add( "" );
            }

            for(int i = 0; i < valueListParameter.Values.Count; i++)
            {
                list.Items[i] = string.Format( "[{0}]", ParamValueToString.Get( valueListParameter.Values[i], null ) ); // globals is passed as null because the value should not be global anyway
            }
            list.SelectedIndexChanged += list_SelectedIndexChanged;
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            try
            {
                tabEditor.ValidateCurrentTab(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            tabEditor.Save( );
            
            valueListParameter.Values.Add( null );
            list.Items.Add( "" );

            list.SelectedIndex = valueListParameter.Values.Count - 1; // start editing
        }

        private void btRemove_Click(object sender, EventArgs e)
        {
            if (list.SelectedIndex < 0 || list.SelectedItem == null)
            {
                return;
            }
            int index = list.SelectedIndex;
            valueListParameter.Values.RemoveAt( index );
            UpdateList( );
            tabEditor.Clear();
            if(valueListParameter.Values.Count > 0)
            {
                list.SelectedIndex = Utilities.MathUtil.Limit( index, 0, valueListParameter.Values.Count - 1 );
            }
            else
            {
                deleteToolStripMenuItem.Enabled = btRemove.Enabled = false;                
                tabEditor.Clear( );
            }
        }

        int lastIndex = -1;
        private void list_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(IsDisposed)
            {
                return;
            }
            try
            {
                try
                {
                    tabEditor.ValidateCurrentTab(false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }

                int index = list.SelectedIndex;
                if (lastIndex != -1 && lastIndex < valueListParameter.Values.Count)
                {
                    AbstractParameterValue previousEditedValue;
                    tabEditor.Save(out previousEditedValue);
                    valueListParameter.Values[lastIndex] = previousEditedValue;
                }

                UpdateList( );
                deleteToolStripMenuItem.Enabled = btRemove.Enabled = index >= 0;
                moveUpToolStripMenuItem.Enabled = btUp.Enabled = index > 0;
                moveDownToolStripMenuItem.Enabled = btDown.Enabled = index >= 0 && index < list.Items.Count - 1;

                if(index < 0)
                {
                    tabEditor.Clear( );
                    return;
                }
                if (index == lastIndex)
                {
                    return;
                }

                List<AbstractParameterValue> parameterValueTypes = ParameterTypeCollection.Instance.GetSuitableValueTypes(valueListParameter.ParameterInfo);
                Dictionary<AbstractParameterValue, AbstractParameterValue> paramTypeValueMap = new Dictionary<AbstractParameterValue, AbstractParameterValue>();
                var cloneToEdit = valueListParameter.Values[index];
                foreach (var paramType in parameterValueTypes)
                {
                    if (cloneToEdit != null && paramType.GetType() == cloneToEdit.GetType())
                    {
                        paramTypeValueMap.Add(paramType, cloneToEdit);
                    }
                    else
                    {
                        paramTypeValueMap.Add(paramType, null);
                    }
                }

                lastIndex = index;
                tabEditor.Edit(paramTypeValueMap);
            }
            catch 
            { 
            }
        }
        
        private void btUp_Click(object sender, EventArgs e)
        {
            if(list.SelectedIndex <= 0 || list.SelectedItem == null)
            {
                return;
            }

            try
            {
                tabEditor.ValidateCurrentTab(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            tabEditor.Save( );
            int index = list.SelectedIndex;
            
            try
            {
                var x = valueListParameter.Values[index];
                valueListParameter.Values.RemoveAt( index );
                valueListParameter.Values.Insert( index - 1, x );
                UpdateList( );
                list.SelectedIndex = index - 1;
            }
            catch { }
        }

        private void btDown_Click(object sender, EventArgs e)
        {
            if(list.SelectedIndex == list.Items.Count - 1 || list.SelectedItem == null)
            {
                return;
            }

            try
            {
                tabEditor.ValidateCurrentTab(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            tabEditor.Save( );
            int index = list.SelectedIndex;

            try
            {
                var x = valueListParameter.Values[index];
                valueListParameter.Values.RemoveAt( index );
                valueListParameter.Values.Insert( index + 1, x );
                UpdateList( );
                list.SelectedIndex = index + 1;
            }
            catch { }
        }

        private void ParameterValueListEditor_ParentChanged(object sender, EventArgs e)
        {
            if(this.ParentForm != null)
            {
                ParentForm.KeyDown += ParentForm_KeyDown;
                ParentForm.KeyPreview = true;
            }
        }

        void ParentForm_KeyDown(object sender, KeyEventArgs e)
        {
            //TODO: hint with shortcut list
            if(e.Control && !e.Alt)
            {
                //move down
                if(e.KeyCode == Keys.Down && e.Shift && moveDownToolStripMenuItem.Enabled)
                {
                    btDown_Click( sender, null );
                    e.Handled = true;
                }
                //move up
                else if(e.KeyCode == Keys.Up && e.Shift && moveUpToolStripMenuItem.Enabled)
                {
                    btUp_Click( sender, null );
                    e.Handled = true;
                }
                //select next item
                else if(e.KeyCode == Keys.Down && !e.Shift && moveDownToolStripMenuItem.Enabled)
                {
                    list.SelectedIndex = Utilities.MathUtil.Limit( list.SelectedIndex + 1, 0, list.Items.Count - 1 );
                    e.Handled = true;
                }
                //select previous item
                else if(e.KeyCode == Keys.Up && !e.Shift && moveUpToolStripMenuItem.Enabled)
                {
                    list.SelectedIndex = Utilities.MathUtil.Limit( list.SelectedIndex - 1, 0, list.Items.Count - 1 );
                    e.Handled = true;
                }
                //insert
                else if(e.KeyCode == Keys.Insert && addToolStripMenuItem.Enabled)
                {
                    btAdd_Click( sender, null );
                    e.Handled = true;
                }
                //remove
                else if(e.KeyCode == Keys.Delete && deleteToolStripMenuItem.Enabled)
                {
                    btRemove_Click( sender, null );
                    e.Handled = true;
                }
            }
        }
                
    }
}
