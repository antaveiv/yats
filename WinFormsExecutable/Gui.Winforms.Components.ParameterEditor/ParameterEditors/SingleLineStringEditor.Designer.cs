﻿namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    partial class SingleLineStringEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox = new yats.Gui.Winforms.Components.Util.TextBoxAdv();
            this.rbNone = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.rbCrLf = new System.Windows.Forms.RadioButton();
            this.rbLF = new System.Windows.Forms.RadioButton();
            this.rbCR = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox
            // 
            this.textBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox.Location = new System.Drawing.Point(6, 19);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(618, 20);
            this.textBox.TabIndex = 0;
            // 
            // rbNone
            // 
            this.rbNone.AutoSize = true;
            this.rbNone.Location = new System.Drawing.Point(3, 3);
            this.rbNone.Name = "rbNone";
            this.rbNone.Size = new System.Drawing.Size(122, 17);
            this.rbNone.TabIndex = 1;
            this.rbNone.TabStop = true;
            this.rbNone.Text = "No ending character";
            this.rbNone.UseVisualStyleBackColor = true;
            this.rbNone.CheckedChanged += new System.EventHandler(this.lineEndingRadioChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.flowLayoutPanel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(630, 52);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Line end";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.rbNone);
            this.flowLayoutPanel1.Controls.Add(this.rbCrLf);
            this.flowLayoutPanel1.Controls.Add(this.rbLF);
            this.flowLayoutPanel1.Controls.Add(this.rbCR);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(624, 33);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // rbCrLf
            // 
            this.rbCrLf.AutoSize = true;
            this.rbCrLf.Location = new System.Drawing.Point(131, 3);
            this.rbCrLf.Name = "rbCrLf";
            this.rbCrLf.Size = new System.Drawing.Size(86, 17);
            this.rbCrLf.TabIndex = 2;
            this.rbCrLf.TabStop = true;
            this.rbCrLf.Text = "CR+LF (/r/n)";
            this.rbCrLf.UseVisualStyleBackColor = true;
            this.rbCrLf.CheckedChanged += new System.EventHandler(this.lineEndingRadioChanged);
            // 
            // rbLF
            // 
            this.rbLF.AutoSize = true;
            this.rbLF.Location = new System.Drawing.Point(223, 3);
            this.rbLF.Name = "rbLF";
            this.rbLF.Size = new System.Drawing.Size(57, 17);
            this.rbLF.TabIndex = 3;
            this.rbLF.TabStop = true;
            this.rbLF.Text = "LF (/n)";
            this.rbLF.UseVisualStyleBackColor = true;
            this.rbLF.CheckedChanged += new System.EventHandler(this.lineEndingRadioChanged);
            // 
            // rbCR
            // 
            this.rbCR.AutoSize = true;
            this.rbCR.Location = new System.Drawing.Point(286, 3);
            this.rbCR.Name = "rbCR";
            this.rbCR.Size = new System.Drawing.Size(57, 17);
            this.rbCR.TabIndex = 4;
            this.rbCR.TabStop = true;
            this.rbCR.Text = "CR (/r)";
            this.rbCR.UseVisualStyleBackColor = true;
            this.rbCR.CheckedChanged += new System.EventHandler(this.lineEndingRadioChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(630, 48);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Value";
            // 
            // SingleLineStringEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.MinimumSize = new System.Drawing.Size(630, 100);
            this.Name = "SingleLineStringEditor";
            this.Size = new System.Drawing.Size(630, 100);
            this.groupBox1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private yats.Gui.Winforms.Components.Util.TextBoxAdv textBox;
        private System.Windows.Forms.RadioButton rbNone;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.RadioButton rbCrLf;
        private System.Windows.Forms.RadioButton rbLF;
        private System.Windows.Forms.RadioButton rbCR;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}
