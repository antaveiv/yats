﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using yats.TestCase.Parameter;

namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    public partial class MultiNumericTextEditor : UserControl, IEditor
    {
        private string editedTypeName;

        public MultiNumericTextEditor()
        {
            InitializeComponent();
        }
        
        #region IEditor Members

        public void ParameterToEditor(AbstractParameterValue parameterValue)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ParameterToEditor(parameterValue); }));
            }
            else
            {
                ParameterValueList valueList = parameterValue as ParameterValueList;
                ByteArrayValue byteArray = parameterValue as ByteArrayValue;
                if (valueList != null)
                {
                    editedTypeName = valueList.ParameterInfo.ParamType;
                    textBox.Text = string.Join(" ", valueList.Values.Select(x=>x.GetValue(null).ToString()).ToArray());
                }
                if (byteArray != null)
                {
                    editedTypeName = typeof(byte).AssemblyQualifiedName;
                    if (byteArray.Value != null)
                    {
                        textBox.Text = string.Join(" ", byteArray.Value.Select(x => x.ToString()).ToArray());
                    }
                }
                if (editedTypeName == null)
                {
                    return;
                }
                if (Type.GetType(editedTypeName).IsGenericType && Type.GetType(editedTypeName).GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    editedTypeName = Nullable.GetUnderlyingType(Type.GetType(editedTypeName)).AssemblyQualifiedName;
                }

                if (editedTypeName == typeof(byte).AssemblyQualifiedName)
                {
                    parser = ParseByte;
                }
                else if (editedTypeName == typeof(sbyte).AssemblyQualifiedName)
                {
                    parser = ParseSByte;
                }
                else if (editedTypeName == typeof(int).AssemblyQualifiedName)
                {
                    parser = ParseInt;
                }
                else if (editedTypeName == typeof(uint).AssemblyQualifiedName)
                {
                    parser = ParseUInt;
                }
                else if (editedTypeName == typeof(short).AssemblyQualifiedName)
                {
                    parser = ParseInt16;
                }
                else if (editedTypeName == typeof(ushort).AssemblyQualifiedName)
                {
                    parser = ParseUInt16;
                }
                else if (editedTypeName == typeof(decimal).AssemblyQualifiedName)
                {
                    parser = ParseDecimal;
                }
                else if (editedTypeName == typeof(double).AssemblyQualifiedName)
                {
                    parser = ParseDouble;
                }
                else if (editedTypeName == typeof(float).AssemblyQualifiedName)
                {
                    parser = ParseFloat;
                }
                else if (editedTypeName == typeof(Int64).AssemblyQualifiedName)
                {
                    parser = ParseInt64;
                }
                else if (editedTypeName == typeof(UInt64).AssemblyQualifiedName)
                {
                    parser = ParseUInt64;
                }
                else
                {
                    throw new NotImplementedException(string.Format("Editor not implemented for {0}", editedTypeName));
                }
            }
        }

        public void EditorToParameter(AbstractParameterValue parameterValue)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameterValue); }));
            }
            else
            {
                var numbers = textBox.Text.Split(new char[]{' ', '\t'}, StringSplitOptions.RemoveEmptyEntries);
                ParameterValueList valueList = parameterValue as ParameterValueList;
                ByteArrayValue byteArray = parameterValue as ByteArrayValue;
                if (valueList != null)
                {
                    valueList.Values.Clear();
                    if (editedTypeName == typeof(double).AssemblyQualifiedName)
                    {
                        foreach (var numberString in numbers)
                        {
                            valueList.Values.Add(new DoubleParameter(double.Parse(numberString)));
                        }
                    }
                    else if (editedTypeName == typeof(float).AssemblyQualifiedName)
                    {
                        foreach (var numberString in numbers)
                        {
                            valueList.Values.Add(new FloatParameter(float.Parse(numberString)));
                        }
                    }
                    else
                    { // integer types
                        foreach (var numberString in numbers)
                        {
                            decimal d = decimal.Parse(numberString);
                            valueList.Values.Add(new IntegerParameterValue(d, Type.GetType(editedTypeName)));
                        }
                    }
                    textBox.Text = string.Join(" ", valueList.Values.Select(x => x.GetValue(null).ToString()).ToArray());
                }

                if (byteArray != null)
                {                    
                    Debug.Assert(parser == ParseByte);
                    byteArray.Value = numbers.Select(x=>byte.Parse(x)).ToArray();
                }
            }
        }


        [DebuggerStepThrough]
        public string TabName()
        {
            return "Text entry";
        }
                
        private delegate bool ParserDelegate(string s, out object value);
        private ParserDelegate parser;

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(delegate(AbstractParameterValue parameterValue, ref int valueTypePriority)
            {
                ParameterValueList valueList = parameterValue as ParameterValueList;
                ByteArrayValue byteArray = parameterValue as ByteArrayValue;
                string editedTypeName; // do not use member - it stays between calls to this delegate
                if (valueList != null)
                {
                    editedTypeName = valueList.ParameterInfo.ParamType;
                }
                else if (byteArray != null)
                {
                    editedTypeName = typeof(byte).AssemblyQualifiedName;
                }
                else
                {
                    return null;
                }

                if (editedTypeName != null)
                {
                    if (Type.GetType(editedTypeName).IsGenericType && Type.GetType(editedTypeName).GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        editedTypeName = Nullable.GetUnderlyingType(Type.GetType(editedTypeName)).AssemblyQualifiedName;
                    }

                    if (Utilities.NumericTypeHelper.IsNumericType(Type.GetType(editedTypeName)) == false)
                    {
                        return null;
                    }
                    
                    valueTypePriority = 100;
                    return this.GetType();
                }

                return null;
            });
        }

        public event EventHandler OnConfirm;

        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }
                
        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
        {
            var numbers = textBox.Text.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries); 
            foreach (var n in numbers)
            {
                object value;
                if (!parser(n, out value))
                {
                    textBox.SelectionStart = textBox.Text.IndexOf(n);
                    textBox.SelectionLength = n.Length;
                    textBox.Focus();
                    throw new Exception("Invalid number format: " + n);
                }
            }
        }

        //TODO a helper utility should be made out of this mess
        bool ParseByte(string s, out object value)
        {
            byte v;
            bool result = byte.TryParse(s, out v);
            value = v;
            return result;
        }

        bool ParseSByte(string s, out object value)
        {
            sbyte v;
            bool result = sbyte.TryParse(s, out v);
            value = v;
            return result;
        }

        bool ParseInt(string s, out object value)
        {
            int v;
            bool result = int.TryParse(s, out v);
            value = v;
            return result;
        }

        bool ParseUInt(string s, out object value)
        {
            uint v;
            bool result = uint.TryParse(s, out v);
            value = v;
            return result;
        }

        bool ParseInt16(string s, out object value)
        {
            short v;
            bool result = short.TryParse(s, out v);
            value = v;
            return result;
        }

        bool ParseUInt16(string s, out object value)
        {
            ushort v;
            bool result = ushort.TryParse(s, out v);
            value = v;
            return result;
        }

        bool ParseInt64(string s, out object value)
        {
            Int64 v;
            bool result = Int64.TryParse(s, out v);
            value = v;
            return result;
        }

        bool ParseUInt64(string s, out object value)
        {
            UInt64 v;
            bool result = UInt64.TryParse(s, out v);
            value = v;
            return result;
        }

        bool ParseDecimal(string s, out object value)
        {
            decimal v;
            bool result = decimal.TryParse(s, out v);
            value = v;
            return result;
        }

        bool ParseFloat(string s, out object value)
        {
            float v;
            bool result = float.TryParse(s, out v);
            value = v;
            return result;
        }

        bool ParseDouble(string s, out object value)
        {
            double v;
            bool result = double.TryParse(s, out v);
            value = v;
            return result;
        }
        #endregion
    }
}
