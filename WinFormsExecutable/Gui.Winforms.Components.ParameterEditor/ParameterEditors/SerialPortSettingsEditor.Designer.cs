﻿namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    partial class SerialPortSettingsEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabPageTcpIpServer = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.tcpServerAcceptLaterConnections = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tcpServerConnectTimeout = new System.Windows.Forms.NumericUpDown();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.tcpServerPort = new System.Windows.Forms.NumericUpDown();
            this.tabPageTcpIpClient = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.tcpClientAddress = new yats.Gui.Winforms.Components.Util.TextBoxAdv();
            this.group = new System.Windows.Forms.GroupBox();
            this.tcpClientPort = new yats.Gui.Winforms.Components.Util.TextBoxAdv();
            this.tabs = new System.Windows.Forms.TabControl();
            this.tabPageSerial = new System.Windows.Forms.TabPage();
            this.grPortName = new System.Windows.Forms.GroupBox();
            this.portName = new System.Windows.Forms.ListBox();
            this.grDtrEnable = new System.Windows.Forms.GroupBox();
            this.dtrEnable = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.rbRtsOff = new System.Windows.Forms.RadioButton();
            this.rbRtsNoChange = new System.Windows.Forms.RadioButton();
            this.rbRtsOn = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.stopBits = new System.Windows.Forms.ListBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.parity = new System.Windows.Forms.ListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.handshake = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataBits = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.baudRate = new System.Windows.Forms.ListBox();
            this.tabPageUdpSocket = new System.Windows.Forms.TabPage();
            this.udpConfigureRemote = new System.Windows.Forms.RadioButton();
            this.udpOverwritePort = new System.Windows.Forms.RadioButton();
            this.groupRemote = new System.Windows.Forms.GroupBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.udpBroadcastIpCustom = new System.Windows.Forms.RadioButton();
            this.udpBroadcastIpDefault = new System.Windows.Forms.RadioButton();
            this.udpIpBroadcastRemoteAddress = new IPAddressControlLib.IPAddressControl();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.udpIpUnicastRemoteAddress = new IPAddressControlLib.IPAddressControl();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.udpRemotePort = new System.Windows.Forms.NumericUpDown();
            this.udpBroadcast = new System.Windows.Forms.RadioButton();
            this.udpUnicast = new System.Windows.Forms.RadioButton();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.udpLocalPort = new System.Windows.Forms.NumericUpDown();
            this.tabPageSshClient = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.sshClientPassword = new yats.Gui.Winforms.Components.Util.TextBoxAdv();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.sshClientUser = new yats.Gui.Winforms.Components.Util.TextBoxAdv();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sshClientHost = new yats.Gui.Winforms.Components.Util.TextBoxAdv();
            this.tabPageBluetooth = new System.Windows.Forms.TabPage();
            this.btAddBluetoothDevice = new System.Windows.Forms.Button();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.tbBluetoothPin = new System.Windows.Forms.TextBox();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.lbSelectedBluetoothDevice = new System.Windows.Forms.Label();
            this.btSelectBluetoothDevice = new System.Windows.Forms.Button();
            this.tabPageEmulator = new System.Windows.Forms.TabPage();
            this.emulatorHexEditor = new yats.Gui.Winforms.Components.Util.ExtendedHexEditor();
            this.btBrowseForFile = new System.Windows.Forms.Button();
            this.tbEmulatorFileName = new System.Windows.Forms.TextBox();
            this.rbEmulatorFromBytes = new System.Windows.Forms.RadioButton();
            this.rbEmulatorFromFile = new System.Windows.Forms.RadioButton();
            this.serialPortListRefreshTimer = new System.Windows.Forms.Timer(this.components);
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.cbDiscardDataWithoutParsers = new System.Windows.Forms.CheckBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.tcpClientKeepAlive = new System.Windows.Forms.CheckBox();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.tcpServerKeepAlive = new System.Windows.Forms.CheckBox();
            this.tabPageTcpIpServer.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcpServerConnectTimeout)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcpServerPort)).BeginInit();
            this.tabPageTcpIpClient.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.group.SuspendLayout();
            this.tabs.SuspendLayout();
            this.tabPageSerial.SuspendLayout();
            this.grPortName.SuspendLayout();
            this.grDtrEnable.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPageUdpSocket.SuspendLayout();
            this.groupRemote.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udpRemotePort)).BeginInit();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udpLocalPort)).BeginInit();
            this.tabPageSshClient.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPageBluetooth.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.tabPageEmulator.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPageTcpIpServer
            // 
            this.tabPageTcpIpServer.Controls.Add(this.groupBox22);
            this.tabPageTcpIpServer.Controls.Add(this.groupBox16);
            this.tabPageTcpIpServer.Controls.Add(this.groupBox12);
            this.tabPageTcpIpServer.Controls.Add(this.groupBox8);
            this.tabPageTcpIpServer.Location = new System.Drawing.Point(4, 22);
            this.tabPageTcpIpServer.Name = "tabPageTcpIpServer";
            this.tabPageTcpIpServer.Size = new System.Drawing.Size(674, 350);
            this.tabPageTcpIpServer.TabIndex = 2;
            this.tabPageTcpIpServer.Text = "TCP/IP server";
            this.tabPageTcpIpServer.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox16.Controls.Add(this.tcpServerAcceptLaterConnections);
            this.groupBox16.Controls.Add(this.label2);
            this.groupBox16.Location = new System.Drawing.Point(3, 117);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(668, 67);
            this.groupBox16.TabIndex = 3;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Accept connections";
            // 
            // tcpServerAcceptLaterConnections
            // 
            this.tcpServerAcceptLaterConnections.AutoSize = true;
            this.tcpServerAcceptLaterConnections.Location = new System.Drawing.Point(6, 17);
            this.tcpServerAcceptLaterConnections.Name = "tcpServerAcceptLaterConnections";
            this.tcpServerAcceptLaterConnections.Size = new System.Drawing.Size(221, 17);
            this.tcpServerAcceptLaterConnections.TabIndex = 4;
            this.tcpServerAcceptLaterConnections.Text = "Replace last socket with new connection";
            this.tcpServerAcceptLaterConnections.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(451, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "If checked, the opened socket should work even if the client disconnects and conn" +
    "ects again";
            // 
            // groupBox12
            // 
            this.groupBox12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox12.Controls.Add(this.label1);
            this.groupBox12.Controls.Add(this.tcpServerConnectTimeout);
            this.groupBox12.Location = new System.Drawing.Point(3, 60);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(668, 51);
            this.groupBox12.TabIndex = 2;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Client connect timeout";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(132, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "seconds. 0 - no timeout";
            // 
            // tcpServerConnectTimeout
            // 
            this.tcpServerConnectTimeout.Location = new System.Drawing.Point(6, 19);
            this.tcpServerConnectTimeout.Maximum = new decimal(new int[] {
            86400,
            0,
            0,
            0});
            this.tcpServerConnectTimeout.Name = "tcpServerConnectTimeout";
            this.tcpServerConnectTimeout.Size = new System.Drawing.Size(120, 20);
            this.tcpServerConnectTimeout.TabIndex = 2;
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this.tcpServerPort);
            this.groupBox8.Location = new System.Drawing.Point(3, 3);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(668, 51);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Listening port number";
            // 
            // tcpServerPort
            // 
            this.tcpServerPort.Location = new System.Drawing.Point(6, 19);
            this.tcpServerPort.Maximum = new decimal(new int[] {
            65536,
            0,
            0,
            0});
            this.tcpServerPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tcpServerPort.Name = "tcpServerPort";
            this.tcpServerPort.Size = new System.Drawing.Size(120, 20);
            this.tcpServerPort.TabIndex = 1;
            this.tcpServerPort.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tabPageTcpIpClient
            // 
            this.tabPageTcpIpClient.Controls.Add(this.groupBox9);
            this.tabPageTcpIpClient.Controls.Add(this.groupBox17);
            this.tabPageTcpIpClient.Controls.Add(this.group);
            this.tabPageTcpIpClient.Location = new System.Drawing.Point(4, 22);
            this.tabPageTcpIpClient.Name = "tabPageTcpIpClient";
            this.tabPageTcpIpClient.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTcpIpClient.Size = new System.Drawing.Size(674, 350);
            this.tabPageTcpIpClient.TabIndex = 1;
            this.tabPageTcpIpClient.Text = "TCP/IP client";
            this.tabPageTcpIpClient.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.tcpClientAddress);
            this.groupBox9.Location = new System.Drawing.Point(6, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(662, 57);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "TCP/IP Server address";
            // 
            // tcpClientAddress
            // 
            this.tcpClientAddress.Location = new System.Drawing.Point(6, 19);
            this.tcpClientAddress.Name = "tcpClientAddress";
            this.tcpClientAddress.Size = new System.Drawing.Size(397, 20);
            this.tcpClientAddress.TabIndex = 0;
            // 
            // group
            // 
            this.group.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.group.Controls.Add(this.tcpClientPort);
            this.group.Location = new System.Drawing.Point(6, 69);
            this.group.Name = "group";
            this.group.Size = new System.Drawing.Size(662, 57);
            this.group.TabIndex = 1;
            this.group.TabStop = false;
            this.group.Text = "Port number";
            // 
            // tcpClientPort
            // 
            this.tcpClientPort.Location = new System.Drawing.Point(6, 19);
            this.tcpClientPort.Name = "tcpClientPort";
            this.tcpClientPort.Size = new System.Drawing.Size(100, 20);
            this.tcpClientPort.TabIndex = 0;
            // 
            // tabs
            // 
            this.tabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabs.Controls.Add(this.tabPageSerial);
            this.tabs.Controls.Add(this.tabPageTcpIpClient);
            this.tabs.Controls.Add(this.tabPageTcpIpServer);
            this.tabs.Controls.Add(this.tabPageUdpSocket);
            this.tabs.Controls.Add(this.tabPageSshClient);
            this.tabs.Controls.Add(this.tabPageBluetooth);
            this.tabs.Controls.Add(this.tabPageEmulator);
            this.tabs.Location = new System.Drawing.Point(0, 0);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(682, 376);
            this.tabs.TabIndex = 8;
            // 
            // tabPageSerial
            // 
            this.tabPageSerial.Controls.Add(this.grPortName);
            this.tabPageSerial.Controls.Add(this.grDtrEnable);
            this.tabPageSerial.Controls.Add(this.groupBox7);
            this.tabPageSerial.Controls.Add(this.groupBox6);
            this.tabPageSerial.Controls.Add(this.groupBox5);
            this.tabPageSerial.Controls.Add(this.groupBox4);
            this.tabPageSerial.Controls.Add(this.groupBox3);
            this.tabPageSerial.Controls.Add(this.groupBox2);
            this.tabPageSerial.Location = new System.Drawing.Point(4, 22);
            this.tabPageSerial.Name = "tabPageSerial";
            this.tabPageSerial.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSerial.Size = new System.Drawing.Size(674, 350);
            this.tabPageSerial.TabIndex = 0;
            this.tabPageSerial.Text = "Serial port";
            this.tabPageSerial.UseVisualStyleBackColor = true;
            // 
            // grPortName
            // 
            this.grPortName.Controls.Add(this.portName);
            this.grPortName.Location = new System.Drawing.Point(6, 8);
            this.grPortName.Name = "grPortName";
            this.grPortName.Size = new System.Drawing.Size(200, 329);
            this.grPortName.TabIndex = 0;
            this.grPortName.TabStop = false;
            this.grPortName.Text = "Serial port";
            // 
            // portName
            // 
            this.portName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.portName.FormattingEnabled = true;
            this.portName.Location = new System.Drawing.Point(3, 16);
            this.portName.Name = "portName";
            this.portName.Size = new System.Drawing.Size(194, 310);
            this.portName.TabIndex = 0;
            this.portName.DoubleClick += new System.EventHandler(this.portName_DoubleClick);
            // 
            // grDtrEnable
            // 
            this.grDtrEnable.Controls.Add(this.dtrEnable);
            this.grDtrEnable.Location = new System.Drawing.Point(422, 232);
            this.grDtrEnable.Name = "grDtrEnable";
            this.grDtrEnable.Size = new System.Drawing.Size(242, 47);
            this.grDtrEnable.TabIndex = 6;
            this.grDtrEnable.TabStop = false;
            this.grDtrEnable.Text = "DTR signal";
            // 
            // dtrEnable
            // 
            this.dtrEnable.AutoSize = true;
            this.dtrEnable.Location = new System.Drawing.Point(6, 19);
            this.dtrEnable.Name = "dtrEnable";
            this.dtrEnable.Size = new System.Drawing.Size(68, 17);
            this.dtrEnable.TabIndex = 11;
            this.dtrEnable.Text = "Set DTR";
            this.dtrEnable.ThreeState = true;
            this.dtrEnable.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.rbRtsOff);
            this.groupBox7.Controls.Add(this.rbRtsNoChange);
            this.groupBox7.Controls.Add(this.rbRtsOn);
            this.groupBox7.Location = new System.Drawing.Point(422, 285);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(242, 52);
            this.groupBox7.TabIndex = 7;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Set RTS";
            // 
            // rbRtsOff
            // 
            this.rbRtsOff.AutoSize = true;
            this.rbRtsOff.Location = new System.Drawing.Point(169, 19);
            this.rbRtsOff.Name = "rbRtsOff";
            this.rbRtsOff.Size = new System.Drawing.Size(39, 17);
            this.rbRtsOff.TabIndex = 2;
            this.rbRtsOff.TabStop = true;
            this.rbRtsOff.Text = "Off";
            this.rbRtsOff.UseVisualStyleBackColor = true;
            // 
            // rbRtsNoChange
            // 
            this.rbRtsNoChange.AutoSize = true;
            this.rbRtsNoChange.Location = new System.Drawing.Point(71, 19);
            this.rbRtsNoChange.Name = "rbRtsNoChange";
            this.rbRtsNoChange.Size = new System.Drawing.Size(78, 17);
            this.rbRtsNoChange.TabIndex = 1;
            this.rbRtsNoChange.TabStop = true;
            this.rbRtsNoChange.Text = "No change";
            this.rbRtsNoChange.UseVisualStyleBackColor = true;
            // 
            // rbRtsOn
            // 
            this.rbRtsOn.AutoSize = true;
            this.rbRtsOn.Location = new System.Drawing.Point(6, 19);
            this.rbRtsOn.Name = "rbRtsOn";
            this.rbRtsOn.Size = new System.Drawing.Size(39, 17);
            this.rbRtsOn.TabIndex = 0;
            this.rbRtsOn.TabStop = true;
            this.rbRtsOn.Text = "On";
            this.rbRtsOn.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.stopBits);
            this.groupBox6.Location = new System.Drawing.Point(549, 8);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(115, 98);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Stop bits";
            // 
            // stopBits
            // 
            this.stopBits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stopBits.FormattingEnabled = true;
            this.stopBits.Location = new System.Drawing.Point(3, 16);
            this.stopBits.Name = "stopBits";
            this.stopBits.Size = new System.Drawing.Size(109, 79);
            this.stopBits.TabIndex = 1;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.parity);
            this.groupBox5.Location = new System.Drawing.Point(422, 112);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(115, 114);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Parity";
            // 
            // parity
            // 
            this.parity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parity.FormattingEnabled = true;
            this.parity.Location = new System.Drawing.Point(3, 16);
            this.parity.Name = "parity";
            this.parity.Size = new System.Drawing.Size(109, 95);
            this.parity.TabIndex = 1;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.handshake);
            this.groupBox4.Location = new System.Drawing.Point(543, 112);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(121, 114);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Handshake";
            // 
            // handshake
            // 
            this.handshake.Dock = System.Windows.Forms.DockStyle.Fill;
            this.handshake.FormattingEnabled = true;
            this.handshake.Location = new System.Drawing.Point(3, 16);
            this.handshake.Name = "handshake";
            this.handshake.Size = new System.Drawing.Size(115, 95);
            this.handshake.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataBits);
            this.groupBox3.Location = new System.Drawing.Point(422, 8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(115, 98);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Data bits";
            // 
            // dataBits
            // 
            this.dataBits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataBits.FormattingEnabled = true;
            this.dataBits.Location = new System.Drawing.Point(3, 16);
            this.dataBits.Name = "dataBits";
            this.dataBits.Size = new System.Drawing.Size(109, 79);
            this.dataBits.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.baudRate);
            this.groupBox2.Location = new System.Drawing.Point(216, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 329);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Baud rate";
            // 
            // baudRate
            // 
            this.baudRate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.baudRate.FormattingEnabled = true;
            this.baudRate.Items.AddRange(new object[] {
            "110",
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "19200",
            "28800",
            "38400",
            "56000",
            "57600",
            "115200",
            "128000",
            "153600",
            "230400",
            "256000",
            "460800",
            "921600"});
            this.baudRate.Location = new System.Drawing.Point(3, 16);
            this.baudRate.Name = "baudRate";
            this.baudRate.Size = new System.Drawing.Size(194, 310);
            this.baudRate.TabIndex = 0;
            this.baudRate.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.baudRate_MouseDoubleClick);
            // 
            // tabPageUdpSocket
            // 
            this.tabPageUdpSocket.Controls.Add(this.udpConfigureRemote);
            this.tabPageUdpSocket.Controls.Add(this.udpOverwritePort);
            this.tabPageUdpSocket.Controls.Add(this.groupRemote);
            this.tabPageUdpSocket.Controls.Add(this.groupBox13);
            this.tabPageUdpSocket.Location = new System.Drawing.Point(4, 22);
            this.tabPageUdpSocket.Name = "tabPageUdpSocket";
            this.tabPageUdpSocket.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUdpSocket.Size = new System.Drawing.Size(674, 350);
            this.tabPageUdpSocket.TabIndex = 4;
            this.tabPageUdpSocket.Text = "UDP Socket";
            this.tabPageUdpSocket.UseVisualStyleBackColor = true;
            // 
            // udpConfigureRemote
            // 
            this.udpConfigureRemote.AutoSize = true;
            this.udpConfigureRemote.Checked = true;
            this.udpConfigureRemote.Location = new System.Drawing.Point(6, 84);
            this.udpConfigureRemote.Name = "udpConfigureRemote";
            this.udpConfigureRemote.Size = new System.Drawing.Size(105, 17);
            this.udpConfigureRemote.TabIndex = 1;
            this.udpConfigureRemote.TabStop = true;
            this.udpConfigureRemote.Text = "Configure remote";
            this.udpConfigureRemote.UseVisualStyleBackColor = true;
            this.udpConfigureRemote.CheckedChanged += new System.EventHandler(this.udpConfiguredEndpoint_CheckedChanged);
            // 
            // udpOverwritePort
            // 
            this.udpOverwritePort.AutoSize = true;
            this.udpOverwritePort.Location = new System.Drawing.Point(6, 63);
            this.udpOverwritePort.Name = "udpOverwritePort";
            this.udpOverwritePort.Size = new System.Drawing.Size(452, 17);
            this.udpOverwritePort.TabIndex = 1;
            this.udpOverwritePort.Text = "Use address and port of last received packet. Need to receive a packet to be able" +
    " to send";
            this.udpOverwritePort.UseVisualStyleBackColor = true;
            this.udpOverwritePort.CheckedChanged += new System.EventHandler(this.udpConfiguredEndpoint_CheckedChanged);
            // 
            // groupRemote
            // 
            this.groupRemote.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupRemote.Controls.Add(this.groupBox21);
            this.groupRemote.Controls.Add(this.groupBox15);
            this.groupRemote.Controls.Add(this.groupBox20);
            this.groupRemote.Controls.Add(this.udpBroadcast);
            this.groupRemote.Controls.Add(this.udpUnicast);
            this.groupRemote.Location = new System.Drawing.Point(6, 107);
            this.groupRemote.Name = "groupRemote";
            this.groupRemote.Size = new System.Drawing.Size(662, 237);
            this.groupRemote.TabIndex = 5;
            this.groupRemote.TabStop = false;
            this.groupRemote.Text = "Remote endpoint";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.udpBroadcastIpCustom);
            this.groupBox21.Controls.Add(this.udpBroadcastIpDefault);
            this.groupBox21.Controls.Add(this.udpIpBroadcastRemoteAddress);
            this.groupBox21.Enabled = false;
            this.groupBox21.Location = new System.Drawing.Point(46, 125);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(209, 76);
            this.groupBox21.TabIndex = 4;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Remote IP address";
            // 
            // udpBroadcastIpCustom
            // 
            this.udpBroadcastIpCustom.AutoSize = true;
            this.udpBroadcastIpCustom.Location = new System.Drawing.Point(6, 45);
            this.udpBroadcastIpCustom.Name = "udpBroadcastIpCustom";
            this.udpBroadcastIpCustom.Size = new System.Drawing.Size(14, 13);
            this.udpBroadcastIpCustom.TabIndex = 1;
            this.udpBroadcastIpCustom.UseVisualStyleBackColor = true;
            this.udpBroadcastIpCustom.CheckedChanged += new System.EventHandler(this.udpBroadcastIpRadiobox_CheckedChanged);
            // 
            // udpBroadcastIpDefault
            // 
            this.udpBroadcastIpDefault.AutoSize = true;
            this.udpBroadcastIpDefault.Checked = true;
            this.udpBroadcastIpDefault.Location = new System.Drawing.Point(6, 19);
            this.udpBroadcastIpDefault.Name = "udpBroadcastIpDefault";
            this.udpBroadcastIpDefault.Size = new System.Drawing.Size(106, 17);
            this.udpBroadcastIpDefault.TabIndex = 1;
            this.udpBroadcastIpDefault.TabStop = true;
            this.udpBroadcastIpDefault.Text = "255.255.255.255";
            this.udpBroadcastIpDefault.UseVisualStyleBackColor = true;
            this.udpBroadcastIpDefault.CheckedChanged += new System.EventHandler(this.udpBroadcastIpRadiobox_CheckedChanged);
            // 
            // udpIpBroadcastRemoteAddress
            // 
            this.udpIpBroadcastRemoteAddress.AllowInternalTab = false;
            this.udpIpBroadcastRemoteAddress.AutoHeight = true;
            this.udpIpBroadcastRemoteAddress.BackColor = System.Drawing.SystemColors.Window;
            this.udpIpBroadcastRemoteAddress.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.udpIpBroadcastRemoteAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.udpIpBroadcastRemoteAddress.Location = new System.Drawing.Point(26, 42);
            this.udpIpBroadcastRemoteAddress.MinimumSize = new System.Drawing.Size(87, 20);
            this.udpIpBroadcastRemoteAddress.Name = "udpIpBroadcastRemoteAddress";
            this.udpIpBroadcastRemoteAddress.ReadOnly = false;
            this.udpIpBroadcastRemoteAddress.Size = new System.Drawing.Size(120, 20);
            this.udpIpBroadcastRemoteAddress.TabIndex = 0;
            this.udpIpBroadcastRemoteAddress.Text = "...";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.udpIpUnicastRemoteAddress);
            this.groupBox15.Location = new System.Drawing.Point(46, 42);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(209, 54);
            this.groupBox15.TabIndex = 4;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Remote IP address";
            // 
            // udpIpUnicastRemoteAddress
            // 
            this.udpIpUnicastRemoteAddress.AllowInternalTab = false;
            this.udpIpUnicastRemoteAddress.AutoHeight = true;
            this.udpIpUnicastRemoteAddress.BackColor = System.Drawing.SystemColors.Window;
            this.udpIpUnicastRemoteAddress.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.udpIpUnicastRemoteAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.udpIpUnicastRemoteAddress.Location = new System.Drawing.Point(26, 18);
            this.udpIpUnicastRemoteAddress.MinimumSize = new System.Drawing.Size(87, 20);
            this.udpIpUnicastRemoteAddress.Name = "udpIpUnicastRemoteAddress";
            this.udpIpUnicastRemoteAddress.ReadOnly = false;
            this.udpIpUnicastRemoteAddress.Size = new System.Drawing.Size(120, 20);
            this.udpIpUnicastRemoteAddress.TabIndex = 0;
            this.udpIpUnicastRemoteAddress.Text = "...";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.udpRemotePort);
            this.groupBox20.Location = new System.Drawing.Point(261, 19);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(141, 182);
            this.groupBox20.TabIndex = 3;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Remote port number";
            // 
            // udpRemotePort
            // 
            this.udpRemotePort.Location = new System.Drawing.Point(6, 19);
            this.udpRemotePort.Maximum = new decimal(new int[] {
            65536,
            0,
            0,
            0});
            this.udpRemotePort.Name = "udpRemotePort";
            this.udpRemotePort.Size = new System.Drawing.Size(120, 20);
            this.udpRemotePort.TabIndex = 1;
            this.udpRemotePort.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // udpBroadcast
            // 
            this.udpBroadcast.AutoSize = true;
            this.udpBroadcast.Location = new System.Drawing.Point(6, 102);
            this.udpBroadcast.Name = "udpBroadcast";
            this.udpBroadcast.Size = new System.Drawing.Size(73, 17);
            this.udpBroadcast.TabIndex = 0;
            this.udpBroadcast.Text = "Broadcast";
            this.udpBroadcast.UseVisualStyleBackColor = true;
            this.udpBroadcast.CheckedChanged += new System.EventHandler(this.udpConfiguredEndpoint_CheckedChanged);
            // 
            // udpUnicast
            // 
            this.udpUnicast.AutoSize = true;
            this.udpUnicast.Checked = true;
            this.udpUnicast.Location = new System.Drawing.Point(6, 19);
            this.udpUnicast.Name = "udpUnicast";
            this.udpUnicast.Size = new System.Drawing.Size(61, 17);
            this.udpUnicast.TabIndex = 0;
            this.udpUnicast.TabStop = true;
            this.udpUnicast.Text = "Unicast";
            this.udpUnicast.UseVisualStyleBackColor = true;
            this.udpUnicast.CheckedChanged += new System.EventHandler(this.udpConfiguredEndpoint_CheckedChanged);
            // 
            // groupBox13
            // 
            this.groupBox13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox13.Controls.Add(this.udpLocalPort);
            this.groupBox13.Location = new System.Drawing.Point(6, 6);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(662, 51);
            this.groupBox13.TabIndex = 2;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Local port number";
            // 
            // udpLocalPort
            // 
            this.udpLocalPort.Location = new System.Drawing.Point(6, 19);
            this.udpLocalPort.Maximum = new decimal(new int[] {
            65536,
            0,
            0,
            0});
            this.udpLocalPort.Name = "udpLocalPort";
            this.udpLocalPort.Size = new System.Drawing.Size(120, 20);
            this.udpLocalPort.TabIndex = 1;
            this.udpLocalPort.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tabPageSshClient
            // 
            this.tabPageSshClient.Controls.Add(this.groupBox11);
            this.tabPageSshClient.Controls.Add(this.groupBox10);
            this.tabPageSshClient.Controls.Add(this.groupBox1);
            this.tabPageSshClient.Location = new System.Drawing.Point(4, 22);
            this.tabPageSshClient.Name = "tabPageSshClient";
            this.tabPageSshClient.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSshClient.Size = new System.Drawing.Size(674, 350);
            this.tabPageSshClient.TabIndex = 3;
            this.tabPageSshClient.Text = "SSH client";
            this.tabPageSshClient.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.sshClientPassword);
            this.groupBox11.Location = new System.Drawing.Point(6, 132);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(189, 57);
            this.groupBox11.TabIndex = 4;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Password";
            // 
            // sshClientPassword
            // 
            this.sshClientPassword.Location = new System.Drawing.Point(6, 19);
            this.sshClientPassword.Name = "sshClientPassword";
            this.sshClientPassword.Size = new System.Drawing.Size(174, 20);
            this.sshClientPassword.TabIndex = 0;
            this.sshClientPassword.UseSystemPasswordChar = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.sshClientUser);
            this.groupBox10.Location = new System.Drawing.Point(6, 69);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(189, 57);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "User name";
            // 
            // sshClientUser
            // 
            this.sshClientUser.Location = new System.Drawing.Point(6, 19);
            this.sshClientUser.Name = "sshClientUser";
            this.sshClientUser.Size = new System.Drawing.Size(174, 20);
            this.sshClientUser.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.sshClientHost);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(662, 57);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Host";
            // 
            // sshClientHost
            // 
            this.sshClientHost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sshClientHost.Location = new System.Drawing.Point(6, 19);
            this.sshClientHost.Name = "sshClientHost";
            this.sshClientHost.Size = new System.Drawing.Size(650, 20);
            this.sshClientHost.TabIndex = 0;
            // 
            // tabPageBluetooth
            // 
            this.tabPageBluetooth.Controls.Add(this.btAddBluetoothDevice);
            this.tabPageBluetooth.Controls.Add(this.groupBox19);
            this.tabPageBluetooth.Controls.Add(this.groupBox18);
            this.tabPageBluetooth.Location = new System.Drawing.Point(4, 22);
            this.tabPageBluetooth.Name = "tabPageBluetooth";
            this.tabPageBluetooth.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBluetooth.Size = new System.Drawing.Size(674, 350);
            this.tabPageBluetooth.TabIndex = 5;
            this.tabPageBluetooth.Text = "Bluetooth";
            this.tabPageBluetooth.UseVisualStyleBackColor = true;
            // 
            // btAddBluetoothDevice
            // 
            this.btAddBluetoothDevice.Location = new System.Drawing.Point(12, 54);
            this.btAddBluetoothDevice.Name = "btAddBluetoothDevice";
            this.btAddBluetoothDevice.Size = new System.Drawing.Size(100, 23);
            this.btAddBluetoothDevice.TabIndex = 2;
            this.btAddBluetoothDevice.Text = "Add new device";
            this.btAddBluetoothDevice.UseVisualStyleBackColor = true;
            this.btAddBluetoothDevice.Click += new System.EventHandler(this.btAddBluetoothDevice_Click);
            // 
            // groupBox19
            // 
            this.groupBox19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox19.Controls.Add(this.tbBluetoothPin);
            this.groupBox19.Location = new System.Drawing.Point(6, 89);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(662, 58);
            this.groupBox19.TabIndex = 1;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "PIN";
            // 
            // tbBluetoothPin
            // 
            this.tbBluetoothPin.Location = new System.Drawing.Point(6, 19);
            this.tbBluetoothPin.Name = "tbBluetoothPin";
            this.tbBluetoothPin.Size = new System.Drawing.Size(100, 20);
            this.tbBluetoothPin.TabIndex = 0;
            // 
            // groupBox18
            // 
            this.groupBox18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox18.Controls.Add(this.lbSelectedBluetoothDevice);
            this.groupBox18.Controls.Add(this.btSelectBluetoothDevice);
            this.groupBox18.Location = new System.Drawing.Point(6, 6);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(662, 77);
            this.groupBox18.TabIndex = 0;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Device";
            // 
            // lbSelectedBluetoothDevice
            // 
            this.lbSelectedBluetoothDevice.AutoSize = true;
            this.lbSelectedBluetoothDevice.Location = new System.Drawing.Point(115, 24);
            this.lbSelectedBluetoothDevice.Name = "lbSelectedBluetoothDevice";
            this.lbSelectedBluetoothDevice.Size = new System.Drawing.Size(67, 13);
            this.lbSelectedBluetoothDevice.TabIndex = 1;
            this.lbSelectedBluetoothDevice.Text = "Not selected";
            // 
            // btSelectBluetoothDevice
            // 
            this.btSelectBluetoothDevice.Location = new System.Drawing.Point(6, 19);
            this.btSelectBluetoothDevice.Name = "btSelectBluetoothDevice";
            this.btSelectBluetoothDevice.Size = new System.Drawing.Size(103, 23);
            this.btSelectBluetoothDevice.TabIndex = 0;
            this.btSelectBluetoothDevice.Text = "Select Device";
            this.btSelectBluetoothDevice.UseVisualStyleBackColor = true;
            this.btSelectBluetoothDevice.Click += new System.EventHandler(this.btSelectBluetoothDevice_Click);
            // 
            // tabPageEmulator
            // 
            this.tabPageEmulator.Controls.Add(this.emulatorHexEditor);
            this.tabPageEmulator.Controls.Add(this.btBrowseForFile);
            this.tabPageEmulator.Controls.Add(this.tbEmulatorFileName);
            this.tabPageEmulator.Controls.Add(this.rbEmulatorFromBytes);
            this.tabPageEmulator.Controls.Add(this.rbEmulatorFromFile);
            this.tabPageEmulator.Location = new System.Drawing.Point(4, 22);
            this.tabPageEmulator.Name = "tabPageEmulator";
            this.tabPageEmulator.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEmulator.Size = new System.Drawing.Size(674, 350);
            this.tabPageEmulator.TabIndex = 6;
            this.tabPageEmulator.Text = "Emulator";
            this.tabPageEmulator.UseVisualStyleBackColor = true;
            // 
            // emulatorHexEditor
            // 
            this.emulatorHexEditor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.emulatorHexEditor.ByteProvider = null;
            this.emulatorHexEditor.Location = new System.Drawing.Point(6, 60);
            this.emulatorHexEditor.Name = "emulatorHexEditor";
            this.emulatorHexEditor.ReadOnly = false;
            this.emulatorHexEditor.Size = new System.Drawing.Size(662, 284);
            this.emulatorHexEditor.TabIndex = 4;
            // 
            // btBrowseForFile
            // 
            this.btBrowseForFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btBrowseForFile.Location = new System.Drawing.Point(593, 11);
            this.btBrowseForFile.Name = "btBrowseForFile";
            this.btBrowseForFile.Size = new System.Drawing.Size(75, 23);
            this.btBrowseForFile.TabIndex = 3;
            this.btBrowseForFile.Text = "Browse...";
            this.btBrowseForFile.UseVisualStyleBackColor = true;
            this.btBrowseForFile.Click += new System.EventHandler(this.btBrowseForFile_Click);
            // 
            // tbEmulatorFileName
            // 
            this.tbEmulatorFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEmulatorFileName.Location = new System.Drawing.Point(118, 13);
            this.tbEmulatorFileName.Name = "tbEmulatorFileName";
            this.tbEmulatorFileName.Size = new System.Drawing.Size(469, 20);
            this.tbEmulatorFileName.TabIndex = 2;
            // 
            // rbEmulatorFromBytes
            // 
            this.rbEmulatorFromBytes.AutoSize = true;
            this.rbEmulatorFromBytes.Location = new System.Drawing.Point(6, 37);
            this.rbEmulatorFromBytes.Name = "rbEmulatorFromBytes";
            this.rbEmulatorFromBytes.Size = new System.Drawing.Size(114, 17);
            this.rbEmulatorFromBytes.TabIndex = 1;
            this.rbEmulatorFromBytes.TabStop = true;
            this.rbEmulatorFromBytes.Text = "Configure hex data";
            this.rbEmulatorFromBytes.UseVisualStyleBackColor = true;
            this.rbEmulatorFromBytes.CheckedChanged += new System.EventHandler(this.rbEmulatorFromFile_CheckedChanged);
            // 
            // rbEmulatorFromFile
            // 
            this.rbEmulatorFromFile.AutoSize = true;
            this.rbEmulatorFromFile.Location = new System.Drawing.Point(6, 14);
            this.rbEmulatorFromFile.Name = "rbEmulatorFromFile";
            this.rbEmulatorFromFile.Size = new System.Drawing.Size(88, 17);
            this.rbEmulatorFromFile.TabIndex = 0;
            this.rbEmulatorFromFile.TabStop = true;
            this.rbEmulatorFromFile.Text = "Load from file";
            this.rbEmulatorFromFile.UseVisualStyleBackColor = true;
            this.rbEmulatorFromFile.CheckedChanged += new System.EventHandler(this.rbEmulatorFromFile_CheckedChanged);
            // 
            // serialPortListRefreshTimer
            // 
            this.serialPortListRefreshTimer.Enabled = true;
            this.serialPortListRefreshTimer.Interval = 5000;
            this.serialPortListRefreshTimer.Tick += new System.EventHandler(this.serialPortListRefreshTimer_Tick);
            // 
            // groupBox14
            // 
            this.groupBox14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox14.Controls.Add(this.cbDiscardDataWithoutParsers);
            this.groupBox14.Location = new System.Drawing.Point(3, 382);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(662, 56);
            this.groupBox14.TabIndex = 9;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Common";
            // 
            // cbDiscardDataWithoutParsers
            // 
            this.cbDiscardDataWithoutParsers.AutoSize = true;
            this.cbDiscardDataWithoutParsers.Location = new System.Drawing.Point(6, 19);
            this.cbDiscardDataWithoutParsers.Name = "cbDiscardDataWithoutParsers";
            this.cbDiscardDataWithoutParsers.Size = new System.Drawing.Size(248, 17);
            this.cbDiscardDataWithoutParsers.TabIndex = 0;
            this.cbDiscardDataWithoutParsers.Text = "Discard received data when no parser is active";
            this.cbDiscardDataWithoutParsers.UseVisualStyleBackColor = true;
            // 
            // groupBox17
            // 
            this.groupBox17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox17.Controls.Add(this.tcpClientKeepAlive);
            this.groupBox17.Location = new System.Drawing.Point(6, 132);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(662, 57);
            this.groupBox17.TabIndex = 1;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Options";
            // 
            // tcpClientKeepAlive
            // 
            this.tcpClientKeepAlive.AutoSize = true;
            this.tcpClientKeepAlive.Location = new System.Drawing.Point(6, 19);
            this.tcpClientKeepAlive.Name = "tcpClientKeepAlive";
            this.tcpClientKeepAlive.Size = new System.Drawing.Size(76, 17);
            this.tcpClientKeepAlive.TabIndex = 0;
            this.tcpClientKeepAlive.Text = "Keep alive";
            this.tcpClientKeepAlive.UseVisualStyleBackColor = true;
            // 
            // groupBox22
            // 
            this.groupBox22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox22.Controls.Add(this.tcpServerKeepAlive);
            this.groupBox22.Location = new System.Drawing.Point(5, 190);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(662, 57);
            this.groupBox22.TabIndex = 4;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Options";
            // 
            // tcpServerKeepAlive
            // 
            this.tcpServerKeepAlive.AutoSize = true;
            this.tcpServerKeepAlive.Location = new System.Drawing.Point(6, 19);
            this.tcpServerKeepAlive.Name = "tcpServerKeepAlive";
            this.tcpServerKeepAlive.Size = new System.Drawing.Size(76, 17);
            this.tcpServerKeepAlive.TabIndex = 0;
            this.tcpServerKeepAlive.Text = "Keep alive";
            this.tcpServerKeepAlive.UseVisualStyleBackColor = true;
            // 
            // SerialPortSettingsEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox14);
            this.Controls.Add(this.tabs);
            this.MinimumSize = new System.Drawing.Size(682, 441);
            this.Name = "SerialPortSettingsEditor";
            this.Size = new System.Drawing.Size(682, 441);
            this.tabPageTcpIpServer.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tcpServerConnectTimeout)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tcpServerPort)).EndInit();
            this.tabPageTcpIpClient.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.group.ResumeLayout(false);
            this.group.PerformLayout();
            this.tabs.ResumeLayout(false);
            this.tabPageSerial.ResumeLayout(false);
            this.grPortName.ResumeLayout(false);
            this.grDtrEnable.ResumeLayout(false);
            this.grDtrEnable.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tabPageUdpSocket.ResumeLayout(false);
            this.tabPageUdpSocket.PerformLayout();
            this.groupRemote.ResumeLayout(false);
            this.groupRemote.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.udpRemotePort)).EndInit();
            this.groupBox13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.udpLocalPort)).EndInit();
            this.tabPageSshClient.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPageBluetooth.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.tabPageEmulator.ResumeLayout(false);
            this.tabPageEmulator.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPageTcpIpServer;
        private System.Windows.Forms.TabPage tabPageTcpIpClient;
        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage tabPageSerial;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton rbRtsOff;
        private System.Windows.Forms.RadioButton rbRtsNoChange;
        private System.Windows.Forms.RadioButton rbRtsOn;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ListBox stopBits;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ListBox parity;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListBox handshake;
        private System.Windows.Forms.CheckBox dtrEnable;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox dataBits;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox baudRate;
        private System.Windows.Forms.GroupBox grPortName;
        private System.Windows.Forms.ListBox portName;
        private System.Windows.Forms.GroupBox groupBox9;
        private yats.Gui.Winforms.Components.Util.TextBoxAdv tcpClientAddress;
        private System.Windows.Forms.GroupBox group;
        private yats.Gui.Winforms.Components.Util.TextBoxAdv tcpClientPort;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox grDtrEnable;
        private System.Windows.Forms.TabPage tabPageSshClient;
        private System.Windows.Forms.GroupBox groupBox11;
        private yats.Gui.Winforms.Components.Util.TextBoxAdv sshClientPassword;
        private System.Windows.Forms.GroupBox groupBox10;
        private yats.Gui.Winforms.Components.Util.TextBoxAdv sshClientUser;
        private System.Windows.Forms.GroupBox groupBox1;
        private yats.Gui.Winforms.Components.Util.TextBoxAdv sshClientHost;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.NumericUpDown tcpServerPort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown tcpServerConnectTimeout;
        private System.Windows.Forms.TabPage tabPageUdpSocket;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.NumericUpDown udpLocalPort;
        private IPAddressControlLib.IPAddressControl udpIpUnicastRemoteAddress;
        private System.Windows.Forms.Timer serialPortListRefreshTimer;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.CheckBox tcpServerAcceptLaterConnections;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupRemote;
        private System.Windows.Forms.RadioButton udpOverwritePort;
        private System.Windows.Forms.RadioButton udpUnicast;
        private System.Windows.Forms.TabPage tabPageBluetooth;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.TextBox tbBluetoothPin;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Button btSelectBluetoothDevice;
        private System.Windows.Forms.Label lbSelectedBluetoothDevice;
        private System.Windows.Forms.Button btAddBluetoothDevice;
        private System.Windows.Forms.GroupBox groupBox21;
        private IPAddressControlLib.IPAddressControl udpIpBroadcastRemoteAddress;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.NumericUpDown udpRemotePort;
        private System.Windows.Forms.RadioButton udpBroadcast;
        private System.Windows.Forms.RadioButton udpBroadcastIpCustom;
        private System.Windows.Forms.RadioButton udpBroadcastIpDefault;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.CheckBox cbDiscardDataWithoutParsers;
        private System.Windows.Forms.TabPage tabPageEmulator;
        private Util.ExtendedHexEditor emulatorHexEditor;
        private System.Windows.Forms.Button btBrowseForFile;
        private System.Windows.Forms.TextBox tbEmulatorFileName;
        private System.Windows.Forms.RadioButton rbEmulatorFromBytes;
        private System.Windows.Forms.RadioButton rbEmulatorFromFile;
        private System.Windows.Forms.RadioButton udpConfigureRemote;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.CheckBox tcpClientKeepAlive;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.CheckBox tcpServerKeepAlive;
    }
}
