﻿namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    partial class RandomIntegerEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.minValue = new System.Windows.Forms.NumericUpDown();
            this.maxValue = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelSize = new System.Windows.Forms.Label();
            this.labelValue = new System.Windows.Forms.Label();
            this.labelMs1 = new System.Windows.Forms.Label();
            this.labelMs2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.minValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxValue)).BeginInit();
            this.SuspendLayout();
            // 
            // minValue
            // 
            this.minValue.Location = new System.Drawing.Point(111, 30);
            this.minValue.Name = "minValue";
            this.minValue.Size = new System.Drawing.Size(144, 20);
            this.minValue.TabIndex = 1;
            // 
            // maxValue
            // 
            this.maxValue.Location = new System.Drawing.Point(111, 56);
            this.maxValue.Name = "maxValue";
            this.maxValue.Size = new System.Drawing.Size(144, 20);
            this.maxValue.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Minimum (inclusive)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Maximum (inclusive)";
            // 
            // labelSize
            // 
            this.labelSize.AutoSize = true;
            this.labelSize.Location = new System.Drawing.Point(7, 10);
            this.labelSize.Name = "labelSize";
            this.labelSize.Size = new System.Drawing.Size(93, 13);
            this.labelSize.TabIndex = 5;
            this.labelSize.Text = "Buffer size (bytes):";
            this.labelSize.Visible = false;
            // 
            // labelValue
            // 
            this.labelValue.AutoSize = true;
            this.labelValue.Location = new System.Drawing.Point(7, 10);
            this.labelValue.Name = "labelValue";
            this.labelValue.Size = new System.Drawing.Size(104, 13);
            this.labelValue.TabIndex = 6;
            this.labelValue.Text = "Random value limits:";
            this.labelValue.Visible = false;
            // 
            // labelMs1
            // 
            this.labelMs1.AutoSize = true;
            this.labelMs1.Location = new System.Drawing.Point(261, 32);
            this.labelMs1.Name = "labelMs1";
            this.labelMs1.Size = new System.Drawing.Size(20, 13);
            this.labelMs1.TabIndex = 7;
            this.labelMs1.Text = "ms";
            this.labelMs1.Visible = false;
            // 
            // labelMs2
            // 
            this.labelMs2.AutoSize = true;
            this.labelMs2.Location = new System.Drawing.Point(261, 58);
            this.labelMs2.Name = "labelMs2";
            this.labelMs2.Size = new System.Drawing.Size(20, 13);
            this.labelMs2.TabIndex = 8;
            this.labelMs2.Text = "ms";
            this.labelMs2.Visible = false;
            // 
            // RandomIntegerEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelMs2);
            this.Controls.Add(this.labelMs1);
            this.Controls.Add(this.labelValue);
            this.Controls.Add(this.labelSize);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.maxValue);
            this.Controls.Add(this.minValue);
            this.MinimumSize = new System.Drawing.Size(271, 58);
            this.Name = "RandomIntegerEditor";
            this.Size = new System.Drawing.Size(282, 81);
            ((System.ComponentModel.ISupportInitialize)(this.minValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxValue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown minValue;
        private System.Windows.Forms.NumericUpDown maxValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelSize;
        private System.Windows.Forms.Label labelValue;
        private System.Windows.Forms.Label labelMs1;
        private System.Windows.Forms.Label labelMs2;
    }
}
