﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using yats.TestCase.Parameter;
using System.Collections.Generic;

namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    public partial class EnumEditor : UserControl, IEditor
    {
        [DebuggerStepThrough]
        public EnumEditor()
        {
            InitializeComponent();
        }

        #region IEditor Members
        EnumParameterValue param;

        private class ListBoxItem
        {
            public object Value { get; set; }
            public string Display { get; set; }
        }

        public void ParameterToEditor(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate () { ParameterToEditor(parameter); }));
            }
            else
            {
                param = parameter as EnumParameterValue;
                var enumType = Type.GetType(param.EnumTypeName);
                var underlyingType = Enum.GetUnderlyingType(enumType);
                var enumValues = Enum.GetValues(Type.GetType(param.EnumTypeName));
                // using System.Convert to underlying type because previous cast to int fails e.g. when enum is defined as byte  
                var cc = enumValues.OfType<object>().Select(x => new ListBoxItem { Value = x, Display = string.Format("{0}  [ {1}\u2081\u2080 0x{2:X}\u2081\u2086 ]", x, System.Convert.ChangeType(x, underlyingType), System.Convert.ChangeType(x, underlyingType)) }).ToList();
                listBox.DataSource = cc;
                listBox.DisplayMember = "Display";
                listBox.ValueMember = "Value";
                listBox.SelectedItem = cc.FirstOrDefault(x => x.Value.ToString() == param.Value);
            }
        }

        public void EditorToParameter(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameter); }));
            }
            else
            {
                if (listBox.SelectedItem == null)
                {
                    throw new Exception("Not selected");
                }
                EnumParameterValue param = parameter as EnumParameterValue;
                param.Value = (listBox.SelectedItem as ListBoxItem).Value.ToString();
            }
        }
        
        [DebuggerStepThrough]
        public string TabName()
        {
            return "Enumeration";
        }

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(typeof(EnumParameterValue), this.GetType(), 10);
        }

        public event EventHandler OnConfirm;

        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }

        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
		{
			if (validatingForSave && listBox.SelectedItem == null) {
				throw new Exception ("No value is selected");
			}
		}

        #endregion

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listBox.SelectedItem != null)
            {
                Clipboard.SetText(listBox.SelectedItem.ToString());
            }
        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            copyToolStripMenuItem.Enabled = (listBox.SelectedItem != null);
        }

        private void listBox_DoubleClick(object sender, EventArgs e)
        {
            if ((listBox.SelectedItem != null) && this.OnConfirm != null)
            {
                OnConfirm(sender, e);
            }
        }

        private void listBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Control && (char)e.KeyValue == 'F')
            {
                // ctrl+f
                if (searchPanel.Visible)
                {
                    HideSearchField();
                }
                else
                {
                    ShowSearchField();
                }
                e.Handled = true;
            }
            else if (!e.Control && !e.Alt)
            {
                if (e.KeyCode == Keys.Escape)//TODO this key is not caught. Parameter editor handles ESC to close the editor first.
                {
                    HideSearchField();
                }
                else
                {
                    ShowSearchField();
                    tbSearch.Text += ((char)e.KeyValue).ToString().ToLower();
                    tbSearch.SelectionStart = tbSearch.Text.Length;
                    tbSearch.DeselectAll();
                }
                e.Handled = true;
            }
        }

        private void HideSearchField()
        {
            tbSearch.Text = "";
            searchPanel.Visible = false;
            listBox.Focus();
        }

        private void ShowSearchField()
        {
            searchPanel.Visible = true;
            tbSearch.Focus();
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbSearch.Text.Trim()))
            {
                listBox.DataSource = Enum.GetValues(Type.GetType(param.EnumTypeName));
                return;
            }
            string[] search = tbSearch.Text.Trim().ToLower().Split(' ');
            Array allValues = Enum.GetValues(Type.GetType(param.EnumTypeName));
            List<Object> result = new List<object>();
            var enumType = Type.GetType(param.EnumTypeName);
            var underlyingType = Enum.GetUnderlyingType(enumType);
            var y = Enum.GetValues(Type.GetType(param.EnumTypeName));
            // keep the same listview format when search is done
            var cc = y.OfType<object>().Where(v => search.Where(s => v.ToString().ToLower().Contains(s)).Count() > 0).Select(x => new ListBoxItem { Value = x, Display = string.Format("{0}  [ {1}\u2081\u2080 0x{2:X}\u2081\u2086 ]", x, System.Convert.ChangeType(x, underlyingType), System.Convert.ChangeType(x, underlyingType)) }).ToList();
            listBox.DataSource = cc;            
        }
    }
}
