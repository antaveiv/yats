﻿namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    partial class TimespanEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TimespanEditor));
            this.numDays = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numHours = new System.Windows.Forms.NumericUpDown();
            this.numMin = new System.Windows.Forms.NumericUpDown();
            this.numSec = new System.Windows.Forms.NumericUpDown();
            this.numMs = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.trackBar = new gTrackBar.gTrackBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.valueLabel = new System.Windows.Forms.Label();
            this.groupBoxSign = new System.Windows.Forms.GroupBox();
            this.rbPositive = new System.Windows.Forms.RadioButton();
            this.rbNegative = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.numDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMs)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBoxSign.SuspendLayout();
            this.SuspendLayout();
            // 
            // numDays
            // 
            this.numDays.Location = new System.Drawing.Point(10, 22);
            this.numDays.Maximum = new decimal(new int[] {
            365,
            0,
            0,
            0});
            this.numDays.Name = "numDays";
            this.numDays.Size = new System.Drawing.Size(66, 20);
            this.numDays.TabIndex = 0;
            this.numDays.ValueChanged += new System.EventHandler(this.editorUpDown_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(82, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "days";
            // 
            // numHours
            // 
            this.numHours.Location = new System.Drawing.Point(129, 22);
            this.numHours.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.numHours.Name = "numHours";
            this.numHours.Size = new System.Drawing.Size(46, 20);
            this.numHours.TabIndex = 2;
            this.numHours.ValueChanged += new System.EventHandler(this.editorUpDown_ValueChanged);
            // 
            // numMin
            // 
            this.numMin.Location = new System.Drawing.Point(202, 22);
            this.numMin.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numMin.Name = "numMin";
            this.numMin.Size = new System.Drawing.Size(46, 20);
            this.numMin.TabIndex = 3;
            this.numMin.ValueChanged += new System.EventHandler(this.editorUpDown_ValueChanged);
            // 
            // numSec
            // 
            this.numSec.Location = new System.Drawing.Point(276, 22);
            this.numSec.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numSec.Name = "numSec";
            this.numSec.Size = new System.Drawing.Size(46, 20);
            this.numSec.TabIndex = 4;
            this.numSec.ValueChanged += new System.EventHandler(this.editorUpDown_ValueChanged);
            // 
            // numMs
            // 
            this.numMs.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMs.Location = new System.Drawing.Point(379, 22);
            this.numMs.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numMs.Name = "numMs";
            this.numMs.Size = new System.Drawing.Size(66, 20);
            this.numMs.TabIndex = 5;
            this.numMs.ValueChanged += new System.EventHandler(this.editorUpDown_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(181, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "H";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(254, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "M";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(328, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "S";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(451, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "ms";
            // 
            // trackBar
            // 
            this.trackBar.AButColorA = System.Drawing.Color.CornflowerBlue;
            this.trackBar.AButColorB = System.Drawing.Color.Lavender;
            this.trackBar.AButColorBorder = System.Drawing.Color.SteelBlue;
            this.trackBar.ArrowColorDown = System.Drawing.Color.GhostWhite;
            this.trackBar.ArrowColorHover = System.Drawing.Color.DarkBlue;
            this.trackBar.ArrowColorUp = System.Drawing.Color.LightSteelBlue;
            this.trackBar.BackColor = System.Drawing.SystemColors.Control;
            this.trackBar.BorderColor = System.Drawing.Color.Black;
            this.trackBar.BorderShow = false;
            this.trackBar.BrushDirection = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.trackBar.BrushStyle = gTrackBar.gTrackBar.eBrushStyle.Path;
            this.trackBar.ChangeLarge = 10;
            this.trackBar.ChangeSmall = 1;
            this.trackBar.ColorDown = System.Drawing.SystemColors.ControlDark;
            this.trackBar.ColorDownBorder = System.Drawing.SystemColors.ControlDark;
            this.trackBar.ColorDownHiLt = System.Drawing.SystemColors.ControlDark;
            this.trackBar.ColorHover = System.Drawing.SystemColors.ControlDarkDark;
            this.trackBar.ColorHoverBorder = System.Drawing.SystemColors.ControlDarkDark;
            this.trackBar.ColorHoverHiLt = System.Drawing.SystemColors.ControlDarkDark;
            this.trackBar.ColorUp = System.Drawing.SystemColors.ControlDarkDark;
            this.trackBar.ColorUpBorder = System.Drawing.SystemColors.ControlDarkDark;
            this.trackBar.ColorUpHiLt = System.Drawing.SystemColors.ControlDarkDark;
            this.trackBar.FloatValue = false;
            this.trackBar.FloatValueFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.trackBar.FloatValueFontColor = System.Drawing.Color.SlateGray;
            this.trackBar.Label = null;
            this.trackBar.LabelAlighnment = System.Drawing.StringAlignment.Near;
            this.trackBar.LabelColor = System.Drawing.Color.MediumBlue;
            this.trackBar.LabelFont = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.trackBar.LabelPadding = new System.Windows.Forms.Padding(3);
            this.trackBar.LabelShow = false;
            this.trackBar.Location = new System.Drawing.Point(5, 19);
            this.trackBar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.trackBar.MaxValue = 11;
            this.trackBar.MinValue = 0;
            this.trackBar.Name = "trackBar";
            this.trackBar.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.trackBar.ShowFocus = false;
            this.trackBar.Size = new System.Drawing.Size(332, 29);
            this.trackBar.SliderCapEnd = System.Drawing.Drawing2D.LineCap.Round;
            this.trackBar.SliderCapStart = System.Drawing.Drawing2D.LineCap.Round;
            this.trackBar.SliderColorHigh = System.Drawing.Color.DarkGray;
            this.trackBar.SliderColorLow = System.Drawing.Color.Red;
            this.trackBar.SliderFocalPt = ((System.Drawing.PointF)(resources.GetObject("trackBar.SliderFocalPt")));
            this.trackBar.SliderHighlightPt = ((System.Drawing.PointF)(resources.GetObject("trackBar.SliderHighlightPt")));
            this.trackBar.SliderShape = gTrackBar.gTrackBar.eShape.Rectangle;
            this.trackBar.SliderSize = new System.Drawing.Size(20, 10);
            this.trackBar.SliderWidth = 1;
            this.trackBar.TabIndex = 11;
            this.trackBar.TickColor = System.Drawing.Color.DarkGray;
            this.trackBar.TickInterval = 1;
            this.trackBar.TickType = gTrackBar.gTrackBar.eTickType.Both;
            this.trackBar.TickWidth = 5;
            this.trackBar.UpDownAutoWidth = true;
            this.trackBar.UpDownWidth = 30;
            this.trackBar.Value = 0;
            this.trackBar.ValueBox = gTrackBar.gTrackBar.eValueBox.None;
            this.trackBar.ValueBoxBackColor = System.Drawing.Color.White;
            this.trackBar.ValueBoxBorder = System.Drawing.Color.MediumBlue;
            this.trackBar.ValueBoxFont = new System.Drawing.Font("Arial", 8.25F);
            this.trackBar.ValueBoxFontColor = System.Drawing.Color.MediumBlue;
            this.trackBar.ValueBoxShape = gTrackBar.gTrackBar.eShape.Rectangle;
            this.trackBar.ValueBoxSize = new System.Drawing.Size(30, 20);
            this.trackBar.ValueChanged += new gTrackBar.gTrackBar.ValueChangedEventHandler(this.trackBar_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.trackBar);
            this.groupBox1.Location = new System.Drawing.Point(3, 117);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(342, 58);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Preset";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numHours);
            this.groupBox2.Controls.Add(this.numDays);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.numMin);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.numSec);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.numMs);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(3, 58);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(530, 53);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Detailed";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.valueLabel);
            this.groupBox3.Location = new System.Drawing.Point(351, 117);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(182, 58);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Value";
            // 
            // valueLabel
            // 
            this.valueLabel.AutoSize = true;
            this.valueLabel.Location = new System.Drawing.Point(6, 19);
            this.valueLabel.Name = "valueLabel";
            this.valueLabel.Size = new System.Drawing.Size(0, 13);
            this.valueLabel.TabIndex = 0;
            // 
            // groupBoxSign
            // 
            this.groupBoxSign.Controls.Add(this.rbNegative);
            this.groupBoxSign.Controls.Add(this.rbPositive);
            this.groupBoxSign.Location = new System.Drawing.Point(3, 3);
            this.groupBoxSign.Name = "groupBoxSign";
            this.groupBoxSign.Size = new System.Drawing.Size(530, 49);
            this.groupBoxSign.TabIndex = 15;
            this.groupBoxSign.TabStop = false;
            this.groupBoxSign.Text = "Timespan sign";
            // 
            // rbPositive
            // 
            this.rbPositive.Location = new System.Drawing.Point(6, 19);
            this.rbPositive.Name = "rbPositive";
            this.rbPositive.Size = new System.Drawing.Size(70, 24);
            this.rbPositive.TabIndex = 0;
            this.rbPositive.TabStop = true;
            this.rbPositive.Text = "&Positive";
            this.rbPositive.UseVisualStyleBackColor = true;
			this.rbPositive.CheckedChanged += new System.EventHandler(this.editorUpDown_ValueChanged);
            // 
            // rbNegative
            // 
            this.rbNegative.AutoSize = true;
            this.rbNegative.Location = new System.Drawing.Point(97, 23);
            this.rbNegative.Name = "rbNegative";
            this.rbNegative.Size = new System.Drawing.Size(68, 17);
            this.rbNegative.TabIndex = 1;
            this.rbNegative.TabStop = true;
            this.rbNegative.Text = "&Negative";
            this.rbNegative.UseVisualStyleBackColor = true;
			this.rbNegative.CheckedChanged += new System.EventHandler(this.editorUpDown_ValueChanged);
            // 
            // TimespanEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxSign);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MinimumSize = new System.Drawing.Size(543, 184);
            this.Name = "TimespanEditor";
            this.Size = new System.Drawing.Size(543, 184);
            ((System.ComponentModel.ISupportInitialize)(this.numDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMs)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBoxSign.ResumeLayout(false);
            this.groupBoxSign.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numDays;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numHours;
        private System.Windows.Forms.NumericUpDown numMin;
        private System.Windows.Forms.NumericUpDown numSec;
        private System.Windows.Forms.NumericUpDown numMs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private gTrackBar.gTrackBar trackBar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label valueLabel;
        private System.Windows.Forms.GroupBox groupBoxSign;
        private System.Windows.Forms.RadioButton rbNegative;
        private System.Windows.Forms.RadioButton rbPositive;
    }
}
