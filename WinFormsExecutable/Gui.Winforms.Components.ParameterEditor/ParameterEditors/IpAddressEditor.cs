﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Net;
using System.Windows.Forms;
using yats.TestCase.Parameter;
using yats.Utilities;

namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    public partial class IpAddressEditor : UserControl, IEditor
    {
        public IpAddressEditor()
        {
            InitializeComponent();
        }                

        public void ParameterToEditor(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ParameterToEditor(parameter); }));
            }
            else
            {
                IPAddressParameterValue param = parameter as IPAddressParameterValue;
                if (param != null)
                {
                    if (param.IP != null)
                    {
                        ipAddress.SetAddressBytes(param.IP.GetAddressBytes());
                    }
                }
            }
        }

        public void EditorToParameter(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameter); }));
            }
            else
            {
                (parameter as IPAddressParameterValue).IP = new SerializableIPAddress(ipAddress.GetAddressBytes());
            }
        }
        
        public string TabName()
        {
            return "IP address";
        }

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(typeof(IPAddressParameterValue), this.GetType(), 10);
        }

        public event EventHandler OnConfirm;
        
        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }

        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
		{
			byte [] editorBytes = ipAddress.GetAddressBytes();
			var tmp = new IPAddress(editorBytes);
		}

        public void Set(IPAddress ip)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { Set(ip); }));
                return;
            }
            ipAddress.SetAddressBytes(ip.GetAddressBytes());
        }
    }
}
