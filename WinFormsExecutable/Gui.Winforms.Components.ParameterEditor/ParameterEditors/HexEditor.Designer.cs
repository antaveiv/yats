﻿namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    partial class HexEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.hexBox = new yats.Gui.Winforms.Components.Util.ExtendedHexEditor();
            this.SuspendLayout();
            // 
            // hexBox
            // 
            this.hexBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hexBox.Location = new System.Drawing.Point(0, 0);
            this.hexBox.Name = "hexBox";
            this.hexBox.ReadOnly = false;
            this.hexBox.Size = new System.Drawing.Size(639, 202);
            this.hexBox.TabIndex = 0;
            // 
            // HexEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.hexBox);
            this.MinimumSize = new System.Drawing.Size(639, 202);
            this.Name = "HexEditor";
            this.Size = new System.Drawing.Size(639, 202);
            this.ResumeLayout(false);

        }

        #endregion

        private yats.Gui.Winforms.Components.Util.ExtendedHexEditor hexBox;

    }
}
