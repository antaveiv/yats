﻿namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    partial class BoolEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.valueCB = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // valueCB
            // 
            this.valueCB.AutoSize = true;
            this.valueCB.Location = new System.Drawing.Point(3, 3);
            this.valueCB.Name = "valueCB";
            this.valueCB.Size = new System.Drawing.Size(53, 17);
            this.valueCB.TabIndex = 0;
            this.valueCB.Text = "Value";
            this.valueCB.UseVisualStyleBackColor = true;
            // 
            // BoolEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.valueCB);
            this.MinimumSize = new System.Drawing.Size(247, 30);
            this.Name = "BoolEditor";
            this.Size = new System.Drawing.Size(247, 30);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox valueCB;
    }
}
