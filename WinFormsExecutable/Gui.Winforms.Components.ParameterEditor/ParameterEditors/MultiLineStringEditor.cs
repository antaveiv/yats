﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.Windows.Forms;
using yats.TestCase.Parameter;

namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    public partial class MultiLineStringEditor : UserControl, IEditor
    {
        protected bool edited = false;

        [DebuggerStepThrough]
        public MultiLineStringEditor()
        {
            InitializeComponent();
            textBox.TextChanged += textBox_TextChanged;
        }

        #region IEditor Members

        public void ParameterToEditor(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ParameterToEditor(parameter); }));
            }
            else
            {
                if (parameter is SimpleStringValue)
                {
                    SetFromString((parameter as SimpleStringValue).Value);
                }
                if (parameter is ByteArrayValue)
                {
                    if ((parameter as ByteArrayValue).Value == null)
                    {
                        SetFromString("");
                    }
                    else
                    {
                        SetFromString(Utilities.ByteArray.ToString((parameter as ByteArrayValue).Value));
                    }
                }
            }
        }

        public void EditorToParameter(AbstractParameterValue parameter)
        {
            if (!edited)
            {
                // trying to solve a problem: a byte array is showed in hex editor. It has bytes that do not translate to ASCII well. When switching to string editor and back to hex editor, the hex is changed even if no changes were done in string editor.
                return;
            }
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameter); }));
            }
            else
            {
                if (parameter is SimpleStringValue)
                {
                    (parameter as SimpleStringValue).Value = EditorToString();
                }

                if (parameter is ByteArrayValue)
                {
                    (parameter as ByteArrayValue).Value = Utilities.ByteArray.ToArray(EditorToString());
                }
            }
        }


        [DebuggerStepThrough]
        public string TabName()
        {
            return "Multi-line";
        }

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(typeof(ByteArrayValue), this.GetType(), 2);
            parameterTypes.RegisterEditorType(typeof(SimpleStringValue), this.GetType(), 2);
        }

        public event EventHandler OnConfirm;

        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }

        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
        {
        }

        #endregion

        protected void SetFromString(string value)
        {
            textBox.TextChanged -= textBox_TextChanged; // disable temporarily
            edited = false;
            if (string.IsNullOrEmpty(value))
            {
                textBox.Text = string.Empty;
                rbNone.Checked = true;
                textBox.TextChanged += textBox_TextChanged;
                return;
            }

            if (value.Contains("\r\n"))
            {
                rbCrLf.Checked = true;
                if (value.EndsWith("\r\n"))
                {
                    value = value.Substring(0, value.Length - 2);
                    cbNewLineToLastLine.Checked = true;
                }
                textBox.Lines = value.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            }
            else if (value.Contains("\r"))
            {
                rbCR.Checked = true;
                if (value.EndsWith("\r"))
                {
                    value = value.Substring(0, value.Length - 1);
                    cbNewLineToLastLine.Checked = true;
                }
                textBox.Lines = value.Split(new string[] { "\r" }, StringSplitOptions.None);
            }
            else if (value.Contains("\n"))
            {
                rbLF.Checked = true;
                if (value.EndsWith("\n"))
                {
                    value = value.Substring(0, value.Length - 1);
                    cbNewLineToLastLine.Checked = true;
                }
                textBox.Lines = value.Split(new string[] { "\n" }, StringSplitOptions.None);
            }
            else
            {
                rbNone.Checked = true;
                textBox.Text = value;
            }
            textBox.TextChanged += textBox_TextChanged;
        }

        void textBox_TextChanged(object sender, EventArgs e)
        {
            edited = true;
        }

        protected string EditorToString()
        {
            string value = string.Empty;

            if (rbNone.Checked)
            {
                value = string.Join("", textBox.Lines);
            }
            else if (rbCR.Checked)
            {
                value = string.Join("\r", textBox.Lines);
                if (cbNewLineToLastLine.Checked)
                {
                    value += "\r";
                }
            }
            else if (rbCrLf.Checked)
            {
                value = string.Join("\r\n", textBox.Lines);
                if (cbNewLineToLastLine.Checked)
                {
                    value += "\r\n";
                }
            }
            else if (rbLF.Checked)
            {
                value = string.Join("\n", textBox.Lines);
                if (cbNewLineToLastLine.Checked)
                {
                    value += "\n";
                }
            }

            return value;
        }

        private void lineEndingRadioChanged(object sender, EventArgs e)
        {
            edited = true;
        }
    }
}
