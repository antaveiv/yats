﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.Windows.Forms;
using yats.TestCase.Parameter;

namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    public partial class RandomIntegerEditor  : UserControl, IEditor
    {
        [DebuggerStepThrough]
        public RandomIntegerEditor()
        {
            InitializeComponent();
            minValue.Minimum = decimal.MinValue;
            minValue.Maximum = decimal.MaxValue;
        }

        #region IEditor Members

        public void ParameterToEditor(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ParameterToEditor(parameter); }));
            }
            else
            {
                if (parameter is RandomIntegerParameterValue)
                {
                    RandomIntegerParameterValue param = parameter as RandomIntegerParameterValue;
                    
                    minValue.Minimum = param.Min;
                    minValue.Maximum = param.Max;
                    minValue.Value = param.UserMinimum;
                                        
                    maxValue.Minimum = param.Min;
                    maxValue.Maximum = param.Max;
                    maxValue.Value = param.UserMaximum;
                    labelValue.Visible = true;
                }
                else if (parameter is RandomByteArrayValue)
                {
                    RandomByteArrayValue param = parameter as RandomByteArrayValue;

                    minValue.Minimum = 0;
                    minValue.Maximum = 1024*1024;
                    minValue.Value = param.MinSize;

                    maxValue.Minimum = 0;
                    maxValue.Maximum = 1024*1024;
                    maxValue.Value = param.MaxSize;
                    labelSize.Visible = true;
                }
                else if (parameter is RandomTimespanValue)
                {
                    RandomTimespanValue param = parameter as RandomTimespanValue;
                    
                    minValue.Minimum = 0;
                    minValue.Maximum = 1000 * 60 * 60 * 24;
                    minValue.Value = param.MinMs;

                    maxValue.Minimum = 0;
                    maxValue.Maximum = 1000 * 60 * 60 * 24;
                    maxValue.Value = param.MaxMs;
                    labelMs1.Visible = true;
                    labelMs2.Visible = true;
                }
            }
        }

        public void EditorToParameter(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameter); }));
            }
            else
            {
                if (parameter is RandomIntegerParameterValue)
                {
                    RandomIntegerParameterValue param = parameter as RandomIntegerParameterValue;
                    param.UserMinimum = (int)minValue.Value;
                    param.UserMaximum = (int)maxValue.Value;
                }
                else if (parameter is RandomByteArrayValue)
                {
                    RandomByteArrayValue param = parameter as RandomByteArrayValue;
                    param.MinSize = (int)minValue.Value;
                    param.MaxSize = (int)maxValue.Value;
                }
                else if (parameter is RandomTimespanValue)
                {
                    RandomTimespanValue param = parameter as RandomTimespanValue;
                    param.MinMs = (int)minValue.Value;
                    param.MaxMs = (int)maxValue.Value;
                }
            }
        }
        
        [DebuggerStepThrough]
        public string TabName()
        {
            return "Random";
        }

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(typeof(RandomIntegerParameterValue), this.GetType(), 9);
            parameterTypes.RegisterEditorType(typeof(RandomByteArrayValue), this.GetType(), 9);
            parameterTypes.RegisterEditorType(typeof(RandomTimespanValue), this.GetType(), 50);
        }

        public event EventHandler OnConfirm;

        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }

        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
		{
			if (minValue.Validate() == false) {
				throw new Exception ("Invalid minimum value");
			}
            if (maxValue.Validate() == false)
            {
                throw new Exception("Invalid maximum value");
            }
            if (maxValue.Value < minValue.Value)
            {
                throw new Exception("Maximum value should be greater than minimum");
            }
		}

        #endregion
    }
}
