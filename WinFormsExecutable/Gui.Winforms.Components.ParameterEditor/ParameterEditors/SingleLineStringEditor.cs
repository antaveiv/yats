﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.Windows.Forms;
using yats.TestCase.Parameter;

namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    public partial class SingleLineStringEditor : UserControl, IEditor
    {
        protected bool edited = false;
        [DebuggerStepThrough]
        public SingleLineStringEditor()
        {
            InitializeComponent();
            textBox.TextChanged += textBox_TextChanged;
        }

        #region IEditor Members

        public void ParameterToEditor(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ParameterToEditor(parameter); }));
            }
            else
            {
                if (parameter is SimpleStringValue)
                {
                    SetFromString((parameter as SimpleStringValue).Value);
                }

                if (parameter is ByteArrayValue)
                {
                    if ((parameter as ByteArrayValue).Value == null)
                    {
                        SetFromString("");
                    }
                    else
                    {
                        SetFromString(Utilities.ByteArray.ToString((parameter as ByteArrayValue).Value));
                    }
                }
            }
        }

        public void EditorToParameter(AbstractParameterValue parameter)
        {
            if (!edited)
            {
                // trying to solve a problem: a byte array is showed in hex editor. It has bytes that do not translate to ASCII well. When switching to string editor and back to hex editor, the hex is changed even if no changes were done in string editor.
                return;
            }
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameter); }));
            }
            else
            {
                if (parameter is SimpleStringValue)
                {
                    (parameter as SimpleStringValue).Value  = EditorToString();
                }

                if (parameter is ByteArrayValue)
                {
                    (parameter as ByteArrayValue).Value = Utilities.ByteArray.ToArray(EditorToString());
                }
            }
        }

        [DebuggerStepThrough]
        public string TabName()
        {
            return "Single line";
        }

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(typeof(SimpleStringValue), this.GetType(), 1);
            parameterTypes.RegisterEditorType(typeof(ByteArrayValue), this.GetType(), 0);
            //parameterTypes.RegisterEditorType(typeof(RegexParameterValue), this.GetType(), 2);
        }

        public event EventHandler OnConfirm;

        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }

        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
        {
        }

        #endregion
        
        protected void SetFromString(string value)
        {
            textBox.TextChanged -= textBox_TextChanged; // disable temporarily
            edited = false;
            if (string.IsNullOrEmpty(value))
            {
                textBox.Text = string.Empty;
                rbNone.Checked = true;
                textBox.TextChanged += textBox_TextChanged;
                return;
            }

            bool crFound = false, lfFound = false;
            string stripped = string.Empty;

            foreach (char c in value)
            {
                switch (c)
                {
                    case '\r': crFound = true; break;
                    case '\n': lfFound = true; break;
                    default: stripped += c; break;
                }
            }

            textBox.Text = stripped;
            rbNone.Checked = (!crFound && !lfFound);
            rbCR.Checked = (crFound && !lfFound);
            rbCrLf.Checked = (crFound && lfFound);
            rbLF.Checked = (!crFound && lfFound);
            textBox.TextChanged += textBox_TextChanged;
        }

        void textBox_TextChanged(object sender, EventArgs e)
        {
            edited = true;
        }

        protected string EditorToString()
        {
            string stripped = textBox.Text.Trim(new char[] { '\r', '\n' });
            if (rbCR.Checked)
            {
                stripped += "\r";
            }
            else if (rbCrLf.Checked)
            {
                stripped += "\r\n";
            }
            else if (rbLF.Checked)
            {
                stripped += "\n";
            }

            return stripped;
        }

        private void lineEndingRadioChanged(object sender, EventArgs e)
        {
            edited = true;
        }
    }
}
