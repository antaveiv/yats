﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.Windows.Forms;
using yats.TestCase.Parameter;
using yats.Utilities;

namespace yats.Gui.Winforms.Components.ParameterEditor.ParameterEditors
{
    public partial class IntegerHexEditor : UserControl, IEditor
    {
        public Label[] binLabels = new Label[64];//public for unit tests

        [DebuggerStepThrough]
        public IntegerHexEditor()
        {
            InitializeComponent();
            binLabels[0] = this.lbBin00;
            binLabels[1] = this.lbBin01;
            binLabels[2] = this.lbBin02;
            binLabels[3] = this.lbBin03;
            binLabels[4] = this.lbBin04;
            binLabels[5] = this.lbBin05;
            binLabels[6] = this.lbBin06;
            binLabels[7] = this.lbBin07;
            binLabels[8] = this.lbBin08;
            binLabels[9] = this.lbBin09;

            binLabels[10] = this.lbBin10;
            binLabels[11] = this.lbBin11;
            binLabels[12] = this.lbBin12;
            binLabels[13] = this.lbBin13;
            binLabels[14] = this.lbBin14;
            binLabels[15] = this.lbBin15;
            binLabels[16] = this.lbBin16;
            binLabels[17] = this.lbBin17;
            binLabels[18] = this.lbBin18;
            binLabels[19] = this.lbBin19;

            binLabels[20] = this.lbBin20;
            binLabels[21] = this.lbBin21;
            binLabels[22] = this.lbBin22;
            binLabels[23] = this.lbBin23;
            binLabels[24] = this.lbBin24;
            binLabels[25] = this.lbBin25;
            binLabels[26] = this.lbBin26;
            binLabels[27] = this.lbBin27;
            binLabels[28] = this.lbBin28;
            binLabels[29] = this.lbBin29;

            binLabels[30] = this.lbBin30;
            binLabels[31] = this.lbBin31;
            binLabels[32] = this.lbBin32;
            binLabels[33] = this.lbBin33;
            binLabels[34] = this.lbBin34;
            binLabels[35] = this.lbBin35;
            binLabels[36] = this.lbBin36;
            binLabels[37] = this.lbBin37;
            binLabels[38] = this.lbBin38;
            binLabels[39] = this.lbBin39;

            binLabels[40] = this.lbBin40;
            binLabels[41] = this.lbBin41;
            binLabels[42] = this.lbBin42;
            binLabels[43] = this.lbBin43;
            binLabels[44] = this.lbBin44;
            binLabels[45] = this.lbBin45;
            binLabels[46] = this.lbBin46;
            binLabels[47] = this.lbBin47;
            binLabels[48] = this.lbBin48;
            binLabels[49] = this.lbBin49;

            binLabels[50] = this.lbBin50;
            binLabels[51] = this.lbBin51;
            binLabels[52] = this.lbBin52;
            binLabels[53] = this.lbBin53;
            binLabels[54] = this.lbBin54;
            binLabels[55] = this.lbBin55;
            binLabels[56] = this.lbBin56;
            binLabels[57] = this.lbBin57;
            binLabels[58] = this.lbBin58;
            binLabels[59] = this.lbBin59;

            binLabels[60] = this.lbBin60;
            binLabels[61] = this.lbBin61;
            binLabels[62] = this.lbBin62;
            binLabels[63] = this.lbBin63;
        }

        Decimal Minimum;
        Decimal Maximum;
        Decimal DecimalValue;
        public int numBits;

        #region IEditor Members

        public void ParameterToEditor(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ParameterToEditor(parameter); }));
            }
            else
            {
                IntegerParameterValue param = parameter as IntegerParameterValue;
                this.Minimum = param.Min;
                this.lbMin.Text = param.Min.ToString();
                this.Maximum = param.Max;
                this.lbMax.Text = param.Max.ToString();
                this.DecimalValue = param.DecimalValue;
                numBits = (int)Math.Ceiling(Math.Log((double)(Maximum - Minimum), 2));
                UpdateDisplay(true, true, true);
            }
        }

        public void EditorToParameter(AbstractParameterValue parameter)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EditorToParameter(parameter); }));
            }
            else
            {
                IntegerParameterValue param = parameter as IntegerParameterValue;
                param.DecimalValue = DecimalValue;
            }
        }

        void UpdateDisplay(bool updateDec, bool updateHex, bool updateBin)
        {
            //unregister event handlers
            tbDecimal.TextChanged -= tbDecimal_TextChanged;
            tbHex.TextChanged -= tbHex_TextChanged;
            for (int i = 0; i < binLabels.Length; i++)
            {
                binLabels[i].Click -= new EventHandler(bitClick);
            } 

            //DecimalValue = -1024;
            if (updateDec)
            {
                this.tbDecimal.Text = DecimalValue.ToString();
            }

            if (updateHex)
            {
                this.tbHex.Text = DecimalValue.ToHexString(numBits, Minimum < 0);
            }

            if (updateBin)
            {
                int i = 0;
                for (i = numBits; i < binLabels.Length; i++)
                {
                    binLabels[i].Text = "0";
                    binLabels[i].Enabled = false;
                }
                //TODO: TwosComplement is probably wrong approach
                int[] bits = Decimal.GetBits(DecimalValue.TwosComplement(numBits));
                i = 0;
                while(i < numBits - 1)
                {
                    binLabels[i].Text = (bits[i / 32] & (1 << (i % 32))) == 0 ? "0" : "1";
                    binLabels[i].Enabled = true;
                    i++;
                }
                // sign bit
                if (Minimum < 0)
                {
                    binLabels[i].Text = (DecimalValue >= 0) ? "0" : "1";
                }
                else
                {
                    binLabels[i].Text = (bits[i / 32] & (1 << (i % 32))) == 0 ? "0" : "1";
                }
                binLabels[i].Enabled = true;
            }

            //register event handlers
            tbDecimal.TextChanged += tbDecimal_TextChanged;
            tbHex.TextChanged += tbHex_TextChanged;
            for (int i = 0; i < binLabels.Length; i++)
            {
                binLabels[i].Click += new EventHandler(bitClick);
            }            
        }                       

        void tbDecimal_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DecimalValue = DecimalHelper.ParseString(tbDecimal.Text, numBits, Minimum < 0, System.Globalization.NumberStyles.Integer);
                UpdateDisplay(false, true, true);
            }
            catch (Exception)
            {
            }
        }

        void tbHex_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DecimalValue = DecimalHelper.ParseString(tbHex.Text, numBits, Minimum < 0, System.Globalization.NumberStyles.HexNumber);
                UpdateDisplay(true, false, true);
            }
            catch (Exception)
            {
            }
        }

        void bitClick(object sender, EventArgs e)
        {
            int bitIndex = Array.IndexOf(binLabels, sender);
            binLabels[bitIndex].Text = (binLabels[bitIndex].Text == "0") ? "1" : "0"; // invert bit label
            string asString = "";
            for (int i = 0; i < numBits / 4; i++)
            {
                byte qval = 0;
                if (binLabels[i * 4 + 0].Text == "1")
                {
                    qval += 1;
                }
                if (binLabels[i * 4 + 1].Text == "1")
                {
                    qval += 2;
                }
                if (binLabels[i * 4 + 2].Text == "1")
                {
                    qval += 4;
                }
                if (binLabels[i * 4 + 3].Text == "1")
                {
                    qval += 8;
                }
                asString = qval.ToString("X") + asString;
            }
            DecimalValue = DecimalHelper.ParseString(asString, numBits, Minimum < 0, System.Globalization.NumberStyles.HexNumber);

            UpdateDisplay(true, true, false);
        }

        [DebuggerStepThrough]
        public string TabName()
        {
            return "Hex";
        }

        public void RegisterEditorType(IParameterTypeCollection parameterTypes)
        {
            parameterTypes.RegisterEditorType(typeof(IntegerParameterValue), this.GetType(), 2);
        }

        public event EventHandler OnConfirm;
        
        public void RaiseConfirm()
        {
            if (OnConfirm != null)
            {
                OnConfirm(this, null);
            }
        }

        public void ValidateEditor(AbstractParameterValue parameter, bool validatingForSave)
		{
            DecimalHelper.ParseString(tbDecimal.Text, numBits, Minimum < 0, System.Globalization.NumberStyles.Integer);
            DecimalHelper.ParseString(tbHex.Text, numBits, Minimum < 0, System.Globalization.NumberStyles.HexNumber);
		}

        #endregion
    }
}
