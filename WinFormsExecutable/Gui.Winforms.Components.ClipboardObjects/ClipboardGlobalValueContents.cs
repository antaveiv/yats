﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows;
using yats.TestCase.Parameter;
using System;

namespace yats.Gui.Winforms.Components.ClipboardObjects
{
    [Serializable]
    public class ClipboardGlobalValueContents
    {
        protected string serializedGlobals;

        public List<GlobalValue> GetGlobalValues()
        {
            return yats.Utilities.SerializationHelper.Deserialize<List<GlobalValue>>(serializedGlobals) ?? new List<GlobalValue>();
        }

        public ClipboardGlobalValueContents()
        {
        }

        public ClipboardGlobalValueContents(List<GlobalValue> globalValues)
        {
            //clipboard uses binary serialization that TestStep does not support. It's easier to reuse the existing XML serialization and let the Clipboard handle a single string
            this.serializedGlobals = yats.Utilities.SerializationHelper.SerializeToString<List<GlobalValue>>(globalValues);
        }

        public void CopyToClipboard()
        {
#if !__MonoCS__
            //register my custom data format with Windows or get it if it's already registered
            DataFormat format = DataFormats.GetDataFormat(this.GetType().FullName);

            //now copy to clipboard
            IDataObject dataObj = new DataObject();
            dataObj.SetData(format.Name, this);
            Clipboard.Clear();
            try
            {
                Clipboard.SetDataObject(dataObj, true);
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                Clipboard.SetDataObject(dataObj, true);
            }
#endif
        }

        public static ClipboardGlobalValueContents GetFromClipboard()
        {
            ClipboardGlobalValueContents result = null;
#if !__MonoCS__
            IDataObject dataObj = null;
            string format = typeof(ClipboardGlobalValueContents).FullName;
            try
            {
                dataObj = Clipboard.GetDataObject();
            }
            catch (COMException)
            {
                dataObj = Clipboard.GetDataObject();
            }

            if (dataObj != null && dataObj.GetDataPresent(format))
            {
                result = (dataObj.GetData(format) as ClipboardGlobalValueContents);
            }
#endif
            return result;
        }

        public static bool ClipboardContainsItems()
        {
#if !__MonoCS__
            IDataObject dataObj = Clipboard.GetDataObject();
            string format = typeof(ClipboardGlobalValueContents).FullName;

            return (dataObj.GetDataPresent(format));
#else
			return false;
#endif
        }
    }
}
