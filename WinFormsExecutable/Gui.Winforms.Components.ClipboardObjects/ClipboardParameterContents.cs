﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;

#if !__MonoCS__
using System.Windows;
#endif
using yats.TestCase.Parameter;
using System.Runtime.InteropServices;

namespace yats.Gui.Winforms.Components.ClipboardObjects
{
    [Serializable]
    public class ClipboardParameterContents
    {
        protected string serializedParameters;
        protected string serializedGlobals;

        public List<IParameter> GetParameters()
        {
            return yats.Utilities.SerializationHelper.Deserialize<List<IParameter>>(serializedParameters) ?? new List<IParameter>();
        }

        public IGlobalParameterCollection GetGlobals()
        {
            return yats.Utilities.SerializationHelper.Deserialize<IGlobalParameterCollection>(serializedGlobals);
        }

        public ClipboardParameterContents()
        {
        }

        public ClipboardParameterContents(List<IParameter> parameters, IGlobalParameterCollection globals)
        {
            //clipboard uses binary serialization that TestStep does not support. It's easier to reuse the existing XML serialization and let the Clipboard handle a single string
            this.serializedParameters = yats.Utilities.SerializationHelper.SerializeToString<List<IParameter>>(parameters);
            this.serializedGlobals = yats.Utilities.SerializationHelper.SerializeToString<IGlobalParameterCollection>(globals);
        }

        public void CopyToClipboard()
        {
#if !__MonoCS__
            //register my custom data format with Windows or get it if it's already registered
            DataFormat format = DataFormats.GetDataFormat(this.GetType().FullName);

            //now copy to clipboard
            IDataObject dataObj = new DataObject();
            dataObj.SetData(format.Name, this);
            Clipboard.Clear();
            try
            {
                Clipboard.SetDataObject(dataObj, true);
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                Clipboard.SetDataObject(dataObj, true);
            }
#endif
        }

        public static ClipboardParameterContents GetFromClipboard()
        {
            ClipboardParameterContents result = null;
#if !__MonoCS__
            IDataObject dataObj = null;
            string format = typeof(ClipboardParameterContents).FullName;
            try
            {
                dataObj = Clipboard.GetDataObject();
            }
            catch (COMException)
            {
                dataObj = Clipboard.GetDataObject();
            }            

            if (dataObj !=null && dataObj.GetDataPresent(format))
            {
                result = (dataObj.GetData(format) as ClipboardParameterContents);
            }
#endif
            return result;
        }

        public static bool ClipboardContainsItems()
        {
#if !__MonoCS__
            IDataObject dataObj = Clipboard.GetDataObject();
            string format = typeof(ClipboardParameterContents).FullName;

            return (dataObj.GetDataPresent(format));
#else
			return false;
#endif
        }
    }
}
