﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using yats.TestRepositoryManager.Interface;
#if !__MonoCS__
using System.Windows;
#endif

namespace yats.Gui.Winforms.Components.ClipboardObjects
{
    [Serializable]
    public class ClipboardTestCaseContents
    {
        protected List<string> uniqueTestCaseIds;
        public List<string> UniqueTestCaseIds
        {
            get
            {
                return uniqueTestCaseIds;
            }
            set
            {
                uniqueTestCaseIds = value;
            }
        }

        public ClipboardTestCaseContents()
        {
        }

        public ClipboardTestCaseContents(List<string> uniqueTestCaseIds)
        {
            this.uniqueTestCaseIds = uniqueTestCaseIds;
        }

        public ClipboardTestCaseContents(List<ITestCaseInfo> testCaseInfoList)
        {
            this.uniqueTestCaseIds = testCaseInfoList.Select(x => x.RepositoryManager.GetUniqueTestId(x.TestCase)).ToList();
        }

        public void CopyToClipboard()
        {
#if !__MonoCS__
            //register my custom data format with Windows or get it if it's already registered
            DataFormat format = DataFormats.GetDataFormat(this.GetType().FullName);

            //now copy to clipboard
            IDataObject dataObj = new DataObject();
            dataObj.SetData(format.Name, this);
            Clipboard.Clear();
            try
            {
                Clipboard.SetDataObject(dataObj, true);
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                Clipboard.SetDataObject(dataObj, true);
            }
#endif
        }

        public static ClipboardTestCaseContents GetFromClipboard()
        {
            ClipboardTestCaseContents result = null;
#if !__MonoCS__
            IDataObject dataObj = null;
            string format = typeof(ClipboardTestCaseContents).FullName;

            try
            {
                dataObj = Clipboard.GetDataObject();
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                dataObj = Clipboard.GetDataObject();
            }

            if (dataObj != null && dataObj.GetDataPresent(format))
            {
                result = dataObj.GetData(format) as ClipboardTestCaseContents;
            }
#endif
            return result;
        }
    }
}
