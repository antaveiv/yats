﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Windows.Forms;
using yats.ExecutionQueue;

namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    public partial class TestStepSelectionDialog : Form
    {
        public TestStepSelectionDialog()
        {
            InitializeComponent();
        }

        public static List<TestStep> Select(List<TestStep> allSteps, List<TestStep> currentSelection)
        {
            TestStepSelectionDialog dialog = new TestStepSelectionDialog();
            foreach (var s in allSteps){
                dialog.listBox.Items.Add(s.Name, currentSelection.Contains(s));
            }

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                var result = new List<TestStep>();
                foreach (var idx in dialog.listBox.CheckedIndices)
                {
                    result.Add(allSteps[(int)idx]);
                }
                return result;
            }
            return currentSelection;
        }
    }
}
