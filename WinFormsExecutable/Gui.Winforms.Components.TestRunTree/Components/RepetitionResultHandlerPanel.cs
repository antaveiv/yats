﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using yats.ExecutionQueue.ResultHandler;
using yats.ExecutionQueue;
using yats.TestRun.Commands;
using yats.Utilities.Commands;

namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    public partial class RepetitionResultHandlerPanel : UserControl
    {
        public RepetitionResultHandlerPanel()
        {
            InitializeComponent();
        }
        
        internal void DisplayModelValues<T>(List<T> steps) where T :yats.ExecutionQueue.TestStep
        {
            HashSet<Type> types = new HashSet<Type>();
            foreach (var step in steps)
            {
                types.Add(step.RepetitionResultHandler.GetType());
            }
            this.rbBestCaseRepetitionResultHandler.Checked = (types.Count == 1) && types.Contains(typeof(BestCaseRepetitionResultHandler));
            this.rbDefaultRepetitionResultHandler.Checked = (types.Count == 1) && types.Contains(typeof(DefaultRepetitionResultHandler));
            this.rbWorstCaseRepetitionResultHandler.Checked = (types.Count == 1) && types.Contains(typeof(WorstCaseRepetitionResultHandler));
        }
        
        internal ICommand GetConfigChanges<T>(List<T> steps) where T : TestStep
        {
            IRepetitionResultHandler handler;
            if (this.rbBestCaseRepetitionResultHandler.Checked)
            {
                handler = new BestCaseRepetitionResultHandler();
            }
            else if (this.rbDefaultRepetitionResultHandler.Checked) 
            {
                handler = new DefaultRepetitionResultHandler();
            }
            else if (this.rbWorstCaseRepetitionResultHandler.Checked)
            {
                handler = new WorstCaseRepetitionResultHandler();
            }
            else
            {
                //TODO review message texts
                throw new Exception("Repetition result handler is not selected");
            }
            return new ChangeRepetitionResultHandler<T>(handler, steps, null);
        }
    }
}
