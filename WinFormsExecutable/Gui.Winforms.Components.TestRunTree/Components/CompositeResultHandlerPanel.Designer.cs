﻿namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    partial class CompositeResultHandlerPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btSelectSteps = new System.Windows.Forms.Button();
            this.rbWorstCaseSelectedResultHandler = new System.Windows.Forms.RadioButton();
            this.rbWorstCaseCompositeResultHandler = new System.Windows.Forms.RadioButton();
            this.rbDefaultCompositeResultHandler = new System.Windows.Forms.RadioButton();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btSelectSteps);
            this.groupBox5.Controls.Add(this.rbWorstCaseSelectedResultHandler);
            this.groupBox5.Controls.Add(this.rbWorstCaseCompositeResultHandler);
            this.groupBox5.Controls.Add(this.rbDefaultCompositeResultHandler);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(457, 87);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Single run result interpretation";
            // 
            // btSelectSteps
            // 
            this.btSelectSteps.Location = new System.Drawing.Point(219, 61);
            this.btSelectSteps.Name = "btSelectSteps";
            this.btSelectSteps.Size = new System.Drawing.Size(75, 23);
            this.btSelectSteps.TabIndex = 3;
            this.btSelectSteps.Text = "Configure...";
            this.btSelectSteps.UseVisualStyleBackColor = true;
            this.btSelectSteps.Click += new System.EventHandler(this.btSelectSteps_Click);
            // 
            // rbWorstCaseSelectedResultHandler
            // 
            this.rbWorstCaseSelectedResultHandler.AutoSize = true;
            this.rbWorstCaseSelectedResultHandler.Location = new System.Drawing.Point(6, 64);
            this.rbWorstCaseSelectedResultHandler.Name = "rbWorstCaseSelectedResultHandler";
            this.rbWorstCaseSelectedResultHandler.Size = new System.Drawing.Size(207, 17);
            this.rbWorstCaseSelectedResultHandler.TabIndex = 2;
            this.rbWorstCaseSelectedResultHandler.Text = "Take the worst of selected step results";
            this.rbWorstCaseSelectedResultHandler.UseVisualStyleBackColor = true;
            // 
            // rbWorstCaseCompositeResultHandler
            // 
            this.rbWorstCaseCompositeResultHandler.AutoSize = true;
            this.rbWorstCaseCompositeResultHandler.Location = new System.Drawing.Point(6, 42);
            this.rbWorstCaseCompositeResultHandler.Name = "rbWorstCaseCompositeResultHandler";
            this.rbWorstCaseCompositeResultHandler.Size = new System.Drawing.Size(214, 17);
            this.rbWorstCaseCompositeResultHandler.TabIndex = 1;
            this.rbWorstCaseCompositeResultHandler.Text = "Take the worst of all internal step results";
            this.rbWorstCaseCompositeResultHandler.UseVisualStyleBackColor = true;
            // 
            // rbDefaultCompositeResultHandler
            // 
            this.rbDefaultCompositeResultHandler.AutoSize = true;
            this.rbDefaultCompositeResultHandler.Checked = true;
            this.rbDefaultCompositeResultHandler.Location = new System.Drawing.Point(6, 19);
            this.rbDefaultCompositeResultHandler.Name = "rbDefaultCompositeResultHandler";
            this.rbDefaultCompositeResultHandler.Size = new System.Drawing.Size(422, 17);
            this.rbDefaultCompositeResultHandler.TabIndex = 0;
            this.rbDefaultCompositeResultHandler.TabStop = true;
            this.rbDefaultCompositeResultHandler.Text = "If there were no NOT RUN or CANCELED internal step results, set this step to PASS" +
                "";
            this.rbDefaultCompositeResultHandler.UseVisualStyleBackColor = true;
            // 
            // CompositeResultHandlerPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox5);
            this.Name = "CompositeResultHandlerPanel";
            this.Size = new System.Drawing.Size(457, 87);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rbWorstCaseCompositeResultHandler;
        private System.Windows.Forms.RadioButton rbDefaultCompositeResultHandler;
        private System.Windows.Forms.RadioButton rbWorstCaseSelectedResultHandler;
        private System.Windows.Forms.Button btSelectSteps;
    }
}
