﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Windows.Forms;
using yats.ExecutionQueue;

namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    public partial class StateMachineConfigurationForm : Form
    {
        TestStepComposite test;
        StateMachineExecutionModifier sm;

        public StateMachineConfigurationForm()
        {
            InitializeComponent();
        }

        public StateMachineConfigurationForm(TestStepComposite test, StateMachineExecutionModifier sm):this()
        {
            this.test = test;
            this.sm = sm;
            Changed(true);//will call smConfig.SetModel
            smEditor.StructureChanged += smConfig_StructureChanged;
            smEditor.PropertiesChanged += smConfig_PropertiesChanged;
        }

        void smConfig_StructureChanged(object sender, EventArgs e)
        {
            Changed(true);
        }

        void smConfig_PropertiesChanged(object sender, EventArgs e)
        {
            Changed(false);
        }

        private void Changed(bool structure)
        {
            bool isModified = false;
            try
            {
                StateMachineSanityCheckVisitor.CheckStepMachine(test, sm, out isModified);
                errorLabel.Visible = false;
            }
            catch (Exception ex)
            {
                errorLabel.Text = ex.Message;
                errorLabel.Visible = true;
            }

            if (structure)
            {
                smEditor.SetModel(test, sm); // update view
            }
        }

        internal static bool Edit(TestStepComposite test, StateMachineExecutionModifier sm)
        {            
            StateMachineConfigurationForm dlg = new StateMachineConfigurationForm(test, sm);
            return dlg.ShowDialog() == DialogResult.OK;
        }
    }
}
