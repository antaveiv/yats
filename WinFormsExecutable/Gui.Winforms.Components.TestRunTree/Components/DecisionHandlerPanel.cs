﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using yats.ExecutionQueue.DecisionHandler;
using yats.TestCase.Interface;
using yats.TestRun.Commands;
using yats.Utilities;
using yats.ExecutionQueue;
using yats.Utilities.Commands;

namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    public partial class DecisionHandlerPanel : UserControl
    {
        public DecisionHandlerPanel()
        {
            InitializeComponent();
            cbListOfResults.DataSource = Enum.GetValues(typeof(ResultEnum));
        }

		bool initializing = false;
        internal void DisplayModelValues<T>(List<T> steps) where T : TestStep
        {
            initializing = true;
            
            HashSet<Type> types = new HashSet<Type>();
            foreach (var step in steps)
            {
                types.Add(step.DecisionHandler.GetType());
            }
            this.rbRepeatIgnoreFail.Checked = (types.Count == 1) && types.Contains(typeof(RepeatIgnoreFails));
            this.rbRepeatUntilCancel.Checked = (types.Count == 1) && types.Contains(typeof(RepeatUntilCancel));
            this.rbRepeatUntilConfigured.Checked = (types.Count == 1) && types.Contains(typeof(RepeatUntilConfiguredResult));
            this.rbSingleRun.Checked = (types.Count == 1) && types.Contains(typeof(SingleRun));
            this.rbWhilePass.Checked = (types.Count == 1) && types.Contains(typeof(RepeatWhilePass));

            numRepeatsIgnoreFails.Value = 1;
            numRepeatUntilConfigured.Value = 1;
            numWhilePass.Value = 1;

            if (this.rbRepeatIgnoreFail.Checked)
            {
                HashSet<int> values = new HashSet<int>();
                foreach (var step in steps)
                {
                    values.Add((step.DecisionHandler as RepeatIgnoreFails).Repetitions);
                }
                numRepeatsIgnoreFails.Value = values.Min();
            }
            else if (this.rbRepeatUntilConfigured.Checked)
            {
                HashSet<int> values = new HashSet<int>();
                Dictionary<ResultEnum, bool> commonResults = new Dictionary<ResultEnum, bool>();
                foreach (var val in Enum.GetValues(typeof(ResultEnum)))
                {
                    commonResults[(ResultEnum) val] = true;
                }
                foreach (var step in steps)
                {
                    values.Add((step.DecisionHandler as RepeatUntilConfiguredResult).Repetitions);
                    foreach (var val in Enum.GetValues(typeof(ResultEnum)))
                    {
                        if ((step.DecisionHandler as RepeatUntilConfiguredResult).WaitFor.Contains((ResultEnum)val) == false)
                        {
                            commonResults[(ResultEnum)val] = false;
                        }
                    }
                }
                foreach (var val in commonResults.Keys)
                {
                    cbListOfResults.SetItemChecked(cbListOfResults.Items.IndexOf(val), commonResults[val]);
                }
                numRepeatUntilConfigured.Value = values.Min();
            }
            else if (rbWhilePass.Checked)
            {
                HashSet<int> values = new HashSet<int>();
                foreach (var step in steps)
                {
                    values.Add((step.DecisionHandler as RepeatWhilePass).Repetitions);
                }
                numWhilePass.Value = values.Min();
            }

            initializing = false;
        }

        private void numRepeatsIgnoreFails_Click(object sender, EventArgs e)
        {
			if (initializing) return;
            rbRepeatIgnoreFail.Checked = true;
        }

        private void numWhilePass_Click(object sender, EventArgs e)
        {
			if (initializing) return;
            rbWhilePass.Checked = true;
        }

        private void numRepeatUntilConfigured_Click(object sender, EventArgs e)
        {
			if (initializing) return;
            rbRepeatUntilConfigured.Checked = true;
        }

        internal ICommand GetConfigChanges<T>(List<T> steps) where T : TestStep
        {
            IDecisionHandler handler;
            if (this.rbRepeatIgnoreFail.Checked)
                {
                    handler = new RepeatIgnoreFails((int)numRepeatsIgnoreFails.Value);
                }
                else if (this.rbRepeatUntilCancel.Checked)
                {
                    handler = new RepeatUntilCancel();
                }
                else if (this.rbRepeatUntilConfigured.Checked)
                {
                    List<ResultEnum> waitFor = new List<ResultEnum>(cbListOfResults.CheckedItems.Cast<ResultEnum>());
                    handler = new RepeatUntilConfiguredResult((int)numRepeatUntilConfigured.Value, waitFor);
                    if(waitFor.Count == 0)
                    {
                        throw new Exception("No result values are selected, execution will not stop" );
                    }
                }
                else if (this.rbSingleRun.Checked)
                {
                    handler = new SingleRun();
                }
                else if (this.rbWhilePass.Checked)
                {
                    handler = new RepeatWhilePass((int)numWhilePass.Value);
                }
                else
                {
                    throw new Exception("Repetition option is not selected");
                    //nothing checked - error
                }
            return new ChangeStepDecisionHandler<T>(handler, steps, null);
        }
    }
}
