﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using yats.ExecutionQueue;
using yats.TestRun.Commands;
using yats.Utilities.Commands;

namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    public partial class ExecutionMethodForSingleStepPanel : UserControl
    {
        public ExecutionMethodForSingleStepPanel()
        {
            InitializeComponent();
        }

        internal void DisplayModelValues(List<TestStepSingle> steps)
        {
            HashSet<Type> types = new HashSet<Type>();
            foreach (var step in steps)
            {
                types.Add(step.ExecutionMethod.GetType());
            }
            this.rbSequential.Checked = (types.Count == 1) && types.Contains(typeof(SequentialExecutionModifier));
            this.rbStepDisabled.Checked = (types.Count == 1) && types.Contains(typeof(StepDisabledModifier));
        }


        internal ICommand GetConfigChanges<T>(List<T> steps) where T : TestStepSingle
        {
            IStepExecution handler;
            if (this.rbSequential.Checked)
            {
                handler = new SequentialExecutionModifier();
            }
            else if (this.rbStepDisabled.Checked)
            {
                handler = new StepDisabledModifier();
            }
            else
            {
                throw new Exception("Execution method is not selected");
            }

            return new ChangeStepExecutionMethod<T>(handler, steps, null);
        }
    }
}
