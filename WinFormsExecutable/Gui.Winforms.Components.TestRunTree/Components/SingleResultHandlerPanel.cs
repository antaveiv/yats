﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using yats.ExecutionQueue.ResultHandler;
using yats.TestCase.Interface;
using yats.TestRun.Commands;
using yats.ExecutionQueue;
using yats.Utilities.Commands;

namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    public partial class SingleResultHandlerPanel : UserControl
    {
        public SingleResultHandlerPanel()
        {
            InitializeComponent();
        }

		bool initializing = false;
        internal void DisplayModelValues(List<yats.ExecutionQueue.TestStepSingle> steps)
        {
            initializing = true;
            
            HashSet<Type> types = new HashSet<Type>();
            foreach (var step in steps)
            {
                types.Add(step.SingleRunResultHandler.GetType());
            }
            this.rbCustomResultMapping.Checked = (types.Count == 1) && types.Contains(typeof(CustomResultMapping));
            this.rbDefaultSingleResultHandler.Checked = (types.Count == 1) && types.Contains(typeof(DefaultSingleResultHandler));
            this.rbIgnoreFailureSingleResultHandler.Checked = (types.Count == 1) && types.Contains(typeof(IgnoreFailureSingleResultHandler));

            cbCanceled.DataSource = Enum.GetValues(typeof(ResultEnum)); cbCanceled.SelectedItem = ResultEnum.CANCELED;
            cbFail.DataSource = Enum.GetValues(typeof(ResultEnum)); cbFail.SelectedItem = ResultEnum.FAIL;
            cbInconclusive.DataSource = Enum.GetValues(typeof(ResultEnum)); cbInconclusive.SelectedItem = ResultEnum.INCONCLUSIVE;
            cbNotRun.DataSource = Enum.GetValues(typeof(ResultEnum)); cbNotRun.SelectedItem = ResultEnum.NOT_RUN;
            cbPass.DataSource = Enum.GetValues(typeof(ResultEnum)); cbPass.SelectedItem = ResultEnum.PASS;

            if (this.rbCustomResultMapping.Checked)
            {
                HashSet<ResultEnum> valuesCanceled = new HashSet<ResultEnum>();
                HashSet<ResultEnum> valuesFail = new HashSet<ResultEnum>();
                HashSet<ResultEnum> valuesInconclusive = new HashSet<ResultEnum>();
                HashSet<ResultEnum> valuesNotRun = new HashSet<ResultEnum>();
                HashSet<ResultEnum> valuesPass = new HashSet<ResultEnum>();

                foreach (var step in steps)
                {
                    CustomResultMapping crm = step.SingleRunResultHandler as CustomResultMapping;
                    if (crm == null)
                    {
                        continue;
                    }
                    valuesCanceled.Add(crm.ValueCanceled);
                    valuesFail.Add(crm.ValueFail);
                    valuesInconclusive.Add(crm.ValueInconclusive);
                    valuesNotRun.Add(crm.ValueNotRun);
                    valuesPass.Add(crm.ValuePass);
                }

                if (valuesCanceled.Count == 1) cbCanceled.SelectedItem = valuesCanceled.First();
                if (valuesFail.Count == 1) cbFail.SelectedItem = valuesFail.First();
                if (valuesInconclusive.Count == 1) cbInconclusive.SelectedItem = valuesInconclusive.First();
                if (valuesNotRun.Count == 1) cbNotRun.SelectedItem = valuesNotRun.First();
                if (valuesPass.Count == 1) cbPass.SelectedItem = valuesPass.First(); 
            }
            
            initializing = false;
        }
        
        internal ICommand GetConfigChanges<T>(List<T> steps) where T : TestStepSingle
        {
            ISingleResultHandler handler;
            if (this.rbCustomResultMapping.Checked && cbCanceled.SelectedItem != null && cbFail.SelectedItem != null && cbInconclusive.SelectedItem != null && cbNotRun.SelectedItem != null && cbPass.SelectedItem != null)
            {
                handler = new CustomResultMapping(
                    (ResultEnum)cbCanceled.SelectedItem,
                    (ResultEnum)cbPass.SelectedItem,
                    (ResultEnum)cbFail.SelectedItem,
                    (ResultEnum)cbInconclusive.SelectedItem,
                    (ResultEnum)cbNotRun.SelectedItem);
            }
            else if (this.rbDefaultSingleResultHandler.Checked)
            {
                handler = new DefaultSingleResultHandler();
            }
            else if (this.rbIgnoreFailureSingleResultHandler.Checked)
            {
                handler = new IgnoreFailureSingleResultHandler();
            }
            else
            {
                throw new Exception("Result handler is not selected");
            }

            return new ChangeSingleResultHandler<T>(handler, steps, null);
        }

        private void cbPass_Click(object sender, EventArgs e)
        {
			if (initializing) return;
            rbCustomResultMapping.Checked = true;
        }
    }
}
