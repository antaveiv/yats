﻿namespace yats.Gui.Winforms.Components.TestRunTree
{
    partial class SingleStepPropertiesPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.decisionHandlerPanel = new yats.Gui.Winforms.Components.TestRunTree.Components.DecisionHandlerPanel();
            this.executionMethodForSingleStepPanel = new yats.Gui.Winforms.Components.TestRunTree.Components.ExecutionMethodForSingleStepPanel();
            this.singleResultHandlerPanel = new yats.Gui.Winforms.Components.TestRunTree.Components.SingleResultHandlerPanel();
            this.repetitionResultHandlerPanel = new yats.Gui.Winforms.Components.TestRunTree.Components.RepetitionResultHandlerPanel();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.decisionHandlerPanel);
            this.flowLayoutPanel1.Controls.Add(this.executionMethodForSingleStepPanel);
            this.flowLayoutPanel1.Controls.Add(this.singleResultHandlerPanel);
            this.flowLayoutPanel1.Controls.Add(this.repetitionResultHandlerPanel);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(664, 640);
            this.flowLayoutPanel1.TabIndex = 2;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // decisionHandlerPanel
            // 
            this.decisionHandlerPanel.Location = new System.Drawing.Point(3, 3);
            this.decisionHandlerPanel.Name = "decisionHandlerPanel";
            this.decisionHandlerPanel.Size = new System.Drawing.Size(653, 218);
            this.decisionHandlerPanel.TabIndex = 6;
            // 
            // executionMethodForSingleStepPanel
            // 
            this.executionMethodForSingleStepPanel.Location = new System.Drawing.Point(3, 227);
            this.executionMethodForSingleStepPanel.Name = "executionMethodForSingleStepPanel";
            this.executionMethodForSingleStepPanel.Size = new System.Drawing.Size(653, 68);
            this.executionMethodForSingleStepPanel.TabIndex = 9;
            // 
            // singleResultHandlerPanel
            // 
            this.singleResultHandlerPanel.Location = new System.Drawing.Point(3, 301);
            this.singleResultHandlerPanel.Name = "singleResultHandlerPanel";
            this.singleResultHandlerPanel.Size = new System.Drawing.Size(653, 234);
            this.singleResultHandlerPanel.TabIndex = 10;
            // 
            // repetitionResultHandlerPanel
            // 
            this.repetitionResultHandlerPanel.Location = new System.Drawing.Point(3, 541);
            this.repetitionResultHandlerPanel.Name = "repetitionResultHandlerPanel";
            this.repetitionResultHandlerPanel.Size = new System.Drawing.Size(653, 91);
            this.repetitionResultHandlerPanel.TabIndex = 11;
            // 
            // SingleStepPropertiesPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "SingleStepPropertiesPanel";
            this.Size = new System.Drawing.Size(664, 640);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private yats.Gui.Winforms.Components.TestRunTree.Components.DecisionHandlerPanel decisionHandlerPanel;
        private yats.Gui.Winforms.Components.TestRunTree.Components.ExecutionMethodForSingleStepPanel executionMethodForSingleStepPanel;
        private yats.Gui.Winforms.Components.TestRunTree.Components.SingleResultHandlerPanel singleResultHandlerPanel;
        private yats.Gui.Winforms.Components.TestRunTree.Components.RepetitionResultHandlerPanel repetitionResultHandlerPanel;


    }
}
