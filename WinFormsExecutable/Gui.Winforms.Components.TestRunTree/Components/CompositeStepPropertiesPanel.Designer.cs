﻿namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    partial class CompositeStepPropertiesPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.decisionHandlerPanel = new yats.Gui.Winforms.Components.TestRunTree.Components.DecisionHandlerPanel();
            this.executionMethodForCompositeStepPanel = new yats.Gui.Winforms.Components.TestRunTree.Components.ExecutionMethodForCompositeStepPanel();
            this.compositeDecisionPanel = new yats.Gui.Winforms.Components.TestRunTree.Components.CompositeDecisionPanel();
            this.repetitionResultHandlerPanel = new yats.Gui.Winforms.Components.TestRunTree.Components.RepetitionResultHandlerPanel();
            this.compositeResultHandlerPanel = new yats.Gui.Winforms.Components.TestRunTree.Components.CompositeResultHandlerPanel();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.decisionHandlerPanel);
            this.flowLayoutPanel1.Controls.Add(this.executionMethodForCompositeStepPanel);
            this.flowLayoutPanel1.Controls.Add(this.compositeDecisionPanel);
            this.flowLayoutPanel1.Controls.Add(this.repetitionResultHandlerPanel);
            this.flowLayoutPanel1.Controls.Add(this.compositeResultHandlerPanel);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(713, 563);
            this.flowLayoutPanel1.TabIndex = 1;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // decisionHandlerPanel
            // 
            this.decisionHandlerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.decisionHandlerPanel.Location = new System.Drawing.Point(3, 3);
            this.decisionHandlerPanel.Name = "decisionHandlerPanel";
            this.decisionHandlerPanel.Size = new System.Drawing.Size(703, 218);
            this.decisionHandlerPanel.TabIndex = 1;
            // 
            // executionMethodForCompositeStepPanel
            // 
            this.executionMethodForCompositeStepPanel.Location = new System.Drawing.Point(3, 227);
            this.executionMethodForCompositeStepPanel.Name = "executionMethodForCompositeStepPanel";
            this.executionMethodForCompositeStepPanel.Size = new System.Drawing.Size(703, 50);
            this.executionMethodForCompositeStepPanel.TabIndex = 2;
            // 
            // compositeDecisionPanel
            // 
            this.compositeDecisionPanel.Location = new System.Drawing.Point(3, 283);
            this.compositeDecisionPanel.Name = "compositeDecisionPanel";
            this.compositeDecisionPanel.Size = new System.Drawing.Size(703, 69);
            this.compositeDecisionPanel.TabIndex = 4;
            // 
            // repetitionResultHandlerPanel
            // 
            this.repetitionResultHandlerPanel.Location = new System.Drawing.Point(3, 358);
            this.repetitionResultHandlerPanel.Name = "repetitionResultHandlerPanel";
            this.repetitionResultHandlerPanel.Size = new System.Drawing.Size(703, 92);
            this.repetitionResultHandlerPanel.TabIndex = 5;
            // 
            // compositeResultHandlerPanel
            // 
            this.compositeResultHandlerPanel.Location = new System.Drawing.Point(3, 456);
            this.compositeResultHandlerPanel.Name = "compositeResultHandlerPanel";
            this.compositeResultHandlerPanel.Size = new System.Drawing.Size(703, 98);
            this.compositeResultHandlerPanel.TabIndex = 6;
            // 
            // CompositeStepPropertiesPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "CompositeStepPropertiesPanel";
            this.Size = new System.Drawing.Size(713, 563);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DecisionHandlerPanel decisionHandlerPanel;
        private ExecutionMethodForCompositeStepPanel executionMethodForCompositeStepPanel;
        private CompositeDecisionPanel compositeDecisionPanel;
        private RepetitionResultHandlerPanel repetitionResultHandlerPanel;
        private CompositeResultHandlerPanel compositeResultHandlerPanel;

    }
}
