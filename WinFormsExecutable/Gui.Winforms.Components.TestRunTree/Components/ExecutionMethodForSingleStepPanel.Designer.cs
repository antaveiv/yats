﻿namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    partial class ExecutionMethodForSingleStepPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbStepDisabled = new System.Windows.Forms.RadioButton();
            this.rbSequential = new System.Windows.Forms.RadioButton();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbStepDisabled);
            this.groupBox2.Controls.Add(this.rbSequential);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(591, 68);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Execution settings";
            // 
            // rbStepDisabled
            // 
            this.rbStepDisabled.AutoSize = true;
            this.rbStepDisabled.Location = new System.Drawing.Point(6, 42);
            this.rbStepDisabled.Name = "rbStepDisabled";
            this.rbStepDisabled.Size = new System.Drawing.Size(89, 17);
            this.rbStepDisabled.TabIndex = 1;
            this.rbStepDisabled.Text = "Step disabled";
            this.rbStepDisabled.UseVisualStyleBackColor = true;
            // 
            // rbSequential
            // 
            this.rbSequential.AutoSize = true;
            this.rbSequential.Checked = true;
            this.rbSequential.Location = new System.Drawing.Point(6, 19);
            this.rbSequential.Name = "rbSequential";
            this.rbSequential.Size = new System.Drawing.Size(88, 17);
            this.rbSequential.TabIndex = 0;
            this.rbSequential.TabStop = true;
            this.rbSequential.Text = "Step enabled";
            this.rbSequential.UseVisualStyleBackColor = true;
            // 
            // ExecutionMethodForSingleStepPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Name = "ExecutionMethodForSingleStepPanel";
            this.Size = new System.Drawing.Size(591, 68);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbStepDisabled;
        private System.Windows.Forms.RadioButton rbSequential;
    }
}
