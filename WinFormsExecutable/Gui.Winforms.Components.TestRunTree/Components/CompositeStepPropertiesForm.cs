﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using yats.ExecutionQueue;
using yats.TestRun.Commands;
using yats.Utilities.Commands;

namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    public partial class CompositeStepPropertiesForm : Form
    {
        public CompositeStepPropertiesForm()
        {
            InitializeComponent();
        }

        public CompositeStepPropertiesForm(List<TestStepComposite> steps):this()
        {
            compositeStepPropertiesPanel.DisplayModelValues(steps);
        }

        IEnumerable<ICommand> result = null;

        private void btOK_Click(object sender, EventArgs e)
        {
            result = compositeStepPropertiesPanel.GetStepConfigChanges();
            if (result != null)
            {
                DialogResult = DialogResult.OK;
            }
        }

        internal IEnumerable<ICommand> GetStepConfigChanges()
        {
            if (ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                return null;
            }
            return result;
        }
    }
}
