﻿namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    partial class SingleResultHandlerPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbCanceled = new System.Windows.Forms.ComboBox();
            this.cbNotRun = new System.Windows.Forms.ComboBox();
            this.cbInconclusive = new System.Windows.Forms.ComboBox();
            this.cbFail = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbPass = new System.Windows.Forms.ComboBox();
            this.rbCustomResultMapping = new System.Windows.Forms.RadioButton();
            this.rbIgnoreFailureSingleResultHandler = new System.Windows.Forms.RadioButton();
            this.rbDefaultSingleResultHandler = new System.Windows.Forms.RadioButton();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.rbCustomResultMapping);
            this.groupBox3.Controls.Add(this.rbIgnoreFailureSingleResultHandler);
            this.groupBox3.Controls.Add(this.rbDefaultSingleResultHandler);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(493, 229);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "One run result interpretation";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.cbCanceled);
            this.groupBox4.Controls.Add(this.cbNotRun);
            this.groupBox4.Controls.Add(this.cbInconclusive);
            this.groupBox4.Controls.Add(this.cbFail);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.cbPass);
            this.groupBox4.Location = new System.Drawing.Point(183, 65);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(298, 156);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "custom result mapping";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "CANCELED becomes";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "NOT RUN becomes";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "INCONCLUSIVE becomes";
            // 
            // cbCanceled
            // 
            this.cbCanceled.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCanceled.FormattingEnabled = true;
            this.cbCanceled.Location = new System.Drawing.Point(166, 127);
            this.cbCanceled.Name = "cbCanceled";
            this.cbCanceled.Size = new System.Drawing.Size(121, 21);
            this.cbCanceled.TabIndex = 6;
            this.cbCanceled.SelectedValueChanged += new System.EventHandler(this.cbPass_Click);
            this.cbCanceled.Click += new System.EventHandler(this.cbPass_Click);
            // 
            // cbNotRun
            // 
            this.cbNotRun.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNotRun.FormattingEnabled = true;
            this.cbNotRun.Location = new System.Drawing.Point(166, 100);
            this.cbNotRun.Name = "cbNotRun";
            this.cbNotRun.Size = new System.Drawing.Size(121, 21);
            this.cbNotRun.TabIndex = 5;
            this.cbNotRun.SelectedValueChanged += new System.EventHandler(this.cbPass_Click);
            this.cbNotRun.Click += new System.EventHandler(this.cbPass_Click);
            // 
            // cbInconclusive
            // 
            this.cbInconclusive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInconclusive.FormattingEnabled = true;
            this.cbInconclusive.Location = new System.Drawing.Point(166, 73);
            this.cbInconclusive.Name = "cbInconclusive";
            this.cbInconclusive.Size = new System.Drawing.Size(121, 21);
            this.cbInconclusive.TabIndex = 4;
            this.cbInconclusive.SelectedValueChanged += new System.EventHandler(this.cbPass_Click);
            this.cbInconclusive.Click += new System.EventHandler(this.cbPass_Click);
            // 
            // cbFail
            // 
            this.cbFail.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFail.FormattingEnabled = true;
            this.cbFail.Location = new System.Drawing.Point(166, 46);
            this.cbFail.Name = "cbFail";
            this.cbFail.Size = new System.Drawing.Size(121, 21);
            this.cbFail.TabIndex = 3;
            this.cbFail.SelectedValueChanged += new System.EventHandler(this.cbPass_Click);
            this.cbFail.Click += new System.EventHandler(this.cbPass_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "FAIL becomes";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "PASS becomes";
            // 
            // cbPass
            // 
            this.cbPass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPass.FormattingEnabled = true;
            this.cbPass.Location = new System.Drawing.Point(166, 19);
            this.cbPass.Name = "cbPass";
            this.cbPass.Size = new System.Drawing.Size(121, 21);
            this.cbPass.TabIndex = 0;
            this.cbPass.SelectedValueChanged += new System.EventHandler(this.cbPass_Click);
            this.cbPass.Click += new System.EventHandler(this.cbPass_Click);
            // 
            // rbCustomResultMapping
            // 
            this.rbCustomResultMapping.AutoSize = true;
            this.rbCustomResultMapping.Location = new System.Drawing.Point(6, 65);
            this.rbCustomResultMapping.Name = "rbCustomResultMapping";
            this.rbCustomResultMapping.Size = new System.Drawing.Size(171, 17);
            this.rbCustomResultMapping.TabIndex = 2;
            this.rbCustomResultMapping.TabStop = true;
            this.rbCustomResultMapping.Text = "Specify custom result mapping:";
            this.rbCustomResultMapping.UseVisualStyleBackColor = true;
            // 
            // rbIgnoreFailureSingleResultHandler
            // 
            this.rbIgnoreFailureSingleResultHandler.AutoSize = true;
            this.rbIgnoreFailureSingleResultHandler.Location = new System.Drawing.Point(6, 42);
            this.rbIgnoreFailureSingleResultHandler.Name = "rbIgnoreFailureSingleResultHandler";
            this.rbIgnoreFailureSingleResultHandler.Size = new System.Drawing.Size(290, 17);
            this.rbIgnoreFailureSingleResultHandler.TabIndex = 1;
            this.rbIgnoreFailureSingleResultHandler.Text = "Ignore FAIL or INCONCLUSIVE result (change to PASS)";
            this.rbIgnoreFailureSingleResultHandler.UseVisualStyleBackColor = true;
            // 
            // rbDefaultSingleResultHandler
            // 
            this.rbDefaultSingleResultHandler.AutoSize = true;
            this.rbDefaultSingleResultHandler.Checked = true;
            this.rbDefaultSingleResultHandler.Location = new System.Drawing.Point(6, 19);
            this.rbDefaultSingleResultHandler.Name = "rbDefaultSingleResultHandler";
            this.rbDefaultSingleResultHandler.Size = new System.Drawing.Size(107, 17);
            this.rbDefaultSingleResultHandler.TabIndex = 0;
            this.rbDefaultSingleResultHandler.TabStop = true;
            this.rbDefaultSingleResultHandler.Text = "Leave result as is";
            this.rbDefaultSingleResultHandler.UseVisualStyleBackColor = true;
            // 
            // SingleResultHandlerPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Name = "SingleResultHandlerPanel";
            this.Size = new System.Drawing.Size(493, 229);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbCanceled;
        private System.Windows.Forms.ComboBox cbNotRun;
        private System.Windows.Forms.ComboBox cbInconclusive;
        private System.Windows.Forms.ComboBox cbFail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbPass;
        private System.Windows.Forms.RadioButton rbCustomResultMapping;
        private System.Windows.Forms.RadioButton rbIgnoreFailureSingleResultHandler;
        private System.Windows.Forms.RadioButton rbDefaultSingleResultHandler;
    }
}
