﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Windows.Forms;
using yats.ExecutionQueue;

namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    public partial class StateMachineStateView : UserControl
    {
        StateMachineExecutionModifier.State state;

        public StateMachineStateView()
        {
            InitializeComponent();
        }

        public StateMachineStateView(StateMachineExecutionModifier.State state)
        {
            InitializeComponent();
            this.state = state;
            UpdateView();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (StateMachineEditStateDialog.Edit(state))
            {
                UpdateView();
                valueChanged(this, e);
            }
        }

        public event EventHandler Changed;

        private void valueChanged(object sender, EventArgs e)
        {
            if (Changed != null)
            {
                Changed(this, e);
            }
        }

        private void UpdateView()
        {
            lbStateName.Text = state.Name;
            if (state.IsFinishState){
                lbStartFinishState.Text = string.Format("Finish: {0}", state.ResultOnFinish);
                lbStartFinishState.Visible = true;
            }
            else if (state.IsStartState)
            {
                lbStartFinishState.Text = "Start";
                lbStartFinishState.Visible = true;
            }
            else
            {
                lbStartFinishState.Visible = false;
            }
        }

        public event EventHandler OnAddState;
        private void addStateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OnAddState != null)
            {
                OnAddState(state, e);
            }
        }

        public event EventHandler OnDeleteState;
        private void deleteStateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OnDeleteState != null)
            {
                OnDeleteState(state, e);
            }
        }

        public event EventHandler OnMoveStateLeft;
        private void moveLeftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OnMoveStateLeft != null)
            {
                OnMoveStateLeft(state, e);
            }
        }

        public event EventHandler OnMoveStateRight;
        private void moveRightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OnMoveStateRight != null)
            {
                OnMoveStateRight(state, e);
            }
        }
    }
}
