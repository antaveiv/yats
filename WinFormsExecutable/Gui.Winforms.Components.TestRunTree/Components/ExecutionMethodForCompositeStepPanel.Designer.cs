﻿namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    partial class ExecutionMethodForCompositeStepPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbExecutionMethod = new System.Windows.Forms.ComboBox();
            this.btConfigureStateMachine = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbExecutionMethod);
            this.groupBox1.Controls.Add(this.btConfigureStateMachine);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(628, 55);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Execution method";
            // 
            // cbExecutionMethod
            // 
            this.cbExecutionMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbExecutionMethod.FormattingEnabled = true;
            this.cbExecutionMethod.Location = new System.Drawing.Point(6, 21);
            this.cbExecutionMethod.Name = "cbExecutionMethod";
            this.cbExecutionMethod.Size = new System.Drawing.Size(181, 21);
            this.cbExecutionMethod.TabIndex = 8;
            this.cbExecutionMethod.SelectedIndexChanged += new System.EventHandler(this.cbExecutionMethod_SelectedIndexChanged);
            // 
            // btConfigureStateMachine
            // 
            this.btConfigureStateMachine.Enabled = false;
            this.btConfigureStateMachine.Location = new System.Drawing.Point(193, 19);
            this.btConfigureStateMachine.Name = "btConfigureStateMachine";
            this.btConfigureStateMachine.Size = new System.Drawing.Size(75, 23);
            this.btConfigureStateMachine.TabIndex = 7;
            this.btConfigureStateMachine.Text = "Configure...";
            this.btConfigureStateMachine.UseVisualStyleBackColor = true;
            this.btConfigureStateMachine.Click += new System.EventHandler(this.btConfigureStateMachine_Click);
            // 
            // ExecutionMethodForCompositeStepPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "ExecutionMethodForCompositeStepPanel";
            this.Size = new System.Drawing.Size(628, 55);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btConfigureStateMachine;
        private System.Windows.Forms.ComboBox cbExecutionMethod;
    }
}
