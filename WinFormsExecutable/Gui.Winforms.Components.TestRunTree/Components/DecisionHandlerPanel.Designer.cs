﻿namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    partial class DecisionHandlerPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbListOfResults = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numRepeatUntilConfigured = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numWhilePass = new System.Windows.Forms.NumericUpDown();
            this.rbWhilePass = new System.Windows.Forms.RadioButton();
            this.rbRepeatUntilConfigured = new System.Windows.Forms.RadioButton();
            this.rbRepeatUntilCancel = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.numRepeatsIgnoreFails = new System.Windows.Forms.NumericUpDown();
            this.rbRepeatIgnoreFail = new System.Windows.Forms.RadioButton();
            this.rbSingleRun = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatUntilConfigured)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWhilePass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatsIgnoreFails)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbListOfResults);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.numRepeatUntilConfigured);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.numWhilePass);
            this.groupBox1.Controls.Add(this.rbWhilePass);
            this.groupBox1.Controls.Add(this.rbRepeatUntilConfigured);
            this.groupBox1.Controls.Add(this.rbRepeatUntilCancel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.numRepeatsIgnoreFails);
            this.groupBox1.Controls.Add(this.rbRepeatIgnoreFail);
            this.groupBox1.Controls.Add(this.rbSingleRun);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(615, 216);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Step repetition";
            // 
            // cbListOfResults
            // 
            this.cbListOfResults.CheckOnClick = true;
            this.cbListOfResults.FormattingEnabled = true;
            this.cbListOfResults.Items.AddRange(new object[] {
            "PASS",
            "FAIL",
            "INCONCLUSIVE",
            "NOT_RUN",
            "CANCELED"});
            this.cbListOfResults.Location = new System.Drawing.Point(487, 115);
            this.cbListOfResults.Name = "cbListOfResults";
            this.cbListOfResults.Size = new System.Drawing.Size(120, 94);
            this.cbListOfResults.TabIndex = 11;
            this.cbListOfResults.Click += new System.EventHandler(this.numRepeatUntilConfigured_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(223, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(258, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "times max. Repetition is stopped on selected result(s):";
            // 
            // numRepeatUntilConfigured
            // 
            this.numRepeatUntilConfigured.Location = new System.Drawing.Point(97, 115);
            this.numRepeatUntilConfigured.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numRepeatUntilConfigured.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRepeatUntilConfigured.Name = "numRepeatUntilConfigured";
            this.numRepeatUntilConfigured.Size = new System.Drawing.Size(120, 20);
            this.numRepeatUntilConfigured.TabIndex = 9;
            this.numRepeatUntilConfigured.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRepeatUntilConfigured.ValueChanged += new System.EventHandler(this.numRepeatUntilConfigured_Click);
            this.numRepeatUntilConfigured.Click += new System.EventHandler(this.numRepeatUntilConfigured_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(223, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(214, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "times max. Repetition is stopped if a run fails";
            // 
            // numWhilePass
            // 
            this.numWhilePass.Location = new System.Drawing.Point(97, 69);
            this.numWhilePass.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numWhilePass.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numWhilePass.Name = "numWhilePass";
            this.numWhilePass.Size = new System.Drawing.Size(120, 20);
            this.numWhilePass.TabIndex = 7;
            this.numWhilePass.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numWhilePass.ValueChanged += new System.EventHandler(this.numWhilePass_Click);
            this.numWhilePass.Click += new System.EventHandler(this.numWhilePass_Click);
            // 
            // rbWhilePass
            // 
            this.rbWhilePass.AutoSize = true;
            this.rbWhilePass.Location = new System.Drawing.Point(6, 69);
            this.rbWhilePass.Name = "rbWhilePass";
            this.rbWhilePass.Size = new System.Drawing.Size(60, 17);
            this.rbWhilePass.TabIndex = 6;
            this.rbWhilePass.Text = "Repeat";
            this.rbWhilePass.UseVisualStyleBackColor = true;
            // 
            // rbRepeatUntilConfigured
            // 
            this.rbRepeatUntilConfigured.AutoSize = true;
            this.rbRepeatUntilConfigured.Location = new System.Drawing.Point(6, 115);
            this.rbRepeatUntilConfigured.Name = "rbRepeatUntilConfigured";
            this.rbRepeatUntilConfigured.Size = new System.Drawing.Size(60, 17);
            this.rbRepeatUntilConfigured.TabIndex = 5;
            this.rbRepeatUntilConfigured.Text = "Repeat";
            this.rbRepeatUntilConfigured.UseVisualStyleBackColor = true;
            // 
            // rbRepeatUntilCancel
            // 
            this.rbRepeatUntilCancel.AutoSize = true;
            this.rbRepeatUntilCancel.Location = new System.Drawing.Point(6, 92);
            this.rbRepeatUntilCancel.Name = "rbRepeatUntilCancel";
            this.rbRepeatUntilCancel.Size = new System.Drawing.Size(129, 17);
            this.rbRepeatUntilCancel.TabIndex = 4;
            this.rbRepeatUntilCancel.Text = "Repeat until canceled";
            this.rbRepeatUntilCancel.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(223, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "times. If a run fails the repetition continues";
            // 
            // numRepeatsIgnoreFails
            // 
            this.numRepeatsIgnoreFails.Location = new System.Drawing.Point(97, 42);
            this.numRepeatsIgnoreFails.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numRepeatsIgnoreFails.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRepeatsIgnoreFails.Name = "numRepeatsIgnoreFails";
            this.numRepeatsIgnoreFails.Size = new System.Drawing.Size(120, 20);
            this.numRepeatsIgnoreFails.TabIndex = 2;
            this.numRepeatsIgnoreFails.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRepeatsIgnoreFails.ValueChanged += new System.EventHandler(this.numRepeatsIgnoreFails_Click);
            this.numRepeatsIgnoreFails.Click += new System.EventHandler(this.numRepeatsIgnoreFails_Click);
            // 
            // rbRepeatIgnoreFail
            // 
            this.rbRepeatIgnoreFail.AutoSize = true;
            this.rbRepeatIgnoreFail.Location = new System.Drawing.Point(6, 42);
            this.rbRepeatIgnoreFail.Name = "rbRepeatIgnoreFail";
            this.rbRepeatIgnoreFail.Size = new System.Drawing.Size(60, 17);
            this.rbRepeatIgnoreFail.TabIndex = 1;
            this.rbRepeatIgnoreFail.Text = "Repeat";
            this.rbRepeatIgnoreFail.UseVisualStyleBackColor = true;
            // 
            // rbSingleRun
            // 
            this.rbSingleRun.AutoSize = true;
            this.rbSingleRun.Checked = true;
            this.rbSingleRun.Location = new System.Drawing.Point(6, 19);
            this.rbSingleRun.Name = "rbSingleRun";
            this.rbSingleRun.Size = new System.Drawing.Size(72, 17);
            this.rbSingleRun.TabIndex = 0;
            this.rbSingleRun.TabStop = true;
            this.rbSingleRun.Text = "Single run";
            this.rbSingleRun.UseVisualStyleBackColor = true;
            // 
            // DecisionHandlerPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "DecisionHandlerPanel";
            this.Size = new System.Drawing.Size(615, 216);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatUntilConfigured)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWhilePass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRepeatsIgnoreFails)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckedListBox cbListOfResults;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numRepeatUntilConfigured;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numWhilePass;
        private System.Windows.Forms.RadioButton rbWhilePass;
        private System.Windows.Forms.RadioButton rbRepeatUntilConfigured;
        private System.Windows.Forms.RadioButton rbRepeatUntilCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numRepeatsIgnoreFails;
        private System.Windows.Forms.RadioButton rbRepeatIgnoreFail;
        private System.Windows.Forms.RadioButton rbSingleRun;
    }
}
