﻿namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    partial class StateMachineStepView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.numProbability = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.cbInconclusive = new System.Windows.Forms.ComboBox();
            this.cbFail = new System.Windows.Forms.ComboBox();
            this.cbPass = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbRun = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numProbability)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.numProbability);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cbInconclusive);
            this.panel1.Controls.Add(this.cbFail);
            this.panel1.Controls.Add(this.cbPass);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbRun);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(149, 131);
            this.panel1.TabIndex = 0;
            // 
            // numProbability
            // 
            this.numProbability.Location = new System.Drawing.Point(64, 106);
            this.numProbability.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numProbability.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numProbability.Name = "numProbability";
            this.numProbability.Size = new System.Drawing.Size(80, 20);
            this.numProbability.TabIndex = 6;
            this.numProbability.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numProbability.ValueChanged += new System.EventHandler(this.numProbability_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Probability";
            // 
            // cbInconclusive
            // 
            this.cbInconclusive.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbInconclusive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInconclusive.FormattingEnabled = true;
            this.cbInconclusive.Location = new System.Drawing.Point(40, 79);
            this.cbInconclusive.Name = "cbInconclusive";
            this.cbInconclusive.Size = new System.Drawing.Size(107, 21);
            this.cbInconclusive.TabIndex = 4;
            this.cbInconclusive.SelectedIndexChanged += new System.EventHandler(this.cbInconclusive_SelectedIndexChanged);
            // 
            // cbFail
            // 
            this.cbFail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFail.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFail.FormattingEnabled = true;
            this.cbFail.Location = new System.Drawing.Point(40, 52);
            this.cbFail.Name = "cbFail";
            this.cbFail.Size = new System.Drawing.Size(107, 21);
            this.cbFail.TabIndex = 4;
            this.cbFail.SelectedIndexChanged += new System.EventHandler(this.cbFail_SelectedIndexChanged);
            // 
            // cbPass
            // 
            this.cbPass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPass.FormattingEnabled = true;
            this.cbPass.Location = new System.Drawing.Point(39, 25);
            this.cbPass.Name = "cbPass";
            this.cbPass.Size = new System.Drawing.Size(107, 21);
            this.cbPass.TabIndex = 4;
            this.cbPass.SelectedIndexChanged += new System.EventHandler(this.cbPass_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Inc.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Fail";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pass";
            // 
            // cbRun
            // 
            this.cbRun.AutoSize = true;
            this.cbRun.Location = new System.Drawing.Point(2, 3);
            this.cbRun.Name = "cbRun";
            this.cbRun.Size = new System.Drawing.Size(46, 17);
            this.cbRun.TabIndex = 0;
            this.cbRun.Text = "Run";
            this.cbRun.UseVisualStyleBackColor = true;
            this.cbRun.CheckedChanged += new System.EventHandler(this.cbRun_CheckedChanged);
            // 
            // StateMachineStepView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(149, 131);
            this.Name = "StateMachineStepView";
            this.Size = new System.Drawing.Size(149, 131);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numProbability)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cbInconclusive;
        private System.Windows.Forms.ComboBox cbFail;
        private System.Windows.Forms.ComboBox cbPass;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbRun;
        private System.Windows.Forms.NumericUpDown numProbability;
        private System.Windows.Forms.Label label4;
    }
}
