﻿namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    partial class CompositeDecisionPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbExecuteUntilCanceled = new System.Windows.Forms.RadioButton();
            this.rbExecuteWhilePassing = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbExecuteWhilePassing);
            this.groupBox1.Controls.Add(this.rbExecuteUntilCanceled);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(499, 70);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Step execution";
            // 
            // rbExecuteUntilCanceled
            // 
            this.rbExecuteUntilCanceled.AutoSize = true;
            this.rbExecuteUntilCanceled.Checked = true;
            this.rbExecuteUntilCanceled.Location = new System.Drawing.Point(6, 19);
            this.rbExecuteUntilCanceled.Name = "rbExecuteUntilCanceled";
            this.rbExecuteUntilCanceled.Size = new System.Drawing.Size(174, 17);
            this.rbExecuteUntilCanceled.TabIndex = 0;
            this.rbExecuteUntilCanceled.TabStop = true;
            this.rbExecuteUntilCanceled.Text = "Execute steps even after failure";
            this.rbExecuteUntilCanceled.UseVisualStyleBackColor = true;
            // 
            // rbExecuteWhilePassing
            // 
            this.rbExecuteWhilePassing.AutoSize = true;
            this.rbExecuteWhilePassing.Location = new System.Drawing.Point(6, 42);
            this.rbExecuteWhilePassing.Name = "rbExecuteWhilePassing";
            this.rbExecuteWhilePassing.Size = new System.Drawing.Size(93, 17);
            this.rbExecuteWhilePassing.TabIndex = 1;
            this.rbExecuteWhilePassing.Text = "Stop on failure";
            this.rbExecuteWhilePassing.UseVisualStyleBackColor = true;
            // 
            // CompositeDecisionPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "CompositeDecisionPanel";
            this.Size = new System.Drawing.Size(499, 70);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbExecuteWhilePassing;
        private System.Windows.Forms.RadioButton rbExecuteUntilCanceled;
    }
}
