﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Windows.Forms;
using yats.ExecutionQueue;
using yats.Gui.Winforms.Components.TestRunTree.Components;
using yats.TestRun.Commands;
using System;
using yats.Utilities.Commands;

namespace yats.Gui.Winforms.Components.TestRunTree
{
    public partial class SingleStepPropertiesPanel : UserControl, IStepPropertiesPanel
    {
        public SingleStepPropertiesPanel()
        {
            InitializeComponent();
        }

        List<TestStepSingle> steps = new List<TestStepSingle>();
        public SingleStepPropertiesPanel(List<TestStepSingle> steps)
            : this()
        {
            
            DisplayModelValues(steps);
        }

        public void DisplayModelValues(List<TestStepSingle> steps)
        {
            List<TestStep> tmp = new List<TestStep>();//to be able to pass as List<TestStep> parameter
            foreach (var s in steps)
            {
                tmp.Add(s);
            }

            this.steps = steps;
            this.decisionHandlerPanel.DisplayModelValues(tmp);
            this.executionMethodForSingleStepPanel.DisplayModelValues(steps);
            this.singleResultHandlerPanel.DisplayModelValues(steps);
            this.repetitionResultHandlerPanel.DisplayModelValues(tmp);
        }

        internal IEnumerable<ICommand> GetStepConfigChanges()
        {
            try
            {
                List<ICommand> result = new List<ICommand>();
                result.Add(decisionHandlerPanel.GetConfigChanges(steps));
                result.Add(executionMethodForSingleStepPanel.GetConfigChanges(steps));
                result.Add(singleResultHandlerPanel.GetConfigChanges(steps));
                result.Add(repetitionResultHandlerPanel.GetConfigChanges(steps));
                return result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return null;
        }
    }
}
