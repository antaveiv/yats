﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using yats.ExecutionQueue;
using yats.TestRun.Commands;
using yats.Utilities;
using yats.Utilities.Commands;

namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    public partial class CompositeStepPropertiesPanel : UserControl, IStepPropertiesPanel
    {
        List<TestStepComposite> steps = new List<TestStepComposite>();

        public CompositeStepPropertiesPanel()
        {
            InitializeComponent();
            this.executionMethodForCompositeStepPanel.Changed += executionMethodForCompositeStepPanel_Changed;
        }
                
        public CompositeStepPropertiesPanel(List<TestStepComposite> steps) : this()
        {            
            DisplayModelValues(steps);
        }

        void executionMethodForCompositeStepPanel_Changed(object sender, ExecutionMethodChangeEventArgs e)
        {
            bool stateMachineConfigured = e.NewType == typeof(StateMachineExecutionModifier);

            this.compositeDecisionPanel.Enabled = !stateMachineConfigured;

            this.compositeResultHandlerPanel.Enabled = !stateMachineConfigured;
        }

        public void DisplayModelValues(List<TestStepComposite> steps)
        {
            this.steps = steps;
            this.decisionHandlerPanel.DisplayModelValues(steps);
            this.executionMethodForCompositeStepPanel.DisplayModelValues(steps);
            this.compositeDecisionPanel.DisplayModelValues(steps);
            this.repetitionResultHandlerPanel.DisplayModelValues(steps);
            this.compositeResultHandlerPanel.DisplayModelValues(steps);
        }
                
        internal IEnumerable<ICommand> GetStepConfigChanges()
        {
            try{
            List<ICommand> result = new List<ICommand>();
            result.Add(decisionHandlerPanel.GetConfigChanges(steps));
            result.Add(executionMethodForCompositeStepPanel.GetConfigChanges(steps));
            result.Add(compositeDecisionPanel.GetConfigChanges(steps));
            result.Add(repetitionResultHandlerPanel.GetConfigChanges(steps));
            if (compositeResultHandlerPanel.Enabled)
            {
                result.Add(compositeResultHandlerPanel.GetConfigChanges(steps));                
            }
            return result;
            } catch (Exception ex){
                MessageBox.Show(ex.Message);
            }
            return null;
        }
    }
}
