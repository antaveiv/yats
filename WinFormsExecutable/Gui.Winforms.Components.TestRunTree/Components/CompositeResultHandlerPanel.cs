﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using yats.ExecutionQueue;
using yats.ExecutionQueue.ResultHandler;
using yats.TestRun.Commands;
using yats.Utilities.Commands;

namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    public partial class CompositeResultHandlerPanel : UserControl
    {
        SelectedStepResultHandler selectedStepResultHandler;
        TestStepComposite step;
        
        public CompositeResultHandlerPanel()
        {
            InitializeComponent();
        }
                
        internal void DisplayModelValues(List<TestStepComposite> steps)
        {
            this.step = steps[0];

            HashSet<Type> types = new HashSet<Type>();
            foreach (var step in steps)
            {
                types.Add(step.CompositeResultHandler.GetType());
            }
            this.rbDefaultCompositeResultHandler.Checked = (types.Count == 1) && types.Contains(typeof(DefaultCompositeResultHandler));
            this.rbWorstCaseCompositeResultHandler.Checked = (types.Count == 1) && types.Contains(typeof(WorstCaseCompositeResultHandler));
            if ((steps.Count == 1) && types.Contains(typeof(SelectedStepResultHandler)))
            {
                
                this.selectedStepResultHandler = step.CompositeResultHandler.Clone() as SelectedStepResultHandler; //make a clone for modification, assign only on OK
                this.rbWorstCaseSelectedResultHandler.Checked = true;
            }
            this.rbWorstCaseSelectedResultHandler.Enabled = this.btSelectSteps.Enabled = steps.Count == 1; // don't allow using 'selected steps' method for multiple selected steps
        }
                
        internal ICommand GetConfigChanges<T>(List<T> steps) where T : TestStepComposite
        {
            ICompositeResultHandler handler;

            if (this.rbDefaultCompositeResultHandler.Checked)
            {
                handler = new DefaultCompositeResultHandler();
            }
            else if (this.rbWorstCaseCompositeResultHandler.Checked)
            {
                handler = new WorstCaseCompositeResultHandler();
            }
            else if (this.rbWorstCaseSelectedResultHandler.Checked)
            {
                handler = selectedStepResultHandler;
            }
            else
            {
                //nothing checked - error
                throw new Exception("Result handling method is not selected");
            }

            return new ChangeCompositeResultHandler<T>(handler, steps, null);
        }

        private void btSelectSteps_Click(object sender, EventArgs e)
        {
            if (selectedStepResultHandler == null)
            {
                selectedStepResultHandler = new SelectedStepResultHandler();
            }
            
            rbWorstCaseSelectedResultHandler.Checked = true;

            selectedStepResultHandler.StepIdsToEvaluate.RemoveAll(x=>step.Steps.Count(y => y.Guid.Equals(x))==0);
            
            var newSelectedSteps = TestStepSelectionDialog.Select(step.Steps, step.Steps.Where(x=>selectedStepResultHandler.StepIdsToEvaluate.Contains(x.Guid)).ToList());
            selectedStepResultHandler.StepIdsToEvaluate = newSelectedSteps.Select(x => x.Guid).ToList();
        }
    }
}
