﻿namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    partial class RepetitionResultHandlerPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbBestCaseRepetitionResultHandler = new System.Windows.Forms.RadioButton();
            this.rbWorstCaseRepetitionResultHandler = new System.Windows.Forms.RadioButton();
            this.rbDefaultRepetitionResultHandler = new System.Windows.Forms.RadioButton();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rbBestCaseRepetitionResultHandler);
            this.groupBox5.Controls.Add(this.rbWorstCaseRepetitionResultHandler);
            this.groupBox5.Controls.Add(this.rbDefaultRepetitionResultHandler);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(404, 87);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Repetition result interpretation";
            // 
            // rbBestCaseRepetitionResultHandler
            // 
            this.rbBestCaseRepetitionResultHandler.AutoSize = true;
            this.rbBestCaseRepetitionResultHandler.Location = new System.Drawing.Point(6, 65);
            this.rbBestCaseRepetitionResultHandler.Name = "rbBestCaseRepetitionResultHandler";
            this.rbBestCaseRepetitionResultHandler.Size = new System.Drawing.Size(205, 17);
            this.rbBestCaseRepetitionResultHandler.TabIndex = 2;
            this.rbBestCaseRepetitionResultHandler.Text = "Take the best result among repetitions";
            this.rbBestCaseRepetitionResultHandler.UseVisualStyleBackColor = true;
            // 
            // rbWorstCaseRepetitionResultHandler
            // 
            this.rbWorstCaseRepetitionResultHandler.AutoSize = true;
            this.rbWorstCaseRepetitionResultHandler.Location = new System.Drawing.Point(6, 42);
            this.rbWorstCaseRepetitionResultHandler.Name = "rbWorstCaseRepetitionResultHandler";
            this.rbWorstCaseRepetitionResultHandler.Size = new System.Drawing.Size(210, 17);
            this.rbWorstCaseRepetitionResultHandler.TabIndex = 1;
            this.rbWorstCaseRepetitionResultHandler.Text = "Take the worst result among repetitions";
            this.rbWorstCaseRepetitionResultHandler.UseVisualStyleBackColor = true;
            // 
            // rbDefaultRepetitionResultHandler
            // 
            this.rbDefaultRepetitionResultHandler.AutoSize = true;
            this.rbDefaultRepetitionResultHandler.Checked = true;
            this.rbDefaultRepetitionResultHandler.Location = new System.Drawing.Point(6, 19);
            this.rbDefaultRepetitionResultHandler.Name = "rbDefaultRepetitionResultHandler";
            this.rbDefaultRepetitionResultHandler.Size = new System.Drawing.Size(320, 17);
            this.rbDefaultRepetitionResultHandler.TabIndex = 0;
            this.rbDefaultRepetitionResultHandler.TabStop = true;
            this.rbDefaultRepetitionResultHandler.Text = "If there were no NOT RUN or CANCELED results, set to PASS";
            this.rbDefaultRepetitionResultHandler.UseVisualStyleBackColor = true;
            // 
            // RepetitionResultHandlerPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox5);
            this.Name = "RepetitionResultHandlerPanel";
            this.Size = new System.Drawing.Size(404, 87);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rbBestCaseRepetitionResultHandler;
        private System.Windows.Forms.RadioButton rbWorstCaseRepetitionResultHandler;
        private System.Windows.Forms.RadioButton rbDefaultRepetitionResultHandler;
    }
}
