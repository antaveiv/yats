﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Drawing;
using System.Windows.Forms;
using yats.ExecutionQueue;
using yats.Utilities;

namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    public partial class StateMachineConfigurationPanel : UserControl
    {
        TestStepComposite test;
        StateMachineExecutionModifier stateMachine;

        public StateMachineConfigurationPanel()
        {
            InitializeComponent();
        }
        
        public event EventHandler StructureChanged;
        public event EventHandler PropertiesChanged;

        internal void SetModel(TestStepComposite test, StateMachineExecutionModifier stateMachine)
        {
            this.test = test;
            this.stateMachine = stateMachine;
            
            tableLayout.SuspendLayout();

            tableLayout.RowCount = test.Steps.Count + 1; // 1 for header with State info
            tableLayout.ColumnCount = stateMachine.States.Count + 1; // 1 for header with test cases
            tableLayout.Controls.Clear();
            int columnIndex = 1; //leave 1 for header
            foreach (var state in stateMachine.States)
            {
                var view = new StateMachineStateView(state);
                view.Changed += structureChanged;
                view.OnAddState += new EventHandler(view_OnAddState);
                view.OnDeleteState += new EventHandler(view_OnDeleteState);
                view.OnMoveStateLeft += new EventHandler(view_OnMoveStateLeft);
                view.OnMoveStateRight += new EventHandler(view_OnMoveStateRight);
                //view.Dock = DockStyle.Fill;
                tableLayout.Controls.Add(view, columnIndex, 0);
                columnIndex++;
            }
            int rowIndex = 1;
            float maxLength = 0;
            var graphics = Graphics.FromHwnd(IntPtr.Zero);

            foreach (var step in test.Steps)
            {
                Label view = new Label();
                maxLength = Math.Max(maxLength, graphics.MeasureString(step.Name, view.Font).Width);
                //maxLength = Math.Max(maxLength, TextRenderer.MeasureText(step.Name, view.Font).Width);
                view.Text = step.Name;
                view.TextAlign = ContentAlignment.MiddleCenter;
                //view.Dock = DockStyle.Fill;
                tableLayout.Controls.Add(view, 0, rowIndex);
                rowIndex++;
            }

            columnIndex = 1; //leave 1 for header
            foreach (var state in stateMachine.States)
            {
                rowIndex = 1;
                foreach (var step in test.Steps)
                {
                    StateMachineStepView view = new StateMachineStepView(stateMachine, state, step.Guid);
                    view.Changed += propertiesChanged;
                    //view.Dock = DockStyle.Fill;
                    tableLayout.Controls.Add(view, columnIndex, rowIndex);
                    rowIndex++;
                }
                columnIndex++;
            }
            
            tableLayout.ColumnStyles.Clear();
            tableLayout.RowStyles.Clear();
            
            // 1st column width
            tableLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, maxLength+6));
            //tableLayout.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            /*
            for (int row = 0; row < tableLayout.RowCount-1; row++)
            {
                //int height = tableLayout.GetControlFromPosition(1, row).MinimumSize.Height + 4;
                //tableLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, height));
                tableLayout.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            }
            //tableLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, 1));
            */
            /*
            for (int col = 1; col < tableLayout.ColumnCount; col++)
            {
                //int width = tableLayout.GetControlFromPosition(col, 1).MinimumSize.Width + 4;
                //tableLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, width));
                tableLayout.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
            }
            */

            tableLayout.ResumeLayout();
        }

        void view_OnMoveStateRight(object sender, EventArgs e)
        {
            int currentIndex = stateMachine.States.IndexOf(sender as StateMachineExecutionModifier.State);
            if (stateMachine.States.SwitchItems(currentIndex, currentIndex + 1))
            {
                structureChanged(this, e);
            }
        }

        void view_OnMoveStateLeft(object sender, EventArgs e)
        {
            int currentIndex = stateMachine.States.IndexOf(sender as StateMachineExecutionModifier.State);
            if (stateMachine.States.SwitchItems(currentIndex, currentIndex - 1))
            {
                structureChanged(this, e);
            }
        }

        void view_OnDeleteState(object sender, EventArgs e)
        {
            int currentIndex = stateMachine.States.IndexOf(sender as StateMachineExecutionModifier.State);
            stateMachine.States.RemoveAt(currentIndex);
            structureChanged(this, e);
        }

        void view_OnAddState(object sender, EventArgs e)
        {
            int currentIndex = stateMachine.States.IndexOf(sender as StateMachineExecutionModifier.State);
            StateMachineExecutionModifier.State newState = new StateMachineExecutionModifier.State();
            if (StateMachineEditStateDialog.Edit(newState))
            {
                stateMachine.States.Insert(currentIndex, newState);
                structureChanged(this, e);
            }
        }

        void propertiesChanged(object sender, EventArgs e)
        {
            if (PropertiesChanged != null)
            {
                PropertiesChanged(this, e);
            }
        }

        void structureChanged(object sender, EventArgs e)
        {
            if (StructureChanged != null)
            {
                StructureChanged(this, e);
            }
        }
    }
}
