﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using yats.ExecutionQueue;
using yats.ExecutionQueue.CompositeStepExecuteDecision;
using yats.TestRun.Commands;
using yats.ExecutionQueue.ResultHandler;
using yats.Gui.Winforms.Components.TestRunTree.Properties;
using System.Linq;
using yats.Utilities.Commands;

namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    public partial class ExecutionMethodForCompositeStepPanel : UserControl
    {
        TestStepComposite step;
        StateMachineExecutionModifier sm;
        Dictionary<Type, string> availableMethods = new Dictionary<Type,string>() { 
            {typeof(SequentialExecutionModifier), Resources.SequentialExecutionModifier}, 
            {typeof(StepDisabledModifier), Resources.StepDisabledModifier}, 
            {typeof(ParallelExecutionModifier), Resources.ParallelExecutionModifier},
            {typeof(RandomOrderModifier), Resources.RandomOrderModifier},
            {typeof(OneRandomStepExecutionModifier), Resources.OneRandomStepExecutionModifier},
            {typeof(StateMachineExecutionModifier), Resources.StateMachineExecutionModifier}
        };

        public ExecutionMethodForCompositeStepPanel()
        {
            InitializeComponent();
            cbExecutionMethod.DataSource = new BindingSource(availableMethods, null);
            cbExecutionMethod.DisplayMember = "Value";
            cbExecutionMethod.ValueMember = "Key";
        }

        public event EventHandler<ExecutionMethodChangeEventArgs> Changed;

        internal void DisplayModelValues(List<TestStepComposite> steps)
        {
            HashSet<Type> types = new HashSet<Type>();
            foreach (var step in steps)
            {
                types.Add(step.ExecutionMethod.GetType());
            }
            if (types.Count == 1)
            {
                cbExecutionMethod.SelectedValue = types.First();
            }

            if (steps.Count == 1){
                this.step = steps[0];
                this.sm = step.ExecutionMethod as StateMachineExecutionModifier;
            }
        }

        internal ICommand GetConfigChanges<T>(List<T> steps) where T : TestStepComposite
        {
            IStepExecution handler;
            Type methodType = cbExecutionMethod.SelectedValue as Type;
            if (methodType == typeof(ParallelExecutionModifier))
            {
                handler = new ParallelExecutionModifier();
            }
            else if (methodType == typeof(RandomOrderModifier))
            {
                handler = new RandomOrderModifier();
            }
            else if (methodType == typeof(SequentialExecutionModifier))
            {
                handler = new SequentialExecutionModifier();
            }
            else if (methodType == typeof(StepDisabledModifier))
            {
                handler = new StepDisabledModifier();
            }
            else if (methodType == typeof(OneRandomStepExecutionModifier))
            {
                handler = new OneRandomStepExecutionModifier();
            }
            else if (methodType == typeof(StateMachineExecutionModifier))
            {
                CompositeCommand command = new CompositeCommand();
                command.Add(new ChangeStepExecutionMethod<T>(sm, steps, null));
                command.Add(new ChangeStepResultHandler<T>(new StateMachineResultHandler(), steps, null));
                command.Add(new ChangeCompositeDecisionHandler<T>(new ExecuteUntilCanceled(), steps, null));
                return command;
            }
            else
            {
                //nothing checked - error
                throw new Exception("Execution method is not selected");
            }
                        
            return new ChangeStepExecutionMethod<T>(handler, steps, null);
        }

        private void btConfigureStateMachine_Click(object sender, EventArgs e)
        {
            if (this.sm == null)
            {
                this.sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            }
            StateMachineExecutionModifier clone = (StateMachineExecutionModifier)this.sm.Clone();
            if (StateMachineConfigurationForm.Edit(step, clone))
            {
                this.sm = clone;
            }
        }
        
        private void cbExecutionMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            Type methodType = cbExecutionMethod.SelectedValue as Type;
            if (this.sm == null && methodType == typeof(StateMachineExecutionModifier))
            {
                this.sm = StateMachineExecutionModifier.NewWithStartAndFinish();
            }

            btConfigureStateMachine.Enabled = cbExecutionMethod.SelectedValue == typeof(StateMachineExecutionModifier);

            // raise a type change event
            var copy = Changed;
            if (copy == null)
            {
                return;
            }
            copy(this, new ExecutionMethodChangeEventArgs() { NewType = methodType });
        }
    }

    public class ExecutionMethodChangeEventArgs : EventArgs
    {
        public Type NewType;
    }
}
