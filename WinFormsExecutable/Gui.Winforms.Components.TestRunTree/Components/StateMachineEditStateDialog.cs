﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Windows.Forms;
using yats.ExecutionQueue;
using yats.TestCase.Interface;

namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    public partial class StateMachineEditStateDialog : Form
    {
        public StateMachineEditStateDialog()
        {
            InitializeComponent();
        }

        public static bool Edit(StateMachineExecutionModifier.State state)
        {
            StateMachineEditStateDialog dlg = new StateMachineEditStateDialog();
            dlg.tbName.Text = state.Name;
            dlg.rbFinish.Checked = state.IsFinishState;
            dlg.rbStart.Checked = state.IsStartState;
            dlg.rbNone.Checked = (state.IsStartState == false && state.IsFinishState == false);
            dlg.cbResult.Enabled = dlg.rbFinish.Checked;
            dlg.cbResult.Items.Add(ResultEnum.PASS);
            dlg.cbResult.Items.Add(ResultEnum.FAIL);
            dlg.cbResult.Items.Add(ResultEnum.INCONCLUSIVE);
            dlg.cbResult.SelectedItem = state.ResultOnFinish;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                state.IsFinishState = dlg.rbFinish.Checked;
                state.IsStartState = dlg.rbStart.Checked;

                if (dlg.rbFinish.Checked)
                {
                    state.ResultOnFinish = (ResultEnum)dlg.cbResult.SelectedItem;
                }
                
                state.Name = dlg.tbName.Text;
                return true;
            }
            return false;
        }

        private void rbFinish_CheckedChanged(object sender, EventArgs e)
        {
            cbResult.Enabled = rbFinish.Checked;
        }
    }
}
