﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Linq;
using System.Windows.Forms;
using yats.ExecutionQueue;

namespace yats.Gui.Winforms.Components.TestRunTree.Components
{
    public partial class StateMachineStepView : UserControl
    {
        StateMachineExecutionModifier.State state;
        Guid testStepGuid;
        StateMachineExecutionModifier sm;
        private readonly object mLockObject = new object ();

        public StateMachineStepView()
        {
            InitializeComponent();
        }

        public StateMachineStepView(StateMachineExecutionModifier sm, StateMachineExecutionModifier.State state, Guid testStepGuid)
            : this()
        {
            this.sm = sm;
            this.state = state;
            this.testStepGuid = testStepGuid;

            UpdateView(true);
        }

        public void UpdateView(bool updateRunCheckbox)
        {
        lock (mLockObject)
            {
                bool enabled = state.Steps.Count(s => s.StepGuid == testStepGuid) > 0;
                if (updateRunCheckbox)
                {
                    cbRun.CheckedChanged -= cbRun_CheckedChanged;
                    cbRun.Checked = enabled;
                    cbRun.CheckedChanged += cbRun_CheckedChanged;
                }

                cbPass.Enabled = cbInconclusive.Enabled = cbFail.Enabled = numProbability.Enabled = enabled;

                cbPass.Items.Clear();
                cbInconclusive.Items.Clear();
                cbFail.Items.Clear();
                if (enabled)
                {
                    var states = sm.States.ToArray();
                    object[] states2 = new object[states.Length + 1];
                    Array.Copy(states, 0, states2, 1, states.Length);
                    states2[0] = string.Empty;
                    cbPass.Items.AddRange(states2);
                    cbInconclusive.Items.AddRange(states2);
                    cbFail.Items.AddRange(states2);
                    StateMachineExecutionModifier.TestCaseResultMap map = state.Steps.First(s => s.StepGuid == testStepGuid);
                    if (map.ResultStateMap.ContainsKey(yats.TestCase.Interface.ResultEnum.PASS))
                    {
                        cbPass.SelectedItem = sm.States.First(s => s.StateGUID == map.ResultStateMap[yats.TestCase.Interface.ResultEnum.PASS]);
                    }
                    if (map.ResultStateMap.ContainsKey(yats.TestCase.Interface.ResultEnum.INCONCLUSIVE))
                    {
                        cbInconclusive.SelectedItem = sm.States.First(s => s.StateGUID == map.ResultStateMap[yats.TestCase.Interface.ResultEnum.INCONCLUSIVE]);
                    }
                    if (map.ResultStateMap.ContainsKey(yats.TestCase.Interface.ResultEnum.FAIL))
                    {
                        cbFail.SelectedItem = sm.States.First(s => s.StateGUID == map.ResultStateMap[yats.TestCase.Interface.ResultEnum.FAIL]);
                    }
                    numProbability.Value = map.Probability;
                }
            }
        }
        
        public event EventHandler Changed;

        private void valueChanged(object sender, EventArgs e)
        {
            var copy = Changed;
            if (copy != null)
            {
                copy(sender, e);
            }
        }

        private void cbRun_CheckedChanged(object sender, EventArgs e)
        {
        lock (mLockObject)
            {
                if (cbRun.Checked)
                {
                    state.Steps.Add(new StateMachineExecutionModifier.TestCaseResultMap(testStepGuid));
                }
                else
                {
                    state.Steps.RemoveAll(s => s.StepGuid == testStepGuid);
                }
                UpdateView(false);
            }
            valueChanged(sender, e);
        }

        private void cbPass_SelectedIndexChanged(object sender, EventArgs e)
        {
        lock (mLockObject)
            {
                if (cbPass.SelectedItem is StateMachineExecutionModifier.State)
                {
                    state.Steps.First(s => s.StepGuid == testStepGuid).ResultStateMap[yats.TestCase.Interface.ResultEnum.PASS] = (cbPass.SelectedItem as StateMachineExecutionModifier.State).StateGUID;
                }
                else
                {
                    state.Steps.First(s => s.StepGuid == testStepGuid).ResultStateMap.Remove(yats.TestCase.Interface.ResultEnum.PASS);
                }
            }
            valueChanged(sender, e);
        }

        private void cbFail_SelectedIndexChanged(object sender, EventArgs e)
        {
        lock (mLockObject)
            {
                if (cbFail.SelectedItem is StateMachineExecutionModifier.State)
                {
                    state.Steps.First(s => s.StepGuid == testStepGuid).ResultStateMap[yats.TestCase.Interface.ResultEnum.FAIL] = (cbFail.SelectedItem as StateMachineExecutionModifier.State).StateGUID;
                }
                else
                {
                    state.Steps.First(s => s.StepGuid == testStepGuid).ResultStateMap.Remove(yats.TestCase.Interface.ResultEnum.FAIL);
                }
            }
            valueChanged(sender, e);
        }

        private void cbInconclusive_SelectedIndexChanged(object sender, EventArgs e)
        {
        lock (mLockObject)
            {
                if (cbInconclusive.SelectedItem is StateMachineExecutionModifier.State)
                {
                    state.Steps.First(s => s.StepGuid == testStepGuid).ResultStateMap[yats.TestCase.Interface.ResultEnum.INCONCLUSIVE] = (cbInconclusive.SelectedItem as StateMachineExecutionModifier.State).StateGUID;
                }
                else
                {
                    state.Steps.First(s => s.StepGuid == testStepGuid).ResultStateMap.Remove(yats.TestCase.Interface.ResultEnum.INCONCLUSIVE);
                }
            }
            valueChanged(sender, e);
        }

        private void numProbability_ValueChanged(object sender, EventArgs e)
        {
        lock (mLockObject)
            {
                state.Steps.First(s => s.StepGuid == testStepGuid).Probability = (int)numProbability.Value;
            }
            valueChanged(sender, e);
        }
    }
}
