﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using Aga.Controls.Tree;
using yats.ExecutionQueue;
using yats.Gui.Winforms.Components.TestRunTree.Properties;
using yats.TestCase.Interface;
using yats.TestRun;
using yats.TestRepositoryManager.Discovery;
using System.Linq;

namespace yats.Gui.Winforms.Components.TestRunTree
{
    // should be abstract but Tree.Node requires a concrete class with a parameterless constructor.
    internal class TestCaseTreeNode : Aga.Controls.Tree.Node<TestCaseTreeNode>
    {
        public virtual Image Icon
        {
            get;
            protected set;
        }

        public virtual Image StatusIcon
        {
            get;
            protected set;
        }

        public virtual String Name
        {
            get;
            protected set;
        }

        public virtual String Info
        {
            get;
            protected set;
        }

        public virtual TestStep TestStep
        {
            get;
            protected set;
        }

        public virtual string Results
        {
            get;
            protected set;
        }

        protected TreeModel model;
        public TestCaseTreeNode(TreeModel model)
        {
            this.model = model;
        }

        public TestCaseTreeNode()
        {
        }
    }

    internal class SingleStepNode : TestCaseTreeNode
    {
        public override Image Icon
        {
            get { return Resources.arrow_skip; }
        }

        public override Image StatusIcon
        {
            get 
            {
                try
                {

                    if (IsStepDisabledVisitor.Get(step))
                    {
                        return Resources.control_pause_blue;
                    }

                    if (model.currentlyRunningSteps.Contains(step))
                    {
                        return Resources.media_playback_start;
                    }

                    ITestResult result = step.Result;
                    
                    if (result == null)
                    {
                        return null;
                    }

                    switch (result.Result)
                    {
                        case ResultEnum.CANCELED:
                            return Resources.minus_circle_green_16;
                        case ResultEnum.FAIL:
                            return Resources.circle_red_16;
                        case ResultEnum.INCONCLUSIVE:
                            return Resources.help_circle_blue_16;
                        case ResultEnum.PASS:
                            return Resources.circle_green_16;
                        case ResultEnum.NOT_RUN:
                            return Resources.circle_grey_16;
                    }
                }
                catch
                {
                    //TODO race condition - step.Result may be set to null while painting is done. Will cause a red cross of death.
                    //lock on step?
                }
                return null; 
            }
        }

        public override String Name
        {
            [DebuggerStepThrough]
            get {
                if(model.ShowTestCaseType)
                {
                    string nameFromRepositoryManager = TestCaseRepository.Instance.Managers.GetTestName( step.TestCase );
                    if (step.Name == nameFromRepositoryManager)
                    {
                        return step.Name;
                    }
                    return string.Format("{0} [{1}]", step.Name, nameFromRepositoryManager);
                }
                return step.Name; 
            }
        }

        public override String Info
        {
            [DebuggerStepThrough]
            get { return StepInfoStringGenerator.Get(step); }
        }

        public override TestStep TestStep
        {
            [DebuggerStepThrough]
            get { return step; }
        }

        public override string Results
        {
            [DebuggerStepThrough]
            get { return model.Results.GetStatus(step); }
        }

        public TestStepSingle step;

        public SingleStepNode(TreeModel model, TestStepSingle step)
            : base(model)
        {
            this.step = step;
        }
    }

    internal class CompositeStepNode : TestCaseTreeNode
    {
        public override Image Icon
        {
            get { return Resources.arrow_turn_270; }
        }

        public override Image StatusIcon
        {
            get
            {
            if (IsStepDisabledVisitor.Get (step))
                {
                    return Resources.control_pause_blue;
                }
                
                if(model.currentlyRunningSteps.Contains( step ))
                {
                    return Resources.media_playback_start;
                }

                if (step.Result == null)
                {
                    //return Resources.circle_grey_16;
                    return null;
                }
                switch (step.Result.Result)
                {
                    case ResultEnum.CANCELED:
                        return Resources.minus_circle_green_16;
                    case ResultEnum.FAIL:
                        return Resources.circle_red_16;
                    case ResultEnum.INCONCLUSIVE:
                        return Resources.help_circle_blue_16;
                    case ResultEnum.PASS:
                        return Resources.circle_green_16;
                    case ResultEnum.NOT_RUN:
                        return Resources.circle_grey_16;
                }

                return null;
            }
        }

        public override String Name
        {
            [DebuggerStepThrough]
            get { return step.Name; }
        }

        public override String Info
        {
            [DebuggerStepThrough]
            get { return StepInfoStringGenerator.Get(step); }
        }

        public override TestStep TestStep
        {
            [DebuggerStepThrough]
            get { return step; }
        }

        public override string Results
        {
            [DebuggerStepThrough]
            get { return model.Results.GetStatus(step); }
        }

        internal TestStepComposite step;
        public CompositeStepNode(TreeModel model, TestStepComposite step)
            : base(model)
        {
            this.step = step;
        }
    }

    internal class TreeModel : Aga.Controls.Tree.TreeModel<TestCaseTreeNode>
    {
        TreeViewAdv tree;
        public ITestRun TestRun;
        public TestRunResultSummaryCollector Results;
        public bool ShowTestCaseType = false; // show test case type next to its name in the editor
        public TestRunTreePanel OwnerPanel;
        bool hideTopNode;
        Dictionary<TestStep, TestCaseTreeNode> StepNodeMap = new Dictionary<TestStep, TestCaseTreeNode>();

        internal HashSet<TestStep> currentlyRunningSteps = new HashSet<TestStep>( );

        public TreeModel(TestRunTreePanel ownerPanel, TreeViewAdv tree, ITestRun testRun, TestRunResultSummaryCollector results, bool hideTopNode):base()
        {
            this.OwnerPanel = ownerPanel;
            this.tree = tree;
            this.TestRun = testRun;
            this.Results = results;
            this.hideTopNode = hideTopNode;
        }

        private class StepVisitor : ITestStepVisitor
        {
            TreeModel model;
            internal bool isRoot;
            TestCaseTreeNode parent;
            int? insertIndex;
            TreeViewAdv tree;

            public StepVisitor(TreeModel model, TreeViewAdv tree, TestCaseTreeNode parent, bool isRoot)
            {
                this.model = model;
                this.isRoot = isRoot;
                this.parent = parent;
                this.tree = tree;
            }

            public StepVisitor(TreeModel model, TreeViewAdv tree, TestCaseTreeNode parent, int index)
            {
                this.model = model;
                this.isRoot = false;
                this.parent = parent;
                this.insertIndex = index;
                this.tree = tree;
            }

            public void AcceptSingle(TestStepSingle test)
            {
                var node = new SingleStepNode(model, test);
                if (insertIndex == null)
                {
                    parent.Nodes.Add(node);
                }
                else
                {
                    parent.Nodes.Insert(insertIndex.Value, node);
                }
                model.StepNodeMap.Add(test, node);
            }

            public void AcceptComposite(TestStepComposite test)
            {
                TestCaseTreeNode newParent = parent;
                if (isRoot)
                {   // for top-level step (which is not shown), only list child steps
                    isRoot = false;
                }
                else
                {
                    var node = new CompositeStepNode(model, test);
                    model.StepNodeMap.Add(test, node);
                    if (insertIndex == null)
                    {
                        parent.Nodes.Add(node);
                    }
                    else
                    {
                        parent.Nodes.Insert(insertIndex.Value, node);
                    }
                    var n = tree.AllNodes.First(x=>x.Tag == node);
                    n.IsExpanded = true;
                    newParent = node;
                }
                foreach (var s in test.Steps)
                {
                    s.Accept(new StepVisitor(model, tree, newParent, false));
                }             
            }
        }
                
        internal void SetStepRunning(TestStep step, bool isRunning)
        {
            if(isRunning)
            {
                currentlyRunningSteps.Add( step );
            }
            else
            {
                currentlyRunningSteps.Remove( step );
            }
        }

        private class StepRemoveVisitor : ITestStepVisitor
        {
            TreeModel model;
            TestStepComposite parent;
            TestStep child;

            public StepRemoveVisitor(TreeModel model, TestStepComposite parent, TestStep child)
            {
                this.model = model;
                this.parent = parent;
                this.child = child;
            }

            public void AcceptSingle(TestStepSingle step)
            {
                Debug.Assert(model.StepNodeMap.ContainsKey(step));
                Debug.Assert(model.StepNodeMap[parent].Nodes.Contains(model.StepNodeMap[step]));
                model.StepNodeMap[parent].Nodes.Remove(model.StepNodeMap[step]);
                model.StepNodeMap.Remove(step);
            }

            public void AcceptComposite(TestStepComposite step)
            {
                Debug.Assert(model.StepNodeMap.ContainsKey(step));
                Debug.Assert(model.StepNodeMap[parent].Nodes.Contains(model.StepNodeMap[step]));
                
                foreach (var s in step.Steps)
                {
                    s.Accept(new StepRemoveVisitor(model, step, s));
                }

                model.StepNodeMap[parent].Nodes.Remove(model.StepNodeMap[step]);
                model.StepNodeMap.Remove(step);
            }
        }

        internal void RemoveSteps(TestStepComposite parent, IEnumerable<TestStep> steps)
        {
            Debug.Assert(StepNodeMap.ContainsKey(parent));
            foreach (var step in steps)
            {
                step.Accept(new StepRemoveVisitor(this, parent, step));
            }
        }

        internal void InsertStep(TestStepComposite parent, TestStep step, int index)
        {
            StepVisitor visitor = new StepVisitor(this, tree, StepNodeMap[parent], false);
            step.Accept(visitor);
        }

        internal void InsertSteps(TestStepComposite parent, IEnumerable<TestStep> steps, int index)
        {
            foreach (var step in steps)
            {
                StepVisitor visitor = new StepVisitor(this, tree, StepNodeMap[parent], index++);
                step.Accept(visitor);
            }
        }

        internal void FullUpdate()
        {
            StepNodeMap.Clear();
            StepNodeMap.Add(TestRun.TestSequence, Root);
            StepVisitor visitor = new StepVisitor(this, tree, Root, hideTopNode);
            TestRun.TestSequence.Accept(visitor);
        }
    }
}
