﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using yats.ExecutionQueue;
using yats.Gui.Winforms.Components.ClipboardObjects;
using yats.Gui.Winforms.Components.TestRunTree.Components;
using yats.TestCase.Parameter;
using yats.TestRepositoryManager.Interface;
using yats.TestRun;
using yats.Utilities;
using yats.TestRun.Commands;
using Aga.Controls.Tree;
using yats.Utilities.Commands;
using yats.Gui.Winforms.Components.TestRunTree.Properties;
using yats.TestRepositoryManager.Discovery;

namespace yats.Gui.Winforms.Components.TestRunTree
{
    public partial class TestRunTreePanel : UserControl
    {
        public ITestRun TestRun
        {
            [DebuggerStepThrough]
            get
            {
                if (model == null)
                {
                    return null;
                }
                return model.TestRun;
            }
        }

        TreeModel model;
        CommandStack mCommandStack = new CommandStack();
        internal CommandStack CommandStack
        {
            get { return mCommandStack; }
        }
        

        public TestRunTreePanel()
        {
            InitializeComponent();
            CommandStack.OnModifiedChange += (sender, e) =>
            {
                TestRun.IsModified = e.IsModified;
            };
            Settings.Default.SettingChanging += ApplicationSettingChanging;
        }

        public void SetTestRun(ITestRun testRun, TestRunResultSummaryCollector results, bool hideTopNode, ITestExecutionStatus executionStatus)
        {
            model = new TreeModel(this, tree, testRun, results, hideTopNode);
            model.ShowTestCaseType = Settings.Default.ShowTestCaseType;
            tree.Model = model;
            model.FullUpdate();
            mCommandStack.Clear();                        
            tree.ExpandAll();
            if (this.executionStatus != null)
            {
                this.executionStatus.OnStatusChange -= executionStatus_OnStatusChange;
            }
            this.executionStatus = executionStatus;
            if (this.executionStatus != null)
            {
                this.executionStatus.OnStatusChange += executionStatus_OnStatusChange;
            }
        }

        void executionStatus_OnStatusChange(object sender, TestExecutionStatusEventArgs e)
        {
            UpdateMenuItemsEnabled();
        }
        
        internal void RemoveSelected()
        {
            mCommandStack.Execute(new RemoveSteps(TestRun, SelectedSteps, OnStepsRemoved, OnStepsAdded));
        }

        internal void ClearModel()
        {
            mCommandStack.Execute(new ClearTestRun(TestRun, () =>
            {
                model.FullUpdate();
                tree_SelectionChanged(this, null);
                if (StepsChanged != null)
                {
                    List<TestStep> changed = new List<TestStep>();
                    changed.Add(TestRun.TestSequence);
                    StepsChanged(this, new TestStepsEventArgs(changed));
                }
            }));
        }

        public event EventHandler<FileDropEventArgs> OnFileDrop;
        public event EventHandler<TestStepsEventArgs> StepDoubleClicked;
        public event EventHandler<TestStepsEventArgs> StepMiddleClicked;
        public event EventHandler<TestStepsEventArgs> StepsChanged;
        public event EventHandler<TestStepsEventArgs> StepsAdded;
        public event EventHandler<TestStepsEventArgs> SelectionChanged;
        public event EventHandler<TestStepsEventArgs> OnEditParameters;

        private void tree_SelectionChanged(object sender, EventArgs e)
        {
            if (SelectionChanged != null)
            {
                List<TestStep> result = new List<TestStep>();
                foreach (var selNode in tree.SelectedNodes)
                {
                    TestCaseTreeNode node = selNode.Tag as TestCaseTreeNode;

                    result.AddIfNotNull(node.TestStep);
                }

                SelectionChanged (this, new TestStepsEventArgs (result));
            }

            UpdateMenuItemsEnabled();
        }

        private void UpdateMenuItemsEnabled()
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { UpdateMenuItemsEnabled(); }));
                return;
            }
            bool areSelected;
            bool allSameType;
            bool allDisabled;
            bool allEnabled;
            bool singleSelected;
            bool isRunning = executionStatus.IsRunning;
            GetSelectedItemInfo(out allDisabled, out allEnabled, out areSelected, out allSameType, out singleSelected);

            //Enable Properties menu only if there are selected items and they all are either Single or Composite
            propertiesMenuItem.Enabled = !isRunning && areSelected && allSameType;

            disableToolStripMenuItem.Enabled = !isRunning && areSelected;
            enableToolStripMenuItem.Enabled = !isRunning && areSelected;
            disableToolStripMenuItem.Visible = !isRunning && allEnabled;
            enableToolStripMenuItem.Visible = !isRunning && areSelected && (allDisabled || !allEnabled);

            renameMenuItem.Enabled = !isRunning && areSelected;
            deleteToolStripMenuItem.Enabled = !isRunning && areSelected;
            //parametersToolStripMenuItem.Enabled = !IsRunning && areSelected;
            deleteToolStripMenuItem.Enabled = !isRunning && areSelected;

            cutToolStripMenuItem.Enabled = !isRunning && areSelected;
            copyToolStripMenuItem.Enabled = !isRunning && areSelected;
            pasteToolStripMenuItem.Enabled = !isRunning;
            pasteStepPropertiesToolStripMenuItem.Enabled = !isRunning && CanPasteStepProperties();
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public List<TestStep> SelectedSteps
        {
            [DebuggerStepThrough]
            get
            {
                List<TestStep> result = new List<TestStep>();
                foreach (var selNode in tree.SelectedNodes)
                {
                    TestCaseTreeNode node = selNode.Tag as TestCaseTreeNode;
                    result.AddIfNotNull(node.TestStep);
                }
                return result;
            }
            set
            {
                foreach (var node in tree.AllNodes)
                {
                    node.IsSelected = (value.Contains((node.Tag as TestCaseTreeNode).TestStep));
                }
            }
        }

        private void tree_DragOver(object sender, DragEventArgs e)
        {
            if (executionStatus.IsRunning)
            {
                e.Effect = DragDropEffects.None;
                return;
            }

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                //allow dropping an XML file to load it
                e.Effect = DragDropEffects.Move;
                return;
            }

            bool testCases = false;
            if (e.Data.GetDataPresent(typeof(IEnumerable<ITestCaseInfo>)))
            {
                testCases = true;
            }
            else if (e.Data.GetDataPresent(typeof(List<ITestCaseInfo>)))
            {
                testCases = true;// drag from test case list
            }
            else if (e.Data.GetDataPresent(typeof(ITestCaseInfo)))
            {
                testCases = true;
            }
            else if (e.Data.GetDataPresent(typeof(IEnumerable<TestStep>)))
            {
                testCases = true;
            }
            else if (e.Data.GetDataPresent(typeof(List<TestStep>)))
            {
                testCases = true;
            }
            else if (e.Data.GetDataPresent(typeof(TestStep)))
            {
                testCases = true;
            }
            else if (e.Data.GetDataPresent(typeof(StepDragInfo)))
            {
                StepDragInfo d = (StepDragInfo)(e.Data.GetData(typeof(StepDragInfo)));
                var steps = TestStepHierarchyFilter.Filter(d.steps);

                //this is supposed to prevent dragging a composite step into its own child step
                //if (tree.DropPosition.Position == Aga.Controls.Tree.NodePosition.Inside)
                {
                    var targetNode = (tree.DropPosition.Node == null) ? null : tree.DropPosition.Node.Tag as TestCaseTreeNode;

                    if (targetNode != null)
                    {
                        TestStep targetStep = targetNode.TestStep;
                        if (targetNode.TestStep is TestStepSingle)
                        {
                            targetStep = GetParentVisitor.Get(TestRun.TestSequence, targetStep);
                        }
                        foreach (var dropped in steps)
                        {
                            if (IsInHierarchyVisitor.Get(dropped, targetStep))
                            {
                                e.Effect = DragDropEffects.None;
                                return;
                            }
                        }
                    }
                }
                testCases = true;
            }


            if (!testCases)
            {
                e.Effect = DragDropEffects.None;
                return;
            }

            if ((e.KeyState & 0x08) == 0x08)
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.Move;
            }
        }

        private void tree_DragDrop(object sender, DragEventArgs e)
        {
            if (executionStatus.IsRunning)
            {
                return;
            }

            TestStep addTo = null;
            if (tree.DropPosition.Node == null)
            {
                addTo = TestRun.TestSequence;
            }
            else
            {
                addTo = (tree.DropPosition.Node.Tag as TestCaseTreeNode).TestStep;
            }

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                if (OnFileDrop == null)
                {
                    return;
                }
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                OnFileDrop (this, new FileDropEventArgs (null, files) );

                return;
            }

            {
                List<ITestCaseInfo> testsDropped = new List<ITestCaseInfo>();
                IEnumerable<ITestCaseInfo> t1 = (IEnumerable<ITestCaseInfo>)(e.Data.GetData(typeof(IEnumerable<ITestCaseInfo>)));
                testsDropped.AddIfNotNull(t1);
                List<ITestCaseInfo> t2 = (List<ITestCaseInfo>)(e.Data.GetData(typeof(List<ITestCaseInfo>)));
                testsDropped.AddIfNotNull(t2);
                ITestCaseInfo t3 = (ITestCaseInfo)(e.Data.GetData(typeof(ITestCaseInfo)));
                testsDropped.AddIfNotNull(t3);
                if (testsDropped.IsNullOrEmpty() == false)
                {
                    List<TestStep> steps = new List<TestStep>();
                    foreach (var t in testsDropped)
                    {
                        var ts = new TestStepSingle(t.TestCase, t.RepositoryManager);
                        steps.Add(ts);
                    }
                    mCommandStack.Execute(new InsertSteps(TestRun, steps, (InsertSteps.InsertPosition)Enum.Parse(typeof(InsertSteps.InsertPosition), tree.DropPosition.Position.ToString()), addTo, true,
                        OnStepsAdded,
                        OnStepsRemoved)
                        );
                    return;
                }
            }

            TreeViewAdv draggedFrom = this.tree;
            List<TestStep> testsDroppedTMP = new List<TestStep>();
            {
                IEnumerable<TestStep> t4 = (IEnumerable<TestStep>)(e.Data.GetData(typeof(IEnumerable<TestStep>)));
                testsDroppedTMP.AddIfNotNull(t4);
                List<TestStep> t5 = (List<TestStep>)(e.Data.GetData(typeof(List<TestStep>)));
                testsDroppedTMP.AddIfNotNull(t5);
                TestStep t6 = (TestStep)(e.Data.GetData(typeof(TestStep)));
                testsDroppedTMP.AddIfNotNull(t6);
                StepDragInfo t7 = (StepDragInfo)(e.Data.GetData(typeof(StepDragInfo)));
                if (t7 != null)
                {
                    draggedFrom = t7.sender;
                    testsDroppedTMP.AddRange(t7.steps);
                }
            }

            List<TestStep> stepsDropped = new List<TestStep>();
            // if multiple nodes are selected, find the common hierarchy - e.g. all the nodes belong and include a common parent(s)
            testsDroppedTMP = TestStepHierarchyFilter.Filter(testsDroppedTMP);
            bool needUpdateGuids = false;
            CompositeCommand command = new CompositeCommand();

            if (e.Effect == DragDropEffects.Copy) // clone copied test steps
            {
                foreach (var step in testsDroppedTMP)
                {
                    stepsDropped.Add(step.Clone());
                    needUpdateGuids = true;
                }
            }
            else if (object.ReferenceEquals(draggedFrom, this.tree))//if moved, remove from current test run and add to new collection to be added
            {
                command.Add(new RemoveSteps(TestRun, testsDroppedTMP, OnStepsRemoved, OnStepsAdded));
                foreach (var step in testsDroppedTMP)
                {
                    stepsDropped.Add(step);
                }
            }
            else if (draggedFrom != null && e.Effect == DragDropEffects.Move)
            {
                TreeModel model = draggedFrom.Model as TreeModel;
                if (model != null)
                {
                    //update the other test run
                    command.Add(new RemoveSteps(model.TestRun, testsDroppedTMP, model.OwnerPanel.OnStepsRemoved, model.OwnerPanel.OnStepsAdded));
                }
                foreach (var step in testsDroppedTMP)
                {
                    stepsDropped.Add(step);
                }                
            }

            if (stepsDropped.Count == 0)
            {
                return;
            }

            if (needUpdateGuids)
            {
                CreateUniqueGuidsForTestResultParameters(stepsDropped);
            }

            if (object.ReferenceEquals(draggedFrom, this.tree) == false)
            {
                CopyTestRunParameters(((draggedFrom as Aga.Controls.Tree.TreeViewAdv).Model as TreeModel).TestRun, TestRun, stepsDropped);
            }

            command.Add(new InsertSteps(TestRun, stepsDropped, (InsertSteps.InsertPosition)Enum.Parse(typeof(InsertSteps.InsertPosition), tree.DropPosition.Position.ToString()),addTo, false, OnStepsAdded, OnStepsRemoved));
            mCommandStack.Execute(command);
        }

        /// <summary>
        /// When moving/copying tests between test runs, the parameters with TEST_RUN scope must be copied accordingly
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="steps"></param>
        private void CopyTestRunParameters(ITestRun from, ITestRun to, List<TestStep> steps)
        {
            var parameters = ParameterEnumerator.GetParameters(steps);
            foreach (var param in parameters)
            {
                var globalValue = param.Value as GlobalValue;
                if (globalValue == null)
                {
                    continue;
                }
                if (GetParameterScopeVisitor.Get(globalValue, from.GlobalParameters) != ParameterScope.TEST_RUN)
                {
                    continue;
                }
                var manager = to.GlobalParameters.GetManagers(ParameterScope.TEST_RUN, true)[0];
                if (manager.Get(globalValue.ParameterID) == null)
                {
                    manager.Parameters[globalValue.ParameterID] = (GlobalParameter)from.GlobalParameters.Managers[globalValue.StorageId].Get(globalValue.ParameterID).Clone();
                }
                globalValue.StorageId = manager.ID;
            }
        }

        private void CreateUniqueGuidsForTestResultParameters(List<TestStep> steps)
        {
            Dictionary<Guid, Guid> replace = new Dictionary<Guid, Guid>();
            var allParameters = ParameterEnumerator.GetParameters(steps);
            // find all results, if GUID is assigned - change and remember what was changed to what
            foreach (var parameter in allParameters)
            {
                if (parameter.IsResult == false)
                {
                    continue;
                }
                yats.TestCase.Parameter.TestResultParameter value = parameter.Value as yats.TestCase.Parameter.TestResultParameter;
                if (value == null)
                {
                    continue;
                }
                Guid newGuid = Guid.NewGuid();
                replace[value.ResultID] = newGuid;
                value.ResultID = newGuid;
            }
            // go through parameters, replace GUIDs
            foreach (var parameter in allParameters)
            {
                if (parameter.IsResult)
                {
                    continue;
                }
                yats.TestCase.Parameter.TestResultParameter value = parameter.Value as yats.TestCase.Parameter.TestResultParameter;
                if (value == null)
                {
                    continue;
                }
                Guid newGuid;
                if (replace.TryGetValue(value.ResultID, out newGuid))
                {
                    value.ResultID = newGuid;
                }
            }
        }

        private ICommand CopyDraggedTests(List<TestStep> testsDropped, Aga.Controls.Tree.NodePosition nodePosition, TestStep addTo, bool newTestSteps)
        {
            if (TestRun.TestSequence is TestStepComposite == false)
            {
                return null; // can not add to a single step
            }

            return new InsertSteps(TestRun, testsDropped, (InsertSteps.InsertPosition)Enum.Parse(typeof(InsertSteps.InsertPosition), nodePosition.ToString()), addTo, newTestSteps, OnStepsAdded, OnStepsRemoved);
        }

        private void OnStepsRemoved(TestStepComposite parent, IEnumerable<TestStep> removedSteps)
        {
            model.RemoveSteps(parent, removedSteps);

            FindNullParametersVisitor.Validate(TestRun.TestSequence, TestRun.GlobalParameters, TestCaseRepository.Instance.Managers);
            if (StepsChanged != null)
            {
            StepsChanged (this, new TestStepsEventArgs (SelectedSteps));
            }
            tree_SelectionChanged(this, null);
        }

        private void OnStepsAdded(TestStepComposite parent, IEnumerable<TestStep> insertedSteps, int index, bool newTestSteps)
        {
            //CheckStepObjectReferencesVisitor.Check(TestRun.TestSequence);
            model.InsertSteps(parent, insertedSteps, index);
            if (StepsChanged != null)
            {
                StepsChanged(this, new TestStepsEventArgs(parent));
            }
            if (newTestSteps && StepsAdded != null)
            {
                StepsAdded(this, new TestStepsEventArgs(insertedSteps.ToList()));
            }
        }

        internal void SetStepRunning(TestStep step, bool isRunning)
        {
            model.SetStepRunning(step, isRunning);
            tree.Invalidate();
        }

        private class StepDragInfo
        {
            public List<TestStep> steps = new List<TestStep>();
            public TreeViewAdv sender;
        }

        private void tree_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if (executionStatus.IsRunning)
            {
                return;
            }

            System.Diagnostics.Debug.Assert(!this.InvokeRequired, "InvokeRequired");

            var drag = new StepDragInfo();
            drag.steps = this.SelectedSteps;
            drag.sender = tree;

            tree.DoDragDrop(drag, DragDropEffects.Move | DragDropEffects.Copy);
        }

        internal void propertiesMenuItem_Click(object sender, EventArgs e)
        {
            HashSet<Type> types = new HashSet<Type>();
            foreach (var step in SelectedSteps)
            {
                types.Add(step.GetType());
            }

            if (types.Count != 1) // this menu item should be disabled if types are mixed, check just in case...
            {
                return;
            }

            IEnumerable<ICommand> commands = null;

            if (types.Contains(typeof(TestStepComposite))) // show form for composite item(s)
            {
                List<TestStepComposite> steps = new List<TestStepComposite>();
                foreach (var s in SelectedSteps)
                {
                    steps.Add((TestStepComposite)s);
                }
                commands = new CompositeStepPropertiesForm(steps).GetStepConfigChanges();
            }
            else
            {
                List<TestStepSingle> steps = new List<TestStepSingle>();
                foreach (var s in SelectedSteps)
                {
                    steps.Add((TestStepSingle)s);
                }
                commands = new SingleStepPropertiesForm(steps).GetStepConfigChanges();
            }

            if (commands != null)
            {
                mCommandStack.Execute(new CompositeCommand((bool execute) =>
                {
                    tree.Invalidate();
                }, commands));
            }
        }

        private void parametersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OnEditParameters != null)
            {
            OnEditParameters (this, new TestStepsEventArgs (SelectedSteps));
            }
        }

        internal void GetSelectedItemInfo(out bool allDisabled, out bool allEnabled, out bool anyItemsSelected, out bool allOfSameType, out bool singleSelected)
        {
            allDisabled = true;
            allEnabled = true;
            anyItemsSelected = false;
            allOfSameType = false;

            HashSet<Type> types = new HashSet<Type>();

            foreach (var step in SelectedSteps)
            {
                allDisabled = allDisabled && IsStepDisabledVisitor.Get(step);
                allEnabled = allEnabled && !(IsStepDisabledVisitor.Get (step));
                types.Add(step.GetType());
                anyItemsSelected = true;
            }

            allOfSameType = types.Count == 1;
            singleSelected = SelectedSteps.Count == 1;
        }

        //event handler actually toggles the "enabled" status, therefore used in both Enable and Disable menu items. Just one menu item should be visible
        internal void disableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if all enabled - disable. If some enabled - enable all. If all enabled - disable all;
            bool allDisabled;
            bool allEnabled;
            bool areSelected;
            bool allSameType;
            bool singleSelected;

            GetSelectedItemInfo(out allDisabled, out allEnabled, out areSelected, out allSameType, out singleSelected);

            IStepExecution method;
            if (allDisabled || !allEnabled)
            {
                method = new SequentialExecutionModifier();
            }
            else
            {
                method = new StepDisabledModifier();
            }

            mCommandStack.Execute(new ChangeStepExecutionMethod<TestStep>(method, SelectedSteps, () =>
            {
                tree.Invalidate();
            }));
        }

        public void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoveSelected();
        }

        private void tree_NodeMouseClick(object sender, TreeNodeAdvMouseEventArgs e)
        {
            if (executionStatus.IsRunning)
            {
                return;
            }

            if (e.Button == MouseButtons.Middle && e.Clicks == 1 && e.Node != null)
            {
                if (StepMiddleClicked != null)
                {
                    StepMiddleClicked(this, new TestStepsEventArgs((e.Node.Tag as TestCaseTreeNode).TestStep));
                }
            }
        }

        private void tree_NodeMouseDoubleClick(object sender, Aga.Controls.Tree.TreeNodeAdvMouseEventArgs e)
        {
            if (executionStatus.IsRunning)
            {
                return;
            }

            if (StepDoubleClicked != null)
            {
                StepDoubleClicked (this, new TestStepsEventArgs (SelectedSteps));
            }
        }

        internal void renameMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedSteps.Count == 0)
            {
                return;
            }

            string value = SelectedSteps[0].Name;
            HashSet<string> autocomplete = new HashSet<string>();
            foreach (var step in SelectedSteps)
            {
                if (string.IsNullOrEmpty(step.Name) == false)
                {
                    autocomplete.Add(step.Name);
                }
            }

            if (yats.Gui.Winforms.Components.Util.InputBox.Get("Rename", "New name:", autocomplete.ToArray(), ref value) == DialogResult.OK)
            {
                mCommandStack.Execute(new RenameStep(value, SelectedSteps, () => {
                    if (StepsChanged != null)
                    {
                    StepsChanged (this, new TestStepsEventArgs (SelectedSteps));
                    }
                    tree.Invalidate();
                }));
            }
        }

        private ITestExecutionStatus executionStatus;

        private void nodeTextBox1_DrawText(object sender, Aga.Controls.Tree.NodeControls.DrawEventArgs e)
        {
            SingleStepNode single = e.Node.Tag as SingleStepNode;
            CompositeStepNode composite = e.Node.Tag as CompositeStepNode;
            bool disabled = false;

            if (single != null && IsStepDisabledVisitor.Get(TestRun.TestSequence, single.step))
            {
                disabled = true;
            }
            else if (composite != null && IsStepDisabledVisitor.Get(TestRun.TestSequence, composite.step))
            {
                disabled = true;
            }

            if (disabled)
            {
                e.TextColor = System.Drawing.Color.SlateGray;
            }
        }

        private void contextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            UpdateMenuItemsEnabled();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                List<TestStep> stepsDropped = new List<TestStep>();
                bool newTestSteps = false;

                ClipboardTestCaseContents testCaseInfo = ClipboardTestCaseContents.GetFromClipboard();
                if (testCaseInfo != null)
                {
                    newTestSteps = true;
                    // from TestCaseTree selection form
                    foreach (var testCaseUniqueId in testCaseInfo.UniqueTestCaseIds)
                    {
                        try
                        {
                            var ts = new TestStepSingle(testCaseUniqueId, TestCaseRepository.Instance.Managers);
                            stepsDropped.AddIfNotNull(ts);
                        }
                        catch { }
                    }
                }

                ClipboardTestStepContents steps = ClipboardTestStepContents.GetFromClipboard();
                if (steps != null)
                {
                    stepsDropped = steps.GetTestSteps();
                    TestHierarchyEnumerator.OnSingle(stepsDropped, single =>
                    {
                        // Test Case and Manager properties are ignored during serialization. Recreate
                        single.TestCase = TestCaseRepository.Instance.Managers.GetByUniqueId(single.UniqueTestId);
                        single.RepositoryManager = TestCaseRepository.Instance.Managers.GetManagerByTestUniqueId(single.UniqueTestId);
                    });
                    var loadedGlobals = yats.TestCase.Parameter.ExtractGlobalParameters.GetGlobals(steps.GetGlobals(), ParameterEnumerator.GetParameters(stepsDropped));
                    ParameterImporter.ImportGlobalParameters(steps.GetGlobals(), loadedGlobals, TestRun.GlobalParameters);
                }

                if (stepsDropped.Count == 0)
                {
                    return;
                }

                if (SelectedSteps.Count == 0)
                {
                    // add to top level
                    mCommandStack.Execute(CopyDraggedTests(stepsDropped, Aga.Controls.Tree.NodePosition.Inside, null, newTestSteps));
                }
                else
                {
                    // avoid 'collection is modified' exception
                    var copy = new List<Aga.Controls.Tree.TreeNodeAdv>(tree.SelectedNodes);
                    
                    CompositeCommand command = new CompositeCommand();

                    foreach (var selectedNode in copy)
                    {
                        TestCaseTreeNode targetNode = (TestCaseTreeNode)selectedNode.Tag;
                        if (targetNode == null)
                        {
                            continue; // should not happen
                        }

                        command.Add(CopyDraggedTests(stepsDropped,
                            (targetNode.TestStep is TestStepComposite) ? Aga.Controls.Tree.NodePosition.Inside : Aga.Controls.Tree.NodePosition.After,
                            targetNode.TestStep,
                            newTestSteps));
                        // if pasting to more than one group, make new clones each time. Otherwise duplicate references are created
                        var newClones = new List<TestStep>(stepsDropped.Count);
                        foreach (var ts in stepsDropped)
                        {
                            newClones.Add(ts.Clone());
                        }
                        stepsDropped = newClones;
                    }
                    mCommandStack.Execute(command);
                }
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex, "pasteToolStripMenuItem_Click");
            }
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                CopyStepsToClipboard();
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex, "copyToolStripMenuItem_Click");
            }
        }

        private void CopyStepsToClipboard()
        {
            var steps = SelectedSteps;
            if (steps.IsNullOrEmpty())
            {
                return;
            }
            steps = TestStepHierarchyFilter.Filter(steps);
            new ClipboardTestStepContents(steps.Select(x => x.Clone()).ToList(), TestRun.GlobalParameters).CopyToClipboard();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CopyStepsToClipboard();
            RemoveSelected();
        }

        private bool CanPasteStepProperties()
        {
            try
            {
                var clipboard = ClipboardObjects.ClipboardTestStepContents.GetFromClipboard();
                if (clipboard == null)
                {
                    return false;
                }
                List<TestStep> stepsInClipboard = clipboard.GetTestSteps();
                // only one step must be in clipboard in order to paste
                if (stepsInClipboard.Count != 1)
                {
                    return false;
                }
                bool areSelected;
                bool allSameType;
                bool allDisabled;
                bool allEnabled;
                bool singleSelected;

                GetSelectedItemInfo(out allDisabled, out allEnabled, out areSelected, out allSameType, out singleSelected);
                // can't paste if nothing is selected or composite and single types are mixed
                if (!areSelected || !allSameType)
                {
                    return false;
                }
                // can't paste if e.g. the copied item is a Composite and selected is Single
                if (stepsInClipboard[0].GetType() != SelectedSteps[0].GetType())
                {
                    return false;
                }
                return true;
            }
            catch { }
            return false;
        }

        private void pasteStepPropertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (!CanPasteStepProperties())
                {
                    return;
                }

                TestStep stepInClipboard = ClipboardObjects.ClipboardTestStepContents.GetFromClipboard().GetTestSteps()[0];

                mCommandStack.Execute(new ChangeStepProperties(stepInClipboard, SelectedSteps, () =>
                    {
                        tree.Invalidate();
                    }
                ));
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex, "pasteStepPropertiesToolStripMenuItem_Click");
            }
        }

        internal void AddCompositeToSelectedSteps()
        {
            CompositeCommand command = new CompositeCommand();
            var targets = SelectedSteps.IsNullOrEmpty() ? TestRun.TestSequence.ToSingleItemList() : SelectedSteps;
            foreach (var sel in targets)
            {
                command.Add(new InsertSteps(TestRun, new TestStepComposite(), InsertSteps.InsertPosition.Inside, sel, true, OnStepsAdded, OnStepsRemoved));
            }
            CommandStack.Execute(command);
        }

        internal void AddTestCases(List<TestStep> targets, params ITestCaseInfo [] testCasesToAdd)
        {
            
            CompositeCommand command = new CompositeCommand();
            if (targets != null && targets.Count > 0)
            {
                foreach (var sel in targets)
                {
                    var toAdd = testCasesToAdd.Select<ITestCaseInfo, TestStep>(t => new TestStepSingle(t.TestCase, t.RepositoryManager)).ToList();
                    command.Add(new InsertSteps(TestRun, toAdd, InsertSteps.InsertPosition.Inside, sel, true, OnStepsAdded, OnStepsRemoved));
                }
            }
            else
            {
                var toAdd = testCasesToAdd.Select<ITestCaseInfo, TestStep>(t => new TestStepSingle(t.TestCase, t.RepositoryManager)).ToList();
                command.Add(new InsertSteps(TestRun, toAdd, InsertSteps.InsertPosition.Inside, null, true, OnStepsAdded, OnStepsRemoved));
            }

            CommandStack.Execute(command);
        }

        private void ApplicationSettingChanging(object sender, System.Configuration.SettingChangingEventArgs e)
        {
            // apply settings immediately
            if (e.SettingName == "ShowTestCaseType")
            {
                model.ShowTestCaseType = (bool)e.NewValue;
                tree.Invalidate();
            }
        }

        
    }

    public class CheckStepObjectReferencesVisitor : ITestStepVisitor
    {
        List<object> knownSteps = new List<object>();

        public static void Check(TestStep root)
        {
#if DEBUG
            var visitor = new CheckStepObjectReferencesVisitor();
            root.Accept(visitor);
#endif
        }

        #region ITestStepVisitor Members

        [DebuggerStepThrough]
        public void AcceptSingle(TestStepSingle test)
        {
            foreach (var o in knownSteps)
            {
                if (object.ReferenceEquals(o, test))
                {
                    throw new Exception("CheckStepObjectReferences");
                }
            }
            knownSteps.Add(test);
        }

        public void AcceptComposite(TestStepComposite test)
        {
            foreach (var o in knownSteps)
            {
                if (object.ReferenceEquals(o, test))
                {
                    throw new Exception("CheckStepObjectReferences");
                }
            }
            knownSteps.Add(test);

            foreach (var step in test.Steps)
            {
                step.Accept(this);
            }
        }

        #endregion
    }

    public class FileDropEventArgs : EventArgs
    {
        public TestRunTreeForm Form;
        public string [] FileNames;
        public FileDropEventArgs(TestRunTreeForm form, params string [] fileNames)
        {
            Form = form;
            FileNames = fileNames;
        }
    }

}
