﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Windows.Forms;
using yats.TestRun;
using yats.TestRun.Commands;
using yats.Utilities.Commands;

namespace yats.Gui.Winforms.Components.TestRunTree
{
    public partial class TestRunPropertiesForm : Form
    {
        public TestRunPropertiesForm()
        {
            InitializeComponent( );
        }

        public static ICommand EditProperties(ITestRun testRun, string fileName)
        {
            TestRunPropertiesForm form = new TestRunPropertiesForm( );
            form.tbName.Text = testRun.Name;
            form.tbDescr.Text = testRun.Description;
            if (string.IsNullOrEmpty(fileName) == false) {
                form.fileLink.Text = fileName;
                form.fileLink.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            }
            else 
            {
                form.fileLink.Visible = false;
            }

            if(form.ShowDialog( ) == DialogResult.OK)
            {
                return new ChangeTestRunProperties(testRun, form.tbName.Text, form.tbDescr.Text);
            }
            return null;
        }

        private void fileLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            yats.Utilities.PlatformHelper.OpenFileBrowser( fileLink.Text );
        }
    }
}
