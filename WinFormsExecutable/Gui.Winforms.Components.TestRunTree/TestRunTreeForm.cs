﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using yats.ExecutionQueue;
using yats.Gui.Winforms.Components.Util;
using yats.TestRepositoryManager.Interface;
using yats.TestRun;
using System.Drawing;
using yats.TestRun.Commands;
using yats.Utilities;
using yats.Utilities.Commands;
using System.Linq;
using System.Security;
using yats.Gui.Winforms.Components.TestRunTree.Properties;

namespace yats.Gui.Winforms.Components.TestRunTree
{
    public partial class TestRunTreeForm : DockContent, IMergeableToolStripForm
    {
        public ITestRun TestRun
        {
            [DebuggerStepThrough]
            get
            {
                return testRunTreePanel.TestRun;
            }            
        }

        string _fileName;
        public string FileName
        {
            [DebuggerStepThrough]
            set {
                if(string.IsNullOrEmpty( value ) && !string.IsNullOrEmpty( _fileName ))
                {
                    // was not empty, now empty... code for non-reproduceable bug
                    yats.Utilities.CrashLog.Write( "Error: losing file name" );
                }
                _fileName = value;
                UpdateMenuItemsEnabled();
            }
            [DebuggerStepThrough]
            get {
                return _fileName;
            }
        }

        protected override string GetPersistString()
        {
            if (string.IsNullOrEmpty(FileName))
            {
                return null;
            }
            // Add extra information into the persist string for this document
            // so that it is available when deserialized.
            return GetType().ToString() + "," + FileName;
        }
        
        public List<TestStep> SelectedSteps
        {
            [DebuggerStepThrough]
            get { return testRunTreePanel.SelectedSteps; }
            [DebuggerStepThrough]
            set { testRunTreePanel.SelectedSteps = value; }
        }

        protected TestRunResultSummaryCollector resultTotal = new TestRunResultSummaryCollector();
        
        // contains test step results that are accumulated and displayed during test run. The results should be reset when starting a new test run
        public TestRunResultSummaryCollector Results
        {
            [DebuggerStepThrough]
            get { return resultTotal; }
        }

        public TestRunTreeForm()
        {
            InitializeComponent();
            ExecutionStatus = new TestExecutionStatus();
            testRunTreePanel.OnEditParameters += testRunTreePanel_OnEditParameters;
            testRunTreePanel.StepDoubleClicked += testRunTreePanel_OnEditParameters;
            testRunTreePanel.StepMiddleClicked += testRunTreePanel_OnStepsMiddleClick;
            testRunTreePanel.OnFileDrop += testRunTreePanel_OnFileDrop;
            testRunTreePanel.StepsChanged += testRunTreePanel_StepsChanged;
            testRunTreePanel.StepsAdded += testRunTreePanel_StepsAdded;
            testRunTreePanel.SelectionChanged += testRunTreePanel_SelectionChanged;

            renameMainMenuItem.Click += testRunTreePanel.renameMenuItem_Click;
            enableMainMenuItem.Click += testRunTreePanel.disableToolStripMenuItem_Click;
            enableMainMenuItem.Click += enableMainMenuItem_Click;// event handler to update menu item visibility. must be added after the actual step disabling
            disableMainMenuItem.Click += testRunTreePanel.disableToolStripMenuItem_Click;
            disableMainMenuItem.Click += enableMainMenuItem_Click; 
            propertiesMainMenuItem.Click += testRunTreePanel.propertiesMenuItem_Click;

            //DockHandler.TabColor = Color.BlanchedAlmond; // TODO: implement again, make pull request to dockpanelsuite

            testRunTreePanel.CommandStack.OnStatusChange += (object sender, EventArgs e) =>
            {
                UpdateMenuItemsEnabled();
            };
            showTestCaseTypeToolStripMenuItem.Checked = Settings.Default.ShowTestCaseType;
            showTestCaseTypeToolStripMenuItem.CheckedChanged += showTestCaseTypeToolStripMenuItem_CheckedChanged;
            Settings.Default.SettingChanging += ApplicationSettingChanging;
        }

        public TestRunTreeForm(ITestRun testRun, string fileName, bool hideTopNode)
            : this()
        {
            this.FileName = fileName;
            
            if(testRunTreePanel.TestRun!= null)
            {
                testRunTreePanel.TestRun.OnModifiedChanged -= testRun_OnModifiedChanged;
            }
            this.testRunTreePanel.SetTestRun( testRun, resultTotal, hideTopNode, this.executionStatus);
            testRun.OnModifiedChanged += testRun_OnModifiedChanged;
        
            UpdateFormTitle( );
            UpdateMenuItemsEnabled( );
        }

        void testRun_OnModifiedChanged(object sender, EventArgs e)
        {
            UpdateFormTitle( );
        }

        public void UpdateFormTitle()
        {
            if (this.InvokeRequired)     
            {         
                BeginInvoke( new MethodInvoker( delegate() { UpdateFormTitle(); } ) );     
                return;
            }
            if(string.IsNullOrEmpty( TestRun.Name ))
            {
                if(string.IsNullOrEmpty( FileName ))
                {
                    this.Text = "Test sequence";
                    this.ToolTipText = "";
                }
                else
                {
                    this.Text = Path.GetFileNameWithoutExtension( FileName );
                    this.ToolTipText = FileName;
                }
            }
            else
            {
                this.Text = TestRun.Name;
            }
            if(TestRun.IsModified)
            {
                this.Text += " *";
            }
        }

        void testRunTreePanel_OnStepsMiddleClick(object sender, TestStepsEventArgs e)
        {
            if (OnStepsMiddleClick != null)
            {
                OnStepsMiddleClick(this, new TestStepEventArgs(this, TestRun, e.Steps));
            }
        }

        
        void testRunTreePanel_OnEditParameters (object sender, TestStepsEventArgs e)
        {
            if (OnEditParameters != null)
            {
                OnEditParameters( this, new TestStepEventArgs(this, TestRun, e.Steps) );
            }
        }


        void testRunTreePanel_StepsAdded (object sender, TestStepsEventArgs e)
        {
            if (StepsAdded != null && e.Steps != null && e.Steps.Count > 0)
            {
                StepsAdded(this, new TestStepEventArgs(this, TestRun, e.Steps));
            }
        }

        private void addGroupBtn_Click(object sender, EventArgs e)
        {
            testRunTreePanel.AddCompositeToSelectedSteps();
        }

        private void removeBtn_Click(object sender, EventArgs e)
        {
            if (!IsActivated)
            {
                return;
            }
            testRunTreePanel.RemoveSelected();
        }

        private void clearAllButton_Click(object sender, EventArgs e)
        {
            testRunTreePanel.ClearModel();
        }

        public event EventHandler<RunPressedEventArgs> RunPressed;
        public event EventHandler RunScheduledPressed;
        public event EventHandler<TestStepEventArgs> OnStepsMiddleClick;
        public event EventHandler<TestStepEventArgs> OnEditParameters;
        public event EventHandler<TestStepEventArgs> StepsChanged;
        public event EventHandler<TestStepEventArgs> StepsAdded;
        public event EventHandler<RunPressedEventArgs> OnRunSelectedSteps;
        public event EventHandler<RunPressedEventArgs> OnRunFromSelectedStep;
        public event EventHandler<TestStepEventArgs> SelectionChanged;        
        public event EventHandler<TestRunFileEventArgs> SavedToFile;

        private ITestExecutionStatus executionStatus;
        public ITestExecutionStatus ExecutionStatus 
        {
            [DebuggerStepThrough]
            get {return executionStatus;}
            //[DebuggerStepThrough]
            set {
                if (executionStatus != null)
                {
                    executionStatus.OnStatusChange -= executionStatus_OnStatusChange;
                }
                executionStatus = value;
                if (executionStatus != null)
                {
                    executionStatus.OnStatusChange += executionStatus_OnStatusChange;
                }
            }
        }

        void executionStatus_OnStatusChange(object sender, TestExecutionStatusEventArgs e)
        {
            if (IsDisposed)
            {
                return;
            }

            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { executionStatus_OnStatusChange(sender, e); }));
            }
            else
            {
                UpdateMenuItemsEnabled();
            }
        }

        private void UpdateMenuItemsEnabled()
        {
            if (IsDisposed)
            {
                return;
            }

            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { UpdateMenuItemsEnabled(); }));
                return;
            }     

            bool areSelected;
            bool allSameType;
            bool allDisabled;
            bool allEnabled;
            bool singleSelected;

            int selSteps = testRunTreePanel.SelectedSteps.Count;

            testRunTreePanel.GetSelectedItemInfo(out allDisabled, out allEnabled, out areSelected, out allSameType, out singleSelected);
            
            exportAsTestCaseToolStripMenuItem.Enabled = singleSelected;

            runFromSelectedStepMenuItem.Enabled = runFromSelectedStepToolStripButton.Enabled = selSteps == 1;
            runSelectedStepsMenuItem.Enabled = runSelectedStepsToolStripButton.Enabled = selSteps > 0;

            propertiesMainMenuItem.Enabled = !ExecutionStatus.IsRunning && areSelected && allSameType;
            renameMainMenuItem.Enabled = !ExecutionStatus.IsRunning && areSelected;

            disableMainMenuItem.Enabled = !ExecutionStatus.IsRunning && areSelected;
            enableMainMenuItem.Enabled = !ExecutionStatus.IsRunning && areSelected;
            disableMainMenuItem.Visible = !ExecutionStatus.IsRunning && allEnabled;
            enableMainMenuItem.Visible = !ExecutionStatus.IsRunning && areSelected && (allDisabled || !allEnabled);

            this.cancelToolstripButton.Enabled = ExecutionStatus.IsRunning;
            this.cancelMenuItem.Enabled = ExecutionStatus.IsRunning;

            this.addGroupMenuItem.Enabled = this.addGroupBtn.Enabled = !ExecutionStatus.IsRunning;
            this.addTestCaseToolStripMenuItem.Enabled = !ExecutionStatus.IsRunning;
            this.clearAllMenuItem.Enabled = this.clearAllButton.Enabled = !ExecutionStatus.IsRunning;
            this.openButton.Enabled = !ExecutionStatus.IsRunning;
            //this.parametersMenuItem.Enabled = this.parametersBtn.Enabled = !IsRunning;
            this.removeMenuItem.Enabled = this.removeBtn.Enabled = !ExecutionStatus.IsRunning && areSelected;

            this.runToolstripButton.Enabled = !ExecutionStatus.IsRunning;
            this.runMenuItem.Enabled = !ExecutionStatus.IsRunning;
            this.runToolStripMenuItem_NoLog.Enabled = !ExecutionStatus.IsRunning;

            this.runScheduledToolstripButton.Enabled = !ExecutionStatus.IsRunning;
            this.runScheduledMenuItem.Enabled = !ExecutionStatus.IsRunning;

            //this.saveButton.Enabled = !IsRunning;
            this.selectAddTestBtn.Enabled = !ExecutionStatus.IsRunning;
            
            this.runSelectedStepsToolStripButton.Enabled = !ExecutionStatus.IsRunning;
            this.runSelectedStepsMenuItem.Enabled = !ExecutionStatus.IsRunning;
            this.runSelectedStepsToolStripMenuItem_NoLog.Enabled = !ExecutionStatus.IsRunning;

            this.runFromSelectedStepToolStripButton.Enabled = !ExecutionStatus.IsRunning;
            this.runFromSelectedStepMenuItem.Enabled = !ExecutionStatus.IsRunning;
            this.runFromSelectedStepToolStripMenuItem_NoLog.Enabled = !ExecutionStatus.IsRunning;

            this.undoMenuItem.Enabled = !ExecutionStatus.IsRunning && testRunTreePanel.CommandStack.CanUndo;
            this.redoMenuItem.Enabled = !ExecutionStatus.IsRunning && testRunTreePanel.CommandStack.CanRedo;

            this.reloadFromDiskToolStripMenuItem.Visible = (string.IsNullOrEmpty(FileName) == false);
            this.renameTestFileToolStripMenuItem.Visible = (string.IsNullOrEmpty(FileName) == false);
            this.deleteFromDiskToolStripMenuItem.Visible = (string.IsNullOrEmpty(FileName) == false);
            shellMenuToolStripMenuItem.Visible = string.IsNullOrEmpty(_fileName) == false;
            executionProgressBar.Visible = ExecutionStatus.IsRunning && executionProgressBar.Maximum > 0;
        }

        public void SetStepRunning(TestStep step, bool isRunning)
        {
            if (IsDisposed)
            {
                return;
            }
            
            if(this.InvokeRequired)
            {
                BeginInvoke( new MethodInvoker( delegate() { SetStepRunning( step, isRunning ); } ) );
            }
            else
            {
                testRunTreePanel.SetStepRunning( step, isRunning );
            }
        }

        private void parametersBtn_Click(object sender, EventArgs e)
        {
            if (OnEditParameters == null)
            {
                return;
            }

            OnEditParameters( this, new TestStepEventArgs(this, TestRun, testRunTreePanel.SelectedSteps ));
        }

        private class GetTestStepsVisitor : ITestStepVisitor
        {
            List<TestStep> result = new List<TestStep>();
            public static List<TestStep> Get(ITestRun testRun)
            {
                GetTestStepsVisitor visitor = new GetTestStepsVisitor();
                testRun.TestSequence.Accept(visitor);
                return visitor.result;
            }


            #region ITestStepVisitor Members

            public void AcceptSingle(TestStepSingle test)
            {
                result.Add(test);
            }

            public void AcceptComposite(TestStepComposite test)
            {
                result.AddRange(test.Steps);
            }

            #endregion
        }

        public class AddTestCasesRequestEventArgs : EventArgs
            {
            public IList<ITestCaseInfo> SelectedTestCases;
            public bool Selected;
            }
        
        public event EventHandler<AddTestCasesRequestEventArgs> AddTestCasesRequest;

        private void selectAddTestBtn_ButtonClick(object sender, EventArgs e)
        {
            if (AddTestCasesRequest == null)
            {
                return;
            }

            // test case selection failed / canceled
            AddTestCasesRequestEventArgs args = new AddTestCasesRequestEventArgs ();
            AddTestCasesRequest(this, args);
            if (args.Selected == false  || args.SelectedTestCases.IsNullOrEmpty())
            {
                return;
            }

            var targets = testRunTreePanel.SelectedSteps.IsNullOrEmpty()? TestRun.TestSequence.ToSingleItemList():testRunTreePanel.SelectedSteps;

            testRunTreePanel.AddTestCases(targets, args.SelectedTestCases.ToArray());
        }
        
        public void AddTestCases(params ITestCaseInfo [] testCases)
        {
            testRunTreePanel.AddTestCases(null, testCases);
        }
        
        void testRunTreePanel_StepsChanged (object sender, TestStepsEventArgs e)
        {
            if(StepsChanged != null)
            {
                StepsChanged( this, new TestStepEventArgs(this,this.TestRun, e.Steps ));
            }
        }

        private void runScheduledBtn_Click(object sender, EventArgs e)
        {
            if(this.RunScheduledPressed != null)
            {
                RunScheduledPressed( this, e );
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save( true, TestRun );
        }
        
        private void saveButton_Click(object sender, EventArgs e)
        {
            Save( false, TestRun);
        }

        private void renameTestFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string previousFile = FileName;
                if (Save(true, TestRun) && File.Exists(previousFile) && FileName.Equals(previousFile) == false)
                {
                    File.Delete(previousFile);
                }
            }
            catch { }
        }


        private void deleteFromDiskToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                File.Delete(FileName);
                this.FileName = null;
                testRunTreePanel.CommandStack.Clear();//this marks command stack as unchanged, so that we don't get a "save as" dialog again
                Close();
            }
            catch { }
        }

        private bool Save(bool showSaveDialog, ITestRun testRunToSave)
        {
            if(string.IsNullOrEmpty( this.FileName ) || showSaveDialog)
            {
				bool previousFileNameMatchesTestName = false;
                if (string.IsNullOrEmpty(this.FileName) == false && string.IsNullOrEmpty(testRunToSave.Name) == false)
                {
                    previousFileNameMatchesTestName = yats.Utilities.FileUtil.StripInvalidFileCharacters(testRunToSave.Name).ToLower().Equals(Path.GetFileNameWithoutExtension(this.FileName).ToLower());
				}
                string suggestedFileName = yats.Utilities.FileUtil.StripInvalidFileCharacters(testRunToSave.Name);

                SaveFileDialog dialog = (testRunToSave is SavedCompositeTestStep) ? saveTestCaseDialog : saveTestRunDialog;

                if(string.IsNullOrEmpty( suggestedFileName ) == false)
                {
                    dialog.FileName = suggestedFileName + dialog.DefaultExt;
                }

                if (dialog.ShowDialog() != DialogResult.OK)
                {
                    return false;
                }
                this.FileName = dialog.FileName;
                string newName = Path.GetFileNameWithoutExtension(dialog.FileName);
                if (string.IsNullOrEmpty(testRunToSave.Name))
                {
                    testRunToSave.Name = newName;
                    UpdateFormTitle( );
                }
				else if (previousFileNameMatchesTestName && MessageBox.Show(string.Format("Update test name to '{0}'?", newName), "Save as", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                    testRunToSave.Name = newName;
                    UpdateFormTitle( );
				}
                
            }

            try
            {
                SetStatusLabel( "Saving..." );

                // update and serialize parameter value type hashes. They should be verified when the test is loaded
                yats.TestCase.Parameter.UpdateParamTypeHashCode.Update(ParameterEnumerator.GetParameters(testRunToSave.TestSequence), testRunToSave.GlobalParameters);

                yats.Utilities.SerializationHelper.SerializeToFile<ITestRun>(testRunToSave, FileName);

                SetStatusLabel( "Saved as " + FileName );
                testRunTreePanel.CommandStack.SetSavedStatus();
                if (SavedToFile != null)
                {
                    SavedToFile(this, new TestRunFileEventArgs(this, testRunToSave, FileName));
                }
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show( ex.Message, "Error" );
                Utilities.CrashLog.Write(ex, "Test run serialization");
                return false;
            }
        }

        #region RUN menu items

        private void runBtn_Click(object sender, EventArgs e)
        {
            if (RunPressed != null)
            {
                RunPressed(this, new RunPressedEventArgs(this, TestRun, null, true));
            }
        }
        
        private void runFromSelectedStepToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OnRunFromSelectedStep != null)
            {
                OnRunFromSelectedStep(this, new RunPressedEventArgs(this, TestRun, testRunTreePanel.SelectedSteps, true));
            }
        }

        private void runSelectedStepsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (OnRunSelectedSteps != null)
            {
                OnRunSelectedSteps(this, new RunPressedEventArgs(this, TestRun, testRunTreePanel.SelectedSteps, true));
            }
        }
                
        private void runToolStripMenuItem_NoLog_Click(object sender, EventArgs e)
        {
            if (RunPressed != null)
            {
                RunPressed(this, new RunPressedEventArgs(this, TestRun, null, false));
            }
        }

        private void runSelectedStepsToolStripMenuItem_NoLog_Click(object sender, EventArgs e)
        {
            if (OnRunSelectedSteps != null)
            {
                OnRunSelectedSteps(this, new RunPressedEventArgs(this, TestRun, testRunTreePanel.SelectedSteps, false));
            }
        }

        private void runFromSelectedStepToolStripMenuItem_NoLog_Click(object sender, EventArgs e)
        {
            if (OnRunFromSelectedStep != null)
            {
                OnRunFromSelectedStep(this, new RunPressedEventArgs(this, TestRun, testRunTreePanel.SelectedSteps, false));
            }
        }
        #endregion

        #region status label
        private void SetStatusLabel(string status)
        {
            statusLabel.Text = status;
            statusLabel.Visible = true;
            hideStatusTimer.Stop();
            hideStatusTimer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            statusLabel.Visible = false;
        }
        #endregion

        #region Load test run
        public event EventHandler OpenClicked;
        public event EventHandler ReloadFromDiskClicked;

        private void openButton_Click(object sender, EventArgs e)
        {
            if (OpenClicked != null)
            {
                OpenClicked(this, e);
            }
        }

        public void SetTestRun(ITestRun testRun, string fileName, bool hideTopNode)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { SetTestRun(testRun, fileName, hideTopNode); }));
                return;
            }  
            this.FileName = fileName;
        
            if (testRunTreePanel.TestRun != null)
            {
                testRunTreePanel.TestRun.OnModifiedChanged -= testRun_OnModifiedChanged;
            }
            ExecutionStatus = new TestExecutionStatus();
            this.testRunTreePanel.SetTestRun(testRun, resultTotal, hideTopNode, ExecutionStatus);
            testRun.OnModifiedChanged += testRun_OnModifiedChanged;
        
            UpdateFormTitle( );
            
            if(StepsChanged != null)
            {
                List<TestStep> changed = new List<TestStep>( );
                changed.Add( TestRun.TestSequence );
                StepsChanged( this, new TestStepEventArgs(this, TestRun, changed ));
            }
        }

        public event EventHandler<FileDropEventArgs> OnFileDrop;

        void testRunTreePanel_OnFileDrop (object sender, FileDropEventArgs e)
        {
            if (OnFileDrop != null)
            {
                e.Form = this;
                OnFileDrop(sender, e);
            }
        }
        
        #endregion
        
        private void cancelButton_Click(object sender, EventArgs e)
        {
            if(OnCancel != null)
            {
                OnCancel( this, null );
            }
        }

        public event EventHandler OnCancel;

        public void SetProgressMaxItems(bool isEstimated, int progressMax)
        {
            if (IsDisposed)
            {
                return;
            }

            if(this.InvokeRequired)
            {
                BeginInvoke( new MethodInvoker( delegate() { SetProgressMaxItems(isEstimated, progressMax ); } ) );
            }
            else
            {
                executionProgressBar.Maximum = isEstimated? progressMax : 0;
                executionProgressBar.Value = 0;
            }
        }

        public void IncrementProgress()
        {
            if (IsDisposed)
            {
                return;
            }

            if(this.InvokeRequired)
            {
                BeginInvoke( new MethodInvoker( delegate() { IncrementProgress( ); } ) );
            }
            else
            {
                try
                {
                    if (executionProgressBar.Value < executionProgressBar.Maximum)
                    {
                        executionProgressBar.Value++;
                    }
                }
                catch { }
            }
        }

        private void testRunTreePanel_SelectionChanged(object sender, TestStepsEventArgs e)
        {            
            if(SelectionChanged != null)
            {
                SelectionChanged( this, new TestStepEventArgs(this, this.TestRun, e.Steps ));
            }
            UpdateMenuItemsEnabled();
        }

        private void propertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(testRunTreePanel.CommandStack.Execute(TestRunPropertiesForm.EditProperties( TestRun, FileName )))
            {
                SetStatusLabel( "Properties updated" );
            }
        }

        private void TestRunTreeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(!TestRun.IsModified || !Settings.Default.AskToSave)
            {
                return;
            }
            Activate();
            switch(MessageBox.Show( string.Format("Save test sequence?\r\n{0}", TestRun.Name), "Save", MessageBoxButtons.YesNoCancel ))
            {
                case DialogResult.Yes:
                    if(Save( false, TestRun ))
                    {
                        return;
                    }
                    e.Cancel = true;
                    break;
                case DialogResult.No:
                    break;
                case DialogResult.Cancel:
                    e.Cancel = true;
                    break;
            }
        }

        private void enableMainMenuItem_Click(object sender, EventArgs e)
        {
            UpdateMenuItemsEnabled();
        }

        private void reloadFromDiskToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(ReloadFromDiskClicked != null)
            {
                ReloadFromDiskClicked( this, e );
            }
        }

        #region IMergeableToolStripForm Members

        public ToolStrip ChildToolStrip
        {
            get
            {
                return toolStrip;
            }
            set
            {
                toolStrip = value;
            }
        }

        #endregion

        #region Tab context menu
        
        public event EventHandler CloseOtherTabsPressed;

        private void closeTabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void closeOtherTestsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(CloseOtherTabsPressed != null)
            {
                CloseOtherTabsPressed( this, e );
            }
        }

        private void copyFullPathToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty( this.FileName ) == false)
            {
                Clipboard.SetText( this.FileName );
            }
        }

        private void openContainingFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty( this.FileName ) == false)
            {
                yats.Utilities.PlatformHelper.OpenFileBrowser( this.FileName );
            }
        }

        #endregion

        private void exportAsTestCaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (testRunTreePanel.SelectedSteps.Count != 1) {
                return;
            }
            ITestRun exported = new SavedCompositeTestStep(testRunTreePanel.SelectedSteps[0], TestRun.GlobalParameters);
            Save(true, exported);            
        }

        private void undoMenuItem_Click(object sender, EventArgs e)
        {
            if (executionStatus.IsRunning)
            {
                return;
            }
            testRunTreePanel.CommandStack.Undo();
        }

        private void redoMenuItem_Click(object sender, EventArgs e)
        {
            if (executionStatus.IsRunning)
            {
                return;
            }
            testRunTreePanel.CommandStack.Redo();
        }

        public CommandStack CommandStack
        {
            get { return testRunTreePanel.CommandStack; }
        }
        
        private void shellMenuToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            try
            {
                ShellContextMenu scm = new ShellContextMenu();
                scm.ShowContextMenu(new FileInfo[] { new FileInfo(_fileName) }, Cursor.Position);
            }
            catch (SecurityException ex)
            {
                Console.WriteLine("Call to native code containing method was stopped by code access security! {0}", ex.Message);
            }
        }
        
        private void showTestCaseTypeToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.ShowTestCaseType = showTestCaseTypeToolStripMenuItem.Checked;
            Settings.Default.Save();
        }

        private void ApplicationSettingChanging(object sender, System.Configuration.SettingChangingEventArgs e)
        {
            // apply settings immediately
            if (e.SettingName == "ShowTestCaseType")
            {
                showTestCaseTypeToolStripMenuItem.Checked = (bool)e.NewValue;
            }
        }
    }

    public class TestStepEventArgs : EventArgs
        {
        public TestRunTreeForm Sender;
        public ITestRun TestRun;
        public IList<TestStep> Steps;
        public TestStepEventArgs (TestRunTreeForm sender, ITestRun testRun, IList<TestStep> steps)
            {
            Sender = sender;
            TestRun = testRun;
            Steps = steps;
            }
        }

    public class RunPressedEventArgs : EventArgs{
        public TestRunTreeForm Sender;
        public ITestRun TestRun;
        public List<TestStep> Steps;
        public bool Log;

        public RunPressedEventArgs (TestRunTreeForm sender, ITestRun testRun, List<TestStep> steps, bool log){
            Sender = sender;
            TestRun = testRun;
            Steps = steps;
            Log = log;
            }
        }

    public class TestRunFileEventArgs : EventArgs
        {
        public TestRunTreeForm Sender;
        public ITestRun TestRun;
        public string FileName;
        public TestRunFileEventArgs (TestRunTreeForm sender, ITestRun testRun, string fileName){
            Sender = sender;
            TestRun = testRun;
            FileName = fileName;
            }
        }
}
