﻿using System;
using yats.TestRepositoryManager.Interface;

namespace yats.Gui.Winforms.Components.TestRunTree
{
    partial class TestRunTreeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestRunTreeForm));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.executionProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel7 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel8 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel9 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel10 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel11 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel12 = new System.Windows.Forms.ToolStripStatusLabel();
            this.testRunTreePanel = new yats.Gui.Winforms.Components.TestRunTree.TestRunTreePanel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reloadFromDiskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameTestFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteFromDiskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportAsTestCaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.propertiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.showTestCaseTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addTestCaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addGroupMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.renameMainMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disableMainMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enableMainMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearAllMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.parametersMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.propertiesMainMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runSelectedStepsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runFromSelectedStepMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.withoutLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem_NoLog = new System.Windows.Forms.ToolStripMenuItem();
            this.runSelectedStepsToolStripMenuItem_NoLog = new System.Windows.Forms.ToolStripMenuItem();
            this.runFromSelectedStepToolStripMenuItem_NoLog = new System.Windows.Forms.ToolStripMenuItem();
            this.runScheduledMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.cancelMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.openButton = new System.Windows.Forms.ToolStripButton();
            this.saveButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.runToolstripButton = new System.Windows.Forms.ToolStripSplitButton();
            this.runScheduledToolstripButton = new System.Windows.Forms.ToolStripMenuItem();
            this.runSelectedStepsToolStripButton = new System.Windows.Forms.ToolStripMenuItem();
            this.runFromSelectedStepToolStripButton = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelToolstripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.addGroupBtn = new System.Windows.Forms.ToolStripButton();
            this.selectAddTestBtn = new System.Windows.Forms.ToolStripButton();
            this.removeBtn = new System.Windows.Forms.ToolStripSplitButton();
            this.clearAllButton = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.parametersBtn = new System.Windows.Forms.ToolStripButton();
            this.saveTestRunDialog = new System.Windows.Forms.SaveFileDialog();
            this.hideStatusTimer = new System.Windows.Forms.Timer(this.components);
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel6 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.closeTabToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeOtherTestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.copyFullPathToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openContainingFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shellMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveTestCaseDialog = new System.Windows.Forms.SaveFileDialog();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.tabContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.testRunTreePanel);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(895, 427);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(895, 449);
            this.toolStripContainer1.TabIndex = 1;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.menuStrip1);
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.executionProgressBar,
            this.toolStripStatusLabel7,
            this.toolStripStatusLabel8,
            this.toolStripStatusLabel9,
            this.toolStripStatusLabel10,
            this.toolStripStatusLabel11,
            this.toolStripStatusLabel12});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(895, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // executionProgressBar
            // 
            this.executionProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.executionProgressBar.Name = "executionProgressBar";
            this.executionProgressBar.Size = new System.Drawing.Size(150, 16);
            this.executionProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // toolStripStatusLabel7
            // 
            this.toolStripStatusLabel7.Name = "toolStripStatusLabel7";
            this.toolStripStatusLabel7.Size = new System.Drawing.Size(49, 17);
            this.toolStripStatusLabel7.Text = "Legend:";
            // 
            // toolStripStatusLabel8
            // 
            this.toolStripStatusLabel8.Image = global::yats.Gui.Winforms.Components.TestRunTree.Properties.Resources.circle_green_16;
            this.toolStripStatusLabel8.Name = "toolStripStatusLabel8";
            this.toolStripStatusLabel8.Size = new System.Drawing.Size(46, 17);
            this.toolStripStatusLabel8.Text = "Pass";
            // 
            // toolStripStatusLabel9
            // 
            this.toolStripStatusLabel9.Image = global::yats.Gui.Winforms.Components.TestRunTree.Properties.Resources.circle_red_16;
            this.toolStripStatusLabel9.Name = "toolStripStatusLabel9";
            this.toolStripStatusLabel9.Size = new System.Drawing.Size(41, 17);
            this.toolStripStatusLabel9.Text = "Fail";
            // 
            // toolStripStatusLabel10
            // 
            this.toolStripStatusLabel10.Image = global::yats.Gui.Winforms.Components.TestRunTree.Properties.Resources.help_circle_blue_16;
            this.toolStripStatusLabel10.Name = "toolStripStatusLabel10";
            this.toolStripStatusLabel10.Size = new System.Drawing.Size(89, 17);
            this.toolStripStatusLabel10.Text = "Inconclusive";
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Image = global::yats.Gui.Winforms.Components.TestRunTree.Properties.Resources.circle_grey_16;
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Size = new System.Drawing.Size(64, 17);
            this.toolStripStatusLabel11.Text = "Not run";
            // 
            // toolStripStatusLabel12
            // 
            this.toolStripStatusLabel12.Image = global::yats.Gui.Winforms.Components.TestRunTree.Properties.Resources.minus_circle_green_16;
            this.toolStripStatusLabel12.Name = "toolStripStatusLabel12";
            this.toolStripStatusLabel12.Size = new System.Drawing.Size(72, 17);
            this.toolStripStatusLabel12.Text = "Canceled";
            // 
            // testRunTreePanel
            // 
            this.testRunTreePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.testRunTreePanel.Location = new System.Drawing.Point(0, 0);
            this.testRunTreePanel.Name = "testRunTreePanel";
            this.testRunTreePanel.Size = new System.Drawing.Size(895, 427);
            this.testRunTreePanel.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.editToolStripMenuItem,
            this.runToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(168, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reloadFromDiskToolStripMenuItem,
            this.toolStripSeparator4,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.renameTestFileToolStripMenuItem,
            this.deleteFromDiskToolStripMenuItem,
            this.exportAsTestCaseToolStripMenuItem,
            this.propertiesToolStripMenuItem,
            this.toolStripSeparator8});
            this.fileToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.MatchOnly;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // reloadFromDiskToolStripMenuItem
            // 
            this.reloadFromDiskToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.reloadFromDiskToolStripMenuItem.MergeIndex = 3;
            this.reloadFromDiskToolStripMenuItem.Name = "reloadFromDiskToolStripMenuItem";
            this.reloadFromDiskToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.reloadFromDiskToolStripMenuItem.Text = "Re&load from disk";
            this.reloadFromDiskToolStripMenuItem.Click += new System.EventHandler(this.reloadFromDiskToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.toolStripSeparator4.MergeIndex = 4;
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(211, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.saveToolStripMenuItem.MergeIndex = 5;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.saveAsToolStripMenuItem.MergeIndex = 6;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.saveAsToolStripMenuItem.Text = "Save &As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // renameTestFileToolStripMenuItem
            // 
            this.renameTestFileToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.renameTestFileToolStripMenuItem.MergeIndex = 7;
            this.renameTestFileToolStripMenuItem.Name = "renameTestFileToolStripMenuItem";
            this.renameTestFileToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.renameTestFileToolStripMenuItem.Text = "Rena&me test file";
            this.renameTestFileToolStripMenuItem.Click += new System.EventHandler(this.renameTestFileToolStripMenuItem_Click);
            // 
            // deleteFromDiskToolStripMenuItem
            // 
            this.deleteFromDiskToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.deleteFromDiskToolStripMenuItem.MergeIndex = 8;
            this.deleteFromDiskToolStripMenuItem.Name = "deleteFromDiskToolStripMenuItem";
            this.deleteFromDiskToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.deleteFromDiskToolStripMenuItem.Text = "&Close and delete from disk";
            this.deleteFromDiskToolStripMenuItem.Click += new System.EventHandler(this.deleteFromDiskToolStripMenuItem_Click);
            // 
            // exportAsTestCaseToolStripMenuItem
            // 
            this.exportAsTestCaseToolStripMenuItem.Enabled = false;
            this.exportAsTestCaseToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.exportAsTestCaseToolStripMenuItem.MergeIndex = 9;
            this.exportAsTestCaseToolStripMenuItem.Name = "exportAsTestCaseToolStripMenuItem";
            this.exportAsTestCaseToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.exportAsTestCaseToolStripMenuItem.Text = "Export as test case";
            this.exportAsTestCaseToolStripMenuItem.Click += new System.EventHandler(this.exportAsTestCaseToolStripMenuItem_Click);
            // 
            // propertiesToolStripMenuItem
            // 
            this.propertiesToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.propertiesToolStripMenuItem.MergeIndex = 10;
            this.propertiesToolStripMenuItem.Name = "propertiesToolStripMenuItem";
            this.propertiesToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.propertiesToolStripMenuItem.Text = "Test p&roperties";
            this.propertiesToolStripMenuItem.Click += new System.EventHandler(this.propertiesToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.toolStripSeparator8.MergeIndex = 11;
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(211, 6);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator10,
            this.showTestCaseTypeToolStripMenuItem});
            this.viewToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.MatchOnly;
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "&View";
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.toolStripSeparator10.MergeIndex = 9;
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(174, 6);
            // 
            // showTestCaseTypeToolStripMenuItem
            // 
            this.showTestCaseTypeToolStripMenuItem.CheckOnClick = true;
            this.showTestCaseTypeToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.showTestCaseTypeToolStripMenuItem.MergeIndex = 10;
            this.showTestCaseTypeToolStripMenuItem.Name = "showTestCaseTypeToolStripMenuItem";
            this.showTestCaseTypeToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.showTestCaseTypeToolStripMenuItem.Text = "Show test case type";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTestCaseToolStripMenuItem,
            this.addGroupMenuItem,
            this.undoMenuItem,
            this.redoMenuItem,
            this.toolStripSeparator6,
            this.renameMainMenuItem,
            this.removeMenuItem,
            this.disableMainMenuItem,
            this.enableMainMenuItem,
            this.clearAllMenuItem,
            this.toolStripSeparator7,
            this.parametersMenuItem,
            this.propertiesMainMenuItem});
            this.editToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.editToolStripMenuItem.MergeIndex = 1;
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // addTestCaseToolStripMenuItem
            // 
            this.addTestCaseToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("addTestCaseToolStripMenuItem.Image")));
            this.addTestCaseToolStripMenuItem.Name = "addTestCaseToolStripMenuItem";
            this.addTestCaseToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.N)));
            this.addTestCaseToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.addTestCaseToolStripMenuItem.Text = "&Add test case...";
            this.addTestCaseToolStripMenuItem.Click += new System.EventHandler(this.selectAddTestBtn_ButtonClick);
            // 
            // addGroupMenuItem
            // 
            this.addGroupMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("addGroupMenuItem.Image")));
            this.addGroupMenuItem.Name = "addGroupMenuItem";
            this.addGroupMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.addGroupMenuItem.Size = new System.Drawing.Size(228, 22);
            this.addGroupMenuItem.Text = "Add &group";
            this.addGroupMenuItem.Click += new System.EventHandler(this.addGroupBtn_Click);
            // 
            // undoMenuItem
            // 
            this.undoMenuItem.Image = global::yats.Gui.Winforms.Components.TestRunTree.Properties.Resources.arrow_return_180_left;
            this.undoMenuItem.Name = "undoMenuItem";
            this.undoMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoMenuItem.Size = new System.Drawing.Size(228, 22);
            this.undoMenuItem.Text = "Undo";
            this.undoMenuItem.Click += new System.EventHandler(this.undoMenuItem_Click);
            // 
            // redoMenuItem
            // 
            this.redoMenuItem.Image = global::yats.Gui.Winforms.Components.TestRunTree.Properties.Resources.arrow_return;
            this.redoMenuItem.Name = "redoMenuItem";
            this.redoMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoMenuItem.Size = new System.Drawing.Size(228, 22);
            this.redoMenuItem.Text = "Redo";
            this.redoMenuItem.Click += new System.EventHandler(this.redoMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(225, 6);
            // 
            // renameMainMenuItem
            // 
            this.renameMainMenuItem.Enabled = false;
            this.renameMainMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("renameMainMenuItem.Image")));
            this.renameMainMenuItem.Name = "renameMainMenuItem";
            this.renameMainMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.renameMainMenuItem.Size = new System.Drawing.Size(228, 22);
            this.renameMainMenuItem.Text = "Rena&me";
            // 
            // removeMenuItem
            // 
            this.removeMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("removeMenuItem.Image")));
            this.removeMenuItem.Name = "removeMenuItem";
            this.removeMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.removeMenuItem.Size = new System.Drawing.Size(228, 22);
            this.removeMenuItem.Text = "Remo&ve";
            this.removeMenuItem.Click += new System.EventHandler(this.removeBtn_Click);
            // 
            // disableMainMenuItem
            // 
            this.disableMainMenuItem.Enabled = false;
            this.disableMainMenuItem.Name = "disableMainMenuItem";
            this.disableMainMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.disableMainMenuItem.Size = new System.Drawing.Size(228, 22);
            this.disableMainMenuItem.Text = "&Disable";
            // 
            // enableMainMenuItem
            // 
            this.enableMainMenuItem.Enabled = false;
            this.enableMainMenuItem.Name = "enableMainMenuItem";
            this.enableMainMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.enableMainMenuItem.Size = new System.Drawing.Size(228, 22);
            this.enableMainMenuItem.Text = "E&nable";
            this.enableMainMenuItem.Visible = false;
            // 
            // clearAllMenuItem
            // 
            this.clearAllMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("clearAllMenuItem.Image")));
            this.clearAllMenuItem.Name = "clearAllMenuItem";
            this.clearAllMenuItem.Size = new System.Drawing.Size(228, 22);
            this.clearAllMenuItem.Text = "&Clear all";
            this.clearAllMenuItem.Click += new System.EventHandler(this.clearAllButton_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(225, 6);
            // 
            // parametersMenuItem
            // 
            this.parametersMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("parametersMenuItem.Image")));
            this.parametersMenuItem.Name = "parametersMenuItem";
            this.parametersMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.parametersMenuItem.Size = new System.Drawing.Size(228, 22);
            this.parametersMenuItem.Text = "Param&eters";
            this.parametersMenuItem.Click += new System.EventHandler(this.parametersBtn_Click);
            // 
            // propertiesMainMenuItem
            // 
            this.propertiesMainMenuItem.Name = "propertiesMainMenuItem";
            this.propertiesMainMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.propertiesMainMenuItem.Size = new System.Drawing.Size(228, 22);
            this.propertiesMainMenuItem.Text = "Step p&roperties";
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runMenuItem,
            this.runSelectedStepsMenuItem,
            this.runFromSelectedStepMenuItem,
            this.withoutLogToolStripMenuItem,
            this.runScheduledMenuItem,
            this.toolStripSeparator5,
            this.cancelMenuItem});
            this.runToolStripMenuItem.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.runToolStripMenuItem.MergeIndex = 3;
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.runToolStripMenuItem.Text = "&Run";
            // 
            // runMenuItem
            // 
            this.runMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("runMenuItem.Image")));
            this.runMenuItem.Name = "runMenuItem";
            this.runMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.runMenuItem.Size = new System.Drawing.Size(214, 22);
            this.runMenuItem.Text = "&Run";
            this.runMenuItem.Click += new System.EventHandler(this.runBtn_Click);
            // 
            // runSelectedStepsMenuItem
            // 
            this.runSelectedStepsMenuItem.Enabled = false;
            this.runSelectedStepsMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("runSelectedStepsMenuItem.Image")));
            this.runSelectedStepsMenuItem.Name = "runSelectedStepsMenuItem";
            this.runSelectedStepsMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.runSelectedStepsMenuItem.Size = new System.Drawing.Size(214, 22);
            this.runSelectedStepsMenuItem.Text = "Run &selected step(s)";
            this.runSelectedStepsMenuItem.Click += new System.EventHandler(this.runSelectedStepsToolStripMenuItem_Click);
            // 
            // runFromSelectedStepMenuItem
            // 
            this.runFromSelectedStepMenuItem.Enabled = false;
            this.runFromSelectedStepMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("runFromSelectedStepMenuItem.Image")));
            this.runFromSelectedStepMenuItem.Name = "runFromSelectedStepMenuItem";
            this.runFromSelectedStepMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.runFromSelectedStepMenuItem.Size = new System.Drawing.Size(214, 22);
            this.runFromSelectedStepMenuItem.Text = "Run &from selected step";
            this.runFromSelectedStepMenuItem.Click += new System.EventHandler(this.runFromSelectedStepToolStripMenuItem_Click);
            // 
            // withoutLogToolStripMenuItem
            // 
            this.withoutLogToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runToolStripMenuItem_NoLog,
            this.runSelectedStepsToolStripMenuItem_NoLog,
            this.runFromSelectedStepToolStripMenuItem_NoLog});
            this.withoutLogToolStripMenuItem.Name = "withoutLogToolStripMenuItem";
            this.withoutLogToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.withoutLogToolStripMenuItem.Text = "&Without log";
            // 
            // runToolStripMenuItem_NoLog
            // 
            this.runToolStripMenuItem_NoLog.Name = "runToolStripMenuItem_NoLog";
            this.runToolStripMenuItem_NoLog.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F5)));
            this.runToolStripMenuItem_NoLog.Size = new System.Drawing.Size(241, 22);
            this.runToolStripMenuItem_NoLog.Text = "&Run";
            this.runToolStripMenuItem_NoLog.Click += new System.EventHandler(this.runToolStripMenuItem_NoLog_Click);
            // 
            // runSelectedStepsToolStripMenuItem_NoLog
            // 
            this.runSelectedStepsToolStripMenuItem_NoLog.Name = "runSelectedStepsToolStripMenuItem_NoLog";
            this.runSelectedStepsToolStripMenuItem_NoLog.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F6)));
            this.runSelectedStepsToolStripMenuItem_NoLog.Size = new System.Drawing.Size(241, 22);
            this.runSelectedStepsToolStripMenuItem_NoLog.Text = "Run &selected step(s)";
            this.runSelectedStepsToolStripMenuItem_NoLog.Click += new System.EventHandler(this.runSelectedStepsToolStripMenuItem_NoLog_Click);
            // 
            // runFromSelectedStepToolStripMenuItem_NoLog
            // 
            this.runFromSelectedStepToolStripMenuItem_NoLog.Name = "runFromSelectedStepToolStripMenuItem_NoLog";
            this.runFromSelectedStepToolStripMenuItem_NoLog.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F7)));
            this.runFromSelectedStepToolStripMenuItem_NoLog.Size = new System.Drawing.Size(241, 22);
            this.runFromSelectedStepToolStripMenuItem_NoLog.Text = "Run &from selected step";
            this.runFromSelectedStepToolStripMenuItem_NoLog.Click += new System.EventHandler(this.runFromSelectedStepToolStripMenuItem_NoLog_Click);
            // 
            // runScheduledMenuItem
            // 
            this.runScheduledMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("runScheduledMenuItem.Image")));
            this.runScheduledMenuItem.Name = "runScheduledMenuItem";
            this.runScheduledMenuItem.Size = new System.Drawing.Size(214, 22);
            this.runScheduledMenuItem.Text = "Schedule&d run";
            this.runScheduledMenuItem.Click += new System.EventHandler(this.runScheduledBtn_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(211, 6);
            // 
            // cancelMenuItem
            // 
            this.cancelMenuItem.Enabled = false;
            this.cancelMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cancelMenuItem.Image")));
            this.cancelMenuItem.Name = "cancelMenuItem";
            this.cancelMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.cancelMenuItem.Size = new System.Drawing.Size(214, 22);
            this.cancelMenuItem.Text = "&Cancel";
            this.cancelMenuItem.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openButton,
            this.saveButton,
            this.toolStripSeparator9,
            this.runToolstripButton,
            this.cancelToolstripButton,
            this.toolStripSeparator1,
            this.addGroupBtn,
            this.selectAddTestBtn,
            this.removeBtn,
            this.toolStripSeparator3,
            this.parametersBtn});
            this.toolStrip.Location = new System.Drawing.Point(3, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(557, 25);
            this.toolStrip.TabIndex = 2;
            this.toolStrip.Visible = false;
            // 
            // openButton
            // 
            this.openButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openButton.Image = ((System.Drawing.Image)(resources.GetObject("openButton.Image")));
            this.openButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(23, 22);
            this.openButton.Text = "Open";
            this.openButton.Visible = false;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveButton.Image = ((System.Drawing.Image)(resources.GetObject("saveButton.Image")));
            this.saveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveButton.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.saveButton.MergeIndex = 1;
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(23, 22);
            this.saveButton.Text = "Save";
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 25);
            // 
            // runToolstripButton
            // 
            this.runToolstripButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runScheduledToolstripButton,
            this.runSelectedStepsToolStripButton,
            this.runFromSelectedStepToolStripButton});
            this.runToolstripButton.Image = ((System.Drawing.Image)(resources.GetObject("runToolstripButton.Image")));
            this.runToolstripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.runToolstripButton.Name = "runToolstripButton";
            this.runToolstripButton.Size = new System.Drawing.Size(60, 22);
            this.runToolstripButton.Text = "Run";
            this.runToolstripButton.ToolTipText = "Run (F5)";
            this.runToolstripButton.ButtonClick += new System.EventHandler(this.runBtn_Click);
            // 
            // runScheduledToolstripButton
            // 
            this.runScheduledToolstripButton.Image = ((System.Drawing.Image)(resources.GetObject("runScheduledToolstripButton.Image")));
            this.runScheduledToolstripButton.Name = "runScheduledToolstripButton";
            this.runScheduledToolstripButton.Size = new System.Drawing.Size(214, 22);
            this.runScheduledToolstripButton.Text = "Scheduled run";
            this.runScheduledToolstripButton.Click += new System.EventHandler(this.runScheduledBtn_Click);
            // 
            // runSelectedStepsToolStripButton
            // 
            this.runSelectedStepsToolStripButton.Enabled = false;
            this.runSelectedStepsToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("runSelectedStepsToolStripButton.Image")));
            this.runSelectedStepsToolStripButton.Name = "runSelectedStepsToolStripButton";
            this.runSelectedStepsToolStripButton.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.runSelectedStepsToolStripButton.Size = new System.Drawing.Size(214, 22);
            this.runSelectedStepsToolStripButton.Text = "Run selected step(s)";
            this.runSelectedStepsToolStripButton.ToolTipText = "Run selected steps (F6)";
            this.runSelectedStepsToolStripButton.Click += new System.EventHandler(this.runSelectedStepsToolStripMenuItem_Click);
            // 
            // runFromSelectedStepToolStripButton
            // 
            this.runFromSelectedStepToolStripButton.Enabled = false;
            this.runFromSelectedStepToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("runFromSelectedStepToolStripButton.Image")));
            this.runFromSelectedStepToolStripButton.Name = "runFromSelectedStepToolStripButton";
            this.runFromSelectedStepToolStripButton.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.runFromSelectedStepToolStripButton.Size = new System.Drawing.Size(214, 22);
            this.runFromSelectedStepToolStripButton.Text = "Run from selected step";
            this.runFromSelectedStepToolStripButton.ToolTipText = "Run from selected step (F7)";
            this.runFromSelectedStepToolStripButton.Click += new System.EventHandler(this.runFromSelectedStepToolStripMenuItem_Click);
            // 
            // cancelToolstripButton
            // 
            this.cancelToolstripButton.Enabled = false;
            this.cancelToolstripButton.Image = ((System.Drawing.Image)(resources.GetObject("cancelToolstripButton.Image")));
            this.cancelToolstripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cancelToolstripButton.Name = "cancelToolstripButton";
            this.cancelToolstripButton.Size = new System.Drawing.Size(63, 22);
            this.cancelToolstripButton.Text = "Cancel";
            this.cancelToolstripButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // addGroupBtn
            // 
            this.addGroupBtn.Image = ((System.Drawing.Image)(resources.GetObject("addGroupBtn.Image")));
            this.addGroupBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addGroupBtn.Name = "addGroupBtn";
            this.addGroupBtn.Size = new System.Drawing.Size(84, 22);
            this.addGroupBtn.Text = "Add group";
            this.addGroupBtn.ToolTipText = "Add group (Ctrl+G)";
            this.addGroupBtn.Click += new System.EventHandler(this.addGroupBtn_Click);
            // 
            // selectAddTestBtn
            // 
            this.selectAddTestBtn.Image = ((System.Drawing.Image)(resources.GetObject("selectAddTestBtn.Image")));
            this.selectAddTestBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.selectAddTestBtn.Name = "selectAddTestBtn";
            this.selectAddTestBtn.Size = new System.Drawing.Size(106, 22);
            this.selectAddTestBtn.Text = "Add test case...";
            this.selectAddTestBtn.Click += new System.EventHandler(this.selectAddTestBtn_ButtonClick);
            // 
            // removeBtn
            // 
            this.removeBtn.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearAllButton});
            this.removeBtn.Image = ((System.Drawing.Image)(resources.GetObject("removeBtn.Image")));
            this.removeBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.removeBtn.Name = "removeBtn";
            this.removeBtn.Size = new System.Drawing.Size(82, 22);
            this.removeBtn.Text = "Remove";
            this.removeBtn.ButtonClick += new System.EventHandler(this.removeBtn_Click);
            // 
            // clearAllButton
            // 
            this.clearAllButton.Image = ((System.Drawing.Image)(resources.GetObject("clearAllButton.Image")));
            this.clearAllButton.Name = "clearAllButton";
            this.clearAllButton.Size = new System.Drawing.Size(101, 22);
            this.clearAllButton.Text = "Clear";
            this.clearAllButton.Click += new System.EventHandler(this.clearAllButton_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // parametersBtn
            // 
            this.parametersBtn.Image = ((System.Drawing.Image)(resources.GetObject("parametersBtn.Image")));
            this.parametersBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.parametersBtn.Name = "parametersBtn";
            this.parametersBtn.Size = new System.Drawing.Size(86, 22);
            this.parametersBtn.Text = "Parameters";
            this.parametersBtn.ToolTipText = "Parameters (F3)";
            this.parametersBtn.Click += new System.EventHandler(this.parametersBtn_Click);
            // 
            // saveTestRunDialog
            // 
            this.saveTestRunDialog.FileName = "testRun.testrun";
            this.saveTestRunDialog.Filter = "Test run files (*.testrun;*.xml)|*.testrun;*.xml|Test run files (*.testrun)|*.tes" +
    "trun|XML files|*.xml|All files|*.*";
            this.saveTestRunDialog.Title = "Save test run";
            // 
            // hideStatusTimer
            // 
            this.hideStatusTimer.Interval = 3000;
            this.hideStatusTimer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(49, 17);
            this.toolStripStatusLabel1.Text = "Legend:";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Image = global::yats.Gui.Winforms.Components.TestRunTree.Properties.Resources.circle_green_16;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(46, 17);
            this.toolStripStatusLabel2.Text = "Pass";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Image = global::yats.Gui.Winforms.Components.TestRunTree.Properties.Resources.circle_red_16;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(41, 17);
            this.toolStripStatusLabel3.Text = "Fail";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Image = global::yats.Gui.Winforms.Components.TestRunTree.Properties.Resources.help_circle_blue_16;
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(89, 17);
            this.toolStripStatusLabel4.Text = "Inconclusive";
            // 
            // toolStripStatusLabel5
            // 
            this.toolStripStatusLabel5.Image = global::yats.Gui.Winforms.Components.TestRunTree.Properties.Resources.circle_grey_16;
            this.toolStripStatusLabel5.Name = "toolStripStatusLabel5";
            this.toolStripStatusLabel5.Size = new System.Drawing.Size(64, 17);
            this.toolStripStatusLabel5.Text = "Not run";
            // 
            // toolStripStatusLabel6
            // 
            this.toolStripStatusLabel6.Image = global::yats.Gui.Winforms.Components.TestRunTree.Properties.Resources.minus_circle_green_16;
            this.toolStripStatusLabel6.Name = "toolStripStatusLabel6";
            this.toolStripStatusLabel6.Size = new System.Drawing.Size(72, 17);
            this.toolStripStatusLabel6.Text = "Canceled";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(38, 17);
            this.statusLabel.Text = "status";
            this.statusLabel.Visible = false;
            // 
            // tabContextMenu
            // 
            this.tabContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeTabToolStripMenuItem,
            this.closeOtherTestsToolStripMenuItem,
            this.toolStripSeparator2,
            this.copyFullPathToolStripMenuItem,
            this.openContainingFolderToolStripMenuItem,
            this.shellMenuToolStripMenuItem});
            this.tabContextMenu.Name = "tabContextMenu";
            this.tabContextMenu.Size = new System.Drawing.Size(202, 120);
            // 
            // closeTabToolStripMenuItem
            // 
            this.closeTabToolStripMenuItem.Name = "closeTabToolStripMenuItem";
            this.closeTabToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.closeTabToolStripMenuItem.Text = "Close Tab";
            this.closeTabToolStripMenuItem.Click += new System.EventHandler(this.closeTabToolStripMenuItem_Click);
            // 
            // closeOtherTestsToolStripMenuItem
            // 
            this.closeOtherTestsToolStripMenuItem.Name = "closeOtherTestsToolStripMenuItem";
            this.closeOtherTestsToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.closeOtherTestsToolStripMenuItem.Text = "Close Other Tests";
            this.closeOtherTestsToolStripMenuItem.Click += new System.EventHandler(this.closeOtherTestsToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(198, 6);
            // 
            // copyFullPathToolStripMenuItem
            // 
            this.copyFullPathToolStripMenuItem.Name = "copyFullPathToolStripMenuItem";
            this.copyFullPathToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.copyFullPathToolStripMenuItem.Text = "Copy Full Path";
            this.copyFullPathToolStripMenuItem.Click += new System.EventHandler(this.copyFullPathToolStripMenuItem_Click);
            // 
            // openContainingFolderToolStripMenuItem
            // 
            this.openContainingFolderToolStripMenuItem.Name = "openContainingFolderToolStripMenuItem";
            this.openContainingFolderToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.openContainingFolderToolStripMenuItem.Text = "Open Containing Folder";
            this.openContainingFolderToolStripMenuItem.Click += new System.EventHandler(this.openContainingFolderToolStripMenuItem_Click);
            // 
            // shellMenuToolStripMenuItem
            // 
            this.shellMenuToolStripMenuItem.Name = "shellMenuToolStripMenuItem";
            this.shellMenuToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.shellMenuToolStripMenuItem.Text = "Shell Menu";
            this.shellMenuToolStripMenuItem.DropDownOpening += new System.EventHandler(this.shellMenuToolStripMenuItem_DropDownOpening);
            // 
            // saveTestCaseDialog
            // 
            this.saveTestCaseDialog.FileName = "testCase.testcase";
            this.saveTestCaseDialog.Filter = "Test case files (*.testcase)|*.testcase|XML files|*.xml|All files|*.*";
            this.saveTestCaseDialog.Title = "Export test case";
            // 
            // TestRunTreeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 449);
            this.Controls.Add(this.toolStripContainer1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "TestRunTreeForm";
            this.ShowInTaskbar = false;
            this.TabPageContextMenuStrip = this.tabContextMenu;
            this.Text = "Test sequence";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TestRunTreeForm_FormClosing);
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.tabContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private TestRunTreePanel testRunTreePanel;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton saveButton;
        private System.Windows.Forms.ToolStripButton openButton;
        private System.Windows.Forms.SaveFileDialog saveTestRunDialog;
        private System.Windows.Forms.Timer hideStatusTimer;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel7;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel8;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel9;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel10;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel11;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel12;
        private System.Windows.Forms.ToolStripProgressBar executionProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel5;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel6;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem propertiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runFromSelectedStepMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runSelectedStepsMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem cancelMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runScheduledMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addTestCaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addGroupMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem removeMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearAllMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem parametersMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameMainMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disableMainMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enableMainMenuItem;
        private System.Windows.Forms.ToolStripMenuItem propertiesMainMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reloadFromDiskToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSplitButton runToolstripButton;
        private System.Windows.Forms.ToolStripMenuItem runScheduledToolstripButton;
        private System.Windows.Forms.ToolStripMenuItem runSelectedStepsToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem runFromSelectedStepToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton cancelToolstripButton;
        private System.Windows.Forms.ToolStripButton addGroupBtn;
        private System.Windows.Forms.ToolStripSplitButton removeBtn;
        private System.Windows.Forms.ToolStripMenuItem clearAllButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton parametersBtn;
        private System.Windows.Forms.ContextMenuStrip tabContextMenu;
        private System.Windows.Forms.ToolStripMenuItem closeTabToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeOtherTestsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem copyFullPathToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openContainingFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton selectAddTestBtn;
        private System.Windows.Forms.ToolStripMenuItem withoutLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem_NoLog;
        private System.Windows.Forms.ToolStripMenuItem runSelectedStepsToolStripMenuItem_NoLog;
        private System.Windows.Forms.ToolStripMenuItem runFromSelectedStepToolStripMenuItem_NoLog;
        private System.Windows.Forms.ToolStripMenuItem renameTestFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteFromDiskToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportAsTestCaseToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveTestCaseDialog;
        private System.Windows.Forms.ToolStripMenuItem undoMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shellMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem showTestCaseTypeToolStripMenuItem;
    }
}