﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using Tamir.SharpSsh;
using yats.Utilities;
using yats.Ports.Properties;
using log4net;

namespace yats.Ports
{
    public class SshPort : AbstractPort, IDisposable
    {
        protected SshStream ssh;
        protected readonly object mLockObject = new object();
        protected string host;
        protected string username;
        protected string password;
        protected ILog Logger;

        public override string ToString()
        {
            return string.Format("{1}@{0}, connected: {2}", username, host, (ssh != null));
        }

        public string Host
        {
            [DebuggerStepThrough]
            get { return host; }
            [DebuggerStepThrough]
            set { host = value; }
        }

        public string User
        {
            [DebuggerStepThrough]
            get { return username; }
            [DebuggerStepThrough]
            set { username = value; }
        }

        public string Password
        {
            [DebuggerStepThrough]
            get { return password; }
            [DebuggerStepThrough]
            set { password = value; }
        }

        public SshPort(string host, string username, string password)
        {
            this.host = host;
            this.username = username;
            this.password = password;
        }

        public SshPort()
        {
        }

        internal SshPort(SshClientInfo settings) : this(settings.Host, settings.User, settings.Password)
        {            
        }

        protected byte[] buffer = new byte[1024];

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Port is already open</exception> 
        public override void Open()
        {
            CheckDisposed();
            lock(mLockObject)
            {
                if(ssh != null)
                {
                    throw new InvalidOperationException("port is already open");
                }
                ssh = new SshStream( host, username, password );

                ssh.BeginRead( buffer, 0, buffer.Length,
                        new AsyncCallback( ReceiveCallback ), this );
                if(Settings.Default.UseLog4Net){
                    Logger = LogManager.GetLogger(string.Format("{0}@{1}", username, host));
                }
                if(Logger != null && Logger.IsDebugEnabled)
                {
                    Logger.DebugFormat( "Socket connected to {0}@{1}. Client version: {2}", username, host, ssh.ClientVersion );
                }
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            // Retrieve the state object and the client socket 
            // from the asynchronous state object.
            SshPort client = (SshPort)ar.AsyncState;
            SshStream socket = client.ssh;

            lock(client.mLockObject)
            {
                if(socket == null)
                {
                    return;
                }
            }
                        
            try
            {

                // Read data from the remote device.
                int bytesRead = socket.EndRead( ar );
                if(bytesRead > 0)
                {
                    DataReceivedHandler( client, client.buffer, bytesRead );
                    lock(client.mLockObject)
                    {
                        if(socket == null)
                        {
                            return;
                        }
                    }
                    //  continue receiving
                    socket.BeginRead( client.buffer, 0, client.buffer.Length, 
                        new AsyncCallback( ReceiveCallback ), client );
                }
            }
            catch(Exception e)
            {
                if (Logger != null)
                {
                    Logger.Error("Exception ", e);
                }
                this.ErrorReceivedHandler( this, e );
                socket.Close( );
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override void Close()
        {
            CheckDisposed();
            CloseParsers();
            lock(mLockObject)
            {
                if(ssh == null)
                {
                    return;
                }
                ssh.Close( );
                ssh = null;
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        public override void Write(byte[] buffer, int offset, int count)
        {
            CheckDisposed();
            CheckClosed(); 
            ssh.Write( buffer, offset, count );
            GenerateOnDataSent(ByteArray.Copy(buffer, offset, count), DateTime.Now);
        }

        /*public override void Accept(ISerialDeviceVisitor visitor)
        {
            throw new NotImplementedException( );
        }*/

        internal SshClientInfo GetSettings()
        {
            return new SshClientInfo( host, username, password, DiscardDataWithoutParsers );
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override bool IsOpen()
        {
            CheckDisposed();
            lock (mLockObject)
            {
                return ssh != null;
            }
        }

        private bool _disposed = false;

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // Clear all property values that maybe have been set
                    // when the class was instantiated
                    if (ssh != null)
                    {
                        ssh.Dispose();
                        ssh = null;
                    }
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
            base.Dispose(disposing);
        }

        protected override void CheckDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            base.CheckDisposed();
        }
    }
}
