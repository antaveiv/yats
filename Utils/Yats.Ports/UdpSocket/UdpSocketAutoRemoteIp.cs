﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using yats.Utilities;
using yats.Ports.Properties;
using log4net;
using System.Net.NetworkInformation;

namespace yats.Ports
{
    /// <summary>
    /// UDP socket, listens on specified local port. When a packet is received the remote IP endpoint for sending is configured. Will always send to the IP:port the last packet was received from. Will throw exception when trying to send before any packets are received
    /// </summary>
    public class UdpSocketAutoRemoteIp : UdpSocket, IDisposable
    {        
        protected object mLockObject = new object();
        IPEndPoint remoteEndPoint = null;
        UdpClient client;
        protected ILog Logger;
                
        int remotePort;
        public int RemotePort
        {
            get { return remotePort; }
        }

        IPAddress remoteIp;
        /// <summary>
        /// can be a single IP or broadcast address
        /// </summary>
        public IPAddress RemoteIp
        {
            get { return remoteIp; }
        }

        public UdpSocketAutoRemoteIp()
        {
        }

        public UdpSocketAutoRemoteIp(int localPort):this()
        {
            this.localPort = localPort;
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Port is already open</exception> 
        public override void Open()
        {
            CheckDisposed();
            lock (mLockObject)
            {
                if (client != null)
                {
                    throw new InvalidOperationException("Port is already open");
                }
                if (Settings.Default.UseLog4Net)
                {
                    Logger = LogManager.GetLogger(string.Format("UDP {0}", localPort));
                }

                client = new UdpClient(localPort);
                //Socket s = client.Client;
                //s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
                //s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontRoute, 1);

                //remoteEndPoint = new IPEndPoint(remoteIp, remotePort);
                
                client.BeginReceive(ReceiveCallback, null);
            }
        }

        protected void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                if (_disposed)
                {
                    return;
                }
                if (IsOpen() == false)
                {
                    return;
                }

                // Read data from the remote device.
                IPEndPoint groupEP = null;
                byte[] bytesRead = client.EndReceive(ar, ref groupEP);
                if (bytesRead.Length > 0)
                {
                    this.remoteIp = groupEP.Address;                    
                    this.remotePort = groupEP.Port;
                    this.remoteEndPoint = new IPEndPoint(remoteIp, remotePort);
                    
                    DataReceivedHandler(groupEP, bytesRead, bytesRead.Length);
                    
                    if (IsOpen() == false)
                    {
                        return;
                    }                    
                    //  continue receiving
                    client.BeginReceive(ReceiveCallback, this);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception {0}", e);
                if (Logger != null && Logger.IsWarnEnabled)
                {
                    Logger.Warn("Receive exception: ", e);
                }
                this.ErrorReceivedHandler(this, e);
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override void Close()
        {
            CheckDisposed();
            CloseParsers();
            lock (mLockObject)
            {
                if (client != null)
                {
                    client.Close();
                    client = null;
                }
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override bool IsOpen()
        {
            CheckDisposed();
            lock (mLockObject)
            {
                return client != null;
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        /// <exception cref="System.ArgumentNullException">Argument 'buffer' is null</exception>
        public override void Write(byte[] buffer, int offset, int count)
        {
            CheckDisposed();
            CheckClosed();
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer is null");
            }
            if (buffer.IsNullOrEmpty(offset, count)){
                return;
            }
            
            if (remoteEndPoint != null)
            {
                if (count != buffer.Length)
                {
                    buffer = ByteArray.Copy(buffer, offset, count);
                }
                
                client.Send(buffer, buffer.Length, remoteEndPoint);
                GenerateOnDataSent(ByteArray.Copy(buffer, offset, count), DateTime.Now);
            }
        }

        
        private bool _disposed = false;

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (client != null)
                    {
                        client.Close();
                        client = null;
                    }
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
            base.Dispose(disposing);
        }

        protected override void CheckDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            base.CheckDisposed();
        }
     }
}