﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using yats.Utilities;
using yats.Ports.Properties;
using log4net;
using System.Net.NetworkInformation;
using System.Linq;

namespace yats.Ports
{
    /// <summary>
    /// UDP socket, listens on specified local port. Sends packets as broadcast
    /// </summary>
    public class UdpSocketBroadcast : UdpSocket, IDisposable
    {        
        protected object mLockObject = new object();
        UdpClient client;
        protected ILog Logger;
        
        int remotePort;
        public int RemotePort
        {
            get { return remotePort; }
        }

        IPAddress broadcastAddress = IPAddress.Broadcast;
        /// <summary>
        /// broadcast address. By default set to IPAddress.Broadcast
        /// </summary>
        public IPAddress BroadcastAddress
        {
            get { return broadcastAddress; }
            set { broadcastAddress = value; }
        }

        public UdpSocketBroadcast()
        {
        }

        public UdpSocketBroadcast(int localPort, int remotePort):this()
        {
            this.localPort = localPort;
            this.remotePort = remotePort;
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Port is already open</exception> 
        public override void Open()
        {
            CheckDisposed();
            lock (mLockObject)
            {
                if (client != null)
                {
                    throw new InvalidOperationException("Port is already open");
                }
                if (Settings.Default.UseLog4Net)
                {
                    Logger = LogManager.GetLogger(string.Format("UDP {0}", localPort));
                }

                //client = new UdpClient(new IPEndPoint(IPAddress.Any, localPort));
                client = new UdpClient(localPort);

                //NetworkInterface[] Interfaces = NetworkInterface.GetAllNetworkInterfaces();
                //foreach (NetworkInterface networkInterface in Interfaces)
                //{
                //    if (networkInterface.NetworkInterfaceType == NetworkInterfaceType.Loopback)
                //    {
                //        continue;
                //    }
                //    if (networkInterface.OperationalStatus != OperationalStatus.Up)
                //    {
                //        continue;
                //    }
                //    if (!networkInterface.GetIPProperties().MulticastAddresses.Any())
                //    {
                //        continue; // most of VPN adapters will be skipped
                //    }
                //    if (!networkInterface.SupportsMulticast)
                //    {
                //        continue; // multicast is meaningless for this type of connection
                //    }
                //    IPv4InterfaceProperties p = networkInterface.GetIPProperties().GetIPv4Properties();
                //    if (null == p)
                //    {
                //        continue; // IPv4 is not configured on this adapter
                //    }

                //    foreach (UnicastIPAddressInformation unicastIPAddress in networkInterface.GetIPProperties().UnicastAddresses)
                //    {
                //        if (unicastIPAddress.Address.AddressFamily != AddressFamily.InterNetwork)
                //        {
                //            continue;
                //        }
                //        client.JoinMulticastGroup(IPAddress.Parse("127.0.0.1"), unicastIPAddress.Address);
                //        break;
                //    }

                //}

                client.BeginReceive(ReceiveCallback, null);
            }
        }

        protected void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                if (_disposed)
                {
                    return;
                }
                if (IsOpen() == false)
                {
                    return;
                }

                // Read data from the remote device.
                IPEndPoint groupEP = null;
                byte[] bytesRead = client.EndReceive(ar, ref groupEP);
                if (bytesRead.Length > 0)
                {
                    DataReceivedHandler(groupEP, bytesRead, bytesRead.Length);
                    
                    if (IsOpen() == false)
                    {
                        return;
                    }                    
                    //  continue receiving
                    client.BeginReceive(ReceiveCallback, this);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception {0}", e);
                if (Logger != null && Logger.IsWarnEnabled)
                {
                    Logger.Warn("Receive exception: ", e);
                }
                this.ErrorReceivedHandler(this, e);
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override void Close()
        {
            CheckDisposed();
            CloseParsers();
            lock (mLockObject)
            {
                if (client != null)
                {
                    client.Close();
                    client = null;
                }
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override bool IsOpen()
        {
            CheckDisposed();
            lock (mLockObject)
            {
                return client != null;
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        /// <exception cref="System.ArgumentNullException">Argument 'buffer' is null</exception>
        public override void Write(byte[] buffer, int offset, int count)
        {
            CheckDisposed();
            CheckClosed();
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer is null");
            }
            if (buffer.IsNullOrEmpty(offset, count)){
                return;
            }
            int sent = 0;

            NetworkInterface[] Interfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface networkInterface in Interfaces)
            {
                if (networkInterface.NetworkInterfaceType == NetworkInterfaceType.Loopback)
                {
                    continue;
                }
                if (networkInterface.OperationalStatus != OperationalStatus.Up)
                {
                    continue;
                }
                if (!networkInterface.GetIPProperties().MulticastAddresses.Any())
                {
                    continue; // most of VPN adapters will be skipped
                }
                if (!networkInterface.SupportsMulticast)
                {
                    continue; // multicast is meaningless for this type of connection
                }
                IPv4InterfaceProperties p = networkInterface.GetIPProperties().GetIPv4Properties();
                if (null == p)
                {
                    continue; // IPv4 is not configured on this adapter
                }

                UnicastIPAddressInformationCollection unicastAddresses = networkInterface.GetIPProperties().UnicastAddresses;
                foreach (UnicastIPAddressInformation unicastAddress in unicastAddresses)
                {
                    if (unicastAddress.Address.AddressFamily != AddressFamily.InterNetwork)
                    {
                        continue;
                    }
                    using (UdpClient sendClient = new UdpClient(AddressFamily.InterNetwork))
                    {
                        //new UdpClient(new IPEndPoint(ipAddress, localSendPort))
                        sendClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                        sendClient.Client.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastInterface, (int)IPAddress.HostToNetworkOrder(p.Index));
                        sendClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
                        sendClient.Client.Bind(new IPEndPoint(unicastAddress.Address, localPort));
                        IPEndPoint targetEndPoint = new IPEndPoint(BroadcastAddress, remotePort);

                        sendClient.Send(buffer, count, targetEndPoint);
                        sent++;
                    }
                }
            }

            if (sent > 0)
            {
                GenerateOnDataSent(ByteArray.Copy(buffer, offset, count), DateTime.Now);
            }
            else
            {
                throw new Exception("No suitable network interfaces");
            }

            
            /*
            if (remoteEndPoint != null)
            {
                if (count != buffer.Length)
                {
                    buffer = ByteArray.Copy(buffer, offset, count);
                }
                
                client.Send(buffer, buffer.Length, remoteEndPoint);
                
            }
             * */
        }

        
        private bool _disposed = false;

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (client != null)
                    {
                        client.Close();
                        client = null;
                    }
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
            base.Dispose(disposing);
        }

        protected override void CheckDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            base.CheckDisposed();
        }
     }
}