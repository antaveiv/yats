﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace yats.Ports
{
    public abstract class UdpSocket : AbstractPort
    {
        protected int localPort;
        public int LocalPort
        {
            get { return localPort; }
        }

    }
}
