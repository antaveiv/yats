﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using yats.Utilities;
using yats.Ports.Properties;
using log4net;
using System.Net.NetworkInformation;

namespace yats.Ports
{
    /// <summary>
    /// UDP socket with predefined remote IP endpoint
    /// </summary>
    public class UdpSocketKnownRemoteIp : UdpSocket, IDisposable
    {        
        protected object mLockObject = new object();
        IPEndPoint remoteEndPoint = null;
        UdpClient client;
        protected ILog Logger;
        
        int remotePort;
        public int RemotePort
        {
            get { return remotePort; }
        }

        IPAddress remoteIp;
        /// <summary>
        /// can be a single IP or broadcast address
        /// </summary>
        public IPAddress RemoteIp
        {
            get { return remoteIp; }
        }

        public UdpSocketKnownRemoteIp()
        {
        }

        public UdpSocketKnownRemoteIp(IPAddress remoteIp, int localPort, int remotePort):this()
        {
            this.remoteIp = remoteIp;
            this.localPort = localPort;
            this.remotePort = remotePort;
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Remote IP is not set</exception>
        /// <exception cref="System.InvalidOperationException">Port is already open</exception> 
        public override void Open()
        {
            CheckDisposed();
            if (remoteIp == null)
            {
                throw new InvalidOperationException("Remote IP is not set");
            }
            lock (mLockObject)
            {
                if (client != null)
                {
                    throw new InvalidOperationException("Port is already open");
                }
                if (Settings.Default.UseLog4Net)
                {
                    Logger = LogManager.GetLogger(string.Format("UDP {0}", localPort));
                }

                client = new UdpClient(localPort);

                //while (true)
                //{
                //    var remoteEP = new IPEndPoint(IPAddress.Any, remotePort);
                //    var data = client.Receive(ref remoteEP);
                //    client.Send(new byte[] { 1 }, 1, remoteEP); // if data is received reply letting the client know that we got his data          
                //}

                //client = new UdpClient(new IPEndPoint(IPAddress.Parse("192.168.10.1"), localPort));
                //client = new UdpClient(localPort);
                //IPEndPoint ep = new IPEndPoint(remoteIp, remotePort); // endpoint to send data to
                //client.Connect(ep);

                //IPEndPoint ep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), localPort); // endpoint where server is listening

                ///ep = new IPEndPoint(remoteIp, remotePort); // endpoint to send data to
                //client.Connect(ep);

                remoteEndPoint = new IPEndPoint(remoteIp, remotePort);
                //client.Client.Connect(remoteEndPoint);

                client.BeginReceive(ReceiveCallback, this);
            }
        }

        protected void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                if (_disposed)
                {
                    return;
                }
                if (IsOpen() == false)
                {
                    return;
                }

                // Read data from the remote device.
                IPEndPoint groupEP = null;
                byte[] bytesRead = client.EndReceive(ar, ref groupEP);
                if (bytesRead.Length > 0)
                {
                    if (groupEP.Address.Equals(remoteIp))
                    {
                        DataReceivedHandler(groupEP, bytesRead, bytesRead.Length);
                    }
                    if (IsOpen() == false)
                    {
                        return;
                    }                    
                    //  continue receiving
                    client.BeginReceive(ReceiveCallback, this);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception {0}", e);
                if (Logger != null && Logger.IsWarnEnabled)
                {
                    Logger.Warn("Receive exception: ", e);
                }
                this.ErrorReceivedHandler(this, e);
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override void Close()
        {
            CheckDisposed();
            CloseParsers();
            lock (mLockObject)
            {
                if (client != null)
                {
                    client.Close();
                    client = null;
                }
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override bool IsOpen()
        {
            CheckDisposed();
            lock (mLockObject)
            {
                return client != null;
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        /// <exception cref="System.ArgumentNullException">Argument 'buffer' is null</exception>
        public override void Write(byte[] buffer, int offset, int count)
        {
            CheckDisposed();
            CheckClosed();
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer is null");
            }
            if (buffer.IsNullOrEmpty(offset, count)){
                return;
            }
            
            if (remoteEndPoint != null)
            {
                if (count != buffer.Length)
                {
                    buffer = ByteArray.Copy(buffer, offset, count);
                }
                
                client.Send(buffer, buffer.Length, remoteEndPoint);
                GenerateOnDataSent(ByteArray.Copy(buffer, offset, count), DateTime.Now);
            }
        }

        
        private bool _disposed = false;

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (client != null)
                    {
                        client.Close();
                        client = null;
                    }
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
            base.Dispose(disposing);
        }

        protected override void CheckDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            base.CheckDisposed();
        }
     }
}