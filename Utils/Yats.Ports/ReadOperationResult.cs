﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.IO.Ports;
using System.Diagnostics;

namespace yats.Ports
{
    public class ReadOperationResult
    {
        public enum Result
        {
            // returned by result interpreter
            SUCCESS,
            ERROR,
            // returned by driver
            TIMEOUT,
            CANCELLED,
            PORT_ERROR,
            INTERNAL_ERROR,
        }
        public Result OperationResult;
        public SerialError? SerialPortError;
        public Exception PortException;

        [DebuggerStepThrough]
        public ReadOperationResult(Result result)
        {
            this.OperationResult = result;
        }

        [DebuggerStepThrough]
        public ReadOperationResult(Result result, SerialError error)
            : this(result)
        {
            this.SerialPortError = error;
        }

        [DebuggerStepThrough]
        public ReadOperationResult(Result result, Exception ex)
            : this(result)
        {
            this.PortException = ex;
        }

        [DebuggerStepThrough]
        public override string ToString()
        {
            return OperationResult.ToString();
        }
    }
}
