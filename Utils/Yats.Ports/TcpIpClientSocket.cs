﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Net.Sockets;
using yats.Utilities;
using yats.Ports.Properties;
using log4net;

namespace yats.Ports
{
    public class TcpIpClientSocket : AbstractPort, IDisposable
    {
        public TcpIpClientSocket(int portNumber)
        {
            this.m_portNumber = portNumber;
        }

        public TcpIpClientSocket(String ipAddress, int portNumber)
            : this(portNumber)
        {
            this.m_address = ipAddress;
        }

        public TcpIpClientSocket(String ipAddress, int portNumber, Socket establishedSocket)
            : this(ipAddress, portNumber)
        {
            this.ClientSocket = establishedSocket;
        }

        protected ILog Logger;

        private String m_address;
        // IP or address string
        public String Address
        {
            get
            {
                return m_address;
            }
            set
            {
                m_address = value;
            }
        }

        private int m_portNumber;
        public int PortNumber
        {
            get
            {
                return m_portNumber;
            }
            protected set
            {
                m_portNumber = value;
            }
        }

        public override string Name
        {
            get
            {
                if (string.IsNullOrEmpty(m_address))
                {
                    return "TCP/IP address not set";
                }
                return m_address + ":" + m_portNumber;
            }
            set { }
        }

        protected TimeSpan? connectTimeout = null;
        public TimeSpan? ConnectTimeout
        {
            get { return connectTimeout; }
            set { connectTimeout = value; }
        }

        protected Socket clientSocket = null;
        protected Socket ClientSocket
        {
            get { return clientSocket; }
            set
            {
                if (object.ReferenceEquals(clientSocket, value))
                {
                    return;
                }
                if (clientSocket != null && clientSocket.Connected)
                {
                    clientSocket.Close(1); // this way, FIN, ACK is sent instead of RST, ACK
                }
                clientSocket = value;
                if (clientSocket != null)
                {
                    // Begin receiving the data from the remote device.
                    clientSocket.BeginReceive(buffer, 0, buffer.Length, 0,
                            new AsyncCallback(ReceiveCallback), this);
                }
            }
        }

        public bool KeepAlive {
            get
            {
                if (clientSocket != null)
                {
                    try
                    {
                        return Convert.ToBoolean(clientSocket.GetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive));
                    }
                    catch { }
                }
                return false;
            }
            set
            {
                if (clientSocket != null)
                {
                    try
                    {
                        clientSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, value);
                    }
                    catch { }
                }
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override bool IsOpen()
        {
            CheckDisposed();
            if (clientSocket == null || clientSocket.Connected == false)
            {
                return false;
            }

            try
            {
                return !(clientSocket.Poll(1, SelectMode.SelectRead) && clientSocket.Available == 0);
            }
            catch (SocketException) { return false; }
        }

        protected byte[] buffer = new byte[1024];

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override void Open()
        {
            CheckDisposed();
            if (ClientSocket != null)
            {
                throw new Exception("Port is open");
            }
            if (Settings.Default.UseLog4Net)
            {
                Logger = LogManager.GetLogger(string.Format("{0}:{1}", Address, PortNumber));
            }                
            try
            {
                if (IpAddressRegex.IsIp(Address))
                {
                    System.Net.IPAddress remoteIPAddress = System.Net.IPAddress.Parse(Address);
                    System.Net.EndPoint remoteEndPoint = new System.Net.IPEndPoint(remoteIPAddress, PortNumber);
                    var clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    if (connectTimeout.HasValue)
                    {
                        var result = clientSocket.BeginConnect(remoteEndPoint, null, null);

                        var success = result.AsyncWaitHandle.WaitOne(connectTimeout.Value);

                        if (!success)
                        {
                            throw new Exception("Connect failed");
                        }

                        // we have connected
                        clientSocket.EndConnect(result);
                    }
                    else
                    {
                        clientSocket.Connect(remoteEndPoint);
                    }
                    this.ClientSocket = clientSocket;
                }
                else
                {
                    var clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    if (connectTimeout.HasValue)
                    {
                        var result = clientSocket.BeginConnect(Address, PortNumber, null, null);

                        var success = result.AsyncWaitHandle.WaitOne(connectTimeout.Value);

                        if (!success)
                        {
                            throw new Exception("Connect failed");
                        }

                        // we have connected
                        clientSocket.EndConnect(result);
                    }
                    else
                    {
                        clientSocket.Connect(Address, PortNumber);
                    }
                    this.ClientSocket = clientSocket;
                }
                if (Logger != null && Logger.IsDebugEnabled)
                {
                    Logger.Debug("Socket connected to " + m_address + ":" + m_portNumber);
                }
            }
            catch (Exception ex)
            {
                ClientSocket = null;
                if (Logger != null && Logger.IsWarnEnabled)
                {
                    Logger.Warn("Connect failed: ", ex);
                }
                throw;
            }
        }

        protected void ReceiveCallback(IAsyncResult ar)
        {
            // Retrieve the state object and the client socket 
            // from the asynchronous state object.
            TcpIpClientSocket client = (TcpIpClientSocket)ar.AsyncState;
            Socket socket = client.clientSocket;

            if (_disposed)
            {
                return;
            }

            if (socket == null)
            {
                return;
            }

            if (socket.Connected == false)
            {
                return;
            }

            try
            {
                // Read data from the remote device.
                int bytesRead = socket.EndReceive(ar);
                if (bytesRead > 0)
                {
                    DataReceivedHandler(client, client.buffer, bytesRead);
                    if (socket.Connected == false)
                    {
                        return;
                    }
                    //  continue receiving
                    socket.BeginReceive(client.buffer, 0, client.buffer.Length, 0,
                        new AsyncCallback(ReceiveCallback), client);
                }
                else
                {
                    if (Logger != null && Logger.IsDebugEnabled)
                    {
                        Logger.Debug("Client disconnected");
                    }
                    clientSocket.Close(1); // this way, FIN, ACK is sent instead of RST, ACK
                    clientSocket = null;
                    if (OnClose != null)
                    {
                        OnClose(this, null);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception {0}", e);
                if (Logger != null && Logger.IsWarnEnabled)
                {
                    Logger.Warn("Receive exception: ", e);
                }
                this.ErrorReceivedHandler(this, e);
                clientSocket.Close(1); // this way, FIN, ACK is sent instead of RST, ACK
                if (OnClose != null)
                {
                    OnClose(this, null);
                }
            }
        }
        public event EventHandler OnClose;

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override void Close()
        {
            CheckDisposed();
            CloseParsers();
            if (IsOpen())
            {
                clientSocket.Close(1); // this way, FIN, ACK is sent instead of RST, ACK
                if (Logger != null && Logger.IsDebugEnabled)
                {
                    Logger.Debug("Socket closed");
                }
                Console.WriteLine("Socket closed");
                if (OnClose != null)
                {
                    OnClose(this, null);
                }
            }
            clientSocket = null;
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        public override void Write(byte[] buffer, int offset, int count)
        {
            CheckDisposed();
            CheckClosed();

            try
            {
                if (count != clientSocket.Send(buffer, offset, count, SocketFlags.None))
                {
                    if (Logger != null && Logger.IsDebugEnabled)
                    {
                        Logger.Debug("Socket send (" + count + " bytes) failed");
                    }
                    
                    return;
                }
            }
            catch (SocketException ex)
            {
                if (Logger != null && Logger.IsWarnEnabled)
                {
                    Logger.Warn("Send exception: ", ex);
                }
                throw;
            }
            GenerateOnDataSent(ByteArray.Copy(buffer, offset, count), DateTime.Now);
            if (offset == 0 && count == buffer.Length)
            {
                Logger.Debug(buffer);
            }
            else
            {
                Logger.Debug(ByteArray.Copy(buffer, offset, count));
            }
        }

        private bool _disposed = false;

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (clientSocket != null)
                    {
                        clientSocket.Close(1);
                        clientSocket = null;
                    }
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
            base.Dispose(disposing);
        }

        protected override void CheckDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            base.CheckDisposed();
        }
    }
}