﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Utilities;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO.Ports;
using System.Threading;
using System.IO;

//using System.Management;  // Project + Add Reference required

namespace yats.Ports
{
    /// <summary>
    /// Wrapper to a <see cref="T:System.IO.Ports.SerialPort"/>.
    /// 
    /// </summary>
    public class SerialPort : AbstractPort, IDisposable
    {
        protected System.IO.Ports.SerialPort mDriver;
        protected Stream theBaseStream;// http://stackoverflow.com/questions/13408476/detecting-when-a-serialport-gets-disconnected/13419871#13419871

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Elgama.VilKa.Connection.SerialPortDevice"/> class.
        /// Same as <see cref="T:System.IO.Ports.SerialPort"/>
        /// </summary>
        [MonitoringDescription( "SerialPortDesc" )]
        public SerialPort ()
        {
            if (PlatformHelper.IsRunningOnMono())
            {
                MonoSerialPort port = new MonoSerialPort();
                mDriver = port;
                mDriver.DataReceived += mDriver_DataReceived;
            }
            else
            {
                mDriver = new System.IO.Ports.SerialPort();
                mDriver.DataReceived += mDriver_DataReceived;
            }
        	mDriver.PortName = "COM1";
        	mDriver.BaudRate = 300;
        	mDriver.DataBits = 7;
        	mDriver.Parity = Parity.Even;
        	mDriver.StopBits = StopBits.One;
        	mDriver.DtrEnable = true;
        	mDriver.RtsEnable = true;
        	mDriver.Handshake = Handshake.None;
        	mDriver.WriteTimeout = System.IO.Ports.SerialPort.InfiniteTimeout;
        	mDriver.ReadTimeout = 3500;
        	mDriver.PinChanged += new SerialPinChangedEventHandler (OnPortPinChanged);
            mDriver.ErrorReceived += (sender, e) => { ErrorReceivedHandler(this, new Exception(string.Format("Port error: {0}", e.EventType))); };
            mDriver.ErrorReceived += new SerialErrorReceivedEventHandler(MyOnErrorReceived);
		}
		
		public SerialPort (string deviceName, int baudRate, int dataBits, Parity parity): this()
        {
        	mDriver.PortName = deviceName;
        	mDriver.BaudRate = baudRate;
        	mDriver.DataBits = dataBits;
			mDriver.Parity = parity;
        }

        void mDriver_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                int len = mDriver.BytesToRead;
                byte[] buffer = new byte[len];
                len = mDriver.Read(buffer, 0, len);

                DataReceivedHandler(this, buffer, len);
            }
            catch (Exception ex)
            {
                GenerateOnPortError(ex);
            }
        }

        public event SerialErrorReceivedEventHandler ErrorReceived;

        protected void MyOnErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            if (ErrorReceived != null)
            {
                ErrorReceived(this, e);
            }
        }

        /// <summary>
        /// Gets or sets the serial baud rate.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// The baud rate.
        /// 
        /// </returns>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The baud rate specified is less than or equal to zero, or is greater than the maximum allowable baud rate for the device. </exception>
        /// <exception cref="T:System.IO.IOException">The port is in an invalid state.
        /// 
        /// - or -
        /// 
        /// An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort"/> object were invalid.
        /// </exception>
        [MonitoringDescription( "BaudRate" )]
        [Browsable( true )]
        [DefaultValue( 9600 )]
        public int BaudRate
        {
            get
            {
                return mDriver.BaudRate;
            }
            set
            {
                mDriver.BaudRate = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the standard length of data bits per byte.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// The data bits length.
        /// 
        /// </returns>
        /// <exception cref="T:System.IO.IOException">The port is in an invalid state.
        /// 
        /// - or -
        /// 
        /// An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort"/> object were invalid.
        /// </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The data bits value is less than 5 or more than 8. </exception>
        [MonitoringDescription( "DataBits" )]
        [DefaultValue( 8 )]
        [Browsable( true )]
        public int DataBits
        {
            get
            {
                return mDriver.DataBits;
            }
            set
            {
                mDriver.DataBits = value;
            }
        }

        /// <summary>
        /// Gets or sets the parity-checking protocol.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// One of the <see cref="T:System.IO.Ports.Parity"/> values that represents the parity-checking protocol. The default is None.
        /// 
        /// </returns>
        /// <exception cref="T:System.IO.IOException">The port is in an invalid state.
        /// 
        /// - or -
        /// 
        /// An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort"/> object were invalid.
        /// </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The <see cref="P:System.IO.Ports.SerialPort.Parity"/> value passed is not a valid value in the <see cref="T:System.IO.Ports.Parity"/> enumeration. </exception>
        /// <PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/></PermissionSet>
        [MonitoringDescription( "Parity" )]
        [Browsable( true )]
        [DefaultValue( 0 )]
        public Parity Parity
        {
            get
            {
                return mDriver.Parity;
            }
            set
            {
                mDriver.Parity = value;
            }
        }

        /// <summary>
        /// Gets or sets the standard number of stopbits per byte.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// One of the <see cref="T:System.IO.Ports.StopBits"/> values.
        /// 
        /// </returns>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The <see cref="P:System.IO.Ports.SerialPort.StopBits"/> value is not one of the values from the <see cref="T:System.IO.Ports.StopBits"/> enumeration. </exception>
        /// <exception cref="T:System.IO.IOException">The port is in an invalid state.
        /// 
        /// - or -
        /// 
        /// An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort"/> object were invalid.
        /// </exception>
        /// <PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/></PermissionSet>
        [DefaultValue( 1 )]
        [MonitoringDescription( "StopBits" )]
        [Browsable( true )]
        public StopBits StopBits
        {
            get
            {
                return mDriver.StopBits;
            }
            set
            {
                mDriver.StopBits = value;
            }
        }

        public bool DtrEnable
        {
            get
            {
                return mDriver.DtrEnable;
            }
            set
            {
                mDriver.DtrEnable = value;
            }
        }

        public Handshake Handshake
        {
            get
            {
                return mDriver.Handshake;
            }
            set
            {
                mDriver.Handshake = value;
            }
        }

        /// <summary>
        
        /// <summary>
        /// Gets or sets the number of milliseconds before a time-out occurs when a read operation does not finish.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// The number of milliseconds before a time-out occurs when a read operation does not finish.
        /// 
        /// </returns>
        /// <exception cref="T:System.IO.IOException">The port is in an invalid state.
        /// 
        /// - or -
        /// 
        /// An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort"/> object were invalid. </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The read time-out value is less than zero and not equal to <see cref="F:System.IO.Ports.SerialPort.InfiniteTimeout"/>. </exception>
        [Browsable( true )]
        [MonitoringDescription( "ReadTimeout" )]
        [DefaultValue( -1 )]
        public int ReadTimeout
        {
            get
            {
                return mDriver.ReadTimeout;
            }
            set
            {
                mDriver.ReadTimeout = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of the serial port output buffer.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// The size of the output buffer. The default is 2048.
        /// 
        /// </returns>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The <see cref="P:System.IO.Ports.SerialPort.WriteBufferSize"/> value is less than or equal to zero. </exception>
        /// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.IO.Ports.SerialPort.WriteBufferSize"/> property was set while the stream was open. </exception>
        /// <exception cref="T:System.IO.IOException">The <see cref="P:System.IO.Ports.SerialPort.WriteBufferSize"/> property was set to an odd integer value. </exception>
        [Browsable( true )]
        [DefaultValue( 2048 )]
        [MonitoringDescription( "WriteBufferSize" )]
        public int WriteBufferSize
        {
            get
            {
                return mDriver.WriteBufferSize;
            }
            set
            {
                mDriver.WriteBufferSize = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of the <see cref="T:System.IO.Ports.SerialPort"/> input buffer.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// The buffer size. The default value is 4096.
        /// 
        /// </returns>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The <see cref="P:System.IO.Ports.SerialPort.ReadBufferSize"/> value set is less than or equal to zero. </exception>
        /// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.IO.Ports.SerialPort.ReadBufferSize"/> property was set while the stream was open. </exception>
        /// <exception cref="T:System.IO.IOException">The <see cref="P:System.IO.Ports.SerialPort.ReadBufferSize"/> property was set to an odd integer value. </exception>
        [Browsable( true )]
        [MonitoringDescription( "ReadBufferSize" )]
        [DefaultValue( 4096 )]
        public int ReadBufferSize
        {
            get
            {
                return mDriver.ReadBufferSize;
            }
            set
            {
                mDriver.ReadBufferSize = value;
            }
        }

        /// <summary>
        /// Discards data from the serial driver's receive buffer.
        /// 
        /// </summary>
        /// <exception cref="T:System.IO.IOException">The port is in an invalid state.
        /// 
        /// - or -
        /// 
        /// An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort"/> object were invalid. </exception>
        /// <exception cref="T:System.InvalidOperationException">The stream is closed. This can occur because the <see cref="M:System.IO.Ports.SerialPort.Open"/> method has not been called or the <see cref="M:System.IO.Ports.SerialPort.Close"/> method has been called. </exception>
        /// <PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/></PermissionSet>
        public override void DiscardInBuffer( )
        {
            base.DiscardInBuffer();
            if (mDriver.IsOpen)
            {
                mDriver.DiscardInBuffer();
            }
        }
        
        /// <summary>
        /// Gets or sets the port for communications, including but not limited to all available COM ports.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// The communications port. The default is COM1.
        /// 
        /// </returns>
        /// <exception cref="T:System.ArgumentException">The <see cref="P:System.IO.Ports.SerialPort.PortName"/> property was set to a value with a length of zero.
        /// 
        /// -or-
        /// 
        /// The <see cref="P:System.IO.Ports.SerialPort.PortName"/> property was set to a value that starts with "\\".
        /// 
        /// -or-
        /// 
        /// The port name was not valid. </exception>
        /// <exception cref="T:System.ArgumentNullException">The <see cref="P:System.IO.Ports.SerialPort.PortName"/> property was set to null. </exception>
        /// <exception cref="T:System.InvalidOperationException">The specified port is open. </exception>
        [MonitoringDescription( "PortName" )]
        [Browsable( true )]
        [DefaultValue( "COM1" )]
        public override string Name
        {
            get
            {
                return mDriver.PortName;
            }
            set
            {
                mDriver.PortName = value;
            }
        }

        /// <summary>
        /// Gets or sets the value used to interpret the end of a call to the <see cref="M:System.IO.Ports.SerialPort.ReadLine"/> and <see cref="M:System.IO.Ports.SerialPort.WriteLine(System.String)"/> methods.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// A value that represents the end of a line. The default is a line feed, (<see cref="P:System.Environment.NewLine"/>).
        /// 
        /// </returns>
        /// <exception cref="T:System.ArgumentException">The property value is empty. </exception>
        /// <exception cref="T:System.ArgumentNullException">The property value is null. </exception>
        [MonitoringDescription( "NewLine" )]
        [Browsable( false )]
        [DefaultValue( "\n" )]
        public string NewLine
        {
            get
            {
                return mDriver.NewLine;
            }
            set
            {
                mDriver.NewLine = value;
            }
        }

        /// <summary>
        /// Gets or sets the break signal state.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// true if the port is in a break state; otherwise, false.
        /// 
        /// </returns>
        /// <exception cref="T:System.IO.IOException">The port is in an invalid state.
        /// 
        /// - or -
        /// 
        /// An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort"/> object were invalid. </exception>
        /// <exception cref="T:System.InvalidOperationException">The stream is closed. This can occur because the <see cref="M:System.IO.Ports.SerialPort.Open"/> method has not been called or the <see cref="M:System.IO.Ports.SerialPort.Close"/> method has been called. </exception>
        /// <PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/></PermissionSet>
        [Browsable( false )]
        public bool BreakState
        {
            get
            {
                return mDriver.BreakState;
            }
            set
            {
                mDriver.BreakState = value;
            }
        }

        /// <summary>
        /// Gets the state of the Carrier Detect line for the port.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// true if the carrier is detected; otherwise, false.
        /// 
        /// </returns>
        /// <exception cref="T:System.IO.IOException">The port is in an invalid state.
        /// 
        /// - or -
        /// 
        /// An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort"/> object were invalid. </exception>
        /// <exception cref="T:System.InvalidOperationException">The stream is closed. This can occur because the <see cref="M:System.IO.Ports.SerialPort.Open"/> method has not been called or the <see cref="M:System.IO.Ports.SerialPort.Close"/> method has been called. </exception>
        /// <PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/></PermissionSet>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool CDHolding
        {
            [DebuggerStepThrough]
            get
            {
                return mDriver.CDHolding;
            }
        }

        public bool RtsEnable
        {
            get { return mDriver.RtsEnable; }
            set { mDriver.RtsEnable = value; }
        }

        #region Overrides of SerialConnectionImp
        /// <summary>
        /// Gets a value indicating the open or closed status of the <see cref="T:System.IO.Ports.SerialPort"/> object.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// true if the serial port is open; otherwise, false. The default is false.
        /// 
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">The <see cref="P:System.IO.Ports.SerialPort.IsOpen"/> value passed is null. </exception>
        /// <exception cref="T:System.ArgumentException">The <see cref="P:System.IO.Ports.SerialPort.IsOpen"/> value passed is an empty string (""). </exception>
        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        [Browsable(false)]
        public override bool IsOpen( )
        {
            CheckDisposed();
            return mDriver.IsOpen;
        }

        /// <summary>
        /// Opens a new serial port connection.
        /// 
        /// </summary>
        /// <exception cref="T:System.InvalidOperationException">The specified port is open. </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">One or more of the properties for this instance are invalid. For example, the <see cref="P:System.IO.Ports.SerialPort.Parity"/>, <see cref="P:System.IO.Ports.SerialPort.DataBits"/>, or <see cref="P:System.IO.Ports.SerialPort.Handshake"/> properties are not valid values; the <see cref="P:System.IO.Ports.SerialPort.BaudRate"/> is less than or equal to zero; the <see cref="P:System.IO.Ports.SerialPort.ReadTimeout"/> or <see cref="P:System.IO.Ports.SerialPort.WriteTimeout"/> property is less than zero and is not <see cref="F:System.IO.Ports.SerialPort.InfiniteTimeout"/>. </exception>
        /// <exception cref="T:System.ArgumentException">The port name does not begin with "COM".
        /// 
        /// - or -
        /// 
        /// The file type of the port is not supported. </exception>
        /// <exception cref="T:System.IO.IOException">The port is in an invalid state.
        /// 
        /// - or -
        /// 
        /// An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort"/> object were invalid. </exception>
        /// <exception cref="T:System.UnauthorizedAccessException">Access is denied to the port. </exception>
        /// <PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/></PermissionSet>
        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override void Open()
        {            
            CheckDisposed();
            Thread.Sleep( 500 );
            if (mDriver is MonoSerialPort)
            {
                //Open is overridden in MonoSerialPort, but not marked as virtual in System.IO.Ports.SerialPort. Casting to MonoSerialPort to call the correct Open method
                (mDriver as MonoSerialPort).Open();
            }
            else
            {
                mDriver.Open();
            }

            theBaseStream = mDriver.BaseStream;
            GC.SuppressFinalize(mDriver.BaseStream);

            //Logger.WarnFormat("{0} opened", Name);
        }

        /// <summary>
        /// Closes the port connection, sets the <see cref="P:System.IO.Ports.SerialPort.IsOpen"/> property to false, and disposes of the internal <see cref="T:System.IO.Stream"/> object.
        /// </summary>
        /// <exception cref="T:System.InvalidOperationException">The specified port is not open. </exception>
        /// <PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/></PermissionSet>
        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override void Close()
        {
            CheckDisposed();
            CloseParsers();
            mDriver.Close( );
            //Logger.WarnFormat("{0} closed", Name);
            Thread.Sleep( 500 );
        }

        /// <summary>
        /// Writes a specified number of bytes to the serial port using data from a buffer.
        /// 
        /// </summary>
        /// <param name="buffer">The byte array that contains the data to write to the port. </param>
        /// <param name="offset">The zero-based byte offset in the <paramref name="buffer"/> parameter at which to begin copying bytes to the port. </param>
        /// <param name="count">The number of bytes to write. </param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="buffer"/> passed is null. </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The <paramref name="offset"/> or <paramref name="count"/> parameters are outside a valid region of the <paramref name="buffer"/> being passed. Either <paramref name="offset"/> or <paramref name="count"/> is less than zero. </exception>
        /// <exception cref="T:System.ArgumentException"><paramref name="offset"/> plus <paramref name="count"/> is greater than the length of the <paramref name="buffer"/>. </exception>
        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        public override void Write(byte[] buffer, int offset, int count)
        {
            CheckDisposed();
            CheckClosed();
            mDriver.Write(buffer, offset, count);
            GenerateOnDataSent(ByteArray.Copy(buffer, offset, count), DateTime.Now);
            SleepAfterWrite(count);
        }

        protected void SleepAfterWrite(int numBytes)
        {
            int delay = GetTransmissionTime(numBytes);
			if (delay > 0){
            	Thread.Sleep( delay );
			}
        }
                
        #endregion
                
        /// <summary>
        /// Get the time needed to send out the given number of bytes with the current baud rate etc settings
        /// </summary>
        /// <param name="numBytes">number of bytes</param>
        /// <returns>time in milliseconds</returns>
        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public int GetTransmissionTime(int numBytes)
		{
            CheckDisposed();
            int baudPerByte = DataBits + 1; // 1 start bit
            if (Parity != Parity.None)
            {
                baudPerByte++;
            }
            switch (StopBits)
            {
                case StopBits.None:
                    break;
                case StopBits.One:
                    baudPerByte++;
                    break;
                default://1.5 or 2
                    baudPerByte += 2;
                    break;
            }
            return numBytes * baudPerByte * 1000 / BaudRate;
		}
		
        private bool _disposed = false;

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    try
                    {
                        if (theBaseStream != null && theBaseStream.CanRead)
                        {
                            theBaseStream.Close();
                            GC.ReRegisterForFinalize(theBaseStream);
                        }
                    }
                    catch
                    {
                        // ignore exception - bug with USB - serial adapters.
                    }

                    mDriver.Dispose();
                    mDriver = null;
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
            base.Dispose(disposing);
        }

        protected override void CheckDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            base.CheckDisposed();
        }
    }
}
