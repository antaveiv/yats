﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
//using Log;
using System.Text;
using System.Threading;
using log4net;
using yats.Utilities;
using System.Diagnostics;
using yats.Ports.Properties;

namespace yats.Ports
{
    /// <summary>
    /// Port abstraction. Allows reading writing and command execution on different types of ports.
    /// Incoming data should preferrably processed by passing a Parser instance to a Command or Execute methods. If no parser is active, the incoming data may be read using Read methods.
    /// Multiple parsers may be used in parallel to catch different types of packets.
    /// </summary>
    public abstract class AbstractPort : IDisposable
    {
        private static ILog Logger = LogManager.GetLogger("Port");
        public virtual string Name { get; set; }

        public abstract bool IsOpen();

        
        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        public virtual void DiscardInBuffer()
        {
            CheckDisposed();
            CheckClosed();
            //Logger.CommLog.Debug("Discard buffer");
            m_receivedDataQueue.Clear();
        }

        public abstract void Open();

        public abstract void Close();
        
        protected virtual void CloseParsers()
        {
            lock (mWaitingParsers)
            {
                foreach (var pair in mWaitingParsers)
                {
                    pair.Key.Canceled = true;
                    pair.Value.Dispose();
                }
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.ArgumentNullException">Argument 'buffer' is null</exception>
        [Obsolete("Use Execute, Command methods to pass received data to a parser")]
        public int Read(byte[] buffer)
        {
            return Read(buffer, 0, buffer.Length);
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when data is already being consumed by a parser</exception>
        /// <exception cref="System.ArgumentNullException">Argument 'buffer' is null</exception>         
        [Obsolete("Use Execute, Command methods to pass received data to a parser")]
        public virtual int Read(byte[] buffer, int offset, int count)
        {
            CheckDisposed();
            CheckClosed();
            if (mWaitingParsers.Count > 0)
            {
                throw new InvalidOperationException("Can not read: data is processed by a Parser");
            }
            if (buffer == null)
            {
                throw new ArgumentNullException("argument 'buffer' is null");
            }
            lock (m_receivedDataQueue.SyncRoot)
            {
                int result = 0;
                for (int i = offset; i < offset + count; i++)
                {
                    if (m_receivedDataQueue.Count > 0)
                    {
                        buffer[i] = (byte)m_receivedDataQueue.Dequeue();
                        result++;
                    }
                    else
                    {
                        break;
                    }
                }
                return result;
            }
        }

        /// <summary>
        /// Returns a byte from received data buffer or -1 if empty.
        /// </summary>
        /// <remarks>Throws Exception if a Parser is active</remarks>
        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when data is already being consumed by a parser</exception> 
        [Obsolete("Use Execute, Command methods to pass received data to a parser")]
        public virtual int ReadByte()
        {
            CheckDisposed();
            CheckClosed();
            if (mWaitingParsers.Count > 0)
            {
                throw new InvalidOperationException("Can not read: data is processed by a Parser");
            }
            try 
            {
                return (byte)m_receivedDataQueue.Dequeue();
            }
            catch (InvalidOperationException)
            {
                return -1;
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        public int BytesToRead()
        {
            CheckDisposed();
            CheckClosed();
            return m_receivedDataQueue.Count;
        }

        /// <summary>
        /// Write buffer using default ASCII encoding
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        public void Write(char[] buffer, int offset, int count)
        {
            Write(buffer, offset, count, Encoding.ASCII);
        }

        public void Write(char[] buffer, int offset, int count, Encoding encoding)
        {
            byte[] bytes = encoding.GetBytes(buffer); // convert chars to bytes
            Write(bytes, offset, count);
        }

        public void Write(string s, Encoding encoding)
        {
            Write(encoding.GetBytes(s));
        }

        /// <summary>
        /// Write a string, use ASCII encoding
        /// </summary>
        /// <param name="s"></param>
        public void Write(string s)
        {
            Write(s, Encoding.ASCII);
        }

        public void Write(byte[] buffer)
        {
            Write(buffer, 0, buffer.Length);
        }

        public abstract void Write(byte[] buffer, int offset, int count);

        protected Exception mPortException = null;
        protected bool ignorePortErrors = true;
        /// <summary>
        /// If errors are not ignored (value set to false), the waiting parsers will finish when a port error condition is detected. Default - true.
        /// </summary>
        public bool IgnorePortErrors { get { return ignorePortErrors; } set { ignorePortErrors = value; } }
        protected bool discardDataWithoutParsers = true;
        /// <summary>
        /// If true, incoming data will be lost when there are no parsers listening for it. If false data will be queued until parser is added by Command, Execute or Attach methods
        /// </summary>
        public bool DiscardDataWithoutParsers {
            get { return discardDataWithoutParsers; }
            set {
                if (value == discardDataWithoutParsers)
                {
                    return;
                }
                if (value) // if have data waiting for parsers and the property is changed to true, discard the data.
                {
                    lock (mWaitingParsers)
                    {
                        if (mWaitingParsers.Count == 0)
                        {
                            m_receivedDataQueue.Clear();
                        }
                    }
                }
                discardDataWithoutParsers = value;
            }
        }

        #region Event-based data receiving through protocol parser(s)

        /// <summary>
        /// A dictionary associating any waiting parsers with their 'release' events
        /// </summary>
        private readonly Dictionary<Parser, ParserDoneHandler> mWaitingParsers = new Dictionary<Parser, ParserDoneHandler>();
        private Queue m_receivedDataQueue = Queue.Synchronized(new Queue(1000));

        internal event EventHandler<ParserEventArgs> ParserAdded;
        internal event EventHandler<ParserEventArgs> ParserRemoved;

        internal int GetWaitingParserCount()
        {
            return mWaitingParsers.Count;
        }

        private abstract class ParserDoneHandler : IDisposable
        {
            public ReadOperationResult Result;
            public DateTime StartTime { get; internal set; }

            // called after parser reports packet parse finish. If true is returned from Done, the parser is removed from mWaitingParsers
            public abstract bool Done(Parser parser);

            public abstract void Dispose();
        }

        private class SingleParserDoneHandler : ParserDoneHandler
        {
            public AutoResetEvent evt;

            public SingleParserDoneHandler()
            {
                this.evt = new AutoResetEvent(false);
            }

            public SingleParserDoneHandler(AutoResetEvent evt)
            {
                this.evt = evt;
            }

            public override bool Done(Parser parser)
            {
                evt.Set();
                return true;
            }

            private bool _disposed = false;


            ~SingleParserDoneHandler()
            {
                Dispose(false);
            }

            public override void Dispose()
            {
                Dispose(true);

                // Use SupressFinalize in case a subclass 
                // of this type implements a finalizer.
                GC.SuppressFinalize(this);
            }

            protected virtual void Dispose(bool disposing)
            {
                if (!_disposed)
                {
                    if (disposing)
                    {
                        // Clear all property values that maybe have been set
                        // when the class was instantiated
                        evt.Close();
                    }

                    // Indicate that the instance has been disposed.
                    _disposed = true;
                }
            }
        }

        private class PermanentParserDoneHandler<T> : ParserDoneHandler where T : class, IResetable, ICloneable
        {
            T parser;
            IProducerConsumer<T> consumer;
            public PermanentParserDoneHandler(T parser, IProducerConsumer<T> consumer)
            {
                this.parser = parser;
                this.consumer = consumer;
            }

            public override bool Done(Parser parser)
            {
                if (!_disposed)
                {
                    Debug.Assert(object.ReferenceEquals(this.parser, parser));
                    var resultParser = (T)this.parser.Clone();
                    (resultParser as Parser).ExecutionTime = DateTime.Now - StartTime;
                    StartTime = DateTime.Now;
                    consumer.EnqueueTask(resultParser);
                    this.parser.Reset();                    
                }
                return false;
            }

            private bool _disposed = false;

            ~PermanentParserDoneHandler()
            {
                Dispose(false);
            }

            public override void Dispose()
            {
                Dispose(true);

                // Use SupressFinalize in case a subclass 
                // of this type implements a finalizer.
                GC.SuppressFinalize(this);
            }

            protected virtual void Dispose(bool disposing)
            {
                if (!_disposed)
                {
                    if (disposing)
                    {
                        // Clear all property values that maybe have been set
                        // when the class was instantiated
                        if (consumer != null)
                        {
                            consumer.Dispose();
                            consumer = null;
                        }
                    }

                    // Indicate that the instance has been disposed.
                    _disposed = true;
                }
            }
        }

        private readonly object DataProcessLock = new object();
        // Feeds the received data bytes to the currently waiting parsers. Any parser that returns True in its IncomingData method is 'released'. The 
        void ProcessDataWithParsers()
        {
            lock (DataProcessLock)
            {
                try
                {
                    if (mWaitingParsers.Count == 0)
                    {
                        if (DiscardDataWithoutParsers)
                        {
                            m_receivedDataQueue.Clear();
                        }
                        return;
                    }

                    while (m_receivedDataQueue.Count > 0)
                    {
                        if (mWaitingParsers.Count == 0)
                        {
                            break;
                        }
                        byte bt = (byte)m_receivedDataQueue.Dequeue();
                        
                        List<Parser> toRemove = new List<Parser>();

                        lock (mWaitingParsers)
                        {
                            foreach (var pair in mWaitingParsers)
                            {
                                //Parser.IncomingData returns true when it finishes receiving its packet
                                try
                                {
                                    if (pair.Key.IncomingData(bt))
                                    {
                                        //parser is finished
                                        pair.Value.Result = pair.Key.GetResult();
                                        if (pair.Value.Done(pair.Key))// release waiting process wait event
                                        {
                                            // remove single-use parsers, don't remove permanent parsers
                                            toRemove.Add(pair.Key);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                    if (Settings.Default.UseLog4Net)
                                    {
                                        Logger.Error(ex);
                                    }
                                    GenerateOnParserError(ex);
                                }
                            }

                            mWaitingParsers.RemoveKeys(toRemove);
                        }

                        if (ParserRemoved != null)
                        {
                            foreach (var p in toRemove)
                            {
                                ParserRemoved(this, new ParserEventArgs() { Parser = p });
                            }
                        }
                    }

                    if (mWaitingParsers.Count == 0)
                    {
                        if (DiscardDataWithoutParsers)
                        {
                            m_receivedDataQueue.Clear();
                        }
                        return;
                    }
                }
                catch (Exception ex)
                {
                    if (Settings.Default.UseLog4Net)
                    {
                        Logger.Error(ex);
                    }
                }
            }
        }

        //Attach a parser to a port. A producer-consumer instance will receive a clone of the parser when the parsing is done. Can be used to receive multiple packets instead of multiple calls to Execute() that could lose data in between calls
        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.ArgumentNullException">Argument 'parser' is null</exception>
        public void Attach<T>(T parser, IProducerConsumer<T> consumer) where T : Parser, IResetable, ICloneable
        {
            CheckDisposed();
            if (parser == null)
            {
                throw new ArgumentNullException("Port.Attach: parser is null");
            }

            var parserDoneHandler = new PermanentParserDoneHandler<T>(parser, consumer);
            parserDoneHandler.StartTime = DateTime.Now;
            lock (mWaitingParsers)
            {
                this.mWaitingParsers.Add(parser, parserDoneHandler);
            }
            if (ParserAdded != null)
            {
                ParserAdded(this, new ParserEventArgs() { Parser = parser });
            }
            ProcessDataWithParsers();
        }

        protected virtual void CheckDisposed()
        {
            if (_disposed) throw new ObjectDisposedException(GetType().Name);
        }

        protected void CheckClosed(){
            if (!IsOpen())
            {
                throw new InvalidOperationException("Port is closed");
            }
        }

        /// <summary>
        /// Detach a parser that was previously added with Attach method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="parser"></param>
        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.ArgumentNullException">Argument 'parser' is null</exception>
        public void Detach<T>(T parser) where T : Parser, IResetable, ICloneable
        {
            if (parser == null)
            {
                throw new ArgumentNullException("Port.Detach: parser is null");
            }
            CheckDisposed();

            lock (mWaitingParsers)
            {
                if (this.mWaitingParsers.Remove(parser))
                {
                    if (ParserRemoved != null)
                    {
                        ParserRemoved(this, new ParserEventArgs() { Parser = parser });
                    }
                }
            }
        }

        /// <summary>
        /// Wait until a given parser finishes
        /// </summary>
        /// <param name="timeout">Operation timeout. If parser is not finished, ReadOperationResult.Timeout will be returned</param>
        /// <param name="parser">Received data parser</param>
        /// <param name="cancelWaitEvent">Can be used to cancel the operation before parser is done or timeout. If set, ReadOperationResult.Canceled will be returned</param>
        /// <returns></returns>
        public ReadOperationResult Execute(TimeSpan timeout, Parser parser, WaitHandle cancelWaitEvent)
        {
            return Execute((int)timeout.TotalMilliseconds, parser, cancelWaitEvent);
        }

        /// <summary>
        /// Wait until a given parser finishes
        /// </summary>
        /// <param name="timeout">Operation timeout. If parser is not finished, ReadOperationResult.Timeout will be returned</param>
        /// <param name="parser">Received data parser</param>
        /// <returns></returns>
        [Obsolete("Use cancellable method")]
        public ReadOperationResult Execute(TimeSpan timeout, Parser parser)
        {
            return Execute((int)timeout.TotalMilliseconds, parser, null);
        }

        /// <summary>
        /// Wait until a given parser finishes
        /// </summary>
        /// <param name="parser">Received data parser</param>
        /// <returns></returns>
        [Obsolete("Use cancellable method")]
        public ReadOperationResult Execute(Parser parser)
        {
            return Execute(null, parser, null);
        }

        /// <summary>
        /// Wait until a given parser finishes
        /// </summary>
        /// <param name="parser">Received data parser</param>
        /// <param name="cancelWaitEvent">Can be used to cancel the operation before parser is done or timeout. If set, ReadOperationResult.Canceled will be returned</param>
        /// <returns></returns>
        public ReadOperationResult Execute(Parser parser, WaitHandle cancelWaitEvent)
        {
            return Execute(null, parser, cancelWaitEvent);
        }

        /// <summary>
        /// Wait until a given parser finishes
        /// </summary>
        /// <param name="timeout">Operation timeout. If parser is not finished, ReadOperationResult.Timeout will be returned</param>
        /// <param name="parser">Received data parser</param>
        /// <returns></returns>
        [Obsolete("Use cancellable method")]
        public ReadOperationResult Execute(int? timeoutMs, Parser parser)
        {
            return Execute(timeoutMs, parser, null);
        }

        /// <summary>
        /// Wait for parser
        /// </summary>
        /// <param name="timeoutMs">Timeout in milliseconds</param>
        /// <param name="parser">Response protocol parser</param>
        /// <param name="cancelWaitEvent">When set, the execution is canceled</param>
        /// <returns></returns>
        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        /// <exception cref="System.ArgumentNullException">Argument 'parser' is null</exception>
        public virtual ReadOperationResult Execute(int? timeoutMs, Parser parser, WaitHandle cancelWaitEvent)
        {
            return Command((string)null, parser, timeoutMs, cancelWaitEvent);
        }

        /// <summary>
        /// Send a command and wait for a response. Both sending and receiving are optional
        /// </summary>
        /// <param name='commandToSend'>
        /// Binary data to be written to the port. If set to null, no data is written
        /// </param>
        /// <param name='parser'>
        /// Parser responsible for receiving the command response. If null, only a data write will be done
        /// </param>
        /// <param name='timeout'>
        /// Response timeout 
        /// </param>
        [Obsolete("Use cancellable method")]
        public ReadOperationResult Command(string commandToSend, Parser parser, TimeSpan timeout)
        {
            return Command(commandToSend, parser, timeout, null);
        }

        /// <summary>
        /// Send a command and wait for a response. Both sending and receiving are optional
        /// </summary>
        /// <param name='commandToSend'>
        /// Binary data to be written to the port. If set to null, no data is written
        /// </param>
        /// <param name='parser'>
        /// Parser responsible for receiving the command response. If null, only a data write will be done
        /// </param>
        /// <param name='timeout'>
        /// Response timeout 
        /// </param>
        /// <param name='cancelWaitEvent'>
        /// Cancel wait handle. When set, the command execution is canceled
        /// </param>
        public ReadOperationResult Command(string commandToSend, Parser parser, TimeSpan timeout, WaitHandle cancelWaitEvent)
        {
            if (commandToSend == null)
            {
                return Command((byte[])null, parser, (int)timeout.TotalMilliseconds, cancelWaitEvent);
            }
            return Command(Encoding.ASCII.GetBytes(commandToSend), parser, (int)timeout.TotalMilliseconds, cancelWaitEvent);
        }

        /// <summary>
        /// Send a command and wait for a response
        /// </summary>
        /// <param name='commandToSend'>
        /// Command string. Can be null, in this case no bytes are written to the port. If not null, the string is converted to byte array using ASCII encoding
        /// </param>
        /// <param name='parser'>
        /// Parser responsible for receiving the command response. If null, only a data write will be done
        /// </param>
        /// <param name='timeoutMs'>
        /// Response timeout in ms
        /// </param>
        [Obsolete("Use cancellable method")]
        public ReadOperationResult Command(string commandToSend, Parser parser, int? timeoutMs)
        {
            return Command(commandToSend, parser, timeoutMs, null);
        }

        /// <summary>
        /// Send a command and wait for a response
        /// </summary>
        /// <param name='commandToSend'>
        /// Command string. Can be null, in this case no bytes are written to the port. If not null, the string is converted to byte array using ASCII encoding
        /// </param>
        /// <param name='parser'>
        /// Parser responsible for receiving the command response. If null, only a data write will be done
        /// </param>
        /// <param name='timeoutMs'>
        /// Response timeout in ms
        /// </param>
        /// <param name='cancelWaitEvent'>
        /// Cancel wait handle. When set, the command execution is canceled
        /// </param>
        public ReadOperationResult Command(string commandToSend, Parser parser, int? timeoutMs, WaitHandle cancelWaitEvent)
        {
            if (commandToSend == null)
            {
                return Command((byte[])null, parser, timeoutMs, cancelWaitEvent);
            }
            return Command(Encoding.ASCII.GetBytes(commandToSend), parser, timeoutMs, cancelWaitEvent);
        }

        /// <summary>
        /// Send a command and wait for a response. Both sending and receiving are optional
        /// </summary>
        /// <param name='commandToSend'>
        /// Binary data to be written to the port. If set to null, no data is written
        /// </param>
        /// <param name='parser'>
        /// Parser responsible for receiving the command response. If null, only a data write will be done
        /// </param>
        /// <param name='timeout'>
        /// Response timeout
        /// </param>
        [Obsolete("Use cancellable method")]
        public virtual ReadOperationResult Command(byte[] commandToSend, Parser parser, TimeSpan timeout)
        {
            return Command(commandToSend, parser, (int)timeout.TotalMilliseconds, null);
        }

        /// <summary>
        /// Send a command and wait for a response. Both sending and receiving are optional
        /// </summary>
        /// <param name='commandToSend'>
        /// Binary data to be written to the port. If set to null, no data is written
        /// </param>
        /// <param name='parser'>
        /// Parser responsible for receiving the command response. If null, only a data write will be done
        /// </param>
        /// <param name='timeout'>
        /// Response timeout
        /// </param>
        /// <param name='cancelWaitEvent'>
        /// Cancel wait handle. When set, the command execution is canceled
        /// </param>
        public virtual ReadOperationResult Command(byte[] commandToSend, Parser parser, TimeSpan timeout, WaitHandle cancelWaitEvent)
        {
            return Command(commandToSend, parser, (int)timeout.TotalMilliseconds, cancelWaitEvent);
        }

        /// <summary>
        /// Send a command and wait for a response. Both sending and receiving are optional
        /// </summary>
        /// <param name='commandToSend'>
        /// Binary data to be written to the port. If set to null, no data is written
        /// </param>
        /// <param name='parser'>
        /// Parser responsible for receiving the command response. If null, only a data write will be done
        /// </param>
        /// <param name='timeoutMs'>
        /// Response timeout in ms
        /// </param>
        [Obsolete("Use cancellable method")]
        public virtual ReadOperationResult Command(byte[] commandToSend, Parser parser, int? timeoutMs)
        {
            return Command(commandToSend, parser, timeoutMs, null);
        }

#if FALSE
        DateTime debugTimestamp = DateTime.Now;
#endif

        /// <summary>
        /// Send a command and wait for a response. Both sending and receiving are optional
        /// </summary>
        /// <param name='commandToSend'>
        /// Binary data to be written to the port. If set to null, no data is written
        /// </param>
        /// <param name='parser'>
        /// Parser responsible for receiving the command response. If null, only a data write will be done
        /// </param>
        /// <param name='timeoutMs'>
        /// Response timeout in ms. If parser is null and timeout is given, will simply sleep for the duration after writing command data.
        /// </param>
        /// <param name='cancelWaitEvent'>
        /// Cancel wait handle. When set, the command execution is canceled
        /// </param>
        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        public virtual ReadOperationResult Command(byte[] commandToSend, Parser parser, int? timeoutMs, WaitHandle cancelWaitEvent)
        {
            CheckDisposed();
            CheckClosed();
            AutoResetEvent waitEvent = new AutoResetEvent(false);
            WaitHandle[] waitHandles;
            if (cancelWaitEvent != null)
            {
                waitHandles = new WaitHandle[] { waitEvent, cancelWaitEvent };
            }
            else
            {
                waitHandles = new WaitHandle[] { waitEvent };
            }

            var handler = new SingleParserDoneHandler(waitEvent);
            //DiscardInBuffer();

            if (parser != null)
            {
                lock (mWaitingParsers)
                {
                    handler.StartTime = DateTime.Now;
                    this.mWaitingParsers.Add(parser, handler);
#if FALSE
                DateTime now = DateTime.Now;
                Console.WriteLine(string.Format("Add parser: {0}", now.Subtract(debugTimestamp).ToFriendlyDisplay(2)));
                debugTimestamp = now;
#endif
                }
                if (ParserAdded != null)
                {
                    ParserAdded(this, new ParserEventArgs() { Parser = parser });
                }
            }

            if (commandToSend != null)
            {
                // if (Logger.CommLog.IsDebugEnabled)
                {
                    //   Logger.CommLog.DebugFormat("Command: {0}", ByteArray.ToStringCrLfToHex(commandString));
                }
                Write(commandToSend, 0, commandToSend.Length);
            }

            ProcessDataWithParsers();

            if (parser != null)
            {
                try
                {
                    mPortException = null;
                    int res = WaitHandle.WaitAny(waitHandles, (timeoutMs.HasValue && timeoutMs.Value > 0)? timeoutMs.Value : int.MaxValue, false);
                    if (res <= 1)
                    {
                        parser.ExecutionTime = DateTime.Now - handler.StartTime;
                        if (parser.Canceled || res == 1)
                        {
                            return new ReadOperationResult(ReadOperationResult.Result.CANCELLED);
                        }
                        if (mPortException != null)
                        {
                            return new ReadOperationResult(ReadOperationResult.Result.PORT_ERROR, mPortException);
                        }
                        return parser.GetResult();
                    }
                    else
                    {
                        ProcessDataWithParsers();
                        parser.ExecutionTime = DateTime.Now - handler.StartTime;
                        return handler.Result ?? new ReadOperationResult(ReadOperationResult.Result.TIMEOUT);
                    }
                }
                catch (Exception e)
                {
                    parser.ExecutionTime = DateTime.Now - handler.StartTime;
                    if (Settings.Default.UseLog4Net)
                    {
                        Logger.Error("Exception", e);
                    }                    
                    return new ReadOperationResult(ReadOperationResult.Result.INTERNAL_ERROR, e);
                }
                finally
                {
                    lock (mWaitingParsers)
                    {
                        this.mWaitingParsers.Remove(parser);
#if FALSE
                    DateTime now = DateTime.Now;
                    Console.WriteLine(string.Format("Remove parser: {0}", now.Subtract(debugTimestamp).ToFriendlyDisplay(2)));
                    debugTimestamp = now;
#endif
                    }
                    if (ParserRemoved != null)
                    {
                        ParserRemoved(this, new ParserEventArgs() { Parser = parser });
                    }

                }
            }
            else
            {
                if (timeoutMs.HasValue && timeoutMs > 0)
                {
                    Thread.Sleep(timeoutMs.Value);
                }
                return new ReadOperationResult(ReadOperationResult.Result.SUCCESS);
            }
        }

        /// <summary>
        /// Raised when data is received from port. Can be used for logging etc.
        /// </summary>
        public event EventHandler<DataReceivedEventArgs> OnDataReceived;

        /// <summary>
        /// Raised when data is sent to the port. Can be used for logging etc.
        /// </summary>
        public event EventHandler<DataSentEventArgs> OnDataSent;

        /// <summary>
        /// Warning: not implemented for all exceptions
        /// </summary>
        public event EventHandler<UnhandledExceptionEventArgs> OnPortError;
        protected void GenerateOnPortError(Exception ex)
        {
            if (OnPortError != null)
            {
                OnPortError(this, new UnhandledExceptionEventArgs(ex, false));
            }
        }

        public event EventHandler<UnhandledExceptionEventArgs> OnParserError;
        protected void GenerateOnParserError(Exception ex)
        {
            if (OnParserError != null)
            {
                OnParserError(this, new UnhandledExceptionEventArgs(ex, false));
            }
        }

        protected void GenerateOnDataSent(byte[] data, DateTime timestamp)
        {
            if (OnDataSent != null && data.IsNullOrEmpty() == false)
            {
                OnDataSent(this, new DataSentEventArgs(data, DateTime.Now));
            }
        }

        protected void DataReceivedHandler(object sender, byte[] buffer, int size)
        {
            if (buffer == null || size == 0)
            {
                return;
            }

            if (OnDataReceived != null)
            {
                OnDataReceived(sender, new DataReceivedEventArgs(ByteArray.Copy(buffer, 0, size), DateTime.Now));
            }
                        
            m_receivedDataQueue.EnqueueAll(buffer, 0, size);
            
            try
            {
                ProcessDataWithParsers();
            }
            catch (Exception ex)
            {
                if (Settings.Default.UseLog4Net)
                {
                    Logger.Error("DataReceivedHandler exception ", ex);
                }
            }            
        }

        protected void OnPortPinChanged(object sender, SerialPinChangedEventArgs e)
        {
            lock (mWaitingParsers)
            {
                try
                {
                    List<Parser> toRemove = new List<Parser>();

                    foreach (var pair in mWaitingParsers)
                    {
                        try
                        {
                            bool res = pair.Key.PinChanged(e);
                            if (res)
                            {
                                pair.Value.Result = pair.Key.GetResult();
                                if (pair.Value.Done(pair.Key))// release waiting process wait event
                                {
                                    // remove single-use parsers, don't remove permanent parsers
                                    toRemove.Add(pair.Key);
                                }
                            }

                            //Rx_Analyzer.PinChanged returns true when its waiting is done
                        }
                        catch (Exception ex)
                        {
                            if (Settings.Default.UseLog4Net)
                            {
                                Logger.Error("PortPinChanged exception ", ex);
                            }
                        }
                    }

                    mWaitingParsers.RemoveKeys(toRemove);
                    if (ParserRemoved != null)
                    {
                        foreach (var p in toRemove)
                        {
                            ParserRemoved(this, new ParserEventArgs() { Parser = p });
                        }
                    }
                }
                catch
                {
                    throw;
                }
            }
        }

        protected void ErrorReceivedHandler(object sender, Exception ex)
        {
            if (Settings.Default.UseLog4Net)
            {
                Logger.Error("Port error: {0}", ex);
            }
            if (!ignorePortErrors)
            {
                lock (mWaitingParsers)
                {
                    try
                    {
                        this.mPortException = ex;
                        foreach (var pair in mWaitingParsers)
                        {
                            pair.Value.Done(pair.Key);
                        }
                    }
                    catch
                    {
                        throw;
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// Cancel operation of a given parser
        /// </summary>
        /// <param name="parser"></param>
        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public void Cancel(Parser parser)
        {
            CheckDisposed();
            if (parser == null)
            {
                return;
            }

            List<Parser> toRemove = new List<Parser>();

            lock (mWaitingParsers)
            {
                try
                {
                    ParserDoneHandler waitEvent;
                    if (mWaitingParsers.TryGetValue(parser, out waitEvent))
                    {
                        parser.Canceled = true;
                        waitEvent.Done(parser);
                    }
                }
                catch
                {
                    throw;
                }

                mWaitingParsers.RemoveKeys(toRemove);
                if (ParserRemoved != null)
                {
                    foreach (var p in toRemove)
                    {
                        ParserRemoved(this, new ParserEventArgs() { Parser = p });
                    }
                }
            }
        }

        /// <summary>
        /// Cancel all response parsers
        /// </summary>
        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public void CancelAllParsers()
        {
            CheckDisposed();
            List<Parser> toRemove = new List<Parser>();

            lock (mWaitingParsers)
            {
                try
                {
                    foreach (var pair in mWaitingParsers)
                    {
                        pair.Key.Canceled = true;
                        pair.Value.Done(pair.Key);
                        toRemove.Add(pair.Key);
                    }
                }
                catch
                {
                    throw;
                }

                mWaitingParsers.RemoveKeys(toRemove);
                if (ParserRemoved != null)
                {
                    foreach (var p in toRemove)
                    {
                        ParserRemoved(this, new ParserEventArgs() { Parser = p });
                    }
                }
            }
        }

        private bool _disposed = false;

        ~AbstractPort()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);

            // Use SupressFinalize in case a subclass 
            // of this type implements a finalizer.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    CloseParsers();
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
        }
    }

    internal class ParserEventArgs : EventArgs
    {
        public Parser Parser;
    }

    public class DataReceivedEventArgs : EventArgs
    {
        public byte[] Data;
        public DateTime Timestamp;
        public DataReceivedEventArgs(byte[] data, DateTime timestamp)
        {
            this.Data = data;
            this.Timestamp = timestamp;
        }
    }

    public class DataSentEventArgs : DataReceivedEventArgs
    {
        public DataSentEventArgs(byte[] data, DateTime timestamp)
            : base(data, timestamp)
        {
        }
    }
}