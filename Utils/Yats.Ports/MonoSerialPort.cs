﻿// /*
// Copyright 2013 Antanas Veiverys www.veiverys.com
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// */
//
using System;
using System.IO.Ports;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Threading;
using yats.Utilities;

namespace yats.Ports
{
    [ComVisible(false)]//fix code analysis warning
    public class MonoSerialPort : System.IO.Ports.SerialPort
    {
        public MonoSerialPort()
            : base()
        {
        }

        public MonoSerialPort(IContainer container)
            : base(container)
        {
        }

        public MonoSerialPort(string portName)
            : base(portName)
        {
        }

        public MonoSerialPort(string portName, int baudRate)
            : base(portName, baudRate)
        {
        }

        public MonoSerialPort(string portName, int baudRate, Parity parity)
            : base(portName, baudRate, parity)
        {
        }

        public MonoSerialPort(string portName, int baudRate, Parity parity, int dataBits)
            : base(portName, baudRate, parity, dataBits)
        {
        }

        public MonoSerialPort(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits)
            : base(portName, baudRate, parity, dataBits, stopBits)
        {
        }

        FieldInfo fieldInfo;
        public new void Open()
        {
            base.Open();
            if (PlatformHelper.IsRunningOnMono())
            {
                fieldInfo = typeof(System.IO.Ports.SerialPort).GetField("data_received", BindingFlags.Instance | BindingFlags.NonPublic);

                byte[] buffer = new byte[0];
                System.Action startReadDelegate = null;
                startReadDelegate = delegate
                {
                    BaseStream.BeginRead(buffer, 0, buffer.Length, delegate (IAsyncResult ar)
                    {
                        try
                        {
                            int actualLength = BaseStream.EndRead(ar);
                            //byte[] received = new byte[actualLength];
                            //Buffer.BlockCopy(buffer, 0, received, 0, actualLength);
                            //DataReceivedTmp
                            OnDataReceived(null);
                        }
                        catch (IOException)
                        {
                            //handleAppSerialError(exc);
                        }
                        catch (TimeoutException)
                        {
                        }
                        if (IsOpen)
                        {
                            startReadDelegate();
                        }
                    }, null);
                };
                startReadDelegate();
            }
        }

        protected void OnDataReceived(SerialDataReceivedEventArgs args)
        {
            object data_received;

            data_received = fieldInfo.GetValue(this);

            SerialDataReceivedEventHandler handler = (SerialDataReceivedEventHandler)Events[data_received];

            if (handler != null)
            {
                handler(this, args);
            }
        }
    }
}
