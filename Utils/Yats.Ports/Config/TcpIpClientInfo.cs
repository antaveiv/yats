﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;

namespace yats.Ports
{
    internal class TcpIpClientInfo
    {
        protected int portNumber;
        public int PortNumber
        {
            [DebuggerStepThrough]
            get { return portNumber; }
            [DebuggerStepThrough]
            set { portNumber = value; }
        }

        protected string address;
        public string Address
        {
            [DebuggerStepThrough]
            get { return address; }
            [DebuggerStepThrough]
            set { address = value; }
        }

        protected bool keepAlive;
        public bool KeepAlive
        {
            [DebuggerStepThrough]
            get
            { return keepAlive; }
            [DebuggerStepThrough]
            set
            { keepAlive = value; }
        }

        public bool DiscardDataWithoutParsers { get; set; }

        [DebuggerStepThrough]
        public TcpIpClientInfo()
        {
            DiscardDataWithoutParsers = true;
        }

        [DebuggerStepThrough]
        public TcpIpClientInfo(string address, int portNumber, bool discardDataWithoutParsers, bool keepAlive)
            : this()
        {
            this.address = address;
            this.portNumber = portNumber;
            this.DiscardDataWithoutParsers = discardDataWithoutParsers;
            this.keepAlive = keepAlive;
        }

        public TcpIpClientInfo Clone()
        {
            TcpIpClientInfo result = new TcpIpClientInfo();
            result.portNumber = this.portNumber;
            result.address = this.address;
            result.DiscardDataWithoutParsers = this.DiscardDataWithoutParsers;
            result.keepAlive = this.keepAlive;
            return result;
        }

        public override string ToString()
        {
            return string.Format("TCP/IP client {0}:{1}", address, portNumber);
        }
    }
}
