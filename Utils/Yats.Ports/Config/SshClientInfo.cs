﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;

namespace yats.Ports
{
    internal class SshClientInfo
    {
        protected string host;
        public string Host
        {
            [DebuggerStepThrough]
            get { return host; }
            [DebuggerStepThrough]
            set { host = value; }
        }

        protected string user;
        public string User
        {
            [DebuggerStepThrough]
            get { return user; }
            [DebuggerStepThrough]
            set { user = value; }
        }

        protected string password;
        public string Password
        {
            [DebuggerStepThrough]
            get { return password; }
            [DebuggerStepThrough]
            set { password = value; }
        }

        public bool DiscardDataWithoutParsers { get; set; }
        
        [DebuggerStepThrough]
        public SshClientInfo()
        {
            DiscardDataWithoutParsers = true;
        }

        [DebuggerStepThrough]
        public SshClientInfo(string host, string user, string password, bool discardDataWithoutParsers)
            : this()
        {
            this.host = host;
            this.user = user;
            this.password = password;
            this.DiscardDataWithoutParsers = discardDataWithoutParsers;
        }

        public SshClientInfo Clone()
        {
            SshClientInfo result = new SshClientInfo( this.host, this.user, this.password, this.DiscardDataWithoutParsers);
            return result;
        }

        public override string ToString()
        {
            return string.Format("SSH client {0}@{1}", user, host);
        }

        public override bool Equals(object obj)
        {
            if(object.ReferenceEquals( this, obj ))
            {
                return true;
            }

            SshClientInfo other = obj as SshClientInfo;
            if(other == null || this == null)
            {
                return false;
            }

            return this.host == other.host && this.user == other.user && this.password == other.password;
        }

        public override int GetHashCode()
        {
            int res = 88311;
            if (host != null)
            {
                res ^= host.GetHashCode();
            }
            if (user != null)
            {
                res ^= user.GetHashCode();
            }
            if (password != null)
            {
                res ^= password.GetHashCode();
            }
            return res;
        }
    }
}
