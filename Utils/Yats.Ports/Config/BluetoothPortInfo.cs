﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using InTheHand.Net;
using System.Diagnostics;

namespace yats.Ports
{
    internal class BluetoothPortInfo
    {
        [DebuggerStepThrough]
        public BluetoothPortInfo()
        {
            DiscardDataWithoutParsers = true;
        }

        [DebuggerStepThrough]
        public BluetoothPortInfo(BluetoothAddress address, string pin, bool discardDataWithoutParsers)
            : this()
        {
            this.address = address;
            this.pin = pin;
            this.DiscardDataWithoutParsers = discardDataWithoutParsers;
        }


        protected BluetoothAddress address;
        public BluetoothAddress Address
        {
            [DebuggerStepThrough]
            get
            {
                return address;
            }
            [DebuggerStepThrough]
            set
            {
                address = value;
            }
        }

        protected string pin = "";
        public string PIN
        {
            [DebuggerStepThrough]
            get
            {
                return pin;
            }
            [DebuggerStepThrough]
            set
            {
                pin = value;
            }
        }

        public bool DiscardDataWithoutParsers { get; set; }


        public BluetoothPortInfo Clone()
        {
            BluetoothPortInfo clone = new BluetoothPortInfo
            {
                address = (BluetoothAddress)this.address.Clone(),
                pin = this.pin,
                DiscardDataWithoutParsers = this.DiscardDataWithoutParsers
            };
            return clone;
        }

        public override string ToString()
        {
            return string.Format("Bluetooth port {0} [{1}]", address, pin);
        }
    }
}
