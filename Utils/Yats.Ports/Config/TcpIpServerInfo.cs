﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;

namespace yats.Ports
{
    internal class TcpIpServerInfo
    {
        protected int portNumber;
        public int PortNumber
        {
            [DebuggerStepThrough]
            get { return portNumber; }
            [DebuggerStepThrough]
            set { portNumber = value; }
        }
        
        protected bool replaceSocket;
        public bool ReplaceSocket
        {
            [DebuggerStepThrough]
            get { return replaceSocket; }
            [DebuggerStepThrough]
            set { replaceSocket = value; }
        }
        
        public bool DiscardDataWithoutParsers { get; set; }

        protected bool keepAlive;
        public bool KeepAlive
        {
            [DebuggerStepThrough]
            get
            { return keepAlive; }
            [DebuggerStepThrough]
            set
            { keepAlive = value; }
        }

        [DebuggerStepThrough]
        public TcpIpServerInfo()
        {
            DiscardDataWithoutParsers = true;
        }

        [DebuggerStepThrough]
        public TcpIpServerInfo(int portNumber, bool replaceSocket, bool discardDataWithoutParsers, bool keepAlive)
            : this()
        {
            this.portNumber = portNumber;
            this.replaceSocket = replaceSocket;
            this.DiscardDataWithoutParsers = discardDataWithoutParsers;
            this.keepAlive = keepAlive;
        }

        public TcpIpServerInfo Clone()
        {
            TcpIpServerInfo result = new TcpIpServerInfo();
            result.portNumber = this.portNumber;
            result.replaceSocket = this.replaceSocket;
            result.DiscardDataWithoutParsers = this.DiscardDataWithoutParsers;
            result.keepAlive = this.keepAlive;
            return result;
        }

        public override string ToString()
        {
            return string.Format("TCP/IP server port {0}", portNumber);
        }
    }
}
