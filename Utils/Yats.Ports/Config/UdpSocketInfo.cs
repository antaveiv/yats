﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;
using System.Net;

namespace yats.Ports
{
    internal class UdpSocketInfo
    {
        int localPortNumber, remotePortNumber;
        IPAddress ip;
        bool overwriteIP = false;

        public int LocalPort
        {
            get { return localPortNumber; }
        }

        public int RemotePort
        {
            get { return remotePortNumber; }
        }

        public IPAddress IP
        {
            get { return ip; }
        }

        public bool DiscardDataWithoutParsers { get; set; }
        
        [DebuggerStepThrough]
        public UdpSocketInfo()
        {
            DiscardDataWithoutParsers = true;
        }

        [DebuggerStepThrough]
        public UdpSocketInfo(IPAddress ip, int localPort, int remotePort, bool overwriteIP, bool discardDataWithoutParsers)
        {
            this.ip = ip;
            this.localPortNumber = localPort;
            this.remotePortNumber = remotePort;
            this.overwriteIP = overwriteIP;
            this.DiscardDataWithoutParsers = discardDataWithoutParsers;
        }

        public UdpSocketInfo Clone()
        {
            UdpSocketInfo result = new UdpSocketInfo(this.ip, this.localPortNumber, this.remotePortNumber, this.overwriteIP, this.DiscardDataWithoutParsers);
            return result;
        }

        public override string ToString()
        {
            if (!IsConfigured)
            {
                return "UDP socket: not configured";
            }
            return string.Format("UDP socket {0} {1}-{2}", (ip != null)?ip.ToString() : "IP not set", localPortNumber, remotePortNumber);
        }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }

            UdpSocketInfo other = obj as UdpSocketInfo;
            if (other == null || this == null)
            {
                return false;
            }

            return this.localPortNumber == other.localPortNumber && this.remotePortNumber == other.remotePortNumber && (this.ip == other.ip || this.overwriteIP || other.overwriteIP);
        }

        public override int GetHashCode()
        {
            int res = 88311;
            res ^= localPortNumber;
            res ^= remotePortNumber;
            if (ip != null)
            {
                res ^= ip.GetHashCode();
            }
            return res;
        }

        public bool IsConfigured
        {
            get
            {
                return ip != null && localPortNumber > 0 && localPortNumber < ushort.MaxValue && remotePortNumber > 0 && remotePortNumber < ushort.MaxValue;
            }
        }
    }
}
