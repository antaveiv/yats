﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Diagnostics;
using System.IO.Ports;

namespace yats.Ports
{
    /// <summary>
    /// Internal class. Use in unit tests only
    /// </summary>
    public class SerialPortInfo
    {
        [DebuggerStepThrough]
        public SerialPortInfo()
        {
            DiscardDataWithoutParsers = true;
        }

        [DebuggerStepThrough]
        public SerialPortInfo(bool discardDataWithoutParsers, string deviceName, int baudRate, int dataBits, Parity parity, StopBits stopBits, RtsEnableEnum rtsEnable, bool dtrEnable, Handshake handshake)
            : this()
        {
            this.portName = deviceName;
            this.baudRate = baudRate;
            this.dataBits = dataBits;
            this.parity = parity;
            this.stopBits = stopBits;
            this.rtsEnable = rtsEnable;
            this.dtrEnable = dtrEnable;
            this.handshake = handshake;
            this.DiscardDataWithoutParsers = discardDataWithoutParsers;
        }
                
        protected int baudRate = 9600;
        public int BaudRate
        {
            [DebuggerStepThrough]
            get
            {
                return baudRate;
            }
            [DebuggerStepThrough]
            set
            {
                baudRate = value;
            }
        }

        protected string portName = "COM1";
        public string PortName
        {
            [DebuggerStepThrough]
            get
            {
                return portName;
            }
            [DebuggerStepThrough]
            set
            {
                portName = value;
            }
        }

        /// <summary>
        /// Gets or sets the standard length of data bits per byte.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// The data bits length.
        /// 
        /// </returns>
        /// <exception cref="T:System.IO.IOException">The port is in an invalid state.
        /// 
        /// - or -
        /// 
        /// An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort"/> object were invalid.
        /// </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The data bits value is less than 5 or more than 8. </exception>

        protected int dataBits = 8;
        public int DataBits
        {
            [DebuggerStepThrough]
            get
            {
                return dataBits;
            }
            [DebuggerStepThrough]
            set
            {
                dataBits = value;
            }
        }

        /// <summary>
        /// Gets or sets the parity-checking protocol.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// One of the <see cref="T:System.IO.Ports.Parity"/> values that represents the parity-checking protocol. The default is None.
        /// 
        /// </returns>
        /// <exception cref="T:System.IO.IOException">The port is in an invalid state.
        /// 
        /// - or -
        /// 
        /// An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort"/> object were invalid.
        /// </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The <see cref="P:System.IO.Ports.SerialPort.Parity"/> value passed is not a valid value in the <see cref="T:System.IO.Ports.Parity"/> enumeration. </exception>
        /// <PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/></PermissionSet>
        protected Parity parity = Parity.None;
        public Parity Parity
        {
            [DebuggerStepThrough]
            get
            {
                return parity;
            }
            [DebuggerStepThrough]
            set
            {
                parity = value;
            }
        }

        /// <summary>
        /// Gets or sets the standard number of stopbits per byte.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// One of the <see cref="T:System.IO.Ports.StopBits"/> values.
        /// 
        /// </returns>
        /// <exception cref="T:System.ArgumentOutOfRangeException">The <see cref="P:System.IO.Ports.SerialPort.StopBits"/> value is not one of the values from the <see cref="T:System.IO.Ports.StopBits"/> enumeration. </exception>
        /// <exception cref="T:System.IO.IOException">The port is in an invalid state.
        /// 
        /// - or -
        /// 
        /// An attempt to set the state of the underlying port failed. For example, the parameters passed from this <see cref="T:System.IO.Ports.SerialPort"/> object were invalid.
        /// </exception>
        /// <PermissionSet><IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/></PermissionSet>
        protected StopBits stopBits = StopBits.One;
        public StopBits StopBits
        {
            [DebuggerStepThrough]
            get
            {
                return stopBits;
            }
            [DebuggerStepThrough]
            set
            {
                stopBits = value;
            }
        }
                

        const RtsEnableEnum DEFAULT_RTS_ENABLE = RtsEnableEnum.DONT_CHANGE;
        protected RtsEnableEnum rtsEnable = DEFAULT_RTS_ENABLE;
        public RtsEnableEnum RtsEnable
        {
            get
            {
                return rtsEnable;
            }
            set
            {
                rtsEnable = value;
            }
        }

        const bool DEFAULT_DTR = false;
        protected bool dtrEnable = DEFAULT_DTR;
        public bool DtrEnable
        {
            get
            {
                return dtrEnable;
            }
            set
            {
                dtrEnable = value;
            }
        }

        const Handshake DEFAULT_HANDSHAKE = Handshake.None;
        protected Handshake handshake = DEFAULT_HANDSHAKE;
        public Handshake Handshake
        {
            get
            {
                return handshake;
            }
            set
            {
                handshake = value;
            }
        }

        public bool DiscardDataWithoutParsers { get; set; }

        public SerialPortInfo Clone()
        {
            SerialPortInfo result = new SerialPortInfo();
            result.baudRate = this.baudRate;
            result.dataBits = this.dataBits;
            //result.newLine = this.newLine;
            result.parity = this.parity;
            result.portName = this.portName;
            //result.readBufferSize = this.readBufferSize;
            //result.readTimeout = this.readTimeout;
            result.rtsEnable = this.rtsEnable;
            result.dtrEnable = this.dtrEnable;
            result.stopBits = this.stopBits;
            //result.writeBufferSize = this.writeBufferSize;
            result.handshake = this.handshake;
            result.DiscardDataWithoutParsers = this.DiscardDataWithoutParsers;
            return result;
        }

        public override string ToString()
        {
            string stop = "";
            switch (stopBits){
                case StopBits.None:
                    stop = "0";
                    break;
                case StopBits.One:
                    stop = "1";
                    break;
                case StopBits.OnePointFive:
                    stop = "1.5";
                    break;
                case StopBits.Two:
                    stop = "2";
                    break;
            }

            return string.Format( "Serial port {0} {1}bps {2}.{3} {4}", portName, baudRate, dataBits, stop, parity.ToString( ));
        }
    }
}
