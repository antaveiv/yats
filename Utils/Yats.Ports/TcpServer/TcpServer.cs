using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using yats.Utilities;

// inspired by http://www.codeproject.com/csharp/BasicTcpServer.asp

namespace yats.Ports
{
	public class TcpServer : TcpIpClientSocket
    {
    	protected Socket listener;
        protected readonly bool replaceSocket;

		public TcpServer(int port, bool replaceSocket): base(port)
		{
            this.replaceSocket = replaceSocket;
            OnClose += new EventHandler(TcpServer_OnClose);
		}
        
        public TcpServer(int port): this(port, false)
        {
        }

        void TcpServer_OnClose(object sender, EventArgs e)
        {
            WaitEvent.Set();
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override void Open()
        {
            CheckDisposed();
            StartListening();
        }

		/// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public void StartListening()
		{
            CheckDisposed();
            lock (this)
            {
                if (listener != null)
                {
                    return;
                }
                try {
                    IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, PortNumber);
                    listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    listener.Bind(endPoint);
                    listener.Listen(10);
                    listener.BeginAccept(AcceptConnection, null);
                    if (Logger != null && Logger.IsDebugEnabled)
                    {
                        Logger.Debug("Listening");
                    }
                }
                catch { listener = null; throw; }           
            }
		}

		/// <summary>
		/// Shuts down the listener.
		/// </summary>
		protected void StopListening()
		{
			// Make sure we're not accepting a connection.
			lock (this)
			{
                if (listener != null)
                {
                    listener.Close();
                    listener = null;
                    if (Logger != null && Logger.IsDebugEnabled)
                    {
                        Logger.Debug("Stopped listening");
                    }
                    WaitEvent.Set();
                }
			}
		}

		/// <summary>
		/// Accepts the connection and invokes any Connected event handlers.
		/// </summary>
		/// <param name="res"></param>
		protected void AcceptConnection(IAsyncResult res)
		{
			// Make sure listener doesn't go null on us.
			lock (this)
			{
                if (listener == null)
                {
                    return;
                }
                if (_disposed)
                {
                    return;
                }
                ClientSocket = listener.EndAccept(res);

                if (Logger != null && Logger.IsDebugEnabled)
                {
                    Logger.Debug("Client connected: " + ClientSocket.RemoteEndPoint);
                }

                if (replaceSocket)
                {
                    listener.BeginAccept(AcceptConnection, null); // accept new connection
                    WaitEvent.Set();		
                }
                else
                {
                    StopListening();
                }
			}	
		}
                
        bool isCanceled;
        /// <summary>
        /// Gets cancelled state. If set to true, cancels listening
        /// </summary>
        public bool IsCanceled
        {
            get { return isCanceled; }
            set
            {
                if (!isCanceled && value)
                {
                    StopListening();
                    WaitEvent.Set();			
                }
                isCanceled = value;                
            }
        }
        
        public bool SetKeepAlive { get; set; }

        /// <summary>
        /// Listening cancel event. Listening is canceled when the event is set
        /// </summary>        
        public readonly AutoResetEvent WaitEvent = new AutoResetEvent(false);
        
        /// <summary>
        /// If true, switch communication to new socket when a new socket is connected.
        /// </summary>
        public bool ReplaceSocket
        {
            get { return replaceSocket; }
        }

        public bool IsListening
        {
            get { return listener != null; }
        }

        public override void Close()
        {
            base.Close();
            StopListening();
        }

        private bool _disposed = false;

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    lock (this)
                    {
                        if (listener != null)
                        {
                            listener.Close();
                            listener = null;
                            if (Logger != null && Logger.IsDebugEnabled)
                            {
                                Logger.Debug("Stopped listening");
                            }
                            WaitEvent.Set();
                            WaitEvent.Close();
                        }
                    }
                }

                _disposed = true;
            }
            base.Dispose(disposing);
        }

        protected override void CheckDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            base.CheckDisposed();
        }
    }
}
