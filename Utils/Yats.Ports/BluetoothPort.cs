﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using InTheHand.Net;
using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using yats.Utilities;
using log4net;
using yats.Ports.Properties;

namespace yats.Ports
{
    //A wrapper around InTheHand.Net.Bluetooth port
    public class BluetoothPort : AbstractPort, IDisposable
    {
        //can be set by showing SelectBluetoothDeviceDialog, then taking SelectedDevice.DeviceAddress
        public BluetoothAddress BluetoothAddress { get; set; }
        public string PIN { get; set; }
        protected BluetoothClient client;
        protected NetworkStream stream;
        protected byte[] buffer = new byte[1024];
        protected ILog Logger;

        public BluetoothClient Client
        {
            get { return client; }
        }

        public BluetoothPort()
        {
        }

        public BluetoothPort(BluetoothAddress address, string pin):this()
        {
            this.BluetoothAddress = address;
            this.PIN = pin;
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override bool IsOpen()
        {
            CheckDisposed();
            return client != null && client.Connected;
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override void Open()
        {
            CheckDisposed();
            if (Settings.Default.UseLog4Net)
            {
                Logger = LogManager.GetLogger(string.Format("Bluetooth"));
            }
            BluetoothDeviceInfo deviceInfo = new BluetoothDeviceInfo(BluetoothAddress);
            if (!deviceInfo.Authenticated)
            {
                BluetoothSecurity.PairRequest(BluetoothAddress, PIN);
            }
            client = new BluetoothClient();
            Guid serviceClass;
            serviceClass = BluetoothService.SerialPort;
            var ep = new BluetoothEndPoint(BluetoothAddress, serviceClass);
            client.Connect(ep);
            stream = client.GetStream();
            stream.BeginRead(buffer, 0, buffer.Length, new AsyncCallback(ReceiveCallback), this);
        }

        protected void ReceiveCallback(IAsyncResult ar)
        {
            // Retrieve the state object and the client socket 
            // from the asynchronous state object.
            if (stream == null)
            {
                return;
            }

            if (IsOpen() == false)
            {
                return;
            }

            try
            {
                // Read data from the remote device.
                int bytesRead = stream.EndRead(ar);
                if (bytesRead > 0)
                {
                    DataReceivedHandler(this, buffer, bytesRead);
                    if (IsOpen() == false)
                    {
                        return;
                    }
                    //  continue receiving
                    stream.BeginRead(buffer, 0, buffer.Length, new AsyncCallback(ReceiveCallback), this);
                }
                else
                {
                    if (Logger != null)
                    {
                        Logger.Debug("Client disconnected");
                    }
                    Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception {0}", e);
                this.ErrorReceivedHandler(this, e);
                Close();
            }
        }

        public event EventHandler OnClose;

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override void Close()
        {
            CheckDisposed();
            CloseParsers();
            if (IsOpen())
            {
                client.Close();
                client.Dispose();
                Console.WriteLine("Port closed");
                if (OnClose != null)
                {
                    OnClose(this, null);
                }
            }
            client = null;
            stream = null;
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        public override void Write(byte[] buffer, int offset, int count)
        {
            CheckDisposed();
            CheckClosed();
            
            try
            {
                stream.Write(buffer, offset, count);
            }
            catch (Exception ex)
            {
                if (Logger != null)
                {
                    Logger.Debug("Port exception: ", ex);
                }
                throw;
            }
            
            GenerateOnDataSent(ByteArray.Copy(buffer, offset, count), DateTime.Now);
            
            if (offset == 0 && count == buffer.Length)
            {
                if (Logger != null)
                {
                    Logger.Debug(buffer);
                }
            }
            else
            {
                if (Logger != null)
                {
                    Logger.Debug(ByteArray.Copy(buffer, offset, count));
                }
            }
        }

        
        private bool _disposed = false;

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (client != null)
                    {
                        client.Dispose();
                        client = null;
                    }
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
            base.Dispose(disposing);
        }

        protected override void CheckDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            base.CheckDisposed();
        }
    }
}
