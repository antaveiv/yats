﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;

namespace yats.Ports
{
    public abstract class Parser
    {
        /// <summary>
        /// When a data byte is received by a port, this function is called
        /// </summary>
        /// <returns>True if the analyzer has finished receiving all the necessary data to make a pass/fail decision. <br/>False, if more data should be read from the port</returns>
        //TODO rename to ParseByte
        public abstract bool IncomingData(byte dataByte);

        /// <summary>
        /// Is called automatically after IncomingData returns true. It gives a pass/fail result of the command
        /// </summary>
        /// <returns>value that the command(...) method of ExtSerialPort class should return as the result of the current function</returns>
        ///TODO rename to ParseResult GetParseResult()
        public abstract ReadOperationResult GetResult();

        //TODO is this used?
        public virtual bool PinChanged(System.IO.Ports.SerialPinChangedEventArgs e)
        {
            return false;
        }

        /// <summary>
        /// Implement if needed by application. Not used by the Port library.
        /// </summary>
        /// <returns>Data received while the parser was active</returns>
        //TODO remove
        public virtual byte[] GetReceivedData()
        {
            throw new NotImplementedException();
        }

        public virtual TimeSpan ExecutionTime
        {
            get;
            internal set;
        }

        internal bool Canceled = false;
    }
}
