﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Threading;
using System;
using System.Text;
using yats.Utilities;
using System.IO;
using System.Linq;
using System.Collections;

namespace yats.Ports
{
    /// <summary>
    /// Virtual port, simulates receiving predefined data. The data is consumed only when a read is done (by executing a command, doing a read etc.)
    /// </summary>
    public class PortEmulator : AbstractPort
    {
        private readonly object mLockObject = new object();
        bool isOpen = false;
        protected Queue simulatedReceivedBytes = Queue.Synchronized(new Queue());
        protected readonly Queue<byte> simulatedSentBytes = new Queue<byte>();

        public PortEmulator()
        {
            ParserAdded += PortEmulator_ParserAdded;
        }

        public PortEmulator(params byte[] receivedBytes)
            : this()
        {
            simulatedReceivedBytes.EnqueueAll(receivedBytes);
        }

        public PortEmulator(byte[] receivedBytes, int offset, int count)
            : this()
        {
            simulatedReceivedBytes.EnqueueAll(receivedBytes, offset, count);
        }

        public PortEmulator(IEnumerable<byte> receivedBytes)
            : this()
        {
            simulatedReceivedBytes.EnqueueAll(receivedBytes);
        }

        public PortEmulator(string received, Encoding encoding)
            : this(encoding.GetBytes(received))
        {
        }

        public PortEmulator(string received)
            : this(Encoding.ASCII.GetBytes(received))
        {
        }

        public static PortEmulator FromFile(string receivedDataFileName)
        {
            return new PortEmulator(File.ReadAllBytes(receivedDataFileName));
        }

        void PortEmulator_ParserAdded(object sender, ParserEventArgs e)
        {
            lock (mLockObject)
            {
                while (GetWaitingParserCount() > 0 && simulatedReceivedBytes.Count > 0)
                {
                    byte b = (byte)simulatedReceivedBytes.Dequeue();
                    DataReceivedHandler(this, new byte[] { b }, 1);
                }
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override bool IsOpen()
        {
            CheckDisposed();
            return isOpen;
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override void Open()
        {
            CheckDisposed();
            isOpen = true;
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override void Close()
        {
            CheckDisposed();
            CloseParsers();
            isOpen = false;
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when data is already being consumed by a parser</exception> 
        [Obsolete("Use Execute, Command methods to pass received data to a parser")]
        public override int Read(byte[] buffer, int offset, int count)
        {
            CheckDisposed();
            CheckClosed();
            if (GetWaitingParserCount() > 0)
            {
                throw new InvalidOperationException("Can not read: data is processed by a Parser");
            }

            int idx = offset;
            int result = 0;
            lock (mLockObject)
            {
                while (count > 0)
                {
                    if (simulatedReceivedBytes.Count > 0)
                    {
                        byte b = (byte)simulatedReceivedBytes.Dequeue();
                        buffer[idx++] = b;
                        count--;
                        result++;
                    }
                    else
                    {
                        break;
                    }                    
                }
            }
            return result;
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when data is already being consumed by a parser</exception> 
        [Obsolete("Use Execute, Command methods to pass received data to a parser")]
        public override int ReadByte()
        {
            CheckDisposed();
            CheckClosed();
            if (GetWaitingParserCount() > 0)
            {
                throw new InvalidOperationException("Can not read: data is processed by a Parser");
            }
            lock (mLockObject)
            {
                if (simulatedReceivedBytes.Count > 0)
                {
                    byte b = (byte)simulatedReceivedBytes.Dequeue();
                    return b;
                }
                return -1;
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        public override void Write(byte[] buffer, int offset, int count)
        {
            CheckDisposed();
            CheckClosed();
            if (buffer == null)
            {
                return;
            }
            for (int i = offset; i < offset + count; i++)
            {
                simulatedSentBytes.Enqueue(buffer[i]);
            }
            GenerateOnDataSent(ByteArray.Copy(buffer, offset, count), DateTime.Now);
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        public override ReadOperationResult Command(byte[] commandToSend, Parser parser, int? timeoutMs, WaitHandle cancelWaitEvent)
        {
            CheckDisposed();
            CheckClosed();
            return base.Command(commandToSend, parser, timeoutMs, cancelWaitEvent);
        }
        
        public void EmulateReceivedData(byte[] data)
        {
            EmulateReceivedData(data, 0, data.Length);
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        public void EmulateReceivedData(byte[] data, int offset, int count)
        {
            CheckDisposed();
            CheckClosed();
            lock (mLockObject)
            {
                simulatedReceivedBytes.EnqueueAll(data, offset, count);
            }
            PortEmulator_ParserAdded(null, null);            
        }


        private bool _disposed = false;

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // nothing to dispose yet. Just keeping uniform API between all port variants
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
            base.Dispose(disposing);
        }

        protected override void CheckDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            base.CheckDisposed();
        }
    }
}
