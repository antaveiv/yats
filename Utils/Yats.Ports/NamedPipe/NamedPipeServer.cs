﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Ports.Properties;
using yats.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;

namespace yats.Ports
{
    public class NamedPipeServer : AbstractPort
    {
        protected NamedPipeServerStream pipeServer = null;
        public PipeDirection Direction { get; set; }
        public override String Name { get; set; }

        protected ILog Logger;
        protected readonly object mLockObject = new object();
        protected byte[] buffer = new byte[1024];

        public NamedPipeServer()
        {
            Direction = PipeDirection.InOut;
        }

        public NamedPipeServer(string pipeName)
            : this()
        {
            this.Name = pipeName;
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Port is already open</exception> 
        public override void Open()
        {
            CheckDisposed();
            lock (mLockObject)
            {
                if (pipeServer != null)
                {
                    throw new InvalidOperationException("port is already open");
                }

                pipeServer = new NamedPipeServerStream(Name, Direction, 1, PipeTransmissionMode.Message, PipeOptions.Asynchronous);
                pipeServer.BeginWaitForConnection(ConnectCallback, this);
                
                if (Settings.Default.UseLog4Net)
                {
                    Logger = LogManager.GetLogger(string.Format("{0}", Name));
                }
                if (Logger != null && Logger.IsDebugEnabled)
                {
                    Logger.DebugFormat("Pipe {0} opened", Name);
                }
            }
        }
        
        private void ConnectCallback(IAsyncResult ar)
        {
            if (_disposed || pipeServer == null)
            {
                return;
            }
            pipeServer.EndWaitForConnection(ar);
            pipeServer.BeginRead(buffer, 0, buffer.Length,
                                new AsyncCallback(ReceiveCallback), this);
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            // Retrieve the state object and the client socket 
            // from the asynchronous state object.
            NamedPipeServer client = (NamedPipeServer)ar.AsyncState;
            NamedPipeServerStream socket = client.pipeServer;

            lock (client.mLockObject)
            {
                if (socket == null)
                {
                    return;
                }
            }

            try
            {

                // Read data from the remote device.
                int bytesRead = socket.EndRead(ar);
                if (bytesRead > 0)
                {
                    DataReceivedHandler(client, client.buffer, bytesRead);
                    lock (client.mLockObject)
                    {
                        if (socket == null)
                        {
                            return;
                        }
                    }
                    //  continue receiving
                    socket.BeginRead(client.buffer, 0, client.buffer.Length,
                        new AsyncCallback(ReceiveCallback), client);
                }
            }
            catch (Exception e)
            {
                if (Logger != null)
                {
                    Logger.Error("Exception ", e);
                }
                this.ErrorReceivedHandler(this, e);
                socket.Close();
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override void Close()
        {
            CheckDisposed();
            CloseParsers();
            lock (mLockObject)
            {
                if (pipeServer == null)
                {
                    return;
                }
                pipeServer.Close();
                pipeServer = null;
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        public override void Write(byte[] buffer, int offset, int count)
        {
            CheckDisposed();
            CheckClosed();
            pipeServer.Write(buffer, offset, count);
            GenerateOnDataSent(ByteArray.Copy(buffer, offset, count), DateTime.Now);
        }

        public override bool IsOpen()
        {
            CheckDisposed();
            lock (mLockObject)
            {
                return pipeServer != null;
            }
        }
        
        private bool _disposed = false;

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // Clear all property values that maybe have been set
                    // when the class was instantiated
                    if (pipeServer != null)
                    {
                        pipeServer.Dispose();
                        pipeServer = null;
                    }
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
            base.Dispose(disposing);
        }

        protected override void CheckDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            base.CheckDisposed();
        }
    }
}
