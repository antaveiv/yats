﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using yats.Ports.Properties;
using yats.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;

namespace yats.Ports
{
    public class NamedPipeClient : AbstractPort
    {
        protected NamedPipeClientStream pipeClient = null;
        public PipeDirection Direction { get; set; }
        public int? ConnectionTimeout { get; set; }
        //Pipe name
        public override String Name { get; set; }
        
        //The name of the remote computer to connect to, or "." to specify the local     computer.
        public String ServerName { get; set; }

        protected ILog Logger;
        protected readonly object mLockObject = new object();
        protected byte[] buffer = new byte[1024];

        public NamedPipeClient()
        {
            ServerName = ".";
            Direction = PipeDirection.InOut;
        }

        public NamedPipeClient(string pipeName) :  this()
        {
            Name = pipeName;            
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Port is already open</exception> 
        public override void Open()
        {
            CheckDisposed();
            lock (mLockObject)
            {
                if (pipeClient != null)
                {
                    throw new InvalidOperationException("port is already open");
                }

                pipeClient = new NamedPipeClientStream(ServerName, Name, Direction, PipeOptions.Asynchronous);
                if (ConnectionTimeout.HasValue)
                {
                    pipeClient.Connect(ConnectionTimeout.Value);
                }
                else
                {
                    pipeClient.Connect();
                }
                pipeClient.BeginRead(buffer, 0, buffer.Length,
                        new AsyncCallback(ReceiveCallback), this);
                if (Settings.Default.UseLog4Net)
                {
                    Logger = LogManager.GetLogger(string.Format("{0}", Name));
                }
                if (Logger != null && Logger.IsDebugEnabled)
                {
                    Logger.DebugFormat("Pipe {0} opened", Name);
                }
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            // Retrieve the state object and the client socket 
            // from the asynchronous state object.
            NamedPipeClient client = (NamedPipeClient)ar.AsyncState;
            NamedPipeClientStream socket = client.pipeClient;

            lock (client.mLockObject)
            {
                if (socket == null)
                {
                    return;
                }
            }

            try
            {

                // Read data from the remote device.
                int bytesRead = socket.EndRead(ar);
                if (bytesRead > 0)
                {
                    DataReceivedHandler(client, client.buffer, bytesRead);
                    lock (client.mLockObject)
                    {
                        if (socket == null)
                        {
                            return;
                        }
                    }
                    //  continue receiving
                    socket.BeginRead(client.buffer, 0, client.buffer.Length,
                        new AsyncCallback(ReceiveCallback), client);
                }
            }
            catch (Exception e)
            {
                if (Logger != null)
                {
                    Logger.Error("Exception ", e);
                }
                this.ErrorReceivedHandler(this, e);
                socket.Close();
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        public override void Close()
        {
            CheckDisposed();
            CloseParsers();
            lock (mLockObject)
            {
                if (pipeClient == null)
                {
                    return;
                }
                pipeClient.Close();
                pipeClient = null;
            }
        }

        /// <exception cref="System.ObjectDisposedException">Thrown when method is called on a disposed object</exception>
        /// <exception cref="System.InvalidOperationException">Thrown when port is closed</exception>
        public override void Write(byte[] buffer, int offset, int count)
        {
            CheckDisposed();
            CheckClosed();
            pipeClient.Write(buffer, offset, count);
            GenerateOnDataSent(ByteArray.Copy(buffer, offset, count), DateTime.Now);
        }

        public override bool IsOpen()
        {
            CheckDisposed();
            lock (mLockObject)
            {
                return pipeClient != null;
            }
        }
        
        private bool _disposed = false;

        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // Clear all property values that maybe have been set
                    // when the class was instantiated
                    if (pipeClient != null)
                    {
                        pipeClient.Dispose();
                        pipeClient = null;
                    }
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
            base.Dispose(disposing);
        }

        protected override void CheckDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            base.CheckDisposed();
        }
    }
}
