﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Net;
using log4net;
using System.IO;
using System.Threading;
using InTheHand.Net;
using System.Linq;

namespace yats.Ports
{
    //public class PortOpenEventArgs : EventArgs
    //{
    //    public 
    //}

    public static class PortReusePool
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(PortReusePool));

        //public static StreamWriter CancelLog = File.AppendText("PortReusePool.log");

        private static List<OpenedPortInfo> s_openedPorts = new List<OpenedPortInfo>();

        public static SerialPort OpenSerial(string deviceName, int baudRate, int dataBits, Parity parity, StopBits stopBits, RtsEnableEnum rtsEnable, bool dtrEnable, Handshake handshake, bool discardDataWithoutParsers)
        {
            SerialPortInfo settings = new SerialPortInfo(discardDataWithoutParsers, deviceName, baudRate, dataBits, parity, stopBits, rtsEnable, dtrEnable, handshake);
            return OpenSerial(settings);
        }

        public static SerialPort OpenSerial(SerialPortInfo settings)
        {
            lock (s_openedPorts)
            {
                //s_openedPorts.RemoveAll(x => x.Port.IsOpen() == false);
                foreach (var opened in s_openedPorts)
                {
                    if (opened.SerialPort == null)
                    {
                        continue;
                    }

                    if (opened.SerialPort.PortName == settings.PortName)
                    {
                        opened.Counter++;
                        // Cancel close timer if running - the same port is opened again
                        if (opened.Timer != null)
                        {
                            opened.Timer.Close();
                            opened.Timer = null;
                        }
                        SerialPort foundSerial = (opened.Port as SerialPort);
                        ApplySettings(foundSerial, settings);
                        return foundSerial;
                    }
                }

                SerialPort newSerial = new SerialPort();
                ApplySettings(newSerial, settings);
                newSerial.Open();

                var openedPortInfo = new OpenedPortInfo
                {
                    Counter = 1,
                    SerialPort = settings,
                    Port = newSerial
                };

                s_openedPorts.Add(openedPortInfo);
                return newSerial;
            }
        }

        public static TcpIpClientSocket OpenTcpIpClient(string address, int portNumber, bool discardDataWithoutParsers, bool keepAlive)
        {
            TcpIpClientInfo settings = new TcpIpClientInfo(address, portNumber, discardDataWithoutParsers, keepAlive);
            lock (s_openedPorts)
            {
                //s_openedPorts.RemoveAll(x => x.Port.IsOpen() == false);
                foreach (var opened in s_openedPorts)
                {
                    if (opened.TcpClient == null || opened.Port.IsOpen() == false)
                    {
                        continue;
                    }

                    if (opened.TcpClient.Address == settings.Address && opened.TcpClient.PortNumber == settings.PortNumber)
                    {
                        opened.Counter++;
                        // Cancel close timer if running - the same port is opened again
                        if (opened.Timer != null)
                        {
                            opened.Timer.Close();
                            opened.Timer = null;
                        }
                        TcpIpClientSocket foundClient = (opened.Port as TcpIpClientSocket);
                        foundClient.DiscardDataWithoutParsers = discardDataWithoutParsers;
                        return foundClient;
                    }
                }
            }
            TcpIpClientSocket newClient = new TcpIpClientSocket(settings.Address, settings.PortNumber)
            {
                DiscardDataWithoutParsers = discardDataWithoutParsers
            };
            newClient.Open();
            var openedPortInfo = new OpenedPortInfo
            {
                Counter = 1,
                TcpClient = settings,
                Port = newClient
            };
            lock (s_openedPorts)
            {
                s_openedPorts.Add(openedPortInfo);
            }
            return newClient;
        }

        /// <exception cref="System.InvalidOperationException">Thrown when 'replaceSocket' value does not match existing value</exception>
        public static TcpServer OpenTcpIpServer(int portNumber, int? clientConnectTimeoutMs, bool replaceSocket, WaitHandle cancelWaitEvent, bool discardDataWithoutParsers, bool keepAlive)
        {
            TcpServer tcpServer = null;
            OpenedPortInfo openedPort = null;
            lock (s_openedPorts)
            {
                foreach (var opened in s_openedPorts)
                {
                    if (opened.TcpServer == null)
                    {
                        continue;
                    }

                    if (opened.TcpServer.PortNumber == portNumber)
                    {
                        if (opened.TcpServer.ReplaceSocket != replaceSocket)
                        {
                            throw new InvalidOperationException("Can not change ReplaceSocket value after opening socket");
                        }
                        opened.Counter++;
                        // Cancel close timer if running - the same port is opened again
                        if (opened.Timer != null)
                        {
                            opened.Timer.Close();
                            opened.Timer = null;
                        }

                        tcpServer = opened.Port as TcpServer;
                        tcpServer.KeepAlive = keepAlive;
                        openedPort = opened;
                        break;
                    }
                }

                if (tcpServer == null)
                {
                    TcpIpServerInfo info = new TcpIpServerInfo(portNumber, replaceSocket, discardDataWithoutParsers, keepAlive);
                    openedPort = new OpenedPortInfo
                    {
                        Counter = 1,
                        TcpServer = info
                    };
                    tcpServer = new TcpServer(portNumber, replaceSocket)
                    {
                        KeepAlive = keepAlive
                    };
                    openedPort.Port = tcpServer;
                    s_openedPorts.Add(openedPort);
                }

                tcpServer.DiscardDataWithoutParsers = discardDataWithoutParsers;
                tcpServer.StartListening();
            }
            // Wait until a connection is made and processed before continuing.
            //CancelLog.WriteLine(string.Format("{0:G} Start opening server port", DateTime.Now));
            //CancelLog.Flush();
            WaitHandle[] waitHandles;
            if (cancelWaitEvent != null)
            {
                waitHandles = new WaitHandle[] { tcpServer.WaitEvent, cancelWaitEvent };
            }
            else
            {
                waitHandles = new WaitHandle[] { tcpServer.WaitEvent };
            }

            int timeout = clientConnectTimeoutMs ?? -1;

            int res = WaitHandle.WaitAny(waitHandles, timeout);
            if (res == 0 && tcpServer.IsOpen())
            {
                return tcpServer;
            }

            return null;
        }


        public static SshPort OpenSshClient(string host, string user, string password, bool discardDataWithoutParsers)
        {
            SshClientInfo settings = new SshClientInfo(host, user, password, discardDataWithoutParsers);
            lock (s_openedPorts)
            {
                //s_openedPorts.RemoveAll(x => x.Port.IsOpen() == false);
                foreach (var opened in s_openedPorts)
                {
                    if (opened.SshPort == null || opened.Port.IsOpen() == false)
                    {
                        continue;
                    }

                    if (opened.SshPort.Equals(settings))
                    {
                        opened.Counter++;
                        // Cancel close timer if running - the same port is opened again
                        if (opened.Timer != null)
                        {
                            opened.Timer.Close();
                            opened.Timer = null;
                        }
                        opened.Port.DiscardDataWithoutParsers = discardDataWithoutParsers;
                        return opened.Port as SshPort;
                    }
                }

                SshPort newClient = new SshPort(settings);
                newClient.Open();

                var openedPortInfo = new OpenedPortInfo
                {
                    Counter = 1,
                    SshPort = settings,
                    Port = newClient
                };

                s_openedPorts.Add(openedPortInfo);
                return newClient;
            }
        }

        public static AbstractPort OpenUpdSocket(IPAddress remoteIp, int localPort, int remotePort, bool overwriteIP, bool broadcast, bool discardDataWithoutParsers)
        {
            UdpSocketInfo settings = new UdpSocketInfo(remoteIp, localPort, remotePort, overwriteIP, discardDataWithoutParsers);
            lock (s_openedPorts)
            {
                //s_openedPorts.RemoveAll(x => x.Port.IsOpen() == false);
                foreach (var opened in s_openedPorts)
                {
                    if (opened.UdpSocket == null || opened.Port.IsOpen() == false)
                    {
                        continue;
                    }

                    if (opened.UdpSocket.Equals(settings))
                    {
                        opened.Counter++;
                        // Cancel close timer if running - the same port is opened again
                        if (opened.Timer != null)
                        {
                            opened.Timer.Close();
                            opened.Timer = null;
                        }
                        opened.Port.DiscardDataWithoutParsers = discardDataWithoutParsers;
                        return opened.Port;
                    }
                }

                UdpSocket newClient;
                if (overwriteIP)
                {
                    newClient = new UdpSocketAutoRemoteIp(localPort);
                }
                else if (broadcast)
                {
                    newClient = new UdpSocketBroadcast(localPort, remotePort);
                }
                else
                {
                    newClient = new UdpSocketKnownRemoteIp(remoteIp, localPort, remotePort);
                }

                newClient.DiscardDataWithoutParsers = discardDataWithoutParsers;
                newClient.Open();

                var openedPortInfo = new OpenedPortInfo
                {
                    Counter = 1,
                    UdpSocket = settings,
                    Port = newClient
                };

                s_openedPorts.Add(openedPortInfo);
                return newClient;
            }
        }

        public static BluetoothPort OpenBluetoothPort(BluetoothAddress bluetoothAddress, string pin, bool discardDataWithoutParsers)
        {
            BluetoothPortInfo settings = new BluetoothPortInfo(bluetoothAddress, pin, discardDataWithoutParsers);
            lock (s_openedPorts)
            {
                //s_openedPorts.RemoveAll(x => x.Port.IsOpen() == false);
                foreach (var opened in s_openedPorts)
                {
                    if (opened.BluetoothPort == null || opened.Port.IsOpen() == false)
                    {
                        continue;
                    }

                    if (opened.BluetoothPort.Address.Equals(settings.Address))
                    {
                        opened.Counter++;
                        // Cancel close timer if running - the same port is opened again
                        if (opened.Timer != null)
                        {
                            opened.Timer.Close();
                            opened.Timer = null;
                        }
                        BluetoothPort foundClient = (opened.Port as BluetoothPort);
                        return foundClient;
                    }
                }

                BluetoothPort newPort = new BluetoothPort(settings.Address, settings.PIN);
                newPort.Open();

                var openedPortInfo = new OpenedPortInfo
                {
                    Counter = 1,
                    BluetoothPort = settings,
                    Port = newPort
                };

                s_openedPorts.Add(openedPortInfo);
                return newPort;
            }
        }

        public static void CancelOpen(int portNumber)
        {
            lock (s_openedPorts)
            {
                foreach (var opened in s_openedPorts)
                {
                    if (!(opened.Port is TcpServer server))
                    {
                        continue;
                    }

                    if (server.PortNumber == portNumber)
                    {
                        //CancelLog.AutoFlush = true;
                        //CancelLog.WriteLine(string.Format("{0:G} CancelOpen()", DateTime.Now));
                        server.WaitEvent.Set();
                        if (!server.IsOpen())
                        {
                            opened.Counter--;
                        }
                    }
                }
            }
        }

        public static bool Close(AbstractPort port)
        {
            if (port == null)
            {
                return false;
            }

            lock (s_openedPorts)
            {
                OpenedPortInfo found = null;
                foreach (var opened in s_openedPorts)
                {
                    //refactor, should use a single mechanism for any port type. Implement Equals?
                    if (opened.SerialPort != null && port is SerialPort && opened.SerialPort.PortName == (port as SerialPort).Name)
                    {
                        found = opened;
                        break;
                    }

                    if (opened.TcpClient != null && port is TcpIpClientSocket && opened.TcpClient.Address == (port as TcpIpClientSocket).Address && opened.TcpClient.PortNumber == (port as TcpIpClientSocket).PortNumber)
                    {
                        found = opened;
                        break;
                    }

                    if (opened.SshPort != null && port is SshPort && opened.SshPort.Host == (port as SshPort).Host && opened.SshPort.User == (port as SshPort).Name)
                    {
                        found = opened;
                        break;
                    }

                    if (opened.TcpServer != null && port is TcpIpClientSocket && opened.TcpServer.PortNumber == (port as TcpIpClientSocket).PortNumber)
                    {
                        found = opened;
                        break;
                    }

                    if (opened.UdpSocket != null && port is UdpSocket && opened.UdpSocket.LocalPort == (port as UdpSocket).LocalPort)
                    {
                        found = opened;
                        break;
                    }

                    if (opened.BluetoothPort != null && port is BluetoothPort && opened.BluetoothPort.Address.Equals((port as BluetoothPort).BluetoothAddress))
                    {
                        found = opened;
                        break;
                    }
                }

                StartCloseTimer(found);
                return found != null;
            }
        }

        private static void StartCloseTimer(OpenedPortInfo portInfo)
        {
            if (portInfo == null)
            {
                return;
            }
            portInfo.Counter--;
            //if counter reaches 0, no test cases have the port open. Start a port close timer
            if (portInfo.Counter <= 0)
            {
                portInfo.Timer = new System.Timers.Timer(portInfo.TimeoutMs)
                {
                    AutoReset = false
                };
                portInfo.Timer.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Elapsed);
                portInfo.Timer.Start();
            }
        }
        /*
        public static void CloseSerial(string portName)
        {
            if (portName == null)
            {
                return;
            }

            lock (s_openedPorts)
            {
                OpenedPortInfo found = null;
                foreach (var opened in s_openedPorts)
                {
                    if (opened.SerialPort != null && opened.SerialPort.PortName == portName)
                    {
                        found = opened;
                        break;
                    }
                }

                StartCloseTimer(found);
            }
        }

        public static void CloseTcpIpClient(string address, int portNumber)
        {
            lock (s_openedPorts)
            {
                OpenedPortInfo found = null;
                foreach (var opened in s_openedPorts)
                {
                    if (opened.TcpClient != null && opened.TcpClient.Address == address && opened.TcpClient.PortNumber == portNumber)
                    {
                        found = opened;
                        break;
                    }
                }
                StartCloseTimer(found);
            }
        }
        */
        public static void CloseTcpIpServer(int portNumber)
        {
            lock (s_openedPorts)
            {
                OpenedPortInfo found = s_openedPorts.FirstOrDefault(x => x.TcpServer != null && x.TcpServer.PortNumber == portNumber);
                StartCloseTimer(found);
            }
        }

        static void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            lock (s_openedPorts)
            {
                foreach (var opened in s_openedPorts)
                {
                    if (sender == opened.Timer && opened.Counter <= 0)
                    {
                        opened.Port.Dispose();
                        //Console.WriteLine( string.Format( "{0} closed", opened.Port.Name ) );
                        s_openedPorts.Remove(opened);
                        return;
                    }
                }
            }
        }

        private class OpenedPortInfo
        {
            public SerialPortInfo SerialPort;
            public TcpIpClientInfo TcpClient;
            public TcpIpServerInfo TcpServer;
            public SshClientInfo SshPort;
            public UdpSocketInfo UdpSocket;
            public BluetoothPortInfo BluetoothPort;

            public AbstractPort Port;

            public int Counter;

            // port is actually closed when the timer expires. The timer is started when Counter reaches 0
            public System.Timers.Timer Timer;

            // close timeout. Takes static default but can be overriden
            public double TimeoutMs = Properties.Settings.Default.PortReusePoolDelay;
        }

        private static void ApplySettings(SerialPort port, SerialPortInfo settings)
        {
            if (port.IsOpen() == false && port.Name != settings.PortName)
            {
                port.Name = settings.PortName;
            }
            if (port.BaudRate != settings.BaudRate)
            {
                port.BaudRate = settings.BaudRate;
            }
            if (port.DataBits != settings.DataBits)
            {
                port.DataBits = settings.DataBits;
            }
            if (port.DtrEnable != settings.DtrEnable)
            {
                port.DtrEnable = settings.DtrEnable;
            }
            if (port.Handshake != settings.Handshake)
            {
                port.Handshake = settings.Handshake;
            }
            //port.NewLine = settings.NewLine;
            if (port.Parity != settings.Parity)
            {
                port.Parity = settings.Parity;
            }
            //port.ReadBufferSize = settings.ReadBufferSize;
            //port.ReadTimeout = settings.ReadTimeout;
            switch (settings.RtsEnable)
            {
                case RtsEnableEnum.OFF:
                    if (port.RtsEnable != false)
                    {
                        port.RtsEnable = false;
                    }
                    break;
                case RtsEnableEnum.ON:
                    if (port.RtsEnable != true)
                    {
                        port.RtsEnable = true;
                    }
                    break;
            }
            if (port.StopBits != settings.StopBits)
            {
                port.StopBits = settings.StopBits;
            }
            port.DiscardDataWithoutParsers = settings.DiscardDataWithoutParsers;
            //port.WriteBufferSize = settings.WriteBufferSize;
        }

        public static void Flush()
        {
            lock (s_openedPorts)
            {
                List<OpenedPortInfo> toRemove = new List<OpenedPortInfo>();
                foreach (var opened in s_openedPorts)
                {
                    if (opened.Counter <= 0)
                    {
                        opened.Timer.Close();
                        opened.Port.Dispose();
                        //Console.WriteLine(string.Format("{0} closed", opened.Port.Name));
                        toRemove.Add(opened);
                    }
                }
                foreach (var rem in toRemove)
                {
                    s_openedPorts.Remove(rem);
                }
            }
        }

        // Override default close timeout for opened port
        public static void SetCloseTimeout(AbstractPort port, double timeoutMs)
        {
            lock (s_openedPorts)
            {
                foreach (var opened in s_openedPorts)
                {
                    if (object.ReferenceEquals(opened.Port, port))
                    {
                        opened.TimeoutMs = timeoutMs;
                    }
                }
            }
        }

        // Override default close timeout for opened port
        public static void SetCloseTimeout(AbstractPort port, TimeSpan timeout)
        {
            SetCloseTimeout(port, timeout.TotalMilliseconds);
        }

        public static void CloseImmediately(AbstractPort port)
        {
            lock (s_openedPorts)
            {
                foreach (var opened in s_openedPorts)
                {
                    if (object.ReferenceEquals(opened.Port, port))
                    {
                        opened.Counter--;
                        if (opened.Counter <= 0)
                        {
                            if (opened.Timer != null)
                            {
                                opened.Timer.Close();
                            }
                            opened.Port.Dispose();
                            //Console.WriteLine(string.Format("{0} closed", opened.Port.Name));
                            s_openedPorts.Remove(opened);
                            return; // must return here, otherwise CollectionModified would be thrown
                        }
                    }
                }
            }
        }
    }
}
