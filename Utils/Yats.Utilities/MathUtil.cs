﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace yats.Utilities
{
    public static class MathUtil
    {
        public static T Limit<T>(this T value, T minValue, T maxValue) where T : IComparable
        {
            if(value.CompareTo( minValue ) == -1)
                return minValue;
            if(value.CompareTo( maxValue ) == 1)
                return maxValue;
            return value;
        }

        public static double StandardDeviation(this IEnumerable<double> values)
        {
            double avg = values.Average();
            return Math.Sqrt(values.Average(v => Math.Pow(v - avg, 2)));
        }

        public static float StandardDeviation(this IEnumerable<float> values)
        {
            float avg = values.Average();
            return (float) Math.Sqrt(values.Average(v => Math.Pow(v - avg, 2)));
        }

        public static float Mean(this IEnumerable<float> values)
        {
            return values.Sum() / values.Count();
        }
        
        public static int Constrain(int value, int minInclusive, int maxInclusive)
        {
            if (value < minInclusive)
            {
                return minInclusive;
            }
            if (value > maxInclusive)
            {
                return maxInclusive;
            }
            return value;
        }
    }
}
