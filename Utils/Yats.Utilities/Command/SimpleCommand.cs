﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace yats.Utilities.Commands
{
    /// <summary>
    /// Execute / Undo actions can be passed as anonymous functions instead of creating a ICommand class
    /// </summary>
    public class SimpleCommand : ICommand
    {
        Func<bool> execute;
        Func<bool> undo;

        public SimpleCommand(Func<bool> execute, Func<bool> undo)
        {
            this.execute = execute;
            this.undo = undo;
        }

        public virtual bool Execute()
        {
            if (execute != null)
            {
                return execute();
            }
            return false;
        }

        public virtual bool Undo()
        {
            if (undo != null)
            {
                return undo();
            }
            return false;
        }
    }
}
