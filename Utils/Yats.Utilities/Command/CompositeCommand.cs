﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using yats.Utilities;

namespace yats.Utilities.Commands
{
    public class CompositeCommand<T> : ICommand where T:ICommand
    {
        protected List<T> mSteps = new List<T>();
        protected Action<bool> mExecuteAction;
        protected bool mCleanCommands = false;
        
        /// <summary>
        /// If set, subcommands that return false will be removed from subcommand list when Execute runs. Useful to filter out actions that know they don't actually modify anything
        /// </summary>
        public bool CleanFailingCommands
        {
            get { return mCleanCommands; }
            set { mCleanCommands = value; }
        }

        public CompositeCommand(IEnumerable<T> steps)
        {
            mSteps.AddRange(steps);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="onExecuteAction">Action to perform after Executing (parameter value = true) or Undo (false)</param>
        /// <param name="steps">Add steps to composite</param>
        public CompositeCommand(Action<bool> onExecuteAction, IEnumerable<T> steps)
            : this(steps)
        {
            mExecuteAction = onExecuteAction;
        }

        public CompositeCommand(params T[] steps)
        {
            mSteps.AddRange(steps);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="onExecuteAction">Action to perform after Executing (parameter value = true) or Undo (false)</param>
        /// <param name="steps">Add steps to composite</param>
        public CompositeCommand(Action<bool> onTestModify, params T[] steps)
            : this(steps)
        {
            mExecuteAction = onTestModify;
        }

        public virtual bool Add(params T[] steps)
        {
            bool result = false;
            foreach (var step in steps)
            {
                if (step == null)
                {
                    continue;
                }
                mSteps.Add(step);
                result = true;
            }
            return result;
        }

        public ReadOnlyCollection<T> GetSteps()
        {
            return mSteps.AsReadOnly();
        }

        public virtual bool Execute()
        {
            bool result = false;
            List<T> toRemove = new List<T>();
            
            for (int i = 0; i < mSteps.Count; i++)
            {
                if (mSteps[i] == null)
                {
                    continue; // should not happen
                }
                bool res = mSteps[i].Execute();
                if (!res)
                {
                    toRemove.Add(mSteps[i]);
                }
                result |= res;
            }

            if (mCleanCommands)
            {
                mSteps.RemoveRange(toRemove);
            }
            
            if (result && mExecuteAction != null)
            {
                mExecuteAction(true);
            }
            return result;
        }

        /// <summary>
        /// Subcommand Undo is performed in reverse order
        /// </summary>
        /// <returns>True if any subcommand returned true</returns>
        public virtual bool Undo()
        {
            bool result = false;
            for (int i = mSteps.Count - 1; i >= 0; i--)
            {
                result |= mSteps[i].Undo();
            }
            if (result && mExecuteAction != null)
            {
                mExecuteAction(false);
            }
            return result;
        }
    }

    [DebuggerDisplay("{mSteps.Count} inner commands")]
    public class CompositeCommand : CompositeCommand<ICommand> {
        public CompositeCommand(IEnumerable<ICommand> steps):base(steps)
        {
        }

        public CompositeCommand(Action<bool> onTestModify, IEnumerable<ICommand> steps)
            : base(onTestModify, steps)
        {
        }

        public CompositeCommand(params ICommand[] steps):base(steps)
        {
        }

        public CompositeCommand(Action<bool> onTestModify, params ICommand[] steps):base(onTestModify, steps)
        {
        }
    }
}
