﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
namespace yats.Utilities.Commands
{
    public interface ICommandStack
    {
        bool CanRedo { get; }
        bool CanUndo { get; }
        /// <summary>
        /// Execute command. Add to Undo list if the command is not null and returns true on execute
        /// </summary>
        /// <returns>True if executed and added</returns>
        bool Execute(ICommand command);
        /// <summary>
        /// Get command that was last executed. Can be used to roll back multiple commands by passing the returned value back to UndoUntil
        /// </summary>
        /// <returns>Command object or null if undo stack is empty</returns>
        ICommand GetLastExecutedCommand(); 
        bool Redo();
        bool Undo();
        /// <summary>
        /// Undo all commands until but not including the given command. 
        /// </summary>
        /// <param name="lastExecutedCommand">Command to undo until. If null, all commands will be undone</param>
        /// <param name="leaveRedoCommands">Clear redo stack if false</param>
        /// <returns>True if any command was undone</returns>
        bool UndoUntil(ICommand lastExecutedCommand, bool leaveRedoCommands);
        /// <summary>
        /// Returns list of commands in the Undo stack up to current state. Redo commands are not included. Does not modify internal state.
        /// </summary>
        List<ICommand> GetUndoCommands();
        /// <summary>
        /// Clear undo/redo. Generates OnModifiedChanged event with IsModified=false
        /// </summary>
        void Clear();
        void SetSavedStatus();
        event EventHandler OnStatusChange;
        event EventHandler<ModifiedStatusChangedEventArgs> OnModifiedChange;
        bool Modified { get; }
    }
    
    public interface ICommandStack<T> where T : ICommand
    {
        bool Execute(T command);
        List<T> GetUndoCommands();
        T GetLastExecutedCommand();
        bool UndoUntil(T lastExecutedCommand, bool leaveRedoCommands);
    }

    public class ModifiedStatusChangedEventArgs : EventArgs
    {
        public bool IsModified;
    }
}
