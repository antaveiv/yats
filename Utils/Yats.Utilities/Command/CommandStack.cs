﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace yats.Utilities.Commands
{
    /// <summary>
    /// Execute commands and stores them in a undo/redo stack.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CommandStack<T> : ICommandStack<T> where T:ICommand
    {
        private List<T> mCommands = new List<T>();
        private int mIndexOfExecutedCommand = -1;
        private int mIndexOfSavePoint = -1;
        /// <summary>
        /// Raised when CanUndo/CanRedo/Modified status changes - after save, clear stack, undo, redo
        /// </summary>
        public event EventHandler OnStatusChange;
        
        /// <summary>
        /// Raised when Modified status changes
        /// </summary>
        public event EventHandler<ModifiedStatusChangedEventArgs> OnModifiedChange;

        /// <summary>
        /// Execute given command
        /// </summary>
        /// <param name="command"></param>
        /// <returns>true if command is not null and command.Execute() returns true</returns>
        public virtual bool Execute(T command)
        {
            if (command == null)
            {
                return false;
            }
            // only register to Undo stack if the command indicates that some change was actually performed
            if (command.Execute())
            {
                // clear Redo stack
                if (mIndexOfExecutedCommand < mCommands.Count - 1)
                {
                    mCommands.RemoveRange(mIndexOfExecutedCommand + 1, mCommands.Count - mIndexOfExecutedCommand - 1);
                }
                mCommands.Add(command);
                mIndexOfExecutedCommand++;
                RaiseEvents();
                return true;
            }
            return false;
        }

        public virtual bool Undo()
        {
            if (CanUndo)
            {
                mCommands[mIndexOfExecutedCommand--].Undo();
                RaiseEvents();
                return true;
            }
            return false;
        }

        public virtual bool Redo()
        {
            if (CanRedo)
            {
                mCommands[++mIndexOfExecutedCommand].Execute();
                RaiseEvents();
                return true;
            }
            return false;
        }

        public virtual bool CanUndo
        {
            get
            {
                return mIndexOfExecutedCommand >= 0;
            }
        }

        public virtual bool CanRedo
        {
            get
            {
                return (mIndexOfExecutedCommand < mCommands.Count - 1);
            }
        }

        public virtual void Clear()
        {
            mCommands.Clear();
            mIndexOfExecutedCommand = -1;
            mIndexOfSavePoint = -1;
            RaiseEvents();
        }

        /// <summary>
        /// Clear Modified status to false. Raises OnModifiedChanged if status actually changes
        /// </summary>
        public virtual void SetSavedStatus()
        {
            if (mIndexOfSavePoint != mIndexOfExecutedCommand)
            {
                mIndexOfSavePoint = mIndexOfExecutedCommand;
                RaiseEvents();
            }
        }

        /// <summary>
        /// Indicates that commands were executed and not undone after CommandStack was created or SetSavedStatus called
        /// </summary>
        public bool Modified { get { return mIndexOfExecutedCommand != mIndexOfSavePoint; } }

        /// <summary>
        /// Returns list of commands in the Undo stack up to current state. Redo commands are not included. Does not modify internal state.
        /// </summary>
        public virtual List<T> GetUndoCommands()
        {
            if (mIndexOfExecutedCommand >= 0)
            {
                return mCommands.GetRange(0, mIndexOfExecutedCommand + 1);
            }
            return new List<T>();
        }

        public T GetLastExecutedCommand()
        {
            if (mIndexOfExecutedCommand >= 0)
            {
                return mCommands[mIndexOfExecutedCommand];
            } 
            return default(T);
        }

        public bool UndoUntil(T lastExecutedCommand, bool leaveRedoCommands)
        {
            int targetIndex = -1;
            if (lastExecutedCommand != null)
            {
                targetIndex = mCommands.LastIndexOf(lastExecutedCommand);
                if (targetIndex == -1)
                {
                    throw new ArgumentException("lastExecutedCommand was not found");
                }
            }
            bool result = false;
            while (mIndexOfExecutedCommand > targetIndex)
            {
                mCommands[mIndexOfExecutedCommand--].Undo();
                result = true;
            }

            if (leaveRedoCommands == false)
            {
                // clear Redo stack
                if (mIndexOfExecutedCommand < mCommands.Count - 1)
                {
                    mCommands.RemoveRange(mIndexOfExecutedCommand + 1, mCommands.Count - mIndexOfExecutedCommand - 1);
                }
            }

            if (result)
            {
                RaiseEvents();
            }
            return result;
        }

        protected void RaiseEvents()
        {
            if (OnStatusChange != null)
            {
                OnStatusChange(this, null);
            }
            if (OnModifiedChange != null)
            {
                OnModifiedChange(this, new ModifiedStatusChangedEventArgs() { IsModified = Modified });
            }
        }

        // "Flatten" command hierarchy by expanding composite commands etc
        public static List<ICommand> FlattenCommands(params ICommand[] commands)
        {
            List<ICommand> result = new List<ICommand>();
            foreach (var command in commands)
            {
                if (command == null)
                {
                    continue;
                }
                if (command is CompositeCommand)
                {
                    result.AddRange(FlattenCommands((command as CompositeCommand).GetSteps().ToArray()));
                }
                else if (command is AlreadyExecutedCommand)
                {
                    result.AddRange(FlattenCommands((command as AlreadyExecutedCommand).GetCommand()));
                }
                else
                {
                    result.Add(command);
                }
            }
            return result;
        }
    }

    public class CommandStack : CommandStack<ICommand>, ICommandStack
    {        
    }
}
