﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace yats.Utilities.Commands
{
    // A Command wrapper to be wrapped around an already executed command. I.e. it will do nothing the first time Execute is called. Useful in various GUI applications when the action needs to be done immediately and packed into a Composite for undoing
    public class AlreadyExecutedCommand<T> : ICommand  where T:ICommand
    {
        private T mCommand;
        bool skip = true;

        public AlreadyExecutedCommand(T command)
        {
            mCommand = command;
        }

        public T GetCommand()
        {
            return mCommand;
        }

        public bool Execute()
        {
            if (skip)
            {
                skip = false;
                return true;
            }
            if (mCommand == null)
            {
                return false;
            }
            return mCommand.Execute();
        }

        public bool Undo()
        {
            skip = false;
            return mCommand.Undo();
        }
    }

    public class AlreadyExecutedCommand : AlreadyExecutedCommand<ICommand> {
        public AlreadyExecutedCommand(ICommand command) :base(command)
        {
        }
    }
}
