﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Text;

namespace yats.Utilities
{
    public static class TimeSpanExtensions
    {
        public enum TimeSpanField : byte
        {
            Day = 0,
            Hour = 1,
            Minute = 2,
            Second = 3,
            Millisecond = 4
        }

        public static string ToFriendlyDisplay(this TimeSpan value, int maxNrOfElements, TimeSpanField smallestField)
        {
            TimeSpan tmp = value;
            bool minus = false;
            if(value.TotalMilliseconds < 0)
            {
                tmp = TimeSpan.FromMilliseconds( value.TotalMilliseconds * -1 );
                minus = true;
            }

            int numElements = 0;

            if(tmp.TotalMilliseconds == 0)
            {
                return "0 ms";
            }

            string result = string.Empty;

            if(numElements < maxNrOfElements && tmp.Days > 0 && smallestField >= TimeSpanField.Day)
            {
                result = string.Format( "{0}, {1} days", result, tmp.Days );
                numElements++;
            }

            if(numElements < maxNrOfElements && tmp.Hours > 0 && smallestField >= TimeSpanField.Hour)
            {
                result = string.Format( "{0}, {1} hours", result, tmp.Hours );
                numElements++;
            }

            if(numElements < maxNrOfElements && tmp.Minutes > 0 && smallestField >= TimeSpanField.Minute)
            {
                result = string.Format( "{0}, {1} minutes", result, tmp.Minutes );
                numElements++;
            }

            if(numElements < maxNrOfElements && tmp.Seconds > 0 && smallestField >= TimeSpanField.Second)
            {
                result = string.Format( "{0}, {1} seconds", result, tmp.Seconds );
                numElements++;
            }

            if(numElements < maxNrOfElements && tmp.Milliseconds > 0 && smallestField >= TimeSpanField.Millisecond)
            {
                result = string.Format( "{0}, {1} ms", result, tmp.Milliseconds );
                numElements++;
            }

            if(string.IsNullOrEmpty( result ) == false)
            {
                result = result.Substring( 2 );
                if(minus)
                {
                    return "minus " + result;
                }
            }
            return result;
        }

        public static string ToShortFriendlyDisplay(this TimeSpan value)
        {
            return value.ToShortFriendlyDisplay(2, TimeSpanField.Millisecond);
        }

        public static string ToShortFriendlyDisplay(this TimeSpan value, int maxNrOfElements, TimeSpanField smallestField)
        {
            TimeSpan tmp = value;
            bool minus = false;
            if (value.TotalMilliseconds < 0)
            {
                tmp = TimeSpan.FromMilliseconds(value.TotalMilliseconds * -1);
                minus = true;
            }

            int numElements = 0;

            if (tmp.TotalMilliseconds == 0)
            {
                return "0ms";
            }

            string result = string.Empty;

            if (numElements < maxNrOfElements && tmp.Days > 0 && smallestField >= TimeSpanField.Day)
            {
                result = string.Format("{0} {1}d", result, tmp.Days);
                numElements++;
            }

            if (numElements < maxNrOfElements && tmp.Hours > 0 && smallestField >= TimeSpanField.Hour)
            {
                result = string.Format("{0} {1}h", result, tmp.Hours);
                numElements++;
            }

            if (numElements < maxNrOfElements && tmp.Minutes > 0 && smallestField >= TimeSpanField.Minute)
            {
                result = string.Format("{0} {1}m", result, tmp.Minutes);
                numElements++;
            }

            if (numElements < maxNrOfElements && tmp.Seconds > 0 && smallestField >= TimeSpanField.Second)
            {
                result = string.Format("{0} {1}s", result, tmp.Seconds);
                numElements++;
            }

            if (numElements < maxNrOfElements && tmp.Milliseconds > 0 && smallestField >= TimeSpanField.Millisecond)
            {
                result = string.Format("{0} {1}ms", result, tmp.Milliseconds);
                numElements++;
            }

            result = result.Trim();

            if (string.IsNullOrEmpty(result) == false)
            {
                if (minus)
                {
                    return "- " + result;
                }
            }
            return result;
        }

        /// <summary>
        /// Provides a friendly display string, e.g. 3 days 40 minutes
        /// </summary>
        /// <param name="value"></param>
        /// <param name="maxNrOfElements"></param>
        /// <returns></returns>
        public static string ToFriendlyDisplay(this TimeSpan value, int maxNrOfElements)
        {
            return value.ToFriendlyDisplay( maxNrOfElements, TimeSpanField.Millisecond );
        }

        /// <summary>
        /// Converts to display string. Difference from ToFriendlyDisplay is that ToFriendlyDisplay tries to be precise, e.g. "1m 1ms", ToShortFriendlyDisplay2 would give "1m 0s" for easy comparison
        /// </summary>
        /// <param name="value"></param>
        /// <param name="maxNrOfElements"></param>
        /// <returns></returns>
        public static string ToShortFriendlyDisplay2(this TimeSpan value, int maxNrOfElements)
        {
            TimeSpan tmp;
            bool minus = false;
            if (value.TotalMilliseconds < 0)
            {
                tmp = TimeSpan.FromMilliseconds(Math.Round(value.TotalMilliseconds) * -1);
                minus = true;
            }
            else
            {
                tmp = TimeSpan.FromMilliseconds(Math.Round(value.TotalMilliseconds));
            }

            int numElements = 0;

            

            if (tmp.TotalMilliseconds == 0)
            {
                return "0ms";
            }

            string result = string.Empty;
            bool formatStarted = false;

            if (numElements < maxNrOfElements && tmp.Days > 0)
            {
                formatStarted = true;
                result = string.Format("{0} {1}d", result, tmp.Days);
                numElements++;
            }

            if (numElements < maxNrOfElements && (tmp.Hours > 0 || formatStarted))
            {
                formatStarted = true;
                result = string.Format("{0} {1}h", result, tmp.Hours);
                numElements++;
            }

            if (numElements < maxNrOfElements && (tmp.Minutes > 0 || formatStarted))
            {
                formatStarted = true;
                result = string.Format("{0} {1}m", result, tmp.Minutes);
                numElements++;
            }

            if (numElements < maxNrOfElements && (tmp.Seconds > 0 || formatStarted))
            {
                formatStarted = true;
                result = string.Format("{0} {1}s", result, tmp.Seconds);
                numElements++;
            }

            if (numElements < maxNrOfElements && (tmp.Milliseconds > 0 || formatStarted))
            {
                formatStarted = true;
                result = string.Format("{0} {1}ms", result, tmp.Milliseconds);
                numElements++;
            }

            result = result.Trim();

            if (string.IsNullOrEmpty(result) == false)
            {
                if (minus)
                {
                    return "- " + result;
                }
            }
            return result;
        }
    }
}

