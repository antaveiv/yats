﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text.RegularExpressions;

namespace yats.Utilities
{
    public static class FileUtil
    {
        public static string StripInvalidFileCharacters(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return fileName;
            }

            //TODO: test in Linux
            string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());

            foreach (char c in invalid)
            {
                fileName = fileName.Replace(c.ToString(), "");
            }

            return fileName;
        }

        public static bool FileNameContainsInvalidChars(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            //TODO: test in Linux
            string invalid = new string(Path.GetInvalidFileNameChars());

            foreach (char c in invalid)
            {
                if (fileName.Contains(c)){
                    return true;
                }
            }

            return false;
        }

        public static bool FilePathContainsInvalidChars(string fileName)
        {
            fileName = Path.GetFullPath(fileName);
            //TODO: test in Linux
            string invalid = new string(Path.GetInvalidPathChars());

            foreach (char c in invalid)
            {
                if (fileName.Contains(c)){
                    return true;
                }
            }

            return false;
        }

        public static bool IsValidFilename(string fileName)
        {
            string regexString = "[" + Regex.Escape(new string(Path.GetInvalidPathChars())) + "]";
            Regex containsABadCharacter = new Regex(regexString);

            if (containsABadCharacter.IsMatch(fileName))
            {
                return false;
            }

            // Check for drive
            if (Path.IsPathRooted(fileName) && (Directory.GetLogicalDrives().Contains(Path.GetPathRoot(fileName)) == false))
            {
                return false;
            }

            bool bOk = false;
            try
            {
                new System.IO.FileInfo(fileName);
                bOk = true;
            }
            catch (ArgumentException)
            {
            }
            catch (System.IO.PathTooLongException)
            {
            }
            catch (NotSupportedException)
            {
            }
            return bOk;
        }

        public static string GetExePath()
        {
            var entryAssembly = System.Reflection.Assembly.GetEntryAssembly();
            if (entryAssembly != null)
            {
                return Path.GetDirectoryName(entryAssembly.Location);
            }
            else
            {
                return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location); //workaround for running NUnit tests
            }
        }

        public static string GetExePath(string fileName)
        {
            return Path.Combine(GetExePath(), fileName);
        }

        public static string GetExeFullName()
        {
            return System.Reflection.Assembly.GetEntryAssembly().Location;
        }

        // when called from Test.exe, will return c:/.../My Documents/Test. Will also create directory if does not exist
        public static string GetAppDirInMyDocuments()
        {
            string name = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
            string path = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), name);
            Directory.CreateDirectory(path);
            return path;
        }
        public static string GetApplicationDataPath()
        {
            try
            {
                string name = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), name);
            }
            catch 
            { 
                // fails when running NUnit tests
            }
            return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        }

        public static string GetApplicationDataPath(string fileName)
        {
            return Path.Combine(GetApplicationDataPath(), fileName);
        }

        public static string ReplaceEnvironmentVariables(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return path;
            }

            //TODO test in Linux


            var vars = new Dictionary<string, string>
            {
                { "%AppData%", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) },
                //{ "%Temp%", Environment.GetFolderPath(Environment.SpecialFolder.) },
                { "%Desktop%", Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory) },
                { "%MyDocuments%", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) },
                { "%ProgramFiles%", Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) },
                { "%ThisAppData%", GetApplicationDataPath() },
                { "%ExePath%", GetExePath() }
            };

            foreach (var v in vars)
            {
                path = path.Replace(v.Key, v.Value);
            }

            path = Environment.ExpandEnvironmentVariables(path);
            return FixPathSeparator(path);
        }

        public static string FixPathSeparator(string path)
        {
            switch (System.Environment.OSVersion.Platform)
            {
                case PlatformID.MacOSX:
                case PlatformID.Unix:
                    return path.Replace('\\', '/');

                default:
                    return path.Replace('/', '\\');
            }
        }

        //delete file, do not throw exceptions if fails
        public static void Delete(string fileName)
        {
            try
            {
                File.Delete(fileName);
            }
            catch
            {
            }
        }

        /// <summary>
        /// if fileName is e.g. A.txt, renames it to A1.txt. Previous A1.txt is renamed to A2.txt up to maxBackups
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="maxBackups"></param>
        public static void BackupPush(string fileName, int maxBackups)
        {
            if (maxBackups < 1)
            {
                throw new Exception("maxBackups must be >= 1");
            }

            if (File.Exists(fileName) == false)
            {
                return;
            }

            string path = Path.GetDirectoryName(Path.GetFullPath(fileName));
            string extension = Path.GetExtension(fileName);

            for (int i = maxBackups; i > 1; i--)
            {
                string renameFrom = Path.Combine(path, Path.GetFileNameWithoutExtension(fileName) + (i - 1).ToString() + extension);
                string renameTo = Path.Combine(path, Path.GetFileNameWithoutExtension(fileName) + (i).ToString() + extension);
                try
                {
                    File.Move(renameFrom, renameTo);
                }
                catch { }
            }

            string _renameTo = Path.Combine(path, Path.GetFileNameWithoutExtension(fileName) + "1" + extension);
            try
            {
                File.Move(fileName, _renameTo);
            }
            catch { }
        }

        /// <summary>
        /// http://bloggingabout.net/blogs/jschreuder/archive/2006/07/06/12886.aspx
        /// </summary>
        /// <summary>
        /// Check if a file can be opened for writing
        /// </summary>
        /// <param name="filename">The name of file to check.</param>
        public static bool CanWriteFile(string fileName)
        {
            // If the file can be opened for exclusive access it means that the file
            // is no longer locked by another process.
            try
            {
                using (FileStream inputStream = File.Open(fileName, FileMode.Open,
                            FileAccess.Read,
                            FileShare.None))
                {
                    return true;
                }
            }
            catch (IOException)
            {
                return false;
            }
        }

        public static bool WaitUntilAvailable(string fileName, int timeoutMs)
        {
            int RetryDelayMs = 100;
            DateTime start = DateTime.Now;

            while (true)
            {
                if (CanWriteFile(fileName))
                {
                    return true;
                }

                // Calculate the elapsed time and stop if the maximum retry
                // period has been reached.
                TimeSpan timeElapsed = DateTime.Now - start;

                if (timeElapsed.TotalMilliseconds > timeoutMs)
                {
                    return false;
                }

                System.Threading.Thread.Sleep(RetryDelayMs);
                RetryDelayMs *= 2;
            }
        }

        // http://stackoverflow.com/questions/652037/how-do-i-check-if-a-filename-matches-a-wildcard-pattern
        public static bool FileNameMatchesWildcardPattern(string fileName, string pattern)
        {
            Regex regex = FindFilesPatternToRegex.Convert(pattern);
            return regex.IsMatch(fileName);
        }

		//faster but less thorough than FileNameMatchesWildcardPattern
        public static bool FileNameMatchesWildcardPattern2(string fileName, string pattern)
        {
            Regex mask = new Regex(
                '^' +
                pattern
                    .Replace(".", "[.]")
                    .Replace("*", ".*")
                    .Replace("?", ".")
                + '$',
                RegexOptions.IgnoreCase);
            return mask.IsMatch(fileName);
        }

        internal static class FindFilesPatternToRegex
        {
            private static Regex HasQuestionMarkRegEx = new Regex(@"\?", RegexOptions.Compiled);
            private static Regex IlegalCharactersRegex = new Regex("[" + @"\/:<>|" + "\"]", RegexOptions.Compiled);
            private static Regex CatchExtentionRegex = new Regex(@"^\s*.+\.([^\.]+)\s*$", RegexOptions.Compiled);
            private static string NonDotCharacters = @"[^.]*";
            public static Regex Convert(string pattern)
            {
                if (pattern == null)
                {
                    throw new ArgumentNullException();
                }
                pattern = pattern.Trim();
                if (pattern.Length == 0)
                {
                    throw new ArgumentException("Pattern is empty.");
                }
                if (IlegalCharactersRegex.IsMatch(pattern))
                {
                    throw new ArgumentException("Patterns contains ilegal characters.");
                }
                bool hasExtension = CatchExtentionRegex.IsMatch(pattern);
                bool matchExact = false;
                if (HasQuestionMarkRegEx.IsMatch(pattern))
                {
                    matchExact = true;
                }
                else if (hasExtension)
                {
                    matchExact = CatchExtentionRegex.Match(pattern).Groups[1].Length != 3;
                }
                string regexString = Regex.Escape(pattern);
                regexString = "^" + Regex.Replace(regexString, @"\\\*", ".*");
                regexString = Regex.Replace(regexString, @"\\\?", ".");
                if (!matchExact && hasExtension)
                {
                    regexString += NonDotCharacters;
                }
                regexString += "$";
                Regex regex = new Regex(regexString, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                return regex;
            }
        }

        public static bool FileNotEmpty(string fileName)
        {
            if (File.Exists(fileName) == false)
            {
                return false;
            }
            return new FileInfo(fileName).Length > 0;
        }

        // Opens given file in Visual Studio. Due to VS bug, the lineNumber works only when a new instance is started.
        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        public static void OpenInVisualStudio(string file, int lineNumber)
        {
            if (File.Exists(file) == false)
            {
                return;
            }

            string param = string.Format("/edit \"{0}\" /command \"Edit.GoTo {1}\"", file, lineNumber);
            Process.Start("devenv", param);
        }

        //http://stackoverflow.com/questions/5617320/given-full-path-check-if-path-is-subdirectory-of-some-other-path-or-otherwise
        /// <summary>
        /// Returns true if <paramref name="path"/> starts with the path <paramref name="baseDirPath"/>.
        /// The comparison is case-insensitive, handles / and \ slashes as folder separators and
        /// only matches if the base dir folder name is matched exactly ("c:\foobar\file.txt" is not a sub path of "c:\foo").
        /// </summary>
        public static bool IsSubPathOf(this string baseDirPath, string path)
        {
            string normalizedPath = Path.GetFullPath(path.Replace('/', '\\')
                .WithEnding("\\"));

            string normalizedBaseDirPath = Path.GetFullPath(baseDirPath.Replace('/', '\\')
                .WithEnding("\\"));
            
            if (PlatformHelper.IsLinux())
            {
                return normalizedPath.StartsWith(normalizedBaseDirPath, StringComparison.Ordinal);
            }
            return normalizedPath.StartsWith(normalizedBaseDirPath, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Returns <paramref name="str"/> with the minimal concatenation of <paramref name="ending"/> (starting from end) that
        /// results in satisfying .EndsWith(ending).
        /// </summary>
        /// <example>"hel".WithEnding("llo") returns "hello", which is the result of "hel" + "lo".</example>
        public static string WithEnding(this string str, string ending)
        {
            if (str == null)
                return ending;

            string result = str;

            // Right() is 1-indexed, so include these cases
            // * Append no characters
            // * Append up to N characters, where N is ending length
            for (int i = 0; i <= ending.Length; i++)
            {
                string tmp = result + ending.Right(i);
                if (tmp.EndsWith(ending))
                    return tmp;
            }

            return result;
        }

        /// <summary>Gets the rightmost <paramref name="length" /> characters from a string.</summary>
        /// <param name="value">The string to retrieve the substring from.</param>
        /// <param name="length">The number of characters to retrieve.</param>
        /// <returns>The substring.</returns>
        public static string Right(this string value, int length)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            if (length < 0)
            {
                throw new ArgumentOutOfRangeException("length", length, "Length is less than zero");
            }

            return (length < value.Length) ? value.Substring(value.Length - length) : value;
        }
    }
}
