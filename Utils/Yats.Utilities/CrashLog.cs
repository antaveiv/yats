/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;

namespace yats.Utilities
{
	public static class CrashLog
	{
        private static readonly object mLockObject = new object();
        private static string fileName = FileUtil.GetApplicationDataPath("crash.log");

        /// <summary>
        /// Returns log file name. The log is stored in application data directory by default
        /// </summary>
        public static string GetLogFileName()
        {
            return fileName;
        }

        /// <summary>
        /// Returns log file directory. The log is stored in application data directory by default
        /// </summary>
        public static string GetLogFileLocation()
        {
            return Path.GetDirectoryName(fileName);
        }

        /// <summary>
        /// Set the log file name.
        /// </summary>
        /// <param name="newPath">File name including path</param>
        public static void SetLogFileName(string newPath)
        {
            fileName = newPath;
        }

        /// <summary>
        /// A string with product name and version. It is written to the log
        /// </summary>
        public static string ProductName { get; set; }

		public static void Write (Exception ex)
		{
            Write(ex, null);
        }

        /// <summary>
        /// An event handler for logging Application.ThreadException. Usage:
        /// Application.ThreadException += CrashLog.WriteThreadException;
        /// </summary>
        public static void WriteThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Write(e.Exception);
        }

        /// <summary>
        /// An event handler for logging AppDomain.CurrentDomain.UnhandledException. Usage:
        /// AppDomain.CurrentDomain.UnhandledException += CrashLog.WriteUnhandledException;
        /// </summary>        
        public static void WriteUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.ExceptionObject is Exception)
            {
                Write(e.ExceptionObject as Exception);
            }
            else
            {
                Write(e.ExceptionObject.ToString());
            }
        }

        public static void Write(Exception ex, string message)
        {
            lock (mLockObject)
            {
                try
                {
                    string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                    string errorMessage = string.Format("\n\n-------------------------------\n{0}{1}{2:u}\n{3}\nException:\n{4}",
                        (string.IsNullOrEmpty(ProductName) ? "" : ProductName + "\n"),
                        (string.IsNullOrEmpty(message) ? "" : message + "\n"),
                        DateTime.Now,
                        userName,
                        ((ex != null) ? ex.ToString() : "")
                    );

                    if (ex != null)
                    {
                        var inner = ex.InnerException;
                        while (inner != null)
                        {
                            errorMessage += string.Format("\n\nInner exception:\n\n{0}", inner);
                            inner = inner.InnerException;
                        }
                    }

                    if (Debugger.IsAttached)
                    {
                        Debug.Write(errorMessage);
                        return;
                    }

                    Directory.CreateDirectory(Path.GetDirectoryName(fileName));

                    File.AppendAllText(
                        fileName,
                        errorMessage
                    );
                }
                catch { }
            }
        }

        /// <summary>
        /// Write a crash log message string
        /// </summary>
        /// <param name="message"></param>
        public static void Write(string message)
        {
            Write(null, message);
        }

        public static void TryUploadLog(string url)
        {
            new Thread(new ParameterizedThreadStart((object o)=>{
                try
                {
                    if (FileUtil.FileNotEmpty(fileName) == false)
                    {
                        return;
                    }
                    var request = WebRequest.Create(url);
                    request.Method = "POST";

                    using (var writer = new StreamWriter(request.GetRequestStream()))
                    {
                        string fileContents;
                        lock (mLockObject)
                        {
                            fileContents = File.ReadAllText(fileName);
                        }
                        if (fileContents.Length < 100)
                        {
                            return;
                        }
                        writer.Write(fileContents);
                    }

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        File.Delete(fileName);
                    }
                }
                catch (Exception)
                {
                }
            })).Start(url);
        }        
    }
}

