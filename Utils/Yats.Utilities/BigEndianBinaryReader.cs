﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace yats.Utilities
{
    public class BigEndianBinaryReader : BinaryReader
    {
        public BigEndianBinaryReader(System.IO.Stream stream) : base(stream) { }
        public override int ReadInt32()
        {
            var bytes = base.ReadBytes(4);
            Array.Reverse(bytes);
            return BitConverter.ToInt32(bytes, 0);
        }
        
        public override UInt32 ReadUInt32()
        {
            var bytes = base.ReadBytes(4);
            Array.Reverse(bytes);
            return BitConverter.ToUInt32(bytes, 0);
        }
        
        public override Int16 ReadInt16()
        {
            var bytes = base.ReadBytes(2);
            Array.Reverse(bytes);
            return BitConverter.ToInt16(bytes, 0);
        }

        public override UInt16 ReadUInt16()
        {
            var bytes = base.ReadBytes(2);
            Array.Reverse(bytes);
            return BitConverter.ToUInt16(bytes, 0);
        }
        
        public override Int64 ReadInt64()
        {
            var bytes = base.ReadBytes(8);
            Array.Reverse(bytes);
            return BitConverter.ToInt64(bytes, 0);
        }

        public override UInt64 ReadUInt64()
        {
            var bytes = base.ReadBytes(8);
            Array.Reverse(bytes);
            return BitConverter.ToUInt64(bytes, 0);
        }
    }
}