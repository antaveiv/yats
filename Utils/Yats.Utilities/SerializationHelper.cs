﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Reflection;
using yats.Utilities;

namespace yats.Utilities
{
    public class SerializationHelper
    {
        private static Dictionary<Type, XmlSerializer> s_cache = new Dictionary<Type, XmlSerializer>();
        private static HashSet<Type> s_badTypeCache = new HashSet<Type>();

        public static void SerializeToStream<T>(T toSerialize, Stream stream)
        {
            XmlSerializer serializer = yats.Utilities.SerializationHelper.GetSerializer(typeof(T));
            try
            {
                serializer.Serialize(stream, toSerialize);
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex);
            }
        }    

        public static string SerializeToString<T>(T toSerialize)
        {
            XmlSerializer serializer = yats.Utilities.SerializationHelper.GetSerializer(typeof(T));
            using (StringWriter writer = new StringWriter())
            {
                try
                {
                    serializer.Serialize(writer, toSerialize);
                    return writer.ToString();
                }
                catch (Exception ex)
                {
                    CrashLog.Write(ex);
                }
            }
            return null;
        }

        public static T Deserialize<T>(string serialized)
        {
            XmlSerializer serializer = yats.Utilities.SerializationHelper.GetSerializer(typeof(T));
            using (StringReader reader = new StringReader(serialized))
            {
                try
                {
                    return (T)serializer.Deserialize(reader);
                }
                catch (Exception ex)
                {
                    CrashLog.Write(ex);
                }
            }
            return default(T);
        }

        public static T DeserializeFile<T>(string fileName)
        {
            return Deserialize<T>(File.ReadAllText(fileName));
        }

        public static void SerializeToFile<T>(T toSerialize, string fileName)
        {
            XmlSerializer serializer = yats.Utilities.SerializationHelper.GetSerializer(typeof(T));
            XmlWriterSettings ws = new XmlWriterSettings();
            ws.NewLineHandling = NewLineHandling.Entitize;
            ws.Indent = true;
            ws.CheckCharacters = true;
            using (XmlWriter wr = XmlWriter.Create(fileName, ws))
            {
                serializer.Serialize(wr, toSerialize);
            }
        }

        public static byte[] SerializeToArray<T>(T toSerialize)
        {
            XmlSerializer serializer = yats.Utilities.SerializationHelper.GetSerializer(typeof(T));
            XmlWriterSettings ws = new XmlWriterSettings();
            ws.NewLineHandling = NewLineHandling.Entitize;
            ws.Indent = true;
            ws.CheckCharacters = true;
            MemoryStream ms = new MemoryStream(10000);
            using (XmlWriter wr = XmlWriter.Create(ms, ws))
            {
                serializer.Serialize(wr, toSerialize);
            }
            return ms.ToArray();
        }

        public static XmlSerializer GetSerializer(object obj)
        {
            return GetSerializer(obj.GetType());
        }

        public static XmlSerializer GetSerializer(Type type)
        {
            lock (s_cache)
            {
                XmlSerializer fromCache = null;
                if (s_cache.TryGetValue(type, out fromCache))
                {
                    return fromCache;
                }
            }

            //log timing to console
            //var now = DateTime.Now;
            //Console.WriteLine(string.Format("GetSerializer {0} start", type.FullName));

            Stack<Type> stack = new Stack<Type>();
            HashSet<Type> result = new HashSet<Type>();

            //File.AppendAllText( @"d:\stack.txt", "----------------------------- " + Environment.NewLine );
            //File.AppendAllText( @"d:\stack.txt", "+ " + type.FullName + Environment.NewLine );

            PushToStack(type, stack);

            while (stack.Count > 0)
            {
                Type t = stack.Pop();
                //File.AppendAllText( @"d:\stack.txt", "- " + t.FullName + Environment.NewLine );
                var derived = AssemblyHelper.Instance.GetDerivedTypes(t);
                foreach (var d in derived)
                {
                    if (result.Add(d))
                    {
                        PushToStack(d, stack);
                        //File.AppendAllText( @"d:\stack.txt", "+ " + d.FullName + Environment.NewLine );
                    }
                }
                PropertyInfo[] properties = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo p in properties)
                {
                    if (p.HasAttribute(typeof(XmlIgnoreAttribute)))
                    {
                        continue;
                    }
                    if (AssemblyHelper.Instance.IsInMyAssembly(p.PropertyType))//TODO: is this necessary??? probably just an optimization
                    {
                        if (result.Add(p.PropertyType))
                        {
                            PushToStack(p.PropertyType, stack);
                            //File.AppendAllText( @"d:\stack.txt", "+ " + p.PropertyType.FullName + Environment.NewLine );
                        }
                    }
                }
            }
            var toRemove = new HashSet<Type>();
            foreach (var t in result)
            {
                lock (s_cache)
                {
                    if (s_cache.ContainsKey(t))
                    {
                        continue;
                    }
                }
                try
                {
                    lock (s_badTypeCache)
                    {
                        if (s_badTypeCache.Contains(t))
                        {
                            toRemove.Add(t);
                            continue;
                        }
                    }
                    // It may look tempting to add this serializer to the cache. Don't do it.
                    // This serializer is constructed without "extra types" parameters and will
                    // cause 'XML error' exceptions when reading XMLs.

                    new XmlSerializer(t); // will cause throw / catch if the type can not be serialized

                }
                catch
                {
                    lock (s_badTypeCache)
                    {
                        s_badTypeCache.Add(t);
                    }
                    toRemove.Add(t);
                }
            }
            foreach (var t in toRemove)
            {
                result.Remove(t);
            }

            // print warnings if precompiled serializer DLLs do not exist for the types
            foreach (var t in result)
            {
                string serializersPath = Path.ChangeExtension(t.Assembly.Location, ".XmlSerializers.dll");
                if (!File.Exists(serializersPath))
                {
                    Console.WriteLine(string.Format("XmlSerializers.dll not generated for {0}", t.Assembly.FullName));
                }
            }

            var serializer = new XmlSerializer(type, result.ToArray());
            lock (s_cache)
            {
                s_cache[type] = serializer;
            }

            //TimeSpan duration = DateTime.Now.Subtract( now );
            //Console.WriteLine(string.Format("GetSerializer {0} {1} end", type.FullName, duration.ToFriendlyDisplay(2)));

            return serializer;
        }

        private static void PushToStack(Type type, Stack<Type> stack)
        {
            stack.Push(type);
            if (type.IsGenericType)
            {
                foreach (var genType in type.GetGenericArguments())
                {
                    stack.Push(genType);
                }
            }
        }
    }
    /* Example - serializing custom objects with newtonsoft.json
     * 
string serialized = JsonConvert.SerializeObject (B, Formatting.Indented, new JsonSerializerSettings ()
{
TypeNameHandling = TypeNameHandling.Objects,
  TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
  });
var deserialized = JsonConvert.DeserializeObject (serialized, typeof(MyTestInterface),
new JsonSerializerSettings () { TypeNameHandling = TypeNameHandling.Objects }
);
*/
}
