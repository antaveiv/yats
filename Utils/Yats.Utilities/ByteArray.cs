﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace yats.Utilities
{
    /// <summary>
    /// 
    /// </summary>
    public static class ByteArray
    {
        /// <summary>
        /// copies subarray from byte array
        /// arr byte[]
        /// start index start 0 - count-1
        /// count
        /// </summary>
        /// <param name="arr">Array copy from</param>
        /// <param name="start"></param>
        /// <param name="count"></param>
        public static byte[] Copy(byte[] arr, int start, int count)
        {
            // <pex>
            if (arr == null)
                throw new ArgumentNullException("arr");
            if (start < 0 | count < 0)
                throw new ArgumentException("start and count must be positive. start < 0 OR count < 0");

            if (start + count > int.MaxValue)
            {
                throw new ArgumentException("overflow");
            }

            if ((uint)start >= (uint)(arr.Length))
                throw new ArgumentException("(uint)start >= (uint)(arr.Length)");
            // </pex>
            if (arr.Length < (start + count))
                throw new System.IndexOutOfRangeException();
            byte[] tmp = new byte[count];
            Buffer.BlockCopy(arr, start, tmp, 0, count);
            //for ( int i = 0; i < count; i++ )
            //    tmp[i] = arr[start + i];
            return tmp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bt"></param>
        /// <returns></returns>
        public static string ToString(byte[] bt)
        {
            // <pex>
            if (bt == null)
                throw new ArgumentNullException("bt");
            // </pex>
            string s = string.Empty;
            foreach (byte b in bt)
            {
                s += (char)b;
            }
            return s;
        }

        /// <summary>
        /// Grazina stringa, kur 0x0d keicamas i CR, 0x0a i LF t.p su SOH, STX, ETX, ACK, NACK, EOT nuo 0x21 iki 0x7e i (char)b, visi kiti i hex
        /// Daznai naudiojamas Debuginimui
        /// </summary>
        /// <param name="bt"></param>
        /// <returns></returns>
        public static string ToStringCrLfToHex(byte[] bt)
        {
            // <pex>
            if (bt == null)
                throw new ArgumentNullException("bt");
            // </pex>

            string s = string.Empty;
            foreach (byte b in bt)
            {
                if (b == 0x0d)
                {
                    s += "<CR>"; // Carriage return
                    continue;
                }
                if (b == 0x0a)
                {
                    s += "<LF>"; // Line feed
                    continue;
                }
                if (b == 0x01)
                {
                    s += "<SOH>"; // Start-of-header
                    continue;
                }
                if (b == 0x02)
                {
                    s += "<STX>"; // Start of text
                    continue;
                }
                if (b == 0x03)
                {
                    s += "<ETX>"; // End of text
                    continue;
                }
                if (b == 0x04)
                {
                    s += "<EOT>"; // End of text block
                    continue;
                }
                if (b == 0x06)
                {
                    s += "<ACK>"; // Acknowledge
                    continue;
                }
                if (b == 0x15)
                {
                    s += "<NAK>"; // Negative acknowledge
                    continue;
                }
                if (b >= 0x21 & b <= 0x7e)
                {
                    s += (char)b;
                    continue;
                }
                s += b.ToString("X2") + " ";
            }
            return s;
        }

        private static readonly char[] lookup = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        public static string ToStringHex(byte[] data)
        {
            if (data == null || data.Length == 0)
            {
                return string.Empty;
            }

            int i = 0, p = 0, l = data.Length;
            char[] c = new char[l * 2];
            byte d;
            while (i < l)
            {
                d = data[i++];
                c[p++] = lookup[d / 0x10];
                c[p++] = lookup[d % 0x10];
            }
            return new string(c, 0, c.Length);
        }



        /// <summary>
        /// Convert to hexadecimal dump
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string ToHexString(this byte[] data)
        {
            return ToStringHex(data);
        }

        public static string ToHexString(this byte[] data, string separator, int offset, int count)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data is null");
            }
            if (offset + count > data.Length)
            {
                throw new ArgumentOutOfRangeException();
            }
            byte[] copy = new byte[count];
            Buffer.BlockCopy(data, offset, copy, 0, count);
            return ToHexString(copy, separator);
        }

        /// <summary>
        /// Convert to hexadecimal dump
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string ToHexString(this byte[] data, string separator)
        {
            if (data == null || data.Length == 0)
            {
                return string.Empty;
            }

            StringBuilder sb = new StringBuilder(data.Length * 2 + (data.Length - 1) * separator.Length);

            int i = 0, l = data.Length;
            char[] c = new char[l * 2];
            byte d;
            while (i < l - 1)
            {
                d = data[i++];
                sb.Append(lookup[d / 0x10]);
                sb.Append(lookup[d % 0x10]);
                sb.Append(separator);
            }
            //last byte
            d = data[i];
            sb.Append(lookup[d / 0x10]);
            sb.Append(lookup[d % 0x10]);
            return sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static byte[] ToArray(string s)
        {
            // <pex>
            if (s == null)
                throw new ArgumentNullException("s");
            //Empty string is perfectly legal !!! 
            //do not refactor things You don't understand !!!  
            // if ( s.Length == 0 ) 
            //     throw new ArgumentException( @"s.Length == 0", "s" );
            // </pex>
            int ln = s.Length;
            //ASCIIEncoding ascii = new ASCIIEncoding();
            //byte[] tmp=ascii.GetBytes( s );
            byte[] tmp = new byte[ln];

            for (int i = 0; i < ln; i++)
            {
                try
                {
                    tmp[i] = (byte)s[i];
                }
                catch (OverflowException)
                {
                    // kai stringe yra Unicode (2 baitu) simboliai, atsiranda sis exceptionas.
                }
            }
            return tmp;
        }

        /// <summary>
        /// adds two subarrays into one
        /// </summary>
        /// <param name="arr1"></param>
        /// <param name="bt"></param>
        /// <returns></returns>
        public static byte[] Add(byte[] arr1, byte bt)
        {
            if (arr1 == null)
                return new byte[] { bt };
            int p1 = arr1.Length;
            byte[] tmp = new byte[p1 + 1];
            Buffer.BlockCopy(arr1, 0, tmp, 0, p1);
            //for ( int i = 0; i < p1; i++ )
            //    tmp[i] = arr1[i];
            tmp[p1] = bt;
            return tmp;
        }

        public static bool IsNullOrEmpty(this byte[] array)
        {
            return array == null || array.Length == 0;
        }

        public static bool IsNullOrEmpty(this byte[] array, int offset, int count)
        {
            if (array.IsNullOrEmpty())
            {
                return true;
            }
            return array.Length - offset <= 0 || count <= 0;
        }

        // returns true if both arrays are null or equal
        public static bool Equal(byte[] A, byte[] B)
        {
            if ((A == null) != (B == null))
            {
                return false;
            }
            if (A == null)
            {
                return true;
            }
            if (A.Length != B.Length)
            {
                return false;
            }
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] != B[i])
                {
                    return false;
                }
            }
            return true;
        }

        // returns true if both arrays are null or equal
        public static bool Equal(byte[] A, byte[] B, int offset, int count)
        {
            if ((A == null) != (B == null))
            {
                return false;
            }
            if (A == null)
            {
                return true;
            }
            if (A.Length < offset + count || B.Length < offset + count)
            {
                return false;
            }
            for (int i = offset; i < offset + count; i++)
            {
                if (A[i] != B[i])
                {
                    return false;
                }
            }
            return true;
        }

        public static bool ContainsAsciiControlCharacters(this byte[] bytes)
        {
            if (bytes == null)
            {
                return false;
            }
            foreach (var b in bytes)
            {
                if (b == 0x09) //Tab
                {
                    continue;
                }
                if (b == 0x0A) //LF
                {
                    continue;
                }
                if (b == 0x0D) // CR
                {
                    continue;
                }
                if (b < 0x20 || b > 0x7E)
                {
                    return true;
                }
            }
            return false;
        }

        public static string ToASCII(this byte[] bytes)
        {
            if (bytes == null)
            {
                return null;
            }
            return Encoding.ASCII.GetString(bytes);
        }

        static public string NullTerminatedBytesToString(byte[] data, Encoding encoding)
        {
            int inx = Array.FindIndex(data, 0, (x) => x == 0);//search for 0
            if (inx >= 0)
                return (encoding.GetString(data, 0, inx));
            else
                return (encoding.GetString(data));
        }

        static public string NullTerminatedBytesToString(byte[] data)
        {
            return NullTerminatedBytesToString(data, Encoding.ASCII);
        }

        /// <summary>
        /// Parses hex string. Removes extra whitespace, "0x", some punctuations
        /// </summary>
        /// <param name="hexText"></param>
        /// <returns>Converted byte array</returns>
        /// <throws>FormatException when bad characters remain after cleanup, or the string length does not divide by 2</throws>
        public static byte[] HexStringToBytes(string hexText)
        {
            if (hexText == null)
            {
                return null;
            }

            hexText = hexText.Replace("0X", "");
            hexText = hexText.Replace("0x", "");

            {
                List<string> tokens = new List<string>();
                string curr = "";
                foreach (char c in hexText)
                {
                    if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'))
                    {
                        curr += c;
                    }
                    else if (curr.IsNullOrEmpty() == false)
                    {
                        tokens.Add(curr);
                        curr = "";
                    }
                }
                if (curr.IsNullOrEmpty() == false) //last token
                {
                    tokens.Add(curr);
                }

                if (tokens.Count > 1 && tokens.Min(x=>x.Length) == 1 && tokens.Max(x=>x.Length) <= 2) {
                    // was split into different size tokens - interpret as individual hex bytes. E.g. 0x00 is put as '0' in input string
                    byte[] result = new byte[tokens.Count];
                    for (int i = 0; i < tokens.Count; i++)
                    {
                        result[i] = byte.Parse(tokens[i], System.Globalization.NumberStyles.HexNumber);
                    }
                    return result;
                }
            }

            
            hexText = hexText.Replace(" ", "");
            hexText = hexText.Replace("\t", "");
            hexText = hexText.Replace(":", "");
            hexText = hexText.Replace(",", "");
            hexText = hexText.Replace("\r", "");
            hexText = hexText.Replace("\n", "");
            hexText = hexText.Replace("{", ""); // used by hercules utility
            hexText = hexText.Replace("}", "");
            hexText = hexText.Replace("'", "");
            hexText = hexText.Replace("\"", "");

            foreach (var c in hexText)
            {
                if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'))
                {
                    continue;
                }
                throw new FormatException("Hex string contains invalid characters");
            }

            if (hexText.Length % 2 != 0)
            {
                throw new FormatException("Invalid hex string");
            }

            int ln = hexText.Length / 2;
            byte[] bt = new byte[ln];
            for (int i = 0; i < ln; i++)
            {
                bt[i] = byte.Parse(hexText.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
            }
            return bt;
        }


        /// <summary>
        /// Parses string containing decimal byte values. Removes whitespace, some punctuation
        /// </summary>
        /// <param name="text"></param>
        /// <returns>Converted byte array</returns>
        /// <throws>FormatException</throws>
        public static byte[] DecimalStringToBytes(string text)
        {
            if (text == null)
            {
                return null;
            }

            List<string> tokens = new List<string>();
            string curr = "";
            foreach (char c in text)
            {
                if ((c >= '0' && c <= '9'))
                {
                    curr += c;
                }
                else if (curr.IsNullOrEmpty() == false)
                {
                    tokens.Add(curr);
                    curr = "";
                }
            }
            if (curr.IsNullOrEmpty() == false) //last token
            {
                tokens.Add(curr);
            }

            byte[] result = new byte[tokens.Count];
            for (int i = 0; i < tokens.Count; i++)
            {
                result[i] = byte.Parse(tokens[i], System.Globalization.NumberStyles.Number);
            }
            return result;
        }

        // <summary>
        // Parses string to int array using given list of separators
        // </summary>
        //  Exceptions:
        //   System.ArgumentNullException:
        //     text is null.
        //
        //   ArgumentException
        //     separators is null or empty
        //
        //   System.FormatException:
        //     text is not in the correct format.
        //
        //   System.OverflowException:
        //     text contains a number less than System.Int32.MinValue or greater than System.Int32.MaxValue.
        public static int[] StringToInts(string text, params string[] separators)
        {
            if (text == null)
            {
                throw new ArgumentNullException("text is null");
            }
            if (separators.IsNullOrEmpty())
            {
                throw new ArgumentException("Split separators must be given");
            }
            return Array.ConvertAll(text.Split(separators, StringSplitOptions.RemoveEmptyEntries), int.Parse);
        }

        // <summary>
        // Parses string to a float array. 
        // </summary>
        //  Exceptions:
        //   System.ArgumentNullException:
        //     text is null.
        //
        //   System.FormatException:
        //     text is not in the correct format.
        //
        //   System.OverflowException:
        //     text contains a number less than System.Int32.MinValue or greater than System.Int32.MaxValue.
        public static float[] StringToFloats(string text, CultureInfo cultureInfo, params string[] separators)
        {
            if (text == null)
            {
                throw new ArgumentNullException("text is null");
            }
            if (separators.IsNullOrEmpty())
            {
                throw new ArgumentException("Split separators must be given");
            }
            return text.Split(separators, StringSplitOptions.RemoveEmptyEntries).Select(s => float.Parse(s, cultureInfo)).ToArray();
        }

        /// <summary>
        /// Convert byte array to C array, e.g. "0x00, 0x03"
        /// </summary>
        /// <param name="data">bytes to convert</param>
        /// <returns>Empty string if data is null or empty. Formatted string otherwise</returns>
        public static string ToCHexArray(byte[] data)
        {
            if (data.IsNullOrEmpty())
            {
                return "";
            }
            return "0x" + data.ToHexString(", 0x");
        }

        /// <summary>
        /// e.g. "unsigned char rawData[2] = { 0x6F, 0x67 };"
        /// </summary>
        /// <param name="data"></param>
        /// <returns>null if data is null, formatted string otherwise</returns>
        public static string ToCArrayFull(byte[] data)
        {
            int chunkSize = 16;
            if (data == null)
            {
                return null;
            }
            if (data.Length == 0)
            {
                return "unsigned char rawData[0];";
            }
            if (data.Length <= chunkSize)
            {
                return string.Format("unsigned char rawData[{0}] = {{{1}}};", data.Length, ToCHexArray(data));
            }
            StringBuilder result = new StringBuilder(string.Format("const uint8_t rawData[{0}] = {{{1}", data.Length, Environment.NewLine));

            int offset = 0;
            while (offset < data.Length)
            {
                result.Append("    ");
                result.Append(ToCHexArray(data.Slice(offset, Math.Min(chunkSize, data.Length - offset))));
                offset += chunkSize;
                if (offset < data.Length)
                {
                    result.Append(",");
                }
                result.Append(Environment.NewLine);
            }
            result.Append("};");
            return result.ToString();
        }

        /// <summary>
        /// e.g. "byte[] rawData = { 0x6F, 0x67 };"
        /// </summary>
        /// <param name="data"></param>
        /// <returns>null if data is null, formatted string otherwise</returns>
        public static string ToCSharpArrayFull(byte[] data)
        {
            int chunkSize = 16;
            if (data == null)
            {
                return null;
            }
            if (data.Length == 0)
            {
                return "byte [] rawData = {};";
            }
            if (data.Length <= chunkSize)
            {
                return string.Format("byte[] rawData = {{{0}}};", ToCHexArray(data));
            }
            StringBuilder result = new StringBuilder(string.Format("byte[] rawData = {{{0}", Environment.NewLine));

            int offset = 0;
            while (offset < data.Length)
            {
                result.Append("    ");
                result.Append(ToCHexArray(data.Slice(offset, Math.Min(chunkSize, data.Length - offset))));
                offset += chunkSize;
                if (offset < data.Length)
                {
                    result.Append(",");
                }
                result.Append(Environment.NewLine);
            }
            result.Append("};");
            return result.ToString();
        }

        /// <summary>
        /// e.g. "byte rawData[] = { (byte)0x6F, (byte)0x67 };"
        /// </summary>
        /// <param name="data"></param>
        /// <returns>null if data is null, formatted string otherwise</returns>
        public static string ToJavaArrayFull(byte[] data)
        {
            int chunkSize = 16;
            if (data == null)
            {
                return null;
            }
            if (data.Length == 0)
            {
                return "byte rawData[] = {};";
            }
            if (data.Length <= chunkSize)
            {
                return string.Format("byte rawData[] = {{{0}}};", "(byte)0x" + data.ToHexString(", (byte)0x"));
            }
            StringBuilder result = new StringBuilder(string.Format("byte rawData[] = {{{0}", Environment.NewLine));

            int offset = 0;
            while (offset < data.Length)
            {
                result.Append("    ");
                result.Append("(byte)0x");
                result.Append(data.Slice(offset, Math.Min(chunkSize, data.Length - offset)).ToHexString(", (byte)0x"));
                offset += chunkSize;
                if (offset < data.Length)
                {
                    result.Append(",");
                }
                result.Append(Environment.NewLine);
            }
            result.Append("};");
            return result.ToString();
        }

        public static decimal InterpretByteArray(this byte[] bytes, bool signed, bool littleEndian)
        {
            if (bytes == null)
            {
                throw new ArgumentNullException("bytes is null");
            }
            if (bytes.Length < 1 || bytes.Length > 8)
            {
                throw new ArgumentOutOfRangeException("Must be between 1 and 8 bytes long");
            }
            //var bt = new byte[] { 0x81, 0x02 }; GetSelection();
            byte[] eightBytes;
            if (!littleEndian)
            {
                bytes = bytes.Reverse().ToArray();
            }
            if (signed)
            {
                if ((bytes[bytes.Length - 1] & 0x80) != 0)//sign bit set
                {
                    eightBytes = new byte[8] { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };
                }
                else
                {
                    eightBytes = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
                }
                bytes.CopyTo(eightBytes, 0);
                return BitConverter.ToInt64(eightBytes, 0);
            }
            else
            {
                eightBytes = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
                bytes.CopyTo(eightBytes, 0);
                return BitConverter.ToUInt64(eightBytes, 0);
            }
        }

        /// <summary>
        /// Converts a value to bytes with specified length
        /// </summary>
        /// <param name="value">Value to convert</param>
        /// <param name="numBytes">Number of bytes to fit in. Must be 1..8</param>
        /// <param name="signed">Signed number</param>
        /// <param name="littleEndian">Little endian number. If false, convert to big endian</param>
        /// <returns></returns>
        public static byte[] ToByteArray(this decimal value, int numBytes, bool signed, bool littleEndian)
        {
            byte [] result;
            if (numBytes < 0 || numBytes > 8){
                throw new ArgumentOutOfRangeException("numBytes must be 1..8");
            }
            decimal min = 0;
            decimal max = (decimal)(Math.Pow(2, numBytes * 8 - 0) - 1);

            if (signed)
            {
                min = -(decimal)(Math.Pow(2, numBytes * 8 - 1));
                max = (decimal)(Math.Pow(2, numBytes * 8 - 1) - 1);
            }

            if (value > max){
                throw new ArgumentOutOfRangeException("Value should be less or equal to " + max);
            }
            else if (value < min){
                throw new ArgumentOutOfRangeException("Value should more or equal to " + min);
            }

            if (signed)
            {
                result = BitConverter.GetBytes((long)value);
            }
            else
            {
                result = BitConverter.GetBytes((ulong)value);
            }
                   
            result = result.Take(numBytes).ToArray();

            if (!littleEndian)
            {
                result = result.Reverse().ToArray();
            }
            return result;
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public static T ByteArrayToStructure<T>(byte[] bytes) where T : struct
        {
            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            try
            {
                T stuff = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
                return stuff;
            }
            finally
            {
                handle.Free();
            }
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public static byte[] StructureToByteArray(object obj)
        {
            int len = Marshal.SizeOf(obj);

            byte[] arr = new byte[len];

            IntPtr ptr = Marshal.AllocHGlobal(len);

            Marshal.StructureToPtr(obj, ptr, true);

            Marshal.Copy(ptr, arr, 0, len);

            Marshal.FreeHGlobal(ptr);

            return arr;
        }
    }
}
