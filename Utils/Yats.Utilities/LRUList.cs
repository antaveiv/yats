﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Collections.Specialized;

namespace yats.Utilities
{
    public class LRUList<T>
    {
        private List<T> m_ItemsArray;
        public List<T> Items
        {
            get { return m_ItemsArray; }
        }
        
        private int m_maxSize;
        public int Max
        {
            get { return m_maxSize; }
            set { m_maxSize = value; }
        }

        public LRUList(int maxSize)
        {
            this.m_maxSize = maxSize;
            m_ItemsArray =  new List<T>(maxSize);
        }

        public LRUList(int maxSize, List<T> items)
        {
            this.m_maxSize = maxSize;
            if(items.Count <= maxSize)
            {
                m_ItemsArray = items;
            }
            else
            {
                m_ItemsArray = new List<T>(maxSize);
                for(int i = 0; i < maxSize; i++)
                {
                    m_ItemsArray.Add( items[i] );
                }
            }
        }

        public void Add(T item)
        {
            if(m_maxSize <= 0)
            {
                return;
            }
            if (m_ItemsArray.Count == 0)
            {
                m_ItemsArray.Insert(0, item);
            }
            if (m_ItemsArray[0].Equals(item))
            {
                return;
            }
            else 
            {
                m_ItemsArray.Remove(item);
                if (m_ItemsArray.Count < m_maxSize)
                {
                    m_ItemsArray.Insert(0, item);
                }
                else
                {
                    m_ItemsArray.RemoveAt(m_maxSize - 1);
                    m_ItemsArray.Insert(0, item);
                }
            }
        }

        public static LRUList<string> FromStringCollection(StringCollection strings, int maxSize)
        {
            var tmp = new List<string>();
            if (strings != null)
            {
                foreach (var s in strings)
                {
                    tmp.Add(s);
                }
            }
            LRUList<string> result = new LRUList<string>(maxSize, tmp);
            return result;
        }

        public static StringCollection ToStringCollection(LRUList<string> list)
        {
            StringCollection result = new StringCollection();
            foreach (var s in list.Items)
            {
                result.Add(s);
            }
            return result;
        }
    }
}
