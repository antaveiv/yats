﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace yats.Utilities
{
    public static class QueueExtensions
    {


        public static void EnqueueAll<T>(this Queue<T> queue, params T[] items)
        {
            foreach (var item in items)
            {
                queue.Enqueue(item);
            }
        }

        public static void EnqueueAll<T>(this Queue<T> queue, IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                queue.Enqueue(item);
            }
        }

        public static void EnqueueAll<T>(this Queue<T> queue, T[] items, int offset, int count)
        {
            for (int i = offset; i < offset + count; i++)
            {
                queue.Enqueue(items[i]);
            }
        }

        public static void EnqueueAll(this Queue queue, IEnumerable items)
        {
            foreach (var item in items)
            {
                queue.Enqueue(item);
            }
        }

        public static void EnqueueAll<T>(this Queue queue, params T[] items)
        {
            foreach (var item in items)
            {
                queue.Enqueue(item);
            }
        }

        public static void EnqueueAll<T>(this Queue queue, T[] items, int offset, int count)
        {
            lock (queue.SyncRoot)
            {
                for (int i = offset; i < offset + count; i++)
                {
                    queue.Enqueue(items[i]);
                }
            }
        }
    }
}
