﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace yats.Utilities
{
    public class BigEndianBinaryWriter : BinaryWriter
    {
        public BigEndianBinaryWriter(Stream output)
            : base(output)
        {
        }

        public override void Write(short value)
        {
            Write((ushort)value);
        }

        public override void Write(ushort value)
        {
            base.Write((byte)(value >> 8));
            base.Write((byte)(value & 0xFF));
        }

        public override void Write(int value)
        {
            Write((uint)value);
        }

        public override void Write(uint value)
        {
            Write((ushort)(value & 0xFFFF));
            Write((ushort)(value >> 16));
        }
    }
}
