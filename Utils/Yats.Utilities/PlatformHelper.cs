﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.IO;
using System.Security.Permissions;

namespace yats.Utilities
{
    public class PlatformHelper
    {
        public static bool IsWindows()
        {
            switch (System.Environment.OSVersion.Platform)
            {
                case PlatformID.Win32NT:
                case PlatformID.Win32Windows:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsLinux()
        {
            switch (System.Environment.OSVersion.Platform)
            {
                case PlatformID.Unix:
                    return true;
                default:
                    return false;
            }
        }

        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        public static void OpenFileBrowser(string fileName)
        {
            if(IsWindows( ))
            {
                string argument;
                if (File.Exists(fileName))
                {
                    argument = @"/select, " + fileName;
                }
                else
                {
                    argument = @"/root," + Path.GetDirectoryName(fileName);
                }
                System.Diagnostics.Process.Start("explorer.exe", argument);
            }
        }

        //Replaces the USERPROFILE variable
        public static string ExpandEnvironmentVariables(string value)
        {
            switch (System.Environment.OSVersion.Platform)
            {
                case PlatformID.MacOSX:
                case PlatformID.Unix:
                    value = value.Replace("%USERPROFILE%", "${USERPROFILE}");
                    break;
                default:
                    value = value.Replace("${USERPROFILE}", "%USERPROFILE%");
                    break;
            }
            return Environment.ExpandEnvironmentVariables(value);
        }

        public static bool IsRunningOnMono()
        {
            return Type.GetType("Mono.Runtime") != null;
        }

        protected static int pcSleepCounter = 0;
        protected static readonly object pcSleepCounterLock = new object();

        /// <summary>
        /// Prevents the PC from entering sleep/hibernate. The calls to this function are counted and the same amount of calls to AllowPcSleep() are necessary to cancel the effect. This is done so that multiple tests may be running in parallel
        /// </summary>
        public static void PreventPcSleep()
        {
            lock (pcSleepCounterLock)
            {
                pcSleepCounter++;
            }
            if (IsWindows())
            {
                EXECUTION_STATE tmp = SafeNativeMethods.SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS | EXECUTION_STATE.ES_DISPLAY_REQUIRED | EXECUTION_STATE.ES_SYSTEM_REQUIRED);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static void AllowPcSleep()
        {
            lock (pcSleepCounterLock)
            {
                pcSleepCounter--;
            }
            if (IsWindows())
            {
                if (pcSleepCounter <= 0)
                {
                    EXECUTION_STATE tmp = SafeNativeMethods.SetThreadExecutionState(EXECUTION_STATE.ES_DISPLAY_REQUIRED | EXECUTION_STATE.ES_SYSTEM_REQUIRED);
                }
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}
