﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace yats.Utilities
{
    //http://www.albahari.com/threading/part4.aspx#%5FWait%5Fand%5FPulse
    //http://stackoverflow.com/questions/1656404/c-sharp-producer-consumer
    //Buffers enqueued items and consumes them one by one. Single or multiple consumer threads are created
    public class ProducerConsumer<T> : IProducerConsumer<T> where T:class
    {
        protected readonly object mLockObject = new object();
        protected Dictionary<ConsumerThread, Thread> workers;
        protected Queue<T> taskQ = new Queue<T>();
        
        /// <summary>
        /// Single worker thread.
        /// </summary>
        /// <param name="consumptionAction">An action to perform on enqueued tasks</param>
        public ProducerConsumer(Action<T> consumptionAction):this(1, ThreadPriority.Normal, consumptionAction)
        {
        }

        /// <summary>
        /// Create a producer-consumer queue with multiple worker threads of given thread priority
        /// </summary>
        /// <param name="workerCount">Number of worker threads to create</param>
        /// <param name="workerPriority">Thread priority</param>
        /// <param name="consumptionAction">An action to perform on enqueued tasks</param>
        public ProducerConsumer(int workerCount, ThreadPriority workerPriority, Action<T> consumptionAction)
        {
            workers = new Dictionary<ConsumerThread, Thread>(workerCount);
            // Create and start a separate thread for each worker
            for (int i = 0; i < workerCount; i++)
            {
                var worker = new ConsumerThread(this, consumptionAction);
                Thread thread = new Thread(worker.Consume);
                thread.Priority = workerPriority;
                if (workerCount > 1)
                {
                    thread.Name = "ProducerConsumer<" + typeof(T).Name + "> #" + i;
                }
                else
                {
                    thread.Name = "ProducerConsumer<" + typeof(T).Name + ">";
                }
                thread.Start();
                workers.Add(worker, thread);
            }
        }

        private bool _disposed = false;
        private bool _disposing = false;

        ~ProducerConsumer() 
        { 
            Dispose(false); 
        }
        
        /// <summary>
        /// Stop worker threads
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            // Use SupressFinalize in case a subclass 
            // of this type implements a finalizer.
            GC.SuppressFinalize(this);
        }
        
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _disposing = true;

                    // Enqueue one null task per worker to make each exit.
                    foreach (var worker in workers.Keys)
                    {
                        worker.Stop = true;
                    }
                    lock (mLockObject)
                    {
                        Monitor.PulseAll(mLockObject);
                    }
                    //foreach (var thread in workers.Values)
                    //{
                    //    thread.Join();
                    //}
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
        }

        /// <summary>
        /// Enqueue a given task. It will be given to an available worker thread
        /// </summary>
        /// <param name="task"></param>
        public void EnqueueTask(T task)
        {
            if (_disposed || _disposing)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            lock (mLockObject)
            {
                taskQ.Enqueue(task);
                Monitor.PulseAll(mLockObject);
            }
        }

        protected class ConsumerThread
        {
            public bool Stop = false;
            private ProducerConsumer<T> mOwner;
            private Action<T> mConsumptionAction;
            public ConsumerThread(ProducerConsumer<T> owner, Action<T> consumptionAction)
            {
                mOwner = owner;
                mConsumptionAction = consumptionAction;
            }

            public void Consume()
            {
                while (!Stop)
                {
                    T task;
                    lock (mOwner.mLockObject)
                    {
                        while (mOwner.taskQ.Count == 0)
                        {
                            Monitor.Wait(mOwner.mLockObject);
                            if (Stop)
                            {
                                return;
                            }
                        }                        
                        task = mOwner.taskQ.Dequeue();
                    }
                    mConsumptionAction(task);
                }
            }
        }

        public int Count
        {
            get { return taskQ.Count; }
        }
    }
}
