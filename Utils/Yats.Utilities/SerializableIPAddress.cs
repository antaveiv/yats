﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Net;

namespace yats.Utilities
{
    public class SerializableIPAddress : IPAddress
    {
        public SerializableIPAddress()
            : base(new byte[] { 0, 0, 0, 0 })
        {
        }

        public SerializableIPAddress(IPAddress ip)
            : base(ip.GetAddressBytes())
        {
        }

        public SerializableIPAddress(byte[] address)
            : base(address)
        {
        }

        public SerializableIPAddress(string ip)
            : base(IPAddress.Parse(ip).GetAddressBytes())
        {
        }
    }
}
