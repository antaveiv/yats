﻿// Copyright (c) Damien Guard.  All rights reserved.
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace yats.Utilities
{
    /// <summary>
    /// Implements a 32-bit CRC-32/MPEG-2 hash algorithm
    /// </summary>
    public class Crc32Mpeg2
    {
        uint[] table;

        public uint ComputeChecksum(byte[] bytes)
        {
            uint crc = 0xffffffff;
            for (int i = 0; i < bytes.Length; i++)
                crc = (crc << 8) ^ table[((crc >> 24) ^ bytes[i]) & 0xff];
            return crc;
        }

        public byte[] ComputeChecksumBytes(byte[] bytes)
        {
            return BitConverter.GetBytes(ComputeChecksum(bytes));
        }

        public Crc32Mpeg2()
        {
            uint poly = 0x04C11DB7;
            table = new uint[256];
            uint i, j, k;

            for (i = 0; i < 256; i++)
            {
                k = 0;
                for (j = (i << 24) | 0x800000; j != 0x80000000; j <<= 1)
                    k = (k << 1) ^ ((((k ^ j) & 0x80000000) != 0) ? poly : 0);

                table[i] = k;
            }
        }
    }
 }

