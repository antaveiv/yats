﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace yats.Utilities
{
    public static class ListExtensions
    {
        public static List<T> ToSingleItemList<T>(this T item)
        {
            return new List<T>(1) { item };
        }

        public static List<T> GetList<T>(params T[] array)
        {
            List<T> result = new List<T>(array);
            return result;
        }

        public static bool ListsEqual<T>(IList<T> a, IList<T> b)
        {
            if ((a == null) != (b == null))
            {
                return false;
            }

            if (a == null)
            {
                return true;
            }

            if (a.Count != b.Count)
            {
                return false;
            }

            for (int i = 0; i < a.Count; i++)
            {
                if (object.Equals(a[i], b[i]) == false)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsAtLastPosition<T>(this IList<T> list, T item)
        {
            return list != null && list.IndexOf(item) == (list.Count - 1);
        }

        public static bool IsAtFirstPosition<T>(this IList<T> list, T item)
        {
            return list != null && list.Count > 0 && list.IndexOf(item) == 0;
        }

        public static bool SwitchItems<T>(this IList<T> list, int index1, int index2)
        {
            if (index1 == index2 || list == null || index1 < 0 || index2 < 0 || list.Count < index1 || list.Count < index2)
            {
                return false;
            }
            T tmp = list[index1];
            list[index1] = list[index2];
            list[index2] = tmp;
            return true;
        }

        public static int Remove<T>(this IList<T> collection, Predicate<T> p)
        {
            int result = 0;
            if (collection == null)
            {
                return result;
            }
            List<int> toRemove = new List<int>();
            for (int index = 0; index < collection.Count; index++)
            {
                int idx = index;
                if (p(collection[idx]))
                {
                    toRemove.Add(idx);
                }
            }
            toRemove.Reverse();
            foreach (int index in toRemove)
            {
                collection.RemoveAt(index);
                result++;
            }
            return result;
        }

        public static int RemoveByReference<T>(this IList<T> collection, T item)
        {
            return collection.Remove(x => object.ReferenceEquals(x, item));
        }

        public static int RemoveByReference<T>(this IList<T> collection, ICollection<T> items)
        {
            int result = 0;
            if (collection == null || items == null)
            {
                return result;
            }
            foreach (T item in items)
            {
                result += collection.Remove(x => object.ReferenceEquals(x, item));
            }
            return result;
        }


        public static void AddRange<T>(this IList<T> list, IEnumerable<T> items)
        {
            if (items == null || list == null)
            {
                return;
            }
            foreach (var item in items)
            {
                list.Add(item);
            }
        }

        public static void RemoveRange<T>(this IList<T> list, IEnumerable<T> items)
        {
            if (items == null || list == null)
            {
                return;
            }
            foreach (var item in items)
            {
                list.Remove(item);
            }
        }

        public static void Sort<T>(this IList<T> list, IComparer<T> comparer)
        {
            if (list == null || comparer == null)
            {
                return;
            }
            if (list is List<T>)
            {
                (list as List<T>).Sort(comparer);
                return;
            }
            throw new NotImplementedException();
        }

        public static void Sort<T>(this IList<T> list, Comparison<T> comparison)
        {
            if (list == null || comparison == null)
            {
                return;
            }
            if (list is List<T>)
            {
                (list as List<T>).Sort(comparison);
                return;
            }
            throw new NotImplementedException();
        }


        public static bool ListEquals<T>(this IList<T> list1, IList<T> list2)
        {
            if ((list1 == null) != (list2 == null))
            {
                return false;
            }
            if (list1.Count != list2.Count)
                return false;
            for (int i = 0; i < list1.Count; i++)
                if (!list1[i].Equals(list2[i]))
                    return false;
            return true;
        }

        public static bool ListEquals(this IList list1, IList list2)
        {
            if ((list1 == null) != (list2 == null))
            {
                return false;
            }
            if (list1.Count != list2.Count)
                return false;
            for (int i = 0; i < list1.Count; i++)
                if (!list1[i].Equals(list2[i]))
                    return false;
            return true;
        }

        /// <summary>
        /// Splits an array into several smaller arrays.
        /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array to split.</param>
        /// <param name="size">The size of the smaller arrays.</param>
        /// <returns>An array containing smaller arrays.</returns>
        public static IEnumerable<IEnumerable<T>> Split<T>(this T[] array, int size)
        {
            for (var i = 0; i < (float)array.Length / size; i++)
            {
                yield return array.Skip(i * size).Take(size);
            }
        }

        /// <summary>
        /// Splits an array into several smaller arrays.
        /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array to split.</param>
        /// <param name="size">The size of the smaller arrays.</param>
        /// <returns>A collection containing smaller arrays.</returns>
        public static IEnumerable<T []> SplitToArrays<T>(this T[] array, int size)
        {
            for (var i = 0; i < (float)array.Length / size; i++)
            {
                yield return array.Skip(i * size).Take(size).ToArray();
            }
        }
    }
}
