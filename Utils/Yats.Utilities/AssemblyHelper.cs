﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace yats.Utilities
{
    public class AssemblyHelper
    {
        private AssemblyHelper()
        {
        }

        private static AssemblyHelper mInstance;
        public static AssemblyHelper Instance
        {
            get
            {
                if (mInstance == null)
                {
                    mInstance = new AssemblyHelper();
                }
                return mInstance;
            }
        }
        private List<Assembly> m_assemblyCache = null;
        
        public IEnumerable<string> GetAssemblyFileNames(params string[] skipAssemblyFileNames)
        {
            return GetAssemblyFileNames(skipAssemblyFileNames, null);
        }
        
        public IEnumerable<string> GetAssemblyFileNames(string[] skipAssemblyFileNames, IEnumerable<string> extraSearchPaths)
        {
            return GetAssemblyFileNames(skipAssemblyFileNames, extraSearchPaths, false);
        }

        //TODO pass list of file extensions instead of hard-coded DLL/EXE
        public IEnumerable<string> GetAssemblyFileNames(string[] skipAssemblyFileNames, IEnumerable<string> extraSearchPaths, bool recursiveExtraPaths)
        {
            HashSet<string> result = new HashSet<string>();
            List<string> assemblies = new List<string>();
            HashSet<string> directories = GetLookupDirectories();
            if (extraSearchPaths != null)
            {
                var extraPathSearchOption = (recursiveExtraPaths) ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
                foreach (var directory in extraSearchPaths)
                {
                    if (Directory.Exists(directory) == false)
                    {
                        continue;
                    }
                    assemblies.AddRange(Directory.GetFiles(directory, "*.dll", extraPathSearchOption));
                    assemblies.AddRange(Directory.GetFiles(directory, "*.exe", extraPathSearchOption));
                }
            }

            foreach (var directory in directories.Where(dir=>Directory.Exists(dir)))
            {
                try
                {
                    assemblies.AddRange(Directory.GetFiles(directory, "*.dll", SearchOption.TopDirectoryOnly));
                    assemblies.AddRange(Directory.GetFiles(directory, "*.exe", SearchOption.TopDirectoryOnly));
                }
                catch (Exception ex)
                {
                    CrashLog.Write(ex, "GetDllList");
                }
            }

            try
            {
                foreach (var dll in assemblies)
                {                    
                    string dllName = System.IO.Path.GetFileName(dll);
                    if (skipAssemblyFileNames != null)
                    {
                        bool skip = false;
                        foreach (string skipped in skipAssemblyFileNames)
                        {
                            if (FileUtil.FileNameMatchesWildcardPattern2(dllName, skipped))
                            {
                                skip = true;
                                break;
                            }
                        }
                        if (skip)
                            continue;
                    }
                    result.Add(dll);
                }
            }
            catch (Exception ex)
            {
                CrashLog.Write(ex, "GetDllList");
            }

            return result;
        }
        
        /// <summary>
        /// Returns true if file is not in EXE path or application path
        /// </summary>
        /// <param name="fileName">Full file path</param>
        /// <returns></returns>
        public static bool IsFileInUnknownDirectory(string fileName)
        {
            string dir = Path.GetDirectoryName(fileName);
            if (dir == FileUtil.GetExePath())
            {
                return false;
            }
            if (dir == FileUtil.GetApplicationDataPath())
            {
                return false;
            }
            return true;
        }

        private HashSet<string> GetLookupDirectories()
        {
            HashSet<string> result = new HashSet<string>( );
            result.AddIfNotNull(Directory.GetCurrentDirectory( ));
            result.AddIfNotNull(FileUtil.GetExePath()); //when executing NUnit test cases, this returns null
            result.AddIfNotNull(FileUtil.GetApplicationDataPath());
            return result;
        }
        
        public List<Assembly> GetAssemblies(bool forceReload, bool ignoreSystemAssemblies)
        {
            if (!forceReload && m_assemblyCache != null)
            {
                return m_assemblyCache;
            }
            HashSet<Assembly> result = new HashSet<Assembly>();

            foreach (var name in GetAssemblyFileNames())
            {
                try
                {
                    Assembly asm = Assembly.LoadFrom(name);
                    result.Add(asm);
                }
                catch (BadImageFormatException)
                {
                }
                catch
                {
                }
            }

            foreach (var asm in AppDomain.CurrentDomain.GetAssemblies()){
                result.Add(asm);
            }

            if (ignoreSystemAssemblies)
            {
                foreach (var asm in result.ToArray())
                {
                    if (asm.FullName.StartsWith("System"))
                    {
                        result.Remove(asm);
                    }
                    if (asm.FullName.StartsWith("Microsoft"))
                    {
                        result.Remove(asm);
                    }
                    if (asm.FullName.StartsWith("mscorlib"))
                    {
                        result.Remove(asm);
                    }
                    if (asm.FullName.StartsWith("vshost"))
                    {
                        result.Remove(asm);
                    }
                }
            }

            m_assemblyCache = new List<Assembly>(result);
            return m_assemblyCache;
        }

        internal bool IsInMyAssembly(Type type)
        {
            var myAssemblies = GetAssemblies(false, true);
            if (type.IsGenericType)
            {
                foreach (var genType in type.GetGenericArguments())
                {
                    if (myAssemblies.Contains(genType.Assembly)){
                        return true;
                    }
                }
            }

            return (myAssemblies.Contains(type.Assembly));
        }

        public Type GetType(string name)
        {
            foreach (var a  in m_assemblyCache)
            {
                Type result = a.GetType(name, false);
                if (result != null)
                {
                    return result;
                }
            }
            return Type.GetType(name, true, false);
        }

        public List<Type> GetDerivedTypes(Type baseType)
        {
            //Console.WriteLine(string.Format("GetDerivedTypes {0}", baseType.FullName));
            var assemblies = GetAssemblies(false, true);
            HashSet<Type> result = new HashSet<Type>();
            foreach(var a in assemblies){
				try {
	                foreach (var type in (from t in a.GetTypes() where t.IsSubclassOf(baseType) select t))
	                {
	                    if (type.IsAbstract || type.IsInterface)
	                    {
	                        continue;
	                    }
	                    result.Add(type);
	                }
	                foreach (var type in (from t in a.GetTypes() where t.GetInterfaces().Contains(baseType) select t))
	                {
	                    if (type.IsAbstract || type.IsInterface)
	                    {
	                        continue;
	                    }
	                    result.Add(type);
	                }
				} catch (ReflectionTypeLoadException){
					// happens in Mono
				} catch {
				}
            }
            return new List<Type>(result);
        }
                
        /// <summary>
        /// Returns assembly version string
        /// </summary>
        /// <param name="objectFromAssembly">Pass any object of a class in the assembly. For example, 'this'</param>
        /// <returns></returns>
        [PermissionSetAttribute(SecurityAction.LinkDemand, Name="FullTrust")]
        public static string GetAssemblyVersion(object objectFromAssembly)
        {
            return GetAssemblyVersion(objectFromAssembly.GetType().Assembly);
        }

        [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
        public static string GetAssemblyVersion(Assembly assembly)
        {
            var fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            return fvi.FileVersion;
        }

        public static object RunStaticMethod(Type type, string strMethod, params object[] aobjParams)
        {
            BindingFlags eFlags =
             BindingFlags.Static | BindingFlags.Public |
             BindingFlags.NonPublic;
            return RunMethod(type, null, strMethod, eFlags, aobjParams);
        } //end of method

        /// <summary>
        /// Run public or private method on object instance
        /// </summary>
        /// <param name="objInstance">Object instance</param>
        /// <param name="strMethod">Method name</param>
        /// <param name="aobjParams">Method parameters</param>
        /// <returns></returns>
        public static object RunInstanceMethod(object objInstance, string strMethod, params object[] aobjParams)
        {
            BindingFlags eFlags = BindingFlags.Instance | BindingFlags.Public |
             BindingFlags.NonPublic;
            return RunMethod(objInstance.GetType(), objInstance, strMethod, eFlags, aobjParams);
        } //end of method

        /// <summary>
        /// run an instance method
        /// </summary>
        /// <param name="objInstance">Object instance</param>
        /// <param name="strMethod">Method name</param>
        /// <param name="eFlags">Method binding flags</param>
        /// <param name="aobjParams">Method parameters</param>
        /// <returns>Value returned by the method</returns>
        public static object RunMethod(Type type, object objInstance, string strMethod, BindingFlags eFlags, params object[] aobjParams)
        {
            MethodInfo m;
            try
            {
                m = type.GetMethod(strMethod, eFlags);
                if (m == null)
                {
                    throw new ArgumentException("There is no method '" +
                     strMethod + "' for type '" + type.ToString() + "'.");
                }

                object objRet = m.Invoke(objInstance, aobjParams);
                return objRet;
            }
            catch
            {
                throw;
            }
        }
    }
}
