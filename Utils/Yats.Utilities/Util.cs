/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace yats.Utilities
{
	public static class Util
	{        
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> collection)
        {
            return collection == null || collection.Count() <= 0;
        }

        public static bool IsNullOrEmpty(this StringCollection collection)
        {
            return collection == null || collection.Count <= 0;
        }
        
        public static bool CollectionsEqual<T>(T[] a, T[] b)
        {
			if ((a == null) != (b == null)){
				return false;
			}
			
			if (a == null){
				return true;
			}
					
			if (a.Length != b.Length){
				return false;
			}
			
			for (int i = 0; i < a.Length; i++){
				if (object.Equals(a[i], b[i]) == false)
                {
					return false;
				}
			}
			return true;
		}
		
        public static int Remove<T>(this ICollection<T> collection, ICollection<T> items)
        {
            int result = 0;
            if (collection == null || items == null)
            {
                return result;
            }
            foreach (T item in items)
            {
                if (collection.Remove(item))
                {
                    result++;
                }
            }
            return result;
        }
                
        
        public static void AddIfNotNull<T>(this ICollection<T> collection, T item)
        {
            if (item != null)
            {
                collection.Add(item);
            }
        }

        public static int AddAll<T>(this HashSet<T> set, IEnumerable<T> items)
        {
            int result = 0;
            if (set != null && items != null){
                foreach (var item in items)
                {
                    if (set.Add(item))
                    {
                        result++;
                    }
                }
            }
            return result;
        }

        public static void AddIfNotNull<T>(this ICollection<T> collection,  ICollection<T> items)
        {
            if (items != null)
            {
                foreach (var x in items)
                {
                    collection.Add(x);
                }
            }
        }

        public static void AddIfNotNull<T>(this ICollection<T> collection, IEnumerable<T> items)
        {
            if (items != null)
            {
                foreach (var x in items)
                {
                    collection.Add(x);
                }
            }
        }

        public static void AddIfNotNull<T>(this ICollection<T> collection, params T[] items)
        {
            if (items != null)
            {
                foreach (var x in items)
                {
                    if (x != null)
                    {
                        collection.Add(x);
                    }
                }
            }
        }
                
        public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> items, Func<T, TKey> property)
        {
            return items.GroupBy(property).Select(x => x.First());
        }

        public static T[] Slice<T>(this T[] source, int index, int length)
        {
            T[] slice = new T[length];
            Array.Copy(source, index, slice, 0, length);
            return slice;
        }
	}
}

