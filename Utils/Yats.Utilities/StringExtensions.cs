﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Text.RegularExpressions;
using System;

namespace yats.Utilities
{
    public static class StringExtensions
    {
        public static string ReplaceControlCharacters(this string text, string replaceWith)
        {
            if (string.IsNullOrEmpty(text))
            {
                return text;
            }

            return Regex.Replace(text,
              @"\p{Cc}",
              " "
                //a=>string.Format("[{0:X2}]", (byte)a.Value[0])
            );
        }

        /// <summary>
        /// Converts e.g. a\nb to a[13]b
        /// </summary>
        /// <param name="text">Input</param>
        /// <returns></returns>
        public static string ControlCharactersToHex(this string text)
        {
            return text.FormatControlCharacters(@"\x{0:X2}");
        }

        /// <summary>
        /// Replace control characters using a given format
        /// </summary>
        /// <param name="text">Input</param>
        /// <param name="format">E.g. "[{0:X2}]"</param>
        /// <returns></returns>
        public static string FormatControlCharacters(this string text, string format)
        {
            if (string.IsNullOrEmpty(text))
            {
                return text;
            }

            return Regex.Replace(text,
              @"\p{Cc}",
              a=>string.Format(format, (byte)a.Value[0])
            );
        }

        public static string RemoveControlCharacters(this string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return text;
            }

            return Regex.Replace(text,
              @"\p{Cc}",
              ""
            );
        }

        public static bool ContainsControlCharacters(this string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return false;
            }

            return text.RemoveControlCharacters() != text;
        }

        /// <summary>
        /// Given a string with mixed newlines (e.g. \n while running in Windows), 
        /// replaces them with current system new line sequence.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string NormalizeNewLines(this string text)
        {
            if(string.IsNullOrEmpty( text ))
            {
                return text;
            }

            return Regex.Replace( text, @"\r\n?|\n", System.Environment.NewLine );
        }

        private static string[] s_emptyArray = new string[0];
        private static char[] s_newLineChars = new char[] { '\n', '\r' };
        public static string[] SplitByNewLines(this string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return s_emptyArray;
            }
            return text.Split(s_newLineChars, System.StringSplitOptions.RemoveEmptyEntries);
        }

        private static Random s_random = new Random();
        private const string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        
        public static string RandomAlphanumericString(int size)
        {
            char[] buffer = new char[size];

            for (int i = 0; i < size; i++)
            {
                buffer[i] = _chars[s_random.Next(_chars.Length)];
            }
            return new string(buffer);
        }
    }
}
