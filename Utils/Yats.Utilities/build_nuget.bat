@echo off 

SET /P ANSWER=Did you update the change log (Y/N)? 
if /i {%ANSWER%}=={y} (goto :yes) 
if /i {%ANSWER%}=={yes} (goto :yes) 
goto :no 
:yes 
..\..\nuget pack Yats.Utilities.csproj -IncludeReferencedProjects -Prop Configuration=Release
pause
exit /b 0 

:no 
notepad Yats.Utilities.nuspec
goto :yes

