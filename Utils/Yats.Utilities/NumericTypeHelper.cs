﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.IO;

namespace yats.Utilities
{
    //http://stackoverflow.com/questions/124411/using-net-how-can-i-determine-if-a-type-is-a-numeric-valuetype
    
    public static class NumericTypeHelper
    {
        /// <summary>
        /// Determines if a type is numeric.  Nullable numeric types are considered numeric.
        /// </summary>
        /// <remarks>
        /// Boolean is not considered numeric.
        /// </remarks>
        public static bool IsNumericType(Type type, out decimal min, out decimal max)
        {
            min = max = 0;
            if (type == null)
            {
                return false;
            }
            if (type.IsEnum)
            {
                return false;
            }
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Byte:
                    min = Byte.MinValue;
                    max = Byte.MaxValue;
                    return true;
                case TypeCode.Decimal:
                    min = Decimal.MinValue;
                    max = Decimal.MaxValue;
                    return true;
                /*case TypeCode.Double:
                    min = Double.MinValue;
                    max = Double.MaxValue;
                    return true;
                 */
                case TypeCode.Int16:
                    min = Int16.MinValue;
                    max = Int16.MaxValue;
                    return true;
                case TypeCode.Int32:
                    min = Int32.MinValue;
                    max = Int32.MaxValue;
                    return true;
                case TypeCode.Int64:
                    min = Int64.MinValue;
                    max = Int64.MaxValue;
                    return true;
                case TypeCode.SByte:
                    min = SByte.MinValue;
                    max = SByte.MaxValue;
                    return true;
                /*case TypeCode.Single:
                    min = Single.MinValue;
                    max = Single.MaxValue;
                    return true;
                */
                case TypeCode.UInt16:
                    min = UInt16.MinValue;
                    max = UInt16.MaxValue;
                    return true;
                case TypeCode.UInt32:
                    min = UInt32.MinValue;
                    max = UInt32.MaxValue;
                    return true;
                case TypeCode.UInt64:
                    min = UInt64.MinValue;
                    max = UInt64.MaxValue;
                    return true;
                case TypeCode.Object:
                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        return IsNumericType(Nullable.GetUnderlyingType(type), out min, out max);
                    }
                    return false;
            }
            return false;
        }

        public static bool IsNumericType(Type type)
        {
            if (type == null)
            {
                return false;
            }

            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Byte:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.SByte:
                case TypeCode.Single:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                    return true;
                case TypeCode.Object:
                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        return IsNumericType(Nullable.GetUnderlyingType(type));
                    }
                    return false;
            }
            return false;
        }

        public static bool TryConvertToByteArray(object value, out byte[] result)
        {
            result = null;
            Type type = value.GetType();
            TypeCode typeCode = Type.GetTypeCode(type);
            switch (typeCode)
            {
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                    break;
                default:
                    return false;
            }

            MemoryStream ms = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(ms);
            switch (typeCode)
            {
                case TypeCode.Byte:
                    writer.Write((byte)value);
                    break;
                case TypeCode.Int16:
                    writer.Write((short)value);
                    break;
                case TypeCode.Int32:
                    writer.Write((int)value);
                    break;
                case TypeCode.Int64:
                    writer.Write((long)value);
                    break;
                case TypeCode.SByte:
                    writer.Write((sbyte)value);
                    break;
                case TypeCode.UInt16:
                    writer.Write((ushort)value);
                    break;
                case TypeCode.UInt32:
                    writer.Write((uint)value);
                    break;
                case TypeCode.UInt64:
                    writer.Write((ulong)value);
                    break;
            }
            result = ms.ToArray();
            return true;
        }
    }
}
