﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace yats.Utilities
{
    [XmlRoot( "dictionary" )]
    [Serializable]
    public class SerializableDictionary<TKey, TValue>
        : Dictionary<TKey, TValue>, IXmlSerializable
    {
        #region IXmlSerializable Members
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            XmlSerializer keySerializer = SerializationHelper.GetSerializer(typeof(TKey));
            XmlSerializer valueSerializer = SerializationHelper.GetSerializer(typeof(TValue));

            bool wasEmpty = reader.IsEmptyElement;
            reader.Read( );

            if(wasEmpty)
                return;

            while(reader.NodeType != System.Xml.XmlNodeType.EndElement)
            {
                reader.ReadStartElement( "item" );

                reader.ReadStartElement( "key" );
                TKey key = (TKey)keySerializer.Deserialize( reader );
                reader.ReadEndElement( );

                reader.ReadStartElement( "value" );
                TValue value = (TValue)valueSerializer.Deserialize( reader );
                reader.ReadEndElement( );

                this.Add( key, value );
                reader.ReadEndElement( );
                reader.MoveToContent( );
            }
            reader.ReadEndElement( );
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            XmlSerializer keySerializer = SerializationHelper.GetSerializer(typeof(TKey));
            XmlSerializer valueSerializer = SerializationHelper.GetSerializer(typeof(TValue));

            foreach(TKey key in this.Keys)
            {
                writer.WriteStartElement( "item" );

                writer.WriteStartElement( "key" );

                keySerializer.Serialize( writer, key );

                writer.WriteEndElement( );

                writer.WriteStartElement( "value" );

                TValue value = this[key];

                valueSerializer.Serialize( writer, value );

                writer.WriteEndElement( );
                writer.WriteEndElement( );
            }
        }
        #endregion

        public static SerializableDictionary<TKey, TValue> ReadString(string serializedDictionary)
        {
            if (string.IsNullOrEmpty(serializedDictionary) == false)
            {
                XmlSerializer serializer = yats.Utilities.SerializationHelper.GetSerializer(typeof(SerializableDictionary<TKey, TValue>));
                StringReader reader = new StringReader(serializedDictionary);
                try
                {
                    return (SerializableDictionary<TKey, TValue>)serializer.Deserialize(reader);
                }
                catch (Exception ex)
                {
                    CrashLog.Write(ex);
                }
            }
            return new SerializableDictionary<TKey,TValue>();
        }

        public string WriteString()
        {
            XmlSerializer serializer = yats.Utilities.SerializationHelper.GetSerializer(this.GetType());
            StringWriter writer = new StringWriter();
            try
            {
                serializer.Serialize(writer, this);
                return writer.ToString();
            }
            catch (Exception ex) {
                CrashLog.Write(ex);
            }
            return null;
        }
    }
}