﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace yats.Utilities
{
    public static class DictionaryExtensions
    {
        public static bool RemoveKeys<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, IEnumerable<TKey> keys)
        {
            bool result = false;
            foreach (var key in keys)
            {
                result |= (dictionary.Remove(key));
            }
            return result;
        }

        public static List<TKey> SelectKeys<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, Predicate<TValue> f)
        {
            List<TKey> result = new List<TKey>();
            foreach (var pair in dictionary)
            {
                KeyValuePair<TKey, TValue> tmp = pair;
                if (f(tmp.Value))
                {
                    result.Add(tmp.Key);
                }
            }
            return result;
        }
    }
}
