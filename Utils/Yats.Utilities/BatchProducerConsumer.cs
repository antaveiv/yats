﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace yats.Utilities
{
    //http://www.albahari.com/threading/part4.aspx#%5FWait%5Fand%5FPulse
    //http://stackoverflow.com/questions/1656404/c-sharp-producer-consumer
    //Buffers enqueued items and consumes them in batches. Single or multiple batch consumer threads are created
    public class BatchProducerConsumer<T> : IProducerConsumer<T> where T:class //It may look like it should inherit ProducerConsumer, but the differences are substantial.. Don't do it
    {
        protected readonly object mLockObject = new object();
        protected Dictionary<ConsumerThread, Thread> workers;
        protected Queue<T> taskQ = new Queue<T>();
        protected System.Threading.Timer timeoutTimer;
        protected System.Threading.Timer forceConsumptionTimer;
        protected int mTimeoutMs;
        /// <summary>
        /// Set / get maximum consumer batch size. 0 - unlimited
        /// </summary>
        public int MaxBatchSize { get; set; }
        /// <summary>
        /// Consume items with a single new thread, default priority - ThreadPriority.Normal
        /// </summary>
        /// <param name="timeoutMs">Consume the items when the given Enqueue timeout passes</param>
        /// <param name="forceConsumptionMs">Force consumption by set period even if items are enqueued in between</param>
        /// <param name="consumptionAction">Action to perform when consuming items</param>
        public BatchProducerConsumer(int timeoutMs, int forceConsumptionMs, Action<T[]> consumptionAction):this(timeoutMs, forceConsumptionMs, ThreadPriority.Normal, 1, consumptionAction)
        {
        }

        
        /// <summary>
        /// Consume items with a single new thread, default priority - ThreadPriority.Normal
        /// </summary>
        /// <param name="timeoutMs">Consume the items when the given Enqueue timeout passes</param>
        /// <param name="forceConsumptionMs">Force consumption by set period even if items are enqueued in between</param>
        /// <param name="threadPriority">Consumer thread priority</param>
        /// <param name="consumptionAction">Action to perform when consuming items</param>
        public BatchProducerConsumer(int timeoutMs, int forceConsumptionMs, ThreadPriority threadPriority, Action<T[]> consumptionAction) : this(timeoutMs, forceConsumptionMs, threadPriority, 1, consumptionAction)
        {
        }


        /// <summary>
        /// Consume items with multiple threads
        /// </summary>
        /// <param name="timeoutMs">Consume the items when the given Enqueue timeout passes</param>
        /// <param name="forceConsumptionMs">Force consumption by set period even if items are enqueued in between</param>
        /// <param name="threadPriority">Consumer thread priority</param>
        /// <param name="workerCount">Number of consumer threads</param>
        /// <param name="consumptionAction">Action to perform when consuming items</param>
        public BatchProducerConsumer(int timeoutMs, int forceConsumptionMs, ThreadPriority threadPriority, int workerCount, Action<T[]> consumptionAction)
        {
            mTimeoutMs = timeoutMs;
            workers = new Dictionary<ConsumerThread, Thread>(workerCount);
            // Create and start a separate thread for each worker
            for (int i = 0; i < workerCount; i++)
            {
                var worker = new ConsumerThread(this, consumptionAction);
                Thread thread = new Thread(worker.Consume);
                thread.Priority = threadPriority;
                if (workerCount > 1)
                {
                    thread.Name = "BatchConsumer<" + typeof(T).Name + "> #" + i;
                }
                else
                {
                    thread.Name = "BatchConsumer<" + typeof(T).Name + ">";
                }
                thread.Start();
                workers.Add(worker, thread);
            }

            timeoutTimer = new System.Threading.Timer(_ =>
            {
                lock(mLockObject){
                    Monitor.PulseAll(mLockObject);
                }
            }, null, timeoutMs, Timeout.Infinite);

            forceConsumptionTimer = new System.Threading.Timer(_ =>
            {
                lock (mLockObject)
                {
                    //timeoutTimer.Change(timeoutMs, timeoutMs);
                    Monitor.PulseAll(mLockObject);
                }
            }, null, forceConsumptionMs, forceConsumptionMs);
        }

        private bool _disposed = false;
        private bool _disposing = false;
        
        ~BatchProducerConsumer() 
        { 
            Dispose(false); 
        }
        
        /// <summary>
        /// Stop worker threads
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            // Use SupressFinalize in case a subclass 
            // of this type implements a finalizer.
            GC.SuppressFinalize(this);
        }
        
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _disposing = true;
                    timeoutTimer.Dispose();
                    forceConsumptionTimer.Dispose();
                    // Enqueue one null task per worker to make each exit.
                    foreach (var worker in workers.Keys)
                    {
                        worker.Stop = true;
                    }
                    lock (mLockObject)
                    {
                        Monitor.PulseAll(mLockObject);
                    }
                    foreach (var thread in workers.Values)
                    {
                        thread.Join();
                    }
                }

                // Indicate that the instance has been disposed.
                _disposed = true;
            }
        }

        /// <summary>
        /// Enqueue a given task. It will be given to an available worker thread
        /// </summary>
        /// <param name="task"></param>
        public void EnqueueTask(T task)
        {
            if (_disposed || _disposing)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
            lock (mLockObject)
            {
                taskQ.Enqueue(task);
                timeoutTimer.Change(mTimeoutMs, Timeout.Infinite);
            }
        }

        /// <summary>
        /// Force consumption of tasks in queue
        /// </summary>
        public void Flush()
        {
            lock (mLockObject)
            {
                timeoutTimer.Change(mTimeoutMs, Timeout.Infinite);
                Monitor.PulseAll(mLockObject);
            }
        }

        protected class ConsumerThread
        {
            public bool Stop = false;
            private BatchProducerConsumer<T> mOwner;
            private Action<T[]> mConsumptionAction;
            public ConsumerThread(BatchProducerConsumer<T> owner, Action<T[]> consumptionAction)
            {
                mOwner = owner;
                mConsumptionAction = consumptionAction;
            }

            public void Consume()
            {
                while (!Stop)
                {                    
                    lock (mOwner.mLockObject)
                    {
                        while (mOwner.taskQ.Count == 0)
                        {
                            Monitor.Wait(mOwner.mLockObject);
                            if (Stop)
                            {
                                return;
                            }
                        }
                        List<T> tasks = new List<T>(mOwner.taskQ.Count);
                        while (mOwner.taskQ.Count > 0 && (mOwner.MaxBatchSize == 0 || tasks.Count < mOwner.MaxBatchSize))
                        {
                            tasks.Add(mOwner.taskQ.Dequeue());
                        }
                        mConsumptionAction(tasks.ToArray());
                    }                    
                }
            }
        }

        public int Count
        {
            get { return taskQ.Count; }
        }
    }
}
