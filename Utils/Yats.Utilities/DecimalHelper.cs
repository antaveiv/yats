﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace yats.Utilities
{
    public static class DecimalHelper
    {
        public static Decimal ParseString(string value, int numBits, bool signed,  System.Globalization.NumberStyles numberStyles)
        {
            switch (numBits)
            {
                case 8:
                    if (signed)
                    {
                        return new Decimal(sbyte.Parse(value, numberStyles));
                    }
                    return new Decimal(byte.Parse(value, numberStyles));
                case 16:
                    if (signed)
                    {
                        return new Decimal(short.Parse(value, numberStyles));
                    }
                    return new Decimal(ushort.Parse(value, numberStyles));
                case 32:
                    if (signed)
                    {
                        return new Decimal(int.Parse(value, numberStyles));
                    }
                    return new Decimal(uint.Parse(value, numberStyles));
                case 64:
                    if (signed)
                    {
                        return new Decimal(long.Parse(value, numberStyles));
                    }
                    return new Decimal(ulong.Parse(value, numberStyles));
            }
            throw new Exception("Unsupported number of bits");
        }

        private static readonly char[] lookup = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        public static string ToHexString(this decimal dec, int numBits, bool signed)
        {
            switch (numBits)
            {
                case 8:
                    if (signed)
                    {
                        return Decimal.ToSByte(dec).ToString("X");
                    }
                    return Decimal.ToByte(dec).ToString("X");
                case 16:
                    if (signed)
                    {
                        return Decimal.ToInt16(dec).ToString("X");
                    }
                    return Decimal.ToUInt16(dec).ToString("X");
                case 32:
                    if (signed)
                    {
                        return Decimal.ToInt32(dec).ToString("X");
                    }
                    return Decimal.ToUInt32(dec).ToString("X");
                case 64:
                    if (signed)
                    {
                        return Decimal.ToInt64(dec).ToString("X");
                    }
                    return Decimal.ToUInt64(dec).ToString("X");
            }

            if (dec == 0)
            {
                return "0";
            }

            throw new NotImplementedException();
        }

        public static Decimal TwosComplement(this Decimal dec, int numBits)
        {
            // Decimal class does not support bit shift. Neither does Math.Pow
            Decimal shifted = 1;
            for (int i = 0; i < numBits; i++)
            {
                shifted *= 2;
            }

            dec = (dec + shifted) % shifted;
            return dec;
        }
    }
}
