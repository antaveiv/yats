﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace yats.Utilities
{
    public interface IProducerConsumer<T>: IDisposable 
    {
        void EnqueueTask(T task);
        int Count { get; }
    }
}
