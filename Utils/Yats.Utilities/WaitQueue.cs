﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace yats.Utilities
{
    public class WaitQueue<T> where T : class
    {
        object sync = new object();
        Queue<T> queue;
        
        public WaitQueue()
        {
            queue = new Queue<T>();
        }

        public WaitQueue( int capacity)
        {
            queue = new Queue<T>(capacity);
        }

        public void Enqueue(T item)
        {
            lock (sync)
            {
                queue.Enqueue(item);
                Monitor.PulseAll(sync);
            }            
        }

        public T Dequeue()
        {
            T task;
            lock (sync)
            {
                while (queue.Count == 0)
                {
                    Monitor.Wait(sync);
                }
                task = queue.Dequeue();
            }
            return task;
        }
    }
}
