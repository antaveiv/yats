﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yats.Utilities;
using NUnit.Framework;
using System.Globalization;

namespace Yats.Utilities.UnitTests
{
    [TestFixture]
    public class ByteArrayTests
    {
        [Test]
        public void IsNullOrEmpty()
        {
            byte [] b = null;
            Assert.IsTrue(b.IsNullOrEmpty());
            b = new byte[]{};
            Assert.IsTrue(b.IsNullOrEmpty());

            Assert.IsFalse(new byte[] {1}.IsNullOrEmpty());
            Assert.IsTrue(new byte[] { 1, 2 }.IsNullOrEmpty(0, 0));
            Assert.IsFalse(new byte[] { 1, 2 }.IsNullOrEmpty(0, 1));
            Assert.IsFalse(new byte[] { 1, 2 }.IsNullOrEmpty(0, 2));
            Assert.IsTrue(new byte[] { 1, 2 }.IsNullOrEmpty(2, 0));
            Assert.IsTrue(new byte[] { 1, 2 }.IsNullOrEmpty(2, 2));
        }

        [Test]
        public static void ToCArrayFull()
        {
            byte[] rawData = {
	            0x6F, 0x67, 0x72, 0x61, 0x6D, 0x44, 0x61, 0x74, 0x61, 0x5C, 0x4D, 0x69,
	            0x63, 0x72, 0x6F, 0x73, 0x6F, 0x66, 0x74, 0x5C, 0x57, 0x69, 0x6E, 0x64,
	            0x6F, 0x77, 0x73, 0x5C, 0x53, 0x74, 0x61, 0x72, 0x74, 0x20, 0x4D, 0x65,
	            0x6E
            };

            string expected = "const uint8_t rawData[37] = {" + Environment.NewLine +
    "    0x6F, 0x67, 0x72, 0x61, 0x6D, 0x44, 0x61, 0x74, 0x61, 0x5C, 0x4D, 0x69, 0x63, 0x72, 0x6F, 0x73," + Environment.NewLine +
	"    0x6F, 0x66, 0x74, 0x5C, 0x57, 0x69, 0x6E, 0x64, 0x6F, 0x77, 0x73, 0x5C, 0x53, 0x74, 0x61, 0x72," + Environment.NewLine +
    "    0x74, 0x20, 0x4D, 0x65, 0x6E" + Environment.NewLine +
    "};";

            var result = ByteArray.ToCArrayFull(rawData);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public static void ToCSharpArrayFull()
        {
            byte[] rawData = {
	            0x6F, 0x67, 0x72, 0x61, 0x6D, 0x44, 0x61, 0x74, 0x61, 0x5C, 0x4D, 0x69,
	            0x63, 0x72, 0x6F, 0x73, 0x6F, 0x66, 0x74, 0x5C, 0x57, 0x69, 0x6E, 0x64,
	            0x6F, 0x77, 0x73, 0x5C, 0x53, 0x74, 0x61, 0x72, 0x74, 0x20, 0x4D, 0x65,
	            0x6E
            };

            string expected = "byte[] rawData = {" + Environment.NewLine +
    "    0x6F, 0x67, 0x72, 0x61, 0x6D, 0x44, 0x61, 0x74, 0x61, 0x5C, 0x4D, 0x69, 0x63, 0x72, 0x6F, 0x73," + Environment.NewLine +
    "    0x6F, 0x66, 0x74, 0x5C, 0x57, 0x69, 0x6E, 0x64, 0x6F, 0x77, 0x73, 0x5C, 0x53, 0x74, 0x61, 0x72," + Environment.NewLine +
    "    0x74, 0x20, 0x4D, 0x65, 0x6E" + Environment.NewLine +
    "};";
            
            var result = ByteArray.ToCSharpArrayFull(rawData);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public static void ToJavaArrayFull()
        {
            byte[] rawData = {
	            0x6F, 0x67, 0x72, 0x61, 0x6D, 0x44, 0x61, 0x74, 0x61, 0x5C, 0x4D, 0x69,
	            0x63, 0x72, 0x6F, 0x73, 0x6F, 0x66, 0x74, 0x5C, 0x57, 0x69, 0x6E, 0x64,
	            0x6F, 0x77, 0x73, 0x5C, 0x53, 0x74, 0x61, 0x72, 0x74, 0x20, 0x4D, 0x65,
	            0x6E
            };

            string expected = "byte rawData[] = {" + Environment.NewLine +
                "    (byte)0x6F, (byte)0x67, (byte)0x72, (byte)0x61, (byte)0x6D, (byte)0x44, (byte)0x61, (byte)0x74, (byte)0x61, (byte)0x5C, (byte)0x4D, (byte)0x69, (byte)0x63, (byte)0x72, (byte)0x6F, (byte)0x73," + Environment.NewLine +
                "    (byte)0x6F, (byte)0x66, (byte)0x74, (byte)0x5C, (byte)0x57, (byte)0x69, (byte)0x6E, (byte)0x64, (byte)0x6F, (byte)0x77, (byte)0x73, (byte)0x5C, (byte)0x53, (byte)0x74, (byte)0x61, (byte)0x72," + Environment.NewLine +
                "    (byte)0x74, (byte)0x20, (byte)0x4D, (byte)0x65, (byte)0x6E" + Environment.NewLine +
            "};";
            
            var result = ByteArray.ToJavaArrayFull(rawData);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public static void ToJavaArrayFull2()
        {
            byte[] rawData = {
	            0x6F, 0x67, 0x72, 0x61
            };

            string expected = "byte rawData[] = {(byte)0x6F, (byte)0x67, (byte)0x72, (byte)0x61};";

            var result = ByteArray.ToJavaArrayFull(rawData);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public static void StringToInts()
        {
            string test = null;
            Assert.Catch(typeof(ArgumentNullException), () => { ByteArray.StringToInts(test, new string[] { }); });

            test = "";
            int[] res = ByteArray.StringToInts(test, " ");
            Assert.IsTrue(Util.CollectionsEqual(res, new int[] { }));

            test = "123";
            res = ByteArray.StringToInts(test, " ");
            Assert.IsTrue(Util.CollectionsEqual(res, new int[] { 123 }));

            test = "123,3";
            res = ByteArray.StringToInts(test, " ", ",");
            Assert.IsTrue(Util.CollectionsEqual(res, new int[] { 123, 3 }));
        }

        [Test]
        public static void StringToFloats()
        {
            string test = null;
            Assert.Catch(typeof(ArgumentNullException), () => { ByteArray.StringToFloats(test, CultureInfo.InvariantCulture, " "); });
            Assert.Catch(typeof(ArgumentException), () => { ByteArray.StringToFloats(test, CultureInfo.InvariantCulture); });
            test = "  ";
            float[] res = ByteArray.StringToFloats(test, CultureInfo.InvariantCulture, " ", ",");
            Assert.IsTrue(Util.CollectionsEqual(res, new float[] {}));

            test = "123.1, 0.1, -3";
            res = ByteArray.StringToFloats(test, CultureInfo.InvariantCulture, " ", ",");
            Assert.IsTrue(Util.CollectionsEqual(res, new float[] { 123.1f, 0.1f, -3 }));
            
            test = "123.1; 0.1\t-3";
            res = ByteArray.StringToFloats(test, CultureInfo.InvariantCulture, " ", ",", ";", "\t");
            Assert.IsTrue(Util.CollectionsEqual(res, new float[] { 123.1f, 0.1f, -3 }));
        }

        [Test]
        public static void DecimalToByteArray()
        {
            decimal x = 0;

            for (int numBytes = 1; numBytes <= 8; numBytes++)
            {
                Assert.AreEqual(x, x.ToByteArray(numBytes, true, true).InterpretByteArray(true, true));
                Assert.AreEqual(x, x.ToByteArray(numBytes, true, false).InterpretByteArray(true, false));
                Assert.AreEqual(x, x.ToByteArray(numBytes, false, true).InterpretByteArray(false, true));
                Assert.AreEqual(x, x.ToByteArray(numBytes, false, false).InterpretByteArray(false, false));
            }


            x = 127;
            for (int numBytes = 1; numBytes <= 8; numBytes++)
            {
                Assert.AreEqual(x, x.ToByteArray(numBytes, true, true).InterpretByteArray(true, true));
                Assert.AreEqual(x, x.ToByteArray(numBytes, true, false).InterpretByteArray(true, false));
                Assert.AreEqual(x, x.ToByteArray(numBytes, false, true).InterpretByteArray(false, true));
                Assert.AreEqual(x, x.ToByteArray(numBytes, false, false).InterpretByteArray(false, false));
            }

            x = 128;
            for (int numBytes = 2; numBytes <= 8; numBytes++)
            {
                Assert.AreEqual(x, x.ToByteArray(numBytes, true, true).InterpretByteArray(true, true));
                Assert.AreEqual(x, x.ToByteArray(numBytes, true, false).InterpretByteArray(true, false));
                Assert.AreEqual(x, x.ToByteArray(numBytes, false, true).InterpretByteArray(false, true));
                Assert.AreEqual(x, x.ToByteArray(numBytes, false, false).InterpretByteArray(false, false));
            }

            x = 1024;
            for (int numBytes = 2; numBytes <= 8; numBytes++)
            {
                Assert.AreEqual(x, x.ToByteArray(numBytes, true, true).InterpretByteArray(true, true));
                Assert.AreEqual(x, x.ToByteArray(numBytes, true, false).InterpretByteArray(true, false));
                Assert.AreEqual(x, x.ToByteArray(numBytes, false, true).InterpretByteArray(false, true));
                Assert.AreEqual(x, x.ToByteArray(numBytes, false, false).InterpretByteArray(false, false));
            }

            x = 450244178033;//there is exactly nothing special about this number
            for (int numBytes = 5; numBytes <= 8; numBytes++)
            {
                Assert.AreEqual(x, x.ToByteArray(numBytes, true, true).InterpretByteArray(true, true));
                Assert.AreEqual(x, x.ToByteArray(numBytes, true, false).InterpretByteArray(true, false));
                Assert.AreEqual(x, x.ToByteArray(numBytes, false, true).InterpretByteArray(false, true));
                Assert.AreEqual(x, x.ToByteArray(numBytes, false, false).InterpretByteArray(false, false));
            }

            x = -127;
            for (int numBytes = 1; numBytes <= 8; numBytes++)
            {
                Assert.AreEqual(x, x.ToByteArray(numBytes, true, true).InterpretByteArray(true, true));
                Assert.AreEqual(x, x.ToByteArray(numBytes, true, false).InterpretByteArray(true, false));
            }

            x = -128;
            for (int numBytes = 1; numBytes <= 8; numBytes++)
            {
                Assert.AreEqual(x, x.ToByteArray(numBytes, true, true).InterpretByteArray(true, true));
                Assert.AreEqual(x, x.ToByteArray(numBytes, true, false).InterpretByteArray(true, false));
            }
        }

        [Test]
        public static void HexStringToBytes1()
        {
            string test;
            test = "";
            byte[] res = ByteArray.HexStringToBytes(test);
            Assert.IsTrue(Util.CollectionsEqual(res, new byte[] { }));

            test = "C1";
            res = ByteArray.HexStringToBytes(test);
            Assert.IsTrue(Util.CollectionsEqual(res, new byte[] {0xC1}));

            test = " C1\t0x44";
            res = ByteArray.HexStringToBytes(test);
            Assert.IsTrue(Util.CollectionsEqual(res, new byte[] {0xC1, 0x44 }));

            test = "\"C1440300\"";
            res = ByteArray.HexStringToBytes(test);
            Assert.IsTrue(Util.CollectionsEqual(res, new byte[] { 0xC1, 0x44, 0x03, 0x00 }));
            
            test = @"0x01, 0x44
            ";
            res = ByteArray.HexStringToBytes(test);
            Assert.IsTrue(Util.CollectionsEqual(res, new byte[] { 1, 0x44 }));

            test = "{C1}{44}";
            res = ByteArray.HexStringToBytes(test);
            Assert.IsTrue(Util.CollectionsEqual(res, new byte[] { 0xC1, 0x44 }));

            test = "{0xC1, 0x44}";
            res = ByteArray.HexStringToBytes(test);
            Assert.IsTrue(Util.CollectionsEqual(res, new byte[] { 0xC1, 0x44 }));

            test = @" 1, 44 0x30 
5            ";
            res = ByteArray.HexStringToBytes(test);
            Assert.IsTrue(Util.CollectionsEqual(res, new byte[] { 1, 0x44, 0x30, 5 }));
        }
                
        [Test]
        public static void DecimalStringToBytes()
        {
            string test;
            test = "";
            byte[] res = ByteArray.DecimalStringToBytes(test);
            Assert.IsTrue(Util.CollectionsEqual(res, new byte[] { }));

            test = "1";
            res = ByteArray.DecimalStringToBytes(test);
            Assert.IsTrue(Util.CollectionsEqual(res, new byte[] { 0x01 }));

            test = " 211\t44";
            res = ByteArray.DecimalStringToBytes(test);
            Assert.IsTrue(Util.CollectionsEqual(res, new byte[] { 211, 44 }));

            test = "\"255,255,255.0\"";
            res = ByteArray.DecimalStringToBytes(test);
            Assert.IsTrue(Util.CollectionsEqual(res, new byte[] { 255,255,255,0 }));

            test = @"01, 02 65
            ";
            res = ByteArray.DecimalStringToBytes(test);
            Assert.IsTrue(Util.CollectionsEqual(res, new byte[] { 1,2,65 }));

            test = "{51}{44}";
            res = ByteArray.DecimalStringToBytes(test);
            Assert.IsTrue(Util.CollectionsEqual(res, new byte[] { 51, 44 }));

            test = @"221,0,0,0,8,224,77,6,0
5            ";
            res = ByteArray.DecimalStringToBytes(test);
            Assert.IsTrue(Util.CollectionsEqual(res, new byte[] { 221, 0, 0, 0, 8, 224, 77, 6, 0, 5 }));
        }
    }
}
