﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yats.Utilities;

namespace Yats.Utilities.UnitTests
{
    [TestFixture]
    public class LRUListTests
    {
        [Test]
        public void DoesNotExceedMax()
        {
            LRUList<string> list = new LRUList<string>(3);
            list.Add("A");
            list.Add("B");
            list.Add("C");
            list.Add("D");
            Assert.AreEqual(3, list.Items.Count);
        }

        [Test]
        public void InsertSame()
        {
            LRUList<string> list = new LRUList<string>(3);
            list.Add("A");
            list.Add("B");
            list.Add("A");
            list.Add("C");
            list.Add("D");
            list.Add("A");
            list.Add("C");
            list.Add("D");
            list.Add("F");
            list.Add("G");
            list.Add("A");
            Assert.AreEqual(3, list.Items.Count);
            Assert.AreEqual("A", list.Items[0]);
            Assert.AreEqual("G", list.Items[1]);
            Assert.AreEqual("F", list.Items[2]);
            
        }

        private class X
        {
            public int A;
            public string B;

            public X(int a, string b)
            {
                this.A = a;
                this.B = b;
            }

            public override bool Equals(object obj)
            {
                X other = obj as X;
                if (other == null)
                {
                    return false;
                }
                return this.A == other.A && this.B == other.B;
            }

            public override int GetHashCode()
            {
                return A.GetHashCode() ^ B.GetHashCode();
            }
        }

        [Test]
        public void DoesNotExceedMax2()
        {
            LRUList<X> list = new LRUList<X>(3);
            list.Add(new X(1, "1"));
            list.Add(new X(1, "2"));
            list.Add(new X(1, "3"));
            list.Add(new X(1, "4"));
            list.Add(new X(1, "5"));
            Assert.AreEqual(3, list.Items.Count);
        }

        [Test]
        public void ZeroSize()
        {
            LRUList<string> list = new LRUList<string>( 0 );
            list.Add( "A" );
            list.Add( "B" );
            list.Add( "C" );
            list.Add( "D" );
            Assert.AreEqual( 0, list.Items.Count );
        }

        [Test]
        public void InitWithLargerList()
        {
            List<string> list = new List<string>( );
            list.Add( "A" );
            list.Add( "B" );
            list.Add( "C" );
            list.Add( "D" );
            LRUList<string> lru = new LRUList<string>( 2, list );
            Assert.AreEqual( 2, lru.Items.Count );
            Assert.AreEqual( "A", lru.Items[0] );
            Assert.AreEqual( "B", lru.Items[1] );
        }
    }
}
