﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using yats.Utilities;

namespace Yats.Utilities.UnitTests
{
    [TestFixture]
    public class TimeSpanExtensionTests
    {        
        [Test]
        public static void ToShortFriendlyDisplay2Test()
        {
            Assert.AreEqual("0ms", TimeSpan.FromMilliseconds(0).ToShortFriendlyDisplay2(2));
            Assert.AreEqual("1s 10ms", TimeSpan.FromMilliseconds(1010).ToShortFriendlyDisplay2(2));
            Assert.AreEqual("1h 0m", new TimeSpan(1,0,1).ToShortFriendlyDisplay2(2));
            Assert.AreEqual("1h", new TimeSpan(1, 0, 1).ToShortFriendlyDisplay2(1));
            Assert.AreEqual("1h 2m 3s", new TimeSpan(1, 2, 3).ToShortFriendlyDisplay2(3));
        }
    }
}
