﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yats.Utilities;
using NUnit.Framework;
using yats.Utilities.Commands;

namespace Yats.Utilities.UnitTests
{
    [TestFixture]
    public class CommandUnitTests
    {
        [Test]
        public static void TestSimpleCommand()
        {
            CommandStack commandStack = new CommandStack();
            commandStack.Execute(null);
            Assert.IsFalse(commandStack.CanUndo);
            Assert.IsFalse(commandStack.CanRedo);
            int i = 0;
            commandStack.Execute(new SimpleCommand(() => { i = 1; return true; }, () => { i = 2; return true; }));
            Assert.IsTrue(commandStack.CanUndo);
            Assert.IsFalse(commandStack.CanRedo);
            Assert.AreEqual(1, i);
            commandStack.Undo();
            Assert.IsFalse(commandStack.CanUndo);
            Assert.IsTrue(commandStack.CanRedo);
            Assert.AreEqual(2, i);            
        }

        private class DummyCommand : SimpleCommand
        {
            public DummyCommand()
                : base(() => { return true; }, () => { return true; }) 
            { 
            }
        }

        private class NoCommand : SimpleCommand
        {
            public NoCommand()
                : base(() => { return false; }, () => { return false; })
            {
            }
        }

        [Test]
        public static void TestModifiedChanged()
        {
            ICommand cmd = new DummyCommand();
            CommandStack commandStack = new CommandStack();
            bool modified = commandStack.Modified;
            commandStack.OnModifiedChange += (sender, e) => { modified = e.IsModified; };
            Assert.IsFalse(modified);
            Assert.IsNull(commandStack.GetLastExecutedCommand()); 
            commandStack.Execute(cmd);
            Assert.IsTrue(modified);
            Assert.IsTrue(object.ReferenceEquals(cmd, commandStack.GetLastExecutedCommand()));
            commandStack.Clear();
            Assert.IsFalse(modified);
            Assert.IsNull(commandStack.GetLastExecutedCommand());
            commandStack.Execute(cmd);
            commandStack.Execute(cmd);
            Assert.AreEqual(2, commandStack.GetUndoCommands().Count);
            Assert.IsTrue(modified);
            commandStack.Undo();
            Assert.IsTrue(modified);
            commandStack.Undo();
            Assert.IsFalse(modified);
            commandStack.Redo();
            Assert.IsTrue(modified);
            commandStack.Redo();
            Assert.IsTrue(modified);
            commandStack.UndoUntil(null, true);
            Assert.IsFalse(modified);
            commandStack.Redo();
            commandStack.SetSavedStatus();
            Assert.IsFalse(modified);
            commandStack.Undo();
            Assert.IsTrue(modified);
            commandStack.Redo();
            Assert.IsFalse(modified);
            commandStack.Redo();
            Assert.IsTrue(modified);
        }

        [Test]
        public static void TestCanUndo()
        {
            ICommand cmd = new DummyCommand();
            ICommand noCmd = new NoCommand();
            CommandStack queue = new CommandStack();
            queue.Execute(noCmd);
            Assert.IsFalse(queue.CanUndo);
            Assert.IsFalse(queue.CanRedo);
            queue.Execute(cmd);
            Assert.IsTrue(queue.CanUndo);
            Assert.IsFalse(queue.CanRedo);
            queue.Clear();
            Assert.IsFalse(queue.CanUndo);
            Assert.IsFalse(queue.CanRedo);
            queue.Execute(cmd);
            queue.Execute(cmd);
            Assert.IsTrue(queue.CanUndo); 
            queue.Undo();
            queue.SetSavedStatus(); 
            Assert.IsTrue(queue.CanUndo);
            Assert.IsTrue(queue.CanRedo); 
            queue.UndoUntil(null, true);
            Assert.IsFalse(queue.CanUndo);
            Assert.IsTrue(queue.CanRedo);
            Assert.IsFalse(queue.Undo());
            queue.UndoUntil(null, false);
            Assert.IsFalse(queue.CanUndo);
            Assert.IsFalse(queue.CanRedo);            
        }

        [Test]
        public static void TestClearQueue()
        {
            ICommand cmd = new DummyCommand();
            ICommand cmd2 = new DummyCommand();
            CommandStack commandStack = new CommandStack();
            commandStack.Execute(cmd);
            commandStack.Execute(cmd);
            commandStack.Execute(cmd);
            Assert.IsTrue(commandStack.Undo());
            Assert.IsTrue(commandStack.Undo());
            Assert.IsTrue(commandStack.Execute(cmd2));
            Assert.IsTrue(commandStack.CanUndo);
            Assert.IsFalse(commandStack.CanRedo);
            while (commandStack.Redo()) ;
            Assert.AreEqual(2, commandStack.GetUndoCommands().Count);
            Assert.IsTrue(commandStack.UndoUntil(null, true));
            Assert.IsFalse(commandStack.CanUndo);
            Assert.IsTrue(commandStack.CanRedo);
            while (commandStack.Redo()) ;
            Assert.IsTrue(commandStack.UndoUntil(null, false));
            Assert.IsFalse(commandStack.CanUndo);
            Assert.IsFalse(commandStack.CanRedo);
            Assert.IsEmpty(commandStack.GetUndoCommands());
            commandStack.Execute(cmd);
            commandStack.Execute(cmd);
            commandStack.Execute(cmd2);
            Assert.IsTrue(commandStack.UndoUntil(cmd, true));
            Assert.AreEqual(2, commandStack.GetUndoCommands().Count);
            Assert.IsTrue(commandStack.CanUndo);
            Assert.IsTrue(commandStack.CanRedo);
            
        }
    }
}
