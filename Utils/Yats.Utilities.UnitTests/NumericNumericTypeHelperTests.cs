﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yats.Utilities;

namespace Yats.Utilities.UnitTests
{
    [TestFixture]
    public class NumericNumericTypeHelperTests
    {
        /// <summary>
        /// Tests the IsNumericType method.
        /// </summary>
        [Test]
        public void IsNumericTypeTest()
        {
            // non numeric types
            Assert.IsFalse(NumericTypeHelper.IsNumericType(null));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(object)));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(DBNull)));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(bool)));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(char)));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(DateTime)));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(string)));

            // arrays of numeric and non-numeric types
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(object[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(DBNull[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(bool[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(char[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(DateTime[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(string[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(byte[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(decimal[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(double[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(Int16[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(Int32[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(Int64[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(SByte[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(Single[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(UInt16[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(UInt32[])));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(UInt64[])));

            // numeric types
            Assert.True(NumericTypeHelper.IsNumericType(typeof(byte)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(decimal)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(double)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(Int16)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(Int32)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(Int64)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(SByte)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(Single)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(UInt16)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(UInt32)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(UInt64)));

            // nullable non-numeric types
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(bool?)));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(char?)));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(typeof(DateTime?)));

            // nullable numeric types
            Assert.True(NumericTypeHelper.IsNumericType(typeof(byte?)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(decimal?)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(double?)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(Int16?)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(Int32?)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(Int64?)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(SByte?)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(Single?)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(UInt16?)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(UInt32?)));
            Assert.True(NumericTypeHelper.IsNumericType(typeof(UInt64?)));

            // testing with GetType because of handling with non-numerics.  see:
            // http://msdn.microsoft.com/en-us/library/ms366789.aspx

            // using gettype - non-numeric
            Assert.IsFalse(NumericTypeHelper.IsNumericType((new object()).GetType()));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(DBNull.Value.GetType()));
            Assert.IsFalse(NumericTypeHelper.IsNumericType(true.GetType()));
            Assert.IsFalse(NumericTypeHelper.IsNumericType('a'.GetType()));
            Assert.IsFalse(NumericTypeHelper.IsNumericType((new DateTime(2009, 1, 1)).GetType()));
            Assert.IsFalse(NumericTypeHelper.IsNumericType("".GetType()));

            // using gettype - numeric types
            // ReSharper disable RedundantCast
            Assert.True(NumericTypeHelper.IsNumericType((new byte()).GetType()));
            Assert.True(NumericTypeHelper.IsNumericType((43.2m).GetType()));
            Assert.True(NumericTypeHelper.IsNumericType((43.2d).GetType()));
            Assert.True(NumericTypeHelper.IsNumericType(((Int16)2).GetType()));
            Assert.True(NumericTypeHelper.IsNumericType(((Int32)2).GetType()));
            Assert.True(NumericTypeHelper.IsNumericType(((Int64)2).GetType()));
            Assert.True(NumericTypeHelper.IsNumericType(((SByte)2).GetType()));
            Assert.True(NumericTypeHelper.IsNumericType(2f.GetType()));
            Assert.True(NumericTypeHelper.IsNumericType(((UInt16)2).GetType()));
            Assert.True(NumericTypeHelper.IsNumericType(((UInt32)2).GetType()));
            Assert.True(NumericTypeHelper.IsNumericType(((UInt64)2).GetType()));
            // ReSharper restore RedundantCast

            // using gettype - nullable non-numeric types
            bool? nullableBool = true;
            Assert.IsFalse(NumericTypeHelper.IsNumericType(nullableBool.GetType()));
            char? nullableChar = ' ';
            Assert.IsFalse(NumericTypeHelper.IsNumericType(nullableChar.GetType()));
            DateTime? nullableDateTime = new DateTime(2009, 1, 1);
            Assert.IsFalse(NumericTypeHelper.IsNumericType(nullableDateTime.GetType()));

            // using getttype - nullable numeric types
            byte? nullableByte = 12;
            Assert.True(NumericTypeHelper.IsNumericType(nullableByte.GetType()));
            decimal? nullableDecimal = 12.2m;
            Assert.True(NumericTypeHelper.IsNumericType(nullableDecimal.GetType()));
            double? nullableDouble = 12.32;
            Assert.True(NumericTypeHelper.IsNumericType(nullableDouble.GetType()));
            Int16? nullableInt16 = 12;
            Assert.True(NumericTypeHelper.IsNumericType(nullableInt16.GetType()));
            Int16? nullableInt32 = 12;
            Assert.True(NumericTypeHelper.IsNumericType(nullableInt32.GetType()));
            Int16? nullableInt64 = 12;
            Assert.True(NumericTypeHelper.IsNumericType(nullableInt64.GetType()));
            SByte? nullableSByte = 12;
            Assert.True(NumericTypeHelper.IsNumericType(nullableSByte.GetType()));
            Single? nullableSingle = 3.2f;
            Assert.True(NumericTypeHelper.IsNumericType(nullableSingle.GetType()));
            UInt16? nullableUInt16 = 12;
            Assert.True(NumericTypeHelper.IsNumericType(nullableUInt16.GetType()));
            UInt16? nullableUInt32 = 12;
            Assert.True(NumericTypeHelper.IsNumericType(nullableUInt32.GetType()));
            UInt16? nullableUInt64 = 12;
            Assert.True(NumericTypeHelper.IsNumericType(nullableUInt64.GetType()));

        }
    }
}
