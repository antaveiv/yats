﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using yats.Utilities;

namespace Yats.Utilities.UnitTests
{
    [TestFixture]
    public class ProducerConsumerTests
    {
        [Test]
        public static void BatchProducerConsumerTimeouts()
        {
            //List<string> received = new List<string>();
            int numEvents = 0;
            using (BatchProducerConsumer<string> pc = new BatchProducerConsumer<string>(100, 5000, (x) => { numEvents++; }))
            {
                var thread = new Thread(() =>
                {
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            Thread.Sleep(200);
                            pc.EnqueueTask(i.ToString());
                        }
                    }
                });

                thread.Start();
                thread.Join();
                Thread.Sleep(200);
                Assert.AreEqual(5, numEvents);
            }
        }

        [Test]
        public static void BatchProducerConsumerForceConsume()
        {
            //List<string> received = new List<string>();
            int numEvents = 0;
            int numStrings = 0;
            using (BatchProducerConsumer<string> pc = new BatchProducerConsumer<string>(500, 1100, (x) => { numEvents++; numStrings += x.Length; }))
            {
                var thread = new Thread(() =>
                {
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            Thread.Sleep(200);
                            pc.EnqueueTask(i.ToString());
                        }
                    }
                });

                thread.Start();
                thread.Join();
                Assert.AreEqual(0, numEvents);//enqueued items kept deferring the procesing. should not be done yet
                Assert.AreEqual(0, numStrings);
                Thread.Sleep(200);
                Assert.AreEqual(1, numEvents); // Force timer should have happened here
                Assert.AreEqual(5, numStrings);
                Thread.Sleep(1000);
                Assert.AreEqual(1, numEvents); // no more updates
                Assert.AreEqual(5, numStrings);
            }
        }

        [Test]
        public static void BatchProducerConsumerBatchSize()
        {
            int numEvents = 0;
            int numStrings = 0;
            List<int> counts=new List<int>();
            using (BatchProducerConsumer<string> pc = new BatchProducerConsumer<string>(10, 10, (x) => {
                //Debug.WriteLine("num strings {0}", x.Length);
                numEvents++; numStrings += x.Length;
                counts.Add(x.Length);
            }))
            {
                pc.MaxBatchSize = 5;
                var thread = new Thread(() =>
                {
                        for (int i = 0; i < 1000; i++)
                        {
                            pc.EnqueueTask(i.ToString());
                        }
                });

                thread.Start();
                thread.Join();
                Thread.Sleep(10);
            }
            //checking after consumer is disposed, this ensures all worker threads are joined
            Assert.IsTrue((numEvents >= 200) && (numEvents <= 201), "numEvents: {0}, processed counts: {1}", numEvents, string.Join(", ", counts));
            Assert.AreEqual(1000, numStrings);
        }

        [Test]
        public static void ProducerConsumerDisposeBug()
        {
            ProducerConsumer<string> pc = new ProducerConsumer<string>((s) => { Thread.Sleep(200); });
            pc.EnqueueTask("x");
            Thread.Sleep(10);
            pc.Dispose();
        } 
    }
}
