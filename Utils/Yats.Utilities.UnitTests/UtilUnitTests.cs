﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yats.Utilities;
using NUnit.Framework;

namespace Yats.Utilities.UnitTests
{
    [TestFixture]
    public class UtilUnitTests
    {
        [Test]
        public void ListFromItem()
        {
            string x = null;
            var list = x.ToSingleItemList();
            Assert.NotNull(list);
            Assert.AreEqual(1, list.Count);
            Assert.IsNull(list[0]);
            x = "1";
            list = x.ToSingleItemList();
            Assert.NotNull(list);
            Assert.AreEqual(1, list.Count);
            Assert.IsNotNull(list[0]);
            Assert.AreEqual("1", list[0]);            
        }

        [Test]
        public void TestCollectionsEqual()
        {
            X[] a1 = { new X(3), new X(4), new X(5), };
            X[] a2 = { new X(3), new X(4), new X(5), };
            X[] a3 = null;
            X[] a4 = { };
            X[] a5 = { new X(3), new X(4), new X(4), };
            X[] a6 = { };
            Assert.IsTrue(Util.CollectionsEqual(a1, a2));
            Assert.IsFalse(Util.CollectionsEqual(a1, a3));
            Assert.IsFalse(Util.CollectionsEqual(a1, a4));
            Assert.IsFalse(Util.CollectionsEqual(a1, a5));
            Assert.IsFalse(Util.CollectionsEqual(a2, a3));
            Assert.IsFalse(Util.CollectionsEqual(a2, a4));
            Assert.IsFalse(Util.CollectionsEqual(a2, a5));
            Assert.IsTrue(Util.CollectionsEqual(a3, a3));
            Assert.IsFalse(Util.CollectionsEqual(a3, a4));
            Assert.IsFalse(Util.CollectionsEqual(a3, a5));
            Assert.IsTrue(Util.CollectionsEqual(a4, a6));
            Assert.IsFalse(Util.CollectionsEqual(a4, a5));
        }

        [Test]
        public void TestListsEqual()
        {
            List<X> a1 = ListExtensions.GetList( new X(3), new X(4), new X(5));
            List<X> a2 = ListExtensions.GetList(new X(3), new X(4), new X(5));
            List<X> a3 = null;
            List<X> a4 = ListExtensions.GetList<X>();
            List<X> a5 = ListExtensions.GetList(new X(3), new X(4), new X(4));
            List<X> a6 = ListExtensions.GetList<X>();
            Assert.IsFalse(ListExtensions.ListsEqual(a1, a3));
            Assert.IsFalse(ListExtensions.ListsEqual(a1, a4));
            Assert.IsFalse(ListExtensions.ListsEqual(a1, a5));
            Assert.IsFalse(ListExtensions.ListsEqual(a2, a3));
            Assert.IsFalse(ListExtensions.ListsEqual(a2, a4));
            Assert.IsFalse(ListExtensions.ListsEqual(a2, a5));
            Assert.IsTrue(ListExtensions.ListsEqual(a3, a3));
            Assert.IsFalse(ListExtensions.ListsEqual(a3, a4));
            Assert.IsFalse(ListExtensions.ListsEqual(a3, a5));
            Assert.IsTrue(ListExtensions.ListsEqual(a4, a6));
            Assert.IsFalse(ListExtensions.ListsEqual(a4, a5));
        }

        private class X 
        {
            public int x;
            public X(int x) { this.x = x; }
            public override bool Equals(object obj)
            {
                X other = obj as X;
                if ((this == null) != (other == null))
                {
                    return false;
                }

                if (other == null)
                {
                    return true;
                }
                return x == other.x;
            }
            public override int GetHashCode()
            {
                return x;
            }
        }
    }
}
