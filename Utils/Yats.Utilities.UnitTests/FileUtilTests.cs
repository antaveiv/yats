﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using yats.Utilities;

namespace Yats.Utilities.UnitTests
{
    [TestFixture]
    public class FileUtilTests
    {
        [Test]
        public void FixPathSeparator()
        {

            if (PlatformHelper.IsWindows())
            {
                string path = @"c:\program files\test_this\bb.exe";
                Assert.AreEqual(@"c:\program files\test_this\bb.exe", FileUtil.FixPathSeparator(path));

                path = @"../tmp/test.txt";
                Assert.AreEqual(@"..\tmp\test.txt", FileUtil.FixPathSeparator(path));

            }
        }

        [TestCase(@"c:\foo", @"c:", ExpectedResult = true)]
        [TestCase(@"c:\foo", @"c:\", ExpectedResult = true)]
        [TestCase(@"c:\foo", @"c:\foo", ExpectedResult = true)]
        [TestCase(@"c:\foo", @"c:\foo\", ExpectedResult = true)]
        [TestCase(@"c:\foo\", @"c:\foo", ExpectedResult = true)]
        [TestCase(@"c:\foo\bar\", @"c:\foo\", ExpectedResult = true)]
        [TestCase(@"c:\foo\bar", @"c:\foo\", ExpectedResult = true)]
        [TestCase(@"c:\foo\a.txt", @"c:\foo", ExpectedResult = true)]
        [TestCase(@"c:\FOO\a.txt", @"c:\foo", ExpectedResult = true)]
        [TestCase(@"c:/foo/a.txt", @"c:\foo", ExpectedResult = true)]
        [TestCase(@"c:\foobar", @"c:\foo", ExpectedResult = false)]
        [TestCase(@"c:\foobar\a.txt", @"c:\foo", ExpectedResult = false)]
        [TestCase(@"c:\foobar\a.txt", @"c:\foo\", ExpectedResult = false)]
        [TestCase(@"c:\foo\a.txt", @"c:\foobar", ExpectedResult = false)]
        [TestCase(@"c:\foo\a.txt", @"c:\foobar\", ExpectedResult = false)]
        [TestCase(@"c:\foo\..\bar\baz", @"c:\foo", ExpectedResult = false)]
        [TestCase(@"c:\foo\..\bar\baz", @"c:\bar", ExpectedResult = true)]
        [TestCase(@"c:\foo\..\bar\baz", @"c:\barr", ExpectedResult = false)]
        public bool IsSubPathOfTest(string baseDirPath, string path)
        {
            return path.IsSubPathOf(baseDirPath);
        }
    }
}
