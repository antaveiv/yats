﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using Gui.Winforms.PublicAPI;
using yats.Ports.Properties;

namespace yats.Gui.Winforms.YatsApplication.Configuration
{
    public partial class PortSettingsPanel : SettingsTabPanel
    {
        public PortSettingsPanel()
        {
            InitializeComponent( );
        }

        public override string GetText()
        {
            return "Ports";
        }

        public override void LoadSettings()
        {
            //cbUseWmi.Checked = Settings.Default.UseWmiForSerialPorts;            
            numPortReusePoolTimeoutMs.Value = Settings.Default.PortReusePoolDelay;
            cbUseLog4Net.Checked = Settings.Default.UseLog4Net;
        }

        public override void SaveSettings()
        {
            Settings.Default.PortReusePoolDelay = (int)numPortReusePoolTimeoutMs.Value;
            //Settings.Default.UseWmiForSerialPorts = cbUseWmi.Checked;
            Settings.Default.UseLog4Net = cbUseLog4Net.Checked;
            Settings.Default.Save( );
        }

        public override int GetSortIndex()
        {
            return 500;
        }
    }
}
