﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
using NUnit.Framework;
using System;
using yats.Ports.Utilities.Parsers;
using yats.Utilities;

namespace yats.Ports.UnitTests
{
    /// <summary>
    /// example data from https://en.wikipedia.org/wiki/Consistent_Overhead_Byte_Stuffing
    /// </summary>
    [TestFixture]
    public class CobsParserTests
    {
        [Test()]
        public void TestCaseDecode1()
        {
            var port = new PortEmulator(ByteArray.HexStringToBytes("01 01 00 "));
            port.Open();
            CobsParser parser = new CobsParser();
            Assert.IsTrue(port.Execute(parser).OperationResult == ReadOperationResult.Result.SUCCESS);
            Assert.AreEqual(ByteArray.HexStringToBytes("00 "), parser.Decode());
            port.Close();
        }

        [Test()]
        public void TestCaseDecode2()
        {
            var port = new PortEmulator(ByteArray.HexStringToBytes("03 11 22 02 33 00"));
            port.Open();
            CobsParser parser = new CobsParser();
            Assert.IsTrue(port.Execute(parser).OperationResult == ReadOperationResult.Result.SUCCESS);
            Assert.AreEqual(ByteArray.HexStringToBytes("11 22 00 33"), parser.Decode());
            port.Close();
        }

        [Test()]
        public void TestCaseDecode3()
        {
            var port = new PortEmulator(ByteArray.HexStringToBytes("02 11 01 01 01 00 "));
            port.Open();
            CobsParser parser = new CobsParser();
            Assert.IsTrue(port.Execute(parser).OperationResult == ReadOperationResult.Result.SUCCESS);
            Assert.AreEqual(ByteArray.HexStringToBytes("11 00 00 00"), parser.Decode());
            port.Close();
        }

        [Test()]
        public void TestCaseEncode()
        {
            Assert.AreEqual(ByteArray.HexStringToBytes("01 01 00 "), CobsParser.Encode(ByteArray.HexStringToBytes("00")));
            Assert.AreEqual(ByteArray.HexStringToBytes("01 01 01 00 "), CobsParser.Encode(ByteArray.HexStringToBytes("00 00")));
            Assert.AreEqual(ByteArray.HexStringToBytes("03 11 22 02 33 00 "), CobsParser.Encode(ByteArray.HexStringToBytes("11 22 00 33")));
            Assert.AreEqual(ByteArray.HexStringToBytes("05 11 22 33 44 00 "), CobsParser.Encode(ByteArray.HexStringToBytes("11 22 33 44")));
            Assert.AreEqual(ByteArray.HexStringToBytes("02 11 01 01 01 00 "), CobsParser.Encode(ByteArray.HexStringToBytes("11 00 00 00")));
        }
    }
}
