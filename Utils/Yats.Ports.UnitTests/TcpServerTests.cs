﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Net.Sockets;
using System.Threading;
using NUnit.Framework;
using yats.Ports;
using yats.TestCase.Parameter;
using yats.Ports.Utilities;
using yats.Ports.Utilities.Parsers;

namespace yats.Ports.UnitTests
{
    [TestFixture]
    public class TcpServerTests
    {
        PortSettings portSettings = new PortSettings(new PortSettings.TcpIpServerInfo(22222, 2000));
        [Test]
        public void CancelConnectThenConnect()
        {
            using (var cancelWaitEvent = new AutoResetEvent(false))
            {
                using (var evt = new AutoResetEvent(false)) // used to synchronize a test helper thread
                {
                    new Thread(new ParameterizedThreadStart(CancelingThreadFunction)).Start(evt);
                    AbstractPort port = null;

                    DateTime start = DateTime.Now;
                    evt.Set();// signal the helper thread to advance to the next stage
                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);//test blocks here until cancel thread cancels the Open

                    Assert.LessOrEqual(DateTime.Now.Subtract(start).TotalMilliseconds, 2000); // see that actually returned earlier than given timeout
                    Assert.IsNull(port);// null should be returned instead of an opened port

                    evt.Set();// signal the helper thread to advance to the next stage

                    port = PortOpenHelper.Open(portSettings, cancelWaitEvent);//should succeed - the helper thread will connect
                    Assert.IsNotNull(port);
                    Assert.IsTrue(port.IsOpen());
                    Assert.IsTrue(port is TcpServer);

                    PortReusePool.SetCloseTimeout(port, 10);
                    PortOpenHelper.Close(port, portSettings);// port is not closed immediately, only after PortReusePool.TimeoutMs timeout       
                    Assert.IsTrue(port.IsOpen());
                    Thread.Sleep(100);

                    Assert.That(() => { port.IsOpen(); }, Throws.TypeOf<ObjectDisposedException>());
                   // Assert.IsFalse(port.IsOpen());
                }
            }
        }

        void CancelingThreadFunction(object param)
        {
            EventWaitHandle evt = (EventWaitHandle)param;
            evt.WaitOne();
			Thread.Sleep(200);	
            PortOpenHelper.CancelOpen(portSettings);
            evt.WaitOne();
            Thread.Sleep(200);

            // connect
            var ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            TcpClient newclient = new TcpClient("localhost", 22222);            
        }

        [Test]
        public void NullParameters()
        {
            using (var cancelWaitEvent = new ManualResetEvent(false))
            {
                PortOpenHelper.Open(null, cancelWaitEvent);
                PortOpenHelper.CloseImmediately(null);
#pragma warning disable CS0618 // Type or member is obsolete
                PortOpenHelper.Close(null);
#pragma warning restore CS0618 // Type or member is obsolete
                PortOpenHelper.Close(null, null);
                PortReusePool.SetCloseTimeout(null, 0);
                PortReusePool.CloseImmediately(null);
            }
        }

        [Test]
        public void CommandTimeout()
        {
            using (var barrier = new Barrier(2) )
            using (var waitEvent = new AutoResetEvent(false))
            {
                PortSettings serverPortSettings = new PortSettings(new PortSettings.TcpIpServerInfo(22222, 10000));
                PortSettings clientPortSettings = new PortSettings(new PortSettings.TcpIpClientInfo("localhost", 22222, false));

                new Thread(() => {
                    {
                        Thread.Sleep(200);
                        var client = PortOpenHelper.Open(clientPortSettings, waitEvent);
                        waitEvent.WaitOne();
                        Thread.Sleep(100);
                        client.Write("te");
                        PortOpenHelper.CloseImmediately(client);
                        Assert.Catch(typeof(ObjectDisposedException), () => { client.IsOpen(); }, "Port is not disposed");
                    }
                }).Start();
                AbstractPort connectedClient = null;
                connectedClient = PortOpenHelper.Open(serverPortSettings, waitEvent);//should succeed - the helper thread will connect
                Assert.IsNotNull(connectedClient);
                Assert.IsTrue(connectedClient.IsOpen());
                Assert.IsTrue(connectedClient is TcpServer);
                DateTime start = DateTime.Now;
                waitEvent.Set();
                Assert.AreEqual(ReadOperationResult.Result.TIMEOUT, connectedClient.Execute(200, new WaitForBytes("test"), waitEvent).OperationResult);

                PortOpenHelper.CloseImmediately(connectedClient);
                Assert.Catch(typeof(ObjectDisposedException), () => { connectedClient.IsOpen(); }, "Port is not disposed");
            }
        }


        [Test]
        public void CommandSuccess()
        {
            using (var evt = new ManualResetEvent(false)) // used to synchronize a test helper thread
            {
                using (var cancelWaitEvent = new ManualResetEvent(false))
                {
                    PortSettings serverPortSettings = new PortSettings(new PortSettings.TcpIpServerInfo(22222, 20000));
                    PortSettings clientPortSettings = new PortSettings(new PortSettings.TcpIpClientInfo("localhost", 22222, false));
                    AbstractPort client = null;
                    new Thread(() =>
                    {
                        evt.WaitOne(100);
                        client = PortOpenHelper.Open(clientPortSettings, cancelWaitEvent);
                        evt.WaitOne(3000);
                        Thread.Sleep(100);
                        client.Write("test");
                        PortOpenHelper.CloseImmediately(client);
                        Assert.Catch(typeof(ObjectDisposedException), () => { client.IsOpen(); }, "Port is not disposed");
                    }).Start();

                    AbstractPort connectedClient = PortOpenHelper.Open(serverPortSettings, cancelWaitEvent);//should succeed - the helper thread will connect
                    Assert.IsFalse(object.ReferenceEquals(client, connectedClient)); //BUG: PortOpenHelper only has 1 reference to open port
                    Assert.IsNotNull(connectedClient, "Server port is null");
                    Assert.IsTrue(connectedClient.IsOpen(), "Port is not open");
                    Assert.IsTrue(connectedClient is TcpServer);
                    DateTime start = DateTime.Now;
                                        
                    evt.Set();
                    var res = connectedClient.Execute(1000, new WaitForBytes("test"), cancelWaitEvent).OperationResult;
                    Assert.AreEqual(ReadOperationResult.Result.SUCCESS, res);
                    Assert.Less(DateTime.Now.Subtract(start).TotalMilliseconds, 1000); // see that actually returned earlier than given timeout

                    PortOpenHelper.CloseImmediately(connectedClient);
                    Assert.Catch(typeof(ObjectDisposedException), () => { connectedClient.IsOpen(); }, "Port is not disposed");
                }
            }
        }
    }
}
