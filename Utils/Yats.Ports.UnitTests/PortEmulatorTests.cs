﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Net.Sockets;
using System.Threading;
using NUnit.Framework;
using yats.Ports;
using yats.TestCase.Parameter;
using yats.Ports.Utilities;
using yats.Ports.Utilities.Parsers;
using yats.Utilities;
using System.Collections.Generic;
using System.Text;

namespace yats.Ports.UnitTests
{
    [TestFixture]
    public class PortEmulatorTests
    {
        [Test]
        public void EmulatorOpenClose()
        {
            PortEmulator port = new PortEmulator("test test ");
            port.Open();
            port.Write("test");
            port.Close();
            Assert.That(() => { port.Write("test"); }, Throws.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void EmulatorParser()
        {
            PortEmulator port = new PortEmulator("test\r\ntest\r\n");
            port.Open();
            var parser = new TextLineParser();
            port.Execute(100, parser, null);
            Assert.AreEqual(parser.GetReceivedLine(), "test");
        }

        [Test]
        public void EmulatorReadByte()
        {
            byte[] test = Encoding.ASCII.GetBytes("test\r\ntest\r\n");
            PortEmulator port = new PortEmulator(test);
            port.Open();
#pragma warning disable 0618
            Assert.AreEqual('t', port.ReadByte());
#pragma warning restore 0618
        }

        [Test]
        public void EmulatorReadBytes()
        {
            byte[] test = Encoding.ASCII.GetBytes("test\r\ntest\r\n");
            PortEmulator port = new PortEmulator(test);
            port.Open();
            byte[] read = new byte[100];
#pragma warning disable 0618
            int len = port.Read(read);
#pragma warning restore 0618
            Assert.AreEqual(len, test.Length);
            Assert.IsTrue(ByteArray.Equal(test, read, 0, test.Length));
        }

        [Test]
        public void EmulatorReadWithCancel()
        {
            PortEmulator port = new PortEmulator("test");
            port.Open();
            var parser = new TextLineParser();
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                new Thread(() => { Thread.Sleep(10); cancelWaitEvent.Set(); }).Start();
                Assert.IsTrue(port.Execute(100, parser, cancelWaitEvent).OperationResult == ReadOperationResult.Result.CANCELLED);
            }
        }

        [Test]
        public void EmulatorPermanentParser()
        {
            using (PortEmulator port = new PortEmulator("test\r\ntest\r\ntest\r\ntest"))
            {
                port.Open();
                var parser = new TextLineParser();
                List<string> receivedLines = new List<string>();
                using (var consumer = new ProducerConsumer<TextLineParser>((x) => { receivedLines.Add(x.GetReceivedLine()); }))
                {
                    port.Attach(parser, consumer);
                    Thread.Sleep(100);
                    port.Detach(parser);
                }

                Assert.AreEqual(3, receivedLines.Count);
                Assert.AreEqual("test", receivedLines[2]);
            }
        }
    }
}