﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Net.Sockets;
using System.Threading;
using NUnit.Framework;
using yats.Ports;
using yats.TestCase.Parameter;
using yats.Ports.Utilities;
using yats.Ports.Utilities.Parsers;
using yats.Utilities;
using System.Collections.Generic;
using System.Text;

namespace yats.Ports.UnitTests
{
    [TestFixture]
    public class AbstractPortTests
    {
#if DEBUG
        [Test]
        public void CancelParser()
        {
            PortEmulator port = new PortEmulator("test");
            port.Open();
            var parser = new TextLineParser();
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                new Thread(() => { Thread.Sleep(10); port.Cancel(parser); }).Start();
                Assert.IsTrue(port.Execute(100, parser, cancelWaitEvent).OperationResult == ReadOperationResult.Result.CANCELLED);
                Assert.AreEqual(0, AssemblyHelper.RunInstanceMethod(port, "GetWaitingParserCount"));
            }
        }

        [Test]
        public void CancelAllParsers()
        {
            PortEmulator port = new PortEmulator("test");
            port.Open();
            var parser = new TextLineParser();
            using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
            {
                new Thread(() => { Thread.Sleep(10); port.CancelAllParsers(); }).Start();
                Assert.IsTrue(port.Execute(100, parser, cancelWaitEvent).OperationResult == ReadOperationResult.Result.CANCELLED);
                Assert.AreEqual(0, AssemblyHelper.RunInstanceMethod(port, "GetWaitingParserCount"));
            }
        }
#endif
    }
}
