﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Net.Sockets;
using System.Threading;
using NUnit.Framework;
using yats.Ports;
using yats.TestCase.Parameter;
using yats.Ports.Utilities;
using yats.Ports.Utilities.Parsers;
using yats.Utilities;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework.Constraints;

namespace yats.Ports.UnitTests
{
    // Tests that public methods throw InvalidOperationException when port is closed
    [TestFixture]
    public class CheckObjectClosedThrown
    {
        #region SerialPort
        [Test]
        public void ClosedSerialPortAttach()
        {
            using (var port = new SerialPort())
            {
                using (var consumer = new ProducerConsumer<TextLineParser>((x) => { }))
                {
                    port.Attach(new TextLineParser(), consumer);
                }
            }
        }

        [Test]
        public void ClosedSerialPortCancel()
        {
            using (SerialPort port = new SerialPort())
            {
                port.Cancel(new TextLineParser());
            }
        }

        [Test]
        public void ClosedSerialPortBytesToRead()
        {
            using (SerialPort port = new SerialPort())
            {
                Assert.That(() => { port.BytesToRead(); }, Throws.TypeOf<InvalidOperationException>());
            }
        }

        [Test]
        public void ClosedSerialPortCancelAllParsers()
        {
            using (SerialPort port = new SerialPort())
            {
                port.CancelAllParsers();
            }
        }

        [Test]
        public void ClosedSerialPortClose()
        {
            using (SerialPort port = new SerialPort())
            {
                port.Close();
            }
        }

        [Test]
        public void ClosedSerialPortCommand()
        {
            using (var port = new SerialPort())
            {
                using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
                {
                    using (var consumer = new ProducerConsumer<TextLineParser>((x) => { }))
                    {
                        Assert.That(() => { port.Command("t", new TextLineParser(), 0, cancelWaitEvent); }, Throws.TypeOf<InvalidOperationException>());
                    }
                }
            }
        }

        [Test]
        public void ClosedSerialPortDetach()
        {
            using (SerialPort port = new SerialPort())
            {
                port.Detach(new TextLineParser());
            }
        }

        [Test]
        public void ClosedSerialPortDiscardInBuffer()
        {
            using (SerialPort port = new SerialPort())
            {
                Assert.That(() => { port.DiscardInBuffer(); }, Throws.TypeOf<InvalidOperationException>());
            }
        }

        [Test]
        public void ClosedSerialPortExecute()
        {
            using (var port = new SerialPort())
            {
                using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
                {
                    using (var consumer = new ProducerConsumer<TextLineParser>((x) => { }))
                    {
                        Assert.That(() => { port.Execute(new TextLineParser(), cancelWaitEvent); }, Throws.TypeOf<InvalidOperationException>());
                    }
                }
            }
        }

        [Test]
        public void ClosedSerialPortGetTransmissionTime()
        {
            using (SerialPort port = new SerialPort())
            {
                port.GetTransmissionTime(1);
            }
        }

        [Test]
        public void ClosedSerialPortIsOpen()
        {
            using (SerialPort port = new SerialPort())
            {
                port.IsOpen();
            }
        }

        [Test]
        public void ClosedSerialPortRead()
        {
            using (SerialPort port = new SerialPort())
            {
#pragma warning disable 0618
                Assert.That(() => { port.Read(new byte[] { }); }, Throws.TypeOf<InvalidOperationException>());
#pragma warning restore 0618
            }
        }

        [Test]
        public void ClosedSerialPortReadByte()
        {
            using (SerialPort port = new SerialPort())
            {
#pragma warning disable 0618
                Assert.That(() => { port.ReadByte(); }, Throws.TypeOf<InvalidOperationException>());
#pragma warning restore 0618
            }
        }

        [Test]
        public void ClosedSerialPortWrite()
        {
            using (SerialPort port = new SerialPort())
            {
                Assert.That(() => { port.Write(new byte[] { }); }, Throws.TypeOf<InvalidOperationException>());
            }
        }

        #endregion
        #region BluetoothPort

        [Test]
        public void ClosedBluetoothPortAttach()
        {
            using (var port = new BluetoothPort())
            {
                using (var consumer = new ProducerConsumer<TextLineParser>((x) => { }))
                {
                    port.Attach(new TextLineParser(), consumer);
                }
            }
        }

        [Test]
        public void ClosedBluetoothPortCancel()
        {
            using (var port = new BluetoothPort())
            {
                port.Cancel(new TextLineParser());
            }
        }

        [Test]
        public void ClosedBluetoothPortBytesToRead()
        {
            using (var port = new BluetoothPort())
            {
                Assert.That(() => { port.BytesToRead(); }, Throws.TypeOf<InvalidOperationException>());
            }
        }

        [Test]
        public void ClosedBluetoothPortCancelAllParsers()
        {
            using (var port = new BluetoothPort())
            {
                port.CancelAllParsers();
            }
        }

        [Test]
        public void ClosedBluetoothPortClose()
        {
            using (var port = new BluetoothPort())
            {
                port.Close();
            }
        }

        [Test]
        public void ClosedBluetoothPortCommand()
        {
            using (var port = new BluetoothPort())
            {
#pragma warning disable 0618
                Assert.That(() => { port.Command("t", new TextLineParser(), 0); }, Throws.TypeOf<InvalidOperationException>());
#pragma warning restore 0618                
            }
        }

        [Test]
        public void ClosedBluetoothPortDetach()
        {
            using (var port = new BluetoothPort())
            {
                port.Detach(new TextLineParser());
            }
        }

        [Test]
        public void ClosedBluetoothPortDiscardInBuffer()
        {
            using (var port = new BluetoothPort())
            {
                Assert.That(() => { port.DiscardInBuffer(); }, Throws.TypeOf<InvalidOperationException>()); 
            }
        }

        [Test]
        public void ClosedBluetoothPortExecute()
        {
            using (var port = new BluetoothPort())
            {
                using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
                {
                    Assert.That(() => { port.Execute(new TextLineParser(), cancelWaitEvent); }, Throws.TypeOf<InvalidOperationException>()); 
                }
            }
        }

        [Test]
        public void ClosedBluetoothPortIsOpen()
        {
            using (var port = new BluetoothPort())
            {
                port.IsOpen();
            }
        }

        [Test]
        public void ClosedBluetoothPortRead()
        {
            using (var port = new BluetoothPort())
            {
#pragma warning disable 0618
                Assert.That(() => { port.Read(new byte[] { }); }, Throws.TypeOf<InvalidOperationException>()); 
#pragma warning restore 0618
            }
        }

        [Test]
        public void ClosedBluetoothPortReadByte()
        {
            using (var port = new BluetoothPort())
            {
#pragma warning disable 0618
                Assert.That(() => { port.ReadByte(); }, Throws.TypeOf<InvalidOperationException>()); 
#pragma warning restore 0618
            }
        }

        [Test]
        public void ClosedBluetoothPortWrite()
        {
            using (var port = new BluetoothPort())
            {
                Assert.That(() => { port.Write(new byte[] { }); }, Throws.TypeOf<InvalidOperationException>()); 
            }
        }

        #endregion
        #region PortEmulator

        [Test]
        public void ClosedPortEmulatorAttach()
        {
            using (var port = new PortEmulator())
            {
                using (var consumer = new ProducerConsumer<TextLineParser>((x) => { }))
                {
                    port.Attach(new TextLineParser(), consumer);
                }
            }
        }

        [Test]
        public void ClosedPortEmulatorCancel()
        {
            using (var port = new PortEmulator())
            {
                port.Cancel(new TextLineParser());
            }
        }

        [Test]
        public void ClosedPortEmulatorBytesToRead()
        {
            using (var port = new PortEmulator())
            {
                Assert.That(() => { port.BytesToRead(); }, Throws.TypeOf<InvalidOperationException>()); 
            }
        }

        [Test]
        public void ClosedPortEmulatorCancelAllParsers()
        {
            using (var port = new PortEmulator())
            {
                port.CancelAllParsers();
            }
        }

        [Test]
        public void ClosedPortEmulatorClose()
        {
            using (var port = new PortEmulator())
            {
                port.Close();
            }
        }

        [Test]
        public void ClosedPortEmulatorCommand()
        {
            using (var port = new PortEmulator())
            {
                using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
                {
                    Assert.That(() => { port.Command("t", new TextLineParser(), 0, cancelWaitEvent); }, Throws.TypeOf<InvalidOperationException>()); 
                }
            }
        }

        [Test]
        public void ClosedPortEmulatorDetach()
        {
            using (var port = new PortEmulator())
            {
                port.Detach(new TextLineParser());
            }
        }

        [Test]
        public void ClosedPortEmulatorDiscardInBuffer()
        {
            using (var port = new PortEmulator())
            {
                Assert.That(() => { port.DiscardInBuffer(); }, Throws.TypeOf<InvalidOperationException>()); 
            }
        }

        [Test]
        public void ClosedPortEmulatorExecute()
        {
            using (var port = new PortEmulator())
            {
                using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
                {
                    Assert.That(() => { port.Execute(new TextLineParser(), cancelWaitEvent); }, Throws.TypeOf<InvalidOperationException>());
                }
            }
        }

        [Test]
        public void ClosedPortEmulatorIsOpen()
        {
            using (var port = new PortEmulator())
            {
                port.IsOpen();
            }
        }

        [Test]
        public void ClosedPortEmulatorRead()
        {
            using (var port = new PortEmulator())
            {
#pragma warning disable 0618
                Assert.That(() => { port.Read(new byte[] { }); }, Throws.TypeOf<InvalidOperationException>()); 
#pragma warning restore 0618
            }
        }

        [Test]
        public void ClosedPortEmulatorReadByte()
        {
            using (var port = new PortEmulator())
            {
#pragma warning disable 0618
                Assert.That(() => { port.ReadByte(); }, Throws.TypeOf<InvalidOperationException>()); 
#pragma warning restore 0618
            }
        }

        [Test]
        public void ClosedPortEmulatorWrite()
        {
            using (var port = new PortEmulator())
            {
                Assert.That(() => { port.Write(new byte[] { }); }, Throws.TypeOf<InvalidOperationException>()); 
            }
        }

        [Test]
        public void ClosedPortEmulatorEmulateReceivedData()
        {
            using (var port = new PortEmulator())
            {
                Assert.That(() => { port.EmulateReceivedData(new byte[] { }); }, Throws.TypeOf<InvalidOperationException>()); 
            }
        }

        #endregion
        #region SshPort

        [Test]
        public void ClosedSshPortAttach()
        {
            using (var port = new SshPort())
            {
                using (var consumer = new ProducerConsumer<TextLineParser>((x) => { }))
                {
                    port.Attach(new TextLineParser(), consumer);
                }
            }
        }

        [Test]
        public void ClosedSshPortCancel()
        {
            using (var port = new SshPort())
            {
                port.Cancel(new TextLineParser());
            }
        }

        [Test]
        public void ClosedSshPortBytesToRead()
        {
            using (var port = new SshPort())
            {
                Assert.That(() => { port.BytesToRead(); }, Throws.TypeOf<InvalidOperationException>()); 
            }
        }

        [Test]
        public void ClosedSshPortCancelAllParsers()
        {
            using (var port = new SshPort())
            {
                port.CancelAllParsers();
            }
        }

        [Test]
        public void ClosedSshPortClose()
        {
            using (var port = new SshPort())
            {
                port.Close();
            }
        }

        [Test]
        public void ClosedSshPortCommand()
        {
            using (var port = new SshPort())
            {
                using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
                {
                    Assert.That(() => { port.Command("t", new TextLineParser(), 0, cancelWaitEvent); }, Throws.TypeOf<InvalidOperationException>()); 
                }
            }
        }

        [Test]
        public void ClosedSshPortDetach()
        {
            using (var port = new SshPort())
            {
                port.Detach(new TextLineParser());
            }
        }

        [Test]
        public void ClosedSshPortDiscardInBuffer()
        {
            using (var port = new SshPort())
            {
                Assert.That(() => { port.DiscardInBuffer(); }, Throws.TypeOf<InvalidOperationException>()); 
            }
        }

        [Test]
        public void ClosedSshPortExecute()
        {
            using (var port = new SshPort())
            {
                using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
                {
                    Assert.That(() => { port.Execute(new TextLineParser(), cancelWaitEvent); }, Throws.TypeOf<InvalidOperationException>()); 
                }
            }
        }

        [Test]
        public void ClosedSshPortIsOpen()
        {
            using (var port = new SshPort())
            {
                port.IsOpen();
            }
        }

        [Test]
        public void ClosedSshPortRead()
        {
            using (var port = new SshPort())
            {
#pragma warning disable 0618
                Assert.That(() => { port.Read(new byte[] { }); }, Throws.TypeOf<InvalidOperationException>()); 
#pragma warning restore 0618
            }
        }

        [Test]
        public void ClosedSshPortReadByte()
        {
            using (var port = new SshPort())
            {
#pragma warning disable 0618
                Assert.That(() => { port.ReadByte(); }, Throws.TypeOf<InvalidOperationException>()); 
#pragma warning restore 0618
            }
        }

        [Test]
        public void ClosedSshPortWrite()
        {
            using (var port = new SshPort())
            {
                Assert.That(() => { port.Write(new byte[] { }); }, Throws.TypeOf<InvalidOperationException>());
            }
        }

        #endregion
        #region TcpIpClientSocket

        [Test]
        public void ClosedTcpIpClientSocketAttach()
        {
            using (var port = new TcpIpClientSocket(80))
            {
                using (var consumer = new ProducerConsumer<TextLineParser>((x) => { }))
                {
                    port.Attach(new TextLineParser(), consumer);
                }
            }
        }

        [Test]
        public void ClosedTcpIpClientSocketCancel()
        {
            using (var port = new TcpIpClientSocket(80))
            {
                port.Cancel(new TextLineParser());
            }
        }

        [Test]
        public void ClosedTcpIpClientSocketBytesToRead()
        {
            using (var port = new TcpIpClientSocket(80))
            {
                Assert.That(() => { port.BytesToRead(); }, Throws.TypeOf<InvalidOperationException>()); 
            }
        }
        
        [Test]
        public void ClosedTcpIpClientSocketCancelAllParsers()
        {
            using (var port = new TcpIpClientSocket(80))
            {
                port.CancelAllParsers();
            }
        }

        [Test]
        public void ClosedTcpIpClientSocketClose()
        {
            using (var port = new TcpIpClientSocket(80))
            {
                port.Close();
            }
        }

        [Test]
        public void ClosedTcpIpClientSocketCommand()
        {
            using (var port = new TcpIpClientSocket(80))
            {
                using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
                {
                    Assert.That(() => { port.Command("t", new TextLineParser(), 0, cancelWaitEvent); }, Throws.TypeOf<InvalidOperationException>()); 
                }
            }
        }

        [Test]
        public void ClosedTcpIpClientSocketDetach()
        {
            using (var port = new TcpIpClientSocket(80))
            {
                port.Detach(new TextLineParser());
            }
        }

        [Test]
        public void ClosedTcpIpClientSocketDiscardInBuffer()
        {
            using (var port = new TcpIpClientSocket(80))
            {
                Assert.That(() => { port.DiscardInBuffer(); }, Throws.TypeOf<InvalidOperationException>()); 
            }
        }

        [Test]
        public void ClosedTcpIpClientSocketExecute()
        {
            using (var port = new TcpIpClientSocket(80))
            {
                using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
                {
                    Assert.That(() => { port.Execute(new TextLineParser(), cancelWaitEvent); }, Throws.TypeOf<InvalidOperationException>()); 
                }
            }
        }

        [Test]
        public void ClosedTcpIpClientSocketIsOpen()
        {
            using (var port = new TcpIpClientSocket(80))
            {
                port.IsOpen();
            }
        }

        [Test]
        public void ClosedTcpIpClientSocketRead()
        {
            using (var port = new TcpIpClientSocket(80))
            {
#pragma warning disable 0618
                Assert.That(() => { port.Read(new byte[] { }); }, Throws.TypeOf<InvalidOperationException>()); 
#pragma warning restore 0618
            }
        }

        [Test]
        public void ClosedTcpIpClientSocketReadByte()
        {
            using (var port = new TcpIpClientSocket(80))
            {
#pragma warning disable 0618
                Assert.That(() => { port.ReadByte(); }, Throws.TypeOf<InvalidOperationException>()); 
#pragma warning restore 0618
            }
        }

        [Test]
        public void ClosedTcpIpClientSocketWrite()
        {
            using (var port = new TcpIpClientSocket(80))
            {
                Assert.That(() => { port.Write(new byte[] { }); }, Throws.TypeOf<InvalidOperationException>()); 
            }
        }

        #endregion
        #region TcpServer

        [Test]
        public void ClosedTcpServerAttach()
        {
            using (var port = new TcpServer(80))
            {
                using (var consumer = new ProducerConsumer<TextLineParser>((x) => { }))
                {
                    port.Attach(new TextLineParser(), consumer);
                }
            }
        }

        [Test]
        public void ClosedTcpServerCancel()
        {
            using (var port = new TcpServer(80))
            {
                port.Cancel(new TextLineParser());
            }
        }

        [Test]
        public void ClosedTcpServerBytesToRead()
        {
            using (var port = new TcpServer(80))
            {
                Assert.That(() => { port.BytesToRead(); }, Throws.TypeOf<InvalidOperationException>()); 
            }
        }

        [Test]
        public void ClosedTcpServerCancelAllParsers()
        {
            using (var port = new TcpServer(80))
            {
                port.CancelAllParsers();
            }
        }

        [Test]
        public void ClosedTcpServerClose()
        {
            using (var port = new TcpServer(80))
            {
                port.Close();
            }
        }

        [Test]
        public void ClosedTcpServerCommand()
        {
            using (var port = new TcpServer(80))
            {
                using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
                {
                    Assert.That(() => { port.Command("t", new TextLineParser(), 0, cancelWaitEvent); }, Throws.TypeOf<InvalidOperationException>()); 
                }
            }
        }

        [Test]
        public void ClosedTcpServerDetach()
        {
            using (var port = new TcpServer(80))
            {
                port.Detach(new TextLineParser());
            }
        }

        [Test]
        public void ClosedTcpServerDiscardInBuffer()
        {
            using (var port = new TcpServer(80))
            {
                Assert.That(() => { port.DiscardInBuffer(); }, Throws.TypeOf<InvalidOperationException>()); 
            }
        }

        [Test]
        public void ClosedTcpServerExecute()
        {
            using (var port = new TcpServer(80))
            {
                using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
                {
                    Assert.That(() => { port.Execute(new TextLineParser(), cancelWaitEvent); }, Throws.TypeOf<InvalidOperationException>()); 
                }
            }
        }

        [Test]
        public void ClosedTcpServerIsOpen()
        {
            using (var port = new TcpServer(80))
            {
                port.IsOpen();
            }
        }

        [Test]
        public void ClosedTcpServerOpen()
        {
            using (var port = new TcpServer(50555))
            {
                port.Open();
            }
        }

        [Test]
        public void ClosedTcpServerRead()
        {
            using (var port = new TcpServer(80))
            {
#pragma warning disable 0618
                Assert.That(() => { port.Read(new byte[] { }); }, Throws.TypeOf<InvalidOperationException>());
#pragma warning restore 0618
            }
        }

        [Test]
        public void ClosedTcpServerReadByte()
        {
            using (var port = new TcpServer(80))
            {
#pragma warning disable 0618
                Assert.That(() => { port.ReadByte(); }, Throws.TypeOf<InvalidOperationException>()); 
#pragma warning restore 0618
            }
        }

        [Test]
        public void ClosedTcpServerStartListening()
        {
            using (var port = new TcpServer(50555))
            {
                port.StartListening();
            }
        }

        [Test]
        public void ClosedTcpServerWrite()
        {
            using (var port = new TcpServer(80))
            {
                Assert.That(() => { port.Write(new byte[] { }); }, Throws.TypeOf<InvalidOperationException>()); 
            }
        }

        #endregion
        #region UdpSocket

        [Test]
        public void ClosedUdpSocketAttach()
        {
            using (var port = new UdpSocketBroadcast())
            {
                using (var consumer = new ProducerConsumer<TextLineParser>((x) => { }))
                {
                    port.Attach(new TextLineParser(), consumer);
                }
            }
        }

        [Test]
        public void ClosedUdpSocketCancel()
        {
            using (var port = new UdpSocketBroadcast())
            {
                port.Cancel(new TextLineParser());
            }
        }

        [Test]
        public void ClosedUdpSocketBytesToRead()
        {
            using (var port = new UdpSocketBroadcast())
            {
                Assert.That(() => { port.BytesToRead(); }, Throws.TypeOf<InvalidOperationException>());
            }
        }

        [Test]
        public void ClosedUdpSocketCancelAllParsers()
        {
            using (var port = new UdpSocketBroadcast())
            {
                port.CancelAllParsers();
            }
        }

        [Test]
        public void ClosedUdpSocketClose()
        {
            using (var port = new UdpSocketBroadcast())
            {
                port.Close();
            }
        }

        [Test]
        public void ClosedUdpSocketCommand()
        {
            using (var port = new UdpSocketBroadcast())
            {
                using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
                {
                    Assert.That(() => { port.Command("t", new TextLineParser(), 0, cancelWaitEvent); }, Throws.TypeOf<InvalidOperationException>());
                }
            }
        }

        [Test]
        public void ClosedUdpSocketDetach()
        {
            using (var port = new UdpSocketBroadcast())
            {
                port.Detach(new TextLineParser());
            }
        }

        [Test]
        public void ClosedUdpSocketDiscardInBuffer()
        {
            using (var port = new UdpSocketBroadcast())
            {
                Assert.That(() => { port.DiscardInBuffer(); }, Throws.TypeOf<InvalidOperationException>());
            }
        }

        [Test]
        public void ClosedUdpSocketExecute()
        {
            using (var port = new UdpSocketBroadcast())
            {
                using (AutoResetEvent cancelWaitEvent = new AutoResetEvent(false))
                {
                    Assert.That(() => { port.Execute(new TextLineParser(), cancelWaitEvent); }, Throws.TypeOf<InvalidOperationException>());
                }
            }
        }

        [Test]
        public void ClosedUdpSocketIsOpen()
        {
            using (var port = new UdpSocketBroadcast())
            {
                port.IsOpen();
            }
        }

        [Test]
        public void ClosedUdpSocketOpen()
        {
            using (var port = new UdpSocketBroadcast())
            {
                port.Open();
            }
        }

        [Test]
        public void ClosedUdpSocketRead()
        {
            using (var port = new UdpSocketBroadcast())
            {
#pragma warning disable 0618
                Assert.That(() => { port.Read(new byte[] { }); }, Throws.TypeOf<InvalidOperationException>());
#pragma warning restore 0618
            }
        }

        [Test]
        public void ClosedUdpSocketReadByte()
        {
            using (var port = new UdpSocketBroadcast())
            {
#pragma warning disable 0618
                Assert.That(() => { port.ReadByte(); }, Throws.TypeOf<InvalidOperationException>());
#pragma warning restore 0618
            }
        }

        [Test]
        public void ClosedUdpSocketWrite()
        {
            using (var port = new UdpSocketBroadcast())
            {
                Assert.That(() => { port.Write(new byte[] { }); }, Throws.TypeOf<InvalidOperationException>());
            }
        }

        #endregion
    }
}
