﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Net.Sockets;
using System.Threading;
using NUnit.Framework;
using yats.Ports;
using yats.TestCase.Parameter;
using yats.Ports.Utilities;
using yats.Ports.Utilities.Parsers;
using yats.Utilities;
using System.Collections.Generic;
using System.Text;

namespace yats.Ports.UnitTests
{
    // Tests that all public methods throw ObjectDisposedException
    [TestFixture]
    public class CheckObjectDisposedThrown
    {
        #region SerialPort
        [Test]
        public void DisposedSerialPortAttach()
        {
            SerialPort port = new SerialPort();
            port.Dispose();
            Assert.That(() => { port.Attach(new TextLineParser(), null); }, Throws.TypeOf<ObjectDisposedException>()); 
        }

        [Test]
        public void DisposedSerialPortCancel()
        {
            SerialPort port = new SerialPort();
            port.Dispose();
            Assert.That(() => { port.Cancel(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>()); 
        }

        [Test]
        public void DisposedSerialPortBytesToRead()
        {
            SerialPort port = new SerialPort();
            port.Dispose();
            Assert.That(() => { port.BytesToRead(); }, Throws.TypeOf<ObjectDisposedException>()); 
        }

        [Test]
        public void DisposedSerialPortCancelAllParsers()
        {
            SerialPort port = new SerialPort();
            port.Dispose();
            Assert.That(() => { port.CancelAllParsers(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedSerialPortClose()
        {
            SerialPort port = new SerialPort();
            port.Dispose();
            Assert.That(() => { port.Close(); }, Throws.TypeOf<ObjectDisposedException>()); 
        }

        [Test]
        public void DisposedSerialPortCommand()
        {
            SerialPort port = new SerialPort();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Command("t", new TextLineParser(), 0); }, Throws.TypeOf<ObjectDisposedException>()); 
#pragma warning restore 0618
        }

        [Test]
        public void DisposedSerialPortDetach()
        {
            SerialPort port = new SerialPort();
            port.Dispose();
            Assert.That(() => { port.Detach(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>()); 
        }

        [Test]
        public void DisposedSerialPortDiscardInBuffer()
        {
            SerialPort port = new SerialPort();
            port.Dispose();
            Assert.That(() => { port.DiscardInBuffer(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedSerialPortExecute()
        {
            SerialPort port = new SerialPort();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Execute(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>()); 
#pragma warning restore 0618
        }

        [Test]
        public void DisposedSerialPortGetTransmissionTime()
        {
            SerialPort port = new SerialPort();
            port.Dispose();
            Assert.That(() => { port.GetTransmissionTime(1); }, Throws.TypeOf<ObjectDisposedException>()); 
        }

        [Test]
        public void DisposedSerialPortIsOpen()
        {
            SerialPort port = new SerialPort();
            port.Dispose();
            Assert.That(() => { port.IsOpen(); }, Throws.TypeOf<ObjectDisposedException>()); 
        }

        [Test]
        public void DisposedSerialPortOpen()
        {
            SerialPort port = new SerialPort();
            port.Dispose();
            Assert.That(() => { port.Open(); }, Throws.TypeOf<ObjectDisposedException>()); 
        }

        [Test]
        public void DisposedSerialPortRead()
        {
            SerialPort port = new SerialPort();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Read(new byte[] { }); }, Throws.TypeOf<ObjectDisposedException>()); 
#pragma warning restore 0618
        }

        [Test]
        public void DisposedSerialPortReadByte()
        {
            SerialPort port = new SerialPort();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.ReadByte(); }, Throws.TypeOf<ObjectDisposedException>()); 
#pragma warning restore 0618
        }

        [Test]
        public void DisposedSerialPortWrite()
        {
            SerialPort port = new SerialPort();
            port.Dispose();
            Assert.That(() => { port.Write(new byte[] { }); }, Throws.TypeOf<ObjectDisposedException>());
        }

        #endregion
        #region BluetoothPort

        [Test]
        public void DisposedBluetoothPortAttach()
        {
            BluetoothPort port = new BluetoothPort();
            port.Dispose();
            Assert.That(() => { port.Attach(new TextLineParser(), null); }, Throws.TypeOf<ObjectDisposedException>()); 
        }

        [Test]
        public void DisposedBluetoothPortCancel()
        {
            BluetoothPort port = new BluetoothPort();
            port.Dispose();
            Assert.That(() => { port.Cancel(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>()); 
        }

        [Test]
        public void DisposedBluetoothPortBytesToRead()
        {
            BluetoothPort port = new BluetoothPort();
            port.Dispose();
            Assert.That(() => { port.BytesToRead(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedBluetoothPortCancelAllParsers()
        {
            BluetoothPort port = new BluetoothPort();
            port.Dispose();
            Assert.That(() => { port.CancelAllParsers(); }, Throws.TypeOf<ObjectDisposedException>()); 
        }

        [Test]
        public void DisposedBluetoothPortClose()
        {
            BluetoothPort port = new BluetoothPort();
            port.Dispose();
            Assert.That(() => { port.Close(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedBluetoothPortCommand()
        {
            BluetoothPort port = new BluetoothPort();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Command("t", new TextLineParser(), 0); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedBluetoothPortDetach()
        {
            BluetoothPort port = new BluetoothPort();
            port.Dispose();
            Assert.That(() => { port.Detach(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedBluetoothPortDiscardInBuffer()
        {
            BluetoothPort port = new BluetoothPort();
            port.Dispose();
            Assert.That(() => { port.DiscardInBuffer(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedBluetoothPortExecute()
        {
            BluetoothPort port = new BluetoothPort();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Execute(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedBluetoothPortIsOpen()
        {
            BluetoothPort port = new BluetoothPort();
            port.Dispose();
            Assert.That(() => { port.IsOpen(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedBluetoothPortOpen()
        {
            BluetoothPort port = new BluetoothPort();
            port.Dispose();
            Assert.That(() => { port.Open(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedBluetoothPortRead()
        {
            BluetoothPort port = new BluetoothPort();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Read(new byte[] { }); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedBluetoothPortReadByte()
        {
            BluetoothPort port = new BluetoothPort();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.ReadByte(); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedBluetoothPortWrite()
        {
            BluetoothPort port = new BluetoothPort();
            port.Dispose();
            Assert.That(() => { port.Write(new byte[] { }); }, Throws.TypeOf<ObjectDisposedException>());
        }

        #endregion
        #region PortEmulator

        [Test]
        public void DisposedPortEmulatorAttach()
        {
            PortEmulator port = new PortEmulator();
            port.Dispose();
            Assert.That(() => { port.Attach(new TextLineParser(), null); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedPortEmulatorCancel()
        {
            PortEmulator port = new PortEmulator();
            port.Dispose();
            Assert.That(() => { port.Cancel(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedPortEmulatorBytesToRead()
        {
            PortEmulator port = new PortEmulator();
            port.Dispose();
            Assert.That(() => { port.BytesToRead(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedPortEmulatorCancelAllParsers()
        {
            PortEmulator port = new PortEmulator();
            port.Dispose();
            Assert.That(() => { port.CancelAllParsers(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedPortEmulatorClose()
        {
            PortEmulator port = new PortEmulator();
            port.Dispose();
            Assert.That(() => { port.Close(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedPortEmulatorCommand()
        {
            PortEmulator port = new PortEmulator();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Command("t", new TextLineParser(), 0); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedPortEmulatorDetach()
        {
            PortEmulator port = new PortEmulator();
            port.Dispose();
            Assert.That(() => { port.Detach(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedPortEmulatorDiscardInBuffer()
        {
            PortEmulator port = new PortEmulator();
            port.Dispose();
            Assert.That(() => { port.DiscardInBuffer(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedPortEmulatorExecute()
        {
            PortEmulator port = new PortEmulator();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Execute(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedPortEmulatorIsOpen()
        {
            PortEmulator port = new PortEmulator();
            port.Dispose();
            Assert.That(() => { port.IsOpen(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedPortEmulatorOpen()
        {
            PortEmulator port = new PortEmulator();
            port.Dispose();
            Assert.That(() => { port.Open(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedPortEmulatorRead()
        {
            PortEmulator port = new PortEmulator();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Read(new byte[] { }); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedPortEmulatorReadByte()
        {
            PortEmulator port = new PortEmulator();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.ReadByte(); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedPortEmulatorWrite()
        {
            PortEmulator port = new PortEmulator();
            port.Dispose();
            Assert.That(() => { port.Write(new byte[] { }); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedPortEmulatorEmulateReceivedData()
        {
            PortEmulator port = new PortEmulator();
            port.Dispose();
            Assert.That(() => { port.EmulateReceivedData(new byte[] { }); }, Throws.TypeOf<ObjectDisposedException>());
        }

        #endregion
        #region SshPort

        [Test]
        public void DisposedSshPortAttach()
        {
            SshPort port = new SshPort();
            port.Dispose();
            Assert.That(() => { port.Attach(new TextLineParser(), null); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedSshPortCancel()
        {
            SshPort port = new SshPort();
            port.Dispose();
            Assert.That(() => { port.Cancel(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedSshPortBytesToRead()
        {
            SshPort port = new SshPort();
            port.Dispose();
            Assert.That(() => { port.BytesToRead(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedSshPortCancelAllParsers()
        {
            SshPort port = new SshPort();
            port.Dispose();
            Assert.That(() => { port.CancelAllParsers(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedSshPortClose()
        {
            SshPort port = new SshPort();
            port.Dispose();
            Assert.That(() => { port.Close(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedSshPortCommand()
        {
            SshPort port = new SshPort();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Command("t", new TextLineParser(), 0); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedSshPortDetach()
        {
            SshPort port = new SshPort();
            port.Dispose();
            Assert.That(() => { port.Detach(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedSshPortDiscardInBuffer()
        {
            SshPort port = new SshPort();
            port.Dispose();
            Assert.That(() => { port.DiscardInBuffer(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedSshPortExecute()
        {
            SshPort port = new SshPort();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Execute(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedSshPortIsOpen()
        {
            SshPort port = new SshPort();
            port.Dispose();
            Assert.That(() => { port.IsOpen(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedSshPortOpen()
        {
            SshPort port = new SshPort();
            port.Dispose();
            Assert.That(() => { port.Open(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedSshPortRead()
        {
            SshPort port = new SshPort();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Read(new byte[] { }); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedSshPortReadByte()
        {
            SshPort port = new SshPort();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.ReadByte(); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedSshPortWrite()
        {
            SshPort port = new SshPort();
            port.Dispose();
            Assert.That(() => { port.Write(new byte[] { }); }, Throws.TypeOf<ObjectDisposedException>());
        }

        #endregion
        #region TcpIpClientSocket

        [Test]
        public void DisposedTcpIpClientSocketAttach()
        {
            TcpIpClientSocket port = new TcpIpClientSocket(80);
            port.Dispose();
            Assert.That(() => { port.Attach(new TextLineParser(), null); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpIpClientSocketCancel()
        {
            TcpIpClientSocket port = new TcpIpClientSocket(80);
            port.Dispose();
            Assert.That(() => { port.Cancel(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpIpClientSocketBytesToRead()
        {
            TcpIpClientSocket port = new TcpIpClientSocket(80);
            port.Dispose();
            Assert.That(() => { port.BytesToRead(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpIpClientSocketCancelAllParsers()
        {
            TcpIpClientSocket port = new TcpIpClientSocket(80);
            port.Dispose();
            Assert.That(() => { port.CancelAllParsers(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpIpClientSocketClose()
        {
            TcpIpClientSocket port = new TcpIpClientSocket(80);
            port.Dispose();
            Assert.That(() => { port.Close(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpIpClientSocketCommand()
        {
            TcpIpClientSocket port = new TcpIpClientSocket(80);
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Command("t", new TextLineParser(), 0); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedTcpIpClientSocketDetach()
        {
            TcpIpClientSocket port = new TcpIpClientSocket(80);
            port.Dispose();
            Assert.That(() => { port.Detach(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpIpClientSocketDiscardInBuffer()
        {
            TcpIpClientSocket port = new TcpIpClientSocket(80);
            port.Dispose();
            Assert.That(() => { port.DiscardInBuffer(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpIpClientSocketExecute()
        {
            TcpIpClientSocket port = new TcpIpClientSocket(80);
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Execute(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedTcpIpClientSocketIsOpen()
        {
            TcpIpClientSocket port = new TcpIpClientSocket(80);
            port.Dispose();
            Assert.That(() => { port.IsOpen(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpIpClientSocketOpen()
        {
            TcpIpClientSocket port = new TcpIpClientSocket(80);
            port.Dispose();
            Assert.That(() => { port.Open(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpIpClientSocketRead()
        {
            TcpIpClientSocket port = new TcpIpClientSocket(80);
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Read(new byte[] { }); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedTcpIpClientSocketReadByte()
        {
            TcpIpClientSocket port = new TcpIpClientSocket(80);
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.ReadByte(); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedTcpIpClientSocketWrite()
        {
            TcpIpClientSocket port = new TcpIpClientSocket(80);
            port.Dispose();
            Assert.That(() => { port.Write(new byte[] { }); }, Throws.TypeOf<ObjectDisposedException>());
        }

        #endregion
        #region TcpServer

        [Test]
        public void DisposedTcpServerAttach()
        {
            TcpServer port = new TcpServer(80);
            port.Dispose();
            Assert.That(() => { port.Attach(new TextLineParser(), null); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpServerCancel()
        {
            TcpServer port = new TcpServer(80);
            port.Dispose();
            Assert.That(() => { port.Cancel(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpServerBytesToRead()
        {
            TcpServer port = new TcpServer(80);
            port.Dispose();
            Assert.That(() => { port.BytesToRead(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpServerCancelAllParsers()
        {
            TcpServer port = new TcpServer(80);
            port.Dispose();
            Assert.That(() => { port.CancelAllParsers(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpServerClose()
        {
            TcpServer port = new TcpServer(80);
            port.Dispose();
            Assert.That(() => { port.Close(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpServerCommand()
        {
            TcpServer port = new TcpServer(80);
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Command("t", new TextLineParser(), 0); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedTcpServerDetach()
        {
            TcpServer port = new TcpServer(80);
            port.Dispose();
            Assert.That(() => { port.Detach(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpServerDiscardInBuffer()
        {
            TcpServer port = new TcpServer(80);
            port.Dispose();
            Assert.That(() => { port.DiscardInBuffer(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpServerExecute()
        {
            TcpServer port = new TcpServer(80);
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Execute(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedTcpServerIsOpen()
        {
            TcpServer port = new TcpServer(80);
            port.Dispose();
            Assert.That(() => { port.IsOpen(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpServerOpen()
        {
            TcpServer port = new TcpServer(80);
            port.Dispose();
            Assert.That(() => { port.Open(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpServerRead()
        {
            TcpServer port = new TcpServer(80);
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Read(new byte[] { }); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedTcpServerReadByte()
        {
            TcpServer port = new TcpServer(80);
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.ReadByte(); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedTcpServerStartListening()
        {
            TcpServer port = new TcpServer(80);
            port.Dispose();
            Assert.That(() => { port.StartListening(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedTcpServerWrite()
        {
            TcpServer port = new TcpServer(80);
            port.Dispose();
            Assert.That(() => { port.Write(new byte[] { }); }, Throws.TypeOf<ObjectDisposedException>());
        }

        #endregion
        #region UdpSocket

        [Test]
        public void DisposedUdpSocketAttach()
        {
            UdpSocket port = new UdpSocketBroadcast();
            port.Dispose();
            Assert.That(() => { port.Attach(new TextLineParser(), null); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedUdpSocketCancel()
        {
            UdpSocket port = new UdpSocketBroadcast();
            port.Dispose();
            Assert.That(() => { port.Cancel(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedUdpSocketBytesToRead()
        {
            UdpSocket port = new UdpSocketBroadcast();
            port.Dispose();
            Assert.That(() => { port.BytesToRead(); }, Throws.TypeOf<ObjectDisposedException>()); 
        }

        [Test]
        public void DisposedUdpSocketCancelAllParsers()
        {
            UdpSocket port = new UdpSocketBroadcast();
            port.Dispose();
            Assert.That(() => { port.CancelAllParsers(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedUdpSocketClose()
        {
            UdpSocket port = new UdpSocketBroadcast();
            port.Dispose();
            Assert.That(() => { port.Close(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedUdpSocketCommand()
        {
            UdpSocket port = new UdpSocketBroadcast();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Command("t", new TextLineParser(), 0); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedUdpSocketDetach()
        {
            UdpSocket port = new UdpSocketBroadcast();
            port.Dispose();
            Assert.That(() => { port.Detach(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedUdpSocketDiscardInBuffer()
        {
            UdpSocket port = new UdpSocketBroadcast();
            port.Dispose();
            Assert.That(() => { port.DiscardInBuffer(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedUdpSocketExecute()
        {
            UdpSocket port = new UdpSocketBroadcast();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Execute(new TextLineParser()); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedUdpSocketIsOpen()
        {
            UdpSocket port = new UdpSocketBroadcast();
            port.Dispose();
            Assert.That(() => { port.IsOpen(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedUdpSocketOpen()
        {
            UdpSocket port = new UdpSocketBroadcast();
            port.Dispose();
            Assert.That(() => { port.Open(); }, Throws.TypeOf<ObjectDisposedException>());
        }

        [Test]
        public void DisposedUdpSocketRead()
        {
            UdpSocket port = new UdpSocketBroadcast();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.Read(new byte[] { }); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedUdpSocketReadByte()
        {
            UdpSocket port = new UdpSocketBroadcast();
            port.Dispose();
#pragma warning disable 0618
            Assert.That(() => { port.ReadByte(); }, Throws.TypeOf<ObjectDisposedException>());
#pragma warning restore 0618
        }

        [Test]
        public void DisposedUdpSocketWrite()
        {
            UdpSocket port = new UdpSocketBroadcast();
            port.Dispose();
            Assert.That(() => { port.Write(new byte[] { }); }, Throws.TypeOf<ObjectDisposedException>());
        }

        #endregion
    }
}
