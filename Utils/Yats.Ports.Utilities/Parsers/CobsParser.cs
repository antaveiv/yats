﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using yats.Ports;
using yats.Utilities;


namespace yats.Ports.Utilities.Parsers
{
    /// <summary>
    /// Consistent Overhead Byte Stuffing https://en.wikipedia.org/wiki/Consistent_Overhead_Byte_Stuffing
    /// </summary>
    public class CobsParser : Parser, IResetable, ICloneable
    {
        private Queue<byte> m_receivedData = new Queue<byte>(256);

        public CobsParser()
        {
        }

        public override ReadOperationResult GetResult()
        {
            if (m_receivedData.Count > 0 && m_receivedData.ToArray()[m_receivedData.Count - 1] == 0)
            {
                return new ReadOperationResult(ReadOperationResult.Result.SUCCESS);
            }
            else
            {
                return new ReadOperationResult(ReadOperationResult.Result.TIMEOUT);
            }
        }

        public override bool IncomingData(byte dataByte)
        {
            m_receivedData.Enqueue(dataByte);
            return dataByte == 0;// packet ends with 0
        }

        public void Reset()
        {
            m_receivedData = new Queue<byte>(256);
        }

        public static byte[] Decode(byte[] encodedBuffer)
        {
            int size = encodedBuffer.Length;
            if (size < 2)
            {
                return new byte[] { };
            }
            var decodedBuffer = new byte[size];

            int read_index = 0;
            int write_index = 0;
            byte code = 0;
            byte i = 0;

            while (read_index < size)
            {
                code = encodedBuffer[read_index];

                if (read_index + code > size && code != 1)
                {
                    return new byte[] { };
                }

                read_index++;

                for (i = 1; i < code; i++)
                {
                    decodedBuffer[write_index++] = encodedBuffer[read_index++];
                }

                if (code != 0xFF && read_index != size)
                {
                    decodedBuffer[write_index++] = 0;
                }
            }

            return decodedBuffer.Slice(0, write_index - 1);
        }

        public byte[] Decode()
        {
            byte[] encodedBuffer = m_receivedData.ToArray();
            return Decode(encodedBuffer);
        }

        public static byte[] Encode(byte[] buffer)
        {
            var encodedBuffer = new byte[buffer.Length + 2];
            int read_index = 0;
            int write_index = 1;
            int code_index = 0;
            byte code = 1;

            while (read_index < buffer.Length)
            {
                if (buffer[read_index] == 0)
                {
                    encodedBuffer[code_index] = code;
                    code = 1;
                    code_index = write_index++;
                    read_index++;
                }
                else
                {
                    encodedBuffer[write_index++] = buffer[read_index++];
                    code++;

                    if (code == 0xFF)
                    {
                        encodedBuffer[code_index] = code;
                        code = 1;
                        code_index = write_index++;
                    }
                }
            }

            encodedBuffer[code_index] = code;

            return encodedBuffer;
        }

        /// \brief Get the maximum encoded buffer size needed for a given unencoded buffer size.
        /// \param unencodedBufferSize The size of the buffer to be encoded.
        /// \returns the maximum size of the required encoded buffer.
        int GetEncodedBufferSize(int unencodedBufferSize)
        {
            return unencodedBufferSize + unencodedBufferSize / 254 + 1;
        }

        public object Clone()
        {
            CobsParser clone = new CobsParser();
            clone.m_receivedData.EnqueueAll(this.m_receivedData.ToArray());
            return clone;
        }

        public override byte[] GetReceivedData()
        {
            return m_receivedData.ToArray();
        }
    }
}
