﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using yats.Ports;
using yats.Utilities;

namespace yats.Ports.Utilities.Parsers
{
    /// <summary>
    /// An example parser. Waits until a text line is received (terminated by CR or LF). Preceding newline is ignored
    /// </summary>
    public class TextLineParser : Parser, IResetable, ICloneable
    {
        private Queue<byte> m_receivedData = new Queue<byte>(128);

        public TextLineParser()
        {
        }

        // override the default line end characters
        public TextLineParser(char[] lineEndCharacters)
        {
            this.m_lineEndCharacters = lineEndCharacters;
        }
        
        public override bool IncomingData(byte dataByte)
        {
            if (m_receivedData.Count == 0 && m_lineEndCharacters.Contains((char)dataByte))
            {
                //ignore
                return false;
            }
            
            if (m_lineEndCharacters.Contains((char)dataByte))
            {
                // finished - line end received
                return true;
            }

            // gather bytes into queue
            m_receivedData.Enqueue(dataByte);
            return false;
        }

        private char [] m_lineEndCharacters = new char [] {'\n', '\r'};
        public override ReadOperationResult GetResult()
        {
            if (m_receivedData.Count > 0)
            {
                return new ReadOperationResult(ReadOperationResult.Result.SUCCESS);
            }
            else
            {
                return new ReadOperationResult(ReadOperationResult.Result.TIMEOUT);
            }
        }

        public override byte[] GetReceivedData()
        {
            return this.m_receivedData.ToArray();
        }

        // returns received line as ASCII string
        public string GetReceivedLine()
        {
            return Encoding.ASCII.GetString(GetReceivedData());
        }

        // returns received line as UTF8 string
        public string GetReceivedLineUTF()
        {
            return Encoding.UTF8.GetString(GetReceivedData());
        }

        public string GetReceivedLine(Encoding encoding)
        {
            return encoding.GetString(GetReceivedData());
        }

        public void Reset()
        {
            m_receivedData = new Queue<byte>(128);
        }

        public object Clone()
        {
            TextLineParser clone = new TextLineParser(m_lineEndCharacters);
            clone.m_receivedData.EnqueueAll(this.m_receivedData.ToArray());
            return clone;
        }
    }
}
