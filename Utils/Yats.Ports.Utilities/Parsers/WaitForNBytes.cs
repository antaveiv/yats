﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using yats.Ports;

namespace yats.Ports.Utilities.Parsers
{
    /// <summary>
    /// Can be used to receive an exact number of bytes
    /// </summary>
    public class WaitForNBytes : Parser
    {
        private Queue<byte> m_receivedData;
        private int m_expectedByteCount;

        public WaitForNBytes(int count)
        {
            this.m_expectedByteCount = count;
            this.m_receivedData = new Queue<byte>(count);
        }

        public override bool IncomingData(byte dataByte)
        {
            m_receivedData.Enqueue(dataByte);
            return m_receivedData.Count == m_expectedByteCount; //finish reading port when N bytes are received
        }

        public override ReadOperationResult GetResult()
        {
            if (m_receivedData.Count == m_expectedByteCount)
            {
                return new ReadOperationResult(ReadOperationResult.Result.SUCCESS);
            }
            else
            {
                return new ReadOperationResult(ReadOperationResult.Result.TIMEOUT);
            }
        }

        public override byte[] GetReceivedData()
        {
            return this.m_receivedData.ToArray();
        }
    }
}
