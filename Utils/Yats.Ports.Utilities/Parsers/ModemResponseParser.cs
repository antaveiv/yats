﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using yats.Ports;
using yats.Utilities;

namespace yats.Ports.Utilities.Parsers
{
    //TODO rename - this waits for a line end, not specific to modem interface
    public class ModemResponseParser : Parser
    {
        private byte[] rcvString = new byte[0];
        string result = "";
        Dictionary<string, ReadOperationResult> responses;

        public ModemResponseParser(Dictionary<string, ReadOperationResult> responses_in)
        {
            /* tikrinam ar teisingai nurodytas(-ti) wait stringas(-ai)*/
            if (responses_in.Count > 0)
            {
                responses = responses_in;
            }
        }

        public override bool IncomingData(byte dataByte)
        {
            this.rcvString = ByteArray.Add(this.rcvString, dataByte);
            //Logger.CommLog.DebugFormat("{0} {1}", dataByte, (char)dataByte);
            result += (char)dataByte;

            if (result.EndsWith("\r\n"))
            {
                foreach (KeyValuePair<string, ReadOperationResult> entry in responses)
                {
                    if (result.Contains(entry.Key))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public override ReadOperationResult GetResult()
        {
            foreach (KeyValuePair<string, ReadOperationResult> entry in responses)
            {
                if (result.Contains(entry.Key))
                {
                    return entry.Value;
                }
            }
            return new ReadOperationResult(ReadOperationResult.Result.ERROR);
        }

        public override byte[] GetReceivedData()
        {
            return this.rcvString;
        }

        public string GetModemResponse()
        {
            return result;
        }
    }
}
