﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Text;
using yats.Ports;
using yats.Utilities;

namespace yats.Ports.Utilities.Parsers
{
    /// <summary>
    /// Waits until any byte sequence is read from a port. Multiple sequences can be configured for either pass or fail result
    /// </summary>
    public class WaitForMultipleSequences : Parser, IResetable
    {
        private class ReceiveState
        {
            public byte[] WaitFor;
            public int State = 0;
            public ReadOperationResult.Result ResultIfReceived;
            public ReceiveState(byte[] waitFor, ReadOperationResult.Result resultIfReceived)
            {
                WaitFor = waitFor;
                ResultIfReceived = resultIfReceived;
            }
            public void Reset()
            {
                State = 0;
            }
        }

        private Queue<byte> m_receivedData = new Queue<byte>(10240);
        private List<ReceiveState> receivers = new List<ReceiveState>();

        public WaitForMultipleSequences(IEnumerable<string> successAnswers, IEnumerable<string> failAnswers)
        {
            if (successAnswers != null)
            {
                foreach (var str in successAnswers)
                {
                    if (string.IsNullOrEmpty(str) == false)
                    {
                        this.receivers.Add(new ReceiveState(Encoding.ASCII.GetBytes(str), ReadOperationResult.Result.SUCCESS));
                    }
                }
            }
            if (failAnswers != null)
            {
                foreach (var str in failAnswers)
                {
                    if (string.IsNullOrEmpty(str) == false)
                    {
                        this.receivers.Add(new ReceiveState(Encoding.ASCII.GetBytes(str), ReadOperationResult.Result.ERROR));
                    }
                }
            }
        }

        public WaitForMultipleSequences(IEnumerable<byte[]> successAnswers, IEnumerable<byte[]> failAnswers)
        {
            if (successAnswers != null)
            {
                foreach (var buffer in successAnswers)
                {
                    if (ByteArray.IsNullOrEmpty(buffer) == false)
                    {
                        this.receivers.Add(new ReceiveState(buffer, ReadOperationResult.Result.SUCCESS));
                    }
                }
            }

            if (failAnswers != null)
            {
                foreach (var buffer in failAnswers)
                {
                    if (ByteArray.IsNullOrEmpty(buffer) == false)
                    {
                        this.receivers.Add(new ReceiveState(buffer, ReadOperationResult.Result.ERROR));
                    }
                }
            }

        }

        public WaitForMultipleSequences(params string [] successAnswers):this(successAnswers, null)
        {
        }

        public WaitForMultipleSequences(params byte[][] successAnswers) : this(successAnswers, null)
        {
        }

        public override bool IncomingData(byte dataByte)
        {
            m_receivedData.Enqueue(dataByte);
            foreach (var rcv in this.receivers)
            {
                if (rcv.WaitFor[rcv.State] == dataByte)
                {
                    rcv.State++;
                    if (rcv.State == rcv.WaitFor.Length)
                    {
                        return true;
                    }
                }
                else
                {
                    rcv.State = 0; // back to start
                }
            }

            return false;
        }

        public override ReadOperationResult GetResult()
        {
            foreach (var rcv in this.receivers)
            {
                if (rcv.State == rcv.WaitFor.Length)
                {
                    return new ReadOperationResult(rcv.ResultIfReceived);
                }
            }
            return new ReadOperationResult(ReadOperationResult.Result.TIMEOUT);
        }

        public override byte[] GetReceivedData()
        {
            return this.m_receivedData.ToArray();
        }

        public virtual void Reset()
        {
            foreach (var rcv in this.receivers)
            {
                rcv.State = 0;
            }
        }
    }
}