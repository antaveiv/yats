﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System.Collections.Generic;
using System.Text;
using yats.Ports;
using yats.Utilities;

namespace yats.Ports.Utilities.Parsers
{
    /// <summary>
    /// Waits until an exact byte sequence is read from a port
    /// </summary>
    public class WaitForBytes : Parser, IResetable
    {
        private Queue<byte> m_receivedData = new Queue<byte>(10240);
        private byte[] m_waitFor;
        private int m_state = 0;

        public WaitForBytes(string waitFor)
        {
            if (string.IsNullOrEmpty(waitFor) == false)
            {
                this.m_waitFor = Encoding.ASCII.GetBytes(waitFor);
            }
        }

        public WaitForBytes(byte [] waitFor)
        {
            this.m_waitFor = waitFor;
        }

        public override bool IncomingData(byte dataByte)
        {
            m_receivedData.Enqueue(dataByte);
            if (m_waitFor.IsNullOrEmpty())
            {
                return false;
            }

            if (m_waitFor[m_state] == dataByte)
            {
                m_state++;
                if (m_state == m_waitFor.Length)
                {
                    return true;
                }
            } else {
                m_state = 0; // back to start
            }

            return false;
        }

        public override ReadOperationResult GetResult()
        {
            if ((m_state == m_waitFor.Length))
            {
                return new ReadOperationResult(ReadOperationResult.Result.SUCCESS);
            }
            else
            {
                return new ReadOperationResult(ReadOperationResult.Result.TIMEOUT);
            }
        }

        public override byte[] GetReceivedData()
        {
            return this.m_receivedData.ToArray();
        }

        public virtual void Reset()
        {
            m_state = 0;
        }
    }
}