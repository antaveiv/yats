﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using yats.Ports;
using yats.TestCase.Parameter;
using System.Threading;

namespace yats.Ports.Utilities
{
    public class PortOpenHelper
    {
        /// <summary>
        /// Open a port (serial, TCP/IP etc.)
        /// </summary>
        /// <param name="portSettings">port configuration</param>
        /// <returns>instance or null if canceled or failed</returns>
        [Obsolete("Use cancellable method")]
        public static AbstractPort Open(PortSettings portSettings)
        {
            return Open(portSettings, null);
        }

        /// <summary>
        /// Open a port (serial, TCP/IP etc.)
        /// </summary>
        /// <param name="portSettings">port configuration</param>
        /// <param name="cancelWaitEvent">if set before/during Open call - the operation is canceled</param> 
        /// <returns>instance or null if canceled or failed</returns>
        public static AbstractPort Open(PortSettings portSettings, WaitHandle cancelWaitEvent)
        {
            if (portSettings == null)
            {
                return null;
            }
            switch(portSettings.Selected)
            {
                case PortSettings.SelectedSetup.SERIAL:
                    yats.Ports.RtsEnableEnum rts;
                    switch(portSettings.SerialPort.RtsEnable)
                    {
                        case yats.TestCase.Parameter.RtsEnableEnum.OFF:
                            rts = yats.Ports.RtsEnableEnum.OFF;
                            break;
                        case yats.TestCase.Parameter.RtsEnableEnum.ON:
                            rts = yats.Ports.RtsEnableEnum.ON;
                            break;
                        default:
                            rts = yats.Ports.RtsEnableEnum.DONT_CHANGE;
                            break;
                    }
                    return PortReusePool.OpenSerial( portSettings.SerialPort.PortName, portSettings.SerialPort.BaudRate, portSettings.SerialPort.DataBits, portSettings.SerialPort.Parity, portSettings.SerialPort.StopBits, rts, portSettings.SerialPort.DtrEnable, portSettings.SerialPort.Handshake, portSettings.DiscardDataWithoutParsers );

                case PortSettings.SelectedSetup.TCP_IP_CLIENT:
                    return PortReusePool.OpenTcpIpClient(portSettings.TcpIpClient.Address, portSettings.TcpIpClient.PortNumber, portSettings.DiscardDataWithoutParsers, portSettings.TcpIpClient.KeepAlive);

                case PortSettings.SelectedSetup.TCP_IP_SERVER:
                    return PortReusePool.OpenTcpIpServer(portSettings.TcpIpServer.PortNumber, portSettings.TcpIpServer.ClientConnectTimeoutMs, portSettings.TcpIpServer.ReplaceSocket, cancelWaitEvent, portSettings.DiscardDataWithoutParsers, portSettings.TcpIpServer.KeepAlive);

                case PortSettings.SelectedSetup.SSH_CLIENT:
                    return PortReusePool.OpenSshClient(portSettings.SshClient.Host, portSettings.SshClient.User, portSettings.SshClient.Password, portSettings.DiscardDataWithoutParsers);

                case PortSettings.SelectedSetup.UDP_SOCKET:
                    return PortReusePool.OpenUpdSocket(portSettings.UdpSocket.RemoteIp, portSettings.UdpSocket.LocalPortNumber, portSettings.UdpSocket.RemotePortNumber, portSettings.UdpSocket.OverwriteIP, portSettings.UdpSocket.Broadcast, portSettings.DiscardDataWithoutParsers);

                case PortSettings.SelectedSetup.BLUETOOTH:
                    return PortReusePool.OpenBluetoothPort(portSettings.BluetoothPort.Address, portSettings.BluetoothPort.PIN, portSettings.DiscardDataWithoutParsers);

                case PortSettings.SelectedSetup.EMULATOR:
                    {
                        PortEmulator port;
                        switch (portSettings.Emulator.DataSource)
                        {
                            case PortSettings.PortEmulatorInfo.DataSouceType.FILE:
                                port = PortEmulator.FromFile(portSettings.Emulator.FileName);
                                break;
                            case PortSettings.PortEmulatorInfo.DataSouceType.BYTE_ARRAY:
                                port = new PortEmulator(portSettings.Emulator.Data);
                                break;
                            default :
                                throw new NotImplementedException(string.Format("DataSource {0} not implemented", portSettings.Emulator.DataSource));
                        }
                        port.Open();
                        return port;
                    }
                    
                default:
                    throw new NotImplementedException( );
            }

        }

        [Obsolete ("Use Close(SerialPortSettingsParameter) - this method does not work when port is null (i.e. something like Cancel happened during open)")]
        public static void Close(AbstractPort port)
        {
            PortReusePool.Close(port);
        }

        public static void Close(AbstractPort port, PortSettings portSettings)
        {
            if (PortReusePool.Close(port))
            {
                return;// for simple cases this will do. For a canceled TCP server port will be null and the next step will take care of the listening socket
            }

            if (portSettings != null)
            {
                switch (portSettings.Selected)
                {
                    case PortSettings.SelectedSetup.TCP_IP_SERVER:
                        PortReusePool.CloseTcpIpServer(portSettings.TcpIpServer.PortNumber);
                        break;
                }
            }
        }

        public static void CloseImmediately(AbstractPort port)
        {
            PortReusePool.CloseImmediately(port);
        }
        
        public static void Flush()
        {
            PortReusePool.Flush();
        }

        public static void CancelOpen(PortSettings portSettings)
        {
            switch (portSettings.Selected)
            {
                case PortSettings.SelectedSetup.SERIAL:
                    return; // cancel not supported
                case PortSettings.SelectedSetup.TCP_IP_CLIENT:
                    return; // cancel not supported

                case PortSettings.SelectedSetup.TCP_IP_SERVER:
                    PortReusePool.CancelOpen(portSettings.TcpIpServer.PortNumber); 
                    return;
                case PortSettings.SelectedSetup.SSH_CLIENT:
                    return; // cancel not supported

                default:
                    throw new NotImplementedException();
            }
        }
    }
}
