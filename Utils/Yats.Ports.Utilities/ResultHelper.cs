/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using log4net;
using yats.Ports;
using yats.TestCase.Interface;

namespace yats.Ports.Utilities
{
	public class ResultHelper
	{
		/// <summary>
		/// Convert a Port operation result to Test case result
		/// </summary>
		/// <param name='result'>
		/// Port operation result
		/// </param>
		public static ITestResult Convert(yats.Ports.ReadOperationResult result)
		{
			if (result.PortException != null)
            {
                return new ExceptionResult(result.PortException);
            }

			if (result.SerialPortError.HasValue)
            {
                LogManager.GetLogger("Port").Info(result.SerialPortError.Value);
                return new TestResult(ResultEnum.INCONCLUSIVE);
            }

            return Convert(result.OperationResult);
		}

        public static ITestResult Convert(yats.Ports.ReadOperationResult.Result result)
        {
            switch (result)
            {
                case ReadOperationResult.Result.SUCCESS:
                    return new TestResult(ResultEnum.PASS);
                case ReadOperationResult.Result.TIMEOUT:
                case ReadOperationResult.Result.ERROR:
                    return new TestResult(ResultEnum.FAIL);
                case ReadOperationResult.Result.CANCELLED:
                    return new TestResult(ResultEnum.CANCELED);
                case ReadOperationResult.Result.PORT_ERROR:
                case ReadOperationResult.Result.INTERNAL_ERROR:
                default:
                    return new TestResult(ResultEnum.INCONCLUSIVE);
            }
        }
	}
}

