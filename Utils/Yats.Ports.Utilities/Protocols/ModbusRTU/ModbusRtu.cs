﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using yats.Ports;
using log4net;
using yats.Utilities;
using System.Threading;

namespace yats.Ports.Utilities.Protocols.ModbusRTU
{
    public class ModbusRtu
    {
        internal static ILog Logger = LogManager.GetLogger("ModBus");
        private const int MODBUS_MAX_BYTES = 200;
        private AbstractPort port;

        public ModbusRtu(AbstractPort port)
        {
            this.port = port;
        }

        public ReadOperationResult.Result Read(int address, int byte_offset, int num_bytes, out byte[] result, int timeoutMs, WaitHandle cancelWaitEvent)
        {
            result = new byte[num_bytes];
            int result_offset = 0;

            for (int i = byte_offset; i < byte_offset + num_bytes; i += MODBUS_MAX_BYTES)
            {
                int size = Math.Min(byte_offset + num_bytes - i, MODBUS_MAX_BYTES);
                byte[] tmp_res;
                yats.Ports.ReadOperationResult.Result res = SendFc3((byte)address, (ushort)(i / 2), (ushort)(size / 2), out tmp_res, timeoutMs, cancelWaitEvent);
                if (res != yats.Ports.ReadOperationResult.Result.SUCCESS)
                {
                    return res;
                }
                Buffer.BlockCopy(tmp_res, 0, result, result_offset, size);
                result_offset += size;
            }
            for (int i = 0; i < num_bytes / 2; i++)
            {
                byte x = result[2 * i];
                result[2 * i] = result[2 * i + 1];
                result[2 * i + 1] = x;
            }
            return yats.Ports.ReadOperationResult.Result.SUCCESS;
        }

        public ReadOperationResult.Result Write(int address, int byte_offset, byte[] bytes, int timeoutMs, WaitHandle cancelWaitEvent)
        {
            for (int i = 0; i < bytes.Length / 2; i++)
            {
                byte x = bytes[2 * i];
                bytes[2 * i] = bytes[2 * i + 1];
                bytes[2 * i + 1] = x;
            }

            int num_bytes = bytes.Length;
            int source_offset = 0;
            for (int i = byte_offset; i < byte_offset + num_bytes; i += MODBUS_MAX_BYTES)
            {
                int size = Math.Min(byte_offset + num_bytes - i, MODBUS_MAX_BYTES);
                byte[] tmp_buf = new byte[size];
                Buffer.BlockCopy(bytes, source_offset, tmp_buf, 0, size);
                source_offset += size;
                yats.Ports.ReadOperationResult.Result res = SendFc16((byte)address, (ushort)(i / 2), (ushort)(size / 2), tmp_buf, timeoutMs, cancelWaitEvent);
                if (res != yats.Ports.ReadOperationResult.Result.SUCCESS)
                {
                    return res;
                }
            }
            return yats.Ports.ReadOperationResult.Result.SUCCESS;
        }

        ///Write Multiple Registers
        protected ReadOperationResult.Result SendFc16(byte address, ushort start, ushort registers, byte[] values, int timeoutMs, WaitHandle cancelWaitEvent)
        {
            //Ensure port is open:
            if (port.IsOpen())
            {
                port.DiscardInBuffer();
                //Message is 1 addr + 1 fcn + 2 start + 2 reg + 1 count + 2 * reg vals + 2 CRC
                byte[] message = new byte[9 + 2 * registers];
                //Function 16 response is fixed at 8 bytes
                byte[] response = new byte[8];

                //Add bytecount to message:
                message[6] = (byte)(registers * 2);
                //Put write values into message prior to sending:
                Buffer.BlockCopy(values, 0, message, 7, values.Length);

                /*for (int i = 0; i < registers; i++)
                {
                    message[7 + 2 * i] = (byte)(values[i] >> 8);
                    message[8 + 2 * i] = (byte)(values[i]);
                }*/

                //Build outgoing message:
                BuildMessage(address, (byte)16, start, registers, ref message);

                //Send Modbus message to Serial Port:
                try
                {
                    ReadOperationResult res = GetResponse(message, ref response, timeoutMs, cancelWaitEvent);
                    if (res.OperationResult == yats.Ports.ReadOperationResult.Result.SUCCESS)
                    {
                        if (Logger.IsDebugEnabled)
                        {
                            Logger.DebugFormat("Write address {0} offset {1} regs {2} [{3}]", address, start, registers, values.ToHexString());
                        }
                    }
                    else
                    {
                        Logger.WarnFormat("Write failed: address {0} offset {1} regs {2} packet: [{3}]", address, start, registers, message.ToHexString());
                    }
                    return res.OperationResult;
                }
                catch (Exception err)
                {
                    Logger.Error("Exception ", err);
                    //modbusStatus = "Error in write event: " + err.Message;
                    return yats.Ports.ReadOperationResult.Result.INTERNAL_ERROR;
                }
            }
            else
            {
                //modbusStatus = "Serial port not open";
                return yats.Ports.ReadOperationResult.Result.PORT_ERROR;
            }
        }

        /// <summary>
        /// Read Registers
        /// </summary>
        /// <param name="address"></param>
        /// <param name="start"></param>
        /// <param name="registers"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        protected ReadOperationResult.Result SendFc3(byte address, ushort start, ushort registers, out byte[] result, int timeoutMs, WaitHandle cancelWaitEvent)
        {
            result = null;

            //Ensure port is open:
            if (port.IsOpen())
            {
                port.DiscardInBuffer();
                //Function 3 request is always 8 bytes:
                byte[] message = new byte[8];
                //Function 3 response buffer:
                byte[] response = new byte[5 + 2 * registers];
                //Build outgoing modbus message:
                BuildMessage(address, (byte)3, start, registers, ref message);

                //Send modbus message to Serial Port:
                try
                {
                    ReadOperationResult res = GetResponse(message, ref response, timeoutMs, cancelWaitEvent);
                    if (res.OperationResult == yats.Ports.ReadOperationResult.Result.SUCCESS)
                    {
                        result = new byte[2 * registers];
                        Buffer.BlockCopy(response, 3, result, 0, 2 * registers);
                        if (Logger.IsDebugEnabled)
                        {
                            Logger.DebugFormat("Read  address {0} offset {1} regs {2} [{3}]", address, start, registers, result.ToHexString());
                        }
                    }
                    else
                    {
                        if (Logger.IsDebugEnabled)
                        {
                            Logger.DebugFormat("Read {0} {1} [{2}]", address, res, response.ToHexString());
                        }
                        result = null;
                    }
                    return res.OperationResult;
                }
                catch (Exception err)
                {
                    //modbusStatus = "Error in read event: " + err.Message;
                    Logger.Error("Exception ", err);
                    return yats.Ports.ReadOperationResult.Result.ERROR;
                }
            }
            else
            {
                //modbusStatus = "Serial port not open";
                return yats.Ports.ReadOperationResult.Result.PORT_ERROR;
            }
        }

        private void BuildMessage(byte address, byte type, ushort start, ushort registers, ref byte[] message)
        {
            //Array to receive CRC bytes:
            byte[] crc = new byte[2];

            message[0] = address;
            message[1] = type;
            message[2] = (byte)(start >> 8);
            message[3] = (byte)start;
            message[4] = (byte)(registers >> 8);
            message[5] = (byte)registers;

            CRC.GetCRC(message, ref crc);
            message[message.Length - 2] = crc[0];
            message[message.Length - 1] = crc[1];
        }

        private ReadOperationResult GetResponse(byte[] message, ref byte[] response, int timeoutMs, WaitHandle cancelWaitEvent)
        {
            int expectedBytes = response.Length;
            ModbusPacketParser parser = new ModbusPacketParser(expectedBytes);
            var res = port.Command(message, parser, timeoutMs, cancelWaitEvent); //timeoutMs + TimeoutEstimator.GetTimeoutMs(Port, expectedBytes)
            response = parser.GetReceivedData();
            return res;
        }
    }
}
