﻿/*
Copyright 2013 Antanas Veiverys www.veiverys.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using yats.Ports;

namespace yats.Ports.Utilities.Protocols.ModbusRTU
{
    internal class ModbusPacketParser : Parser
    {
        private byte[] bytes;
        private int index = 0;
        private ReadOperationResult res = null;
        private int expectedBytes;

        public ModbusPacketParser(int expectedBytes)
        {
            this.bytes = new byte[expectedBytes];
            this.expectedBytes = expectedBytes;
        }

        public override bool IncomingData(byte dataByte)
        {
            bytes[index] = dataByte;
            index++;
            if (index < expectedBytes)
            {
                return false;
            }
            if (CRC.CheckResponse(bytes))
            {
                res = new ReadOperationResult(yats.Ports.ReadOperationResult.Result.SUCCESS);
            }
            else
            {
                res = new ReadOperationResult(yats.Ports.ReadOperationResult.Result.ERROR);
            }
            return true;
        }

        public override ReadOperationResult GetResult()
        {
            return res;
        }

        public override byte[] GetReceivedData()
        {
            return bytes;
        }
    }
}
