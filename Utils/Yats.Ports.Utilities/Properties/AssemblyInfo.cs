﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Yats.Ports.Utilities")]
[assembly: AssemblyDescription("Port open helper for Yats.Ports, various example parsers")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Antanas Veiverys")]
[assembly: AssemblyProduct("Yats.Ports.Utilities")]
[assembly: AssemblyCopyright("Copyright ©  2012 Antanas Veiverys")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("e0fb3faa-6bb0-407e-b803-915bdf100446")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.*")]

